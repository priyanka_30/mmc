<?php $__env->startSection("contentheader_title"); ?>
	<a href="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases')); ?>">Medicalcase</a> :
<?php $__env->stopSection(); ?>
<?php $__env->startSection("contentheader_description", $heightweight->$view_col); ?>
<?php $__env->startSection("section", "Medicalcase"); ?>
<?php $__env->startSection("section_url", url(config('laraadmin.adminRoute') . '/medicalcases')); ?>
<?php $__env->startSection("sub_section", "Edit"); ?>

<?php $__env->startSection("htmlheader_title", "Medicalcase Edit : ".$heightweight->$view_col); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="post" action="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases/heightweightupdate/')); ?>" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
					  <input type="hidden" name="id" value="<?php echo e($heightweight->id); ?>">
													
							<div class="form-group col-md-6">
							<label for="Emailid">DOB :</label>
							<input class="form-control"  name="dob" type="text" value="<?php echo e($heightweight->dob); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="phone">Height :</label>
							<input class="form-control"  name="height" type="text" value="<?php echo e($heightweight->height); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="address">Weight :</label>
							<input class="form-control" name="weight" type="text" value="<?php echo e($heightweight->weight); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="city">LMP :</label>
							<input class="form-control"required="1" name="lmp" type="text" value="<?php echo e($heightweight->lmp); ?>" aria-required="true"></div>
							<div class="form-group col-md-12">
							<label for="pin">Blood Pressure:</label>
							<input class="form-control"required="1" name="blood_pressure" type="text" value="<?php echo e($heightweight->blood_pressure); ?>" aria-required="true"></div>
																	
					                    <br>
					<div class="form-group col-md-12">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('htmlheader_title'); ?>
	Package View
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
<div id="page-content" class="profile2">
	
	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="<?php echo e(url(config('laraadmin.adminRoute') . '/packages')); ?>" data-toggle="tooltip" data-placement="right" title="Back to Packages"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						<?php echo LAFormMaker::display($module, 'name'); ?>
						
						<?php echo LAFormMaker::display($module, 'tags'); ?>
						
						<?php echo LAFormMaker::display($module, 'color'); ?>
						
						<?php echo LAFormMaker::display($module, 'expiry_date'); ?>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>
	</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('la.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
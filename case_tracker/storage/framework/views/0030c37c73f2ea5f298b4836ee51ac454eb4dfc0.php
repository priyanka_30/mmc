Dear <?php echo e($user->name); ?>,<br><br>

You have been registered on <?php echo e(url('/')); ?>.<br><br>

Your login credentials for the same are as below:<br><br>

Username: <?php echo e($user->email); ?><br>
password: <?php echo e($password); ?><br><br>

You can login on <a href="<?php echo e(url('/login')); ?>"><?php echo e(str_replace("http://", "", url('/login'))); ?></a>.<br><br>

Best Regards,
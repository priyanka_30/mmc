<?php $__env->startSection("contentheader_title"); ?>
	<a href="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases')); ?>">Medicalcase</a> :
<?php $__env->stopSection(); ?>
<?php $__env->startSection("contentheader_description", $medicalcase->first_name); ?>
<?php $__env->startSection("section", "Medicalcase"); ?>
<?php $__env->startSection("section_url", url(config('laraadmin.adminRoute') . '/medicalcases')); ?>
<?php $__env->startSection("sub_section", "Edit"); ?>

<?php $__env->startSection("htmlheader_title", "Medicalcase Edit : ".$medicalcase->$view_col); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="POST" action="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases/caseupdate/')); ?>" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					<div class="box-body">
                  <?php /*  <?php echo LAFormMaker::form($module); ?> */ ?>
					<div class="col-md-12" style="padding-left:0;padding-right:0">
					<div class="form-group col-md-4">
						<label for="mobile">RegID :</label>
						<input class="form-control regid" placeholder="RegID" id="regid" name="regid" type="text" style="background: #fff;" readonly  value="<?php echo e($medicalcase->regid); ?>">
						<input type="hidden" name="caseid" value="<?php echo e($medicalcase->id); ?>">
					</div>
					<div class="form-group col-md-4 ">
						<label for="name">Assitsant Doctor:</label>
						<select class="form-control assitant_doctor" id="assitant_doctor" name="assitant_doctor"  required onchange="priceget(this);">
                            <option value="">Select Doctor</option>
							<?php foreach($doctors as $doctorslist){?>
							<option value="<?php echo $doctorslist->id;?>" <?php if($doctorslist->id == $medicalcase->assitant_doctor) { echo "selected"; } ?>><?php echo $doctorslist->name;?></option>
							<?php } ?>
						
						</select>	
                       
					</div>
					<div class="form-group col-md-4 ">
						<label for="mobile">Date :</label>
						
						    <input class="form-control dob" value="<?php echo date('d/m/Y',strtotime($medicalcase->created_at)); ?>" type="text"  readonly />
						  
						
					</div>
				</div>
				<div class="col-md-12" style="padding-left:0;padding-right:0">
					<div class="form-group col-md-2">
						<label for="name">Title:</label>
						<select class="form-control  title" required="1" name="title" required >
                             <option value="">Select </option>
							<option value="Mr" <?php if($medicalcase->title == "Mr") { echo "selected"; } ?> >Mr</option>
							<option value="Mrs" <?php if($medicalcase->title == "Mrs") { echo "selected"; } ?> >Mrs</option>
							<option value="Ms" <?php if($medicalcase->title == "Ms") { echo "selected"; } ?> >Ms</option>
                            <option value="Baby" <?php if($medicalcase->title == "Baby") { echo "selected"; } ?> >Baby</option>
							<option value="Dr" <?php if($medicalcase->title == "Dr") { echo "selected"; } ?> >Dr</option>
							<option value="Er" <?php if($medicalcase->title == "Er") { echo "selected"; } ?> >Er</option>
							<option value="Sir" <?php if($medicalcase->title == "Sir") { echo "selected"; } ?> >Sir</option>
                            <option value="Justice" <?php if($medicalcase->title == "Justice") { echo "selected"; } ?> >Justice</option>
						</select>	
					</div>
					<div class="form-group col-md-4">
						<label for="mobile">Name :</label>
						<input class="form-control first_name" id=first_name name="first_name" type="text" value="<?php echo e($medicalcase->first_name); ?>">
						
					</div>
					<div class="form-group col-md-3">
						<label for="mobile">Middle Name :</label>
						<input class="form-control middle_name"   name="middle_name" type="text" value="<?php echo e($medicalcase->middle_name); ?>">
					</div>
					<div class="form-group col-md-3">
						<label for="mobile">Surname :</label>
						<input class="form-control surname" id="surname"  name="surname" type="text" value="<?php echo e($medicalcase->surname); ?>">
						
					</div>
				</div>
				<div class="col-md-12"  style="padding-left:0;padding-right:0">
				     <div class="col-md-6">
				         <div class="col-md-12" style="padding-left:0;padding-right:0">
				             <div class="form-group col-md-6" style="padding-left:0">
				                 <label for="name">Gender:</label>
            						<select class="form-control  gender"  name="gender"  >
                                         <option value="">Select</option>
            							<option value="Male" <?php if($medicalcase->gender == "Male") { echo "selected"; } ?> >Male</option>
            							<option value="Female" <?php if($medicalcase->gender == "Female") { echo "selected"; } ?> >Female</option>
            							<option value="Other" <?php if($medicalcase->gender == "Other") { echo "selected"; } ?> >Other</option>
            						</select>
				             </div>
				             <div class="form-group col-md-6" style="padding-right:0">
				                 <label for="name">Marital Status:</label>
        						<select class="form-control status"  name="status" required>
                                     <option value="">Select</option>
        							<option value="Unmarried" <?php if($medicalcase->status == "Unmarried") { echo "selected"; } ?> >Unmarried</option>
        							<option value="Married" <?php if($medicalcase->status == "Married") { echo "selected"; } ?> >Married</option>
        							<option value="Other" <?php if($medicalcase->status == "Other") { echo "selected"; } ?> >Other</option>
        						</select>
				             </div>
				         </div>
				         <div class="form-group col-md-12 checkbox-div">
    						<label for="name">Address:</label>
    						<input class="form-control address"  id="address" name="address" type="text" value="<?php echo e($medicalcase->address); ?>">
    						
    					</div>
    					<div class="form-group col-md-12" style="padding-left:0; padding-right:0">
    						<label for="mobile">Road :</label>
    						<input class="form-control road" placeholder="Road" id="road" name="road" type="text" value="<?php echo e($medicalcase->road); ?>">
    					</div>
    					<div class="form-group col-md-12" style="padding-left:0;padding-right:0">
    					    <div class="form-group col-md-6" style="padding-left:0">
        						<label for="mobile">Area/Sector :</label>
        						<input class="form-control area" placeholder="Area/Sector" id="area" name="area" type="text" value="<?php echo e($medicalcase->area); ?>">
        					</div>
        					<div class="form-group col-md-6" style="padding-right:0">
        						<label for="mobile">City :</label>
        						<input class="form-control city" placeholder="City" id="city"  name="city" type="text" value="<?php echo e($medicalcase->city); ?>">
        						
        					</div>
    					</div>
    					<div class="form-group col-md-12" style="padding-left:0;padding-right:0">
    					    <div class="form-group col-md-6" style="padding-left:0">
        						<label for="name">State:</label>
        						<?php $indian_all_states  = array (
								 'AP' => 'Andhra Pradesh',
								 'AR' => 'Arunachal Pradesh',
								 'AS' => 'Assam',
								 'BR' => 'Bihar',
								 'CT' => 'Chhattisgarh',
								 'GA' => 'Goa',
								 'GJ' => 'Gujarat',
								 'HR' => 'Haryana',
								 'HP' => 'Himachal Pradesh',
								 'JK' => 'Jammu & Kashmir',
								 'JH' => 'Jharkhand',
								 'KA' => 'Karnataka',
								 'KL' => 'Kerala',
								 'MP' => 'Madhya Pradesh',
								 'MH' => 'Maharashtra',
								 'MN' => 'Manipur',
								 'ML' => 'Meghalaya',
								 'MZ' => 'Mizoram',
								 'NL' => 'Nagaland',
								 'OR' => 'Odisha',
								 'PB' => 'Punjab',
								 'RJ' => 'Rajasthan',
								 'SK' => 'Sikkim',
								 'TN' => 'Tamil Nadu',
								 'TR' => 'Tripura',
								 'UK' => 'Uttarakhand',
								 'UP' => 'Uttar Pradesh',
								 'WB' => 'West Bengal',
								 'AN' => 'Andaman & Nicobar',
								 'CH' => 'Chandigarh',
								 'DN' => 'Dadra and Nagar Haveli',
								 'DD' => 'Daman & Diu',
								 'DL' => 'Delhi',
								 'LD' => 'Lakshadweep',
								 'PY' => 'Puducherry',
								); ?>
        						<select class="form-control state"  name="state">
        							<?php foreach($indian_all_states as $key=>$value){?>
        							<option value="<?php echo $value;?>" <?php if($medicalcase->state == $value) { echo "selected"; } ?>><?php echo $value;?></option>
        							<?php }?>
        						</select>	
        					</div>
        					<div class="form-group col-md-6" style="padding-right:0">
        						<label for="mobile">Pin :</label>
        						<input class="form-control pin" placeholder="Pin" id="pin" name="pin" type="text" value="<?php echo e($medicalcase->pin); ?>">
        						
        					</div>
    					</div>
    					
						<div class="form-group col-md-12" style="padding-left:0;padding-right:0">
    						<label for="mobile">Email ID :</label>
    						<input class="form-control email" name="email" type="text" value="<?php echo e($medicalcase->email); ?> ">
    						
    					</div>
    					
                        
                        <div class="form-group col-md-12 religion-box" style="padding-left:0;padding-right:0">
                            <label for="name">Select Scheme:</label>
                            <select class="form-control scheme" name="scheme" >
                                    <option value="Other">Other</option>
                                <?php foreach($packages as $packagelist){?>
                                    <option value="<?php echo $packagelist->name;?>" <?php if($medicalcase->scheme == $packagelist->name) { echo "selected"; } ?> ><?php echo $packagelist->name;?></option>
                                <?php } ?>
                                    
                            </select>   
                        </div>
    					<div class="col-md-12">&nbsp;</div>
						
    					
				     </div>
				     <div class="col-md-1">&nbsp;</div>
				     <div class="col-md-5">
				         <div class="form-group col-md-12 religion-box">
        						<label for="mobile">Date of Birth :</label>
        						    <input class="form-control date_of_birth" type="text" name="date_of_birth" id="dob" value="<?php echo e($medicalcase->date_of_birth); ?>"  />
        				 </div>
        				 <div class="form-group col-md-12 religion-box">
    						<label for="name">Religion:</label>
    						<select class="form-control religion"  name="religion">
                                <option value="">Select</option>
    							<option value="Indian" <?php if($medicalcase->religion == "Indian") { echo "selected"; } ?> >Indian</option>
    							<option value="Hindu" <?php if($medicalcase->religion == "Hindu") { echo "selected"; } ?> >Hindu</option>
    							<option value="Muslim" <?php if($medicalcase->religion == "Muslim") { echo "selected"; } ?> >Muslim</option>
    							<option value="Christian" <?php if($medicalcase->religion == "Christian") { echo "selected"; } ?> >Christian</option>
    							<option value="Sikh" <?php if($medicalcase->religion == "Sikh") { echo "selected"; } ?> >Sikh</option>
    							<option value="Other" <?php if($medicalcase->religion == "Other") { echo "selected"; } ?> >Other</option>
    						</select>	
    					</div>
    					<div class="form-group col-md-12 religion-box">
    						<label for="mobile">Phone No :</label>
    						<input class="form-control phone" placeholder="Enter Phone" data-rule-maxlength="20" id="phone" name="phone" type="text" value="<?php echo e($medicalcase->phone); ?>"value=""required>
    						
    					</div>
    					<div class="form-group col-md-12 religion-box">
    						<label for="name">Occupation:</label>
    						
                            <input class="form-control occupation" placeholder="Enter Occupation" name="occupation" type="text" value="<?php echo e($medicalcase->occupation); ?>">
    					</div>
    					<div class="form-group col-md-12 religion-box" style="margin-top: 10px;">
    						<label for="mobile">Mobile 1 / Mother Number:</label>
    						<input class="form-control mobile1" placeholder="Enter Mobile1" data-rule-maxlength="20" id="mobile1" name="mobile1" type="text" value="<?php echo e($medicalcase->mobile1); ?>">
    						
    					</div>
    					
    					<div class="form-group col-md-12 religion-box" style="margin-top: 10px;">
    						<label for="mobile">Landline with STD code  :</label>
    						<input class="form-control mobile2" placeholder="Enter Mobile2" data-rule-maxlength="20" id="mobile2" name="mobile2" type="text" value="<?php echo e($medicalcase->mobile2); ?>">
    					</div>
    					
						
    					
    					
				     </div>
				</div>
				
				<div class="form-group col-md-12">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
					
					
										
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
	
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
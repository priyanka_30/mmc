<?php $__env->startSection("contentheader_title", "Billing"); ?>
<?php $__env->startSection("contentheader_description", "Billing listing"); ?>
<?php $__env->startSection("section", "Billing"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Billing Listing"); ?>

<?php $__env->startSection("headerElems"); ?>
<?php if(LAFormMaker::la_access("Billing", "create")) { ?>
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Billing</button>
<?php } ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>Bill No.</th>
			<th>From Date</th>
			<th>To Date</th>
			<th>RegId</th>
			<th>Name</th>
			<th>Charges</th>
			<th>Received</th>
			<th>Balance</th>
		
		</tr>
		</thead>
		<tbody>
			<?php
				foreach($cases as $val){

			 ?>
			<tr>
				<td><?php echo $val->BillNo ?></td>
				<td><?php echo date('d M Y',strtotime($val->fromdate)); ?></td>
				<td><?php echo date('d M Y',strtotime($val->todate)); ?></td>
				<td><?php echo $val->regid ?></td>
				<td><?php echo $val->first_name ?> <?php echo $val->surname ?></td>
				<td><?php echo $val->charges ?></td>
				<td><?php echo $val->received ?></td>
				<td><?php echo $val->Balance ?></td>
			</tr>
		<?php } ?>
			
		</tbody>
		</table>
	</div>
</div>

<?php if(LAFormMaker::la_access("Billing", "create")) { ?>
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Billing</h4>
			</div>
			<?php echo Form::open(['action' => 'LA\BillingController@addbilling', 'id' => 'daycharges-add-form']); ?>

			<div class="modal-body">
				<div class="box-body">
                   
					<div class="form-group col-md-12 ">
						<label for="name">Case ID:</label>
						<select class="form-control select2-hidden-accessible" required="1" data-placeholder="CaseID" rel="select2" name="case_id" tabindex="-1" aria-hidden="true" aria-required="true"required>
							<?php foreach($cases as $data){?>
                                    <option value="<?php echo $data->regid;?>"><?php echo $data->regid;?></option>
                                <?php } ?>
						</select>	
					</div>
					
					
					<div class="form-group col-md-12">
						<label for="mobile">Amount :</label>
						<input class="form-control" placeholder="Amount"  name="amount" type="text" value="" aria-required="true"required>
					</div>
					<div class="form-group col-md-12">
						<label for="mobile" >Mode of Payment</label>
					</div>
					<div class="form-group col-md-12">
                            <label class="container-radio">Cash
                              <input type="radio" name="mode" class="send_sms" value="Cash">
                              <span class="checkmark-radio"></span>
                            </label>
                            <label class="container-radio">Cheque
                              <input type="radio" name="mode" class="send_sms" value="Cheque">
                              <span class="checkmark-radio"></span>
                            </label>
                             <label class="container-radio">Credit Card
                              <input type="radio" name="mode" class="send_sms" value="Credit Card">
                              <span class="checkmark-radio"></span>
                            </label>
                             <label class="container-radio">Online
                              <input type="radio" name="mode" class="send_sms" value="Online">
                              <span class="checkmark-radio"></span>
                            </label>
					</div>  
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?php echo Form::submit( 'Submit', ['class'=>'btn btn-success']); ?>

			</div>
			<?php echo Form::close(); ?>

		</div>
	</div>
</div>
<?php } ?>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        //serverSide: true,
       // ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/billing_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
	
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
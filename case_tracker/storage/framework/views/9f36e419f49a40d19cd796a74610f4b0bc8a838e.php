<?php $__env->startSection("contentheader_title"); ?>
	<a href="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases')); ?>">Medicalcase</a> :
<?php $__env->stopSection(); ?>
<?php $__env->startSection("contentheader_description", $homeodetail->$view_col); ?>
<?php $__env->startSection("section", "Medicalcase"); ?>
<?php $__env->startSection("section_url", url(config('laraadmin.adminRoute') . '/medicalcases')); ?>
<?php $__env->startSection("sub_section", "Edit"); ?>

<?php $__env->startSection("htmlheader_title", "Medicalcase Edit : ".$homeodetail->$view_col); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="post" action="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases/homeoupdate/')); ?>" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					
					  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
													
							<div class="form-group col-md-6">
							<label for="Emailid">Diagnosis :</label>
							<input type="hidden" name="id" value="<?php echo e($homeodetail->id); ?>">
							<input class="form-control"  name="diagnosis" type="text" value="<?php echo e($homeodetail->diagnosis); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="phone">Complaint Intensity :</label>
							<input class="form-control"  name="complaint_intesity" type="text" value="<?php echo e($homeodetail->complaint_intesity); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="address">Medication Taking :</label>
							<input class="form-control" name="medication_taking" type="text" value="<?php echo e($homeodetail->medication_taking); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="city">Investigation* :</label>
							<input class="form-control"required="1" name="investigation" type="text" value="<?php echo e($homeodetail->investigation); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="pin">Case Taken by:</label>
							<input class="form-control"required="1" name="case_taken" type="text" value="<?php echo e($homeodetail->case_taken); ?>" aria-required="true"></div>
																	
					                    <br>
					<div class="form-group col-md-12">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection("contentheader_title", "Print Stickers"); ?>




<?php $__env->startSection("main-content"); ?>

<style type="text/css" media="print">
  @page  { size: landscape; }

  

</style>

<div class="box">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<div class="row">
			<?php

				for($i=1;$i<=$no; $i++){
			 ?>
			<div class="col-xs-2" style="text-align: center;border: 1px solid #ccc; border-radius: 10px;margin: 10px;">
				<p style="padding-top: 11px;">Homeo Clinic 1 </p>
				<p><?php echo $name;?></p>
				<p style="padding-bottom: 19px;"><span style="float:left;"><?php echo date('d M Y') ?></span> 
					<span style="float: right;">ID:<?php echo $regid; ?></span></p>

			</div>
		<?php } ?>
		<div class="col-xs-12">
			<button class="btn btn-success btnprint" onclick="printbtn();" >Print</button>
		</div>
		</div>
	</div>
</div>



<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script>
	function printbtn(){
		$('.btnprint').css('display','none');
		$('.main-footer').css('display','none');
		
		window.print();


	}
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/smstemplate_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		<?php if($show_actions): ?>
		columnDefs: [ { orderable: false, targets: [-1] }],
		<?php endif; ?>
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
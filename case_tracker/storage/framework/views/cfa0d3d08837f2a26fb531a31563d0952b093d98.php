<?php $__env->startSection("contentheader_title", "Couriers"); ?>
<?php $__env->startSection("contentheader_description", "Couriers listing"); ?>
<?php $__env->startSection("section", "Couriers"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Couriers Listing"); ?>

<?php $__env->startSection("headerElems"); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<style type="text/css">

.modal-body  .box-body .form-group:first-child {
display:none;
    
}


</style>
<div class="box box-success">
    <?php if(LAFormMaker::la_access("Couriers", "create")) { ?>

    <div class="col-sm-12">
<ul class="nav navbar-nav">

		<li><a href="javascript:void(0)" class="topmenu-icon-text" data-toggle="modal" data-target="#usermodal"><img src="<?php echo e(asset('la-assets/img/newuser.png')); ?>" class="topmenu-icon"><br>New</a></li>
		<?php /*?><li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/openfile.png') }}" class="topmenu-icon"><br>Open</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/import.png') }}" class="topmenu-icon"><br>Import</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/export.png') }}" class="topmenu-icon"><br>Export</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/find.png') }}" class="topmenu-icon"><br>Find</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/followup.png') }}" class="topmenu-icon"><br>Follow Up</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/print-icon.png') }}" class="topmenu-icon"><br>Print</a></li>
		 <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/delete.png') }}" class="topmenu-icon"><br>Delete Case</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/exit.png') }}" class="topmenu-icon"><br>Exit</a></li> <?php */ ?>

		
	</ul>
</div><?php } ?>
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<?php foreach( $listing_cols as $col ): ?>
					<th><?php echo e(isset($module->fields[$col]['label']) ? $module->fields[$col]['label'] : ucfirst($col)); ?></th>
			<?php endforeach; ?>
			<?php if($show_actions): ?>
			<th>Actions</th>
			<?php endif; ?>
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

<?php if(LAFormMaker::la_access("Couriers", "create")) { ?>
<div class="modal fade" id="usermodal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" style="width:900px">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Courier Package</h4>
			</div>
			<?php echo Form::open(['action' => 'LA\CouriersController@create', 'id' => 'caseremind-add-form']); ?>

			   <?php echo csrf_field(); ?>


			<div class="modal-body">
				<div class="box-body">
                    <div class="col-md-12 ">
						<label for="name">Package Id:</label>
							<input class="form-control regid" placeholder="Package Id"  name="package_id" id="package_id" type="text" value="" aria-required="true" required>

					</div>

					  <div class="customer_records">
					  	<div class="col-md-4">
				  		<label for="name">Name:</label>
					    <select class="form-control medicine_name" data-placeholder="Name"  name="name[]" id="medicine_name" required onchange="priceget(this);" >
					   	
					    		<option disabled selected>Select Name</option>
	        							<?php foreach($medicines as $stname){?>
	        							<option value="<?php echo e($stname->remedy); ?>" data-price="100"><?php echo e($stname->remedy); ?></option>
	        							<?php }?>
						</select>
						</div>
						<div class="col-md-2">
							<label for="name">Quantity:</label>
					    	<input name="quantity[]" id="quantity" type="number" value="1" class="form-control quantity" placeholder="Quantity" oninput="calc()">
						</div>
						<div class="col-md-2">
							<label for="name">Price:</label>
				    		<input type="text" class="form-control price" placeholder="Price" id="price" readonly>
				    	</div>
						<div class="col-md-2">
							<label for="name">Total Price:</label>
					    	<input name="price[]" type="text" class="form-control total" placeholder="Total Price" id="total">
				    	</div>
				    	
				    	<div class="col-md-2">
				    		<label for="name">&nbsp;</label>
					    	<a class="btn btn-success extra-fields-customer" href="#" style="margin-top: 24px;">Add More</a>
				    	</div>
					  </div>

					  <div class="customer_records_dynamic"></div>


					   
				
				</div>						
			</div>                  
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?php echo Form::submit( 'Submit', ['class'=>'btn btn-success']); ?>

				
			</div>
		</tr>
			<?php echo Form::close(); ?>

		</div>
	</div>
</div>

<?php } ?>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datepicker/datepicker3.css')); ?>"/>

<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('la-assets/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>

<script>
$(function () {
    // $('').attr('name'='booking_date')
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/couriers_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		<?php if($show_actions): ?>
		columnDefs: [ { orderable: false, targets: [-1] }],
		<?php endif; ?>
	});
	$("#caseremind-add-form").validate({
		
	});
	
 
    
});

</script>
<script type="text/javascript">
    //setTimeout(function() { $( "booking-add-form" ).hide();},1000);
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
   
    $('#caseremind-add-form').find("input[name='start_date']").val(today);
     $('#caseremind-add-form').find("input[name='end_date']").val(today);

	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
 
</script>

<script type="text/javascript">
	function randomNumber(len) {
    var randomNumber;
    var n = '';

    for(var count = 0; count < len; count++) {
        randomNumber = Math.floor(Math.random() * 10);
        n += randomNumber.toString();
    }
    return n;
}

document.getElementById("package_id").value = randomNumber(6);
</script>
<script type="text/javascript">
$('.extra-fields-customer').click(function() {
  $('.customer_records').clone().appendTo('.customer_records_dynamic');
  $('.customer_records_dynamic .customer_records').addClass('single remove');
  $('.single .extra-fields-customer').remove();
  $('.single').append('<a href="#" class="btn btn-danger remove-field btn-remove-customer" style="margin-left: 22px;">Remove</a>');
  $('.customer_records_dynamic > .single').attr("class", "remove");

  $('.customer_records_dynamic input').each(function() {
    var count = 0;
    var fieldname = $(this).attr("name");
    $(this).attr('name', fieldname + count);
    count++;
  });

});

$(document).on('click', '.remove-field', function(e) {
  $(this).parent('.remove').remove();
  e.preventDefault();
});

</script>
<script>
// 	$('.medicine_name').on('change',function(){
//     var price = $(this).children('option:selected').data('price');
//      $(".price").val(price);
// });

function calc() 
{
  var price =  $(".price").val();
  var quantity =  $(".quantity").val();
  var total = parseFloat(price) * quantity
  if (!isNaN(total))
	$(".total").val(total);
}

function priceget(obj){
	var price = $(obj).children('option:selected').attr('data-price');
	 $(obj).parent().parent().find(".price").val(price);
}
</script>
<?php $__env->stopPush(); ?>
<style>
    .error{ display:none;}
</style>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection("contentheader_title", "Follow Up Dues"); ?>
<?php $__env->startSection("contentheader_description", "Follow Up Dues listing"); ?>
<?php $__env->startSection("section", "Follow Up Dues"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Follow Up Dues Listing"); ?>

<?php $__env->startSection("headerElems"); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
<style type="text/css">
	
</style>
<div class="box box-success">
   
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<div class="form-horizontal" style="margin-bottom: 6rem;">
          <div class="control-group form-inline">
          	<?php echo Form::open(['action' => 'LA\PendingappointmentsController@search', 'id' => 'daycharges-add-form']); ?>

            <div class="form-group col-md-3">
				<label for="mobile" >From Date</label>
				<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
				    <input class="form-control" type="text" name="from_date" readonly style="background-color: #fff;" value="<?php echo $fromdate; ?>" />
				    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
			</div>
			  <div class="form-group col-md-3">
				<label for="mobile" >To Date</label>
				<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
				    <input class="form-control" type="text" name="to_date" readonly style="background-color: #fff;"  value="<?php echo $todate; ?>"/>
				    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
			</div> 
			<div class="form-group col-md-4">
				
				<?php echo Form::submit( 'Submit', ['class'=>'btn btn-success']); ?>

			</div>
			<?php echo Form::close(); ?>

          </div>
        </div>
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>RegID</th>
			<th>Person Name</th>
			<th>Mobile1</th>
			<th>Mobile2</th>
			<th>FollowUp Date</th>
			<th>Due Date</th>
			<th>Call Status</th>
			<th>Call Date</th>
			<th>Action</th>
			
		</tr>
		</thead>
		<tbody>
			<?php 
			foreach($case as $val){
					$now = strtotime($val->todate); // or your date as well
					$your_date = strtotime($val->dateval);
					$datediff = $now - $your_date;
			?>
			<tr>
				<td><?php echo $val->regid ?></td>
				<td><?php echo $val->first_name. " ". $val->surname;  ?></td>
				<td><?php echo $val->phone ?></td>
				<td><?php echo $val->mobile1 ?></td>
				<td><?php echo date('d M Y',strtotime($val->dateval)); ?></td>
				<td><?php echo date('d M Y',strtotime($val->todate)); ?></td>
				<td><?php echo $val->call_status ?></td>
				<td><?php echo $val->call_date; ?></td>
				
				<td>
					<form method="post" style="margin: 0px;display: inline-block;">
						<input type="hidden" name="" value="<?php echo $val->regid ?>">
						 <a onClick="opensms(<?php echo $val->regid; ?>)" class="btn btn-success btn-xs" style="display:inline;padding: 2px 7px 2px 8px; margin-left:10px; background-color:#343a40; border-color: #343a40;">Send SMS</a>
					</form>
					<a style="padding:2px; margin-left:10px;display: inline-block;" onclick="openreceived(<?php echo $val->id ?>,<?php echo $val->regid ?>)" class="btn btn-primary">Status</a>
				</td>
				
			</tr>
			<?php } ?>
			
		</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="received" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Status</h4>
            </div>
            <?php echo Form::open(['action' => 'LA\PendingappointmentsController@updatestatus', 'id' => 'daycharges-add-form']); ?>

            <div class="modal-body">
                <div class="box-body">
                  
                 <div class="form-group col-md-12 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control pregid" placeholder="RegID" readonly  name="regid" type="text" value="" aria-required="true" required style="background-color: #fff;">
                    </div>
                    <div class="col-md-12"  style="padding-left:0;padding-right:0; ">
                   		<div class="col-md-12" style="margin-bottom: 10px;">
                   			 <label for="mobile" >Status</label>
                   			 <input type="hidden" name="billid" class="billid">
                    			<select class="form-control" name="call_status">
                    				<option value="">Select</option>
                    				
                    				<option value="informed">Informed</option>
                    				<option value="Courier">Courier</option>
                    				<option value="Pickup">Pickup</option>
                    				<option value="Reserve Medicine">Reserve Medicine</option>
                    				<option value="Discontinued">Discontinued</option>
                    			</select>
                      	</div>

                  	 <div class="form-group col-md-12 ">
                        <label for="mobile">Date :</label>
                        <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                            <input class="form-control dob" value="<?php echo date('d/m/Y'); ?>" type="text" name="call_date" readonly style="background-color: #fff;"/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                    
                </div>
                </div>
            </div>
            <div class="modal-footer">
            	<button type="submit" class="btn btn-success height-btn savereceived">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>

<div class="modal fade" id="opensms" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Send SMS</h4>
            </div>
            <?php echo Form::open(['action' => 'LA\PendingappointmentsController@sendsms', 'id' => 'daycharges-add-form']); ?>

            <div class="modal-body">
                <div class="box-body">
                  
                 <div class="form-group col-md-12 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control smsid" placeholder="RegID" readonly  name="smsid" type="text" value="" aria-required="true"required>
                    </div>

                     <div class="col-md-12"  style="">

                        <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                            <label for="mobile">Message:</label>
                            <input type="hidden" name="phone" class="phone">
                                <textarea class="form-control message" placeholder="Enter Message" cols="30" rows="5" name="message" style="margin-top: 10px;" required></textarea>
                        </div>
                  
                      

                    
                </div>
                  
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success height-btn usersendsms">Send SMS</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datepicker/datepicker3.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('la-assets/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>

<script>
	  function opensms(id){
    $('.smsid').val(id);
      var id = id;
    $.ajax({
            type: "GET",
            url: "<?php echo e(url(config('laraadmin.adminRoute') . '/getcasedetail')); ?>",
            data: { id:id }, 
            success: function( msg ) {
              	var casearr = JSON.parse(msg);
        	 	
        	 	var regid= casearr['regid'];
        	 	var message= casearr['message'];
        	 	var phone= casearr['phone'];
            	$('.regid').val(regid);
            	$('.message').val(message);
            	$('.phone').val(phone);
               	$('#opensms').modal('show');
            }
        });
    
}
	$(".sendsms").on("click", function(){
		var regid = $(".regid").val();
		
		$.ajax({
                type: "POST",
                url: "<?php echo e(url(config('laraadmin.adminRoute') . '/sendpendingsms')); ?>",
                data: { regid:regid}, 
                success: function( msg ) {
                	alert("SMS Sent Successfully");
                }
            });
	});
$(function () {

	
    // $('').attr('name'='booking_date')
	$("#example1").DataTable({
		processing: true,
      //  serverSide: true,
    //    ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/pendingappointments_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		dom: 'Bfrtip',
        buttons: [
            {
             extend: 'excel',
             text: 'Excel',
             className: 'btn btn-default',
             exportOptions: {
                columns: 'th:not(:last-child)'
             }
          },
           {
             extend: 'pdfHtml5',
             text: 'PDF',
             className: 'btn btn-default',
             exportOptions: {
                columns: 'th:not(:last-child)'
             }
          }
        ],
		<?php if($show_actions): ?>
		columnDefs: [ { orderable: false, targets: [-1] }],
		<?php endif; ?>
	});
	$("#booking-add-form").validate({
		
	});
	
 
    
});

</script>
<script type="text/javascript">
    //setTimeout(function() { $( "booking-add-form" ).hide();},1000);
    $('#booking-add-form').find("input[name='status']").parent().hide();
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
   
    $('#booking-add-form').find("input[name='booking_date']").val(today);

	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });

	function openreceived(id,regid)
	{
   
    
    $('#received').modal('show');
    $(".billid").val(id);
    var regid = $(".pregid").val(regid);
           
	}
 
</script>
<script>
	$('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});
</script>
<?php $__env->stopPush(); ?>
<style>
    .error{ display:none;}
</style>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
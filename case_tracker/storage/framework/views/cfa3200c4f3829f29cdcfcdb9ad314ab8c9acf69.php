<?php $__env->startSection('htmlheader_title'); ?>
	Payment View
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
<div id="page-content" class="profile2">
	
	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="<?php echo e(url(config('laraadmin.adminRoute') . '/payments')); ?>" data-toggle="tooltip" data-placement="right" title="Back to Payment"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						<?php echo LAFormMaker::display($module, 'case_id'); ?>
						<?php echo LAFormMaker::display($module, 'patient_name'); ?>
						<?php echo LAFormMaker::display($module, 'doctor_id'); ?>
						<?php echo LAFormMaker::display($module, 'consultation_fee'); ?>
						<?php echo LAFormMaker::display($module, 'medicine_price'); ?>
						<?php echo LAFormMaker::display($module, 'total_price'); ?>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('la.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
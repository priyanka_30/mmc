<?php $__env->startSection("contentheader_title", "Doctor"); ?>
<?php $__env->startSection("contentheader_description", "Doctor listing"); ?>
<?php $__env->startSection("section", "Doctor"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Doctor Listing"); ?>

<?php $__env->startSection("headerElems"); ?>
<?php if(LAFormMaker::la_access("Doctors", "create")) { ?>
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Doctor</button>
<?php } ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<?php foreach( $listing_cols as $col ): ?>
			<th><?php echo e(isset($module->fields[$col]['label']) ? $module->fields[$col]['label'] : ucfirst($col)); ?></th>
			
			<?php endforeach; ?>
			<?php if($show_actions): ?>
			<th>Actions</th>
			<?php endif; ?>
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

<?php if(LAFormMaker::la_access("Doctors", "create")) { ?>
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Doctor</h4>
			</div>
			<?php echo Form::open(['action' => 'LA\DoctorsController@store', 'id' => 'doctor-add-form' , 'onsubmit' => 'return checkForm(this)' ,'files'=>true]); ?>

			<div class="modal-body">
				<div class="box-body">
				    <?php //echo "<pre>"; print_r($module); die();?>
				    <?php if($errors->any()): ?>
			        <h4 style="color: #000;background: #d4edda;border-color :#d4edda; text-align: center; font-size: 16px;padding: 7px;"><?php echo e($errors->first()); ?></h4>
			        <?php endif; ?>
                    <?php echo LAFormMaker::form($module); ?>
				
					<?php /*
					
					<?php echo LAFormMaker::input($module, 'designation'); ?>
					<?php echo LAFormMaker::input($module, 'gender'); ?>
					<?php echo LAFormMaker::input($module, 'mobile'); ?>
					<?php echo LAFormMaker::input($module, 'mobile2'); ?>
					<?php echo LAFormMaker::input($module, 'email'); ?>
					<?php echo LAFormMaker::input($module, 'consultation_fee'); ?>
					<?php echo LAFormMaker::input($module, 'dept'); ?>
					<?php echo LAFormMaker::input($module, 'city'); ?>
					<?php echo LAFormMaker::input($module, 'address'); ?>
					<?php echo LAFormMaker::input($module, 'about'); ?>
					<?php echo LAFormMaker::input($module, 'date_birth'); ?>
					<?php echo LAFormMaker::input($module, 'date_hire'); ?>
					<?php echo LAFormMaker::input($module, 'date_left'); ?>
					<?php echo LAFormMaker::input($module, 'salary_cur'); ?>
					*/ ?>
					
					<div class="form-group" style="display:none;">
						<label for="role">Role* :</label>
						<select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="role">
							<option value="3">Doctor</option>
						</select>
					</div>
					<input type="file" name="profilepic">
				</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?php echo Form::submit( 'Submit', ['class'=>'btn btn-success']); ?>

			</div>
			<?php echo Form::close(); ?>

		</div>
	</div>
</div>
<?php } ?>


<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script>
$(function () {
	//$("#NewModal").modal("show");
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/doctor_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		<?php if($show_actions): ?>
		columnDefs: [ { orderable: false, targets: [-1] }],
		<?php endif; ?>
	});
	$("#doctor-add-form").validate({
		
	});
});

function checkForm(form)
  {
  
    if(form.password.value != "" ) {
      if(form.password.value.length < 6) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least six characters!";
       
        form.password.focus();
        return false;
      }
           re = /[0-9]/;
      if(!re.test(form.password.value)) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least one number (0-9)!";
        form.password.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(form.password.value)) {
      	document.getElementById("password_strength").innerHTML = "Password must contain at least one lowercase letter (a-z)!";
         
        form.password.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(form.password.value)) {
      	document.getElementById("password_strength").innerHTML = "Password must contain at least one uppercase letter (A-Z)!";
                form.password.focus();
        return false;
      }
    } else {
      form.password.focus();
      return false;
    }

  }
</script>
<?php $__env->stopPush(); ?>

<style>
    .form-group{ float:left; width:45%;margin-left:15px; margin-right:15px;}
    .modal.in .modal-dialog{ width:800px;}
</style>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
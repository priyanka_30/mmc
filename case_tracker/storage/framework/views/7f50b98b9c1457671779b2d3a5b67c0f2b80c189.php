

<?php $__env->startSection("contentheader_title", "Remedy"); ?>
<?php $__env->startSection("contentheader_description", "Remedy listing"); ?>
<?php $__env->startSection("section", "Remedy"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Remedy Listing"); ?>

<?php $__env->startSection("headerElems"); ?>
<?php if(LAFormMaker::la_access("Smstemplate", "create")) { ?>
	<!-- <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Smstemplate</button> -->
<?php } ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>RegID</th>
			<th>Name</th>
			<th>Remedy</th>
			<th>Potency</th>
			<th>Frequency</th>
			<th>Days</th>
			<th>Prescription</th>
			<th>Post Type</th>
			<th>Print Stickers</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($CasePotency as $val){ ?>
			<tr>
				<td><?php echo  $val->cregid; ?></td>
				<td><?php echo  $val->first_name. " ".$val->surname; ?></td>
				<td><?php echo  $val->name; ?></td>
				<td><?php echo  $val->pname; ?></td>
				<td><?php echo  $val->frequency; ?></td>
				<td><?php echo  $val->days; ?></td>
				<td><?php echo  $val->rxprescription; ?></td>
				<td><?php  if($val->post_type==""){ echo "N/A";} else { echo  $val->post_type;} ; ?></td>
				<td>
					<a onClick="openSticker(<?php echo $val->cregid; ?>,'<?php echo $val->first_name; ?>')" class="btn btn-primary btn-xs" style="display:inline;padding: 2px 7px 2px 8px; margin-left:10px; ">Print Stickers</a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		</table>
	</div>
</div>

<?php if(LAFormMaker::la_access("Remedy", "create")) { ?>
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Smstemplate</h4>
			</div>
			<?php echo Form::open(['action' => 'LA\SmstemplateController@store', 'id' => 'daycharges-add-form']); ?>

			<div class="modal-body">
				<div class="box-body">
                    <div class="box-body">
                    <div class="form-group">
                    	<label for="templatename">Template Name :</label>
                    	<input class="form-control" placeholder="Enter Template Name" name="templatename" type="text" value="">
                    </div>
                    <div class="form-group">
                    		<label for="message">Message :</label>
                    		<textarea class="form-control" placeholder="Enter Message" name="message" type="text" value="" rows="5"></textarea>
                		</div>
                    <div class="form-group"><label for="smstype">SMS Type :</label>
                    <!-- 	<input class="form-control" placeholder="Enter SMS Type" name="smstype" type="text" value=""> -->
                    <label class="container-radio">Normal
                              <input type="radio" name="smstype" class="send_sms" value="Normal">
                              <span class="checkmark-radio"></span>
                            </label>
                            <label class="container-radio">Birthday
                              <input type="radio" name="smstype" class="send_sms" value="Birthday">
                              <span class="checkmark-radio"></span>
                            </label>
                             <label class="container-radio">Courier
                              <input type="radio" name="smstype" class="send_sms" value="Courier">
                              <span class="checkmark-radio"></span>
                            </label>
                             </div>					
									</div>
					
					<?php /*
					<?php echo LAFormMaker::input($module, 'name'); ?>
					<?php echo LAFormMaker::input($module, 'tags'); ?>
					<?php echo LAFormMaker::input($module, 'color'); ?>
					*/ ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?php echo Form::submit( 'Submit', ['class'=>'btn btn-success']); ?>

			</div>
			<?php echo Form::close(); ?>

		</div>
	</div>
</div>

<div class="modal fade" id="openSticker" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Print Stickers</h4>
            </div>
            <?php echo Form::open(['action' => 'LA\DailycollectionController@printstickers', 'id' => 'daycharges-add-form', 'target'=>'_blank']); ?>

            <div class="modal-body">
                <div class="box-body">
                  
                 <div class="form-group col-md-12 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control sticker_regid" placeholder="RegID" readonly  name="sticker_regid" type="text" value="" aria-required="true"required>
                    </div>

                    <div class="form-group col-md-12 " class="patient-info-td">
                        <label for="mobile" >Name</label>
                        <input class="form-control sticker_name" placeholder="" readonly  name="sticker_name" type="text" value="" aria-required="true"required>
                    </div>

                    <div class="form-group col-md-12 " class="patient-info-td">
                        <label for="mobile" >No. of Stickers Print ?</label>
                        <input class="form-control" placeholder=""   name="nostickers" type="text" value="" aria-required="true"required>
                    </div>

                    
                  
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success height-btn">Print</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<?php } ?>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script>
	 function openSticker(id,name){
    $('.sticker_regid').val(id);
    $('.sticker_name').val(name);
    $('#openSticker').modal('show');
}
$(function () {
	$("#example1").DataTable({
		processing: true,
     //   serverSide: true,
      //  ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/smstemplate_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		<?php if($show_actions): ?>
		columnDefs: [ { orderable: false, targets: [-1] }],
		<?php endif; ?>
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
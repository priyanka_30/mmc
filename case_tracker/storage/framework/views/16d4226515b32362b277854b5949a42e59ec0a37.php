<?php $__env->startSection("contentheader_title", "Book Appointment"); ?>
<?php $__env->startSection("contentheader_description", "Book Appointment listing"); ?>
<?php $__env->startSection("section", "Book Appointment"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Book Appointment Listing"); ?>

<?php $__env->startSection("headerElems"); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<style type="text/css">

.modal-body  .box-body .form-group:first-child {
display:none;
    
}


</style>
<div class="box box-success">
    <?php if(LAFormMaker::la_access("Appointments", "create")) { ?>

    <div class="col-sm-12">
<ul class="nav navbar-nav">
		<li><a href="javascript:void(0)" class="topmenu-icon-text" data-toggle="modal" data-target="#usermodal"><img src="<?php echo e(asset('la-assets/img/newuser.png')); ?>" class="topmenu-icon"><br>New</a></li>
		<?php /*?><li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/openfile.png') }}" class="topmenu-icon"><br>Open</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/import.png') }}" class="topmenu-icon"><br>Import</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/export.png') }}" class="topmenu-icon"><br>Export</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/find.png') }}" class="topmenu-icon"><br>Find</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/followup.png') }}" class="topmenu-icon"><br>Follow Up</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/print-icon.png') }}" class="topmenu-icon"><br>Print</a></li>
		 <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/delete.png') }}" class="topmenu-icon"><br>Delete Case</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/exit.png') }}" class="topmenu-icon"><br>Exit</a></li> <?php */ ?>

		
	</ul>
</div><?php } ?>
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
        
			<?php foreach( $listing_cols as $col ): ?>
			<?php if($col=='id'): ?>
			<th>Serial No.</th>
			<?php else: ?>
					<th><?php echo e(isset($module->fields[$col]['label']) ? $module->fields[$col]['label'] : ucfirst($col)); ?></th>
					<?php endif; ?>
			<?php endforeach; ?>
           
			<?php if($show_actions): ?>
			<th>Actions</th>
			<?php endif; ?>
           
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

<?php if(LAFormMaker::la_access("Appointments", "create")) { ?>
<div class="modal fade" id="usermodal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" style="width:900px">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Book an Appointment</h4>
			</div>
			<?php echo Form::open(['action' => 'LA\AppointmentsController@store', 'id' => 'booking-add-form']); ?>

			   <?php echo csrf_field(); ?>


			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" name="">
                    <div class="box-body">
	<input type="hidden" name="">
    	<div class="form-group" style="display: none;">
    		<label for="status">status :</label>
    		<input class="form-control" placeholder="Enter status" name="status" type="text" value="Pending">
    	</div>
    	<div class="form-group">
    		<label for="booking_date">Booking Date :</label>
    			<div class="input-group date">
    				<input class="form-control" placeholder="Enter Booking Date" name="booking_date" type="text" value="">
    				<span class="input-group-addon">
    					<span class="fa fa-calendar"></span>
    				</span>
    			</div>
    	</div>
    	<div class="form-group">
    		<label for="booking_time">Booking Time :</label>
    		<select class="form-control select2-hidden-accessible booking_time" data-placeholder="Enter Booking Time" rel="select2" name="booking_time" tabindex="-1" aria-hidden="true">
                <option value=""> Select</option>
    			<option value="10:00 AM">10:00 AM</option>
                <option value="10:30 AM">10:30 AM</option>

    			<option value="11:00 AM">11:00 AM</option>
                <option value="11:30 AM">11:30 AM</option>

    			<option value="12:00 PM">12:00 PM</option>
                <option value="12:30 PM">12:30 PM</option>

    			<option value="01:00 PM">01:00 PM</option>
                <option value="01:30 PM">01:30 PM</option>

    			<option value="02:00 PM">02:00 PM</option>
                <option value="02:30 PM">02:30 PM</option>

    			<option value="03:00 PM">03:00 PM</option>
                <option value="03:30 PM">03:30 PM</option>
    			<option value="04:00 PM">04:00 PM</option>
                <option value="04:30 PM">04:30 PM</option>
    			<option value="05:00 PM">05:00 PM</option>
                <option value="05:30 PM">05:30 PM</option>
    			<option value="06:00 PM">06:00 PM</option>
                <option value="06:30 PM">06:30 PM</option>
    			
    		</select>
            <span class="errorspan" style="display: none; color: red;">Slot Already Booked</span>
    	</div>
        <div class="form-group">
            <label for="patient_name">Patient Name :</label>
            <!-- <input class="form-control" placeholder="Enter Patient Name" name="patient_name" type="text" value="" required> -->
            <!-- <select class="form-control select2-hidden-accessible" data-placeholder="Enter Assistant Doctor" rel="select2" name="patient_name" tabindex="-1" aria-hidden="true">
                <?php foreach($casename as $casenamelist){?>
                    <option value="<?php echo $casenamelist->regid;?>"><?php echo $casenamelist->regid." - ". $casenamelist->first_name." ".$casenamelist->surname;?></option>
                <?php } ?>
            </select> -->
            <input type="text" list="cars" class="form-control" name="patient_name" onkeyup="getdoctor(this.value);" />
            <datalist id="cars">
              <?php foreach($casename as $casenamelist){?>
                    <option value="<?php echo $casenamelist->regid;?>"><?php echo $casenamelist->regid." - ". $casenamelist->first_name." ".$casenamelist->surname;?></option>
                <?php } ?>
            </datalist>
        </div>
    	<div class="form-group">
    		<label for="assistant_doctor">Assistant Doctor :</label>
    		<!-- <select class="form-control select2-hidden-accessible" data-placeholder="Enter Assistant Doctor" rel="select2" name="assistant_doctor" tabindex="-1" aria-hidden="true">
    			<?php foreach($doctors as $doctorslist){?>
				    <option value="<?php echo $doctorslist->id;?>"><?php echo $doctorslist->name;?></option>
				<?php } ?>
    		</select> -->

            <input type="text" list="doctors" class="form-control doctors" name="assistant_doctor"/>
            <input type="hidden" name="assistant_doctor" class="did">
            <datalist id="doctors">
              <?php foreach($doctors as $doctorslist){?>
                    <option value="<?php echo $doctorslist->id;?>"><?php echo $doctorslist->name;?></option>
                <?php } ?>
            </datalist>
            
    	</div>
    	
    	<div class="form-group">
    		<label for="address">Address :</label>
    		<textarea class="form-control" placeholder="Enter Address" cols="30" rows="3" name="address"></textarea>
    	</div>
    	<div class="form-group">
    		<label for="state">State :</label>
    		<input class="form-control" placeholder="Enter State" name="state" type="text" value="">
    	</div>
    	<div class="form-group">
    		<label for="city">City :</label>
    		<input class="form-control" placeholder="Enter City" name="city" type="text" value="">
    	</div>
    	<div class="form-group">
    		<label for="phone">Mobile :</label>
    		<input class="form-control" placeholder="Enter Mobile" name="phone" type="text" value="">
    	</div> 
    </div>
                </div>							
			</div>                  
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?php echo Form::submit( 'Book an Appointment', ['class'=>'btn btn-success appt']); ?>

				
			</div>
		</tr>
			<?php echo Form::close(); ?>

		</div>
	</div>
</div>

<?php } ?>

<div class="modal fade" id="received" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Status</h4>
            </div>
            <?php echo Form::open(['action' => 'LA\AppointmentsController@updatestatus', 'id' => 'daycharges-add-form']); ?>

            <div class="modal-body">
                <div class="box-body">
                  
               
                    <div class="col-md-12"  style="padding-left:0;padding-right:0; ">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                             <label for="mobile" >Status</label>
                             <input type="hidden" name="appid" class="appid">
                                <select class="form-control" name="status">
                                    <option value="">Select</option>
                                    <option value="Registered">Registered</option>
                                    <option value="Cancelled">Cancelled</option>
                                    <option value="Visited">Visited</option>
                                    <option value="Rescheduled">Rescheduled</option>
                                    
                                </select>
                        </div>


                    
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success height-btn savereceived">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datepicker/datepicker3.css')); ?>"/>

<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('la-assets/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>

<script>
    function openstatus(id)
    {
   
    
    $('#received').modal('show');
    $(".appid").val(id);
   
           
    }
$(function () {
    // $('').attr('name'='booking_date')
	$("#example1").DataTable({
		processing: true,
        //serverSide: true,
        order: [[ 0, "desc" ]],
        ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/appointment_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
	
	});
	$("#booking-add-form").validate({
		
	});
	
 
    
});

</script>
<script type="text/javascript">

    //setTimeout(function() { $( "booking-add-form" ).hide();},1000);
    $('#booking-add-form').find("input[name='status']").parent().hide();
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
   
    $('#booking-add-form').find("input[name='booking_date']").val(today);

	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
            startDate: new Date()

		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
 
</script>
<script>
	$('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});

function getdoctor(value){
  //  alert(value);
     $.ajax({
        type: "GET",
        url: "<?php echo e(url(config('laraadmin.adminRoute') . '/getdoctordetail')); ?>",
        data: { value:value}, 
        success: function( data ) {
            var packagearr = JSON.parse(data);
             var doctorname= packagearr['doctorname'];
             var doctorid= packagearr['doctorid'];
            
            $('.doctors').val(doctorname);
            $('.did').val(doctorid);
          

        }
    });
}
$('.booking_time').on('change', function() {
     
        var booktime = this.value;

        $.ajax({
            type: "GET",
            url: "<?php echo e(url(config('laraadmin.adminRoute') . '/getbooktime')); ?>",
            data: {  booktime:booktime}, 
            success: function( msg ) {
                    if(msg == "1"){
                        $('.errorspan').css('display','block');
                        $('.appt').prop('disabled', true);
                    }
                    else{
                        $('.errorspan').css('display','none');
                        $('.appt').prop('disabled', false);
                    }
                
                }
            })
    });
</script>
<?php $__env->stopPush(); ?>
<style>
    .error{ display:none;}
</style>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
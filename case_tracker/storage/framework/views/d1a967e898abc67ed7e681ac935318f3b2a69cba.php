<?php $__env->startSection("contentheader_title"); ?>
	<a href="<?php echo e(url(config('laraadmin.adminRoute') . '/stocks')); ?>">Stock</a> :
<?php $__env->stopSection(); ?>
<?php $__env->startSection("contentheader_description", $stock->$view_col); ?>
<?php $__env->startSection("section", "Stock"); ?>
<?php $__env->startSection("section_url", url(config('laraadmin.adminRoute') . '/stocks')); ?>
<?php $__env->startSection("sub_section", "Edit"); ?>

<?php $__env->startSection("htmlheader_title", "Stock Edit : ".$stock->$view_col); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<?php echo Form::model($stock, ['route' => [config('laraadmin.adminRoute') . '.stocks.update', $stock->id ], 'method'=>'PUT', 'id' => 'stock-edit-form']); ?>

					<?php echo LAFormMaker::form($module); ?>
					
					<?php /*
					<?php echo LAFormMaker::input($module, 'name'); ?>
					<?php echo LAFormMaker::input($module, 'email'); ?>
					<?php echo LAFormMaker::input($module, 'phone'); ?>
					<?php echo LAFormMaker::input($module, 'website'); ?>
					<?php echo LAFormMaker::input($module, 'assigned_to'); ?>
					<?php echo LAFormMaker::input($module, 'connect_since'); ?>
					<?php echo LAFormMaker::input($module, 'address'); ?>
					<?php echo LAFormMaker::input($module, 'city'); ?>
					<?php echo LAFormMaker::input($module, 'description'); ?>
					<?php echo LAFormMaker::input($module, 'profile_image'); ?>
					<?php echo LAFormMaker::input($module, 'profile'); ?>
					<?php echo LAFormMaker::display($module, 'quantity'); ?>
					<?php echo LAFormMaker::display($module, 'campany'); ?>
					<?php echo LAFormMaker::display($module, 'price'); ?>
					*/ ?>
                    <br>
					<div class="form-group">
						<?php echo Form::submit( 'Update', ['class'=>'btn btn-success']); ?> <button class="btn btn-default pull-right"><a href="<?php echo e(url(config('laraadmin.adminRoute') . '/stocks')); ?>">Cancel</a></button>
					</div>
				<?php echo Form::close(); ?>

			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$(function () {
	$("#stock-edit-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- Collect the nav links, forms, and other content for toggling -->
<style type="text/css">
	
</style>
<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
	<ul class="nav navbar-nav">
		
		<?php
		$menuItems = Dwij\Laraadmin\Models\Menu::where("parent", 0)->orderBy('hierarchy', 'asc')->get();
		?>
		<?php foreach($menuItems as $menu): ?>
			<?php if($menu->type == "module"): ?>
				<?php
				$temp_module_obj = Module::get($menu->name);
				?>
				<?php if(LAFormMaker::la_access($temp_module_obj->id)) { ?>
					<?php if(isset($module->id) && $module->name == $menu->name): ?>
						<?php echo LAHelper::print_menu_topnav($menu ,true); ?>
					<?php else: ?>
						<?php echo LAHelper::print_menu_topnav($menu); ?>
					<?php endif; ?>
				<?php } ?>
			<?php else: ?>
				<?php echo LAHelper::print_menu_topnav($menu); ?>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>
	 
</br>
	
	
	<?php if(LAConfigs::getByKey('sidebar_search')): ?>
	<form class="navbar-form navbar-left" role="search">
		<div class="form-group">
			<input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
		</div>
	</form>
	<?php endif; ?>
</div><!-- /.navbar-collapse -->
<style type="text/css">
	.nav > li > a{ padding: 8px 10px 8px 10px !important; }
</style>
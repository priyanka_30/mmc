<?php $__env->startSection('htmlheader_title'); ?>
	Case Remind View
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>
<div id="page-content" class="profile2">
	<div class="bg-primary clearfix">
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-3">
					<!--<img class="profile-image" src="<?php echo e(asset('la-assets/img/avatar5.png')); ?>" alt="">-->
					<div class="profile-icon text-primary"><i class="fa <?php echo e($module->fa_icon); ?>"></i></div>
				</div>
				<div class="col-md-9">
					<h4 class="name">Package ID : <?php echo e($casereminder->$view_col); ?></h4>
					<br>
					<br>
					<br>
				</div>
			</div>
		</div>
		
		
		
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="<?php echo e(url(config('laraadmin.adminRoute') . '/casereminder')); ?>" data-toggle="tooltip" data-placement="right" title="Back to Case Reminder"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						
						<?php echo LAFormMaker::display($module, 'package_id'); ?>
						<?php echo LAFormMaker::display($module, 'total_no_package'); ?>

						<div class="col-md-6">
					   		<label for="name" class="col-md-3"></label>
					   		<button class="btn btn-success "onclick="window.location.href='/managemyclinic/admin/couriers/<?php echo $casereminder->id;?>/edit'">Update</button>
					   	</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('la.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
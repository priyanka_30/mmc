<?php $__env->startSection("contentheader_title"); ?>
	<a href="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases')); ?>">Medicalcase</a> :
<?php $__env->stopSection(); ?>
<?php $__env->startSection("contentheader_description", $casereminder->$view_col); ?>
<?php $__env->startSection("section", "Medicalcase"); ?>
<?php $__env->startSection("section_url", url(config('laraadmin.adminRoute') . '/casereminder')); ?>
<?php $__env->startSection("sub_section", "Edit"); ?>

<?php $__env->startSection("htmlheader_title", "Medicalcase Edit : ".$casereminder->$view_col); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="POST" action="http://smartops.co.in/managemyclinic/admin/casereminder/<?php echo e($casereminder->id); ?>" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					<input name="_method" type="hidden" value="PUT">
					 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
							<div class="form-group col-md-12 ">
								<label for="mobile">End Date :</label>
								<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
								    <input class="form-control dob" value="<?php echo date("d/m/yy", strtotime($casereminder->end_date)); ?>" type="text" name="end_date" />
								    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div>
							<div class="form-group col-md-12">
								<label for="mobile" >Time</label>
								<div class="input-group time" id="timepicker">
					            	<input class="form-control" placeholder="HH:MM AM/PM" name="remind_time" value="<?php echo e($casereminder->remind_time); ?>" />
					            	<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
					          </div>
							</div>  
																	
					                    <br>
					<div class="form-group">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/casereminder">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datepicker/datepicker3.css')); ?>"/>

<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('la-assets/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>

<script type="text/javascript">
    //setTimeout(function() { $( "booking-add-form" ).hide();},1000);
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
   
    $('#caseremind-add-form').find("input[name='start_date']").val(today);
     $('#caseremind-add-form').find("input[name='end_date']").val(today);

	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
 
</script>


<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection("contentheader_title", "Payment"); ?>
<?php $__env->startSection("contentheader_description", "Payment listing"); ?>
<?php $__env->startSection("section", "Payment"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Payment Listing"); ?>

<?php $__env->startSection("headerElems"); ?>
<?php if(LAFormMaker::la_access("Payments", "create")) { ?>
	<!-- <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Payment</button> -->
<?php } ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		   <!-- Custom Filter -->
		   <div class="form-horizontal">
          <div class="control-group form-inline">
            <label for="name" class="col-md-2" style="width: 8%;margin-top: 2px;"> Min Date:</label>
            <div class="controls col-md-3" style="padding-left: 0px;">
              <input name="min" id="min" type="text" class="form-control input-sm" style="width: 100%;">
            </div>
          </div>
        </div>

        <div class="form-horizontal">
          <div class="control-group form-inline">
            <label for="name" class="col-md-2" style="width: 8%;margin-top: 2px;"> Max Date:</label>
            <div class="controls col-md-3" style="padding-left: 0px;">
              <input name="max" id="max" type="text" class="form-control input-sm" style="width: 100%;">
            </div>
          </div>
        </div>
 
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<?php foreach( $listing_cols as $col ): ?>
			<th><?php echo e(isset($module->fields[$col]['label']) ? $module->fields[$col]['label'] : ucfirst($col)); ?></th>
			<?php endforeach; ?>
			<?php if($show_actions): ?>
			<th>Actions</th>
			<?php endif; ?>
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

<?php if(LAFormMaker::la_access("Payments", "create")) { ?>
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Payment</h4>
			</div>
			<?php echo Form::open(['action' => 'LA\PaymentsController@store', 'id' => 'payment-add-form']); ?>

			<div class="modal-body">
				<div class="box-body">
                    <?php echo LAFormMaker::form($module); ?>
					
					<?php /*
					<?php echo LAFormMaker::input($module, 'name'); ?>
					<?php echo LAFormMaker::input($module, 'email'); ?>
					<?php echo LAFormMaker::input($module, 'phone'); ?>
					<?php echo LAFormMaker::input($module, 'website'); ?>
					<?php echo LAFormMaker::input($module, 'assigned_to'); ?>
					<?php echo LAFormMaker::input($module, 'connect_since'); ?>
					<?php echo LAFormMaker::input($module, 'address'); ?>
					<?php echo LAFormMaker::input($module, 'city'); ?>
					<?php echo LAFormMaker::input($module, 'description'); ?>
					<?php echo LAFormMaker::input($module, 'profile_image'); ?>
					<?php echo LAFormMaker::input($module, 'profile'); ?>
					*/ ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?php echo Form::submit( 'Submit', ['class'=>'btn btn-success']); ?>

			</div>
			<?php echo Form::close(); ?>

		</div>
	</div>
</div>
<?php } ?>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datepicker/datepicker3.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('la-assets/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script>
$(function () {

	$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = $('#min').datepicker("getDate");
            var max = $('#max').datepicker("getDate");
            var startDate = new Date(data[4]);
            if (min == null && max == null) { return true; }
            if (min == null && startDate <= max) { return true;}
            if(max == null && startDate >= min) {return true;}
            if (startDate <= max && startDate >= min) { return true; }
            return false;
        }
        );

       
            $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });

var table = $("#example1").DataTable({
		processing: true,
        serverSide: true,
       ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/payment_dt_ajax')); ?>",
       
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		dom: 'Bfrtip',
		buttons: [
            {
             extend: 'excel',
             text: 'Excel',
             className: 'btn btn-default',
             exportOptions: {
                columns: 'th:not(:last-child)'
             }
          },
           {
             extend: 'pdfHtml5',
             text: 'PDF',
             className: 'btn btn-default',
             exportOptions: {
                columns: 'th:not(:last-child)'
             }
          }
        ],
	});
 $('#min, #max').change(function () {
                table.draw();
            });
	$("#payment-add-form").validate({
		
	});
});

  
</script>
<script type="text/javascript">
	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
  </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection("contentheader_title"); ?>
	<a href="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases')); ?>">Couriermedicine</a> :
<?php $__env->stopSection(); ?>
<?php $__env->startSection("contentheader_description", $couriermedicine->$view_col); ?>
<?php $__env->startSection("section", "Medicalcase"); ?>
<?php $__env->startSection("section_url", url(config('laraadmin.adminRoute') . '/couriermedicine')); ?>
<?php $__env->startSection("sub_section", "Edit"); ?>

<?php $__env->startSection("htmlheader_title", "Couriermedicine Edit : ".$couriermedicine->$view_col); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="post" action="<?php echo e(url(config('laraadmin.adminRoute') . '/couriermedicine/medicineupdate/')); ?>" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					
					  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
								    <input type="hidden" name="id" value="<?php echo $couriermedicine->id; ?>">
                                    <?php if($couriermedicine->post_type=="Courier"){ ?>
                                        <div class="form-group col-md-12" >
                                            <label for="name">POD :</label>
                                            <input type="text" class="form-control" required="1" name="pcd" value="<?php echo $couriermedicine->pcd; ?>" >           
                                            
                                        </div>
                                        <div class="form-group col-md-12" >
                                            <label for="name">Courier :</label>
                                            <input type="text" class="form-control" required="1" name="courier" value="<?php echo $couriermedicine->courier; ?>">            
                                        </div>
                                    <?php } else { ?>
                                        <div class="form-group col-md-12" style="margin-top: 14px;" >
                                            <label class="container-checkbox">Pickup By <?php echo $user->first_name." ".$user->surname; ?>
                                              <input type="checkbox" class="courier_outstation" name="pickup" value="1" required <?php if($couriermedicine->pickup=="1"){ echo "checked" ;} ?> >
                                              <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    <?php } ?>
					<div class="form-group">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
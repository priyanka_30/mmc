<?php $__env->startSection("contentheader_title", "Casemonthwise"); ?>
<?php $__env->startSection("contentheader_description", "Casemonthwise listing"); ?>
<?php $__env->startSection("section", "Casemonthwise"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Casemonthwise Listing"); ?>

<?php $__env->startSection("headerElems"); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>Month</th>
			<th>Year</th>
			<th>New Case</th>
			
		</tr>
		</thead>
		<tbody>
			<?php 
			$total =0;
			foreach ($case as $val){
				// foreach($pot as $casepot){
			 ?>
			<tr>
				<td><?php echo date("F", mktime(0, 0, 0, $val->month, 1)); ?>  </td>
				<td><?php echo $val->year; ?></td>
				<td><?php echo $val->id; ?> </td>
				

			</tr>
			<?php }  ?>
			
		</tbody>
		</table>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datepicker/datepicker3.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('la-assets/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
<script>
$(function () {

	

var table = $("#example1").DataTable({
		processing: true,
       // serverSide: true,
       //ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/payment_dt_ajax')); ?>",
       
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		dom: 'Bfrtip',
		buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
	});
 
	$("#payment-add-form").validate({
		
	});
});

  
</script>
<script type="text/javascript">
	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});


	
  </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
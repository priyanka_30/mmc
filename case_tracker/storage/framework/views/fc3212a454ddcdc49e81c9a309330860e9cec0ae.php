<?php $__env->startSection("contentheader_title"); ?>
	<a href="<?php echo e(url(config('laraadmin.adminRoute') . '/daycharges')); ?>">SMS Templates</a> :
<?php $__env->stopSection(); ?>
<?php $__env->startSection("contentheader_description", $daycharge->$view_col); ?>
<?php $__env->startSection("section", "SMS Template"); ?>
<?php $__env->startSection("section_url", url(config('laraadmin.adminRoute') . '/smstemplate')); ?>
<?php $__env->startSection("sub_section", "Edit"); ?>

<?php $__env->startSection("htmlheader_title", "SMS Template Edit : ".$daycharge->$view_col); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<?php echo Form::model($daycharge, ['route' => [config('laraadmin.adminRoute') . '.smstemplate.update', $daycharge->id ], 'method'=>'PUT', 'id' => 'daycharge-edit-form']); ?>

					<!-- <?php echo LAFormMaker::form($module); ?> -->

					<div class="box-body">
                    <div class="form-group">
                    	<label for="templatename">Template Name :</label>
                    	<input class="form-control" placeholder="Enter Template Name" name="templatename" type="text" value="<?php echo $daycharge->templatename; ?>">
                    </div>
                    <div class="form-group">
                    	<label for="message">Message :</label>
                    	<textarea class="form-control" placeholder="Enter Message" name="message" type="text" rows="5"><?php echo $daycharge->message; ?></textarea>
                    </div>
                    <div class="form-group"><label for="smstype">SMS Type :</label>
                    <!-- 	<input class="form-control" placeholder="Enter SMS Type" name="smstype" type="text" value=""> -->
                    <label class="container-radio">Normal
                              <input type="radio" name="smstype" class="send_sms" value="Normal" <?php if($daycharge->smstype == "Normal"){ echo "checked"; } ?> >
                              <span class="checkmark-radio"></span>
                            </label>
                            <label class="container-radio">Birthday
                              <input type="radio" name="smstype" class="send_sms" value="Birthday" <?php if($daycharge->smstype == "Birthday"){ echo "checked"; } ?>>
                              <span class="checkmark-radio"></span>
                            </label>
                             <label class="container-radio">Courier
                              <input type="radio" name="smstype" class="send_sms" value="Courier" <?php if($daycharge->smstype == "Courier"){ echo "checked"; } ?>>
                              <span class="checkmark-radio"></span>
                            </label>
                             </div>					
									</div>
					
					<?php /*
					<?php echo LAFormMaker::input($module, 'name'); ?>
					<?php echo LAFormMaker::input($module, 'tags'); ?>
					<?php echo LAFormMaker::input($module, 'color'); ?>
					*/ ?>
                    <br>
					<div class="form-group">
						<?php echo Form::submit( 'Update', ['class'=>'btn btn-success']); ?> <button class="btn btn-default pull-right"><a href="<?php echo e(url(config('laraadmin.adminRoute') . '/daycharges')); ?>">Cancel</a></button>
					</div>
				<?php echo Form::close(); ?>

			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$(function () {
	$("#daycharge-edit-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
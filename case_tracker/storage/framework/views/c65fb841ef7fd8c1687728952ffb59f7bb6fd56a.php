<?php $__env->startSection("contentheader_title"); ?>
	<a href="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases')); ?>">Medicalcase</a> :
<?php $__env->stopSection(); ?>
<?php $__env->startSection("contentheader_description", $potencydetail->$view_col); ?>
<?php $__env->startSection("section", "Medicalcase"); ?>
<?php $__env->startSection("section_url", url(config('laraadmin.adminRoute') . '/medicalcases')); ?>
<?php $__env->startSection("sub_section", "Edit"); ?>

<?php $__env->startSection("htmlheader_title", "Medicalcase Edit : ".$potencydetail->$view_col); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="post" action="<?php echo e(url(config('laraadmin.adminRoute') . '/medicalcases/potencyupdate/')); ?>" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
					  <input type="hidden" name="id" value="<?php echo e($potencydetail->id); ?>">
													
							<div class="form-group col-md-6">
							<label for="Emailid">Remedy :</label>
							<input class="form-control"  name="rxremedy" type="text" value="<?php echo e($potencydetail->rxremedy); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="phone">Potency :</label>
							<input class="form-control"  name="rxpotency" type="text" value="<?php echo e($potencydetail->rxpotency); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="address">Frequency :</label>
							<input class="form-control" name="rxfrequency" type="text" value="<?php echo e($potencydetail->rxfrequency); ?>" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="city">Days* :</label>
							<input class="form-control"required="1" name="rxdays" type="text" value="<?php echo e($potencydetail->rxdays); ?>" aria-required="true"></div>
							<div class="form-group col-md-12">
							<label for="pin">Prescription:</label>
							<input class="form-control"required="1" name="rxprescription" type="text" value="<?php echo e($potencydetail->rxprescription); ?>" aria-required="true"></div>
																	
					                    <br>
					<div class="form-group col-md-12">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('htmlheader_title'); ?>
	Doctor View
<?php $__env->stopSection(); ?>


<?php $__env->startSection('main-content'); ?>

<div id="page-content" class="profile2">
	<div class="bg-success clearfix">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">
					<img class="profile-image" src="<?php echo e(asset('la-assets/img/'.$doctor->profilepic)); ?>"style="height: 100px; width: 100px;">
				</div>
				<div class="col-md-9">
					<h4 class="name"><?php echo e($doctor->$view_col); ?></h4>
					<div class="row stats">
						<div class="col-md-6 stat"><div class="label2" data-toggle="tooltip" data-placement="top" title="Designation"><?php echo e($doctor->designation); ?></div></div>
						
					</div>
					<p class="desc"><?php echo e(substr($doctor->about, 0, 33)); ?><?php if(strlen($doctor->about) > 33): ?>...<?php endif; ?></p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="dats1"><i class="fa fa-envelope-o"></i> <?php echo e($doctor->email); ?></div>
			<div class="dats1"><i class="fa fa-phone"></i> <?php echo e($doctor->mobile); ?></div>
		</div>
		
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<?php if($doctor->id != Auth::user()->context_id): ?>
		<li class=""><a href="<?php echo e(url(config('laraadmin.adminRoute') . '/doctors')); ?>" data-toggle="tooltip" data-placement="right" title="Back to Doctors"><i class="fa fa-chevron-left"></i></a></li>
		<?php endif; ?>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		
		<?php if($doctor->id == Auth::user()->context_id || Entrust::hasRole("SUPER_ADMIN")): ?>
			<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settings" data-target="#tab-account-settings"><i class="fa fa-key"></i> Account settings</a></li>
		<?php endif; ?>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-view-appointments" data-target="#tab-view-appointments"><i class="fa fa-key"></i> View Appointments</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						<?php echo LAFormMaker::display($module, 'title'); ?>
						<?php echo LAFormMaker::display($module, 'name'); ?>
						<?php echo LAFormMaker::display($module, 'middlename'); ?>
						<?php echo LAFormMaker::display($module, 'surname'); ?>
						<?php echo LAFormMaker::display($module, 'date_birth'); ?>
						<?php echo LAFormMaker::display($module, 'gender'); ?>
						<?php echo LAFormMaker::display($module, 'mobile'); ?>		
						<?php echo LAFormMaker::display($module, 'email'); ?>
						<?php echo LAFormMaker::display($module, 'city'); ?>
						<?php echo LAFormMaker::display($module, 'address'); ?>
						<?php echo LAFormMaker::display($module, 'permanentaddress'); ?>
						<?php echo LAFormMaker::display($module, 'qualification'); ?>
						<?php echo LAFormMaker::display($module, 'joiningdate'); ?>
						<?php echo LAFormMaker::display($module, 'registrationId'); ?>
						<?php echo LAFormMaker::display($module, 'aadharnumber'); ?>
						<?php echo LAFormMaker::display($module, 'pannumber'); ?>
						<?php echo LAFormMaker::display($module, 'profilepic'); ?>
						
						
					</div>
				</div>
			</div>
		</div>
		
		
		<?php if($doctor->id == Auth::user()->context_id || Entrust::hasRole("SUPER_ADMIN")): ?>
		<div role="tabpanel" class="tab-pane fade" id="tab-account-settings">
			<div class="tab-content">
				<form action="<?php echo e(url(config('laraadmin.adminRoute') . '/doctors/change_password/'.$doctor->id)); ?>" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8">
					<?php echo e(csrf_field()); ?>

					<div class="panel">
						<div class="panel-default panel-heading">
							<h4>Account settings</h4>
						</div>
						<div class="panel-body">
							<?php if(count($errors) > 0): ?>
								<div class="alert alert-danger">
									<ul>
										<?php foreach($errors->all() as $error): ?>
											<li><?php echo e($error); ?></li>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php endif; ?>
							<?php if(Session::has('success_message')): ?>
								<p class="alert <?php echo e(Session::get('alert-class', 'alert-success')); ?>"><?php echo e(Session::get('success_message')); ?></p>
							<?php endif; ?>
							<div class="form-group">
								<label for="password" class=" col-md-2">Password</label>
								<div class=" col-md-10">
									<input type="password" name="password" value="" id="password" class="form-control" placeholder="Password" autocomplete="off" required="required" data-rule-minlength="6" data-msg-minlength="Please enter at least 6 characters.">
								</div>
							</div>
							<div class="form-group">
								<label for="password_confirmation" class=" col-md-2">Retype password</label>
								<div class=" col-md-10">
									<input type="password" name="password_confirmation" value="" id="password_confirmation" class="form-control" placeholder="Retype password" autocomplete="off" required="required" data-rule-equalto="#password" data-msg-equalto="Please enter the same value again.">
								</div>
							</div>
						</div>
						<div class="panel-footer">
							<button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Change Password</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<?php endif; ?>
		<div role="tabpanel" class="tab-pane active fade in" id="tab-view-appointments">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>View Appointments</h4>
					</div>
					<div class="panel-body">
						<table id="example1" class="table table-bordered">
						<thead>
						<tr class="success">
							<th>Patient Name</th>
							<th>Booking Date</th>
							<th>Booking Time</th>
							<th>Phone</th>
							<th>Action</th>
						</tr>
						
						<?php foreach($appointments as $appoint): ?>
						<tr>
							<td><?php echo e($appoint->patient_name); ?></td>
							<td><?php echo e($appoint->booking_date); ?></td>
							<td><?php echo e($appoint->booking_time); ?></td>
							<td><?php echo e($appoint->phone); ?></td>
							<td>
								<?php if($appoint->status == "Accepted"): ?>
								Accepted
								
								<?php if($appoint->patient_id ): ?>
								<form action="<?php echo e(url(config('laraadmin.adminRoute') . '/doctors/newcase/')); ?>" id="password-reset-form" class="general-form dashed-row white" method="post" style="display: inline-block; ">
									<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
									<input type="hidden" name="appointment_id" value="<?php echo e($appoint->id); ?>">
									<input type="hidden" name="patient_id" value="<?php echo e($appoint->patient_id); ?>">
									<input type="hidden" name="type" value="followup">
									<button type="submit" class="btn btn-success" style="margin-left: 19px;">Followup</button>
								</form>
								
								<?php else: ?>
								<form action="<?php echo e(url(config('laraadmin.adminRoute') . '/doctors/newcase/')); ?>" id="password-reset-form" class="general-form dashed-row white" method="post" style="display: inline-block; ">
									<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
									<input type="hidden" name="appointment_id" value="<?php echo e($appoint->id); ?>">
									<input type="hidden" name="patient_id" value="<?php echo e($appoint->patient_id); ?>">
									<input type="hidden" name="type" value="newcase">
									<button type="submit" class="btn btn-success" style="margin-left: 19px;">New Case</button>
								</form>
								<?php endif; ?>
								
								<?php else: ?>
								<form action="<?php echo e(url(config('laraadmin.adminRoute') . '/doctors/update_status/'.$doctor->id)); ?>" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8" style="display: inline-block; ">
									<input type="hidden" name="patient_id" value="<?php echo e($appoint->id); ?>">
									<input type="hidden" name="status" value="Accepted">
									<button type="submit" class="btn btn-info">Accept</button>
								</form>	
								<form action="<?php echo e(url(config('laraadmin.adminRoute') . '/doctors/delete_status/'.$doctor->id)); ?>" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8" style="display: inline-block; ">
									<input type="hidden" name="patient_id" value="<?php echo e($appoint->id); ?>">
									<button type="submit" class="btn btn-danger">Decline</button>
								</form>
							<?php endif; ?>
							
							</td>
						</tr>
						<?php endforeach; ?>
						</thead>
						<tbody>
							
						</tbody>
						</table>
						
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script>
$(function () {
	<?php if($doctor->id == Auth::user()->id || Entrust::hasRole("SUPER_ADMIN")): ?>
	$('#password-reset-form').validate({
		
	});
	<?php endif; ?>
});
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('la.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
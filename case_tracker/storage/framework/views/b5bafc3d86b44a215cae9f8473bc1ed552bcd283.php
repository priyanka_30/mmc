<?php $__env->startSection("contentheader_title", "Record"); ?>
<?php $__env->startSection("contentheader_description", "Record listing"); ?>
<?php $__env->startSection("section", "Record"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Record Listing"); ?>

<?php $__env->startSection("headerElems"); ?>
<?php if(LAFormMaker::la_access("Record", "create")) { ?>
<?php if(Auth::user()->type != "Doctor") { ?>
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Record</button>
<?php } ?>
	<a class="btn btn-success btn-sm pull-right"  href="<?php echo url(config('laraadmin.adminRoute') . '/allrecord/')?>" style="margin-right: 10px;">Show All Records</a>
<?php } ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		 <thead>
        <tr class="success">
        	
            <?php foreach( $listing_cols as $col ): ?>

                    <th><?php echo e(isset($module->fields[$col]['label']) ? $module->fields[$col]['label'] : ucfirst($col)); ?></th>
            <?php endforeach; ?>
          
        </tr>
        </thead>
        <tbody>
            
        </tbody>
		</table>
	</div>
</div>

<?php if(LAFormMaker::la_access("Expenses", "create")) { ?>
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add</h4>
			</div>
			<?php echo Form::open(['action' => 'LA\RecordController@newrecord', 'id' => 'daycharges-add-form']); ?>

			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" name="">
                    <div class="form-group col-md-12 ">
						<label for="name">ID:</label>
						<input class="form-control regid"  name="regid" type="text" value="" aria-required="true"  onkeyup="searchrecord();">
						<input type="hidden" name="recorddate" value="<?php echo date('Y-m-d'); ?>">
					</div>
					
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Transfer"  >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Transfer</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Courier" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Courier</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Enquiry" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Enquiry</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Assistant"  >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Assistant</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Appointment" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Appointment</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Callback" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Callback</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Other"  >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Other</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Pickup" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Pickup</label>
					</div>
					<div class="form-group col-md-4" >
						<input name="recordtype" type="radio" value="Closed" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Closed</label>
					</div>
					<div class="form-group col-md-12">
						<label for="mobile">Mobile :</label>
						<input class="form-control mobile"  name="mobile" type="text" value="" aria-required="true" >
					</div>
					<div class="form-group col-md-12">
						<label for="mobile">Mobile 1 :</label>
						<input class="form-control mobile1"  name="mobile1" type="text" value="" aria-required="true" >
					</div>
					<div class="form-group col-md-12">
						<label for="mobile">Name :</label>
						<input class="form-control name"  name="name" type="text" value="" aria-required="true" >
					</div>
					<div class="form-group col-md-12">
						<label for="mobile" >Instructions :</label>
						<textarea class="form-control comment"   name="comment" type="text" value="" aria-required="true" rows="5" ></textarea>
					</div>   
				
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<?php echo Form::submit( 'Submit', ['class'=>'btn btn-success']); ?>

			</div>
			<?php echo Form::close(); ?>

		</div>
	</div>
</div>
<?php } ?>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script>
$(document).ready(function(){
    $("#AddModal").modal('show');
});
	function searchrecord(){
		var regid = $(".regid").val();
		$.ajax({
            type: "GET",
            url: "<?php echo e(url(config('laraadmin.adminRoute') . '/searchrecord')); ?>",
            data: { regid:regid}, 
            success: function( msg ) {
            	var splitted = msg.split("/");
                $('.name').val(splitted[0]);
                $('.mobile').val(splitted[1]);
                $('.mobile1').val(splitted[2]);
                
        }
       });
	}
$(function () {
	$("#example1").DataTable({
		processing: true,
      	//serverSide: true,
    	 order: [[ 5, "desc" ]],
        ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/record_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		}
	
	});

});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
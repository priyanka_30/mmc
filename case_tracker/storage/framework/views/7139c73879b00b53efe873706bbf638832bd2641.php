<?php $__env->startSection('htmlheader_title'); ?> Apitest <?php $__env->stopSection(); ?>
<?php $__env->startSection('contentheader_title'); ?> Apitest <?php $__env->stopSection(); ?>
<?php $__env->startSection('contentheader_description'); ?> Apitest <?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>

    

        <section class="content">
          
          <!-- Main row -->
          <div class="row">
                
                
              </div>
              <div class="row" style="margin-top: 12px;">
                <div class="col-md-2" style="width: 14%;">
                 <?php echo Form::open(['action' => 'LA\ApitestController@createdetails', 'style' => 'display:inline-block']); ?>


                    <button class="btn btn-success" name="submit"  type="submit">Submit</button>
                  <?php echo Form::close(); ?>

                </div>

                
                
           
          </div>
        </section><!-- /.content -->
 
<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<!-- Morris chart -->
<link rel="stylesheet" href="<?php echo e(asset('la-assets/plugins/morris/morris.css')); ?>">
<!-- jvectormap -->
<link rel="stylesheet" href="<?php echo e(asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')); ?>">
<!-- Date Picker -->
<link rel="stylesheet" href="<?php echo e(asset('la-assets/plugins/datepicker/datepicker3.css')); ?>">
<!-- Daterange picker -->
<link rel="stylesheet" href="<?php echo e(asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css')); ?>">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="<?php echo e(asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>">
<?php $__env->stopPush(); ?>


<?php $__env->startPush('scripts'); ?>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo e(asset('la-assets/plugins/morris/morris.min.js')); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo e(asset('la-assets/plugins/sparkline/jquery.sparkline.min.js')); ?>"></script>
<!-- jvectormap -->
<script src="<?php echo e(asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
<script src="<?php echo e(asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo e(asset('la-assets/plugins/knob/jquery.knob.js')); ?>"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo e(asset('la-assets/plugins/daterangepicker/daterangepicker.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(asset('la-assets/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo e(asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(asset('la-assets/plugins/fastclick/fastclick.js')); ?>"></script>
<!-- dashboard -->
<script src="<?php echo e(asset('la-assets/js/pages/dashboard.js')); ?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js" integrity="sha512-aUhL2xOCrpLEuGD5f6tgHbLYEXRpYZ8G5yD+WlFrXrPy2IrWBlu6bih5C9H6qGsgqnU6mgx6KtU8TreHpASprw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js" integrity="sha512-3j3VU6WC5rPQB4Ld1jnLV7Kd5xr+cq9avvhwqzbH/taCRNURoeEpoPBK9pDyeukwSxwRPJ8fDgvYXd6SkaZ2TA==" crossorigin="anonymous"></script>
<?php $__env->stopPush(); ?>


<?php echo $__env->make('la.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection("contentheader_title", "Upcomingappointments"); ?>
<?php $__env->startSection("contentheader_description", "Upcomingappointments listing"); ?>
<?php $__env->startSection("section", "Upcomingappointments"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Upcomingappointments Listing"); ?>

<?php $__env->startSection("headerElems"); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
<style type="text/css">
	td:nth-child(6) {
  display: none
}
th:nth-child(6) {
  display: none
}
</style>
<div class="box box-success">
   
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<?php foreach( $listing_cols as $col ): ?>
					<th><?php echo e(isset($module->fields[$col]['label']) ? $module->fields[$col]['label'] : ucfirst($col)); ?></th>
			<?php endforeach; ?>
			<?php if($show_actions): ?>
			<th>Actions</th>
			<?php endif; ?>
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datepicker/datepicker3.css')); ?>"/>

<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script src="<?php echo e(asset('la-assets/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>

<script>
$(function () {

	
    // $('').attr('name'='booking_date')
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/upcomingappointments_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		<?php if($show_actions): ?>
		columnDefs: [ { orderable: false, targets: [-1] }],
		<?php endif; ?>
	});
	$("#booking-add-form").validate({
		
	});
	
 
    
});

</script>
<script type="text/javascript">
    //setTimeout(function() { $( "booking-add-form" ).hide();},1000);
    $('#booking-add-form').find("input[name='status']").parent().hide();
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
   
    $('#booking-add-form').find("input[name='booking_date']").val(today);

	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
 
</script>
<script>
	$('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});
</script>
<?php $__env->stopPush(); ?>
<style>
    .error{ display:none;}
</style>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
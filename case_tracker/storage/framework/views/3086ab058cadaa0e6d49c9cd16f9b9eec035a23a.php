<?php $__env->startSection("contentheader_title", "Refrencedetails"); ?>
<?php $__env->startSection("contentheader_description", "Refrencedetails listing"); ?>
<?php $__env->startSection("section", "Refrencedetails"); ?>
<?php $__env->startSection("sub_section", "Listing"); ?>
<?php $__env->startSection("htmlheader_title", "Refrencedetails Listing"); ?>

<?php $__env->startSection("headerElems"); ?>
<?php if(LAFormMaker::la_access("Refrencedetails", "create")) { ?>
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Refrencedetails</button>
<?php } ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection("main-content"); ?>

<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>Refrence Type</th>
			<th>Count</th>
		</tr>
		</thead>
		<tbody>
			
			<tr>
				<td>Patient</td>
				<td><?php echo $count1; ?></td>
			</tr>
			<tr>
				<td>News Paper</td>
				<td><?php echo $count2; ?></td>
			</tr>
			<tr>
				<td>Walk-in</td>
				<td><?php echo $count3; ?></td>
			</tr>
			<tr>
				<td>other</td>
				<td><?php echo $count4; ?></td>
			</tr>
			<tr>
				<td>Just Dial</td>
				<td><?php echo $count5; ?></td>
			</tr>		

			<tr>
				<td>Web online</td>
				<td><?php echo $count6; ?></td>
			</tr>
			<tr>
				<td>panchkula shopp</td>
				<td><?php echo $count7; ?></td>
			</tr>
			<tr>
				<td>Brosher</td>
				<td><?php echo $count8; ?></td>
			</tr>
			<tr>
				<td>Doctor</td>
				<td><?php echo $count9; ?></td>
			</tr>
			<tr>
				<td>Facebook</td>
				<td><?php echo $count10; ?></td>
			</tr>

			<tr>
				<td>Practo</td>
				<td><?php echo $count11; ?></td>
			</tr>
			<tr>
				<td>Lybrate</td>
				<td><?php echo $count12; ?></td>
			</tr>
			<tr>
				<td>Staff</td>
				<td><?php echo $count13; ?></td>
			</tr>
			
		</tbody>
		</table>
	</div>
</div>




<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.css')); ?>"/>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="<?php echo e(asset('la-assets/plugins/datatables/datatables.min.js')); ?>"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
       // serverSide: true,
      //  ajax: "<?php echo e(url(config('laraadmin.adminRoute') . '/refrencedetails_dt_ajax')); ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make("la.layouts.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
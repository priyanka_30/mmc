@extends("la.layouts.app")

@section("contentheader_title", "Packageexpiry")
@section("contentheader_description", "Packageexpiry listing")
@section("section", "Packageexpiry")
@section("sub_section", "Listing")
@section("htmlheader_title", "Packageexpiry Listing")

@section("headerElems")

@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>End Date</th>
			<th>RegID</th>
			<th>Name</th>
			<th>MiddleName</th>
			<th>Surname</th>
			<th>Mobile</th>
		</tr>
		</thead>
		<tbody>
			<?php 
			$total =0;
			foreach ($case as $val){
			?>
			<tr>
				<td><?php echo date('d/m/Y',strtotime($val->date_left)); ?></td>
				<td><?php echo $val->regid; ?> </td>
				<td><?php echo $val->first_name; ?></td>
				<td><?php echo $val->middle_name; ?></td>
				<td><?php echo $val->surname; ?></td>
				<td><?php echo $val->mobile2; ?></td>

			</tr>
			<?php } ?>
			
		</tbody>
		</table>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
$(function () {

	

var table = $("#example1").DataTable({
		processing: true,
       // serverSide: true,
       //ajax: "{{ url(config('laraadmin.adminRoute') . '/payment_dt_ajax') }}",
       
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		dom: 'Bfrtip',
		buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
	});
 
	$("#payment-add-form").validate({
		
	});
});

  
</script>
<script type="text/javascript">
	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});


	
  </script>
@endpush
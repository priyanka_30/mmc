@extends("la.layouts.app")

@section("contentheader_title", "Allrecord")
@section("contentheader_description", "Allrecord listing")
@section("section", "Allrecord")
@section("sub_section", "Listing")
@section("htmlheader_title", "Allrecord Listing")

@section("headerElems")
@la_access("Expenses", "create")
	<!-- <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Record</button> -->
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		 <thead>
        <tr class="success">
        	
            @foreach( $listing_cols as $col )

                    <th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
            @endforeach
          
        </tr>
        </thead>
        <tbody>
            
        </tbody>
		</table>
	</div>
</div>

@la_access("Expenses", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add</h4>
			</div>
			{!! Form::open(['action' => 'LA\RecordController@saverecord', 'id' => 'daycharges-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" name="">
                    <div class="form-group col-md-12 ">
						<label for="name">ID:</label>
						<input class="form-control regid"  name="regid" type="text" value="" aria-required="true"  onkeyup="searchrecord();">
					</div>
					
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Transfer"  >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Transfer</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Courier" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Courier</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Enquiry" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Enquiry</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Assistant"  >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Assistant</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Appointment" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Appointment</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Callback" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Callback</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Other"  >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Other</label>
					</div>
					<div class="form-group col-md-3" style="margin-bottom: 8px;">
						<input name="recordtype" type="radio" value="Pickup" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Pickup</label>
					</div>
					<div class="form-group col-md-4" >
						<input name="recordtype" type="radio" value="Closed" >&nbsp;&nbsp;<label for="mobile" style="position: absolute;">Closed</label>
					</div>
					<div class="form-group col-md-12">
						<label for="mobile">Mobile :</label>
						<input class="form-control mobile"  name="mobile" type="text" value="" aria-required="true" >
					</div>
					<div class="form-group col-md-12">
						<label for="mobile">Mobile 1 :</label>
						<input class="form-control mobile1"  name="mobile1" type="text" value="" aria-required="true" >
					</div>
					<div class="form-group col-md-12">
						<label for="mobile">Name :</label>
						<input class="form-control name"  name="name" type="text" value="" aria-required="true" >
					</div>
					<div class="form-group col-md-12">
						<label for="mobile" >Instructions :</label>
						<textarea class="form-control comment"   name="comment" type="text" value="" aria-required="true" rows="5" ></textarea>
					</div>   
				
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
// $(document).ready(function(){
//     $("#AddModal").modal('show');
// });
	function searchrecord(){
		var regid = $(".regid").val();
		$.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/searchrecord') }}",
            data: { regid:regid}, 
            success: function( msg ) {
            	var splitted = msg.split("/");
                $('.name').val(splitted[0]);
                $('.mobile').val(splitted[1]);
                $('.mobile1').val(splitted[2]);
                
        }
       });
	}
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        order: [[ 5, "desc" ]],
        ajax: "{{ url(config('laraadmin.adminRoute') . '/allrecord_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
@endpush
@extends("la.layouts.app")

@section("contentheader_title", "Targetdetails")
@section("contentheader_description", "Targetdetails listing")
@section("section", "Targetdetails")
@section("sub_section", "Listing")
@section("htmlheader_title", "Targetdetails Listing")

@section("headerElems")
@la_access("Expenses", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Targetdetails</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<div class="form-horizontal" style="margin-bottom: 10px;">
          <div class="control-group form-inline">
          	{!! Form::open(['action' => 'LA\TargetdetailsController@search', 'id' => 'daycharges-add-form']) !!}
            <div class="form-group col-md-3">
				<label for="mobile" >Date</label>
				<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php echo $query; ?>" />
				    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
			</div>
			<div class="form-group col-md-4">
				
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
          </div>
        </div>
		<?php 
			$total = 0;
			$targetcases = 0;
			$targetcollection = 0;
			//echo $query1;
			$paydate = "";
			$q = "";
			if($query1 != "test"){
				$paydate = date('Y-m-d',strtotime($query1));
				$q = date('n/d/Y',strtotime($query1));
				$target = DB::table('daily_target')->where('dateval', '=', $q)->first();
				 $cases = DB::table('medicalcases')->where('dob', '=', $q)->get();
				$payments = DB::table('payments')->where('received_date', '=',$paydate)->get();
			} 
			else{
			$target = DB::table('daily_target')->where('dateval', '=', date('n/d/Y'))->first();
			 $cases = DB::table('medicalcases')->where('dob', '=', date('n/d/Y'))->get();
			$payments = DB::table('payments')->where('received_date', '=',$paydate)->get();
			}
			foreach ($payments as $pay){
				$total += $pay->received_price;
			}
			if(!empty($target)){ 
				$targetcases = $target->cases;
			}
			if(!empty($target)){ 
				$targetcollection = $target->collection;
			}
		 	$totalcases = count($cases);
			$defcases = $targetcases - $totalcases;
			$defamount = $targetcollection - $total;
			if($targetcases!=0){
			$perfcases = ($totalcases *100)/$targetcases;
			}
			else{
				$perfcases = 0;
			}
			if($targetcollection!=0){
			$perfamount = ($total*100)/$targetcollection;
		}
		else{
			$perfamount = 0;
		}
			$maxDays=date('t');
			$currentDayOfMonth=date('j');
			$daysleft = $maxDays - $currentDayOfMonth;

		?>
		<table class="table table-bordered" style="margin-top: 5rem;">
			<tr>
				<th>&nbsp;</th>
				<th>Cases</th>
				<th>Collection</th>
			</tr>
			<tr>
				<th>Target</th>
				<td><?php if(!empty($target)){ echo  $target->cases; } ?></td>
				<td><?php if(!empty($target)){ echo $target->collection; }?></td>
			</tr>
			<tr>
				<th>Achieve</th>
				<td><?php echo $totalcases; ?></td>
				<td><?php echo $total;?></td>
			</tr>

			<tr>
				<th>Defecit</th>
				<td><?php echo $defcases; ?></td>
				<td><?php echo $defamount;?></td>
			</tr>

			<tr>
				<th>Performance</th>
				<td><?php echo number_format($perfcases,2)."%"; ?></td>
				<td><?php echo number_format($perfamount,2)."%";?></td>
			</tr>
			<tr>
				<th colspan="3">Numbers of Days Remaining : <?php echo $daysleft; ?></th>
				
			</tr>
		</table>
		
           
	</div>
</div>

@la_access("Expenses", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Targetdetails</h4>
			</div>
			{!! Form::open(['action' => 'LA\TargetdetailsController@savetarget', 'id' => 'daycharges-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" name="">
                   
					<div class="form-group col-md-12 ">
                        <label for="mobile">Target Date :</label>
                        <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                            <input class="form-control target_date" type="text" name="target_date" readonly/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
					<div class="form-group col-md-12">
						<label for="mobile">Target Cases :</label>
						<input class="form-control" placeholder="Cases"  name="cases" type="text" value="" aria-required="true"required>
					</div>

					<div class="form-group col-md-12">
						<label for="mobile">Target Collection :</label>
						<input class="form-control" placeholder="Collection"  name="collection" type="text" value="" aria-required="true"required>
					</div>
					
				
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
      //  serverSide: true,
       // ajax: "{{ url(config('laraadmin.adminRoute') . '/targetdetails_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
<script type="text/javascript">
	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
</script>
@endpush
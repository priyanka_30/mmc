@extends('la.layouts.app')

@section('htmlheader_title') Dashboard @endsection
@section('contentheader_title') Dashboard @endsection
@section('contentheader_description') Organisation Overview @endsection

@section('main-content')

    
<!-- Main content -->
<style>
html, body {margin: 0; height: 100%; overflow: hidden}

  .chatclass{
    display: inherit;
    width: 660px;
  }
  .slimScrollDiv{
    height: 331px !important;
  }
  .wysihtml5-toolbar li:nth-child(6) {
  display: none !important;
}
.wysihtml5-toolbar li:nth-child(5) {
  display: none !important;
}
#chat-box{
  height: 320px !important;
}
.name{
    text-transform:capitalize;
}
.modal {
    background: rgba(0, 0, 0, 0.9);
}
</style>
        <section class="content">
          <!-- Small boxes (Stat box) -->
           <div class="row" style="margin-bottom: 10px;"> 
           <div id="success_message" class="btn btn-primary" style="display: none; float: right;width: 18%;color: #3c763d;background-color: #d1ecc6; border-color: #d6e9c6;">Chat Sent</div>
         </div>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-7">
              <!-- Custom tabs (Charts with tabs)-->
             
              
              <!-- Chat box -->
              <div class="box box-success" style="height: 436px;">
                <div class="box-header">
                  <i class="fa fa-comments-o"></i>
                  <h3 class="box-title">Managemyclinic</h3>
                  
                </div>
                <div class="box-body chat" id="chat-box">
                  <?php $chatid =''; foreach($casechat as $chats){ if($chatid==""){ $chatid = $chats->id;} } ?>
                  <!-- chat item -->
                    <div id="remove-row" class="text-center">
                        <button id="btn-more" data-id="<?php echo $chatid;?>" class="loadmore-btn" onclick="loadmoredata(this)">Load More</button>
                    </div>
                  <?php 
                  if($casechat!=""){
                    foreach($casechat as $chats){

                  ?>
                  <div class="item">
                    <img src="https://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028.jpg?s=80&d=mm&r=g" alt="user image" class="online">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i>  <?php  echo date('d/m/yy h:i A',strtotime($chats->created_at)); ?></small>
                        <?php  echo  $chats->username; ?>
                      </a>
                      <?php  echo  $chats->message; ?>
                    </p>
                  </div><!-- /.item -->
                  <?php } }
                  ?>
                  <!-- chat item -->
                  
                </div><!-- /.chat -->
                <div class="box-footer">
                  <div class="input-group">
                    {!! Form::open(['action' => 'LA\DashboardController@sendchats', 'class' => 'chatclass']) !!}
                    <input type="hidden" class="username" name="username" value="<?php echo Auth::user()->name; ?>">
                    <input type="hidden" class="userid" name="userid" value="<?php echo Auth::user()->context_id; ?>">
                    <input type="hidden" class="c_time" value="<?php echo date("h:i"); ?>">
                    <input type="hidden" class="c_date" value="<?php echo date('d/m/Y'); ?>">

                    <input class="form-control message_chat" autocomplete="off" style="width: 100%;" name="message" id="message_chat" placeholder="Type message...">
                    <div class="input-group-btn">
                      <button class="btn btn-success sendchats" id="sendchats" type="button">Send</button>
                    </div>
                    {!! Form::close() !!}
                  </div>
                </div>
              </div><!-- /.box (chat box) -->

            

             

            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5">

              <!-- quick email widget -->
              <div class="box box-info">
                <div class="box-header">
                  <i class="fa fa-envelope"></i>
                  <h3 class="box-title">Email</h3>
                 
                </div>
                {!! Form::open(['action' => 'LA\DashboardController@sendmail']) !!}
                <div class="box-body">
                 
                    <div class="form-group">
                      <input type="email" class="form-control emailto" name="emailto" id="emailto" placeholder="Email to:" required autocomplete="off">
                      <div id="emailList">
                       </div>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control subject" name="subject" placeholder="Subject" required>
                    </div>
                    <div>
                      <textarea class="textarea mailmessage"  name="mailmessage" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                  
                </div>
                <div class="box-footer clearfix">
                  <button class="pull-right btn btn-default sendemail" type="button" name="sendemail" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
                </div>
                {!! Form::close() !!}
              </div>

              


            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
<div id="mid"><input type="hidden" id="caseiid" value="<?php foreach($casedata as $case){ $dob = date('d',strtotime($case->date_of_birth)); if($dob == date('d')){ echo "yes"; } } ?>"></div>
  <div class="modal fade" id="NewModal" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Birthday Reminder </h4>
      </div>
      {!! Form::open(['action' => 'LA\DashboardController@sendbirthdy', 'id' => 'doctor-add-form','files'=>true]) !!}
      <div class="modal-body">
        <div class="box-body">
           
            
          
          <div class="form-group col-md-12">
            <table class="table table-bordered">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>DOB</th>
                <th>SMS</th>
              </tr>
            </thead>
            <tbody>
              @foreach($casedata as $case)
                <?php 
                  $dob = date('m',strtotime($case->date_of_birth)); 
                ?>
                @if($dob == date('m'))
                  <tr>
                    <td>{{ $case->first_name }}</td>
                    <td>{{ $case->email }}</td>
                    <td>{{ $case->phone }}</td>
                    <td><?php echo date('d/m/Y',strtotime($case->date_of_birth)); ?> </td>
                    <td>
                      <input type="checkbox" class="sendbirthdy" name="sendbirthdy[]" value="{{ $case->id }}">
                      </td>
                  </tr>
                @endif
              @endforeach
            </tbody>
          </table>
          </div>
        </div>

      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-success height-btn savebirthday">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js" integrity="sha512-aUhL2xOCrpLEuGD5f6tgHbLYEXRpYZ8G5yD+WlFrXrPy2IrWBlu6bih5C9H6qGsgqnU6mgx6KtU8TreHpASprw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js" integrity="sha512-3j3VU6WC5rPQB4Ld1jnLV7Kd5xr+cq9avvhwqzbH/taCRNURoeEpoPBK9pDyeukwSxwRPJ8fDgvYXd6SkaZ2TA==" crossorigin="anonymous"></script>
@endpush

@push('scripts')

<script>
    //$('.notification').hide();
	function loadmoredata(obj){
		var id = $(obj).data('id');
		$("#btn-more").html("Loading....");
		$.ajax({
			url: "{{ url(config('laraadmin.adminRoute') . '/loadmoredata') }}",
			method : "POST",
			data : {id:id, _token:"{{csrf_token()}}"},
			dataType : "text",
			success : function (data){
				if(data != ''){
					$('#remove-row').remove();
					$('#chat-box').prepend(data);
				}else{
					$('#btn-more').html("no more data found");
				}
			}
		});
	} 

(function($) {

$('#chat-box').animate({
    scrollTop: $('#chat-box').get(0).scrollHeight
}, 1000);

var socket = io.connect( 'http://cache.singularity8.com:7889/' ); 
    
<?php if((Auth::user()->type == "Doctor" || Auth::user()->type == "Admin" || Auth::user()->type == "Receptionist") && $showpop!=1) { ?>
    if ($('#caseiid').val()=='') { 
        $("#NewModal").modal("hide");
      }
      else{
        if ($.cookie('modal_shown') == null) {
        $.cookie('modal_shown', 'yes', { expires: 1, });
        //$('#NewModal').reveal();
        
         $("#NewModal").modal("show");
    }
   
       
      }
     
<?php } ?>


  $('.message_chat').keypress(function (e) {
  if (e.which == 13) {
    var message = $('.message_chat').val();
        var username = $('.username').val();
        var userid = $('.userid').val();
        var c_time = $('.c_time').val();
        var c_date = $('.c_date').val();
        var data = [];
        data.message = message;
        data.username = username;
        
        
        if(message!=''){
            socket.emit('message', { username: username, message: message });
        
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/sendchats') }}",
                data: { userid:userid, username:username, message:message }, 
                success: function( msg ) {
                $('#message_chat').val('');
                }
            });
        }
        return false;
  }
});

 $(".sendchats").on("click", function(){
      
        var message = $('.message_chat').val();
        var username = $('.username').val();
        var userid = $('.userid').val();
        var c_time = $('.c_time').val();
        var c_date = $('.c_date').val();
        var data = [];
        data.message = message;
        data.username = username;
        console.log('data', data)
        
        if(message!=''){
            socket.emit('message', { username: username, message: message });
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/sendchats') }}",
                data: { userid:userid, username:username, message:message }, 
                success: function( msg ) {
                $('#message_chat').val('');
                $('#chat-box').animate({
                    scrollTop: $('#chat-box').get(0).scrollHeight
                }, 10);
                }
            });
        }
    });
    
socket.on('message', function(msg){
  console.log(msg);
  var c_time = $('.c_time').val();
  var c_date = $('.c_date').val();
    const audio = new Audio("http://smartops.co.in/managemyclinic/alert.mp3");
    audio.play();
    
  $('.chat').append(' <div class="item"><img src="https://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028.jpg?s=80&amp;d=mm&amp;r=g" alt="user image" class="online"><p class="message"><a href="#" class="name">'+msg.name+'</a>'+msg.message+' </p> <small class="text-muted pull-right"><i class="fa fa-clock-o"></i>&nbsp;'+c_date+'&nbsp;'+c_time+'</small></div>')
  
  $('#chat-box').animate({
    scrollTop: $('#chat-box').get(0).scrollHeight
    }, 10);
    
});

  $(".sendemail").on("click", function(){
      
        var emailto = $('.emailto').val();
        var mailmessage = $('.mailmessage').val();
        var subject = $('.subject').val();
         $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/sendmail') }}",
                data: { emailto:emailto, mailmessage:mailmessage, subject:subject }, 
                success: function( msg ) {
                  alert("Email sent Successfully ");
                  setTimeout(function(){
                               location.reload(); 
                          }, 2000);
                  
                }
            });
    });

  $('.emailto').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        {
        
         $.ajax({
          url: "{{ url(config('laraadmin.adminRoute') . '/fetch') }}",
          method:"GET",
          data:{query:query},
          success:function(data){
           $('#emailList').fadeIn();  
                    $('#emailList').html(data);
          }
         });
        }
    });

    
})(window.jQuery);

 $(document).on('click', '.emaildata', function(){  
        $('#emailto').val($(this).text());  
        $('#emailList').fadeOut();  
    }); 
</script>

@endpush

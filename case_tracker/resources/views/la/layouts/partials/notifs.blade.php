<style type="text/css">
	.badge {
		position: absolute;
		border-radius: 50%;
		background-color: red;
		color: white;
		margin-top: -7px;
		padding: 7px 10px;
		right: -9px;
		display: none;
}
</style>	
		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Messages: style can be found in dropdown.less-->
				
				@if (Auth::guest())
					<li><a href="{{ url('/login') }}">Login</a></li>
					<li><a href="{{ url('/register') }}">Register</a></li>
				@else
				 <?php if(Auth::user()->type == "Receptionist" ) {
			 			$product=DB::table('courier_medicine')->where('currentdate','=', date('Y-m-d'))->get();
			 			
				 ?>
				 <li class="dropdown" style="margin-right: 10px;">
	 					<?php 
	            			$count_m=DB::table('courier_medicine')->where([ ['currentdate','=', date('Y-m-d')], ['read_type','=','unread'] ])->get();
					 			
						 ?>
			          <a href="#" class="dropdown-toggle updatevalue" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" aria-hidden="true" style="font-size: 24px;margin-top: 3px;"></i>  <span class="badge"><b><?php echo count($count_m); ?></b></span></a>
			          <ul class="dropdown-menu notify-drop">
			            <div class="notify-drop-title">
			            	<div class="row">

			            		<div class="col-md-6 col-sm-6 col-xs-6">Notifications (<b><?php echo count($product); ?></b>)</div>
			            		<div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="#" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title=""><i class="fa fa-dot-circle-o"></i></a></div>
			            	</div>
			            </div>
			            <!-- end notify title -->
			            <!-- notify content -->
			            <div class="drop-content">
			            	<?php 
			            	foreach($product as $value){?>
			            		
			            	
			            	<li>
			            		
			            		<div class="col-md-12 col-sm-12 col-xs-12 pd-l0" style="padding-left: 17px;"><a href="javascript:void(0);">Case ID : {{ $value->case_id }}</a>  <a href="javascript:void(0);" style="float: right;"><?php echo  date('d/m/Y', strtotime($value->currentdate)); ?></a>
			            		<!-- <p> <b>Potency</b> : {{ $value->potency }} &nbsp; <b>Frequency</b> : {{ $value->frequency }}</p> -->
			            		<!-- <p class="time">1 Saat önce</p> -->
			            		</div>
			            	</li>
			            <?php } ?>
			            	
			            </div>
			           
			          </ul>
			        </li>
			    <?php } ?>
			<!--     <?php if(Auth::user()->type != "Receptionist" ) {?>
			    <li style="margin-right: 16px;"><a href="{{ url('/admin/groupsms') }}">Group SMS</a></li> 
			<?php } ?> -->
					<!-- User Account Menu -->
					<li class="dropdown user user-menu">
						<!-- Menu Toggle Button -->
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<!-- The user image in the navbar-->
							<img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" class="user-image" alt="User Image"/>
							<!-- hidden-xs hides the username on small devices so only the image appears. -->
							<span class="hidden-xs">{{ Auth::user()->name }}</span>
						</a>
						<ul class="dropdown-menu">
							<!-- The user image in the menu -->
							<li class="user-header">
								<img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" class="img-circle" alt="User Image" />
								<p>
									{{ Auth::user()->name }}
									<?php
									$datec = Auth::user()['created_at'];
									?>
									<small>Member since <?php echo date("M. Y", strtotime($datec)); ?></small>
								</p>
							</li>
							<!-- Menu Body -->
							
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="{{ url(config('laraadmin.adminRoute') . '/users/') .'/'. Auth::user()->id }}" class="btn btn-default btn-flat">Profile</a>
								</div>
								<div class="pull-right">
									<a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
								</div>
							</li>
						</ul>
					</li>
				@endif
				
			</ul>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script type="text/javascript">
			$(".updatevalue").click(function(){
				var read_type ="read";
			  $.ajax({
			    type : 'POST',
			    url : "{{ url(config('laraadmin.adminRoute') . '/couriermedicine/updatenotify') }}",
			    data:{read_type:read_type},
			    success:function(data){
			       console.log(data);
			       var text = "0";
			       	if(data==1){
			       		$('.badge').text(text);
		       		}
		        }

			    

			    });
			});
		</script>
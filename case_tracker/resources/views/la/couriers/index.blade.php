@extends("la.layouts.app")

@section("contentheader_title", "Couriers")
@section("contentheader_description", "Couriers listing")
@section("section", "Couriers")
@section("sub_section", "Listing")
@section("htmlheader_title", "Couriers Listing")

@section("headerElems")

@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<style type="text/css">

.modal-body  .box-body .form-group:first-child {
display:none;
    
}


</style>
<div class="box box-success">
    @la_access("Couriers", "create")

    <div class="col-sm-12">
<ul class="nav navbar-nav">

		<li><a href="javascript:void(0)" class="topmenu-icon-text" data-toggle="modal" data-target="#usermodal"><img src="{{ asset('la-assets/img/newuser.png') }}" class="topmenu-icon"><br>New</a></li>
		<?php /*?><li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/openfile.png') }}" class="topmenu-icon"><br>Open</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/import.png') }}" class="topmenu-icon"><br>Import</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/export.png') }}" class="topmenu-icon"><br>Export</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/find.png') }}" class="topmenu-icon"><br>Find</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/followup.png') }}" class="topmenu-icon"><br>Follow Up</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/print-icon.png') }}" class="topmenu-icon"><br>Print</a></li>
		 <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/delete.png') }}" class="topmenu-icon"><br>Delete Case</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/exit.png') }}" class="topmenu-icon"><br>Exit</a></li> <?php */ ?>

		
	</ul>
</div>@endla_access
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
					<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Couriers", "create")
<div class="modal fade" id="usermodal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" style="width:900px">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Courier Package</h4>
			</div>
			{!! Form::open(['action' => 'LA\CouriersController@create', 'id' => 'caseremind-add-form']) !!}
			   {!! csrf_field() !!}

			<div class="modal-body">
				<div class="box-body">
                    <div class="col-md-12 ">
						<label for="name">Package Id:</label>
							<input class="form-control regid" placeholder="Package Id"  name="package_id" id="package_id" type="text" value="" aria-required="true" required>

					</div>

					  <div class="customer_records">
					  	<div class="col-md-4">
				  		<label for="name">Name:</label>
					    <select class="form-control medicine_name" data-placeholder="Name"  name="name[]" id="medicine_name" required onchange="priceget(this);" >
					   	
					    		<option disabled selected>Select Name</option>
	        							<?php foreach($medicines as $stname){?>
	        							<option value="{{ $stname->remedy }}" data-price="100">{{ $stname->remedy }}</option>
	        							<?php }?>
						</select>
						</div>
						<div class="col-md-2">
							<label for="name">Quantity:</label>
					    	<input name="quantity[]" id="quantity" type="number" value="1" class="form-control quantity" placeholder="Quantity" oninput="calc()">
						</div>
						<div class="col-md-2">
							<label for="name">Price:</label>
				    		<input type="text" class="form-control price" placeholder="Price" id="price" readonly>
				    	</div>
						<div class="col-md-2">
							<label for="name">Total Price:</label>
					    	<input name="price[]" type="text" class="form-control total" placeholder="Total Price" id="total">
				    	</div>
				    	
				    	<div class="col-md-2">
				    		<label for="name">&nbsp;</label>
					    	<a class="btn btn-success extra-fields-customer" href="#" style="margin-top: 24px;">Add More</a>
				    	</div>
					  </div>

					  <div class="customer_records_dynamic"></div>


					   
				
				</div>						
			</div>                  
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
				
			</div>
		</tr>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

<script>
$(function () {
    // $('').attr('name'='booking_date')
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/couriers_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#caseremind-add-form").validate({
		
	});
	
 
    
});

</script>
<script type="text/javascript">
    //setTimeout(function() { $( "booking-add-form" ).hide();},1000);
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
   
    $('#caseremind-add-form').find("input[name='start_date']").val(today);
     $('#caseremind-add-form').find("input[name='end_date']").val(today);

	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
 
</script>

<script type="text/javascript">
	function randomNumber(len) {
    var randomNumber;
    var n = '';

    for(var count = 0; count < len; count++) {
        randomNumber = Math.floor(Math.random() * 10);
        n += randomNumber.toString();
    }
    return n;
}

document.getElementById("package_id").value = randomNumber(6);
</script>
<script type="text/javascript">
$('.extra-fields-customer').click(function() {
  $('.customer_records').clone().appendTo('.customer_records_dynamic');
  $('.customer_records_dynamic .customer_records').addClass('single remove');
  $('.single .extra-fields-customer').remove();
  $('.single').append('<a href="#" class="btn btn-danger remove-field btn-remove-customer" style="margin-left: 22px;">Remove</a>');
  $('.customer_records_dynamic > .single').attr("class", "remove");

  $('.customer_records_dynamic input').each(function() {
    var count = 0;
    var fieldname = $(this).attr("name");
    $(this).attr('name', fieldname + count);
    count++;
  });

});

$(document).on('click', '.remove-field', function(e) {
  $(this).parent('.remove').remove();
  e.preventDefault();
});

</script>
<script>
// 	$('.medicine_name').on('change',function(){
//     var price = $(this).children('option:selected').data('price');
//      $(".price").val(price);
// });

function calc() 
{
  var price =  $(".price").val();
  var quantity =  $(".quantity").val();
  var total = parseFloat(price) * quantity
  if (!isNaN(total))
	$(".total").val(total);
}

function priceget(obj){
	var price = $(obj).children('option:selected').attr('data-price');
	 $(obj).parent().parent().find(".price").val(price);
}
</script>
@endpush
<style>
    .error{ display:none;}
</style>
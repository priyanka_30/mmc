@extends("la.layouts.app")

@section("contentheader_title", "Refrencedetails")
@section("contentheader_description", "Refrencedetails listing")
@section("section", "Refrencedetails")
@section("sub_section", "Listing")
@section("htmlheader_title", "Refrencedetails Listing")

@section("headerElems")
@la_access("Refrencedetails", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Refrencedetails</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>Refrence Type</th>
			<th>Count</th>
		</tr>
		</thead>
		<tbody>
			
			<tr>
				<td>Patient</td>
				<td><?php echo $count1; ?></td>
			</tr>
			<tr>
				<td>News Paper</td>
				<td><?php echo $count2; ?></td>
			</tr>
			<tr>
				<td>Walk-in</td>
				<td><?php echo $count3; ?></td>
			</tr>
			<tr>
				<td>other</td>
				<td><?php echo $count4; ?></td>
			</tr>
			<tr>
				<td>Just Dial</td>
				<td><?php echo $count5; ?></td>
			</tr>		

			<tr>
				<td>Web online</td>
				<td><?php echo $count6; ?></td>
			</tr>
			<tr>
				<td>panchkula shopp</td>
				<td><?php echo $count7; ?></td>
			</tr>
			<tr>
				<td>Brosher</td>
				<td><?php echo $count8; ?></td>
			</tr>
			<tr>
				<td>Doctor</td>
				<td><?php echo $count9; ?></td>
			</tr>
			<tr>
				<td>Facebook</td>
				<td><?php echo $count10; ?></td>
			</tr>

			<tr>
				<td>Practo</td>
				<td><?php echo $count11; ?></td>
			</tr>
			<tr>
				<td>Lybrate</td>
				<td><?php echo $count12; ?></td>
			</tr>
			<tr>
				<td>Staff</td>
				<td><?php echo $count13; ?></td>
			</tr>
			
		</tbody>
		</table>
	</div>
</div>




@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
       // serverSide: true,
      //  ajax: "{{ url(config('laraadmin.adminRoute') . '/refrencedetails_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
@endpush
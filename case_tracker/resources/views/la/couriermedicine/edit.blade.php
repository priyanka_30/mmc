@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/medicalcases') }}">Couriermedicine</a> :
@endsection
@section("contentheader_description", $couriermedicine->$view_col)
@section("section", "Medicalcase")
@section("section_url", url(config('laraadmin.adminRoute') . '/couriermedicine'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Couriermedicine Edit : ".$couriermedicine->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="post" action="{{ url(config('laraadmin.adminRoute') . '/couriermedicine/medicineupdate/') }}" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
								    <input type="hidden" name="id" value="<?php echo $couriermedicine->id; ?>">
                                    <?php if($couriermedicine->post_type=="Courier"){ ?>
                                        <div class="form-group col-md-12" >
                                            <label for="name">POD :</label>
                                            <input type="text" class="form-control" required="1" name="pcd" value="<?php echo $couriermedicine->pcd; ?>" >           
                                            
                                        </div>
                                        <div class="form-group col-md-12" >
                                            <label for="name">Courier :</label>
                                            <input type="text" class="form-control" required="1" name="courier" value="<?php echo $couriermedicine->courier; ?>">            
                                        </div>
                                    <?php } else { ?>
                                        <div class="form-group col-md-12" style="margin-top: 14px;" >
                                            <label class="container-checkbox">Pickup By <?php echo $user->first_name." ".$user->surname; ?>
                                              <input type="checkbox" class="courier_outstation" name="pickup" value="1" required <?php if($couriermedicine->pickup=="1"){ echo "checked" ;} ?> >
                                              <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    <?php } ?>
					<div class="form-group">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
@endpush

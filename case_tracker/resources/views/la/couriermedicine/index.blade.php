@extends("la.layouts.app")

@section("contentheader_title", "Courier")
@section("contentheader_description", "Courier listing")
@section("section", "Courier")
@section("sub_section", "Listing")
@section("htmlheader_title", "Courier Listing")

@section("headerElems")

@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
   
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>RegID</th>
			<th>Name</th>
			<th>Post Type</th>
			
			<th>Is Assign</th>
			<th>POD</th>
			<th>Date</th>
			<th>Actions</th>
		
		</tr>
		</thead>
		<tbody>
			<?php
				foreach($data as $val){
			 ?>
			<tr>
				<td><?php echo $val->case_id ?></td>
				<td><?php echo $val->first_name." ". $val->surname ?></td>
				<td><?php echo $val->post_type ?></td>
				
				<td><?php echo $val->is_assign ?></td>
				<td><?php echo $val->pcd ?></td>
				<td><?php echo date('d M Y',strtotime($val->created_at)); ?></td>
				<td>
					<?php if($val->is_assign==0){ ?>
					<a href="/managemyclinic/admin/couriermedicine/assign/<?php echo $val->id; ?>" class="btn btn-primary btn-xs" style="display:inline;padding:2px 5px 3px 5px; margin-left:8px">Assign</a>
					<a onClick="openPrevious(<?php echo $val->case_id; ?>)" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px; margin-left:8px">Previous Record</a>
					
				<?php }
				else{ ?>
					<button class="btn btn-success btn-xs" type="button"><i class="fa fa-check" style="margin-right: 4px;"></i>Assigned</button>
					<a onClick="openSendSms(<?php echo $val->id; ?>)" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px; margin-left:8px">Send SMS</a>
					<a onClick="openPrevious(<?php echo $val->case_id; ?>)" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px; margin-left:8px">Previous Record</a>
				<?php } ?>

				</td>
			
			</tr>
		<?php } ?>
		</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="AddPackage" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Courier No</h4>
			</div>
			{!! Form::open(['action' => 'LA\CouriermedicineController@savepackages']) !!}
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}
                  	
					<fieldset class="examin-fieldset">
						<input class="form-control" id="id" placeholder=""  name="id" type="hidden" value="" aria-required="true">
						<input class="form-control"  name="is_assign" type="hidden" value="1" >
						<?php if($session!=""){

							  if($courier['post_type']=="Courier"){
						?>
						<div class="form-group col-md-12" >
    						<label for="name">POD :</label>
    						<input type="text" class="form-control" required="1" name="pcd" >		    
    						
    					</div>
    					<div class="form-group col-md-12" >
    						<label for="name">Courier :</label>
    						<input type="text" class="form-control" required="1" name="courier">		    
    					</div>
						<?php } else{ ?>
							<div class="form-group col-md-12" style="margin-top: 14px;" >
							<label class="container-checkbox">Pickup By <?php echo $user->first_name." ".$user->surname; ?>
							  <input type="checkbox" class="courier_outstation" name="pickup" value="1" required>
							  <span class="checkmark"></span>
							</label>
						</div>
						<?php } } ?>				
						
					</fieldset>
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				<button type="submit" class="btn btn-success height-btn savepicture">Save</button>
				 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="AddSendsms" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Send SMS</h4>
			</div>
			{!! Form::open(['action' => 'LA\CouriermedicineController@sendsms']) !!}
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}
                  	
					<fieldset class="examin-fieldset">
						
						<div class="form-group col-md-12" >
    						<label for="name">Mobile :</label>
    						<input type="text" class="form-control phone" required="" name="phone" >
    						<input type="hidden" class="form-control case_id" required="" name="case_id" >
    						<input type="hidden" class="form-control regid" required="" name="regid" >
    						<input type="hidden" class="form-control first_name" required="" name="first_name" >		    
    						<input type="hidden" class="form-control surname" required="" name="surname" >
    						<input type="hidden" class="form-control pcd" required="" name="pcd" >
    						<input type="hidden" class="form-control courier" required="" name="courier" >
    						
    					</div>
    					<div class="form-group col-md-12" >
    						<label for="name">Message :</label>
    						<textarea type="text" class="form-control message" required="1" name="message" placeholder="Message"></textarea>	    
    					</div>
									
						
					</fieldset>
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				<button type="submit" class="btn btn-success height-btn savepicture">Send</button>
				 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="Addprevious" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Previous Record</h4>
			</div>
			{!! Form::open(['action' => 'LA\CouriermedicineController@sendsms']) !!}
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}
                  	
					
						
						 <div class="col-md-12">
               				<table class="table table-bordered tb-bg amounttable">
                                        <tr>
                                            <th style='text-align:center;'>Date</th>
                                            <th style='text-align:center;'>POD</th>
                                            <th style='text-align:center;'>Courier Company</th>
                                        </tr>
                                        <tbody class="searchamount">

                                        </tbody>
                                    </table>
                </div>
									
						
				
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				
				 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

<script>
$(function () {

	var getfirst = '<?php echo $courier['id']; ?>';
    if(getfirst!=""){
      $('#AddPackage').modal('show');
       document.getElementById("id").value=getfirst;
       console.log('id',getfirst);
  	}
  	else{
  		
  		var getfirst = <?php echo Session::forget('pid'); ?>
  		document.getElementById("id").value=getfirst;
  		console.log('id',getfirst);
  		$('#AddPackage').modal('hide');

  	}
    // $('').attr('name'='booking_date')
	$("#example1").DataTable({
		processing: true,
       // serverSide: true,
        //ajax: "{{ url(config('laraadmin.adminRoute') . '/couriermedicine_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#booking-add-form").validate({
		
	});
	
 
    
});

</script>
<script type="text/javascript">

	function openPrevious(id){
    $('.case_id').val(id);
    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/getcourierdetails') }}",
            data: { id:id }, 
            success: function( msg ) {
              	$(".searchamount").html(msg);
               	$('#Addprevious').modal('show');
            }
        });

}

	function openSendSms(id){
    $('.case_id').val(id);
    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/getmedicinedetail') }}",
            data: { id:id }, 
            success: function( msg ) {
              	var casearr = JSON.parse(msg);
        	 	var phone= casearr['phone'];
        	 	var first_name= casearr['first_name'];
        	 	var surname= casearr['surname'];
        	 	var pcd= casearr['pcd'];
        	 	var courier= casearr['courier'];
        	 	var regid= casearr['regid'];
        	 	var message= casearr['message'];
            	$('.phone').val(phone);
            	$('.first_name').val(first_name);
            	$('.surname').val(surname);
            	$('.pcd').val(pcd);
            	$('.courier').val(courier);
            	$('.regid').val(regid);
            	$('.message').val(message);
               	$('#AddSendsms').modal('show');
            }
        });

}
    //setTimeout(function() { $( "booking-add-form" ).hide();},1000);
    $('#booking-add-form').find("input[name='status']").parent().hide();
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
   
    $('#booking-add-form').find("input[name='booking_date']").val(today);

	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
 
</script>
<script>
	$('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});
</script>
@endpush
<style>
    .error{ display:none;}
</style>
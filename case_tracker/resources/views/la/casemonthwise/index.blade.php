@extends("la.layouts.app")

@section("contentheader_title", "Casemonthwise")
@section("contentheader_description", "Casemonthwise listing")
@section("section", "Casemonthwise")
@section("sub_section", "Listing")
@section("htmlheader_title", "Casemonthwise Listing")

@section("headerElems")

@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>Month</th>
			<th>Year</th>
			<th>New Case</th>
			
		</tr>
		</thead>
		<tbody>
			<?php 
			$total =0;
			foreach ($case as $val){
				// foreach($pot as $casepot){
			 ?>
			<tr>
				<td><?php echo date("F", mktime(0, 0, 0, $val->month, 1)); ?>  </td>
				<td><?php echo $val->year; ?></td>
				<td><?php echo $val->id; ?> </td>
				

			</tr>
			<?php }  ?>
			
		</tbody>
		</table>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
$(function () {

	

var table = $("#example1").DataTable({
		processing: true,
       // serverSide: true,
       //ajax: "{{ url(config('laraadmin.adminRoute') . '/payment_dt_ajax') }}",
       
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		dom: 'Bfrtip',
		buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
	});
 
	$("#payment-add-form").validate({
		
	});
});

  
</script>
<script type="text/javascript">
	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});


	
  </script>
@endpush
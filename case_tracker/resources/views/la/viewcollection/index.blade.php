@extends("la.layouts.app")

@section("contentheader_title", "Viewcollection")
@section("contentheader_description", "Viewcollection listing")
@section("section", "Viewcollection")
@section("sub_section", "Listing")
@section("htmlheader_title", "Viewcollection Listing")

@section("headerElems")

@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		 <div class="form-horizontal" style="margin-bottom: 10px;">
          <div class="control-group form-inline">
          	{!! Form::open(['action' => 'LA\ViewcollectionController@search', 'id' => 'daycharges-add-form']) !!}
            <div class="form-group col-md-3">
				<label for="mobile" >Date</label>
				<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php echo $query; ?>" />
				    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
			</div>
			<div class="form-group col-md-4">
				
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
          </div>
        </div>
       
		
			<?php 
			//$total =0;
			$cash = 0;
			$cheque = 0;
			$online = 0;
			$card = 0;
			$deficit = 0;
			
			//$totalbankmonth = 0;
			foreach ($payments as $pay){
				if($pay->mode =="C"){
					$cash += $pay->amount;
				}

				if($pay->mode =="B"){
					$cheque += $pay->amount;
				}

				if($pay->mode =="O"){
					$online += $pay->amount;
				}
				if($pay->mode =="S"){
					$card += $pay->amount;
				}

		//	$total += $pay->received_price
		 ?>
			
			<?php }
			//echo $totalcash;
			$total_coll = $cash + $cheque + $online + $card;
		//	$totalacsh = $totalcash - $amount;
			$totalacsh = $total_coll - $totalcash - $card - $cheque - $online - $amount;
			$cash_handed = $cash_sum - $bank_sum;

			if($totalbank!=0){
				$deficit =  $totalcash - $totalbank;
			}
			

			?>
			
		<div style="margin-top: 6rem;">
			<div class="col-md-6" style="border:1px solid #ccc; margin-right: 10px;width: 40%;"> 
				 <div class = "form-group">
		            <label class = "col-sm-3 control-label" style="margin-bottom: 10px;margin-top: 15px;">Collection</label>
		            <div class = "col-sm-9" style="margin-bottom: 10px;margin-top: 15px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $total_coll;?>"/>
		            </div>
		         </div>
		         <div class = "form-group">
		            <label class = "col-sm-3 control-label" style="margin-bottom: 10px;">Cash Handed</label>
		            <div class = "col-sm-9" style="margin-bottom: 10px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $totalcash;?>"/>
		            </div>
		         </div>
		         <div class = "form-group">
		            <label class = "col-sm-3 control-label" style="margin-bottom: 10px;">Credit Card</label>
		            <div class = "col-sm-9" style="margin-bottom: 10px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $card;?>"/>
		            </div>
		         </div>
		         <div class = "form-group">
		            <label class = "col-sm-3 control-label" style="margin-bottom: 10px;">Cheque</label>
		            <div class = "col-sm-9" style="margin-bottom: 10px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $cheque;?>" />
		            </div>
		         </div>
		         <div class = "form-group">
		            <label class = "col-sm-3 control-label" style="margin-bottom: 10px;">Online</label>
		            <div class = "col-sm-9" style="margin-bottom: 10px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $online;?>"/>
		            </div>
		         </div>
		         <div class = "form-group">
		            <label class = "col-sm-3 control-label" style="margin-bottom: 10px;">Expenses</label>
		            <div class = "col-sm-9" style="margin-bottom: 10px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $amount;?>"/>
		            </div>
		         </div>
			 </div>
			<div class="col-md-6" style="border:1px solid #ccc;height: 281px;">
				  <div class = "form-group">
		            <label class = "col-sm-4 control-label" style="margin-bottom: 10px; margin-top: 15px;">Deficit</label>
		            <div class = "col-sm-8" style="margin-bottom: 10px;margin-top: 15px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $totalacsh;?>"/>
		            </div>
		         </div>
		           <div class = "form-group">
		            <label class = "col-sm-4 control-label" style="margin-bottom: 10px;">Bank Deposit</label>
		            <div class = "col-sm-8" style="margin-bottom: 10px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $totalbank;?>"/>
		            </div>
		         </div>

		          <!-- <div class = "form-group">
		            <label class = "col-sm-4 control-label" style="margin-bottom: 10px;">Bank deposited(month)</label>
		            <div class = "col-sm-8" style="margin-bottom: 10px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $totalbankmonth;?>"/>
		            </div>
		         </div> -->

		         <div class = "form-group">
		            <label class = "col-sm-4 control-label" style="margin-bottom: 10px;">Cash in Hand</label>
		            <div class = "col-sm-8" style="margin-bottom: 10px;">
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $cash_handed;?>"/>
		            </div>
		         </div>
			</div>
			<!--  <div class="form-group col-md-6">
				<label for="mobile" >Collection</label>
				<div  class="input-group" >
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $total_coll;?>"/>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="mobile" >Deficit</label>
				<div  class="input-group" >
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $deficit;?>"/>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="mobile" >Cash Deposit</label>
				<div  class="input-group" >
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $totalcash;?>"/>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="mobile" >Bank Deposit</label>
				<div  class="input-group" >
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $totalbank;?>"/>
				</div>
			</div>
			 <div class="form-group col-md-6">
				<label for="mobile" >Credit Card</label>
				<div  class="input-group" >
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $card;?>"/>
				</div>
			</div>

			 <div class="form-group col-md-6">
				<label for="mobile" >Cash</label>
				<div  class="input-group" >
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $totalacsh;?>" />
				</div>
			</div>

			 <div class="form-group col-md-6">
				<label for="mobile" >Cheque</label>
				<div  class="input-group" >
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $cheque;?>" />
				</div>
			</div>

			 <div class="form-group col-md-6">
				<label for="mobile" >Online</label>
				<div  class="input-group" >
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $online;?>"/>
				</div>
			</div>

			<div class="form-group col-md-6">
				<label for="mobile" >Expenses</label>
				<div  class="input-group" >
				    <input class="form-control" type="text" name="date" readonly style="background-color: #fff;" value="<?php  echo $amount;?>"/>
				</div>
			</div> -->
			
		</div>
			
			
		
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
$(function () {

	

var table = $("#example1").DataTable({
		processing: true,
       // serverSide: true,
       //ajax: "{{ url(config('laraadmin.adminRoute') . '/payment_dt_ajax') }}",
       
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		dom: 'Bfrtip',
		buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
	});
 
	$("#payment-add-form").validate({
		
	});
});

  
</script>
<script type="text/javascript">
	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});


	
  </script>
@endpush
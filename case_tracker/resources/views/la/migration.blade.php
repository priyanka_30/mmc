@extends('la.layouts.app')

@section('htmlheader_title') Migration @endsection
@section('contentheader_title') Migration @endsection
@section('contentheader_description') Migration @endsection

@section('main-content')

    

        <section class="content">
          
          <!-- Main row -->
          <div class="row">
                
                <div class="col-md-2" style="width: 12%;">
                  {!! Form::open(['action' => 'LA\MigrationController@savepersonaldata', 'style' => 'display:inline-block']) !!}
                    <button class="btn btn-success" name="submit"  type="submit">Import Case Data</button>
                  {!! Form::close() !!}
                </div>
            

                
                <div class="col-md-2">
                 {!! Form::open(['action' => 'LA\MigrationController@savecommunicationdetails', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Communication Data</button>
                  {!! Form::close() !!}
                </div>

                <div class="col-md-2" style="width: 13%;">
                 {!! Form::open(['action' => 'LA\MigrationController@savedoctorsdetail', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Doctors Data</button>
                  {!! Form::close() !!}
                </div>

                <div class="col-md-2">
                 {!! Form::open(['action' => 'LA\MigrationController@saveheightweightdetails', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Height/Weight Data</button>
                  {!! Form::close() !!}
                </div>

                <div class="col-md-2" style="width: 14%;">
                 {!! Form::open(['action' => 'LA\MigrationController@savereminderdetails', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Reminder Data</button>
                  {!! Form::close() !!}
                </div>

                <div class="col-md-2">
                 {!! Form::open(['action' => 'LA\MigrationController@savehomeodetails', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Homeo Data</button>
                  {!! Form::close() !!}
                </div>
              </div>
              <div class="row" style="margin-top: 12px;">
                <div class="col-md-2" style="width: 14%;">
                 {!! Form::open(['action' => 'LA\MigrationController@savefollowupdetails', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Remedy Data</button>
                  {!! Form::close() !!}
                </div>

                <div class="col-md-3" style="width: 19%;">
                 {!! Form::open(['action' => 'LA\MigrationController@saveadditionalcharges', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Additional Charges Data</button>
                  {!! Form::close() !!}
                </div>

                <div class="col-md-2">
                 {!! Form::open(['action' => 'LA\MigrationController@saveinvestigation', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Investigation Data</button>
                  {!! Form::close() !!}
                </div>

                <div class="col-md-2">
                 {!! Form::open(['action' => 'LA\MigrationController@savefollowupnotes', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Followp Data</button>
                  {!! Form::close() !!}
                </div>

                <div class="col-md-2">
                 {!! Form::open(['action' => 'LA\MigrationController@savebill', 'style' => 'display:inline-block']) !!}

                    <button class="btn btn-success" name="submit"  type="submit">Import Bill</button>
                  {!! Form::close() !!}
                </div>
                
           
          </div>
        </section><!-- /.content -->
 
@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js" integrity="sha512-aUhL2xOCrpLEuGD5f6tgHbLYEXRpYZ8G5yD+WlFrXrPy2IrWBlu6bih5C9H6qGsgqnU6mgx6KtU8TreHpASprw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js" integrity="sha512-3j3VU6WC5rPQB4Ld1jnLV7Kd5xr+cq9avvhwqzbH/taCRNURoeEpoPBK9pDyeukwSxwRPJ8fDgvYXd6SkaZ2TA==" crossorigin="anonymous"></script>
@endpush


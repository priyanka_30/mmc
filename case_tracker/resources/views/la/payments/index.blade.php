@extends("la.layouts.app")

@section("contentheader_title", "Payment")
@section("contentheader_description", "Payment listing")
@section("section", "Payment")
@section("sub_section", "Listing")
@section("htmlheader_title", "Payment Listing")

@section("headerElems")
@la_access("Payments", "create")
	<!-- <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Payment</button> -->
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		   <!-- Custom Filter -->
		   <div class="form-horizontal">
          <div class="control-group form-inline">
            <label for="name" class="col-md-2" style="width: 8%;margin-top: 2px;"> Min Date:</label>
            <div class="controls col-md-3" style="padding-left: 0px;">
              <input name="min" id="min" type="text" class="form-control input-sm" style="width: 100%;">
            </div>
          </div>
        </div>

        <div class="form-horizontal">
          <div class="control-group form-inline">
            <label for="name" class="col-md-2" style="width: 8%;margin-top: 2px;"> Max Date:</label>
            <div class="controls col-md-3" style="padding-left: 0px;">
              <input name="max" id="max" type="text" class="form-control input-sm" style="width: 100%;">
            </div>
          </div>
        </div>
 
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Payments", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Payment</h4>
			</div>
			{!! Form::open(['action' => 'LA\PaymentsController@store', 'id' => 'payment-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'email')
					@la_input($module, 'phone')
					@la_input($module, 'website')
					@la_input($module, 'assigned_to')
					@la_input($module, 'connect_since')
					@la_input($module, 'address')
					@la_input($module, 'city')
					@la_input($module, 'description')
					@la_input($module, 'profile_image')
					@la_input($module, 'profile')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script>
$(function () {

	$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = $('#min').datepicker("getDate");
            var max = $('#max').datepicker("getDate");
            var startDate = new Date(data[4]);
            if (min == null && max == null) { return true; }
            if (min == null && startDate <= max) { return true;}
            if(max == null && startDate >= min) {return true;}
            if (startDate <= max && startDate >= min) { return true; }
            return false;
        }
        );

       
            $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });

var table = $("#example1").DataTable({
		processing: true,
        serverSide: true,
       ajax: "{{ url(config('laraadmin.adminRoute') . '/payment_dt_ajax') }}",
       
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		dom: 'Bfrtip',
		buttons: [
            {
             extend: 'excel',
             text: 'Excel',
             className: 'btn btn-default',
             exportOptions: {
                columns: 'th:not(:last-child)'
             }
          },
           {
             extend: 'pdfHtml5',
             text: 'PDF',
             className: 'btn btn-default',
             exportOptions: {
                columns: 'th:not(:last-child)'
             }
          }
        ],
	});
 $('#min, #max').change(function () {
                table.draw();
            });
	$("#payment-add-form").validate({
		
	});
});

  
</script>
<script type="text/javascript">
	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
  </script>
@endpush
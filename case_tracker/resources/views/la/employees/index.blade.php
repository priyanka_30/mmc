@extends("la.layouts.app")

@section("contentheader_title", "Patient")
@section("contentheader_description", "Patient listing")
@section("section", "Patient")
@section("sub_section", "Listing")
@section("htmlheader_title", "Patient Listing")

@section("headerElems")
@la_access("Employees", "create")
<?php if(Auth::user()->type != "Receptionist") { ?>

	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Patient</button>
<?php } ?>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
		
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>

	</div>
</div>

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Patient</h4>
			</div>
			{!! Form::open(['action' => 'LA\EmployeesController@store', 'id' => 'employee-add-form', 'onsubmit' => 'return checkForm(this)']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'designation')
					@la_input($module, 'gender')
					@la_input($module, 'mobile')
					@la_input($module, 'mobile2')
					@la_input($module, 'email')
					@la_input($module, 'dept')
					@la_input($module, 'city')
					@la_input($module, 'address')
					@la_input($module, 'about')
					@la_input($module, 'date_birth')
					@la_input($module, 'date_hire')
					@la_input($module, 'date_left')
					@la_input($module, 'salary_cur')
					--}}
				
					<div class="form-group">
						<label for="role">Role* :</label>
						<select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="role">
							<option value="5">Patient</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<div class="modal fade" id="AddPackage" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Packages</h4>
			</div>
			{!! Form::open(['action' => 'LA\EmployeesController@savepackages']) !!}
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}
                  	
					<fieldset class="examin-fieldset">

							<div class="form-group col-md-12 religion-box" >
    						<label for="name">Select Scheme:</label>
    						<select class="form-control select2-hidden-accessible scheme" required="1" data-placeholder="Select Scheme" rel="select2" name="packages" tabindex="-1" aria-hidden="true" aria-required="true"required>
    							    <option value="Other">Other</option>
    							<?php foreach($packages as $packagelist){?>
							        <option value="<?php echo $packagelist->name;?>"><?php echo $packagelist->name;?></option>
							    <?php } ?>
							        
    						</select>	
    						<input class="form-control" id="id" placeholder=""  name="id" type="hidden" value="" aria-required="true">
    					</div>
										
						
					</fieldset>
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				<button type="submit" class="btn btn-success height-btn savepicture">Save</button>
				 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {

    var getfirst = '<?php echo $employees['id']; ?>';
    if(getfirst!=""){
      $('#AddPackage').modal('show');
       document.getElementById("id").value=getfirst;
       console.log('id',getfirst);
  	}
  	else{
  		
  		var getfirst = <?php echo Session::forget('pid'); ?>
  		document.getElementById("id").value=getfirst;
  		console.log('id',getfirst);
  		$('#AddPackage').modal('hide');

  	}
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/employee_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#employee-add-form").validate({
		
	});
});

function checkForm(form)
  {
  
    if(form.password.value != "" ) {
      if(form.password.value.length < 6) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least six characters!";
       
        form.password.focus();
        return false;
      }
           re = /[0-9]/;
      if(!re.test(form.password.value)) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least one number (0-9)!";
        form.password.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(form.password.value)) {
      	document.getElementById("password_strength").innerHTML = "Password must contain at least one lowercase letter (a-z)!";
         
        form.password.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(form.password.value)) {
      	document.getElementById("password_strength").innerHTML = "Password must contain at least one uppercase letter (A-Z)!";
                form.password.focus();
        return false;
      }
    } else {
      form.password.focus();
      return false;
    }

  }
</script>

@endpush
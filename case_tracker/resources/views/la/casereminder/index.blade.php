@extends("la.layouts.app")

@section("contentheader_title", "Reminder")
@section("contentheader_description", "Reminder listing")
@section("section", "Reminder")
@section("sub_section", "Listing")
@section("htmlheader_title", "Reminder Listing")

@section("headerElems")

@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<style type="text/css">

.modal-body  .box-body .form-group:first-child {
display:none;
    
}


</style>
<div class="box box-success">
    @la_access("Casereminder", "create")

    <div class="col-sm-12">
<ul class="nav navbar-nav">

		<li><a href="javascript:void(0)" class="topmenu-icon-text" data-toggle="modal" data-target="#usermodal"><img src="{{ asset('la-assets/img/newuser.png') }}" class="topmenu-icon"><br>New</a></li>
		<?php /*?><li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/openfile.png') }}" class="topmenu-icon"><br>Open</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/import.png') }}" class="topmenu-icon"><br>Import</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/export.png') }}" class="topmenu-icon"><br>Export</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/find.png') }}" class="topmenu-icon"><br>Find</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/followup.png') }}" class="topmenu-icon"><br>Follow Up</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/print-icon.png') }}" class="topmenu-icon"><br>Print</a></li>
		 <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/delete.png') }}" class="topmenu-icon"><br>Delete Case</a></li>
		<li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/exit.png') }}" class="topmenu-icon"><br>Exit</a></li> <?php */ ?>

		
	</ul>
</div>@endla_access
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			
			@foreach( $listing_cols as $col )
			@if($col=='id')
			<th>Serial No.</th>
			@else
					<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
					@endif
			@endforeach
			
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Casereminder", "create")
<div class="modal fade" id="usermodal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" style="width:900px">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Reminder</h4>
			</div>
			{!! Form::open(['action' => 'LA\CasereminderController@store', 'id' => 'caseremind-add-form']) !!}
			   {!! csrf_field() !!}

			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" name="">
                    <div class="form-group col-md-12 ">
						<label for="name">Patient:</label>
						<select class="form-control select2-hidden-accessible assitant_doctor" required="1" data-placeholder="Patient" rel="select2" name="patient_name" tabindex="-1" aria-hidden="true" aria-required="true"required>
							<?php foreach($casedata as $casedatalist){?>
							<option value="<?php echo  $casedatalist->first_name.' '.$casedatalist->middle_name.' '.$casedatalist->surname;?>"><?php echo $casedatalist->first_name.' '.$casedatalist->middle_name.' '.$casedatalist->surname;?></option>
							<?php } ?>
						
						</select>	
					</div>
					<!-- <div class="form-group col-md-12 ">
						<label for="mobile">Start Date :</label>
						<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
						    <input class="form-control dob" value="<?php echo date('d-m-Y'); ?>" type="text" name="start_date" readonly/>
						    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
						</div>
					</div>
					<div class="form-group col-md-12 ">
						<label for="mobile">End Date :</label>
						<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
						    <input class="form-control dob" value="<?php echo date('d-m-Y'); ?>" type="text" name="end_date" readonly/>
						    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
						</div>
					</div>
					<div class="form-group col-md-12">
						<label for="mobile" >Time</label>
						<div class="input-group time" id="timepicker">
			            	<input class="form-control" placeholder="HH:MM AM/PM"name="remind_time"/>
			            	<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
			          </div>
					</div>   
					<div class="form-group col-md-12">
							<label class="container-checkbox">Recursion
							  <input type="checkbox" class="courier_outstation" name="recursion" value="1" required>
							  <span class="checkmark"></span>
							</label>
					</div>   -->
					<div class="form-group col-md-6">
						<label for="mobile" >Remind On</label>
						<div id="datepicker" class="input-group date datepicker" data-date-format="mm/dd/yyyy">
						    <input class="form-control" type="text" name="start_date" readonly />
						    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
						</div>
					</div>
					<div class="form-group col-md-6" style="margin-top: 0px">
						<label for="mobile" >&nbsp;</label>
						<div class="input-group time" id="timepicker">
			            	<input class="form-control" placeholder="HH:MM AM/PM"name="remind_time"/>
			            	<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
			          </div>
					</div>
					<div class="form-group col-md-6">
						<label class="container-radio">10 Minutes
							  <input type="radio" name="remind_after" value="10 Minutes">
							  <span class="checkmark-radio"></span>
						</label>
					</div>
					<div class="form-group col-md-6">
						<label class="container-radio">1 Hour
							  <input type="radio" name="remind_after" value="1 Hour">
							  <span class="checkmark-radio"></span>
						</label>
					</div>
					<div class="form-group col-md-6">
						<label class="container-radio">Tommorrow 11 AM
							  <input type="radio" name="remind_after" value="Tommorrow 11 AM">
							  <span class="checkmark-radio"></span>
						</label>
					</div>
					<div class="form-group col-md-6">
						<label class="container-radio">Evening 5 PM
							  <input type="radio" name="remind_after" value="Evening 5 PM">
							  <span class="checkmark-radio"></span>
						</label>
					</div>
					<div class="form-group col-md-12">
						<label for="mobile">Heading :</label>
						<input class="form-control" placeholder="Heading"  name="heading" type="text" value="" aria-required="true"required>
					</div>
					<div class="form-group col-md-12">
						<label for="mobile" >Comments</label>
						<textarea class="form-control" placeholder="Comments"  name="comments" type="text" value="" aria-required="true" rows="5" required></textarea>
					</div>   
				
				</div>						
			</div>                  
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
				
			</div>
		</tr>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

<script>
$(function () {
    // $('').attr('name'='booking_date')
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        order: [[ 0, "desc" ]],
        ajax: "{{ url(config('laraadmin.adminRoute') . '/casereminder_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		
	});
	$("#caseremind-add-form").validate({
		
	});
	
 
    
});

</script>
<script type="text/javascript">
	$(document).ready(function(){
    $("#usermodal").modal('show');
});
    //setTimeout(function() { $( "booking-add-form" ).hide();},1000);
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
   
    $('#caseremind-add-form').find("input[name='start_date']").val(today);
     $('#caseremind-add-form').find("input[name='end_date']").val(today);

	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
 
</script>
<script>
	$('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});
</script>
@endpush
<style>
    .error{ display:none;}
</style>
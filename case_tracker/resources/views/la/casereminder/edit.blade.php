@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/medicalcases') }}">Medicalcase</a> :
@endsection
@section("contentheader_description", $casereminder->$view_col)
@section("section", "Medicalcase")
@section("section_url", url(config('laraadmin.adminRoute') . '/casereminder'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Medicalcase Edit : ".$casereminder->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="POST" action="http://smartops.co.in/managemyclinic/admin/casereminder/{{$casereminder->id }}" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					<input name="_method" type="hidden" value="PUT">
					 <input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group col-md-12 ">
								<label for="mobile">End Date :</label>
								<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
								    <input class="form-control dob" value="<?php echo date("d/m/yy", strtotime($casereminder->end_date)); ?>" type="text" name="end_date" />
								    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div>
							<div class="form-group col-md-12">
								<label for="mobile" >Time</label>
								<div class="input-group time" id="timepicker">
					            	<input class="form-control" placeholder="HH:MM AM/PM" name="remind_time" value="{{ $casereminder->remind_time }}" />
					            	<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
					          </div>
							</div>  
																	
					                    <br>
					<div class="form-group">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/casereminder">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

<script type="text/javascript">
    //setTimeout(function() { $( "booking-add-form" ).hide();},1000);
    
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
   
    $('#caseremind-add-form').find("input[name='start_date']").val(today);
     $('#caseremind-add-form').find("input[name='end_date']").val(today);

	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
	$("#timepicker").datetimepicker({
	    format: "LT",
	    icons: {
	      up: "fa fa-chevron-up",
	      down: "fa fa-chevron-down"
	    }
  });
 
</script>


<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
@endpush

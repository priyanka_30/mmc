@extends("la.layouts.app")

@section("contentheader_title", "Expenses")
@section("contentheader_description", "Expenses listing")
@section("section", "Expenses")
@section("sub_section", "Listing")
@section("htmlheader_title", "Expenses Listing")

@section("headerElems")
@la_access("Expenses", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Expenses</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if((Auth::user()->type != "Receptionist"))
<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
            <th>Id</th>
            <th>Date</th>
            <th>Head</th>
            <th>Amount</th>
            <th>Detail</th>
            <th>Actions</th>
        </tr>
        <tbody>
            <?php 
           
            foreach($expensedata as $data){ ?>
            <tr>
                <td><?php echo  $data->eid;; ?></td>
                 <td><?php echo $data->dateval; ?></td>
                <td><?php echo $data->expenseshead; ?></td>
                <td><?php echo $data->amount; ?></td>
                <td><?php echo $data->detail; ?></td>
                <td>
                <a href="http://smartops.co.in/managemyclinic/admin/expenses/<?php echo  $data->eid;; ?>/edit" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px; display: inline-block;"><i class="fa fa-edit"></i></a>
			{!! Form::open(['route' => [config('laraadmin.adminRoute') . '.expenses.destroy',$data->eid] , 'method' => 'delete','style' => 'display: inline-block;' ]) !!}
                <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>
                {!! Form::close() !!}
                </td>
            </tr>
        <?php  } ?>
           
        </tbody>
		</table>

	</div>
</div>
@endif
@la_access("Expenses", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Expenses</h4>
			</div>
			{!! Form::open(['action' => 'LA\ExpensesController@saveexpenses', 'id' => 'daycharges-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" name="">
                    <div class="form-group col-md-12 ">
						<label for="name">Expenses:</label>
						<select class="form-control select2-hidden-accessible" required="1" data-placeholder="Expenses" rel="select2" name="head" tabindex="-1" aria-hidden="true" aria-required="true"required>
							<?php foreach($expenseshead as $head){?>
                                    <option value="<?php echo $head->id;?>"><?php echo $head->expenseshead;?></option>
                                <?php } ?>
						</select>	
					</div>
					
					
					<div class="form-group col-md-12">
						<label for="mobile">Amount :</label>
						<input class="form-control" placeholder="Amount"  name="amount" type="text" value="" aria-required="true"required>
					</div>
					<div class="form-group col-md-12">
						<label for="mobile" >Detail</label>
						<textarea class="form-control" placeholder="Detail"  name="detail" type="text" value="" aria-required="true" rows="5" required></textarea>
					</div>   
				
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(document).ready(function(){
    $("#AddModal").modal('show');
});
$(function () {
	$("#example1").DataTable({
		processing: true,
      //  serverSide: true,
       // ajax: "{{ url(config('laraadmin.adminRoute') . '/expenses_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
@endpush
@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/expenses') }}">Expenses</a> :
@endsection

@section("section_url", url(config('laraadmin.adminRoute') . '/expenses'))
@section("sub_section", "Edit")

@section("htmlheader_title", "expenses Edit : ".$expenses->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/expenses/expensesupdate/') }}" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					<div class="box-body">
                  {{--  @la_form($module) --}}

                  <input type="hidden" name="id" value="<?php echo $expenses->id; ?>">
                    <div class="form-group col-md-12 ">
						<label for="name">Expenses:</label>
						<select class="form-control select2-hidden-accessible" required="1" data-placeholder="Expenses" rel="select2" name="head" tabindex="-1" aria-hidden="true" aria-required="true"required>
							<?php foreach($expenseshead as $head){?>
                                    <option value="<?php echo $head->id;?>" <?php if($head->id == $expenses->head){ echo "selected" ;} ?>><?php echo $head->expenseshead;?></option>
                                <?php } ?>
						</select>	
					</div>
					
					
					<div class="form-group col-md-12">
						<label for="mobile">Amount :</label>
						<input class="form-control" placeholder="Amount"  name="amount" type="text" value="<?php echo $expenses->amount ?>" aria-required="true"required>
					</div>
					<div class="form-group col-md-12">
						<label for="mobile" >Detail</label>
						<textarea class="form-control" placeholder="Detail"  name="detail" type="text" value="" aria-required="true" rows="5" required value="<?php echo $expenses->detail ?>"></textarea>
					</div> 
					
					<div class="form-group col-md-12">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/expenses">Cancel</a></button>
					</div>
					
										
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
	
</script>
@endpush

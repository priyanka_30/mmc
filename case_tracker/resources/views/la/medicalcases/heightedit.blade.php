@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/medicalcases') }}">Medicalcase</a> :
@endsection
@section("contentheader_description", $heightweight->$view_col)
@section("section", "Medicalcase")
@section("section_url", url(config('laraadmin.adminRoute') . '/medicalcases'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Medicalcase Edit : ".$heightweight->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="post" action="{{ url(config('laraadmin.adminRoute') . '/medicalcases/heightweightupdate/') }}" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <input type="hidden" name="id" value="{{ $heightweight->id }}">
													
							<div class="form-group col-md-6">
							<label for="Emailid">DOB :</label>
							<input class="form-control"  name="dob" type="text" value="{{ $heightweight->dob }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="phone">Height :</label>
							<input class="form-control"  name="height" type="text" value="{{ $heightweight->height }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="address">Weight :</label>
							<input class="form-control" name="weight" type="text" value="{{ $heightweight->weight }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="city">LMP :</label>
							<input class="form-control"required="1" name="lmp" type="text" value="{{ $heightweight->lmp }}" aria-required="true"></div>
							<div class="form-group col-md-12">
							<label for="pin">Blood Pressure:</label>
							<input class="form-control"required="1" name="blood_pressure" type="text" value="{{ $heightweight->blood_pressure }}" aria-required="true"></div>
																	
					                    <br>
					<div class="form-group col-md-12">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
@endpush

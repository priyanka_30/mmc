@extends("la.layouts.app")

@section("contentheader_title", "Cases")
@section("contentheader_description", "Cases listing")
@section("section", "Cases")
@section("sub_section", "Cases")
@section("htmlheader_title", "Cases Listing")

@section("headerElems")

@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<style>
    .highlight { background-color: grey; }
    .scroll-bar-wrap {
 /* width: 300px;*/
  position: relative;
 /* margin: 2em auto;*/
}
.scroll-box {
 /* width: 100%;*/
  height: 440px;
  overflow-y: scroll;
  position: relative;

}
.scroll-box::-webkit-scrollbar {
  width: .4em; 
}
.scroll-box::-webkit-scrollbar,
.scroll-box::-webkit-scrollbar-thumb {
  overflow:visible;
  border-radius: 4px;
}
.scroll-box::-webkit-scrollbar-thumb {
  background: rgba(0,0,0,.2); 
}
.cover-bar {
  position: absolute;
  background: #dedede;
  height: 100%;  
  top: 0;
  right: 0;
  width: .4em;
  -webkit-transition: all .5s;
  opacity: 1;
}
/* MAGIC HAPPENS HERE */
.scroll-bar-wrap:hover .cover-bar {
   opacity: 0;
  -webkit-transition: all .5s;
}

</style>
<div class="box box-success">
    @la_access("Medicalcases", "create")
    <div class="col-sm-12">
         <?php if(Auth::user()->type != "Doctor") { ?>
<ul class="nav navbar-nav">
        <li><a href="javascript:void(0)" class="topmenu-icon-text" id="openaddmodel"><img src="{{ asset('la-assets/img/newuser.png') }}" class="topmenu-icon"><br>New</a></li>
        <?php if(Auth::user()->type != "Receptionist" ) { ?>
      <!--   <li><a href="javascript:void(0)" onclick="opencase();" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/openfile.png') }}" class="topmenu-icon"><br>Open </a></li> -->
    <?php } ?>
         <?php /*?><li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/import.png') }}" class="topmenu-icon"><br>Import</a></li>
        <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/export.png') }}" class="topmenu-icon"><br>Export</a></li>
        <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/find.png') }}" class="topmenu-icon"><br>Find</a></li>
        <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/followup.png') }}" class="topmenu-icon"><br>Follow Up</a></li>
        <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/print-icon.png') }}" class="topmenu-icon"><br>Print</a></li>
        <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/delete.png') }}" class="topmenu-icon"><br>Delete Case</a></li>
        <li><a href="#" class="topmenu-icon-text"><img src="{{ asset('la-assets/img/exit.png') }}" class="topmenu-icon"><br>Exit</a></li> <?php */ ?>

        
    </ul>
    <?php }?>
</div>@endla_access
    <!--<div class="box-header"></div>-->
    <div class="box-body">
     <?php if(Auth::user()->type != "Doctor" && Auth::user()->type != "Receptionist") { ?>
      <!--   <div class="form-horizontal">
          <div class="control-group form-inline">
            <label for="name" class="col-md-2" style="width: 8%;margin-top: 2px;"> Min Date:</label>
            <div class="controls col-md-3" style="padding-left: 0px;">
              <input name="min" id="min" type="text" class="form-control input-sm" style="width: 100%;">
            </div>
          </div>
        </div>

        <div class="form-horizontal">
          <div class="control-group form-inline">
            <label for="name" class="col-md-2" style="width: 8%;margin-top: 2px;"> Max Date:</label>
            <div class="controls col-md-3" style="padding-left: 0px;">
              <input name="max" id="max" type="text" class="form-control input-sm" style="width: 100%;">
            </div>
          </div>
        </div> -->
    <?php }?>
    
        <table id="example1" class="table table-bordered">
        <thead>
        <tr class="success">
            @foreach( $listing_cols as $col )
                    <th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
            @endforeach
            @if($show_actions)
            <th>Actions</th>
            @endif
        </tr>
        </thead>
        <tbody>
            
        </tbody>
        </table>

        <input type="hidden" name="tdlink" id="tdlink">
       
        
    
</div>

@la_access("Medicalcases", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:900px">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Case</h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@create', 'id' => 'medicalcase-add-form-step1']) !!}
               {!! csrf_field() !!}

            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}
                    <div class="col-md-12" style="padding-left:0;padding-right:0">
                    <div class="form-group col-md-4">
                        <label for="mobile">RegID :</label>
                        <input class="form-control regid" placeholder="RegID" id="regid" name="regid" type="text" style="background: #fff;" readonly value="<?php echo $rgid; ?>" aria-required="true" required>
                    </div>
                    <div class="form-group col-md-4 ">
                        <label for="name">Assitsant Doctor:</label>
                        <select class="form-control assitant_doctor" id="assitant_doctor" name="assitant_doctor"  required onchange="priceget(this);">
                            <option value="">Select Doctor</option>
                            <?php foreach($doctors as $doctorslist){?>
                            <option value="<?php echo $doctorslist->id;?>" data-price="{{ $doctorslist->consultation_fee }}"><?php echo $doctorslist->name;?></option>
                            <?php } ?>
                        
                        </select>   
                        <input type="hidden" name="consultation_fee" id="consultation_fee" class="consultation_fee">
                    </div>
                    <div class="form-group col-md-4 ">
                        <label for="mobile">Date :</label>
                        <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                            <input class="form-control dob" value="<?php echo date('d-m-Y'); ?>" type="text" name="dob" readonly/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding-left:0;padding-right:0">
                    <div class="form-group col-md-2">
                        <label for="name">Title:</label>
                        <select class="form-control  title" required="1" name="title" required >
                             <option value="">Select</option>
                                <?php foreach($title as $titleval){?>
                                    <option value="<?php echo $titleval->id;?>" ><?php echo $titleval->title;?></option>
                                <?php } ?>
                        </select>   
                    </div>
                    <div class="form-group col-md-4">
                        <label for="mobile">Name :</label>
                        <input class="form-control first_name" placeholder="Name" id=first_name name="first_name" type="text" value="" aria-required="true" required>
                        <label id="first_name-error" class="error" for="first_name">This field is required.</label>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="mobile">Middle Name :</label>
                        <input class="form-control middle_name" placeholder="Middle Name"  name="middle_name" type="text" value="" aria-required="true">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="mobile">Surname :</label>
                        <input class="form-control surname" placeholder="Surname" id="surname"  name="surname" type="text" value="" aria-required="true"required>
                        <label id="surname-error" class="error" for="surname">This field is required.</label>
                    </div>
                </div>
                <div class="col-md-12"  style="padding-left:0;padding-right:0">
                     <div class="col-md-6">
                         <div class="col-md-12" style="padding-left:0;padding-right:0">
                             <div class="form-group col-md-6" style="padding-left:0">
                                 <label for="name">Gender:</label>
                                    <select class="form-control  gender"  name="gender"  >
                                         <option value="">Select</option>
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                        <option value="Other">Other</option>
                                    </select>
                             </div>
                             <div class="form-group col-md-6" style="padding-right:0">
                                 <label for="name">Marital Status:</label>
                                <select class="form-control status"  name="status" required>
                                    <option value="">Select</option>
                                <?php foreach($status as $statusval){?>
                                    <option value="<?php echo $statusval->id;?>" ><?php echo $statusval->status;?></option>
                                <?php } ?>
                                </select>
                             </div>
                         </div>
                         <div class="form-group col-md-12 checkbox-div">
                            <label for="name">Address:</label>
                            <input class="form-control address" placeholder="Address FlatNo./Building" id="address" name="address" type="text" value="" aria-required="true"required>
                            <label id="address-error" class="error" for="address">This field is required.</label>
                        </div>
                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <label for="mobile">Road :</label>
                            <input class="form-control road" placeholder="Road" id="road" name="road" type="text" value="" aria-required="true">
                        </div>
                        <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                            <div class="form-group col-md-6" style="padding-left:0">
                                <label for="mobile">Area/Sector :</label>
                                <input class="form-control area" placeholder="Area/Sector" id="area" name="area" type="text" value="" aria-required="true">
                            </div>
                            <div class="form-group col-md-6" style="padding-right:0">
                                <label for="mobile">City :</label>
                                <input class="form-control city" placeholder="City" id="city"  name="city" type="text" value="" aria-required="true"required>
                                <label id="city-error" class="error" for="city">This field is required.</label>
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                            <div class="form-group col-md-6" style="padding-left:0">
                                <label for="name">State:</label>
                                <?php $indian_all_states  = array (
 'AP' => 'Andhra Pradesh',
 'AR' => 'Arunachal Pradesh',
 'AS' => 'Assam',
 'BR' => 'Bihar',
 'CT' => 'Chhattisgarh',
 'GA' => 'Goa',
 'GJ' => 'Gujarat',
 'HR' => 'Haryana',
 'HP' => 'Himachal Pradesh',
 'JK' => 'Jammu & Kashmir',
 'JH' => 'Jharkhand',
 'KA' => 'Karnataka',
 'KL' => 'Kerala',
 'MP' => 'Madhya Pradesh',
 'MH' => 'Maharashtra',
 'MN' => 'Manipur',
 'ML' => 'Meghalaya',
 'MZ' => 'Mizoram',
 'NL' => 'Nagaland',
 'OR' => 'Odisha',
 'PB' => 'Punjab',
 'RJ' => 'Rajasthan',
 'SK' => 'Sikkim',
 'TN' => 'Tamil Nadu',
 'TR' => 'Tripura',
 'UK' => 'Uttarakhand',
 'UP' => 'Uttar Pradesh',
 'WB' => 'West Bengal',
 'AN' => 'Andaman & Nicobar',
 'CH' => 'Chandigarh',
 'DN' => 'Dadra and Nagar Haveli',
 'DD' => 'Daman & Diu',
 'DL' => 'Delhi',
 'LD' => 'Lakshadweep',
 'PY' => 'Puducherry',
); ?>
                                <select class="form-control state"  name="state">
                                    <?php foreach($indian_all_states as $key=>$value){?>
                                    <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                    <?php }?>
                                </select>   
                            </div>
                            <div class="form-group col-md-6" style="padding-right:0">
                                <label for="mobile">Pin :</label>
                                <input class="form-control pin" placeholder="Pin" id="pin" name="pin" type="text" value="" aria-required="true"required>
                                <label id="pin-error" class="error" for="pin">This field is required.</label>
                            </div>
                        </div>
                        
                        <div class="form-group col-md-12 checkbox-div">
                            <label class="container-checkbox">Courier Outstation
                              <input type="checkbox" class="courier_outstation" name="courier_outstation" value="1" required>
                              <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                            <label for="mobile">Email ID :</label>
                            <input class="form-control email" placeholder="Email ID" id="email"  name="email" type="text" value="" aria-required="true"required>
                            <label id="email-error" class="error" for="email">This field is required.</label>
                        </div>
                        <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                            <label for="name">Refrence:</label>
                               
                                <select class="form-control reference" name="reference" >
                                     <option value="">Select </option>
                                    <?php  foreach($refrence as $refrenceval){?>
                                        <option value="<?php echo $refrenceval->id;?>" ><?php echo $refrenceval->referencetype;?></option>
                                    <?php } ?>
                                </select>       
                        </div>
                        <!-- <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                            <label for="name">Patient ID:</label>
                            <select class="form-control  patientid" name="patientid" required>
                                   
                                <?php foreach($patients as $patientslist){?>
                                    <option value="<?php echo $patientslist->id;?>"><?php echo $patientslist->name;?></option>
                                <?php } ?>
                                    
                            </select>   
                        </div> -->
                        <div class="form-group col-md-12 religion-box" style="padding-left:0;padding-right:0">
                            <label for="name">Select Scheme:</label>
                            <select class="form-control scheme" name="scheme" required style="text-transform: capitalize;">
                                   
                                <?php foreach($packages as $packagelist){?>
                                    <option value="<?php echo $packagelist->id;?>"><?php echo $packagelist->name;?></option>
                                <?php } ?>
                                    
                            </select>   
                        </div>
                        <div class="col-md-12">&nbsp;</div>
                        
                        <!-- <div class="col-md-12" style="padding-left:0;padding-right:0">
                            <table class="table-bordered case-table">
                                <tr>
                                    <td style="padding:20px;">
                                        <button type="button" class="btn btn-primary case-btn">Add</button><br>
                                    </td>
                                    <td rowspan="2" style="padding:30px;">
                                        <img src="https://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028.jpg?s=80&amp;d=mm&amp;r=g" class="user-image" alt="User Image">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:20px;"> 
                                        <button type="button" class="btn btn-primary case-btn">Remove</button>
                                    </td>
                                </tr>
                            </table>
                        
                        </div> -->
                     </div>
                     <div class="col-md-1">&nbsp;</div>
                     <div class="col-md-5">
                         <div class="form-group col-md-12 religion-box">
                                <label for="mobile">Date of Birth :</label>
                                <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                    <input class="form-control date_of_birth" type="text" name="date_of_birth" id="dob" readonly required/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                <label id="date_of_birth-error" class="error" for="date_of_birth">This field is required.</label>
                         </div>
                         <div class="form-group col-md-12 religion-box">
                            <label for="name">Religion:</label>
                            <select class="form-control religion"  name="religion">
                                <option value="">Select</option>
                                <?php foreach($religion as $religionval){?>
                                    <option value="<?php echo $religionval->id;?>" ><?php echo $religionval->religion;?></option>
                                <?php } ?>
                            </select>   
                        </div>
                        <div class="form-group col-md-12 religion-box">
                            <label for="mobile">Phone No :</label>
                            <input class="form-control phone" placeholder="Enter Phone"  id="phone" name="phone" type="text" value=""required>

                            <label id="phone-error" class="error" for="phone">This field is required.</label>

                        </div>
                        <div class="form-group col-md-12 religion-box">
                            <label for="name">Occupation:</label>
                            <!-- <select class="form-control select2-hidden-accessible occupation" required="1" data-placeholder="Select Occupation" rel="select2" name="occupation" tabindex="-1" aria-hidden="true" aria-required="true">
                                <option value="Doctor">Doctor</option>
                                <option value="Agriculture">Agriculture</option>
                                <option value="Engineer">Engineer</option>
                                <option value="Beautician">Beautician</option>
                                <option value="Catering">Catering</option>
                                <option value="Lawer">Lawer</option>
                                <option value="Carpentary">Carpentary</option>
                                <option value="Other">Other</option>
                                <option value="None">None</option>
                            </select> -->   
                            <select class="form-control occupation"  name="occupation">
                                <option value="">Select</option>
                                <?php foreach($occupation as $occupationval){?>
                                    <option value="<?php echo $occupationval->id;?>" ><?php echo $occupationval->occupation;?></option>
                                <?php } ?>
                            </select>   
                            <!-- <input class="form-control occupation" placeholder="Enter Occupation" name="occupation" type="text" value=""required> -->
                        </div>
                        <div class="form-group col-md-12 religion-box" style="margin-top: 10px;">
                            <label for="mobile">Mobile 1 / Mother Number:</label>
                            <input class="form-control mobile1" placeholder="Enter Mobile1" data-rule-maxlength="20" id="mobile1" name="mobile1" type="text" value=""required>
                            <label id="mobile1-error" class="error" for="mobile1">This field is required.</label>
                        </div>
                        
                        <div class="form-group col-md-12 religion-box" style="margin-top: 10px;">
                            <label for="mobile">Landline with STD code  :</label>
                            <input class="form-control mobile2" placeholder="Enter Mobile2" data-rule-maxlength="20" id="mobile2" name="mobile2" type="text" value="">
                        </div>
                        <div class="form-group col-md-12" style="margin-top: 14px;">
                            <label for="mobile" class="radio-label"value="1"required>Send SMS:</label>
                            <label class="container-radio">Yes
                              <input type="radio" name="send_sms" class="send_sms" value="1">
                              <span class="checkmark-radio"></span>
                            </label>
                            <label class="container-radio">No
                              <input type="radio" name="send_sms" class="send_sms" value="0">
                              <span class="checkmark-radio"></span>
                            </label>
                            <input type="hidden" name="radio" id="radio" class="radio" value="0">
                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="mobile">Referred By</label>
                            <input class="form-control refered_by"  name="refered_by" id="refered_by" type="text" value="" aria-required="true">
                        </div>
                        <div class="form-group col-md-8">
                            <label for="mobile">&nbsp;</label>
                            <input class="form-control refered_search" name="refered_search"  type="text" value="" aria-required="true" readonly>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="mobile" class="radio-label"value="1"required>Send SMS:</label>
                            <label class="container-radio">Yes
                              <input type="radio" name="refered_sms" class="refered_sms" value="1">
                              <span class="checkmark-radio"></span>
                            </label>
                            <label class="container-radio">No
                              <input type="radio" name="refered_sms" class="refered_sms" value="0">
                              <span class="checkmark-radio"></span>
                            </label>
                            <input type="hidden" name="referedsms" id="referedsms" class="referedsms" value="0">
                            <button class="btn btn-default sendsms" type="button" id="sendsms">Send Sms</button>
                        </div>
                        
                     </div>
                </div>
                
                
                    
                    
                                        
                </div>
            </div>
                                
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="SAVESTEP1">Save</button>

                
                
            </div>
        </tr>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Next Detail Modal -->
<div class="modal fade" id="detailModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:900px">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@basicdetail', 'id' => 'medicalcase-add-formstep2']) !!}
            <div class="modal-body">
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="javascript:void(0)" class="SAVESTEP1">Basic Details</a></li>
                        <li><a href="javascript:void(0)" class="SAVESTEP2">Homoeo Details</a></li>
                        <li><a href="javascript:void(0)" class="SAVESTEP3">Other Details</a></li>
                    </ul>
                    
                      {{--  @la_form($module) --}}
                        <fieldset class="fieldset">
                            <div class="form-group col-md-4">
                                <label for="mobile">RegID :</label>
                                <input class="form-control regid" placeholder="RegID"  name="regid" type="text" value="" aria-required="true"required>
                                <input class="form-control consultation_fee" placeholder="RegID"  name="consultation_fee" type="hidden" value="" aria-required="true"required>

                            </div>
                            <div class="form-group col-md-4">
                                &nbsp;
                            </div>
                            <div class="form-group col-md-4">
                                <label for="mobile">Date :</label>
                                <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                    <input class="form-control current_date dob" type="text" name="current_date" value="<?php echo date('d-m-Y');?>" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-left: 0;padding-right: 0;">
                            <div class="form-group col-md-2">
                                <label for="name">Name:</label>
                                <select class="form-control select2-hidden-accessible title" required="1" data-placeholder="Title" rel="select2" name="title" tabindex="-1" aria-hidden="true" aria-required="true">
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Ms">Ms</option>
                                </select>   
                            </div>
                            <div class="form-group col-md-4">
                                <label for="mobile">&nbsp;</label>
                                <input class="form-control first_name" placeholder="Name"  name="first_name" type="text" value="" aria-required="true" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="mobile">&nbsp;</label>
                                <input class="form-control middle_name" placeholder="Middle Name"  name="middle_name" type="text" value="" aria-required="true">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="mobile">&nbsp;</label>
                                <input class="form-control surname" placeholder="Surname"  name="surname" type="text" value="" aria-required="true" readonly>
                            </div>
                        </div>
                            <div class="form-group col-md-6">
                                <label for="name">Gender:</label>
                                <select class="form-control select2-hidden-accessible gender" required="1" data-placeholder="gender" rel="select2" name="gender" tabindex="-1" aria-hidden="true" aria-required="true">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>   
                            </div>
                            <div class="form-group col-md-6">
                                    <label for="name">Religion:</label>
                                    <select class="form-control select2-hidden-accessible religion" required="1" data-placeholder="religion" rel="select2" name="religion" tabindex="-1" aria-hidden="true" aria-required="true">
                                        <option value="Hindu">Hindu</option>
                                        <option value="Muslim">Muslim</option>
                                        <option value="Christian">Christian</option>
                                        <option value="Sikh">Sikh</option>
                                        <option value="Other">Other</option>
                                    </select>   
                                </div>
                            <div class="form-group col-md-6">
                                <label for="name">Status:</label>
                                <select class="form-control select2-hidden-accessible status" required="1" data-placeholder="gender" rel="select2" name="status" tabindex="-1" aria-hidden="true" aria-required="true">
                                    <option value="Unmarried">Unmarried</option>
                                    <option value="Married">Married</option>
                                </select>   
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mobile">Date of Birth :</label>
                                <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                    <input class="form-control date_of_birth" type="text" name="date_of_birth" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name">Occupation:</label>
                                <select class="form-control select2-hidden-accessible occupation" required="1" data-placeholder="Select Occupation" rel="select2" name="occupation" tabindex="-1" aria-hidden="true" aria-required="true">
                                    <option value="Doctor">Doctor</option>
                                    <option value="Agriculture">Agriculture</option>
                                    <option value="Engineer">Engineer</option>
                                    <option value="Beautician">Beautician</option>
                                    <option value="Catering">Catering</option>
                                    <option value="Lawer">Lawer</option>
                                    <option value="Carpentary">Carpentary</option>
                                    <option value="Other">Other</option>
                                    <option value="None">None</option>
                                </select>   
                            </div>
                        </fieldset>
                        <fieldset class="fieldset">
                            <div class="form-group col-md-12" style="padding-left: 0;padding-right: 0;">
                                <div class="form-group col-md-6" style="padding-left: 0;padding-right: 0;">
                                        <div class="form-group col-md-12">
                                            <label for="name">Address:</label>
                                            <input class="form-control address" placeholder="Address FlatNo./Building"  name="address" type="text" value="" aria-required="true" required readonly> 
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="mobile">Road :</label>
                                            <input class="form-control road" placeholder="Road"  name="road" type="text" value="" aria-required="true">
                                        </div>
                                        <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                                            <div class="form-group col-md-6">
                                                <label for="mobile">Area/Sector :</label>
                                                <input class="form-control area" placeholder="Area/Sector"  name="area" type="text" value="" aria-required="true">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="mobile">City :</label>
                                                <input class="form-control city" placeholder="City"  name="city" type="text" value="" aria-required="true"required readonly>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                                            <div class="form-group col-md-6">
                                                <label for="name">State:</label>
                                                <select class="form-control select2-hidden-accessible state" required="1" data-placeholder="state" rel="select2" name="state" tabindex="-1" aria-hidden="true" aria-required="true">
                                                    <?php foreach($indian_all_states as $key=>$value){?>
                                                        <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                                    <?php }?>
                                                </select>   
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="mobile">Pin :</label>
                                                <input class="form-control pin" placeholder="Pin"  name="pin" type="text" value="" aria-required="true" required readonly>
                                            </div>
                                        </div>
                                </div>
                                <div class="form-group col-md-6">
                                        <table class="table-bordered case-table" style="margin-left:50px;">
                                            <div style="padding:30px;">
                                            <tr>
                                                <td>
                                                    <button type="button" class="btn btn-primary case-btn" style="margin: 5px;">Add</button><br>
                                                </td>
                                                <td rowspan="2">
                                                    <img src="https://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028.jpg?s=80&amp;d=mm&amp;r=g" class="user-image case-img" alt="User Image">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <button type="button" class="btn btn-primary case-btn" style="margin: 5px;">Remove</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="form-group col-md-12 checkbox-form">
                                                        <label class="container-checkbox">Courier Outstation
                                                          <input type="checkbox" name="courieroutstation" value="1"required>
                                                          <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                    
                                                </td>
                                            </tr>
                                            </div>
                                        </table>
                                </div>
                            
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mobile">Phone No :</label>
                                <input class="form-control phone" placeholder="Enter Phone" data-rule-maxlength="20" name="phone_no" type="text" value="" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mobile">Email ID :</label>
                                <input class="form-control email" placeholder="Email ID"  name="email_id" type="text" value="" aria-required="true"required readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mobile">Mobile 1 :</label>
                                <input class="form-control mobile1" placeholder="Enter Mobile1" data-rule-maxlength="20" name="mobile1" type="text" value=""required readonly>
                            </div>
                            
                            <div class="form-group col-md-6">
                                <label for="mobile">Mobile 2 :</label>
                                <input class="form-control mobile2" placeholder="Enter Mobile2" data-rule-maxlength="20" name="mobile2" type="text" value="">
                            </div>
                        </fieldset>
                        
                        
                    <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success SAVESTEP2" id="SAVESTEP2">Next</button>
                
                
            </div>
            {!! Form::close() !!}           
                
              
            
                               
        </div>
    </div>
</div>
</div>
</div>


<div class="modal fade" id="detailHomeModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:900px">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li><a href="javascript:void(0)" class="SAVESTEP1">Basic Details</a></li>
                        <li class="active"><a href="javascript:void(0)" class="SAVESTEP2">Homoeo Details</a></li>
                        <li><a href="javascript:void(0)" class="SAVESTEP3">Other Details</a></li>
                    </ul>
                    
                    
                {!! Form::open(['action' => 'LA\MedicalcasesController@homeodetail', 'id' => 'medicalcase-add-form-step3']) !!}
               {!! csrf_field() !!} 
                   {{--  @la_form($module) --}}
                        <fieldset class="fieldset">
                            <input class="form-control consultation_fee"  name="total_charges" type="hidden" >
                                            <div class="form-group col-md-12" >
                                                <label for="mobile">Diagnosis :</label>
                                                <?php $disease_all  = array (
                                                     'Allergies' => 'Allergies',
                                                     'Asthma' => 'Asthma',
                                                     'Cancer' => 'Cancer',
                                                     'Celiac Disease' => 'Celiac Disease',
                                                     'Heart Disease' => 'Heart Disease',
                                                     'Infectious Diseases' => 'Infectious Diseases',
                                                     'Liver Disease' => 'Liver Disease',
                                                    ); ?>
                                                <select class="form-control select2-hidden-accessible state" required="1" data-placeholder="diagnosis" rel="select2" name="diagnosis" tabindex="-1" aria-hidden="true" aria-required="true" required>
                                                    <?php foreach($disease_all as $key=>$value){?>
                                                    <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                                    <?php }?>
                                                </select>   
                                                <label id="diagnosis-error" class="error" for="diagnosis">This field is required.</label>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="mobile">Complaint Intesity :</label>
                                                <textarea class="form-control complaint_intesity" placeholder="Complaint Intesity"  name="complaint_intesity" type="text" value="" aria-required="true" rows="12"required></textarea>
                                                <label id="complaint_intesity-error" class="error" for="complaint_intesity">This field is required.</label>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label for="mobile">Medication Taking :</label>
                                                <div class="multi-field-wrapper">
                                                  <div class="multi-fields">
                                                    <div class="multi-field">
                                                        <div class="form-group col-md-2" style="padding: 0;">
                                                            <input type="text" name="" class="form-control" placeholder="1">
                                                        </div>
                                                        <div class="form-group col-md-2" style="padding: 0;">
                                                            <select class="form-control medication_taking" data-placeholder="Name"  name="medication_taking" id="medication_taking" required onchange="medicine_price_get(this);" >
                                                                <option disabled selected>Select Name</option>
                                                                        <?php foreach($stocks as $stname){?>
                                                                        <option value="{{ $stname->description }}" data-price="{{ $stname->description }}">{{ $stname->description }}</option>
                                                                        <?php }?>
                                                        </select>
                                                        <input type="hidden" name="medicine_price" id="medicine_price" class="medicine_price">
                                                        </div>
                                                        <div class="form-group col-md-2" style="padding: 0;">
                                                            <?php $medicine_time  = array (
                                                                 'Once' => 'Once',
                                                                 'Twice' => 'Twice',
                                                                 'Thrice' => 'Thrice',
                                                                 'Bed Time' => 'Bed Time',
                                                                 'Empty Stomach' => 'Empty Stomach',
                                                                ); ?>
                                                            <select class="form-control select2-hidden-accessible state" required="1" data-placeholder="diagnosis" rel="select2" name="medicine_name" tabindex="-1" aria-hidden="true" aria-required="true" required>
                                                                <?php foreach($medicine_time as $key=>$value){?>
                                                                <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-2" style="padding: 0;">
                                                            <?php $for_disease  = array (
                                                                 'Thyroid' => 'Thyroid',
                                                                 'Hypertension' => 'Hypertension',
                                                                 'Diabetes' => 'Diabetes',
                                                                ); ?>
                                                            <select class="form-control select2-hidden-accessible state" required="1" data-placeholder="diagnosis" rel="select2" name="medicine_name" tabindex="-1" aria-hidden="true" aria-required="true" required>
                                                                <?php foreach($for_disease as $key=>$value){?>
                                                                <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-2" >
                                                            <button type="button" class="btn btn-success add-field">+</button>
                                                        </div>
                                                        <div class="form-group col-md-1" style="padding: 0;" >
                                                            <button type="button" class="btn btn-danger remove-field">-</button>
                                                        </div>
                                                    </div>
                                                  </div>
                                                
                                              </div>
                                                <!-- <textarea class="form-control medication_taking" placeholder="Medication Taking"  name="medication_taking" type="text" value="" aria-required="true" rows="5"required></textarea>
                                                <label id="medication_taking-error" class="error" for="medication_taking">This field is required.</label> -->
                                            </div>
                                    
                                            <div class="form-group col-md-12">
                                                <label for="mobile">Investigation :</label>
                                                <textarea class="form-control investigation" placeholder="Investigation" name="investigation" type="text" value="" aria-required="true" rows="8"></textarea>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label for="name">Case Taken By:</label>
                                                <select class="form-control select2-hidden-accessible assitant_doctor" required="1" data-placeholder="Select Case Taken" rel="select2" name="case_taken" tabindex="-1" aria-hidden="true" aria-required="true"required>
                                                    <?php foreach($doctors as $doctorslist){?>
                                                        <option value="<?php echo $doctorslist->name;?>"><?php echo $doctorslist->name;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="name">Criteria:</label>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label class="container-checkbox">Skin
                                                  <input type="checkbox" value="skin" name="Criteria[]" class="criteria">
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="container-checkbox">Disability Disorder
                                                  <input type="checkbox" value="disability_order" name="Criteria[]" class="criteria">
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="container-checkbox">PCOD
                                                  <input type="checkbox" value="pcod" name="Criteria[]" class="criteria">
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="container-checkbox">Thyroid
                                                  <input type="checkbox" value="thyroid" name="Criteria[]" class="criteria">
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="container-checkbox">Lifestyle Disorder
                                                  <input type="checkbox" value="lifestyle_disorder" name="Criteria[]" class="criteria">
                                                  <span class="checkmark"></span>
                                                </label>
                                            </div>
                            
                                        </fieldset>
                                        <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-success SAVESTEP3" id="SAVESTEP3">Next</button>
                    
                </div>
                
              
                {!! Form::close() !!} 
                               
        </div>
    </div>
</div>
</div>
</div>

<!-- Full Case Detail -->
<div class="modal fade" id="FullCaseDetail" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@store', 'id' => 'medicalcase-add-form']) !!}
            <div class="modal-body">
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li><a href="javascript:void(0)" class="SAVESTEP1">Basic Details</a></li>
                        <li><a href="javascript:void(0)" class="SAVESTEP2">Homoeo Details</a></li>
                        <li class="active"><a href="javascript:void(0)" class="SAVESTEP3">Other Details</a></li>
                    </ul>
                  {{--  @la_form($module) --}}
                  <fieldset class="fieldset">
                      <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                          <div class="col-md-3">
                                <div class="form-group col-md-12" class="patient-info-td">
                                    <label for="mobile" class="patient-info-label-detail"></label>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="mobile" class="patient-info-label-scheme"></label>
                                </div>
                                <div class="followupdataentery">
                                    
                                </div>
                                
                                
                          </div>
                          <div class="col-md-7" style="background: #ffffff;padding: 20px 10px;">
                                <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                                    <div class="col-md-2">
                                        <label class="container-radio">Clinic
                                          <input type="radio" name="couriertype" value="Clinic">
                                          <span class="checkmark-radio"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="container-radio">Courier
                                          <input type="radio" name="couriertype" value="Courier">
                                          <span class="checkmark-radio"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="container-radio">Pickup
                                          <input type="radio" name="couriertype" value="Pickup">
                                          <span class="checkmark-radio"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                                    <div class="form-group col-md-3">
                                            <label for="name">Remedy:</label>
                                            <select class="form-control select2-hidden-accessible rxremedy" rel="select2" name="remedy" tabindex="-1" aria-hidden="true" aria-required="true">
                                                <option value="Staph">Staph</option>
                                                <option value="Chin">Chin</option>
                                            </select>   
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="name">Potency:</label>
                                            <select class="form-control select2-hidden-accessible rxpotency" rel="select2" name="potency" tabindex="-1" aria-hidden="true" aria-required="true">
                                                <option value="1M">1 M</option>
                                                <option value="2M">2 M</option>
                                                <option value="3M">3 M</option>
                                                <option value="4M">4 M</option>
                                                <option value="5M">5 M</option>
                                                <option value="6M">6 M</option>
                                                <option value="7M">7 M</option>
                                                <option value="8M">8 M</option>
                                                <option value="9M">9 M</option>
                                                <option value="10M">10 M</option>
                                            </select>   
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="name">Frequency:</label>
                                            <select class="form-control select2-hidden-accessible rxfrequency" rel="select2" name="frequency" tabindex="-1" aria-hidden="true" aria-required="true">
                                            
                                                <option value="1 dose">1 dose</option>
                                                <option value="2 dose">2 dose</option>
                                                <option value="3 dose">3 dose</option>
                                            </select>   
                                        </div>  
                                        <div class="form-group col-md-3">
                                            <label for="name">Days:</label>
                                            <select class="form-control select2-hidden-accessible rxdays" rel="select2" name="days" tabindex="-1" aria-hidden="true" aria-required="true">
                                            
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="10">10</option>
                                                <option value="15">15</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                            </select>   
                                        </div>
                            </div>
                                <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                                    <div class="form-group col-md-12">
                                        <label for="mobile">Prescription</label>
                                        <textarea class="form-control rxprescription" placeholder="Prescription"  name="prescription" type="text" value="" aria-required="true" rows="2"></textarea>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <button type="button" class="btn btn-default rxdataadd">Rx</button>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-12">
                                    <table class="table table-bordered tb-bg rxdatavaltable">
                                        <tr>
                                            <th>Remedy</th>
                                            <th>Potency</th>
                                            <th>Frequency</th>
                                            <th>Days</th>
                                            <th>Prescription</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        
                                    </table>
                                </div>
                                
                          </div>
                          <div class="col-md-2">
                              <div class="form-group col-md-12">
                                    <label for="mobile">Diagnosis</label>
                                    <p id="diagnosistest"></p> 
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="mobile">Complaint Intensity</label>
                                    <p id="domplainttest"></p> 
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="mobile">Medication Taking</label>
                                    <p id="medicationtest"></p> 
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="mobile">Investigation</label>
                                    <p id="investigationtest"></p> 
                                </div>
                          </div>
                      </div>
                    <table>
                         <tr>
                            <td colspan="3" valign="top">
                                <div class="form-group col-md-12">
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/icon1.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/brain.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/icon2.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/icon3.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/favorite-icon.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/injection.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/medicine.png') }}" class="case-detail-icon">
                                </a>
                                <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#ExaminationReport">
                                    <img src="{{ asset('la-assets/img/stethoscope-icon.png') }}" class="case-detail-icon">
                                </a>
                                <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#InvestigationReport">
                                    <img src="{{ asset('la-assets/img/microscope.png') }}" class="case-detail-icon">
                                </a>
                                <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#HeightWeightEntry">
                                    <img src="{{ asset('la-assets/img/weight-scale.png') }}" class="case-detail-icon">
                                </a>
                                <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#CommunicationDetail">
                                    <img src="{{ asset('la-assets/img/phone1.png') }}" class="case-detail-icon">
                                </a>
                                <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#AddReminder">
                                    <img src="{{ asset('la-assets/img/alarm-clock.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/statsbar.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/phone2.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/filmmedia.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/user-group.png') }}" class="case-detail-icon">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('la-assets/img/delete-cross.png') }}" class="case-detail-icon">
                                </a>
                                
                                </div>
                            </td>
                            
                        </tr>
                    </table>
                    </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success completetata">Complete</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Add Height Weight -->
<div class="modal fade" id="HeightWeightEntry" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Height Weight Entry</h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@saveHeightWeights', 'id' => 'medicalcase-add-form-step4']) !!}
               {!! csrf_field() !!} 
                   {{--  @la_form($module) --}}
            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}
                  
                   <?php 
                    // echo $iddata1['dob']."<br>";
                    //     $date1 = $iddata1['dob'];
                    //     $date2 = date("Y-m-d");

                    //     $ts1 = strtotime($date1);
                    //     $ts2 = strtotime($date2);

                    //     $year1 = date('Y', $ts1);
                    //     $year2 = date('Y', $ts2);

                    //     $month1 = date('m', $ts1);
                    //     $month2 = date('m', $ts2);

                    //     $diff1 = (($year2 - $year1) * 12) + ($month2 - $month1);
                    //     echo $diff1."d<br>";

                    //     $string = $iddata1['gender'];
 
                    //     //Get the first character.
                    //     $firstCharacter = $string[0];
                    //     echo 't'.$firstCharacter;
                   ?>
                  <div class="form-group col-md-4 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control regid" placeholder="RegID"  name="regid" type="text" value="" aria-required="true"required>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="form-group col-md-8" style="padding-left:0">
                            <table>
                                <tr>
                                    <td>
                                        <table class="table table-bordered tb-bg heightweighttabledata">
                                            <tr>
                                                <th>Date</th>
                                                <th>Height</th>
                                                <th>Weight</th>
                                                <th>BP</th>
                                                <th>LMP</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                    
                                </tr>
                                
                            </table>
                        </div>
                        <div class="form-group col-md-4" style="padding-left:0;padding-right:0">
                                <table>
                                    <tr>
                                        <td>
                                            
                                            <div class="form-group col-md-12">
                                                <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                    <input class="form-control dateval" type="text" name="dateval" readonly value="<?php echo date('d-m-Y');?>"/>
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group col-md-12">
                                                <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                                                    <label for="mobile">Height:</label>
                                                    <input class="form-control height" placeholder="Height"  name="height" type="text" value="" aria-required="true">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group col-md-12">
                                                <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                                                    <label for="mobile">Weight:</label>
                                                    <input class="form-control weight" placeholder="Weight"  name="weight" type="text" value="" aria-required="true">
                                                </div>
                                            </div>
                                            <input class="form-control dateval1" type="hidden" name="dateval" readonly />
                                            <input class="form-control height1" type="hidden" name="dateval" readonly />
                                            <input class="form-control weight1" type="hidden" name="dateval" readonly />
                                            <input class="form-control blood_pressure1" type="hidden" name="dateval" readonly />
                                            <input class="form-control lmpdata1" type="hidden" name="dateval" readonly />
                                             
                                        </td>
                                    </tr>
                                </table>
                        </div>
                    </div>
                    <div class="col-md-12"> 
                    <fieldset class="fieldset">
                        <div class="form-group col-md-6">
                            <label for="mobile">Blood Pressure :</label><br>
                            <input class="form-control blood_pressure" placeholder=""  name="blood_pressure" type="text" value="" aria-required="true">
                            <input class="form-control blood_pressure" placeholder=""  name="blood_pressure" type="text" value="" aria-required="true">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="mobile">LMP :</label>
                            <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                <input class="form-control lmpdata" type="text" name="lmpdata" readonly value="<?php echo date('d-m-Y');?>"/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            </div>
            <div class="modal-footer height-entry-footer">
                <button type="button" class="btn btn-success height-btn savefollowheightweight">Save Copy To Followup</button>
                <button type="button" class="btn btn-success height-btn saveheightweight">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Add Communication Detail -->
<div class="modal fade" id="CommunicationDetail" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Communication Detail</h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@communicationdetail', 'id' => 'medicalcase-add-form5']) !!}
            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}
                    <div class="form-group col-md-8" class="patient-info-td">
                        <label for="mobile" class="patient-info-label-detail">11610 Anayaa Mukesh Kumar Female/5 Yrs 6 Months 5 Days</label>
                    </div>
                    <div class="form-group col-md-4" class="patient-info-td">
                        <label for="mobile" >Comments</label>
                    </div>
                    <div class="form-group col-md-12">
                        <table>
                            <tr>
                                <td rowspan="3" valign="top">
                                    <table class="table table-bordered tb-bg">
                                        <tr>
                                            <th>Date</th>
                                            <th>Comments</th>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name=""> 30 Jan 2020</td>
                                            <td>better cold and congestion hitting o..</td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name=""> 26 Jan 2020</td>
                                            <td>Boils on chest n back from 3-4 da.. </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" name=""> 20 Jan 2020</td>
                                            <td>having nasal discharge..adv to..</td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <div class="form-group col-md-12">
                                        <textarea class="form-control" placeholder="Complaint Intesity"  name="complaint_intesity" type="text" value="" aria-required="true" rows="4" required></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group col-md-12">
                                        <textarea class="form-control" placeholder="Complaint Intesity"  name="complaint_intesity1" type="text" value="" aria-required="true" rows="4"required></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group col-md-12">
                                        <textarea class="form-control" placeholder="Complaint Intesity"  name="complaint_intesity2" type="text" value="" aria-required="true" rows="4" required></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"> 
                                    <button type="button" class="btn btn-success height-btn">Add</button>
                                    <button type="button" class="btn btn-success height-btn">Delete</button>
                                    <button type="button" class="btn btn-success height-btn">Send to Excel</button>
                                </td>
                                <td align="center">
                                    <button type="save" class="btn btn-success height-btn">Save</button>
                                </td>
                            </tr>
                        </table>
                    </div>  
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<div class="modal fade" id="AddPackage" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content medicalcase_model">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Packages</h4>
                </div>
                {!! Form::open(['action' => 'LA\MedicalcasesController@packagesave', 'class' => 'packageform']) !!}
                <div class="modal-body">
                    <div class="box-body">
                      {{--  @la_form($module) --}}
                      
                        <fieldset class="examin-fieldset">
                            <div class="form-group col-md-12 " style="margin-top: 14px;">
                                <label for="mobile" >Reg ID</label>
                                 <input class="form-control" id="case_id" placeholder=""  name="case_id"  value="" aria-required="true" readonly style="width: 22%;"> 
                                 <input type="hidden" class="regcasename">
                                 <input type="hidden" class="regmobile">
                            </div>
                            <div class="form-group col-md-4 " style="margin-top: 14px;">
                                <span class="regname" style="font-weight: bold;"></span> 
                            </div>
                            <div class="form-group col-md-4 " style="margin-top: 14px;">
                                <span class="packagename" style="font-weight: bold;"></span> 
                            </div>
                            <div class="form-group col-md-4 " style="margin-top: 14px;">
                                 <span class="pacakge_expiry" style="font-weight: bold;"></span>
                            </div>
                                 <div class="form-group col-md-6" style="margin-top: 14px;">
                                    
                                    <label class="container-radio">From Today
                                      <input type="radio" name="package_start" class="package_start" value="<?php echo date("Y-m-d"); ?>">
                                      <span class="checkmark-radio"></span>
                                    </label>
                                    <label class="container-radio" id="expirypack" style="display: none;">From Expiry
                                      <input type="radio" name="package_start" class="package_start" >
                                      <span class="checkmark-radio"></span>
                                    </label>
                                   
                                </div>
                               
                                <div class="form-horizontal ">
                                  <div class="control-group form-inline">
                                    <label for="name" class="col-md-2 login-label" style="margin-top: 14px;padding-right: 0px;" > From</label>
                                    <div class="controls col-md-4" style="padding-left: 0px;margin-top: 14px;">
                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control package_start_date" type="text" name="package_start_date" readonly value=""/>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group col-md-12 " >
                                <label for="name">Select Package:</label>
                                <select class="form-control select2-hidden-accessible scheme" required="1" data-placeholder="Select Scheme" rel="select2" name="packages" id="packages" tabindex="-1" aria-hidden="true" aria-required="true"required onchange="getprice(this);">
                                        <option value="Other">Other</option>
                                    <?php foreach($packages as $packagelist){?>
                                        <option value="<?php echo $packagelist->id;?>" data-price="<?php echo $packagelist->color;?>" data-months="<?php echo $packagelist->tags;?>"><?php echo $packagelist->name;?> <?php echo " - ". $packagelist->tags;?>     <?php echo " -  Rs.". $packagelist->color;?></option>
                                    <?php } ?>
                                        
                                </select>   
                              <input type="hidden" class="packagemonths">
                            </div>
                            <div class="form-group col-md-6 " >
                                <span class="packcharges"></span>
                            </div>

                            
                        <!--     <div class="col-md-12 religion-box">
                                 <label>Pacakge Details</label> <br>
                                <span class="packagename"></span>
                            </div>
                            <div class="col-md-12 religion-box">
                                <span class="pacakgestart"></span>
                            </div>
                            <div class="col-md-12 religion-box">
                                <span class="pacakge_expiry"></span>
                            </div>
                            <div class="col-md-12 religion-box">
                                <span class="expmsg" style="color: red;"></span>
                            </div> -->
                                            
                            
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer height-entry-footer">
                    <button type="button" class="btn btn-success height-btn savepicture">Save</button>
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddBill" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content medicalcase_model">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Generate Bill</h4>
                </div>
                
                <div class="modal-body">
                    <div class="box-body">
                      {{--  @la_form($module) --}}
                        
                        <fieldset class="examin-fieldset">
                            <div class="form-group col-md-6 religion-box" >
                                <label>Bill date:</label>
                                <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                    <input class="form-control billdate" value="<?php echo date('d-m-Y'); ?>" type="text" name="billdate" readonly/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    
                                </div>
                                <input type="hidden" class="bill_case_id">
                            </div>
                            <div class="form-group col-md-6 religion-box" >
                                <label>&nbsp;</label>
                                <button type="button" class="btn btn-success height-btn getbill" style="margin-top: 26px;">Save</button>
                            </div>
                                
                            <div class="billdiv" style="display: none;">
                            {!! Form::open(['action' => 'LA\MedicalcasesController@generatePDF' , 'target'=>'_blank']) !!}
                             <div class="form-group col-md-6 religion-box" >
                             <span class="error_msg"></span>
                               <input type="hidden" class="form-control case_name" readonly name="case_name">
                               <input type="hidden" class="case_u_id" name="case_u_id">
                               <input type="hidden" class="bill_date" name="bill_date">
                               <input type="hidden" class="form-control rece_amount" readonly name="rece_amount">
                               <input type="hidden" class="fromdate" name="fromdate">
                               <input type="hidden" class="todate" name="todate">
                               <input type="hidden" class="billno" name="billno">
                            </div>   
                            
                             <div class="form-group col-md-12 religion-box divbill" >
                                <button type="submit" class="btn btn-success height-btn generatebill">Print Bill</button>
                            </div> 
                              {!! Form::close() !!}
                            </div>  
                                            
                            
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer height-entry-footer">
                    
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
              
            </div>
        </div>
    </div>


<div class="modal fade" id="AddHeightWeight" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Height Weight Entry</h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@saveCaseHeightWeights', 'id' => 'medicalcase-add-form5']) !!}
               
                   {{--  @la_form($module) --}}
            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}

                  <div class="form-group col-md-4 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control reg_id" placeholder="RegID" readonly  name="regid" type="text" value="" aria-required="true"required>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="form-group col-md-8" style="padding-left:0">
                            <table>
                                <tr>
                                    <td>
                                        <table class="table table-bordered tb-bg heightweighttabledata_ad">
                                            <tr>
                                                <th>Date</th>
                                                <th>Height</th>
                                                <th>Weight</th>
                                                <!-- <th>BP</th>
                                                <th>BP</th> -->
                                                <th>LMP</th>
                                             
                                            </tr>
                                            <tbody class="height-body">
                                                
                                            </tbody>
                                            
                                        </table>
                                    </td>
                                    
                                </tr>
                                
                            </table>
                        </div>
                        <div class="form-group col-md-4" style="padding-left:0;padding-right:0">
                                  <?php 
                                  $ht = "1";
                                  $wt = "50"; ?>
                                <table>
                                   
                                    <tr>
                                        <td>
                                                    <input class="form-control datevalht" type="hidden" name="dateval" readonly value=""/>
                                                    <input class="form-control datevalht1" type="hidden" value=""/>
                                                    <input type="hidden" class="randht" name="rand" value="">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="form-group col-md-12">
                                                <div class="form-group col-md-7" style="padding-left:0;padding-right:0">
                                                    <label for="mobile">Height:</label>
                                                    <input class="form-control height_ad" placeholder="Height"  name="height" type="text" value="" aria-required="true">
                                                    
                                                    
                                                </div>
                                                <div class="form-group col-md-5" style="padding-right: 0;padding-left: 3px;margin-top: 32px;color: red;">
                                                    <span class="ht"></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group col-md-12">
                                                <div class="form-group col-md-7" style="padding-left:0;padding-right:0">
                                                    <label for="mobile">Weight:</label>
                                                    <input class="form-control weight_ad" placeholder="Weight"  name="weight" type="text" value="" aria-required="true">
                                                </div>
                                                <div class="form-group col-md-5" style="padding-right: 0;padding-left: 3px;margin-top: 32px;color: red;">
                                                    <span class="wt"></span>
                                                </div>
                                            </div>
                                          
                                             
                                        </td>
                                    </tr>
                                </table>
                            
                        </div>
                    </div>
                    <div class="col-md-12"> 
                    
                            <fieldset class="fieldset lmpdata" style="padding-top: 6px;">
                                <div class="form-group col-md-6" style="margin-top: 0px;margin-bottom: 10px;">
                                    <label for="mobile">LMP :</label>
                                    <div id="datepicker" class="input-group date datepickerheight" data-date-format="dd-mm-yyyy">
                                        <input class="form-control lmp" type="text" name="lmpdata" readonly value=""/>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </fieldset>
               
                    
                </div>
              
            </div>
            </div>
            <div class="modal-footer height-entry-footer">
                <button type="button" class="btn btn-success height-btn saveweightheight">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="AddFamily" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Family</h4>
            </div>
            {!! Form::open(['action' => 'LA\DaychargesController@store', 'id' => 'daycharges-add-form']) !!}
            <div class="modal-body">
                <div class="box-body">
                  
                 <fieldset class="fieldset" >
                    <div class="col-md-12" style="padding: 0px;">
                         <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px; "> Name </label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 20%; padding-left: 0px;">
                              <input  type="text" class="search_name" name="search_name" class="form-control" autocomplete="off">
                            </div>
                          </div>
                        </div>

                        <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px;"> Surname</label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 20%;">
                              <input  type="text" class="search_surname" name="search_surname" class="form-control" autocomplete="off">
                            </div>
                          </div>
                        </div>

                        <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px;"> RegId</label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 32%;">
                              <input  type="text" class="search_regid"  name="search_regid" class="form-control" autocomplete="off">
                              <button type="button" class="btn btn-success searchbtn" style="float: right;margin-top: -4px;" >Search</button>
                            </div>
                          </div>
                        </div>
                        
                    </div>
                </fieldset>
                        <div class="col-md-12"  style="padding-left:0;padding-right:0; margin-top: 30px;">
                   

                     <div class="col-md-4" style="padding: 0px;">
                        

                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <table class="table table-bordered tb-bg familytable">
                                        <tr>
                                            <th>RegID</th>
                                            <th>Name</th>
                                        </tr>
                                        <tbody class="search-body">
                                            
                                        </tbody>
                                    </table>
                        </div>
                        
                        <div class="col-md-12">&nbsp;</div>
                        
                        
                     </div>

                     <div class="col-md-3" style="margin-top: 2rem;">
                         <div class="form-group col-md-12" style="padding-left:0; padding-right:0;text-align: center;">
                            <button type="button" class="btn btn-success height-btn addbtn" ><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                            <input type="hidden" name="f_regid" class="f_regid">
                            <input type="hidden" name="f_name" class="f_name">
                            <input type="hidden" name="f_surname" class="f_surname">
                            <input type="hidden" name="f_reg_id" class="f_reg_id">
                        </div>
                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0;text-align: center;">

                            <button type="button" class="btn btn-success height-btn removebtn" ><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                            <input type="hidden" name="familyid" class="familyid">
                        </div>
                     </div>
                  
                     <div class="col-md-5" style="padding-left:0; padding-right:0">
                      
                        <!--  <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-3 login-label" style="margin-top: 5px;"> Name </label>
                            <div class="controls col-md-9" style="margin-bottom: 7px;">
                              <input  type="text" class="search_fname" name="search_fname" class="form-control" >
                            </div>
                          </div>
                        </div>

                         <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-3 login-label" style="margin-top: 5px;"> Surname</label>
                            <div class="controls col-md-9" style="margin-bottom: 7px;">
                              <input  type="text" class="search_fsurname" name="search_fsurname" class="form-control" >
                            </div>
                          </div>
                        </div>

                        <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-3 login-label" style="margin-top: 5px;"> RegId</label>
                            <div class="controls col-md-9" style="margin-bottom: 7px;">
                              <input  type="text" class="search_fregid"  name="search_fregid" class="form-control" >
                            </div>
                          </div>
                        </div>

                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <button type="button" class="btn btn-success searchfamilybtn" style="margin-left: 85px;" >Search</button>
                        </div> -->
                   
                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <table class="table table-bordered tb-bg addtokentable">
                                        <tr>
                                           
                                            <th>RegID</th>
                                            <th>Name</th>
                                        </tr>
                                        <tbody class="searchfamily">

                                        </tbody>
                                    </table>
                        </div>
                        
                        
                        
                        
                        
                     </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="packagehistory" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Package History</h4>
            </div>
            {!! Form::open(['action' => 'LA\DaychargesController@store', 'id' => 'daycharges-add-form']) !!}
            <div class="modal-body">
                <div class="box-body">
                  
                 <div class="form-group col-md-4 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control pregid" placeholder="RegID" readonly  name="regid" type="text" value="" aria-required="true"required>
                    </div>
                    <div class="col-md-12"  style="padding-left:0;padding-right:0; margin-top: 30px;">
                   

                    
                        

                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <table class="table table-bordered tb-bg familytable">
                                        <tr>
                                            <th>Package Date</th>
                                            <th>Package</th>
                                            <th>Period</th>
                                            <th>From Date</th>
                                            <th>To Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        <tbody class="package-body">
                                            
                                        </tbody>
                                    </table>
                        </div>
                      

                    
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<div class="modal fade" id="opensms" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Send SMS</h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@usersendsms', 'id' => 'daycharges-add-form']) !!}
            <div class="modal-body">
                <div class="box-body">
                  
                 <div class="form-group col-md-12 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control smsid" placeholder="RegID" readonly  name="smsid" type="text" value="" aria-required="true"required>
                    </div>

                     <div class="col-md-12"  style="">

                        <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                            <label for="mobile">Template:</label>
                              <select class="form-control select2-hidden-accessible template" required="1" data-placeholder="Select Template" rel="select2" name="template" id="template" tabindex="-1" aria-hidden="true" aria-required="true"required onchange="gettemplate(this);">
                                        <option value="">Select</option>
                                    <?php foreach($sms as $smslist){?>
                                        <option value="<?php echo $smslist->id;?>" data-message="<?php echo $smslist->message;?>" ><?php echo $smslist->templatename;?></option>
                                    <?php } ?>
                                        
                                </select>  
                              
                                <textarea class="form-control messagetemp" placeholder="Enter Message" cols="30" rows="5" name="message" style="margin-top: 30px;" required></textarea>
                        </div>
                  
                      

                    
                </div>
                  
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success height-btn usersendsms">Send SMS</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="Smsreport" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content medicalcase_model">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">SMS Report Record</h4>
      </div>
      {!! Form::open(['action' => 'LA\CouriermedicineController@sendsms']) !!}
      <div class="modal-body">
        <div class="box-body">
                  {{--  @la_form($module) --}}
                    
          
            
             <div class="col-md-12" style="height: 173px; ">
                 <div class="scroll-bar-wrap">
                                 <div class="scroll-box" style="height: 180px;">
                      <table class="table table-bordered tb-bg amounttable" >
                                        <tr>
                                            <th style='text-align:center;'>Date</th>
                                            <th style='text-align:center;'>SMS Type</th>
                                        </tr>
                                        <tbody class="smsreport">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                </div>
                  
            
        
        </div>
      </div>
      <div class="modal-footer height-entry-footer">
        
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>





<script>
    function openSmsreport(id){
   
    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/getsmsdetail') }}",
            data: { id:id }, 
            success: function( msg ) {
               $(".smsreport").html(msg);
                $('#Smsreport').modal('show');
            }
        });

}
    function opensms(id){
    $('.smsid').val(id);
    $('#opensms').modal('show');
}
function openPackage(id){
    $('#case_id').val(id);
    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/getpacakge') }}",
            data: { id:id }, 
            success: function( msg ) {
                 var packagearr = JSON.parse(msg);
                 var pacakge_expiry= packagearr['packageexpiry'];
                var today = packagearr['today'];
                var packid = packagearr['packid'];
                
                var pacakgestart = packagearr['pacakgestart'];
                var packagename = packagearr['packagename'];
                var regname = packagearr['regname'];
                var regmobile = packagearr['regmobile'];
                var currdate = packagearr['currdate'];
                if(today > pacakge_expiry){
                    var expmsg = "Your Pacakge has been Expired";
                }
                $('.pacakge_expiry').text(pacakge_expiry);
                
                if(pacakge_expiry!="N/A"){
                    $('.package_start').val(pacakge_expiry);
                    $('#expirypack').css("display","inline-block");

                }
                $('.packagename').text(packagename);
                $('.pacakgestart').text('Package Start Date : '+ pacakgestart);
                $('.regname').text(regname);
                $('.regcasename').val(regname);
                $('.regmobile').val(regmobile);
                $('.expmsg').text(expmsg);
                if(packid!="N/A"){
                    $(".savepicture").html("Update Package");
                }
                else{
                    $(".savepicture").html("Save");
                    
                }
              //  console.log(pacakge_expiry);
                if(currdate!=pacakge_expiry && pacakge_expiry!="N/A"){
                     $('.savepicture').prop('disabled', true);
                }
                else {
                    $('.savepicture').prop('disabled', false);
                }
               $('#AddPackage').modal('show');
            }
        });

}

function openBill(id){
    $('#AddBill').modal('show');
    $('.bill_case_id').val(id);
}

function openHeightWeight(id){
   
    $('.reg_id').val(id);

    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/getheightweight') }}",
            data: { id:id }, 
            success: function( msg ) {
           
                var heightarr = JSON.parse(msg);
                var height= heightarr['height'];
                var weight= heightarr['weight'];
                var lmp= heightarr['lmp'];
                var gender= heightarr['gender'];
                var dateval= heightarr['dateval'];
                var rand_id= heightarr['rand_id'];
                var ht= heightarr['ht'];
                var wt= heightarr['wt'];
                var tddata= heightarr['tddata'];
                var datevalht= heightarr['datevalht'];

                $('.height_ad').val(height);
                $('.weight_ad').val(weight);
                $('.lmp').val(lmp);
                $('.gender').val(gender);
                $('.datevalht').val(dateval);
                $('.randht').val(rand_id);
                $('.ht').text(ht+" CM");
                $('.wt').text(wt+" KG");

                $('.datevalht1').val(datevalht);

                 $(".height-body").html(tddata);
                if(gender=="Male"){
                    $('.lmpdata').css("display","none");
                 
                }
                $('#AddHeightWeight').modal('show');
            }
        });
}

function openFamily(id){
   
    $('.f_reg_id').val(id);
    
    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/familydata') }}",
            data: { id:id }, 
            success: function( msg ) {
              
                $(".searchfamily").html(msg);
                if(msg==""){
                    var data = "<tr><td style='padding: 14px;'></td><td style='padding: 14px;'></td></tr><tr><td style='padding: 14px;'></td><td style='padding: 14px;'></td></tr>";
                    $(".searchfamily").html(data);
                }
                var tddata = "<tr><td style='padding: 14px;'></td><td style='padding: 14px;'></td></tr><tr><td style='padding: 14px;'></td><td style='padding: 14px;'></td></tr>";
                $(".search-body").html(tddata);
                $('#AddFamily').modal('show');
            }
        });
   
}

function openHistory(id){
   
   
    
    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/packagehistory') }}",
            data: { id:id }, 
            success: function( msg ) {
                console.log(msg);
                $(".pregid").val(id);
                $(".package-body").html(msg);
                $('#packagehistory').modal('show');
            }
        });
   
}

function deletepackage(id){ 
    var id = id;
    
    $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/deletepackage') }}",
            data: { id:id }, 
            success: function( msg ) {
                console.log(msg);
                 alert("Package Remove Successfully");
                   setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000); 
               
            }
        });
   
}

$(function () {

    $(".usersendsms").on("click", function(){
        var smsid = $('.smsid').val();
        var message = $('.messagetemp').val();
        //alert(message);
        if(smsid!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/usersendsms') }}",
                data: { smsid:smsid ,message:message }, 
                success: function( msg ) {
                 //console.log(msg);
                    alert("Message Sent ");
                    setTimeout(function(){// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                     }, 1000); 

                }
            });
        }else {
            alert('Select Id.');
        }
       
    });

    $(".searchbtn").on("click", function(){
        var search_name = $('.search_name').val();
        var search_surname = $('.search_surname').val();
        var search_regid = $('.search_regid').val();
        var f_reg_id = $('.f_reg_id').val();
        if(search_name!="" || search_surname!="" || search_regid!=""){
            $.ajax({
                type: "GET",
                url: "{{ url(config('laraadmin.adminRoute') . '/searchcases') }}",
                data: { search_name:search_name, search_surname:search_surname, search_regid:search_regid,f_reg_id:f_reg_id}, 
                success: function( data ) {
                     if(data!=""){
                        $(".search-body").html(data);
                     }
                     else{
                        var tddata = "<tr><td colspan='2' style='text-align:center;'>No Record Found</td></tr>";
                        $(".search-body").html(tddata);
                     }

                }
            });  
            }
            else{
                alert("Fields required");
            }  
        
    });

    $(".searchfamilybtn").on("click", function(){
        var search_fname = $('.search_fname').val();
        var search_fsurname = $('.search_fsurname').val();
        var search_fregid = $('.search_fregid').val();
        if(search_fname!="" || search_fsurname!="" || search_fregid!=""){
            $.ajax({
                type: "GET",
                url: "{{ url(config('laraadmin.adminRoute') . '/searchfamily') }}",
                data: { search_fname:search_fname, search_fsurname:search_fsurname, search_fregid:search_fregid}, 
                success: function( data ) {
                     
                    $(".searchfamily").html(data);

                }
            });  
            }
            else{
                alert("Fields required");
            }  
        
    });

    $(".addbtn").on("click", function(){
        var f_regid = $('.f_regid').val();
        var f_name = $('.f_name').val();
        var f_reg_id = $('.f_reg_id').val();
        var f_surname = $('.f_surname').val();
        if(regid!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/savefamily') }}",
                data: { f_regid:f_regid ,f_reg_id:f_reg_id,f_surname:f_surname,f_name:f_name }, 
                success: function( msg ) {
                    if(msg=="1"){
                        $('.addtokentable tr:first').after('<tr id="'+f_regid+'"><td onclick="removefunction('+f_regid+')">'+f_regid+'</td><td onclick="removefunction('+f_regid+')">'+f_name+'</td></tr>');
                    }

                }
            });
        }else {
            alert('Select Id.');
        }
       
    });


    $(".removebtn").on("click", function(){
        var id = $('.familyid').val();
      
        if(id!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/deletefamily') }}",
                data: { id:id}, 
                success: function( msg ) {
                    
                    $('#'+id).remove();
                }
            });
        }else {
            alert('Select Id.');
        }
       
    });
   
   $(".saveweightheight").on("click", function(){
        var dateval = $('.datevalht').val();
        var height = $('.height_ad').val();
        var weight = $('.weight_ad').val();
        var lmp = $('.lmp').val();
        var rand1 = $('.randht').val();
        var regid = $('.reg_id').val();
        if(dateval!="" || height!="" || weight!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/saveCaseHeightWeights') }}",
                data: { regid:regid, dateval:dateval,height:height,weight:weight,lmp:lmp,rand:rand1}, 
                success: function( msg ) {
                    $('.heightweighttabledata_ad tr:first').after('<tr id="'+rand1+'"><td>'+dateval+'</td><td>'+height+'</td><td>'+weight+'</td><td>'+lmp+'</td></tr>');
                  
                }
            });
        }else {
            alert('All fields required.');
        }
    });
    
    //alert('<?php echo $appointment['patient_id']; ?>');
    var getfirst = '<?php echo $appointment['patient_name']; ?>';
    var getaddress = '<?php echo $appointment['address']; ?>';
    var getphone = '<?php echo $appointment['phone']; ?>';
    var getcity = '<?php echo $appointment['city']; ?>';
    var getdoc = '<?php echo $appointment['assistant_doctor']; ?>';


  //AddModal
  if(getfirst!=""){
      $('#AddModal').modal('show');
        document.getElementById("first_name").value=getfirst;
        document.getElementById("address").value=getaddress;
        document.getElementById("phone").value=getphone;
        document.getElementById("city").value=getcity;
        document.getElementById("assitant_doctor").value=getdoc;
        <?php session()->forget('id'); ?>
  }

   $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = $('#min').datepicker("getDate");
            var max = $('#max').datepicker("getDate");
            var startDate = new Date(data[4]);
            if (min == null && max == null) { return true; }
            if (min == null && startDate <= max) { return true;}
            if(max == null && startDate >= min) {return true;}
            if (startDate <= max && startDate >= min) { return true; }
            return false;
        }
        );

       
            $("#min").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });
            $("#max").datepicker({ onSelect: function () { table.draw(); }, changeMonth: true, changeYear: true });


           
  
var table = $("#example1").DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/medicalcase_dt_ajax') }}",
         // ajax: {
        //     url: "{{ url(config('laraadmin.adminRoute') . '/medicalcase_dt_ajax') }}",
        //     type: "POST",
        //     data : {min:'2020-05-01', max:'2020-05-10' , _token: '{{csrf_token()}}'}

        // },

        language: {
            //lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Search"
        },
        <?php if(Auth::user()->type != "Doctor" && Auth::user()->type != "Receptionist") { ?>
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'pdfHtml5'
        ],
        <?php }?>

    });
 // Event listener to the two range filtering inputs to redraw on input
            $('#min, #max').change(function () {
                table.draw();
            });
    $("#medicalcase-add-form").validate({
        
    });
     $("#openaddmodel").on("click", function(){
          $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/newcaseadd') }}",
            //data: { name:name, message:message, post_id:postid }, 
            success: function( msg ) {
                $('.form-control').val();
                //$('.regid').val(msg);
                $('#SAVESTEP1').show();
                $('#AddModal').modal('show');
            }
        });
    });
    
    $(".SAVESTEP2").on("click", function(){
        $('#detailModal').modal('hide');
        $('#FullCaseDetail').modal('hide');
        $('#detailHomeModal').modal('show');
    });
    $(".SAVESTEP1").on("click", function(){
        $('#detailHomeModal').modal('hide');
        $('#FullCaseDetail').modal('hide');
       // $('#detailModal').modal('show');
    });
       $(".completetata").on("click", function(){
        $('.modal').modal('hide');
        setTimeout(function(){// wait for 5 secs(2)
               location.reload(); // then reload the page.(3)
          }, 1000); 
        
    });


       $(".getbill").on("click", function(){
        var billdate = $('.billdate').val();
        var bill_case_id = $('.bill_case_id').val();
        if(billdate!="" ){
            $.ajax({
                type: "GET",
                url: "{{ url(config('laraadmin.adminRoute') . '/getbilldetails') }}",
                data: { billdate:billdate,bill_case_id:bill_case_id}, 
                success: function( data ) {
                   if(data!=""){
                   var splitted = data.split("/");
                   $('.billdiv').css('display','block');
                    $('.case_name').val(splitted[0]);
                    $('.rece_amount').val(splitted[1]);
                    $('.case_u_id').val(splitted[2]);
                    $('.bill_date').val(billdate);
                 }
                 else{
                    $('.billdiv').css('display','block');
                    $('.divbill').css('display','none');
                    
                    $('.error_msg').text("No Bill found");
                 }

                }
            });
        }else {
            alert('Select date.');
        }
       
    });
    
    $(".SAVESTEP3").on("click", function(){
        
        $('#detailModal').modal('hide');
          var disease = $('.disease').val();  
          var diagnosis = $('.diagnosis').val();  
          var constitutional = $('.constitutional').val();  
          var complaint_intesity = $('.complaint_intesity').val();  
          var acute = $('.acute').val();  
          var intercurrent = $('.intercurrent').val();  
          var thermal = $('.thermal').val();  
          var medication_taking = $('.medication_taking').val();  
          var medicine_price = $('.medicine_price').val();  
          var assitant_doctor = $('.assitant_doctor').val();  
          var prognosis = $('.prognosis').val(); 
          var define_criteria1 = $('.define_criteria1').val();  
          var investigation = $('.investigation').val();  
          var define_criteria2 = $('.define_criteria2').val();  
          var criteria = $('.criteria').val();  
          var regid = $('.regid').val(); 
          var total_charges = $('.consultation_fee').val();  
          if(disease!="" && diagnosis!="" && constitutional!="" && complaint_intesity!="" && acute!="" && intercurrent!="" && medication_taking!=""){
          
          $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/homeodetail') }}",
            data: { regid:regid,disease:disease, diagnosis:diagnosis,constitutional:constitutional,complaint_intesity:complaint_intesity, acute:acute,intercurrent:intercurrent,
            thermal:thermal,medication_taking:medication_taking, medicine_price:medicine_price, prognosis:prognosis,assitant_doctor:assitant_doctor,define_criteria1:define_criteria1
            ,investigation:investigation,define_criteria2:define_criteria2,criteria:criteria,total_charges:total_charges}, 
            success: function( msg ) {
              $('#diagnosistest').html(disease);
              $('#domplainttest').html(complaint_intesity);
              $('#medicationtest').html(medication_taking);
              $('#investigationtest').html(investigation);
              
              $('#FullCaseDetail').modal('show');
              var db= $('.date_of_birth').val();
              var gnd= $('.gender').val();

              $.ajax({
                type: "get",
                url: "{{ url(config('laraadmin.adminRoute') . '/calculateheight') }}",
                data: { gender:gnd,db :db }, 
                success: function(data) {
                    console.log(data);
                   // $('.form-control').val();
                  var ht= data.split('/');
                 //  $('.height').val(ht[0]);
                  // $('.weight').val(ht[1]);
               
                }
            });
              $('.disease').removeClass('error'); $('#disease-error').hide();
              $('.diagnosis').removeClass('error'); $('#diagnosis-error').hide();
              $('.constitutional').removeClass('error'); $('#constitutional-error').hide();
              $('.complaint_intesity').removeClass('error'); $('#complaint_intesity-error').hide();
              $('.acute').removeClass('error'); $('#acute-error').hide();
              $('.intercurrent').removeClass('error'); $('#intercurrent-error').hide();
              $('.medication_taking').removeClass('error'); $('#medication_taking-error').hide();
             
              return false;
            }
        });
        }else {
            $('#detailHomeModal').modal('show');
            if(disease==""){ $('.disease').addClass('error'); $('#disease-error').show();
            }else { $('.disease').removeClass('error'); $('#disease-error').hide(); }
            
            if(diagnosis==""){ $('.diagnosis').addClass('error'); $('#diagnosis-error').show();
            }else { $('.diagnosis').removeClass('error'); $('#diagnosis-error').hide(); }
            
            if(constitutional==""){ $('.constitutional').addClass('error'); $('#constitutional-error').show();
            }else { $('.constitutional').removeClass('error'); $('#constitutional-error').hide(); }
            
            if(complaint_intesity==""){ $('.complaint_intesity').addClass('error'); $('#complaint_intesity-error').show();
            }else { $('.complaint_intesity').removeClass('error'); $('#complaint_intesity-error').hide(); }
            
            if(acute==""){ $('.acute').addClass('error'); $('#acute-error').show();
            }else { $('.acute').removeClass('error'); $('#acute-error').hide(); }
            
            if(intercurrent==""){ $('.intercurrent').addClass('error'); $('#intercurrent-error').show();
            }else { $('.intercurrent').removeClass('error'); $('#intercurrent-error').hide(); }
            
            if(medication_taking==""){ $('.medication_taking').addClass('error'); $('#medication_taking-error').show();
            }else { $('.medication_taking').removeClass('error'); $('#medication_taking-error').hide(); }
            
            
        }
    });

    $("#SAVESTEP1").on("click", function(){
          
          var regid = $('.regid').val();  
          var current_date = $('.current_date').val();  
          var mobile2 = $('.mobile2').val();  
          var first_name = $('.first_name').val();  
          var gender = $('.gender').val();  
          var mobile1 = $('.mobile1').val();  
          var city = $('.city').val();  
          var address = $('.address').val();  
          var assitant_doctor = $('.assitant_doctor').val();  
          var consultation_fee = $('.consultation_fee').val();  
          var title = $('.title').val(); 
          var middle_name = $('.middle_name').val();  
          var surname = $('.surname').val();  
          var status = $('.status').val();  
          var road = $('.road').val();  
          var religion = $('.religion').val();  
          var occupation = $('.occupation').val();  
          var phone = $('.phone').val();  
          var area = $('.area').val();  
          var state = $('.state').val();  
          var pin = $('.pin').val(); 
          var courier_outstation = $('.courier_outstation').val();  
          var send_sms = $('.radio').val();
         
          var email = $('.email').val();  
          var scheme = $('.scheme').val(); 
          var reference = $('.reference').val();
          var date_of_birth = $('.date_of_birth').val();
          var dob= $('.dob').val();
          var refered_search = $('.refered_search').val(); 
          var refered_by = $('.refered_by').val(); 
         // var patientid = $('.patientid').val(); 
          var refered_sms = $('.referedsms').val();
          if(first_name!="" && surname!="" && date_of_birth!="" && address!="" && phone!="" && mobile1!="" && pin!="" && city!="" && email!=""){
          $('#SAVESTEP1').hide();
          $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/create') }}",
            data: { regid:regid, date_of_birth:date_of_birth,dob:dob,current_date:current_date, mobile2:mobile2,first_name:first_name,gender:gender,mobile1:mobile1,city:city,address:address,assitant_doctor:assitant_doctor, consultation_fee:consultation_fee, title:title
            ,middle_name:middle_name,surname:surname,status:status,road:road,religion:religion,occupation:occupation,phone:phone,area:area,state:state,pin:pin,courier_outstation:courier_outstation,
            send_sms:send_sms,email:email,scheme:scheme,refered_search:refered_search,reference:reference, refered_by:refered_by,refered_sms:refered_sms }, 
            success: function( msg ) {//console.log(msg);
                    setTimeout(function(){// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                     }, 1000); 
             
            }
        });
        }else {
           // alert('Firstname, Surname, Address, City, Pin, Email, Phone and Mobile 1 is required');
            if(first_name==""){ $('.first_name').addClass('error'); $('#first_name-error').show();
            }else { $('.first_name').removeClass('error'); $('#first_name-error').hide(); }
            
            if(surname==""){ $('.surname').addClass('error'); $('#surname-error').show();
            }else { $('.surname').removeClass('error'); $('#surname-error').hide(); }
            
            if(address==""){ $('.address').addClass('error'); $('#address-error').show();
            }else { $('.address').removeClass('error'); $('#address-error').hide(); }
            
            if(date_of_birth==""){ $('.date_of_birth').addClass('error'); $('#date_of_birth-error').show();
            }else { $('.date_of_birth').removeClass('error'); $('#date_of_birth-error').hide(); }
            
            if(city==""){ $('.city').addClass('error'); $('#city-error').show();
            }else { $('.city').removeClass('error'); $('#city-error').hide(); }
            
            // if(pin==""){ $('.pin').addClass('error'); $('#pin-error').show();
            // }else { $('.pin').removeClass('error'); $('#pin-error').hide(); }
            
            // if(email==""){ $('.email').addClass('error'); $('#email-error').show();
            // }else { $('.email').removeClass('error'); $('#email-error').hide(); }
            
            if(phone.length!=10 || phone.length > 10 || phone.length < 10){ $('.phone').addClass('error'); $('#phone-error').show();
            }else { $('.phone').removeClass('error'); $('#phone-error').hide(); }

            
  
            if(mobile1=="" || mobile1.length!=10 || mobile1.length > 10 || mobile1.length < 10){ $('.mobile1').addClass('error'); $('#mobile1-error').show();
            }else { $('.mobile1').removeClass('error'); $('#mobile1-error').hide(); }
        }
    });


    $("#sendsms").on("click", function(){
          var regid = $('.regid').val(); 
          var first_name = $('.first_name').val();  
          var refered_sms = $('.referedsms').val();
          var refered_search = $('.refered_search').val(); 
          var refered_by = $('.refered_by').val(); 
          if(refered_by!=""){
          $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/referedsms') }}",
            data: { regid:regid,first_name:first_name,refered_sms:refered_sms,refered_search:refered_search, refered_by:refered_by }, 
            success: function( msg ) {
                  // console.log(msg);
                  alert("Sms send");
             
            }
        });
      }
      else{
        alert("Please fill refered Id");
      }

      });
    
    $(".rxdataadd").on("click", function(){
        var rxprescription = $('.rxprescription').val();
        var rxdays = $('.rxdays').val();
        var rxfrequency = $('.rxfrequency').val();
        var rxremedy = $('.rxremedy').val();
        var rxpotency = $('.rxpotency').val();
        var rand = '<?php echo rand('99',9999);?>';
        var regid = $('.regid').val();
        if(rxprescription!="" && rxdays!="" && rxfrequency!="" && rxremedy!="" && rxpotency!=""){
            $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/saveremidy') }}",
            data: { regid:regid, rxprescription:rxprescription,rxdays:rxdays,rxfrequency:rxfrequency, rxremedy:rxremedy,rxpotency:rxpotency,rand:rand}, 
            success: function( msg ) {
                    $('.rxdatavaltable tr:last').after('<tr id="'+rand+'"><td>'+rxremedy+'</td><td>'+rxpotency+'</td><td>'+rxfrequency+'</td><td>'+rxdays+'</td><td>'+rxprescription+'</td><td><a href="javascript:void(0)" onclick="removedata('+rand+')">Remove</a></td></tr>');
                    $('.rxprescription').val('');
                }
            })
        }else {
            alert('Fields required.');
        }
    });

    $('input:radio[name="send_sms"]').change(function() {
            
          if ($(this).val() == '1') {
            send_sms1 = "1";
            document.getElementById("radio").value = send_sms1;
          } else {
           send_sms1 = "0";
           document.getElementById("radio").value = send_sms1;
          }
    });

    $('input:radio[name="refered_sms"]').change(function() {
            
          if ($(this).val() == '1') {
            referedsms = "1";
            document.getElementById("referedsms").value = referedsms;
          } else {
           referedsms = "0";
           document.getElementById("referedsms").value = referedsms;
          }
    });

    $(".saveheightweight").on("click", function(){
        var dateval = $('.dateval').val();
        var height = $('.height').val();
        var weight = $('.weight').val();
        var blood_pressure = $('.blood_pressure').val();
        var lmpdata = $('.lmpdata').val();
        var rand1 = '<?php echo rand('99',9999);?>';
        var regid = $('.regid').val();
        if(dateval!="" && height!="" && weight!="" && blood_pressure!="" && lmpdata!=""){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/saveHeightWeights') }}",
                data: { regid:regid, dateval:dateval,height:height,weight:weight, blood_pressure:blood_pressure,lmpdata:lmpdata,rand:rand1}, 
                success: function( msg ) {
                    $('.heightweighttabledata tr:last').after('<tr id="'+rand1+'"><td>'+dateval+'</td><td>'+height+'</td><td>'+weight+'</td><td>'+blood_pressure+'</td><td>'+lmpdata+'</td><td><a href="javascript:void(0)" onclick="removenewdata('+rand1+')">Remove</a></td></tr>');
                    $('.dateval1').val(dateval);$('.height1').val(height);$('.weight1').val(weight);$('.blood_pressure1').val(blood_pressure);$('.lmpdata1').val(lmpdata);
                    $('.dateval').val('');$('.height').val('');$('.weight').val('');$('.blood_pressure').val('');$('.lmpdata').val('');
                }
            });
        }else {
            alert('All fields required.');
        }
    });
    $(".savefollowheightweight").on("click", function(){
        var dateval = $('.dateval1').val();
        var height = $('.height1').val();
        var weight = $('.weight1').val();
        var blood_pressure = $('.blood_pressure1').val();
        var lmpdata = $('.lmpdata1').val();
        if(dateval!="" && height!="" && weight!="" && blood_pressure!="" && lmpdata!=""){
            var followval = height+'\n '+weight+'\n '+blood_pressure+'\n '+lmpdata;
            $('.followupdataentery').append('<div class="form-group col-md-12"><label for="mobile">Follow Up on '+dateval+'</label><textarea class="form-control" name="" type= "" aria-required="false" rows="4">'+followval+'</textarea></div>');
            $('.dateval1').val('');$('.height1').val('');$('.weight1').val('');$('.blood_pressure1').val('');$('.lmpdata1').val('');
        }else {
            alert('All fields required.'); 
        }
    });
    
    
});
function removedata(id){
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/deletedata') }}",
        data: { rand:id}, 
        success: function( msg ) {
            $('#'+id).remove();
        }
    });
}
function removenewdata(id){
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/deleteheightdata') }}",
        data: { rand:id}, 
        success: function( msg ) {
            $('#'+id).remove();
        }
    });
}
</script>
<script type="text/javascript">
    $(function () {
        $(".datepicker").datepicker({
          format:'dd/mm/yyyy', 
            autoclose: true, 
            todayHighlight: true,
        }).l('update', new Date());
    });
    $("#timepicker").datetimepicker({
        format: "LT",
        icons: {
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down"
        }
  });
  
   $(function () {
        $(".datepickerheight").datepicker({
          format:'dd/mm/yyyy', 
            autoclose: true, 
            todayHighlight: true,
            endDate: '+0d',
        });
       
    });

  $('#medicalcase-add-form-step1').validate();
  $('#medicalcase-add-formstep2').validate();
  $('#medicalcase-add-form-step3').validate();
  $('#medicalcase-add-form4').validate();
  $('#medicalcase-add-form-step4').validate();
  $('#medicalcase-add-form5').validate();
  
  //var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  function openaddmodel(){
      
  }
</script>
<script>
    $('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });
});

function priceget(obj){
    var price = $(obj).children('option:selected').attr('data-price');
     $(obj).parent().parent().find(".consultation_fee").val(price);
}
function medicine_price_get(obj){
    var price = $(obj).children('option:selected').attr('data-price');
     $(obj).parent().parent().find(".medicine_price").val(price);
}

$('#refered_by').on('keyup',function(){
var value=$(this).val();
console.log(value);
$.ajax({
type : 'get',
url : "{{ url(config('laraadmin.adminRoute') . '/search') }}",
data:{'search':value},
success:function(data){
    //$('.search-result').html(data);
     $('.refered_search').val(data);
}

});
});
</script>
<script type="text/javascript">
 
$("#example1").click(function() {
    $("tr").click(function() {
     var selected = $(this).hasClass("highlight");
    //alert(selected);
    $("tr").removeClass("highlight");
    if(!selected ||selected){
            $(this).addClass("highlight");
            }
    var text = $(this).closest("tr")   
                       .find(".namec")    
                       .attr("href");        
   document.getElementById("tdlink").value = text;
    //alert(text);       
    });
});

function opencase(){
    var loc = document.getElementById("tdlink").value;
    if(loc!=""){
        location.replace(loc);
    }
    else{
        alert("Please select row from data");
    }
}
function check()
{

    var mobile = document.getElementById('phone');


    var message = document.getElementById('message');

   var goodColor = "#0C6";
    var badColor = "#FF9B37";

    if(mobile.value.length!=10){

        mobile.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "required 10 digits, match requested format!"
    }}

    function focusfunction(id,name,surname){
     $("."+id).focus();
     var regid = id;
      $(".familytable tr").click(function() {
        var selected = $(this).hasClass("highlight");
        //alert(selected);
        $(".familytable tr").removeClass("highlight");
        if(!selected ||selected){
                $(this).addClass("highlight");
        }
    });

     //$(".rxdatavaltable tr[id="+id+"]").css("background-color", "grey");
        var rid = id;
        $('.f_regid').val(id);
        $('.f_name').val(name);
        $('.f_surname').val(surname);

    
}

function removefunction(id){
    
     $("."+id).focus();
     var familyid = id;
      $(".addtokentable tr").click(function() {
        var selected = $(this).hasClass("highlight");
        //alert(selected);
        $(".addtokentable tr").removeClass("highlight");
        if(!selected ||selected){
                $(this).addClass("highlight");
        }
    });

      $('.familyid').val(id);

    
}
function getprice(obj){
    var getprice = $(obj).children('option:selected').attr('data-price');
    var getmonths = $(obj).children('option:selected').attr('data-months');
     $(obj).parent().parent().find(".packcharges").text("Charges : " +getprice);
     $(obj).parent().parent().find(".packagemonths").val(getmonths);
}

function gettemplate(obj){
    var gettemplate = $(obj).children('option:selected').attr('data-message');
  // alert(gettemplate);
     $(obj).parent().parent().find(".messagetemp").val(gettemplate);
   
}


</script>
@endpush
<style>
    .error{ display:none;}
</style>
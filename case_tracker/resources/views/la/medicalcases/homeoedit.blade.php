@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/medicalcases') }}">Medicalcase</a> :
@endsection
@section("contentheader_description", $homeodetail->$view_col)
@section("section", "Medicalcase")
@section("section_url", url(config('laraadmin.adminRoute') . '/medicalcases'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Medicalcase Edit : ".$homeodetail->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="post" action="{{ url(config('laraadmin.adminRoute') . '/medicalcases/homeoupdate/') }}" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
													
							<div class="form-group col-md-6">
							<label for="Emailid">Diagnosis :</label>
							<input type="hidden" name="id" value="{{ $homeodetail->id }}">
							<input class="form-control"  name="diagnosis" type="text" value="{{ $homeodetail->diagnosis }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="phone">Complaint Intensity :</label>
							<input class="form-control"  name="complaint_intesity" type="text" value="{{ $homeodetail->complaint_intesity }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="address">Medication Taking :</label>
							<input class="form-control" name="medication_taking" type="text" value="{{ $homeodetail->medication_taking }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="city">Investigation* :</label>
							<input class="form-control"required="1" name="investigation" type="text" value="{{ $homeodetail->investigation }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="pin">Case Taken by:</label>
							<input class="form-control"required="1" name="case_taken" type="text" value="{{ $homeodetail->case_taken }}" aria-required="true"></div>
																	
					                    <br>
					<div class="form-group col-md-12">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
@endpush

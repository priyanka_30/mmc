    @extends('la.layouts.app')

@section('htmlheader_title')
	Patient View
@endsection


@section('main-content')
<style>
	.panel.infolist .form-group:first-child {
    margin-top: 0px;
}

.panel.infolist .form-group {
    border-bottom: 0px dashed #e2e4e8;
    overflow-y: hidden !important;
}
.detail{
	padding-left: 0px;
    font-size: 14px;
}
.col-class{
	margin-top: 5px;
}
.checkmark-radio {
    position: absolute;
    top: 3px;
    left: 0;
    height: 14px;
    width: 14px;
    background-color: #fff;
    border: 1px solid #d2d6de;
    border-radius: 50%;
}
.container-radio .checkmark-radio:after {
    top: 2px;
    left: 2px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: #000;
}
.input-focus {
    background-color: yellow;
}
.highlight { background-color: grey; }

.btn-default:disabled,
.btn-default[disabled]{
    background-color: #868686; 
    border-color: #868686;
    opacity: 1;
}
.scroll-bar-wrap {
 /* width: 300px;*/
  position: relative;
 /* margin: 2em auto;*/
}
.scroll-box {
 /* width: 100%;*/
  height: 440px;
  overflow-y: scroll;
  position: relative;

}
.scroll-box::-webkit-scrollbar {
  width: .4em; 
}
.scroll-box::-webkit-scrollbar,
.scroll-box::-webkit-scrollbar-thumb {
  overflow:visible;
  border-radius: 4px;
}
.scroll-box::-webkit-scrollbar-thumb {
  background: rgba(0,0,0,.2); 
}
.cover-bar {
  position: absolute;
  background: #dedede;
  height: 100%;  
  top: 0;
  right: 0;
  width: .4em;
  -webkit-transition: all .5s;
  opacity: 1;
}
/* MAGIC HAPPENS HERE */
.scroll-bar-wrap:hover .cover-bar {
   opacity: 0;
  -webkit-transition: all .5s;
}
.nav-pills > li > a {
    border-radius: 0;
    border-top: 3px solid transparent;
    color: #444;
    background: #f7f7f7;
}
.nav-pills > li {
    margin: 4px;
}
.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    cursor: not-allowed;
    background-color: #fff;
    opacity: 1;
}
</style>

<div id="page-content" class="profile2 ">
	<div class="bg-success clearfix">
		<div class="col-md-4">
			<div class="row">
				<!-- <div class="col-md-3">
					<img class="profile-image" src="{{ Gravatar::fallback(asset('/img/avatar5.png'))->get(Auth::user()->email, ['size'=>400]) }}" alt="">
				</div> -->
				<div class="col-md-9">
					<h4 class="name">{{ $medicalcase->$view_col }}</h4>
					
					<p class="desc">{{ substr($medicalcase->address, 0, 33) }}@if(strlen($medicalcase->address) > 33)...@endif</p>
				</div>
			</div>
		</div>
		
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/medicalcases') }}" data-toggle="tooltip" data-placement="right" title="Back to Medical case"><i class="fa fa-chevron-left"></i></a></li>
		<!-- <li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-clock-o"></i> Home Detail</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-timeline1" data-target="#tab-timeline1"><i class="fa fa-clock-o"></i> Potecy Data</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-timeline2" data-target="#tab-timeline2"><i class="fa fa-clock-o"></i> Height & Weight</a></li> -->
		
		
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="">
					   
			
				<div class="box-body">
				    
                  {{--  @la_form($module) --}}
                  <?php 
                  	$dob=date('d-m-Y', strtotime($caseData->date_of_birth));
                  	$today = date("Y-m-d");
					$diff = date_diff(date_create($dob), date_create($today));

					

						$date1 = $caseData->date_of_birth;
						$date2 = date("Y-m-d");

						$ts1 = strtotime($date1);
						$ts2 = strtotime($date2);

						$year1 = date('Y', $ts1);
						$year2 = date('Y', $ts2);

						$month1 = date('m', $ts1);
						$month2 = date('m', $ts2);

						$diff1 = (($year2 - $year1) * 12) + ($month2 - $month1);
						//echo $diff1;
                        if($caseData->gender!=""){
						$string = $caseData->gender;
 
						//Get the first character.
						$firstCharacter = $string[0];
                        }else{
                            $firstCharacter = '';
                        }
                        
                  ?>
               
                  <fieldset class="fieldset" style="background: #dedede;">
                      <div class="form-group col-md-12" style="padding-left:0;padding-right:0">
                        
                        <div class="col-md-3" style="background-color: #dedede; height: 440px; ">
                            <div class="scroll-bar-wrap">
                                 <div class="scroll-box">
                                        <div class="form-group col-md-12" class="patient-info-td" style="margin-bottom: 0px;">
                                            <label for="mobile" class="patient-info-label-detail"><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
                                        </div>
                                        <div class="form-group col-md-12" style="margin-bottom: 0px;">
                                            <label for="mobile" class="patient-info-label-scheme"><?php if($medicalcase->package=="0"){ echo "Scheme Other"; } else{  echo "Scheme ". $pacakeid->packname; }?> 

                                            <?php  if($pacakeid->date_left == "12/31/9999") { echo " "; } else { echo  date('d M Y',strtotime($pacakeid->date_left)); }?></label>
                                        </div>

                                        
                                        <input type="hidden" name="id" value="<?php if($casent!=""){  echo $casent->id; } else { echo "";} ?>" class="nid">
                                        <input type="hidden"  value="<?php if($casent!=""){  echo $casent->notes; } else { echo "";} ?>" class="nnotes">    
                                          <input class="dateval" type="hidden" name="dateval" value="<?php  echo date('d-m-Y'); ?>"/>
                                          <?php foreach($CasePotency as $CasePotencyval){ 
                                           
                                            $notes11 = DB::table('notes')->where([ ['regid', '=', $CasePotencyval->id],['deleted_at','=',NULL] ])->orderBy('id','DESC')->get();
                                            foreach($notes11 as $n1){
                                              //  echo "<pre>"; print_r($n1);
                                            ?>
                                             <div class="form-group col-md-12 " id="<?php echo $n1->rand_id;?>">
                                            
                                            <label for="mobile">Follow Up on  <?php echo date('d M Y',strtotime($n1->dateval));?> </label>
                                            <textarea class="form-control <?php echo $n1->rand_id; ?> notes1" name="notes" onkeyup="updatenotes(this);"  aria-required="false" rows="4"><?php echo $n1->notes; ?></textarea>
                                            <input type="hidden" class="notesid" name="id" id="id" value="<?php echo $n1->id;?>">  

                                        </div>
                                         <?php   } } ?>

                                       

                                        <div class="followupdataentery1"></div>
                                     </div>
                                    <div class="cover-bar"></div>
                                 
                              </div>
                          </div>
                                
                       
                          <div class="col-md-7" style="background: #ffffff;padding: 20px 10px;">
                                <div class="form-group col-md-12" style="padding-left:0;padding-right:0;margin-bottom: 0px; padding: 0;height: 83px;">
                                	<div class="form-group col-md-12" style="margin-bottom: 0px; margin-top: 14px;">
    								
                    	<button type="button" class="col-md-3 btn btn-default height-btn rxdataadd" style="margin: 0px 8px 0px 1px;    width: 83px;">Rx</button>
    								
    									<button type="button" class="col-md-3 btn btn-default height-btn rxdatacopy" style="margin: 0px 8px 0px 8px;    margin-left: 18px;width: 95px;" <?php if($countpotency <= 1){ echo "disabled style='    background-color: #868686;border-color: #868686;'";} ?> >Repeat</button>
    								
    									<button type="button" class="col-md-2 btn btn-default height-btn" data-toggle="modal" data-target="#AddImage" style="margin: 0px 8px 0px 8px;">Add Image</button>

                        <input type="hidden" name="" class="rid">
                      <label class="container-radio" style="font-size: 16px; margin-bottom: 0px; ">Clinic
                        <input type="radio" class="rxtype"  name="couriertype" value="Clinic" >
                        <span class="checkmark-radio"></span>
                      </label>
                      <label class="container-radio" style="font-size: 16px; margin-bottom: 0px;">Courier
                        <input type="radio" class="rxtype" id="rxtype" name="couriertype" value="Courier"  <?php if($couriermed!=""){ if($couriermed->post_type == "Courier"){ echo "checked"; } } ?> >
                        <span class="checkmark-radio"></span>
                      </label>
                      <label class="container-radio" style="font-size: 16px; margin-bottom: 0px;" >Pickup
                        <input type="radio" class="rxtype"  name="couriertype" value="Pickup" <?php if($couriermed!=""){ if($couriermed->post_type == "Pickup"){ echo "checked"; } } ?> >
                        <span class="checkmark-radio"></span>
                      </label>
                                        <input type="hidden" class="prand" name="randid" value="<?php if($casept!=""){  echo $casept->rand_id; } else { echo "";} ?>" >
    									
    								</div>
                                   <!--  <div class="col-md-3">
                                      
									</div> -->
									
								</div>
								<div class="form-group col-md-9" style="padding-left:0;padding-right:0;margin-bottom: 0px; padding: 0;">
                                   
								    <div class="form-group col-md-3" style="margin-bottom: 0px;">
        									<label for="name">Remedy:</label>
        									<input class="form-control datevalrx" type="hidden" name="dateval" readonly value="<?php echo date('d/m/Y');?>"/>
        									<select class="form-control select2-hidden-accessible rxremedy" rel="select2" name="remedy" tabindex="-1" aria-hidden="true" aria-required="true">
        										<option value="">Select</option>
        										<?php foreach($medicines as $stname){?>
                                                <option value="{{ $stname->id }}" <?php if($casept!=""){ if($casept->rxremedy == $stname->id){ echo "selected"; }  } ?> data-remedy="{{ $stname->name }}" >{{ $stname->description }}</option>
                                                <?php }?>
        									</select>	
        								</div>
        								<div class="form-group col-md-3" style="margin-bottom: 0px;">
        									<label for="name">Potency:</label>
        									<select class="form-control select2-hidden-accessible rxpotency" rel="select2" name="potency" tabindex="-1" aria-hidden="true" aria-required="true">
        										<option value="">Select</option>
        										<?php foreach($Potency as $Potencyval){?>
                                                <option value="{{ $Potencyval->id }}" <?php if($casept!=""){ if($casept->rxpotency == $Potencyval->id){ echo "selected"; }  } ?> data-potency="{{ $Potencyval->name }}">{{ $Potencyval->name }}</option>
                                                <?php }?>
        										
        									</select>	
        								</div>
        								<div class="form-group col-md-3" style="margin-bottom: 0px;">
        									<label for="name">Frequency:</label>
        									<select class="form-control select2-hidden-accessible rxfrequency" rel="select2" name="frequency" tabindex="-1" aria-hidden="true" aria-required="true">
        									    <option value="">Select</option>
        										<?php foreach($cfrequency as $frequency){?>
                                                <option value="{{ $frequency->ID }}" <?php if($casept!=""){ if($casept->rxfrequency == $frequency->ID){ echo "selected"; }  } ?> data-frequency="{{ $frequency->frequency }}">{{ $frequency->frequency }}</option>
                                                <?php }?>
        									</select>	
        								</div>	
        								<div class="form-group col-md-3" style="margin-bottom: 0px;">
                           <!-- {!! Form::open(['action' => 'LA\MedicalcasesController@updaterxdays', 'id' => 'medicalcase-add-form5']) !!}  -->
        									<label for="name">Days:</label>
        									<select class="form-control select2-hidden-accessible rxdays" rel="select2" name="days" tabindex="-1" aria-hidden="true" aria-required="true">
        									    <option value="">Select</option>
        										<?php foreach($daysMedicine as $medicine){?>
                                                <option value="{{ $medicine->id }}" <?php if($casept!=""){ if($casept->rxdays == $medicine->id){ echo "selected"; }  } ?> data-days="{{ $medicine->days }}">{{ $medicine->days }}</option>
                                                <?php }?>
        									</select>	
                          <!--  <button type="submit" class="btn btn-success height-btn saveheightweight">Save</button>
                          {!! Form::close() !!}  -->
        								</div>
                                        <input type="hidden" class="scheme_case" value="<?php echo  $caseData->scheme; ?>">
        					</div>
        					    <div class="form-group col-md-12" style="padding-left:0;padding-right:0;margin-bottom: 0px;">
            					    <div class="form-group col-md-12" style="margin-bottom: 0px;">
    									<label for="mobile">Prescription</label>
    									<textarea class="form-control rxprescription" placeholder="Prescription" id="prescription"  name="prescription" type="text" value="" aria-required="true" rows="2"><?php if($casept!=""){ echo $casept->rxprescription; } ?></textarea>
    								</div>
    								<input type="hidden" class="rxprescription1" name="rxprescription1">
    								<input type="hidden"class="rxdays1" name="rxdays1">
    								<input type="hidden" class="rxfrequency1" name="rxfrequency1">
    								<input type="hidden" class="rxpotency1" name="rxpotency1">
    								<input type="hidden" class="rxremedy1" name="rxremedy1">
    								<!-- <div class="form-group col-md-8" style="margin-bottom: 0px;">
    									<button type="button" class="btn btn-default height-btn rxdataadd">Rx</button>
    								
    									<button type="button" class="btn btn-default height-btn rxdatacopy">Ctrl+All</button>
    								
    									<button type="button" class="btn btn-default height-btn" data-toggle="modal" data-target="#AddImage">Add Image</button>
    								</div> -->
                                  
                                    <?php if($plast!=""){ ?>
                                    <input type="hidden" class="prxprescription1" name="rxprescription1" value="<?php echo $plast->rxprescription; ?>">
                                    <input type="hidden"class="prxdays1" name="rxdays1" value="<?php echo $plast->rxdays; ?>">
                                    <input type="hidden" class="prxfrequency1" name="rxfrequency1" value="<?php echo $plast->rxfrequency; ?>">
                                    <input type="hidden" class="prxpotency1" name="rxpotency1" value="<?php echo $plast->rxpotency; ?>">
                                    <input type="hidden" class="prxremedy1" name="rxremedy1" value="<?php echo $plast->rxremedy; ?>">
                                   
                                   
                                 <?php } else{ ?>
                                    <input type="hidden" class="prxprescription1" name="rxprescription1" value="">
                                    <input type="hidden"class="prxdays1" name="rxdays1" value="">
                                    <input type="hidden" class="prxfrequency1" name="rxfrequency1" value="">
                                    <input type="hidden" class="prxpotency1" name="rxpotency1" value="">
                                    <input type="hidden" class="prxremedy1" name="rxremedy1" value="">
                                   
                                   
                                 <?php } ?>

                                 <?php if($potencyid!=""){ ?>
                                     <input type="hidden" class="prand" name="rxremedy1" value="<?php echo $potencyid->rand_id; ?>">
                                      <input type="hidden" class="pdateval" name="rxremedy1" value="<?php echo date('d/m/Y',strtotime($potencyid->created_at)); ?>">
                                    <input type="hidden" class="pprxdays1" name="rxremedy1" value="<?php echo $potencyid->rxdays; ?>">
                                    <input type="hidden" class="pprxfrequency1" name="rxremedy1" value="<?php echo $potencyid->frequency; ?>">
                                    <input type="hidden" class="pprxpotency1" name="rxremedy1" value="<?php echo $potencyid->pname; ?>">
                                    <input type="hidden" class="pprxremedy1" name="rxremedy1" value="<?php echo $potencyid->name; ?>">
                                    <input type="hidden" class="pprxprescription1" name="rxremedy1" value="<?php echo $potencyid->rxprescription; ?>">
                               <?php   } else { ?>
                                 <input type="hidden" class="prand" name="rxremedy1" value="">
                                  <input type="hidden" class="pdateval" name="rxremedy1" value="">
                                    <input type="hidden" class="pprxdays1" name="rxremedy1" value="">
                                    <input type="hidden" class="pprxfrequency1" name="rxremedy1" value="">
                                    <input type="hidden" class="pprxpotency1" name="rxremedy1" value="">
                                    <input type="hidden" class="pprxremedy1" name="rxremedy1" value="">
                                     <input type="hidden" class="pprxprescription1" name="rxremedy1" value="">
                               <?php } ?>
                                    
								</div>
								
								<div class="form-group col-md-12" style="height: 173px; ">
                                     <div class="scroll-bar-wrap">
                                 <div class="scroll-box" style="height: 160px;">
    								<table class="table table-bordered tb-bg rxdatavaltable" id="rxdatavaltable">
    									<tr>
    										<th>Date</th>
    									    <th>Remedy</th>
    										<th>Potency</th>
    										<th>Frequency</th>
    										<th>Days</th>
    										<th>Prescription</th>
                                            <?php if(Auth::user()->type == "Admin"  ) { ?>
    										<th>&nbsp;</th>
                                             <?php } ?>

    									</tr>
    									<?php foreach($CasePotency as $CasePotencyval){ 
                                           
                                           
                                            ?>
    									<tr id="<?php echo $CasePotencyval->rand_id;?>" class="<?php echo $CasePotencyval->rand_id;?>">
    										<td style="cursor: pointer;width:16%;" onclick="focusfunction(<?php echo $CasePotencyval->rand_id;?>);"><?php echo 
                        date('d M Y',strtotime($CasePotencyval->dateval)); ?></td>
    									    <td style="cursor: pointer;" onclick="focusfunction(<?php echo $CasePotencyval->rand_id;?>);"><?php echo $CasePotencyval->name;?></td>
    									    <td style="cursor: pointer;" onclick="focusfunction(<?php echo $CasePotencyval->rand_id;?>);"><?php echo $CasePotencyval->pname;?></td>
    									    <td style="cursor: pointer;" onclick="focusfunction(<?php echo $CasePotencyval->rand_id;?>);"><?php echo $CasePotencyval->frequency;?></td>
    									    <td style="cursor: pointer; width:11%;" onclick="focusfunction(<?php echo $CasePotencyval->rand_id;?>);"><?php echo $CasePotencyval->days;?></td>
    									    <td style="cursor: pointer;" onclick="focusfunction(<?php echo $CasePotencyval->rand_id;?>);"><?php echo $CasePotencyval->rxprescription;?></td>
                                            <?php if(Auth::user()->type == "Admin"  ) { ?>
    									    <td>
					   							<a href="javascript:void(0)" onclick="removedata(<?php echo $CasePotencyval->id;?>);">Remove</a>
					   						</td>
                                        <?php } ?>
    									</tr>
    									<?php } ?>
    									
    								</table>
                                </div>
                                 <div class="cover-bar" style="background: #fff;"></div>
                            </div>
								</div>
								
                          </div>
                          <div class="form-group col-md-2" style="background-color: #dedede; height: 440px; padding: 0;">
                            <div class="scroll-bar-wrap">
                                 <div class="scroll-box">
                                    <div class="followupdataentery4"></div>
                                       <?php if($Homedetailss!=""){ 
                                       foreach($Homedetailss as $Homedetails){ 
                                       ?>
                                       <?php if($Homedetails->diagnosis!=""){ ?>
                                       <div class="form-group col-md-12" style="background: #fff;margin: 0px 0px 0px 15px;width: 90%;  padding: 10px; border-bottom: 4px solid #ccc;">
                                        
                                      <label for="mobile" style="padding-left: 10px;">Follow Up </label>
                                
                                      	 <?php if($Homedetails->diagnosis!=""){ ?>
                                          <div class="form-group col-md-12" style="margin-bottom: 0; padding-bottom: 0;">
            									<label for="mobile" style="margin-bottom: 0;">Diagnosis</label>
            									<p id="diagnosistest">{{ $Homedetails->diagnosis }}</p> 
            								</div>
                                        <?php } ?>
                                        <?php if($Homedetails->complaint_intesity!=""){ ?>
            								<div class="form-group col-md-12" style="margin-bottom: 0; padding-bottom: 0;">
            									<label for="mobile" style="margin-bottom: 0;">Complaint Intensity</label>
            									<p id="domplainttest">{{ $Homedetails->complaint_intesity }}</p> 
            								</div>
                                             <?php } ?>
                                            <?php if($Homedetails->medication_taking!=""){ ?>
            								<div class="form-group col-md-12" style="margin-bottom: 0; padding-bottom: 0;">
            									<label for="mobile" style="margin-bottom: 0;">Medication Taking</label>
            									<p id="medicationtest">{{ $Homedetails->medication_taking }}</p> 
            								</div>
                                             <?php } ?>
                                            <?php if($Homedetails->investigation!=""){ ?>
            								<div class="form-group col-md-12" style="margin-bottom: 0; padding-bottom: 0;">
            									<label for="mobile" style="margin-bottom: 0;">Investigation</label>
            									<p id="investigationtest">{{ $Homedetails->investigation }}</p> 
            								</div>
                                             <?php } ?>
                                      	</div>
                                          <?php } ?>
                                        <?php } } ?>
                                    </div>
                                <div class="cover-bar"></div>
                            </div>
                          </div>
                         
                      </div>
					<table style="width: 100%;">
						 <tr>
						 	<td colspan="2" valign="top" style="width: 75%">
						 		<div class="form-group col-md-12">
						 		
						 		<a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#HomeoDetail">
						 			<img src="{{ asset('la-assets/img/medicine.png') }}" class="case-detail-icon">
						 		</a>
						 		<a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#ExaminationReport">
						 			<img src="{{ asset('la-assets/img/stethoscope-icon.png') }}" class="case-detail-icon">
						 		</a>
						 		<!-- <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#InvestigationReport">
						 			<img src="{{ asset('la-assets/img/microscope.png') }}" class="case-detail-icon">
						 		</a> -->
                                <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#OtherInvestigation">
                                    <img src="{{ asset('la-assets/img/microscope.png') }}" class="case-detail-icon">
                                </a>
						 		<a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#HeightWeightEntry">
						 			<img src="{{ asset('la-assets/img/weight-scale.png') }}" class="case-detail-icon">
						 		</a>
						 		<a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#CommunicationDetail">
						 			<img src="{{ asset('la-assets/img/phone1.png') }}" class="case-detail-icon">
						 		</a>
						 		<a target="_blank" href="http://smartops.co.in/managemyclinic/admin/casereminder">
						 			<img src="{{ asset('la-assets/img/alarm-clock.png') }}" class="case-detail-icon">
						 		</a>
						 		<a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#GraphDetail">
						 			<img src="{{ asset('la-assets/img/statsbar.png') }}" class="case-detail-icon">
						 		</a>
						 	<!-- 	<a href="tel:<?php echo $caseData->phone; ?>">
						 			<img src="{{ asset('la-assets/img/phone2.png') }}" class="case-detail-icon">
						 		</a> -->
						 		<a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#PictureShow">
						 			<img src="{{ asset('la-assets/img/filmmedia.png') }}" class="case-detail-icon">
						 		</a>
						 		<!-- <a href="#">
						 			<img src="{{ asset('la-assets/img/user-group.png') }}" class="case-detail-icon">
						 		</a> -->
						 		<a href="#">
						 			<img src="{{ asset('la-assets/img/delete-cross.png') }}" class="case-detail-icon">
						 		</a>
                                <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#DetailInvestigation">
                                    <img src="{{ asset('la-assets/img/medicalreport.png') }}" class="case-detail-icon">
                                </a>

                                <a data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#Familygroup">
                                    <img src="{{ asset('la-assets/img/groupuser.png') }}" class="case-detail-icon">
                                </a>
                                
						 		
						 		</div>
						 		<div class="col-md-12">
							 		<fieldset class="fieldset" style="padding-top: 0px;">
									<div class="panel-body">
									    
										<div class="col-md-10" style="padding-right: 0px">
										    <div class="col-md-12">
										   		<label class="detail" for="mobile" ><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
					    				    </div>
					    				    <div class="col-md-6 col-class" style="padding-right: 0px">
										   		<label for="name" class="detail" style="width:150px;float:left;"> Address:</label>
						   						<div class="detail"><?php echo $caseData->address.' '.$caseData->road.', '.$caseData->city.' '.$caseData->pin;?></div>
					    				    </div>
					    				    <div class="col-md-6 col-class">
										   		<label for="name" class="detail" style="width:150px;float:left;"> Package name:</label>
						   						<div class="detail"><?php 
						   						if(!empty($packages)) {
						   						    echo $packages->name;
						   						} else {
						   						    echo "N/A";
						   						}
						   						?></div>
					    				    </div>
					    				    <div class="col-md-6 col-class">
					    				    	<?php 

					    				    	if($caseData->scheme =="Monthly Pack"){
			    				    				$expiry = date('d/m/Y',strtotime('+30 days',strtotime(str_replace('/', '-', $caseData->created_at))));
					    				    	}
					    				    	elseif($caseData->scheme =="Half Yearly Pack"){
					    				    		$expiry =  date('d/m/Y',strtotime('+182 days',strtotime(str_replace('/', '-', $caseData->created_at))));

					    				    	}
					    				    	elseif($caseData->scheme =="Yearly Package"){
					    				    		$expiry =  date('d/m/Y',strtotime('+365 days',strtotime(str_replace('/', '-', $caseData->created_at))));
					    				    	}

					    				    	else{
					    				    		$expiry = date('d/m/Y',strtotime('+30 days',strtotime(str_replace('/', '-', $caseData->created_at))));
					    				    	}
					    				    	
					    				    	?>
										   		<label for="name" class="detail" style="width:150px;float:left;"> Package Expiry:</label>
						   						<div class="detail"><?php  if($pacakeid->date_left == "12/31/9999") { echo " "; } else { echo  date('d M Y',strtotime($pacakeid->date_left)); }?></div>
					    				    </div>
					    				     <?php
                                    $total_charges = 0;
                                    $balance = 0;
                                    $applied_charges = 0;
                                    $bill_price = 0;
                                    $total_balance = 0;
                                    $addbal = 0;
                                    $billbal = 0;
                                    $totalbal = 0;
                                    if($paydays!=''){
                                    
                                    $total_charges = $paydays->additional_price + $paydays->charges;
                                    $balance = $total_charges - $paydays->received_price;
                                     }

                                     if($additional_price!=""){
                                      $applied_charges = $additional_price->additional_price;
                                     }
                                      if($billing!=""){  
                                       $bill_price = $billing->Balance;
                                      }
                                      $total_balance = $bill_price + $applied_charges;


                                      if($paydays!=""){ 
                                       $addbal = $paydays->additional_price; 
                                     }
                                     if($billing!=""){  
                                       $billbal = $billing->Balance;
                                      }

                                      $totalbal = $billbal + $addbal;
                                     ?>
					    				     <div class="col-md-6 col-class">
										   		<label for="name" class="detail" style="width:150px;float:left;"> Case taken By:</label>
						   						<div class="detail"><?php echo $case_docter->name; ?></div>
					    				    </div>
					    				    <div class="col-md-6 col-class" style="padding-right: 0px">
										   		<label for="name" class=" detail" style="width:150px;float:left;"> Mobile:</label>
						   						<div class="detail"><?php echo $caseData->mobile1; ?></div>
					    				    </div>
					    				     <div class="col-md-6 col-class">
										   		<label for="name" class="detail" style="width:150px;float:left;"> Balance:</label>
						   						<div class="total-bal"><?php  echo "Rs ". $totalbal; ?></div>
					    				    </div>
					    				     <div class="col-md-6 col-class">
										   		<label for="name" class="detail" style="width:150px;float:left;"> Refered By :</label>
						   						<div class=" detail"><?php if($caseData->refered_name!=""){ echo $caseData->refered_name; }else { echo "N/A";} ?></div>
					    				    </div>
				    				    </div>
				    				    
				    				    <div class="col-md-2" style="text-align: right; padding: 0px;">
				    				      
						 		<table style="width: 100%;">
                                   
						 			<tr>
						 				<td style="font-size:14px;">Regular</td>
						 				<td style="font-size:14px; text-align: right;">
						 				<span class="charges"><?php
                   
                  // this format is string comparable
                    
                   
                     $date_now = date("Y-m-d"); 
                
                   if($packages->id =="0"){
                  
                    $dateexp = date('Y-m-d',strtotime("-1 days"));;
                   }
                   else{
                        $dateexp = date('Y-m-d',strtotime($medicalcase->date_left));
                   }
                  
                  if ($date_now > $dateexp && $paydays!="" ) {
                     echo $paydays->charges;
                  }else {
                      echo "0";
                  }
                    
						 				?></span></td>
						 				<td rowspan="2" style="text-align: right;">
                                          <button type="button" onclick="updatecharges(<?php echo $caseData->regid; ?>);"  style="display: inline-block;"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                            <form method="post" style="display: inline-block;">
                                              <input type="hidden" class="rid1" name="rand_id" value="<?php if($casept!=""){  echo $casept->rand_id; } else { echo "";} ?>" >
                                       
                                            <input type="hidden" name="rand_id" class="rid">
                                           
                                            <button type="button" class="Additionalcharges">+</button>
                                            </form>
                                        
                                        </td>
						 			</tr>
						 			<tr>
						 				<td style="font-size:14px;">Additional</td>
						 				<td style="font-size:14px; text-align: right;border-bottom: 1px solid #00000085;"><span class="additional"><?php if($paydays!=""){ echo $paydays->additional_price; }  ?></span></td>
						 			</tr>
						 			<tr>
						 				<td style="font-size:14px;">Total</td>
						 				<td style="font-size:14px; text-align: right;"><span class="total_chg"><?php if($caseData->scheme!=""){ echo "0"; } else{ echo $total_charges; } ?></span></td>
						 				<td rowspan="2" style="text-align: right;">
                                            <form method="post">
                                              <input type="hidden" class="rid_i" name="rand_id" value="<?php if($casept!=""){  echo $casept->rand_id; } else { echo "";} ?>" >
                                       
                                                <input type="hidden" name="rand_id" class="rid">
                                                <input type="hidden" class="casename" value="<?php echo $caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname; ?>">
                                                <input type="hidden" class="regidcase" value="<?php echo $caseData->id?>">
                                                <button type="button" style="width: 25px;" class="infomodal" >I</button>
                                            </form></td>
						 			</tr>
						 			<tr>
						 				<td style="font-size:14px;">Received</td>
						 				<td style="font-size:14px; text-align: right;border-bottom: 1px solid #00000085;"><span class="rece-chg"><?php if(isset($paydays->received_price) && $paydays->received_price!=""){ echo $paydays->received_price; } else{ echo "0"; } ?></span></td>
						 			</tr>
						 			<tr>
						 				<td style="font-size:14px;">Balance</td>
						 				<td style="font-size:14px; text-align: right;"><span class="total-bal"><?php 

                      if ($date_now > $dateexp && $paydays!="" ) {
                     echo $balance;
                  }else {
                      echo "0";
                  }

                     ?></span></td>
						 				<td style="text-align: right;"><button type="button" data-toggle="modal" data-target="#Recievedcharges">R</button></td>
						 			</tr>

						 		</table>
						 	
						 	
				    				    </div>
			    				    </div>
									
									</fieldset>
								</div>
						 	</td>
						 	
						 	
						</tr>
					</table>
					</fieldset>
					<!-- <div class="col-md-12">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-success completetata">Complete</button>
				
			</div> -->
				</div>
			
			
			{!! Form::close() !!}


			<!-- Add Height Weight -->
<div class="modal fade" id="HeightWeightEntry" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" style="width:800px">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Height Weight Entry</h4>
			</div>
			{!! Form::open(['action' => 'LA\MedicalcasesController@saveHeightWeights', 'id' => 'medicalcase-add-form5']) !!}
			   
				   {{--  @la_form($module) --}}
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}

                  <div class="form-group col-md-4 " class="patient-info-td">
						<label for="mobile" >Reg ID</label>
						<input class="form-control regid" placeholder="RegID" readonly  name="regid" type="text" value="{{ $caseData->regid }}" aria-required="true"required>
					</div>
					<div class="form-group col-md-12">
					    <div class="form-group col-md-8" style="padding-left:0">
    						<table>
    							<tr>
    								<td>
    									<table class="table table-bordered tb-bg heightweighttabledata">
    										<tr>
    											<th>Date</th>
    											<th>Height</th>
    											<th>Weight</th>
    											<!-- <th>BP</th>
    											<th>BP</th> -->
    											<th>LMP</th>
    											<th>&nbsp;</th>
    										</tr>
    										<?php foreach($HeightWeight as $CaseHeightWeight){ ?>
    									<tr id="<?php echo $CaseHeightWeight->rand_id;?>">
    									    <td><?php echo $CaseHeightWeight->dob;?></td>
    									    <td><?php echo $CaseHeightWeight->height;?></td>
    									    <td><?php echo $CaseHeightWeight->weight;?></td>
    									  <!--   <td><?php echo $CaseHeightWeight->blood_pressure;?></td>
    									    <td><?php echo $CaseHeightWeight->blood_pressure2;?></td> -->
    									    <td><?php echo $CaseHeightWeight->lmp;?></td>
    									    <td>
					   							<a href="javascript:void(0)" onclick="removenewdata(<?php echo $CaseHeightWeight->rand_id;?>);">Remove</a>
					   						</td>
    									</tr>
    									<?php } ?>
    										
    									</table>
    								</td>
    								
    							</tr>
    							
    						</table>
						</div>
					    <div class="form-group col-md-4" style="padding-left:0;padding-right:0">
					     <?php 
					    	$ht= "";
		    				$wt= "";
                            
					    	foreach($heightweight as $heightweightval) {
					    		if($heightweightval->months == $diff1 && $heightweightval->gender == $firstCharacter)
				    			{
				    				$ht= $heightweightval->height;
				    				$wt= $heightweightval->weight;
				    			}
                                elseif($heightweightval->gender == $firstCharacter && $diff1 > 240){
                                    $ht= $heightweightval->height;
                                    $wt= $heightweightval->weight;

                                }
				    		}
					    		?> 
					    		
					    	      
        						<table>
                                   
        							<tr>
        								<td>
        									
        									<!-- <div class="form-group col-md-12">
        										<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy"> -->
        										    <input class="form-control datevalht" type="hidden" name="dateval" readonly value="<?php if($caseh!=""){  echo $caseh->dob; } else { echo date('d/m/Y');} ?>"/>
                                                    <input type="hidden" class="randh" name="rand" value="<?php if($caseh!=""){  echo $caseh->rand_id; } else { echo rand('99',9999);} ?>">
        										    <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> -->
        										<!-- </div>
        								</div> -->
        								</td>
        							</tr>

        							<tr>
        								<td>
        								    <div class="form-group col-md-12">
            									<div class="form-group col-md-7" style="padding-left:0;padding-right:0">
            										<label for="mobile">Height:</label>
            										<input class="form-control height" placeholder="Height"  name="height" type="text" value="<?php if($caseh!=""){  echo $caseh->height; } else { echo "";} ?>" aria-required="true">
                                                    <input class="form-control heightid"  name="id" type="hidden" value="<?php if($caseh!=""){  echo $caseh->id; } else { echo "";} ?>" aria-required="true">
                                                    
            									</div>
                                                <div class="form-group col-md-5" style="padding-right: 0;padding-left: 3px;margin-top: 32px;color: red;">
                                                    <span><?php echo  $ht." CM"; ?></span>
                                                </div>
        									</div>
        								</td>
        							</tr>
        							<tr>
        								<td>
        								    <div class="form-group col-md-12">
            									<div class="form-group col-md-7" style="padding-left:0;padding-right:0">
            										<label for="mobile">Weight:</label>
            										<input class="form-control weight" placeholder="Weight"  name="weight" type="text" value="<?php if($caseh!=""){  echo $caseh->weight; } else { echo "";} ?>" aria-required="true">
            									</div>
                                                <div class="form-group col-md-5" style="padding-right: 0;padding-left: 3px;margin-top: 32px;color: red;">
                                                    <span><?php echo  $wt." KG"; ?></span>
                                                </div>
        									</div>
        									<input class="form-control dateval1" type="hidden"  readonly />
        									<input class="form-control height1" type="hidden"  readonly />
        									<input class="form-control weight1" type="hidden"  readonly />
        									<input class="form-control blood_pressure1" type="hidden" readonly />
        									<input class="form-control blood_pressure3" type="hidden"  readonly />
        									<input class="form-control lmpdata1" type="hidden" readonly />

        									<textarea class="allvalues" type="hidden" style="display: none;"></textarea>
        									<input  name="regid1" class="regid1" type="hidden">
        									 
        								</td>
        							</tr>
        						</table>
        					
						</div>
					</div>
					<div class="col-md-12">	
					
						<!-- <div class="form-group col-md-4">
							<label for="mobile">Blood Pressure :</label><br>
							<input class="form-control blood_pressure" placeholder=""  name="blood_pressure" type="text" value="<?php if($caseh!=""){  echo $caseh->blood_pressure; } else { echo "";} ?>" aria-required="true">
                            <input class="form-control blood_pressure2" placeholder=""  name="blood_pressure2" type="text" value="<?php if($caseh!=""){  echo $caseh->blood_pressure2; } else { echo "";} ?>" aria-required="true" style="width: 50%;">
						</div> -->
                        <?php 
                      
                         if($caseData->gender=="F"){ ?>
                            <fieldset class="fieldset" style="padding-top: 6px;">
						<div class="form-group col-md-6" style="margin-top: 0px;margin-bottom: 10px;">
							<label for="mobile">LMP :</label>
							<div id="datepicker" class="input-group date datepickerheight" data-date-format="dd-mm-yyyy">
							    <input class="form-control lmpdata" type="text" name="lmpdata" readonly value="<?php if($caseh!=""){  echo $caseh->lmp; } else { echo date('d-m-Y');} ?>"/>
							    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</div>
                        </fieldset>
                    <?php } ?>
					
				</div>
                 <?php
            if($HeightWeight!=""){
            foreach($HeightWeight as $CaseHeightWeight){ 
                $pdate = $CaseHeightWeight->dob;
                $deleted = $CaseHeightWeight->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                    <input class="form-control hdob" type="hidden" value="<?php echo $CaseHeightWeight->dob; ?>" />
                    <input class="form-control hheight" type="hidden"  value="<?php echo $CaseHeightWeight->height; ?>" />
                    <input class="form-control hweight" type="hidden"  value="<?php echo $CaseHeightWeight->weight; ?>" />
                    <input class="form-control hblood_pressure" type="hidden" value="<?php echo $CaseHeightWeight->blood_pressure; ?>" />
                    <input class="form-control hblood_pressure2" type="hidden"  value="<?php echo $CaseHeightWeight->blood_pressure2; ?>" />
                    <input class="form-control hlmp" type="hidden" value="<?php echo $CaseHeightWeight->lmp; ?>" />
            <?php   } } }

         ?>
                    <input class="form-control hdob" type="hidden" />
                    <input class="form-control hheight" type="hidden" />
                    <input class="form-control hweight" type="hidden"   />
                    <input class="form-control hblood_pressure" type="hidden" />
                    <input class="form-control hblood_pressure2" type="hidden"   />
                    <input class="form-control hlmp" type="hidden"  />
			</div>
			</div>
			<div class="modal-footer height-entry-footer">
				<button type="button" class="btn btn-success height-btn saveheightweight">Save</button>
				<button type="button" class="btn btn-success height-btn savefollowheightweight">Save Copy To Followup</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<!-- Add Communication Detail -->
<div class="modal fade" id="CommunicationDetail" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Communication Detail</h4>
			</div>
			{!! Form::open(['action' => 'LA\MedicalcasesController@communicationdetail', 'id' => 'medicalcase-add-form5']) !!}
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}
                  	<div class="form-group col-md-8" class="patient-info-td">
						<label for="mobile" class="patient-info-label-detail"><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
					</div>
					<div class="form-group col-md-4" class="patient-info-td">
						<label for="mobile" >Comments</label>
					</div>
					<div class="form-group col-md-12" style="padding-right: 0px;">
						<table style="width: 100%">
							<tr>
								<td rowspan="3" valign="top">
									<table class="table table-bordered tb-bg communicationdetailtable">
										<tr>
											<th>Date</th>
											<th>Comments</th>
                                            <th>&nbsp;</th>
										</tr>
										<?php foreach($CommunicationDetail as $CaseCommunicationDetail){ ?>
    									<tr id="<?php echo $CaseCommunicationDetail->rand_id;?>">
    									    <td><?php echo $CaseCommunicationDetail->currentdate;?></td>
    									    <td><?php echo $CaseCommunicationDetail->complaint_intesity;?></td>
    									    <td>
					   							<a href="javascript:void(0)" onclick="removecommunication(<?php echo $CaseCommunicationDetail->rand_id;?>);">Remove</a>
					   						</td>
    									</tr>
    									<?php } ?>
									</table>
								</td>
								<td style="width: 50%;">
									<div class="form-group col-md-12">
										<input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
										<input type="hidden" class="current_date" name="currentdate" value="<?php echo date('d/m/Y'); ?>">
										<input type="hidden" class="rand2" name="rand" value="<?php if($casecom!=""){  echo $casecom->rand_id; } else { echo rand('99',9999);} ?>">
										<textarea class="form-control complaint_intesity" placeholder="Complaint Intesity"  name="complaint_intesity" type="text" value="" aria-required="true" rows="4" required><?php if($casecom!=""){  echo $casecom->complaint_intesity; } else { echo "";} ?></textarea>
                                        <input type="hidden" name="id" value="<?php if($casecom!=""){  echo $casecom->id; } else { echo "";} ?>" class="comid">
									</div>
								</td>
							</tr>
						
						</table>

                         <?php
                        if($CommunicationDetail!=""){
                            foreach($CommunicationDetail as $CaseCommunicationDetail){
                                $pdate = $CaseCommunicationDetail->currentdate;
                                $deleted = $CaseCommunicationDetail->deleted_at;
                                $tdate = date('d/m/Y');
                                if($pdate == $tdate && $deleted==NULL){?>
                                    <input class="form-control ccurrentdate" type="hidden"  value="<?php echo $CaseCommunicationDetail->currentdate; ?>" />
                                    <input class="form-control ccomplaintintesity1" type="hidden"  value="<?php echo $CaseCommunicationDetail->complaint_intesity; ?>" />
                                   
                                
                            <?php   } } }  ?>

                            <input class="form-control ccurrentdate" type="hidden"  />
                            <input class="form-control ccomplaintintesity1" type="hidden" />
					</div>	
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				<button type="button" class="btn btn-success height-btn savecommunication">Save</button>
				<button type="button" class="btn btn-success height-btn savefollowcommunication">Save Copy To Followup</button>
				<!-- <button type="button" class="btn btn-success height-btn">Delete</button> -->
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<!-- Add Examination Report Modal -->
<div class="modal fade" id="ExaminationReport" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Examination Report</h4>
			</div>
			{!! Form::open(['action' => 'LA\MedicalcasesController@examinationdetail', 'id' => 'medicalcase-add-form5']) !!}
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}
                  	<div class="form-group col-md-12">
						<label for="mobile" ><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
					</div>	
					<fieldset class="examin-fieldset">
						<table class="table table-bordered tb-bg examinationtabledata">
							<tr>
								<th>Date</th>
								<th>Blood Pressure</th>
								<th>Blood Pressure</th>
								<!-- <th>Examination</th> -->
							</tr>

							<?php foreach($CaseExamination as $CaseCaseExamination){ ?>
								<tr id="<?php echo $CaseCaseExamination->rand_id;?>">
								    <td><?php echo $CaseCaseExamination->examination_date;?></td>
								    <td><?php echo $CaseCaseExamination->bp1;?></td>
								    <td><?php echo $CaseCaseExamination->bp2;?></td>
								    <!-- <td><?php echo $CaseCaseExamination->examination;?></td> -->
								    <td>
			   							<a href="javascript:void(0)" onclick="removeexamination(<?php echo $CaseCaseExamination->rand_id;?>);">Remove</a>
			   						</td>
								</tr>
							<?php } ?>
							
							
						</table>
						<div class="form-group col-md-12">
							<input class="regid" placeholder="RegID"  type="hidden" name="regid" value="<?php echo $caseData->regid; ?>">
							<input type="hidden" class="examination_date" name="examination_date" value="<?php echo date('d/m/Y'); ?>">
							<input type="hidden" class="examination_rand" name="examination_rand" value="<?php if($caseex!=""){  echo $caseex->rand_id; } else { echo rand('99',9999);} ?>">
							
						</div>

						<div class="col-md-12">	
						<fieldset class="fieldset">
							<div class="form-group col-md-12">
								<label for="mobile">Blood Pressure :</label><br>
								<input class="form-control examination_bp" placeholder=""  name="examination_bp" type="text" value="<?php if($caseex!=""){  echo $caseex->bp1; } else { echo "";} ?>" aria-required="true" style="width: 50%; display: block;  float: left;">
	                            <input class="form-control examination_bp1" placeholder=""  name="examination_bp1" type="text" value="<?php if($caseex!=""){  echo $caseex->bp2; } else { echo "";} ?>" aria-required="true" style="width: 50%;">
                                <input type="hidden" name="id" value="<?php if($caseex!=""){  echo $caseex->id; } else { echo "";} ?>" class="examid">
							</div>
							<!-- <div class="form-group col-md-6" style="margin-top: 0px;">
								<label for="mobile">Examination :</label>
								<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy"> -->
								    <input class="form-control exmdata" type="hidden" name="exmdata" readonly value="<?php echo date('d/m/Y');?>"/>
								    <!-- <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div> -->
						</fieldset>
					</div>
										<input class="form-control examination_date1" type="hidden"  readonly />
    									<input class="form-control examination_bp_1" type="hidden" readonly />
    									<input class="form-control examination_bp_2" type="hidden" readonly />
    									<input class="form-control exmdata1" type="hidden"  readonly />

    									<textarea class="allvalues" type="hidden" style="display: none;"></textarea>
    									<input   class="regid1" type="hidden">
						
					</fieldset>
                     <?php
                        if($CaseExamination!=""){
                            foreach($CaseExamination as $CaseCaseExamination){
                                $pdate = $CaseCaseExamination->examination_date;
                                $deleted = $CaseCaseExamination->deleted_at;
                                $tdate = date('d/m/Y');
                                if($pdate == $tdate && $deleted==NULL){?>
                                    <input class="form-control fexamination_date" type="hidden"  value="<?php echo $CaseCaseExamination->examination_date ?>" />
                                    <input class="form-control fexamination_bp" type="hidden" value="<?php echo $CaseCaseExamination->bp1 ?>" />
                                    <input class="form-control fexamination_bp1" type="hidden" value="<?php echo $CaseCaseExamination->bp2 ?>" />
                                    
                            <?php   } } }

                         ?>
                         <input class="form-control fexamination_date" type="hidden"   />
                        <input class="form-control fexamination_bp" type="hidden"  />
                        <input class="form-control fexamination_bp1" type="hidden"  />
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				<button type="button" class="btn btn-success height-btn saveexamination">Save</button>
				<button type="button" class="btn btn-success height-btn savefollowexamination">Save Copy To Followup</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<!-- Add Homeo Detail Model  -->

<div class="modal fade" id="HomeoDetail" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Homeo Detail</h4>
			</div>
			{!! Form::open(['action' => 'LA\MedicalcasesController@homeodetail', 'id' => 'medicalcase-add-form5']) !!}
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}
                  	<div class="form-group col-md-12">
						<label for="mobile" ><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
					</div>	
					<fieldset class="fieldset">
											<div class="form-group col-md-12" >
												<label for="mobile">Diagnosis :</label>
												<?php $disease_all  = array (
													 'Allergies' => 'Allergies',
													 'Asthma' => 'Asthma',
													 'Cancer' => 'Cancer',
													 'Celiac Disease' => 'Celiac Disease',
													 'Heart Disease' => 'Heart Disease',
													 'Infectious Diseases' => 'Infectious Diseases',
													 'Liver Disease' => 'Liver Disease',
													); ?>

                          <input type="text" list="cars" class="form-control diagnosis" name="diagnosis" value="<?php if($casehomeo!=""){ echo $casehomeo->diagnosis; }?>"/>
                          <datalist id="cars">
                              <option value="N/A">N/A</option>
                              <?php foreach($diseases as $value){?>
                              <option value="<?php echo $value->diseases;?>" <?php if($casehomeo!=""){ if($casehomeo->diagnosis==$value->diseases){ echo "selected";}  } else { echo "";} ?> ><?php echo $value->diseases;?></option>
                              <?php }?>
                          </datalist>
                          
				        					<!-- 	<select class="form-control select2-hidden-accessible diagnosis" required="1" data-placeholder="diagnosis" rel="select2" name="diagnosis" tabindex="-1" aria-hidden="true" aria-required="true" required>
                              <option value="N/A">N/A</option>
				        							<?php foreach($diseases as $value){?>
				        							<option value="<?php echo $value->diseases;?>" <?php if($casehomeo!=""){ if($casehomeo->diagnosis==$value->diseases){ echo "selected";}  } else { echo "";} ?> ><?php echo $value->diseases;?></option>
				        							<?php }?>
				        						</select> -->
											</div>
											<div class="form-group col-md-12">
												<label for="mobile">Complaint Intesity :</label>
												<textarea class="form-control complaint_intesity_h" placeholder="Complaint Intesity"  name="complaint_intesity" type="text" value="" aria-required="true" rows="4" required><?php if($casehomeo!=""){  echo $casehomeo->complaint_intesity; } else { echo "";} ?></textarea>
                                                <input type="hidden" name="id" value="<?php if($casehomeo!=""){  echo $casehomeo->id; } else { echo "";} ?>" class="homeoid">
											</div>

											
											<div class="col-md-12">
												<label for="name">Medication Taking:</label>
											</div>
											 <div class="customer_records">
											  	<div class="col-md-4">
											  	    <input type="text" name="medication_taking[]" style="height:30px; font-size:14px;" class="form-control input-lg country_name" id="country_name_0" placeholder="Enter Medicine Name" autocomplete="off" value="<?php if($casehomeo!=""){  echo $casehomeo->medication_taking; } else { echo "";} ?>" />
											  	    <div class="countryList" id="countryList_0">
                                                    </div>
										  	    
										  	    <?php /* ?>
										  	    <select class="form-control select2-hidden-accessible medication_taking" required="1" data-placeholder="diagnosis" rel="select2" name="medication_taking" id="medicine_name" required onchange="getRelatedDiseases(this);" tabindex="-1" aria-hidden="true" aria-required="true" required>
				        							<?php foreach($medicinetaking as $value){?>
				        							    <option value="<?php echo $value->id ?>"><?php echo $value->medicine_name;?></option>
				        							<?php }?>
				        						</select>
											    
											    <select class="form-control medication_taking" data-placeholder="Name"  name="medication_taking[]" id="medicine_name" required onchange="medicine_price_get(this);" >
											    		<option disabled selected>Select Name</option>
							        							<?php foreach($stocks as $stname){?>
							        							<option value="{{ $stname->description }}" data-price="{{ $stname->description }}" <?php if($casehomeo!=""){ if($casehomeo->medication_taking==$stname->description){ echo "selected";}  } else { echo "";} ?> >{{ $stname->description }}</option>
							        							<?php }?>
												</select>
												<?php */ ?>
												<input type="hidden" name="medicine_price[]" id="medicine_price" class="medicine_price">
												</div>
												<div class="col-md-2" style="padding: 0px; width: 80px; margin-right:5px;">
									          		<?php  $medicine_time  = array (
														 'Once' => 'Once',
														 'Twice' => 'Twice',
														 'Thrice' => 'Thrice',
														 'Bed Time' => 'Bed Time',
														 'Empty Stomach' => 'Empty Stomach',
														); ?>
					        						<select style="height:30px; font-size:14px;" class="form-control  medicine_time"  data-placeholder="diagnosis" name="medicine_time[]" tabindex="-1" aria-hidden="true"  >
					        							<?php foreach($medicine_time as $key=>$value){?>
					        							<option value="<?php echo $value;?>" <?php if($casehomeo!=""){ if($casehomeo->medicine_time==$value){ echo "selected";}  } else { echo "";} ?> ><?php echo $value;?></option>
					        							<?php }?>
					        						</select>
								          		</div>
									          	<div class="col-md-3" style="padding: 0px;">
									          		
                                                        <select class="form-control medicine_for" style="height:30px; font-size:14px;" id="medicine_for_0"  data-placeholder="diagnosis" name="medicine_for[]" tabindex="-1" aria-hidden="true" >
                                                          <option value="" ></option>
                                                           <!--  <option></option> -->
					        						</select>
								          		</div>
													
										    	<div class="col-md-3">
											    	<a class="btn btn-success extra-fields-customer" href="#" style="width: 59px;margin-left: -10px;margin-bottom: 10px;padding: 5px 1px 4px 1px;" >Add</a>
										    	</div>
											  </div>

											  <div class="customer_records_dynamic"></div>
									
											<div class="form-group col-md-12">
												<label for="mobile">Investigation :</label>
												<textarea class="form-control investigation" placeholder="Investigation" name="investigation" type="text" value=""  rows="4"><?php if($casehomeo!=""){  echo $casehomeo->investigation; } else { echo "";} ?></textarea>
											</div>

											<div class="form-group col-md-12">
												<label for="name">Case Taken By:</label>
												<select class="form-control select2-hidden-accessible case_taken" required="1" data-placeholder="Select Case Taken" rel="select2" name="case_taken" tabindex="-1" aria-hidden="true" >
												    <?php foreach($doctors as $doctorslist){?>
                        							    <option value="<?php echo $doctorslist->name;?>" <?php if($casehomeo!=""){ if($casehomeo->case_taken==$doctorslist->name){ echo "selected";}  } else { echo "";} ?> ><?php echo $doctorslist->name;?></option>
                        							<?php } ?>
												</select>
											</div>


                                          <?php 
                                          if($casehomeo!=""){
                                            $str = explode(",",$casehomeo->criteria);
                                            $people = $str;
                                            }
                                            ?>
											<div class="col-md-12">
												<label for="name">Criteria:</label>
											</div>
											<div class="form-group col-md-12">
                        <label class="container-checkbox" style="display: none;">
                                                  <input type="checkbox" value="N/A" name="criteria[]" class="criteria" checked>
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="container-checkbox" style="padding-left: 26px;margin-right: 12px;">Skin
                                                  <input type="checkbox" value="Skin" name="criteria[]" class="criteria" <?php if($casehomeo!=""){ if (in_array("Skin", $people)) { echo "checked"; } } else{ echo " ";} ?> >
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="container-checkbox" style="padding-left: 26px;margin-right: 12px;">Disability Disorder
                                                  <input type="checkbox" value="Disability Disorder" name="criteria[]" class="criteria" <?php if($casehomeo!=""){ if (in_array("Disability Disorder", $people)) { echo "checked"; } } else{ echo " ";} ?>>
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="container-checkbox" style="padding-left: 26px;margin-right: 12px;">PCOD
                                                  <input type="checkbox" value="PCOD" name="criteria[]" class="criteria" <?php if($casehomeo!=""){ if (in_array("PCOD", $people)) { echo "checked"; } } else{ echo " ";} ?>>
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="container-checkbox" style="padding-left: 26px;margin-right: 12px;">Thyroid
                                                  <input type="checkbox" value="Thyroid" name="criteria[]" class="criteria" <?php if($casehomeo!=""){ if (in_array("Thyroid", $people)) { echo "checked"; } } else{ echo " ";} ?>>
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="container-checkbox" style="padding-left: 26px;margin-right: 12px;">Lifestyle Disorder
                                                  <input type="checkbox" value="Lifestyle Disorder" name="criteria[]" class="criteria" <?php if($casehomeo!=""){ if (in_array("Lifestyle Disorder", $people)) { echo "checked"; } } else{ echo " ";} ?>>
                                                  <span class="checkmark"></span>
                                                </label>

                                                <label class="container-checkbox" style="padding-left: 26px;margin-right: 12px;">Other
                                                  <input type="checkbox" value="Other" name="criteria[]" class="criteria" <?php if($casehomeo!=""){ if (in_array("Other", $people)) { echo "checked"; } } else{ echo " ";} ?>>
                                                  <span class="checkmark"></span>
                                                </label>
                                            </div>
    							       
										</fieldset>
										<input type="hidden" class="homeo_date" name="homeo_date" value="<?php echo date('d/m/Y'); ?>">
										<input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
										<input class="consultation_fee" placeholder="RegID"  type="hidden" name="total_charges" value="{{ $caseData->consultation_fee }}">
							
					

						<input class="form-control diagnosis1" type="hidden"  readonly value="<?php if($casehomeo!=""){  echo $casehomeo->diagnosis; } ?>" />
						<input class="form-control complaint_intesity1" type="hidden" readonly value="<?php if($casehomeo!=""){  echo $casehomeo->complaint_intesity; } ?>" />
						<input class="form-control medication_taking1" type="hidden"  readonly />
						<input class="form-control medicine_price1" type="hidden" readonly />
						<input class="form-control medicine_for" type="hidden"  readonly />
						<input class="form-control medicine_time1" type="hidden"  readonly />
						<input class="form-control investigation1" type="hidden" readonly value="<?php if($casehomeo!=""){  echo $casehomeo->investigation; } ?>"/>
						<input class="form-control case_taken1" type="hidden" readonly />
						<input type="hidden" class="homeo_date1" name="invest_date" value="<?php echo date('d/m/Y'); ?>">
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				<button type="button" class="btn btn-success height-btn savehomeo">Save</button>
				<button type="button" class="btn btn-success height-btn savefollowhomeo">Save Copy To Followup</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<!-- Add Image Modal -->
<div class="modal fade" id="AddImage" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Image</h4>
			</div>
			{!! Form::open(['action' => 'LA\MedicalcasesController@picturedetail', 'id' => 'medicalcase-add-form5','files'=>true]) !!}
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}
                  	<div class="col-md-12">
						<label for="mobile" ><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
					</div>	
					<fieldset class="examin-fieldset">
						
							<input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
							<input type="hidden" class="pic_date" name="pic_date" value="<?php echo date('Y-m-d'); ?>">
							<input type="hidden" class="rand_id" name="rand_id" value="<?php echo rand('99',9999);?>">

							<div class="col-md-12" style="margin-bottom: 10px;">
								<label for="mobile">Choose Image :</label><br>
								<input class="form-control picture" placeholder=""  name="picture" type="file" value="" aria-required="true">
	                           
							</div>
										
						
					</fieldset>
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				<button type="submit" class="btn btn-success height-btn savepicture">Save</button>
				 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<!-- Show Image Modal -->
<div class="modal fade" id="PictureShow" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Images</h4>
			</div>
			<div class="modal-body">
				<div class="box-body">
                  {{--  @la_form($module) --}}
                  	<div class="form-group col-md-12">
						<label for="mobile" ><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
					</div>	
					<fieldset class="examin-fieldset">
						<div class="col-md-12">	
								<?php foreach ($CaseImage as $CaseImageval) { ?>
							<div class="form-group col-md-4" style="margin-bottom: 10px; margin-top: 5px; text-align: center;" id="<?php echo $CaseImageval->rand_id;?>">
								<a href="{{ asset('la-assets/img/'.$CaseImageval->picture) }}" target="_blank" ><img src="{{ asset('la-assets/img/'.$CaseImageval->picture) }}" style="height: 150px; width: 150px;"></a>
								<p style="margin-bottom: 1px;">Added At : <?php echo date('d/m/Y', strtotime($CaseImageval->created_at));?></p>
								<a href="javascript:void(0)" onclick="removeimage(<?php echo $CaseImageval->rand_id;?>)" style="color: red; font-weight: bold;">Remove</a>
							</div>
									
								<?php } ?>
	                           
							
							
						
					</div>
										
						
					</fieldset>
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				
			</div>
			
		</div>
	</div>
</div>


<!-- Family Group Modal -->
<div class="modal fade" id="Familygroup" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Family Members</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}
                    <div class="form-group col-md-12" >
                        <label for="mobile" ><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
                    </div> 
                    <?php   if(!empty($family)){ ?> 
                    <fieldset class="examin-fieldset" >
                        
                           
                                <table class="table table-bordered tb-bg examinationtabledata" style="margin-bottom: 0px;">
                            <tr>
                                <th>RegID</th>
                                <th>Name</th>
                                <!-- <th>Examination</th> -->
                            </tr>

                            <?php
                           
                              foreach ($family as $familyval) { 
                               
                                ?>
                                <tr>
                                    <td><?php echo $familyval->regid;?></td>
                                    <td><?php echo $familyval->first_name;?></td>
                                </tr>
                            <?php }  ?>
                            
                            
                        </table> 
                    </fieldset>
                <?php } else { echo "<div class='col-md-12' style='padding: 10px;text-align: center; font-size: 18px;background: #ffff;color: red;'>No family grouping</div>"; } ?>  
                </div>
            </div>
            <div class="modal-footer height-entry-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            
        </div>
    </div>
</div>
<!-- Add Additional Charges -->
<div class="modal fade" id="Additionalcharges" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Additional Charges</h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@updateadditional', 'id' => 'medicalcase-add-form5']) !!}
            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}
                    <div class="form-group col-md-12" class="patient-info-td">
                        <label for="mobile" class="patient-info-label-detail"><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
                    </div>
                      <div class="form-group col-md-12">
                                        <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                        <input type="hidden" class="current_date" name="currentdate" value="<?php echo date('d/m/Y'); ?>">
                                       
                                            <input type="hidden" class="rid1" name="rand_id" value="<?php if($casept!=""){  echo $casept->id; } else { echo "";} ?>" >
                                       
                                            <input type="hidden" name="rand_id" class="rid">
                                            <input type="hidden" class="add_chg" value="<?php if($casept!=""){  echo $casept->additional_price; } else { echo "";} ?>">
                                            <input type="hidden" class="reg_chg" value="<?php if($casept!=""){  echo $casept->charges; } else { echo "";} ?>">
                                        <select class="form-control select2-hidden-accessible additional_name_m" required="1" data-placeholder="Expenses" rel="select2" name="additional_name" tabindex="-1" aria-hidden="true" aria-required="true"required onchange="getadditional(this);">
                                            <?php foreach($chargesadd as $chargesval){?>
                                                    <option value="<?php echo $chargesval->id;?>" data-price="<?php echo $chargesval->amount;?>"><?php echo $chargesval->charges;?></option>
                                                <?php } ?>
                                        </select>   
                                        <!-- <select class="form-control additional_name_m" name="additional_name">
                                            <option value="">Select</option>
                                            <option value="Regular Charges">Regular Charges</option>
                                            <option value="Courier Charges">Courier Charges</option>
                                            <option value="Package Charges">Package Charges</option>
                                            <option value="Consulting Charges">Consulting Charges</option>
                                            <option value="KIT">KIT</option>
                                            <option value="None">None</option>
                                        </select> -->
                                    </br>
                                        <input type="text" name="additional_price" class="form-control additional_price" placeholder="Price">
                                        <input type="hidden" name="id" value="<?php if($casept!=""){  echo $casept->id; } else { echo "";} ?>" class="potid">
                                  

                         <?php
                        if($CasePotency!=""){
                            foreach($CasePotency as $CasePotencyvalue){
                                $pdate = $CasePotencyvalue->dateval;
                                $deleted = $CasePotencyvalue->deleted_at;
                                $tdate = date('d/m/Y');
                                if($pdate == $tdate && $deleted==NULL){?>
                                    <input class="form-control dcurrentdate" type="hidden"  value="<?php echo $CasePotencyvalue->dateval; ?>" />
                                    <input class="form-control additional_name1" type="hidden"  value="<?php echo $CasePotencyvalue->additional_name; ?>" />
                                    <input class="form-control additional_price1" type="hidden"  value="<?php echo $CasePotencyvalue->additional_price; ?>" />
                                   
                                
                            <?php   } } }  ?>

                            <input class="dcurrentdate" type="hidden" />
                            <input class="additional_name1" type="hidden" />
                            <input class="additional_price1" type="hidden" />
                    </div>  
                    <div class="form-group col-md-12" style="padding-right: 0px; height: 173px;">
                         <div class="scroll-bar-wrap">
                                 <div class="scroll-box" style="height: 160px;">
                        <table style="width: 100%; ">
                                  
                            <tr>
                                <td rowspan="3" valign="top">
                                    <table class="table table-bordered tb-bg chargestable">
                                        <tr>
                                            <th>Date</th>
                                            <th>Charges</th>
                                            <th>Amount</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        <tbody class="additional-body">
                                            
                                        </tbody>
                                    </table>
                                </td>
                              
                            </tr>
                        
                        </table>
                      </div>
                    </div>
                      </div>
                     
                </div>
            </div>
            <div class="modal-footer height-entry-footer">
                <button type="button" class="btn btn-success height-btn savecharges">Update</button>
                <!-- <button type="button" class="btn btn-success height-btn">Delete</button> -->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<div class="modal fade" id="Updatecharges" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Charges</h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@updatecharges', 'id' => 'medicalcase-add-form5']) !!}
            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}
                    <div class="form-group col-md-12" class="patient-info-td">
                        <label for="mobile" class="patient-info-label-detail"><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
                    </div>
                      <div class="form-group col-md-12">
                                        <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                       <div class="form-group col-md-12">
                                               
                                                    <label for="mobile">Regular Charges:</label>
                                                    <input class="form-control regcharges" placeholder=""  name="regcharges" type="text" value="" aria-required="true">

                                                    <input type="hidden" name="chgid" class="chgid">
                                               
                                                
                                        </div>

                                        <div class="form-group col-md-12">
                                               
                                                    <label for="mobile">Update Charges:</label>
                                                    <input class="form-control updatechg" placeholder=""  name="updatechg" type="text" value="" aria-required="true">
                                               
                                                
                                        </div>
                                    
                         
                    </div>  
                 
                     
                </div>
            </div>
            <div class="modal-footer height-entry-footer">
                <button type="submit" class="btn btn-success height-btn updatechrgs">Update</button>
                <!-- <button type="button" class="btn btn-success height-btn">Delete</button> -->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Add Received Charges -->
<div class="modal fade" id="Recievedcharges" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Received Charges</h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@updatereceived', 'id' => 'medicalcase-add-form5']) !!}
            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}
                    <div class="form-group col-md-12" class="patient-info-td">
                        <label for="mobile" class="patient-info-label-detail"><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
                    </div>
                    
                    <div class="form-group col-md-12" style="padding-right: 0px;">
                        <table style="width: 100%">
                            <tr>
                                
                                <td style="width: 50%;">
                                    <div class="form-group col-md-12">
                                        <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                        <input type="hidden" class="current_date" name="currentdate" value="<?php echo date('d/m/Y'); ?>">
                                       <input type="hidden" class="rid1" name="rand_id" value="<?php if($casept!=""){  echo $casept->id; } else { echo "";} ?>" >
                                       
                                            <input type="hidden" name="rand_id" class="rid">

                                            <?php if($casept!=""){ 
                                                $total =0;
                                                $total =  $casept->charges + $casept->additional_price ; 
                                                }  ?>
                                             <input type="hidden"  class="tot_chg" value="<?php if($casept!=""){  echo $total; } ?>">
                                             <input type="hidden"  class="rec_pot" value="<?php if($casept!=""){  echo $casept->received_price; } ?>">
                                        <input type="text" name="received_price" class="form-control received_price" placeholder="Price">
                                       
                                    </div>
                                </td>
                            </tr>
                        
                        </table>

                        
                    </div>  
                </div>
            </div>
            <div class="modal-footer height-entry-footer">
                <button type="button" class="btn btn-success height-btn savereceived">Update</button>
                <!-- <button type="button" class="btn btn-success height-btn">Delete</button> -->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Add Info Charges -->
<div class="modal fade" id="InfoModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Account of <?php echo $caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname; ?></h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@updatereceived', 'id' => 'medicalcase-add-form5']) !!}
            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}
                   
                    
                    <div class="form-group col-md-12" style="padding-right: 0px;">
                         <div class="scroll-bar-wrap">
                                 <div class="scroll-box" style="height: 320px;">
                        <table class="table table-bordered tb-bg" style="width: 100%">
                            <tr>
                                <th>RegID</th>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Detail</th>
                                <th>Charges</th>
                                <th>Paid</th>
                            </tr>
                             <tbody class="info-body">
                                            
                            </tbody>

                        
                        </table>
                      </div>
                    </div>

                        
                    </div>  
                </div>
            </div>
            <div class="modal-footer height-entry-footer">
              
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Show Graph Modal -->
<div class="modal fade" id="GraphDetail" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content medicalcase_model">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Graph</h4>
			</div>
			<div class="modal-body">
				<div class="box-body">
	               <!--  <div id="chartContainer" style="height: 400px; width: 100%;"></div>
					<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
                    <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="col-md-2" style="width: 12%;"><input type="radio" name="graph" checked="checked" value="1" />&nbsp;All</div>
                    <div class="col-md-2" style="width: 20%;"><input type="radio" name="graph" value="2" />&nbsp; Height</div>
                    <div class="col-md-3" style="width: 20%;"><input type="radio" name="graph" value="3" />&nbsp; Weight</div>
                    <div class="col-md-3" style="width: 20%;"><input type="radio" name="graph" value="4" />&nbsp; BP Low</div>
                    <div class="col-md-3" style="width: 20%;"><input type="radio" name="graph" value="5" />&nbsp; BP Hight</div>
                    </div>
                    <div id="graph1" class="col-md-12 desc">
                        <div id="chartContainer" style="height: 400px; width: 100%;"></div>
                    </div>
                    <div id="graph2" class="col-md-12  desc" style="display: none;">
                      <div id="chartContainer1" style="height: 400px; width: 100%;"></div>
                    </div>
                    <div id="graph3" class="col-md-12  desc" style="display: none;">
                      <div id="chartContainer2" style="height: 400px; width: 100%;"></div>
                    </div>
                    <div id="graph4" class="col-md-12  desc" style="display: none;">
                      <div id="chartContainer3" style="height: 400px; width: 100%;"></div>
                    </div>
                    <div id="graph5" class="col-md-12  desc" style="display: none;">
                      <div id="chartContainer4" style="height: 400px; width: 100%;"></div>
                    </div>
					
					<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
				</div>
			</div>
			<div class="modal-footer height-entry-footer">
				 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				
			</div>
			
		</div>
	</div>
</div>

<div class="modal fade" id="DetailInvestigation" role="dialog" aria-labelledby="myModalLabel">
     <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Investigation Report</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                   <div class="col-md-12" style="margin-bottom: 5px;">
                        <label for="mobile" ><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
                    </div>  
                   <div id="exTab1" class="container">  
                        <ul  class="nav nav-pills">
                                    <li class="<?php if($countcbc!=0){ echo "active";} ?>"><a  href="#1a" data-toggle="tab">CBC</a></li>
                                    <li class="<?php if($counturine!=0){ echo "active";} ?>"><a href="#2a" data-toggle="tab">Urine</a></li>
                                    <li class="<?php if($countstool!=0){ echo "active";} ?>"><a href="#3a" data-toggle="tab">Stool</a></li>
                                    <li class="<?php if($countarithris!=0){ echo "active";} ?>"><a href="#4a" data-toggle="tab">Arthritis</a> </li>
                                    <li class="<?php if($countendocrine!=0){ echo "active";} ?>"><a href="#5a" data-toggle="tab">Endocrine</a> </li>
                                    <li class="<?php if($countrenal!=0){ echo "active";} ?>"><a href="#6a" data-toggle="tab">Renal Profile</a> </li>
                                    <li class="<?php if($countxray!=0){ echo "active";} ?>"><a href="#7a" data-toggle="tab">X-ray - CT - MRI</a> </li>
                                    <li class="<?php if($countfemale!=0){ echo "active";} ?>"><a href="#8a" data-toggle="tab">USG Female</a> </li>
                                    <li class="<?php if($countmale!=0){ echo "active";} ?>"><a href="#9a" data-toggle="tab">USG Male</a> </li>
                                    <li class="<?php if($countimmunology!=0){ echo "active";} ?>"><a href="#10a" data-toggle="tab">Immunology</a> </li>
                                    <li class="<?php if($countspecific!=0){ echo "active";} ?>"><a href="#11a" data-toggle="tab">Specific</a> </li>
                                    <li class="<?php if($countliver!=0){ echo "active";} ?>"><a href="#12a" data-toggle="tab">Liver Profile</a> </li>
                                    <li class="<?php if($countlipid!=0){ echo "active";} ?>"><a href="#13a" data-toggle="tab">Lipid Profile</a> </li>
                                    <li class="<?php if($countdiabetes!=0){ echo "active";} ?>"><a href="#14a" data-toggle="tab">Diabetes Profile</a> </li>
                                    <li class="<?php if($countcardiac!=0){ echo "active";} ?>"><a href="#15a" data-toggle="tab">Cardiac Profile</a> </li>
                                    <li class="<?php if($countsero!=0){ echo "active";} ?>"><a href="#16a" data-toggle="tab">Serology</a> </li>
                                    <li class="<?php if($countsemen!=0){ echo "active";} ?>"><a href="#17a" data-toggle="tab">Semen Analysis</a> </li>

                                </ul>
                                <div class="tab-content clearfix">

                                <!-- CBC -->
                                <div class="tab-pane active" id="1a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker"  data-date-format="dd-mm-yyyy">
                                                        <input class="form-control datesearch investdate" type="text" value="<?php echo date('d-m-Y');?>" name="datesearch" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>     
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchcbc">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                       <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>HB</th>
                                                  <th>Neutrophils</th>
                                                  <th>R.B.C</th>
                                                  <th>Lymphocytes</th>
                                                  <th>W.B.C</th>
                                                  <th>Eosinophils</th>
                                                  <th>Platelets</th>
                                                  <th>Monocytes</th>
                                                  <th>Vitamin B-12</th>
                                                  <th>Basophils</th>
                                                  <th>25-Oh Vitamin D(Total)</th>
                                                  <th>Band Cells</th>
                                                  <th>Abnor R.B.C</th>
                                                  <th>Parasites</th>
                                                  <th>Abnor W.B.C</th>
                                                  <th>E.S.R</th>
                                                </tr>
                                                <?php foreach($casecbc_show as $val){ ?>
                                              <tr>
                                                  <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->hb;?></td>
                                                  <td><?php echo $val->neutrophils;?></td>
                                                  <td><?php echo $val->rbc;?></td>
                                                  <td><?php echo $val->lymphocytes;?></td>
                                                  <td><?php echo $val->wbc;?></td>
                                                  <td><?php echo $val->eosinophils;?></td>
                                                  <td><?php echo $val->platelets;?></td>
                                                  <td><?php echo $val->monocytes;?></td>
                                                  <td><?php echo $val->vitaminb;?></td>
                                                  <td><?php echo $val->basophils;?></td>
                                                  <td><?php echo $val->vitamind;?></td>
                                                  <td><?php echo $val->band_cells;?></td>
                                                  <td><?php echo $val->abnor_rbc;?></td>
                                                  <td><?php echo $val->parasites;?></td>
                                                  <td><?php echo $val->abnor_wbc;?></td>
                                                  <td><?php echo $val->esr;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                            </div>
                                </div>

                                <!-- Urine  -->
                                <div class="tab-pane" id="2a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control urinedate investdate" type="text" name="urinedate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>   
                                                    <input type="hidden" name="savedate1" class="savedate1">  
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchurine1">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                       <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Quantity</th>
                                                  <th>Protein</th>
                                                  <th>Color</th>
                                                  <th>Sugar</th>
                                                  <th>Appearance</th>
                                                  <th>Acetone Bodies</th>
                                                  <th>Reaction</th>
                                                  <th>Bile Pigments</th>
                                                  <th>Specific GRA</th>
                                                  <th>Occult blood</th>
                                                  <th>Urobilinogen</th>
                                                  <th>Epith Cells</th>
                                                  <th>Red Blood Cells</th>
                                                  <th>Pus Cells</th>
                                                  <th>Other Findings</th>
                                                </tr>
                                                <?php foreach($caseurine_show as $val){ ?>
                                              <tr>
                                                  <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->quantity;?></td>
                                                  <td><?php echo $val->protein;?></td>
                                                  <td><?php echo $val->color;?></td>
                                                  <td><?php echo $val->sugar;?></td>
                                                  <td><?php echo $val->appearance;?></td>
                                                  <td><?php echo $val->acetone;?></td>
                                                  <td><?php echo $val->reaction;?></td>
                                                  <td><?php echo $val->pigments;?></td>
                                                  <td><?php echo $val->gra;?></td>
                                                  <td><?php echo $val->occult_blood;?></td>
                                                  <td><?php echo $val->urobilinogen;?></td>
                                                  <td><?php echo $val->epith_cells;?></td>
                                                  <td><?php echo $val->rbc;?></td>
                                                  <td><?php echo $val->pus_cells;?></td>
                                                  <td><?php echo $val->other_findings;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                 <!-- Stool profile  -->
                                <div class="tab-pane" id="3a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control stooldate investdate" type="text" name="stooldate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>    
                                                    <input type="hidden" name="savedate1" class="savedate1">   
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchstool">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>

                                      <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Color</th>
                                                  <th>Occult blood</th>
                                                  <th>Consistency</th>
                                                  <th>Macrophages</th>
                                                  <th>Mucus</th>
                                                  <th>R.B.C</th>
                                                  <th>Frank Blood</th>
                                                  <th>Ova</th>
                                                  <th>Adult Worm</th>
                                                  <th>Cysts</th>
                                                  <th>Reaction</th>
                                                  <th>Protozoa</th>
                                                  <th>Pus Cells</th>
                                                  <th>Yeast Cells</th>
                                                  <th>Other Findings</th>
                                                </tr>
                                                <?php foreach($casestool_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->color;?></td>
                                                  <td><?php echo $val->occult_blood;?></td>
                                                  <td><?php echo $val->consistency;?></td>
                                                  <td><?php echo $val->macrophages;?></td>
                                                  <td><?php echo $val->mucus;?></td>
                                                  <td><?php echo $val->rbc;?></td>
                                                  <td><?php echo $val->frank_blood;?></td>
                                                  <td><?php echo $val->ova;?></td>
                                                  <td><?php echo $val->adult_worm;?></td>
                                                  <td><?php echo $val->cysts;?></td>
                                                  <td><?php echo $val->reaction;?></td>
                                                  <td><?php echo $val->protozoa;?></td>
                                                  <td><?php echo $val->pus_cells;?></td>
                                                  <td><?php echo $val->yeast_cell;?></td>
                                                  <td><?php echo $val->other_findings;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                      
                                   
                                </div>

                                <!-- Arithrist profile  -->
                                <div class="tab-pane" id="4a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control arithrisdate investdate" type="text" name="arithrisdate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>     
                                                    <input type="hidden" name="savedate1" class="savedate1">  
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searcharithris">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Anti-streptolysin O</th>
                                                  <th>ACCP</th>
                                                  <th>RA Factor</th>
                                                  <th>Alkaline Phosphatase</th>
                                                  <th>Complement4 (C4)</th>
                                                  <th>ANA</th>
                                                  <th>C-Reactive Protein</th>
                                                </tr>
                                                <?php foreach($casearithris_show as $val){ ?>
                                              <tr>
                                                  <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->anti_o;?></td>
                                                  <td><?php echo $val->accp;?></td>
                                                  <td><?php echo $val->ra_factor;?></td>
                                                  <td><?php echo $val->alkaline;?></td>
                                                  <td><?php echo $val->c4;?></td>
                                                  <td><?php echo $val->ana;?></td>
                                                  <td><?php echo $val->c_react;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                            </div>
                                   
                                </div>

                                <!-- Endocrine profile  -->
                                <div class="tab-pane" id="5a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control endocrinedate investdate" type="text" name="endocrinedate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>  

                                                    <input type="hidden" name="savedate1" class="savedate1">     
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchendocrine">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>T3</th>
                                                  <th>LSH Surge</th>
                                                  <th>T4</th>
                                                  <th>Progesterone Day3</th>
                                                  <th>TSH</th>
                                                  <th>Progesterone</th>
                                                  <th>FT3</th>
                                                  <th>DHEA-S</th>
                                                  <th>FT4</th>
                                                  <th>Testosterone</th>
                                                  <th>Anti TPO</th>
                                                  <th>AMA</th>
                                                  <th>Antibody Thyroglobulin</th>
                                                  <th>Fasting Insulin</th>
                                                  <th>Prolactin</th>
                                                  <th>Fasting Glucose</th>
                                                  <th>FSH Day3</th>
                                                </tr>
                                                <?php foreach($caseendocrine_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->t3;?></td>
                                                  <td><?php echo $val->lsh;?></td>
                                                  <td><?php echo $val->t4;?></td>
                                                  <td><?php echo $val->progesterone_3;?></td>
                                                  <td><?php echo $val->tsh;?></td>
                                                  <td><?php echo $val->progesterone;?></td>
                                                  <td><?php echo $val->ft3;?></td>
                                                  <td><?php echo $val->dhea;?></td>
                                                  <td><?php echo $val->ft4;?></td>
                                                  <td><?php echo $val->testosterone;?></td>
                                                  <td><?php echo $val->anti_tpo;?></td>
                                                  <td><?php echo $val->ama;?></td>
                                                  <td><?php echo $val->antibody;?></td>
                                                  <td><?php echo $val->insulin;?></td>
                                                  <td><?php echo $val->prolactin;?></td>
                                                  <td><?php echo $val->glucose;?></td>
                                                  <td><?php echo $val->fsh;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                </div>

                                <!-- Renal profile  -->
                                <div class="tab-pane" id="6a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control renaldate investdate" type="text" name="renaldate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>

                                                    <input type="hidden" name="savedate1" class="savedate1">       
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchrenal">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                         <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>B.U.N</th>
                                                  <th>Phosphorus</th>
                                                  <th>Urea</th>
                                                  <th>Sodium</th>
                                                  <th>Creatinine</th>
                                                  <th>Potassium</th>
                                                  <th>Uric Acid</th>
                                                  <th>Chloride</th>
                                                  <th>Calcium</th>
                                                  
                                                </tr>
                                                <?php foreach($caserenal_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->bun;?></td>
                                                  <td><?php echo $val->phosphorus;?></td>
                                                  <td><?php echo $val->urea;?></td>
                                                  <td><?php echo $val->sodium;?></td>
                                                  <td><?php echo $val->creatinine;?></td>
                                                  <td><?php echo $val->potassium;?></td>
                                                  <td><?php echo $val->uric_acid;?></td>
                                                  <td><?php echo $val->chloride;?></td>
                                                  <td><?php echo $val->calcium;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>


                                <!-- X-ray profile  -->
                                <div class="tab-pane" id="7a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control xraydate investdate" type="text" name="xraydate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>    

                                                    <input type="hidden" name="savedate1" class="savedate1">   
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchxray">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Radiological Reports</th>
                                                  
                                                </tr>
                                                <?php foreach($casexray_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->radiological_report;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                <!-- USG Female profile  -->
                                <div class="tab-pane" id="8a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control usgfemaledate investdate" type="text" name="usgfemaledate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>  

                                                    <input type="hidden" name="savedate1" class="savedate1">     
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchusgfemale">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Uterues Size</th>
                                                  <th>Endometrial thickness</th>
                                                  <th>Fibroids Number</th>
                                                  <th>Description</th>
                                                  <th>Ovary Size(RT/LT)</th>
                                                  <th>Ovary Volume(RT/LT)</th>
                                                  <th>Ovary Follicles(RT/LT)</th>
                                                  
                                                </tr>
                                                <?php foreach($casefemale_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->uterues_size;?></td>
                                                  <td><?php echo $val->thickness;?></td>
                                                  <td><?php echo $val->fibroids_no;?></td>
                                                  <td><?php echo $val->description;?></td>
                                                  <td><?php echo $val->ovary_size_rt."/".$val->ovary_size_lt;?></td>
                                                  <td><?php echo $val->ovary_volume_rt."/".$val->ovary_volume_lt;?></td>
                                                  <td><?php echo $val->follicles_rt."/".$val->follicles_lt;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                <!-- USG Male profile  -->
                                <div class="tab-pane" id="9a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control usgmaledate investdate" type="text" name="usgmaledate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>   

                                                    <input type="hidden" name="savedate1" class="savedate1">    
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchusgmale">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                       <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Prostate Size</th>
                                                  <th>Serum PSA</th>
                                                  <th>Prostate Volume</th>
                                                  <th>Other Findings</th>
                                                  
                                                </tr>
                                                <?php foreach($casemale_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->size;?></td>
                                                  <td><?php echo $val->serum_psa;?></td>
                                                  <td><?php echo $val->volume;?></td>
                                                  <td><?php echo $val->other_findings;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                <!-- Immunology profile  -->
                                <div class="tab-pane" id="10a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control immunlogydate investdate" type="text" name="immunlogydate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>  

                                                    <input type="hidden" name="savedate1" class="savedate1">     
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchimmunlogy">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                       <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>IgG</th>
                                                  <th>IgE</th>
                                                  <th>IgM</th>
                                                  <th>IgA</th>
                                                  <th>Itg</th>
                                                  
                                                </tr>
                                                <?php foreach($caseimmunology_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->igg;?></td>
                                                  <td><?php echo $val->ige;?></td>
                                                  <td><?php echo $val->igm;?></td>
                                                  <td><?php echo $val->iga;?></td>
                                                  <td><?php echo $val->itg;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                 <!-- Specific profile  -->
                                <div class="tab-pane" id="11a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control specificdate investdate" type="text" name="specificdate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>    

                                                    <input type="hidden" name="savedate1" class="savedate1">   
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchspecific">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Other Findings</th>
                                                  <th>Define Field 1</th>
                                                  <th>Define Field 2</th>
                                                  <th>Define Field 3</th>
                                                  <th>Define Field 4</th>
                                                  
                                                </tr>
                                                <?php foreach($casespecific_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->other_findings;?></td>
                                                  <td><?php echo $val->define_field1;?></td>
                                                  <td><?php echo $val->define_field2;?></td>
                                                  <td><?php echo $val->define_field3;?></td>
                                                  <td><?php echo $val->define_field4;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                <!-- Liver profile  -->
                                <div class="tab-pane" id="12a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control liverdate investdate" type="text" name="liverdate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>   

                                                    <input type="hidden" name="savedate1" class="savedate1">    
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchliver">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Total Bil</th>
                                                  <th>Albumin</th>
                                                  <th>Dir.Bilirubin</th>
                                                  <th>Globulin</th>
                                                  <th>Ind.Bilirubin</th>
                                                  <th>S.G.O.T</th>
                                                  <th>Gamma G.T</th>
                                                  <th>S.G.P.T</th>
                                                  <th>Total Proteins</th>
                                                  <th>Alk. Phos</th>
                                                  <th>Aust.Antigen</th>
                                                  <th>Amylase</th>
                                                  
                                                </tr>
                                                <?php foreach($caseliver_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->total_bil;?></td>
                                                  <td><?php echo $val->albumin;?></td>
                                                  <td><?php echo $val->dir_bilirubin;?></td>
                                                  <td><?php echo $val->globulin;?></td>
                                                  <td><?php echo $val->ind_bilirubin;?></td>
                                                  <td><?php echo $val->sgot;?></td>
                                                  <td><?php echo $val->gamma_gt;?></td>
                                                  <td><?php echo $val->sgpt;?></td>
                                                  <td><?php echo $val->total_protein;?></td>
                                                  <td><?php echo $val->alk_phos;?></td>
                                                  <td><?php echo $val->aust_antigen;?></td>
                                                  <td><?php echo $val->amylase;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                <!-- Lipid profile  -->
                                <div class="tab-pane" id="13a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control lipiddate investdate" type="text" name="lipiddate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>  

                                                    <input type="hidden" name="savedate1" class="savedate1">     
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchlipid">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                       <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Total Cholesterol</th>
                                                  <th>Chol/HDL ratio</th>
                                                  <th>Triglycerides</th>
                                                  <th>LDL/HDL</th>
                                                  <th>HDL Cholesterol</th>
                                                  <th>Lipoprotein</th>
                                                  <th>LDL Cholesterol</th>
                                                  <th>Apolipoprotein-A</th>
                                                  <th>VLDL</th>
                                                  <th>Apolipoprotein-B</th>
                                                  
                                                </tr>
                                                <?php foreach($caselipid_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->total_cholesterol;?></td>
                                                  <td><?php echo $val->hdl_ratio;?></td>
                                                  <td><?php echo $val->triglycerides;?></td>
                                                  <td><?php echo $val->ldl_hdl;?></td>
                                                  <td><?php echo $val->hdl_cholesterol;?></td>
                                                  <td><?php echo $val->lipoprotein;?></td>
                                                  <td><?php echo $val->ldl_cholesterol;?></td>
                                                  <td><?php echo $val->apolipoprotein_a;?></td>
                                                  <td><?php echo $val->vldl;?></td>
                                                  <td><?php echo $val->apolipoprotein_b;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                <!-- Cardiac profile  -->
                                <div class="tab-pane" id="15a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control cardiacdate investdate" type="text" name="cardiacdate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div> 

                                                    <input type="hidden" name="savedate1" class="savedate1">      
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchcardiac">Go</button>
                                                </div>
                                        </div>

                                    </fieldset>
                                       <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Homocysteine</th>
                                                  <th>ECG</th>
                                                  <th>2DECHO</th>
                                                  
                                                </tr>
                                                <?php foreach($casecardiac_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->homocysteine;?></td>
                                                  <td><?php echo $val->ecg;?></td>
                                                  <td><?php echo $val->decho;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                <!-- Diabetes profile  -->
                                <div class="tab-pane" id="14a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control diabetesdate investdate" type="text" name="diabetesdate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>  

                                                    <input type="hidden" name="savedate1" class="savedate1">     
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchdiabetes">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                       <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Blood (Fasting)</th>
                                                  <th>Blood (Post Prandial)</th>
                                                  <th>Blood (Random)</th>
                                                  <th>Urine (Fasting)</th>
                                                  <th>Urine (Post Prandial)</th>
                                                  <th>Urine (Random)</th>
                                                  <th>Glu.Tol.Test</th>
                                                  <th>Glycosylated HB</th>
                                                  
                                                </tr>
                                                <?php foreach($casediabetes_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->blood_fasting;?></td>
                                                  <td><?php echo $val->blood_prandial;?></td>
                                                  <td><?php echo $val->blood_random;?></td>
                                                  <td><?php echo $val->urine_fasting;?></td>
                                                  <td><?php echo $val->urine_prandial;?></td>
                                                  <td><?php echo $val->urine_random;?></td>
                                                  <td><?php echo $val->glu_test;?></td>
                                                  <td><?php echo $val->glycosylated_hb;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>
                                
                                <!-- serology profile  -->
                                <div class="tab-pane" id="16a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control serologydate investdate" type="text" name="serologydate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>    

                                                    <input type="hidden" name="savedate1" class="savedate1">   
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchserology">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Serological Reports</th>
                                                  
                                                </tr>
                                                <?php foreach($caseserological_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                  <td><?php echo $val->serological_report;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                <!-- semen profile  -->
                                <div class="tab-pane" id="17a">
                                    <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px; display: none;">
                                        <div class="col-md-12" style="padding: 0px;">
                                          
                                                <div class="col-md-2" style="padding-left: 0px;padding-left: 0px;text-align: center;padding-right: 0px;width: 12%;">
                                                        <label for="mobile" style="margin-top: 6px;">Date</label>
                                                </div>
                                                <div class="col-md-8" style="margin: 0;padding-left: 0px;">
                                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                                        <input class="form-control semendate investdate" type="text" name="semendate" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>   

                                                    <input type="hidden" name="savedate1" class="savedate1">    
                                                </div>
                                                <div class="col-md-2" style="padding-left: 0px;">
                                                     <button type="button" class="btn btn-success height-btn searchsemen">Go</button>
                                                </div>
                                        </div>
                                    </fieldset>
                                        <div class="form-group col-md-12" style="padding: 0px;margin-top: 10px;">
                                              <table class="table table-bordered tb-bg">
                                                <tr>
                                                  <th>Date</th>
                                                  <th>Semen Analysis</th>
                                                  
                                                </tr>
                                                <?php foreach($casesemen_show as $val){ ?>
                                              <tr>
                                                <td><?php echo $val->dateval;?></td>
                                                <td><?php echo $val->semen_analysis;?></td>
                                                 
                                              </tr>
                                              <?php } ?>
                                                
                                              </table>
                                          </div>
                                   
                                </div>

                                </div>
                </div>
            </div>
            <div class="modal-footer height-entry-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            
        </div>
    </div>
</div>
</div>

<!-- Show Investigation Modal -->
<div class="modal fade" id="OtherInvestigation" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Investiagtion Report</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                	<div class="col-md-12" style="margin-bottom: 5px;">
						<label for="mobile" ><?php echo $caseData->regid.' '.$caseData->first_name.' '.$caseData->middle_name.' '.$caseData->surname.' '.$caseData->gender.'/ '.$diff->format('%y Yrs %m Months %d Days'); ?></label>
					</div>	
                   <div id="exTab1" class="container">  
                        <ul  class="nav nav-pills">
                                    <li class="active"><a  href="#1" data-toggle="tab">CBC</a></li>
                                    <li><a href="#2" data-toggle="tab">Urine</a></li>
                                    <li><a href="#3" data-toggle="tab">Stool</a></li>
                                    <li><a href="#4" data-toggle="tab">Arthritis</a> </li>
                                    <li><a href="#5" data-toggle="tab">Endocrine</a> </li>
                                    <li><a href="#6" data-toggle="tab">Renal Profile</a> </li>
                                    <li><a href="#7" data-toggle="tab">X-ray - CT - MRI</a> </li>
                                    <li><a href="#8" data-toggle="tab">USG Female</a> </li>
                                    <li><a href="#9" data-toggle="tab">USG Male</a> </li>
                                    <li><a href="#10" data-toggle="tab">Immunology</a> </li>
                                    <li><a href="#11" data-toggle="tab">Specific</a> </li>
                                    <li><a href="#12" data-toggle="tab">Liver Profile</a> </li>
                                    <li><a href="#13" data-toggle="tab">Lipid Profile</a> </li>
                                    <li><a href="#14" data-toggle="tab">Diabetes Profile</a> </li>
                                    <li><a href="#15" data-toggle="tab">Cardiac Profile</a> </li>
                                    <li><a href="#16" data-toggle="tab">Serology</a> </li>
                                    <li><a href="#17" data-toggle="tab">Semen Analysis</a> </li>
                                    <li><a href="#18" data-toggle="tab">Comparison</a> </li>

                                </ul>

                                    <div class="tab-content clearfix">

                                    	<!-- CBC -->
                                        <div class="tab-pane active" id="1">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@cbcdetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="cbc_date" name="cbc_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="cbc_rand" name="cbc_rand" value="<?php if($casecbc!=""){  echo $casecbc->rand_id; } else { echo rand('99',9999);} ?>">
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 <div class="col-md-12"  style="padding-left:0;padding-right:0; border-bottom: 2px solid #ccc;">
												     <div class="col-md-6" style="border-right: 2px solid #ccc; padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px; ">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">HB:</label>
			                                                    <input class="form-control hb_1" name="hb" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->hb; } else { echo " ";} ?>">

                                                                <input type="hidden" class="cbcid"  name="id" value="<?php if($casecbc!=""){  echo $casecbc->id; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">13.0 to 18.0 gm%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">R.B.C:</label>
			                                                    <input class="form-control rbc_c" name="rbc" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->rbc; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">4-5 to 5-6 mill/c.mm</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">W.B.C:</label>
			                                                    <input class="form-control wbc" name="wbc" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->wbc; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">4000 to 11000 mill/cu.mm</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Platelets:</label>
			                                                    <input class="form-control platelets" name="platelets" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->platelets; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">1-5 to 4-5 Lacs/cu.mm</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Vitamin B-12:</label>
			                                                    <input class="form-control vitaminb" name="vitaminb" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->vitaminb; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">2 to 9</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">25-Oh Vitamin D(Total):</label>
			                                                    <input class="form-control vitamind" name="vitamind" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->vitamind; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">30 to 100</label>
			                                                </div>
			                                                
			                                            </div>
												     </div>
												     <div class="col-md-6" style="padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
													     	<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Neutrophils:</label>
			                                                    <input class="form-control neutrophils" name="neutrophils" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->neutrophils; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">40 to 75 %</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Lymphocytes:</label>
			                                                    <input class="form-control lymphocytes" name="lymphocytes" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->lymphocytes; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">20 to 45 %</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		 <div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Eosinophils:</label>
			                                                    <input class="form-control eosinophils" name="eosinophils" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->eosinophils; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">1 to 6 %</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Monocytes:</label>
			                                                    <input class="form-control monocytes" name="monocytes" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->monocytes; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">2 to 10 %</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Basophils:</label>
			                                                    <input class="form-control basophils" name="basophils" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->basophils; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">0 to 1 %</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Band Cells:</label>
			                                                    <input class="form-control band_cells" name="band_cells" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->band_cells; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">0 to 6 %</label>
			                                                </div>
		                                            	</div>

												     </div>
												 </div>
                                             

                                            <div class="col-md-12" style="padding: 0px; margin-top: 5px;">
                                                <div class="form-group col-md-6">
                                                    <label for="mobile">Abnor R.B.C:</label>
                                                    <input class="form-control abnor_rbc" name="abnor_rbc" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->abnor_rbc; } else { echo " ";} ?>">
                                                    
                                                </div>
                                               
                                                 <div class="form-group col-md-6" style="margin: 0;">
                                                    <label for="mobile">Parasites:</label>
                                                    <input class="form-control parasites" name="parasites" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->parasites; } else { echo " ";} ?>">
                                                    
                                                </div>
                                                
                                            </div>

                                            <div class="col-md-12" style="padding: 0px;">
                                                <div class="form-group col-md-6">
                                                    <label for="mobile">Abnor W.B.C:</label>
                                                    <input class="form-control abnor_wbc" name="abnor_wbc" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->abnor_wbc; } else { echo " ";} ?>">
                                                    
                                                </div>
                                               
                                                 <div class="form-group col-md-3" style="margin: 0;">
                                                    <label for="mobile">E.S.R:</label>
                                                    <input class="form-control esr" name="esr" type="text" aria-required="true" value="<?php if($casecbc!=""){  echo $casecbc->esr; } else { echo " ";} ?>">
                                                    
                                                </div>
                                                <div class="form-group col-md-3" style="padding-left: 0px;">
                                                    <label for="mobile" style="margin-top: 3rem;">0.5mm (Western Green Method)</label>
                                                </div>
                                                
                                            </div>

                                                
                                            </fieldset>

                                            <?php
                                                if($Cbcreport!=""){
                                                    foreach($Cbcreport as $Cbcreportval){
                                                        $pdate = $Cbcreportval->dateval;
                                                        $deleted = $Cbcreportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="cbc_date1" type="hidden"  value="<?php echo $Cbcreportval->dateval; ?>" />
                                                            <input class="hb1" type="hidden"  value="<?php echo $Cbcreportval->hb; ?>" />
                                                            <input class="wbc1" type="hidden"  value="<?php echo $Cbcreportval->wbc; ?>" />
                                                            <input class="platelets1" type="hidden"  value="<?php echo $Cbcreportval->platelets; ?>" />
                                                            <input class="vitaminb1" type="hidden"  value="<?php echo $Cbcreportval->vitaminb; ?>" />
                                                            <input class="vitamind1" type="hidden"  value="<?php echo $Cbcreportval->vitamind; ?>" />
                                                            <input class="neutrophils1" type="hidden"  value="<?php echo $Cbcreportval->neutrophils; ?>" />
                                                            <input class="lymphocytes1" type="hidden"  value="<?php echo $Cbcreportval->lymphocytes; ?>" />
                                                            <input class="eosinophils1" type="hidden"  value="<?php echo $Cbcreportval->eosinophils; ?>" />
                                                            <input class="monocytes1" type="hidden"  value="<?php echo $Cbcreportval->monocytes; ?>" />
                                                            <input class="basophils1" type="hidden"  value="<?php echo $Cbcreportval->basophils; ?>" />
                                                            <input class="abnor_rbc1" type="hidden"  value="<?php echo $Cbcreportval->abnor_rbc; ?>" />
                                                            <input class="abnor_wbc1" type="hidden"  value="<?php echo $Cbcreportval->abnor_wbc; ?>" />
                                                            <input class="parasites1" type="hidden"  value="<?php echo $Cbcreportval->parasites; ?>" />
                                                            <input class="rbc1" type="hidden"  value="<?php echo $Cbcreportval->rbc; ?>" />
                                                            <input class="esr1" type="hidden"  value="<?php echo $Cbcreportval->esr; ?>" />
                                                           
                                                        
                                                    <?php   } } }  ?>

                                                    <input class="cbc_date1" type="hidden"/>
                                                    <input class="hb1" type="hidden" />
                                                    <input class="wbc1" type="hidden"/>
                                                    <input class="platelets1" type="hidden"/>
                                                    <input class="vitaminb1" type="hidden"/>
                                                    <input class="vitamind1" type="hidden"/>
                                                    <input class="neutrophils1" type="hidden"/>
                                                    <input class="lymphocytes1" type="hidden"/>
                                                    <input class="eosinophils1" type="hidden" />
                                                    <input class="monocytes1" type="hidden"/>
                                                    <input class="basophils1" type="hidden"/>
                                                    <input class="abnor_rbc1" type="hidden"/>
                                                    <input class="abnor_wbc1" type="hidden" />
                                                    <input class="parasites1" type="hidden"/>
                                                    <input class="rbc1" type="hidden" />
                                                    <input class="esr1" type="hidden">
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savecbc">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowcbc">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                             {!! Form::close() !!}
                                        </div>

                                        <!-- Urine -->
                                        <div class="tab-pane" id="2">

                                            {!! Form::open(['action' => 'LA\MedicalcasesController@urinedetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="urine_date" name="urine_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="urine_rand" name="urine_date" value="<?php if($caseurine!=""){  echo $caseurine->rand_id; } else { echo rand('99',9999);} ?>">
                                    		<fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
	                                            <div class="col-md-12"  style="padding-left:0;padding-right:0">
												     <div class="col-md-6" style="margin-top: 0px;">
											          	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Quantity:</label>
							                                <input class="form-control quantity" name="quantity" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->quantity; } else { echo " ";} ?>">

                                                             <input type="hidden" class="urineid"  name="id" value="<?php if($caseurine!=""){  echo $caseurine->id; } else { echo " ";} ?>">
							                           
							                        	</div>

							                         	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Color:</label>
							                                <input class="form-control color_u" name="color" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->color; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Appearance:</label>
							                                <input class="form-control appearance" name="appearance" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->appearance; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Reaction:</label>
							                                <input class="form-control reaction" name="reaction" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->reaction; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Specific GRA:</label>
							                                <input class="form-control gra" name="gra" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->gra; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
						                            		&nbsp;
						                            	</div>
														
												     </div>
												     <div class="col-md-6" style="border-left: 2px solid #ccc;margin-top: 0px;">
												          <div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
							                                <label for="mobile">Protein:</label>
							                                <input class="form-control protein" name="protein" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->protein; } else { echo " ";} ?>">
								                        </div>

								                         <div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
							                                <label for="mobile">Sugar:</label>
							                                <input class="form-control sugar" name="sugar" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->sugar; } else { echo " ";} ?>">  
								                        </div>  

								                        <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Acetone Bodies:</label>
							                                <input class="form-control acetone" name="acetone" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->acetone; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Bile Pigments:</label>
							                                <input class="form-control pigments" name="pigments" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->pigments; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Occult blood:</label>
							                                <input class="form-control occult_blood" name="occult_blood" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->occult_blood; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Urobilinogen:</label>
							                                <input class="form-control urobilinogen" name="urobilinogen" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->urobilinogen; } else { echo " ";} ?>">
						                            	</div>
												     </div>
							                    </div>
							                </fieldset>
							                <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
							                	 <div class="col-md-3">
							                	 	<label for="mobile" style="margin-top: 9px;">Microscopic Findings:</label>
							                	 </div>
							                	 <div class="form-horizontal">
										              <div class="control-group form-inline">
										                <label for="name" class="col-md-1" style="padding:0;margin-top: 5px;"> Epith Cells</label>
										                <div class="controls col-md-3">
										                  <input class="form-control epith_cells" name="epith_cells" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->epith_cells; } else { echo " ";} ?>">
										                </div>
										              </div>
									            </div>
									             <div class="form-horizontal">
										              <div class="control-group form-inline">
										                <label for="name" class="col-md-2" style="margin-top: 5px;"> Red Blood Cells</label>
										                <div class="controls col-md-3" style="padding: 0px;">
										                  <input class="form-control rbc" name="rbc" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->rbc; } else { echo " ";} ?>">
										                </div>
										              </div>
									            </div>
									            <div class="col-md-3">
							                	 	<label for="mobile">&nbsp;</label>
							                	 </div>
							                	 <div class="form-horizontal">
										              <div class="control-group form-inline">
										                <label for="name" class="col-md-1" style="padding:0;margin-top: 5px;"> Pus Cells</label>
										                <div class="controls col-md-3">
										                  <input class="form-control pus_cells" name="pus_cells" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->pus_cells; } else { echo " ";} ?>">
										                </div>
										              </div>
									            </div>
									             <div class="form-horizontal">
										              <div class="control-group form-inline">
										                <label for="name" class="col-md-2" style=" margin-top: 5px;"> Other Findings</label>
										                <div class="controls col-md-3" style="padding: 0px;">
										                  <input class="form-control other_findings" name="other_findings" type="text" aria-required="true" value="<?php if($caseurine!=""){  echo $caseurine->other_findings; } else { echo " ";} ?>">
										                </div>
										              </div>
									            </div>
							                </fieldset>
                                            <?php
                                                if($Urinereport!=""){
                                                    foreach($Urinereport as $Urinereportval){
                                                        $pdate = $Urinereportval->dateval;
                                                        $deleted = $Urinereportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="urine_date1" type="hidden"  value="<?php echo $Urinereportval->dateval; ?>" />
                                                            <input class="color1" type="hidden"  value="<?php echo $Urinereportval->color; ?>" />
                                                            <input class="appearance1" type="hidden"  value="<?php echo $Urinereportval->appearance; ?>" />
                                                            <input class="quantity1" type="hidden"  value="<?php echo $Urinereportval->quantity; ?>" />
                                                            <input class="reaction1" type="hidden"  value="<?php echo $Urinereportval->reaction; ?>" />
                                                            <input class="gra1" type="hidden"  value="<?php echo $Urinereportval->gra; ?>" />
                                                            <input class="protein1" type="hidden"  value="<?php echo $Urinereportval->protein; ?>" />
                                                            <input class="sugar1" type="hidden"  value="<?php echo $Urinereportval->sugar; ?>" />
                                                            <input class="occult_blood1" type="hidden"  value="<?php echo $Urinereportval->occult_blood; ?>" />
                                                            <input class="acetone1" type="hidden"  value="<?php echo $Urinereportval->acetone; ?>" />
                                                            <input class="pigments1" type="hidden"  value="<?php echo $Urinereportval->pigments; ?>" />
                                                            <input class="urobilinogen1" type="hidden"  value="<?php echo $Urinereportval->urobilinogen; ?>" />
                                                            <input class="epith_cells1" type="hidden"  value="<?php echo $Urinereportval->epith_cells; ?>" />
                                                            <input class="pus_cells1" type="hidden"  value="<?php echo $Urinereportval->pus_cells; ?>" />
                                                            <input class="rbc1" type="hidden"  value="<?php echo $Urinereportval->rbc; ?>" />
                                                            <input class="other_findings1" type="hidden"  value="<?php echo $Urinereportval->other_findings; ?>" />
                                                           
                                                        
                                                    <?php   } } }  ?>

                                                    <input class="urine_date1" type="hidden"/>
                                                    <input class="color1" type="hidden" />
                                                    <input class="appearance1" type="hidden"/>
                                                    <input class="quantity1" type="hidden"/>
                                                    <input class="reaction1" type="hidden"/>
                                                    <input class="gra1" type="hidden"/>
                                                    <input class="protein1" type="hidden"/>
                                                    <input class="sugar1" type="hidden" />
                                                    <input class="occult_blood1" type="hidden">
                                                    <input class="acetone1" type="hidden"/>
                                                    <input class="pigments1" type="hidden" />
                                                    <input class="urobilinogen1" type="hidden">
                                                    <input class="epith_cells1" type="hidden" />
                                                    <input class="pus_cells1" type="hidden"/>
                                                    <input class="rbc1" type="hidden" />
                                                    <input class="other_findings1" type="hidden" />
							                <div class="col-md-12" style="text-align: center;">
	                                                <button type="button" class="btn btn-success height-btn saveurine">Save</button>
	                                                <button type="button" class="btn btn-success height-btn savefollowurine">Save Copy To Followup</button>
	                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                                        </div>
                                            {!! Form::close() !!}
						                </div>

						                <!-- Stool -->
                                        <div class="tab-pane" id="3">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@stooldetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="stool_date" name="stool_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="stool_rand" name="stool_rand" value="<?php if($casestool!=""){  echo $casestool->rand_id; } else { echo rand('99',9999);} ?>">
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
	                                            <div class="col-md-12"  style="padding-left:0;padding-right:0">
												     <div class="col-md-6" style="margin-top: 0px;">
											          	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Color:</label>
							                                <input class="form-control color" name="color" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->color; } else { echo " ";} ?>">

					                                        <input type="hidden" class="stoolid"  name="id" value="<?php if($casestool!=""){  echo $casestool->id; } else { echo " ";} ?>">
							                        	</div>

							                         	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Consistency:</label>
							                                <input class="form-control consistency" name="consistency" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->consistency; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Mucus:</label>
							                                <input class="form-control mucus" name="mucus" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->mucus; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Frank Blood:</label>
							                                <input class="form-control frank_blood" name="frank_blood" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->frank_blood; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Adult Worm:</label>
							                                <input class="form-control adult_worm" name="adult_worm" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->adult_worm; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
						                            		<label for="mobile">Reaction:</label>
							                                <input class="form-control reaction" name="reaction" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->reaction; } else { echo " ";} ?>">
						                            	</div>
						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
						                            		<label for="mobile">Pus Cells:</label>
							                                <input class="form-control pus_cells" name="pus_cells" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->pus_cells; } else { echo " ";} ?>">
						                            	</div>
														
												     </div>
												     <div class="col-md-6" style="border-left: 2px solid #ccc;margin-top: 0px;">
												          <div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
							                                <label for="mobile">Occult blood:</label>
							                                <input class="form-control occult_blood" name="occult_blood" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->occult_blood; } else { echo " ";} ?>">
								                        </div>

								                         <div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
							                                <label for="mobile">Macrophages:</label>
							                                <input class="form-control macrophages" name="macrophages" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->macrophages; } else { echo " ";} ?>">  
								                        </div>  

								                        <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">R.B.C:</label>
							                                <input class="form-control rbc" name="rbc" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->rbc; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Ova:</label>
							                                <input class="form-control ova" name="ova" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->ova; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Cysts:</label>
							                                <input class="form-control cysts" name="cysts" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->cysts; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Protozoa:</label>
							                                <input class="form-control protozoa" name="protozoa" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->protozoa; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Yeast Cells:</label>
							                                <input class="form-control yeast_cell" name="yeast_cell" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->yeast_cell; } else { echo " ";} ?>">
						                            	</div>

						                            	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
							                                <label for="mobile">Other Findings:</label>
							                                <input class="form-control other_findings" name="other_findings" type="text" aria-required="true" value="<?php if($casestool!=""){  echo $casestool->other_findings; } else { echo " ";} ?>">
						                            	</div>
												     </div>
							                    </div>
							                </fieldset>

                                            <?php
                                                if($Stoolreport!=""){
                                                    foreach($Stoolreport as $Stoolreportval){
                                                        $pdate = $Stoolreportval->dateval;
                                                        $deleted = $Stoolreportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="stool_date1" type="hidden"  value="<?php echo $Stoolreportval->dateval; ?>" />
                                                            <input class="color1" type="hidden"  value="<?php echo $Stoolreportval->color; ?>" />
                                                            <input class="consistency1" type="hidden"  value="<?php echo $Stoolreportval->consistency; ?>" />
                                                            <input class="mucus1" type="hidden"  value="<?php echo $Stoolreportval->mucus; ?>" />
                                                            <input class="frank_blood1" type="hidden"  value="<?php echo $Stoolreportval->frank_blood; ?>" />
                                                            <input class="adult_worm1" type="hidden"  value="<?php echo $Stoolreportval->adult_worm; ?>" />
                                                            <input class="reaction1" type="hidden"  value="<?php echo $Stoolreportval->reaction; ?>" />
                                                            <input class="pus_cells1" type="hidden"  value="<?php echo $Stoolreportval->pus_cells; ?>" />
                                                            <input class="occult_blood1" type="hidden"  value="<?php echo $Stoolreportval->occult_blood; ?>" />
                                                            <input class="macrophages1" type="hidden"  value="<?php echo $Stoolreportval->macrophages; ?>" />
                                                            <input class="rbc1" type="hidden"  value="<?php echo $Stoolreportval->rbc; ?>" />
                                                            <input class="ova1" type="hidden"  value="<?php echo $Stoolreportval->ova; ?>" />
                                                            <input class="cysts1" type="hidden"  value="<?php echo $Stoolreportval->cysts; ?>" />
                                                            <input class="protozoa1" type="hidden"  value="<?php echo $Stoolreportval->protozoa; ?>" />
                                                            <input class="yeast_cell1" type="hidden"  value="<?php echo $Stoolreportval->yeast_cell; ?>" />
                                                            <input class="other_findings1" type="hidden"  value="<?php echo $Stoolreportval->other_findings; ?>" />
                                                           
                                                        
                                                    <?php   } } }  ?>

                                                    <input class="stool_date1" type="hidden" />
                                                    <input class="color1" type="hidden" />
                                                    <input class="consistency1" type="hidden"  />
                                                    <input class="mucus1" type="hidden"  />
                                                    <input class="frank_blood1" type="hidden"/>
                                                    <input class="adult_worm1" type="hidden" />
                                                    <input class="reaction1" type="hidden"/>
                                                    <input class="pus_cells1" type="hidden" />
                                                    <input class="occult_blood1" type="hidden" />
                                                    <input class="macrophages1" type="hidden" />
                                                    <input class="rbc1" type="hidden"/>
                                                    <input class="ova1" type="hidden" />
                                                    <input class="cysts1" type="hidden" />
                                                    <input class="protozoa1" type="hidden" />
                                                    <input class="yeast_cell1" type="hidden" />
                                                    <input class="other_findings1" type="hidden">
							               
							                <div class="col-md-12" style="text-align: center;">
	                                                <button type="button" class="btn btn-success height-btn savestool">Save</button>
	                                                <button type="button" class="btn btn-success height-btn savefollowstool">Save Copy To Followup</button>
	                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                                        </div>
                                            {!! Form::close() !!}
                                        </div>

                                        <!-- Arthritis -->
                                        <div class="tab-pane" id="4">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@arithrisdetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="ar_date" name="ar_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="ar_rand" name="ar_rand" value="<?php if($casearithris!=""){  echo $casearithris->rand_id; } else { echo rand('99',9999);} ?>">
                                             <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 <div class="col-md-12"  style="padding-left:0;padding-right:0;">
												     <div class="col-md-6" style="border-right: 2px solid #ccc; padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px; ">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Anti-streptolysin O:</label>
			                                                    <input class="form-control anti_o" name="anti_o" type="text" aria-required="true" value="<?php if($casearithris!=""){  echo $casearithris->anti_o; } else { echo " ";} ?>" >
			                                                    
                                                                <input type="hidden" class="arid"  name="id" value="<?php if($casearithris!=""){  echo $casearithris->id; } else { echo " ";} ?>">
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;"><200</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">ACCP:</label>
			                                                    <input class="form-control accp" name="accp" type="text" aria-required="true" value="<?php if($casearithris!=""){  echo $casearithris->accp; } else { echo " ";} ?>" >
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">Positive:>=1.11</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">RA Factor:</label>
			                                                    <input class="form-control ra_factor" name="ra_factor" type="text" aria-required="true" value="<?php if($casearithris!=""){  echo $casearithris->ra_factor; } else { echo " ";} ?>" >
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;"><15.9</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Alkaline Phosphatase :</label>
			                                                    <input class="form-control alkaline" name="alkaline" type="text" aria-required="true" value="<?php if($casearithris!=""){  echo $casearithris->alkaline; } else { echo " ";} ?>" >
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">M:53-128-F:42 to 98</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Complement4 (C4):</label>
			                                                    <input class="form-control complement" name="complement" type="text" aria-required="true" value="<?php if($casearithris!=""){  echo $casearithris->c4; } else { echo " ";} ?>" >
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">0.10-0.40</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">ANA:</label>
			                                                    <input class="form-control ana" name="ana" type="text" aria-required="true" value="<?php if($casearithris!=""){  echo $casearithris->ana; } else { echo " ";} ?>" >
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">1ml</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">C-Reactive Protein:</label>
			                                                    <input class="form-control c_react" name="c_react" type="text" aria-required="true" value="<?php if($casearithris!=""){  echo $casearithris->c_react; } else { echo " ";} ?>" >
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">Adults:<0.3</label>
			                                                </div>
			                                                
			                                            </div>

			                                            
												     </div>
                                                     <?php
                                                if($Arithrisreport!=""){
                                                    foreach($Arithrisreport as $Arithrisreportval){
                                                        $pdate = $Arithrisreportval->dateval;
                                                        $deleted = $Arithrisreportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="ar_date1" type="hidden"  value="<?php echo $Arithrisreportval->dateval; ?>" />
                                                            <input class="anti_o1" type="hidden"  value="<?php echo $Arithrisreportval->anti_o; ?>" />
                                                            <input class="accp1" type="hidden"  value="<?php echo $Arithrisreportval->accp; ?>" />
                                                            <input class="ra_factor1" type="hidden"  value="<?php echo $Arithrisreportval->ra_factor; ?>" />
                                                            <input class="alkaline1" type="hidden"  value="<?php echo $Arithrisreportval->alkaline; ?>" />
                                                            <input class="ana1" type="hidden"  value="<?php echo $Arithrisreportval->ana; ?>" />
                                                            <input class="c_react1" type="hidden"  value="<?php echo $Arithrisreportval->c_react; ?>" />
                                                            <input class="complement1" type="hidden"  value="<?php echo $Arithrisreportval->c4; ?>" />
                                                           
                                                        
                                                    <?php   } } }  ?>

                                                    <input class="ar_date1" type="hidden" />
                                                    <input class="anti_o1" type="hidden"/>
                                                    <input class="accp1" type="hidden"  />
                                                    <input class="ra_factor1" type="hidden"/>
                                                    <input class="alkaline1" type="hidden"/>
                                                    <input class="ana1" type="hidden" />
                                                    <input class="c_react1" type="hidden"/>
                                                    <input class="complement1" type="hidden"/>
												     <div class="col-md-6" style="padding: 0px; margin-top: 0px;">
												     	<fieldset class="fieldset" style="margin-left: 5px;margin-right: 5px;">
													     	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
														     	<div class="col-md-4" style="margin: 0;">
				                                                    <label for="mobile"><u>Vitamin B-12</u></label>
				                                                    
				                                                </div>
				                                                <div class="col-md-8" style="padding-left: 0px; text-align: right;">
				                                                    <label for="mobile"><u>25-Oh Vitamin D (Total)</u></label>
				                                                </div>
			                                            	</div>
		                                            	</fieldset>
		                                            	<fieldset class="fieldset" style="margin-left: 5px;margin-right: 5px; margin-top: 10px;">
			                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
			                                            		<div class="col-md-8" style="margin: 0;">
				                                                    <label for="mobile"><u>TSH</u></label>
				                                                </div>
				                                                
			                                            	</div>
		                                            	</fieldset>
		                                            	<fieldset class="fieldset" style="margin-left: 5px;margin-right: 5px; margin-top: 10px;">
			                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 18px;">
			                                            		 <div class="col-md-5" style="margin: 0;">
				                                                    <label for="mobile"><u>Total Cholesterol</u></label>
				                                                    
				                                                </div>
				                                                <div class="col-md-7" style="padding-left: 0px; text-align: right;">
				                                                    <label for="mobile"><u>HDL Cholesterol- Direct</u></label>
				                                                </div>
			                                            	</div>

			                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 18px;">
			                                            		<div class="col-md-8" style="margin: 0;">
				                                                    <label for="mobile"><u>LDL Cholesterol Direct</u></label>
				                                                    
				                                                </div>
				                                                <div class="col-md-4" style="padding-left: 0px;">
				                                                    <label for="mobile"><u>Triglycerides</u></label>
				                                                </div>
			                                            	</div>
		                                            	</fieldset>
		                                            	<fieldset class="fieldset" style="margin-left: 5px;margin-right: 5px; margin-top: 10px;">
			                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 16px;">
			                                            		<div class="col-md-8" style="margin: 0;">
				                                                    <label for="mobile"><u>BUN</u></label>
				                                                    
				                                                </div>
				                                                <div class="col-md-4" style="padding-left: 0px;">
				                                                    <label for="mobile"><u>Uric Acid</u></label>
				                                                </div>
			                                            	</div>

			                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 16px;">
			                                            		<div class="col-md-8" style="margin: 0;">
				                                                    <label for="mobile"><u>Calcium</u></label>
				                                                </div>
				                                                <div class="col-md-4" style="padding-left: 0px;">
				                                                    <label for="mobile" ><u>Phosphorus</u></label>
				                                                </div>
			                                            	</div>

			                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 16px;">
			                                            		<div class="col-md-8" style="margin: 0;">
				                                                    <label for="mobile"><u>Creatinine</u></label>
				                                                </div>
				                                               
			                                            	</div>
		                                            	</fieldset>
		                                            	

												     </div>
												 </div>  
                                            </fieldset>
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savearthris">Save</button>
                                                <button type="button" class="btn btn-success height-btn  savefollowarthris">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                        <!-- Endocrine -->
                                        <div class="tab-pane" id="5">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@endocrinedetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="ed_date" name="ed_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="ed_rand" name="ed_rand" value="<?php if($caseendocrine!=""){  echo $caseendocrine->rand_id; } else { echo rand('99',9999);} ?>">
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 <div class="col-md-12"  style="padding-left:0;padding-right:0;">
												     <div class="col-md-6" style="border-right: 2px solid #ccc; padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px; ">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">T3:</label>
			                                                    <input class="form-control t_3" name="t3" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->t3; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">80-180 ng/dl</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">T4:</label>
			                                                    <input class="form-control t_4" name="t4" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->t4; } else { echo " ";} ?>">

                                                                <input type="hidden" class="erid"  name="id" value="<?php if($caseendocrine!=""){  echo $caseendocrine->id; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">4.6-12 ug/dl</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">TSH:</label>
			                                                    <input class="form-control tsh" name="tsh" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->tsh; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">0.30-5.5 lu/ml</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">FT3:</label>
			                                                    <input class="form-control ft_3" name="ft3" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->ft3; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">3.5-7.8 pmol/l</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">FT4:</label>
			                                                    <input class="form-control ft_4" name="ft4" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->ft4; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">9.0-25.0 pmol/l</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Anti TPO:</label>
			                                                    <input class="form-control anti_tpo" name="anti_tpo" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->anti_tpo; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;"><9.0 IU/ml</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Antibody Thyroglobulin:</label>
			                                                    <input class="form-control antibody" name="antibody" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->antibody; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">&nbsp;</label>
			                                                </div>
			                                                
			                                            </div>

			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Prolactin:</label>
			                                                    <input class="form-control prolactin" name="prolactin" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->prolactin; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;"><24 ng/ml</label>
			                                                </div>
			                                                
			                                            </div>

			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">FSH Day3:</label>
			                                                    <input class="form-control fsh" name="fsh" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->fsh; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">3-20 mIU/ml</label>
			                                                </div>
			                                                
			                                            </div>
												     </div>
												     <div class="col-md-6" style="padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
													     	<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">LSH Surge:</label>
			                                                    <input class="form-control lsh" name="lsh" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->lsh; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">>20 mIU/ml</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Progesterone Day3:</label>
			                                                    <input class="form-control progesterone_3" name="progesterone_3" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->progesterone_3; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;"><1.5 ng/dl</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		 <div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Progesterone:</label>
			                                                    <input class="form-control progesterone" name="progesterone" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->progesterone; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">>15 ng/dl</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">DHEA-S:</label>
			                                                    <input class="form-control dhea" name="dhea" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->dhea; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">0.7-1.9 ng/dl,35-430 ug/dl</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Testosterone:</label>
			                                                    <input class="form-control testosterone" name="testosterone" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->testosterone; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;"><70 ng/dl</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">AMA:</label>
			                                                    <input class="form-control ama" name="ama" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->ama; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">>50</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Fasting Insulin:</label>
			                                                    <input class="form-control insulin" name="insulin" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->insulin; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;"><30 mIU/ml</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Fasting Glucose:</label>
			                                                    <input class="form-control glucose" name="glucose" type="text" aria-required="true" value="<?php if($caseendocrine!=""){  echo $caseendocrine->glucose; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">&nbsp;</label>
			                                                </div>
		                                            	</div>

												     </div>
												 </div>  
                                            </fieldset>
                                            <?php
                                                if($Endocrinereport!=""){
                                                    foreach($Endocrinereport as $Endocrinereportval){
                                                        $pdate = $Endocrinereportval->dateval;
                                                        $deleted = $Endocrinereportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="ed_date1" type="hidden"  value="<?php echo $Endocrinereportval->dateval; ?>" />
                                                            <input class="t_31" type="hidden"  value="<?php echo $Endocrinereportval->t3; ?>" />
                                                            <input class="t_41" type="hidden"  value="<?php echo $Endocrinereportval->t4; ?>" />
                                                            <input class="tsh1" type="hidden"  value="<?php echo $Endocrinereportval->tsh; ?>" />
                                                            <input class="ft_31" type="hidden"  value="<?php echo $Endocrinereportval->ft3; ?>" />
                                                            <input class="ft_41" type="hidden"  value="<?php echo $Endocrinereportval->ft41; ?>" />
                                                            <input class="anti_tpo1" type="hidden"  value="<?php echo $Endocrinereportval->anti_tpo; ?>" />
                                                            <input class="antibody1" type="hidden"  value="<?php echo $Endocrinereportval->antibody; ?>" />
                                                            <input class="prolactin1" type="hidden"  value="<?php echo $Endocrinereportval->prolactin; ?>" />
                                                            <input class="fsh1" type="hidden"  value="<?php echo $Endocrinereportval->fsh; ?>" />
                                                            <input class="lsh" type="hidden"  value="<?php echo $Endocrinereportval->lsh; ?>" />
                                                            <input class="progesterone_31" type="hidden"  value="<?php echo $Endocrinereportval->progesterone_3; ?>" />
                                                            <input class="progesterone1" type="hidden"  value="<?php echo $Endocrinereportval->progesterone; ?>" />
                                                            <input class="dhea1" type="hidden"  value="<?php echo $Endocrinereportval->dhea; ?>" />
                                                            <input class="testosterone1" type="hidden"  value="<?php echo $Endocrinereportval->testosterone; ?>" />
                                                            <input class="ama1" type="hidden"  value="<?php echo $Endocrinereportval->ama; ?>" />
                                                            <input class="insulin1" type="hidden"  value="<?php echo $Endocrinereportval->insulin; ?>" />
                                                            <input class="glucose1" type="hidden"  value="<?php echo $Endocrinereportval->glucose; ?>" />
                                                             
                                                    <?php   } } }  ?>
                                                        <input class="ed_date1" type="hidden"/>
                                                        <input class="t_31" type="hidden" />
                                                        <input class="t_41" type="hidden"  />
                                                        <input class="tsh1" type="hidden" />
                                                        <input class="ft_31" type="hidden"/>
                                                        <input class="ft_41" type="hidden" />
                                                        <input class="anti_tpo1" type="hidden" >
                                                        <input class="antibody1" type="hidden">
                                                        <input class="prolactin1" type="hidden">
                                                        <input class="fsh1" type="hidden"/>
                                                        <input class="lsh" type="hidden" />
                                                        <input class="progesterone_31" type="hidden">
                                                        <input class="progesterone1" type="hidden" >
                                                        <input class="dhea1" type="hidden" />
                                                        <input class="testosterone1" type="hidden" />
                                                        <input class="ama1" type="hidden"  />
                                                        <input class="insulin1" type="hidden"/>
                                                        <input class="glucose1" type="hidden"/>
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn saveed">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowed">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>


                                        <!-- Renal Profile -->
                                        <div class="tab-pane" id="6">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@renaldetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="renal_date" name="renal_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="renal_rand" name="renal_rand" value="<?php if($caserenal!=""){  echo $caserenal->rand_id; } else { echo rand('99',9999);} ?>">
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 <div class="col-md-12"  style="padding-left:0;padding-right:0;">
												     <div class="col-md-6" style="border-right: 2px solid #ccc; padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px; ">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">B.U.N:</label>
			                                                    <input class="form-control bun" name="bun" type="text" aria-required="true" value="<?php if($caserenal!=""){  echo $caserenal->bun; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">10-20 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Urea:</label>
			                                                    <input class="form-control urea" name="urea" type="text" aria-required="true" value="<?php if($caserenal!=""){  echo $caserenal->urea; } else { echo " ";} ?>">

                                                                <input type="hidden" class="renalid"  name="id" value="<?php if($caserenal!=""){  echo $caserenal->id; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">15-45 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Creatinine:</label>
			                                                    <input class="form-control creatinine" name="creatinine" type="text" aria-required="true" value="<?php if($caserenal!=""){  echo $caserenal->creatinine; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">M:0.6-1.1 F:0.5-0.8</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Uric Acid:</label>
			                                                    <input class="form-control uric_acid" name="uric_acid" type="text" aria-required="true" value="<?php if($caserenal!=""){  echo $caserenal->uric_acid; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">M:3.5-7.2 F:2.6-6.0</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Calcium:</label>
			                                                    <input class="form-control calcium" name="calcium" type="text" aria-required="true" value="<?php if($caserenal!=""){  echo $caserenal->calcium; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">8.8 to 10.6 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            
												     </div>
												     <div class="col-md-6" style="padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
													     	<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Phosphorus:</label>
			                                                    <input class="form-control phosphorus" name="phosphorus" type="text" aria-required="true" value="<?php if($caserenal!=""){  echo $caserenal->phosphorus; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">>2.5-4.8 mg%</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Sodium:</label>
			                                                    <input class="form-control sodium" name="sodium" type="text" aria-required="true" value="<?php if($caserenal!=""){  echo $caserenal->sodium; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">133-346 mEq/L</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		 <div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Potassium:</label>
			                                                    <input class="form-control potassium" name="potassium" type="text" aria-required="true" value="<?php if($caserenal!=""){  echo $caserenal->potassium; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">>3.5-5.4 mEq/L</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Chloride:</label>
			                                                    <input class="form-control chloride" name="chloride" type="text" aria-required="true" value="<?php if($caserenal!=""){  echo $caserenal->chloride; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">96-108 mEq/L</label>
			                                                </div>
		                                            	</div>

												     </div>
												 </div>  
                                            </fieldset>
                                            <?php
                                                if($Renalreport!=""){
                                                    foreach($Renalreport as $Renalreportval){
                                                        $pdate = $Renalreportval->dateval;
                                                        $deleted = $Renalreportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="renal_date1" type="hidden"  value="<?php echo $Renalreportval->dateval; ?>" />
                                                            <input class="bun1" type="hidden"  value="<?php echo $Renalreportval->bun; ?>" />
                                                            <input class="urea1" type="hidden"  value="<?php echo $Renalreportval->urea; ?>" />
                                                            <input class="creatinine1" type="hidden"  value="<?php echo $Renalreportval->creatinine; ?>" />
                                                            <input class="uric_acid1" type="hidden"  value="<?php echo $Renalreportval->uric_acid; ?>" />
                                                            <input class="calcium1" type="hidden"  value="<?php echo $Renalreportval->calcium; ?>" />
                                                            <input class="phosphorus1" type="hidden"  value="<?php echo $Renalreportval->phosphorus; ?>" />
                                                            <input class="sodium1" type="hidden"  value="<?php echo $Renalreportval->sodium; ?>" />
                                                            <input class="potassium1" type="hidden"  value="<?php echo $Renalreportval->potassium; ?>" />
                                                            <input class="chloride1" type="hidden"  value="<?php echo $Renalreportval->chloride; ?>" />
                                                             
                                                    <?php   } } }  ?>
                                                        <input class="renal_date1" type="hidden"/>
                                                        <input class="bun1" type="hidden" />
                                                        <input class="urea1" type="hidden" />
                                                        <input class="creatinine1" type="hidden" />
                                                        <input class="uric_acid1" type="hidden" />
                                                        <input class="calcium1" type="hidden"/>
                                                        <input class="phosphorus1" type="hidden">
                                                        <input class="sodium1" type="hidden" />
                                                        <input class="potassium1" type="hidden" />
                                                        <input class="chloride1" type="hidden" />
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn saverenal">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowrenal">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                             {!! Form::close() !!}
                                        </div>

                                        <!-- X-ray - CT - MRI -->
                                        <div class="tab-pane" id="7">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@xraydetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="xray_date" name="xray_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="xray_rand" name="xray_rand" value="<?php if($casexray!=""){  echo $casexray->rand_id; } else { echo rand('99',9999);} ?>">
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                                <div class="form-group col-md-12">
                                                    <label for="mobile">Radiological Reports :</label><br>
                                                    <textarea class="form-control radiological_report_s" name="radiological_report" rows="6" ><?php if($casexray!=""){  echo $casexray->radiological_report; } else { echo " ";} ?></textarea>

                                                    <input type="hidden" class="xrayid"  name="id" value="<?php if($casexray!=""){  echo $casexray->id; } else { echo " ";} ?>">
                                                    
                                                </div>
                                                
                                            </fieldset>
                                            <?php
                                                if($Xrayreport!=""){
                                                    foreach($Xrayreport as $Xrayreportval){
                                                        $pdate = $Xrayreportval->dateval;
                                                        $deleted = $Xrayreportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="xray_date1" type="hidden"  value="<?php echo $Xrayreportval->dateval; ?>" />
                                                            <input class="radiological_report1" type="hidden"  value="<?php echo $Xrayreportval->radiological_report; ?>" />
                                                           
                                                        
                                                    <?php   } } }  ?>

                                                    <input class="xray_date1" type="hidden"  />
                                                    <input class="radiological_report1" type="hidden" />
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savexray">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowxray">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                        <!-- USG Female -->
                                        <div class="tab-pane" id="8">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@maledetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="female_date" name="female_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="female_rand" name="female_rand" value="<?php if($casefemale!=""){  echo $casefemale->rand_id; } else { echo rand('99',9999);} ?>">
                                             <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 <div class="col-md-12"  style="padding-left:0;padding-right:0;">
												     <div class="col-md-6" style="border-right: 2px solid #ccc; padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="text-align: center; padding-bottom: 9px;">
			                                                   <label for="mobile">Uterues</label>
			                                                
			                                            </div>
												     	<div class="col-md-12" style="padding-bottom: 9px;" >
			                                                    <label for="mobile">Uterues Size:</label>
			                                                    <input class="form-control uterues_size" name="uterues_size" type="text" aria-required="true" value="<?php if($casefemale!=""){  echo $casefemale->uterues_size; } else { echo " ";} ?>">

                                                                <input type="hidden" class="femaleid"  name="id" value="<?php if($casefemale!=""){  echo $casefemale->id; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-12" style="padding-bottom: 9px;">
			                                                    <label for="mobile">Endometrial thickness:</label>
			                                                    <input class="form-control thickness" name="thickness" type="text" aria-required="true" value="<?php if($casefemale!=""){  echo $casefemale->thickness; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-12" style="padding-bottom: 9px;">
			                                                
			                                                    <label for="mobile">Fibroids Number:</label>
			                                                    <input class="form-control fibroids_no" name="fibroids_no" type="text" aria-required="true" value="<?php if($casefemale!=""){  echo $casefemale->fibroids_no; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-12" style="padding-bottom: 9px;">
			                                                    <label for="mobile">Description:</label>
			                                                    <textarea class="form-control description" name="description"  aria-required="true"><?php if($casefemale!=""){  echo $casefemale->description; } else { echo " ";} ?></textarea>
			                                            </div>
			                                           
			                                            
												     </div>
												     <div class="col-md-6" style="padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="text-align: center;padding-bottom: 9px; ">
			                                                   <label for="mobile">Ovary</label>
			                                                
			                                            </div>
												     	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
													     	<div class="col-md-4" style="margin: 0;">
			                                                    <label for="mobile">&nbsp;</label>
			                                                </div>
			                                                <div class="col-md-4" style="text-align: center;">
			                                                    <label for="mobile">RT</label>
			                                                </div>
			                                                <div class="col-md-4" style="text-align: center;">
			                                                    <label for="mobile" >LT</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-4" style="margin: 0;">
			                                                    <label for="mobile">Size:</label>
			                                                </div>
			                                               <div class="col-md-4" style="margin: 0;">
			                                                    <input class="form-control ovary_size_rt" name="ovary_size_rt" type="text" aria-required="true" value="<?php if($casefemale!=""){  echo $casefemale->ovary_size_rt; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="margin: 0;">
			                                                    <input class="form-control ovary_size_lt" name="ovary_size_lt" type="text" aria-required="true" value="<?php if($casefemale!=""){  echo $casefemale->ovary_size_lt; } else { echo " ";} ?>">
			                                                    
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		 <div class="col-md-4" style="margin: 0;">
			                                                    <label for="mobile">Volume:</label>
			                                                </div>
			                                                <div class="col-md-4" style="margin: 0;">
			                                                    <input class="form-control ovary_volume_rt" name="ovary_volume_rt" type="text" aria-required="true" value="<?php if($casefemale!=""){  echo $casefemale->ovary_volume_rt; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="margin: 0;">
			                                                    <input class="form-control ovary_volume_lt" name="ovary_volume_lt" type="text" aria-required="true" value="<?php if($casefemale!=""){  echo $casefemale->ovary_volume_lt; } else { echo " ";} ?>">
			                                                    
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-4" style="margin: 0;">
			                                                    <label for="mobile">Follicles:</label>
			                                                </div>
			                                                <div class="col-md-4" style="margin: 0;">
			                                                    <input class="form-control follicles_rt" name="follicles_rt" type="text" aria-required="true" value="<?php if($casefemale!=""){  echo $casefemale->follicles_rt; } else { echo " ";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="margin: 0;">
			                                                    <input class="form-control follicles_lt" name="follicles_lt" type="text" aria-required="true" value="<?php if($casefemale!=""){  echo $casefemale->follicles_lt; } else { echo " ";} ?>">
			                                                    
			                                                </div>
		                                            	</div>

												     </div>
												 </div>  
                                            </fieldset>
                                            <?php
                                                if($Femalereport!=""){
                                                    foreach($Femalereport as $Femalereportval){
                                                        $pdate = $Femalereportval->dateval;
                                                        $deleted = $Femalereportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="female_date1" type="hidden"  value="<?php echo $Femalereportval->dateval; ?>" />
                                                            <input class="uterues_size1" type="hidden"  value="<?php echo $Femalereportval->uterues_size; ?>" />
                                                            <input class="thickness1" type="hidden"  value="<?php echo $Femalereportval->thickness; ?>" />
                                                            <input class="fibroids_no1" type="hidden"  value="<?php echo $Femalereportval->fibroids_no; ?>" />
                                                            <input class="description1" type="hidden"  value="<?php echo $Femalereportval->description; ?>" />
                                                            <input class="ovary_size_rt1" type="hidden"  value="<?php echo $Femalereportval->ovary_size_rt; ?>" />
                                                            <input class="ovary_size_lt1" type="hidden"  value="<?php echo $Femalereportval->ovary_size_lt; ?>" />
                                                            <input class="ovary_volume_rt1" type="hidden"  value="<?php echo $Femalereportval->ovary_volume_rt; ?>" />
                                                            <input class="ovary_volume_lt1" type="hidden"  value="<?php echo $Femalereportval->ovary_volume_lt; ?>" />
                                                            <input class="follicles_rt1" type="hidden"  value="<?php echo $Femalereportval->follicles_rt; ?>" />
                                                            <input class="follicles_lt1" type="hidden"  value="<?php echo $Femalereportval->follicles_lt; ?>" />
                                                             
                                                    <?php   } } }  ?>
                                                        <input class="female_date1" type="hidden">
                                                        <input class="uterues_size1" type="hidden">
                                                        <input class="thickness1" type="hidden">
                                                        <input class="fibroids_no1" type="hidden">
                                                        <input class="description1" type="hidden">
                                                        <input class="ovary_size_rt1" type="hidden">
                                                        <input class="ovary_size_lt1" type="hidden">
                                                        <input class="ovary_volume_rt1" type="hidden">
                                                        <input class="ovary_volume_lt1" type="hidden">
                                                        <input class="follicles_rt1" type="hidden">
                                                        <input class="follicles_lt1" type="hidden">
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savefemale">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowfemale">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                         <!-- USG Male -->
                                        <div class="tab-pane" id="9">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@maledetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="male_date" name="male_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="male_rand" name="male_rand" value="<?php if($casemale!=""){  echo $casemale->rand_id; } else { echo rand('99',9999);} ?>">
                                             <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 
												     	<div class="col-md-6" style="padding-bottom: 9px;" >
			                                                    <label for="mobile">Prostate Size:</label>
			                                                    <input class="form-control size" name="size" type="text" aria-required="true" value="<?php if($casemale!=""){  echo $casemale->size; } else { echo " ";} ?>">

                                                                <input type="hidden" class="maleid"  name="id" value="<?php if($casemale!=""){  echo $casemale->id; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-6" style="padding-bottom: 9px;">
			                                                    <label for="mobile">Serum PSA:</label>
			                                                    <input class="form-control serum_psa" name="serum_psa" type="text" aria-required="true" value="<?php if($casemale!=""){  echo $casemale->serum_psa; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-6" style="padding-bottom: 9px;">
			                                                
			                                                    <label for="mobile">Prostate Volume:</label>
			                                                    <input class="form-control volume" name="volume" type="text" aria-required="true" value="<?php if($casemale!=""){  echo $casemale->volume; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-6" style="padding-bottom: 9px;">
			                                                    <label for="mobile">Other Findings:</label>
			                                                    <textarea class="form-control other_findings" name="other_findings"  aria-required="true"><?php if($casemale!=""){  echo $casemale->other_findings; } else { echo " ";} ?></textarea>
			                                            </div>

                                            </fieldset>
                                            <?php
                                                if($Malereport!=""){
                                                    foreach($Malereport as $Malereportval){
                                                        $pdate = $Malereportval->dateval;
                                                        $deleted = $Malereportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="male_date1" type="hidden"  value="<?php echo $Malereportval->dateval; ?>" />
                                                            <input class="size1" type="hidden"  value="<?php echo $Malereportval->size; ?>" />
                                                            <input class="volume1" type="hidden"  value="<?php echo $Malereportval->volume; ?>" />
                                                            <input class="serum_psa1" type="hidden"  value="<?php echo $Malereportval->serum_psa; ?>" />
                                                            <input class="other_findings1" type="hidden"  value="<?php echo $Malereportval->other_findings; ?>" />
                                                             
                                                    <?php   } } }  ?>
                                                        <input class="male_date1" type="hidden" />
                                                        <input class="size1" type="hidden" />
                                                        <input class="volume1" type="hidden" />
                                                        <input class="serum_psa1" type="hidden" />
                                                        <input class="other_findings1" type="hidden"/>
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savemale">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowmale">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                         <!-- Immunology -->
                                        <div class="tab-pane" id="10">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@updatespecficdetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="immunology_date" name="immunology_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="immunology_rand" name="immunology_rand" value="<?php if($caseimmunology!=""){  echo $caseimmunology->rand_id; } else { echo rand('99',9999);} ?>">
                                             <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 	<div class="col-md-12">
												     	<div class="col-md-6">
			                                                    <label for="mobile">IgG:</label>
			                                                    <input class="form-control igg_imm" name="igg" type="text" aria-required="true" value="<?php if($caseimmunology!=""){  echo $caseimmunology->id; } else { echo " ";} ?>">

                                                                <input type="hidden" class="immunology_id"  name="id" value="<?php if($caseimmunology!=""){  echo $caseimmunology->id; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-6" >
			                                                    <label for="mobile" style="margin-top: 3rem;">>600 to 1400 mg/dl</label>
		                                                </div>
		                                            </div>
		                                            <div class="col-md-12">
			                                            <div class="col-md-6" >
			                                                    <label for="mobile">IgE:</label>
			                                                    <input class="form-control ige" name="ige" type="text" aria-required="true" value="<?php if($caseimmunology!=""){  echo $caseimmunology->ige; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-6" >
			                                                    <label for="mobile" style="margin-top: 3rem;">>0.0002 to 0.2 mg/dl</label>
		                                                </div>
	                                                </div>
	                                                <div class="col-md-12">
		                                                <div class="col-md-6">
			                                                    <label for="mobile">IgM:</label>
			                                                    <input class="form-control igm" name="igm" type="text" aria-required="true" value="<?php if($caseimmunology!=""){  echo $caseimmunology->igm; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-6" >
			                                                    <label for="mobile" style="margin-top: 3rem;">>45 to 250 mg/dl</label>
		                                                </div>
	                                                </div>
	                                                <div class="col-md-12">
			                                            <div class="col-md-6">
			                                                    <label for="mobile">IgA:</label>
			                                                    <input class="form-control iga" name="iga" type="text" aria-required="true" value="<?php if($caseimmunology!=""){  echo $caseimmunology->iga; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-6" >
			                                                    <label for="mobile" style="margin-top: 3rem;">>80 to 350 mg/dl</label>
		                                                </div>
	                                                </div>
	                                                <div class="col-md-12">
			                                            <div class="col-md-6" style="padding-bottom: 9px;" >
			                                                    <label for="mobile">Itg:</label>
			                                                    <input class="form-control itg" name="itg" type="text" aria-required="true" value="<?php if($caseimmunology!=""){  echo $caseimmunology->itg; } else { echo " ";} ?>">
			                                            </div>
			                                            <div class="col-md-6">
			                                                    <label for="mobile" style="margin-top: 3rem;"><4.00 ml</label>
		                                                </div>	
	                                                </div>    

                                            </fieldset>
                                            <?php
                                                if($Immunologyreport!=""){
                                                    foreach($Immunologyreport as $Immunologyreportval){
                                                        $pdate = $Immunologyreportval->dateval;
                                                        $deleted = $Immunologyreportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="immunology_date1" type="hidden"  value="<?php echo $Immunologyreportval->dateval; ?>" />
                                                            <input class="igg1" type="hidden"  value="<?php echo $Immunologyreportval->igg; ?>" />
                                                            <input class="ige1" type="hidden"  value="<?php echo $Immunologyreportval->ige; ?>" />
                                                            <input class="igm1" type="hidden"  value="<?php echo $Immunologyreportval->igm; ?>" />
                                                            <input class="iga1" type="hidden"  value="<?php echo $Immunologyreportval->iga; ?>" />
                                                            <input class="itg1" type="hidden"  value="<?php echo $Immunologyreportval->itg; ?>" />
                                                             
                                                    <?php   } } }  ?>
                                                        <input class="igg1" type="hidden" />
                                                        <input class="ige1" type="hidden"/>
                                                        <input class="igm1" type="hidden"/>
                                                        <input class="iga1" type="hidden" />
                                                        <input class="itg1" type="hidden"/>
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn saveimmunology">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowimmunology">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>


                                        <!-- Specific -->
                                        <div class="tab-pane" id="11">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@updatespecficdetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="specific_date" name="specific_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="specific_rand" name="specific_rand" value="<?php if($casespecific!=""){  echo $casespecific->rand_id; } else { echo rand('99',9999);} ?>">
                                             <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 <div class="col-md-12"  style="padding-left:0;padding-right:0;">
												     <div class="col-md-6" style="border-right: 2px solid #ccc; padding: 0px; margin-top: 0px;">
												     	
												     	<div class="col-md-12" style="padding-bottom: 9px;" >
			                                                    <label for="mobile">Other Findings:</label>
			                                                    <textarea class="form-control other_findings_sp" name="other_findings"  aria-required="true" rows="8"><?php if($casespecific!=""){  echo $casespecific->other_findings; } else { echo "";} ?></textarea>
                                                                <input type="hidden" class="specific_id"  name="id" value="<?php if($casespecific!=""){  echo $casespecific->id; } else { echo " ";} ?>">
			                                            </div>
												     </div>
												     <div class="col-md-6" style="padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding-bottom: 9px;">
													     	
			                                                    <label for="mobile" >Other Findings:</label>
			                                               
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-4" style="margin: 0;">
			                                                    <label for="mobile">Define Field 1:</label>
			                                                </div>
			                                               <div class="col-md-8" style="margin: 0;">
			                                                    <input class="form-control define_field1" name="define_field1" type="text" aria-required="true" value="<?php if($casespecific!=""){  echo $casespecific->define_field1; } else { echo "";} ?>">
			                                                    
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-4" style="margin: 0;">
			                                                    <label for="mobile">Define Field 2:</label>
			                                                </div>
			                                               <div class="col-md-8" style="margin: 0;">
			                                                    <input class="form-control define_field2" name="define_field2" type="text" aria-required="true" value="<?php if($casespecific!=""){  echo $casespecific->define_field2; } else { echo "";} ?>">
			                                                    
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-4" style="margin: 0;">
			                                                    <label for="mobile">Define Field 3:</label>
			                                                </div>
			                                               <div class="col-md-8" style="margin: 0;">
			                                                    <input class="form-control define_field3" name="define_field3" type="text" aria-required="true" value="<?php if($casespecific!=""){  echo $casespecific->define_field3; } else { echo "";} ?>">
			                                                    
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-4" style="margin: 0;">
			                                                    <label for="mobile">Define Field 4:</label>
			                                                </div>
			                                               <div class="col-md-8" style="margin: 0;">
			                                                    <input class="form-control define_field4" name="define_field4" type="text" aria-required="true" value="<?php if($casespecific!=""){  echo $casespecific->define_field4; } else { echo "";} ?>" >
			                                                    
			                                                </div>
		                                            	</div>

												     </div>
												 </div>  
                                            </fieldset>
                                            <?php
                                                if($Specificreport!=""){
                                                    foreach($Specificreport as $Specificreportval){
                                                        $pdate = $Specificreportval->dateval;
                                                        $deleted = $Specificreportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="specific_date1" type="hidden"  value="<?php echo $Specificreportval->dateval; ?>" />
                                                            <input class="other_findings1" type="hidden"  value="<?php echo $Specificreportval->other_findings; ?>" />
                                                             <input class="define_field1_1" type="hidden"  value="<?php echo $Specificreportval->define_field1; ?>" />
                                                            <input class="define_field2_1" type="hidden"  value="<?php echo $Specificreportval->define_field2; ?>" />
                                                             <input class="define_field3_1" type="hidden"  value="<?php echo $Specificreportval->define_field3; ?>" />
                                                            <input class="define_field4_1" type="hidden"  value="<?php echo $Specificreportval->define_field4; ?>" />
                                                             
                                                    <?php   } } }  ?>
                                                         <input class="specific_date1" type="hidden"/>
                                                        <input class="other_findings1" type="hidden"/>
                                                         <input class="define_field1_1" type="hidden"/>
                                                        <input class="define_field2_1" type="hidden"/>
                                                         <input class="define_field3_1" type="hidden"/>
                                                        <input class="define_field4_1" type="hidden"/>
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savespecific">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowspecific">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                        <!-- Liver Profile -->
                                        <div class="tab-pane" id="12">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@liverdetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="liver_date" name="liver_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="liver_rand" name="liver_rand" value="<?php if($caseliver!=""){  echo $caseliver->rand_id; } else { echo rand('99',9999);} ?>">
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 <div class="col-md-12"  style="padding-left:0;padding-right:0;">
												     <div class="col-md-6" style=" padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px; ">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Total Bil:</label>
			                                                    <input class="form-control total_bil" name="total_bil" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->total_bil; } else { echo "";} ?>">
                                                                <input class="form-control liver_id" name="id" type="hidden" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->id; } else { echo "";} ?>" >
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">0.3 to 1.3 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Dir.Bilirubin:</label>
			                                                    <input class="form-control dir_bilirubin" name="dir_bilirubin" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->dir_bilirubin; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">0.1 to 0.4 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Ind.Bilirubin:</label>
			                                                    <input class="form-control ind_bilirubin" name="ind_bilirubin" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->ind_bilirubin; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">0.2 to 0.9 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Gamma G.T:</label>
			                                                    <input class="form-control gamma_gt" name="gamma_gt" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->gamma_gt; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">Upto 50 IU/l</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Total Proteins:</label>
			                                                    <input class="form-control total_protein" name="total_protein" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->total_protein; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">6.3 to 7.9 gm%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            
												     </div>
												     <div class="col-md-6" style="padding: 0px; margin-top: 0px;border-left: 2px solid #ccc;">
												     	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
													     	<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Albumin:</label>
			                                                    <input class="form-control albumin" name="albumin" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->albumin; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">3.7 to 5.3 gm%</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Globulin:</label>
			                                                    <input class="form-control globulin" name="globulin" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->globulin; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">1.8 to 3.6 gm%</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		 <div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">S.G.O.T:</label>
			                                                    <input class="form-control sgot" name="sgot" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->sgot; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">upto 35 IU/L</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">S.G.P.T:</label>
			                                                    <input class="form-control sgpt" name="sgpt" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->sgpt; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">upto 40 IU/L</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Alk. Phos:</label>
			                                                    <input class="form-control alk_phos" name="alk_phos" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->alk_phos; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">37 to 147 IU/L</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Aust.Antigen:</label>
			                                                    <input class="form-control aust_antigen" name="aust_antigen" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->aust_antigen; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">(By Elisa)</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Amylase:</label>
			                                                    <input class="form-control amylase" name="amylase" type="text" aria-required="true" value="<?php if($caseliver!=""){  echo $caseliver->amylase; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">upto 96 IU/L</label>
			                                                </div>
		                                            	</div>

												     </div>
												 </div>  
                                            </fieldset>
                                            <?php
                                                if($Liverreport!=""){
                                                    foreach($Liverreport as $Liverreportval){
                                                        $pdate = $Liverreportval->dateval;
                                                        $deleted = $Liverreportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="liver_date1" type="hidden"  value="<?php echo $Liverreportval->dateval; ?>" />
                                                            <input class="total_bil1" type="hidden"  value="<?php echo $Liverreportval->total_bil1; ?>" />
                                                             <input class="dir_bilirubin1" type="hidden"  value="<?php echo $Liverreportval->dir_bilirubin; ?>" />
                                                            <input class="ind_bilirubin1" type="hidden"  value="<?php echo $Liverreportval->ind_bilirubin; ?>" />
                                                             <input class="gamma_gt1" type="hidden"  value="<?php echo $Liverreportval->gamma_gt; ?>" />
                                                            <input class="total_protein1" type="hidden"  value="<?php echo $Liverreportval->total_protein; ?>" />
                                                             <input class="albumin1" type="hidden"  value="<?php echo $Liverreportval->albumin; ?>" />
                                                             <input class="globulin1" type="hidden"  value="<?php echo $Liverreportval->globulin; ?>" />
                                                             <input class="sgot1" type="hidden"  value="<?php echo $Liverreportval->sgot; ?>" />
                                                             <input class="sgpt1" type="hidden"  value="<?php echo $Liverreportval->sgpt; ?>" />
                                                             <input class="alk_phos1" type="hidden"  value="<?php echo $Liverreportval->alk_phos; ?>" />
                                                             <input class="aust_antigen1" type="hidden"  value="<?php echo $Liverreportval->aust_antigen; ?>" />
                                                             <input class="amylase1" type="hidden"  value="<?php echo $Liverreportval->amylase; ?>" />
                                                           
                                                        
                                                    <?php   } } }  ?>
                                                        <input class="liver_date1" type="hidden" />
                                                        <input class="total_bil1" type="hidden"/>
                                                         <input class="dir_bilirubin1" type="hidden" />
                                                        <input class="ind_bilirubin1" type="hidden" />
                                                         <input class="gamma_gt1" type="hidden"/>
                                                        <input class="total_protein1" type="hidden"/>
                                                         <input class="albumin1" type="hidden" />
                                                         <input class="globulin1" type="hidden" />
                                                         <input class="sgot1" type="hidden" />
                                                         <input class="sgpt1" type="hidden"/>
                                                         <input class="alk_phos1" type="hidden"/>
                                                         <input class="aust_antigen1" type="hidden" />
                                                         <input class="amylase1" type="hidden" />
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn saveliver">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowliver">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                         <!-- Lipid Profile -->
                                        <div class="tab-pane" id="13">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@lipiddetail']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="lipid_date" name="lipid_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="lipid_rand" name="lipid_rand" value="<?php if($caselipid!=""){  echo $caselipid->rand_id; } else { echo rand('99',9999);} ?>">
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 <div class="col-md-12"  style="padding-left:0;padding-right:0;">
												     <div class="col-md-6" style=" padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px; ">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Total Cholesterol:</label>
			                                                    <input class="form-control total_cholesterol" name="total_cholesterol" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->total_cholesterol; } else { echo "";} ?>" >
                                                                <input class="form-control lipid_id" name="id" type="hidden" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->id; } else { echo "";} ?>" >
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">125 to 200 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Triglycerides:</label>
			                                                    <input class="form-control triglycerides" name="triglycerides" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->triglycerides; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">25 to 200 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">HDL Cholesterol:</label>
			                                                    <input class="form-control hdl_cholesterol" name="hdl_cholesterol" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->hdl_cholesterol; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">35 to 80 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">LDL Cholesterol:</label>
			                                                    <input class="form-control ldl_cholesterol" name="ldl_cholesterol" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->ldl_cholesterol; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">85 to 130 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">VLDL:</label>
			                                                    <input class="form-control vldl" name="vldl" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->vldl; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">10 to 38 mg/dl</label>
			                                                </div>
			                                                
			                                            </div>
			                                            
												     </div>
												     <div class="col-md-6" style="padding: 0px; margin-top: 0px;border-left: 2px solid #ccc;">
												     	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
													     	<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Chol/HDL ratio:</label>
			                                                    <input class="form-control hdl_ratio" name="hdl_ratio" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->hdl_ratio; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">upto 4.5</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">LDL/HDL:</label>
			                                                    <input class="form-control ldl_hdl" name="ldl_hdl" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->ldl_hdl; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">0.5 to 3.5 </label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		 <div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Lipoprotein:</label>
			                                                    <input class="form-control lipoprotein" name="lipoprotein" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->lipoprotein; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;"><30.0</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Apolipoprotein-A:</label>
			                                                    <input class="form-control apolipoprotein_a" name="apolipoprotein_a" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->apolipoprotein_a; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">M:110-205 F:125-215</label>
			                                                </div>
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
		                                            		<div class="col-md-8" style="margin: 0;">
			                                                    <label for="mobile">Apolipoprotein-B:</label>
			                                                    <input class="form-control apolipoprotein_b" name="apolipoprotein_b" type="text" aria-required="true" value="<?php if($caselipid!=""){  echo $caselipid->apolipoprotein_b; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">M:55-140 F:55-125</label>
			                                                </div>
		                                            	</div>


												     </div>
												 </div>  
                                            </fieldset>
                                            <?php
                                                if($Lipidreport!=""){
                                                    foreach($Lipidreport as $Lipidreportval){
                                                        $pdate = $Lipidreportval->dateval;
                                                        $deleted = $Lipidreportval->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="lipid_date1" type="hidden"  value="<?php echo $Lipidreportval->dateval; ?>" />
                                                            <input class="total_cholesterol1" type="hidden"  value="<?php echo $Lipidreportval->total_cholesterol; ?>" />
                                                             <input class="triglycerides1" type="hidden"  value="<?php echo $Lipidreportval->triglycerides; ?>" />
                                                            <input class="hdl_cholesterol1" type="hidden"  value="<?php echo $Lipidreportval->hdl_cholesterol; ?>" />
                                                             <input class="ldl_cholesterol1" type="hidden"  value="<?php echo $Lipidreportval->ldl_cholesterol; ?>" />
                                                            <input class="vldl1" type="hidden"  value="<?php echo $Lipidreportval->vldl; ?>" />
                                                             <input class="hdl_ratio1" type="hidden"  value="<?php echo $Lipidreportval->hdl_ratio; ?>" />
                                                             <input class="ldl_hdl1" type="hidden"  value="<?php echo $Lipidreportval->ldl_hdl; ?>" />
                                                             <input class="lipoprotein1" type="hidden"  value="<?php echo $Lipidreportval->lipoprotein; ?>" />
                                                             <input class="apolipoprotein_a1" type="hidden"  value="<?php echo $Lipidreportval->apolipoprotein_a; ?>" />
                                                             <input class="apolipoprotein_b1" type="hidden"  value="<?php echo $Lipidreportval->apolipoprotein_b; ?>" />
                                                           
                                                        
                                                    <?php   } } }  ?>
                                                    <input class="lipid_date1" type="hidden"/>
                                                    <input class="total_cholesterol1" type="hidden"  />
                                                     <input class="triglycerides1" type="hidden" />
                                                    <input class="hdl_cholesterol1" type="hidden"  />
                                                     <input class="ldl_cholesterol1" type="hidden" />
                                                    <input class="vldl1" type="hidden"  />
                                                     <input class="hdl_ratio1" type="hidden"  />
                                                     <input class="ldl_hdl1" type="hidden"  />
                                                     <input class="lipoprotein1" type="hidden" />
                                                     <input class="apolipoprotein_a1" type="hidden" />
                                                     <input class="apolipoprotein_b1" type="hidden"/>
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savelipid">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowlipid">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                        <!-- Diabetes Profile -->
                                        <div class="tab-pane" id="14">
                                        	{!! Form::open(['action' => 'LA\MedicalcasesController@diabetesdetail']) !!}
                                        	<input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
											<input type="hidden" class="diabetes_date" name="diabetes_date" value="<?php echo date('d/m/Y'); ?>">
											<input type="hidden" class="diabetes_rand" name="diabetes_rand" value="<?php if($casediabetes!=""){  echo $casediabetes->rand_id; } else { echo rand('99',9999);} ?>">
											<input type="hidden" name="id" value="<?php if($casediabetes!=""){  echo $casediabetes->id; } else { echo "";} ?>" class="diabetes_id">

                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            	 <div class="col-md-12"  style="padding-left:0;padding-right:0;">
												     <div class="col-md-6" style=" padding: 0px; margin-top: 0px;">
												     	<div class="col-md-12" style="text-align: center;padding-bottom: 9px; ">
			                                                   <label for="mobile">Blood Examination</label>
			                                                
			                                            </div>
												     	<div class="col-md-12" style="padding: 0px; padding-bottom: 9px; ">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Fasting:</label>
			                                                    <input class="form-control blood_fasting" name="blood_fasting" type="text" aria-required="true" value="<?php if($casediabetes!=""){  echo $casediabetes->blood_fasting; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">70 to 100 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Post Prandial:</label>
			                                                    <input class="form-control blood_prandial" name="blood_prandial" type="text" aria-required="true" value="<?php if($casediabetes!=""){  echo $casediabetes->blood_prandial; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">80 to 130 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-8">
			                                                    <label for="mobile">Random:</label>
			                                                    <input class="form-control blood_random" name="blood_random" type="text" aria-required="true" value="<?php if($casediabetes!=""){  echo $casediabetes->blood_random; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-4" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">upto 130 mg%</label>
			                                                </div>
			                                                
			                                            </div>
			                                            
												     </div>
												     <div class="col-md-6" style="margin-top: 0px;border-left: 2px solid #ccc;">
												     	<div class="col-md-12" style="text-align: center;padding-bottom: 9px; ">
			                                                   <label for="mobile">Urine Examination</label>
			                                                
			                                            </div>
												     	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
			                                                    <label for="mobile">Fasting:</label>
			                                                    <input class="form-control urine_fasting" name="urine_fasting" type="text" aria-required="true" value="<?php if($casediabetes!=""){  echo $casediabetes->urine_fasting; } else { echo "";} ?>">
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
			                                                    <label for="mobile">Post Prandial:</label>
			                                                    <input class="form-control urine_prandial" name="urine_prandial" type="text" aria-required="true" value="<?php if($casediabetes!=""){  echo $casediabetes->urine_prandial; } else { echo "";} ?>">
		                                            	</div>

		                                            	<div class="col-md-12" style="padding: 0px;padding-bottom: 9px;">
			                                                    <label for="mobile">Random:</label>
			                                                    <input class="form-control urine_random" name="urine_random" type="text" aria-required="true" value="<?php if($casediabetes!=""){  echo $casediabetes->urine_random; } else { echo "";} ?>">
		                                            	</div>

												     </div>
												 </div>  
                                            </fieldset>
                                             <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
							                	 <div class="col-md-12" style="padding: 0px; padding-bottom: 9px;">
			                                                <div class="col-md-6">
			                                                    <label for="mobile">Glu.Tol.Test:</label>
			                                                    <input class="form-control glu_test" name="glu_test" type="text" aria-required="true" value="<?php if($casediabetes!=""){  echo $casediabetes->glu_test; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                <div class="col-md-6" >
			                                                    <label for="mobile">Glycosylated HB</label>
			                                                    <input class="form-control glycosylated_hb" name="glycosylated_hb" type="text" aria-required="true" value="<?php if($casediabetes!=""){  echo $casediabetes->glycosylated_hb; } else { echo "";} ?>">
			                                                </div>
			                                                
			                                            </div>
							                </fieldset>

							                <?php
						                        if($Diabetesreport!=""){
						                            foreach($Diabetesreport as $Diabetesreportval){
						                                $pdate = $Diabetesreportval->dateval;
						                                $deleted = $Diabetesreportval->deleted_at;
						                                $tdate = date('d/m/Y');
						                                if($pdate == $tdate && $deleted==NULL){?>
						                                    <input class="diabetes_date1" type="hidden"  value="<?php echo $Diabetesreportval->dateval; ?>" />
						                                    <input class="blood_fasting1" type="hidden"  value="<?php echo $Diabetesreportval->blood_fasting; ?>" />
						                                     <input class="blood_prandial1" type="hidden"  value="<?php echo $Diabetesreportval->blood_prandial; ?>" />
						                                    <input class="blood_random1" type="hidden"  value="<?php echo $Diabetesreportval->blood_random; ?>" />
						                                     <input class="urine_fasting1" type="hidden"  value="<?php echo $Diabetesreportval->urine_fasting; ?>" />
						                                    <input class="urine_prandial1" type="hidden"  value="<?php echo $Diabetesreportval->urine_prandial; ?>" />
						                                     <input class="urine_random1" type="hidden"  value="<?php echo $Diabetesreportval->urine_random; ?>" />
						                                     <input class="glu_test1" type="hidden"  value="<?php echo $Diabetesreportval->glu_test; ?>" />
						                                     <input class="glycosylated_hb1" type="hidden"  value="<?php echo $Diabetesreportval->glycosylated_hb; ?>" />
						                                   
						                                
						                            <?php   } } }  ?>
						                            <input class="diabetes_date1" type="hidden"  />
				                                    <input class="blood_fasting1" type="hidden"  />
				                                     <input class="blood_prandial1" type="hidden" />
				                                    <input class="blood_random1" type="hidden" />
				                                     <input class="urine_fasting1" type="hidden"  />
				                                    <input class="urine_prandial1" type="hidden" />
				                                     <input class="urine_random1" type="hidden" />
				                                     <input class="glu_test1" type="hidden"/>
				                                     <input class="glycosylated_hb1" type="hidden" />
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savediabetes">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowdiabetes">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                        <!-- Cardiac Profile -->
                                        <div class="tab-pane" id="15">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@cardiacreport']) !!}
                                                <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                <input type="hidden" class="cardiac_date" name="cardiac_date" value="<?php echo date('d/m/Y'); ?>">
                                                <input type="hidden" class="cardiac_rand" name="cardiac_rand" value="<?php if($casecardiac!=""){  echo $casecardiac->rand_id; } else { echo rand('99',9999);} ?>">
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">
                                            
												     	<div class="col-md-12" style="padding: 0px; ">
			                                                <div class="col-md-4">
			                                                    <label for="mobile">Homocysteine:</label>
			                                                    <input class="form-control homocysteine" name="homocysteine" type="text" aria-required="true"  value="<?php if($casecardiac!=""){  echo $casecardiac->homocysteine; } else { echo "";} ?>" >
			                                                    
			                                                </div>
			                                                <div class="col-md-2" style="padding-left: 0px;">
			                                                    <label for="mobile" style="margin-top: 3rem;">< 15 umol/L</label>
			                                                </div>

			                                                 <div class="col-md-6" style="margin: 0px;">
			                                                    <label for="mobile">ECG:</label>
			                                                    <input class="form-control ecg" name="ecg" type="text" aria-required="true" value="<?php if($casecardiac!=""){  echo $casecardiac->ecg; } else { echo "";} ?>">
                                                                <input class="form-control cardiac_id" name="id" type="hidden" aria-required="true" value="<?php if($casecardiac!=""){  echo $casecardiac->id; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding: 0px;padding-bottom: 9px; ">
			                                                <div class="col-md-6">&nbsp; </div>

			                                                 <div class="col-md-6">
			                                                    <label for="mobile">2DECHO:</label>
			                                                    <input class="form-control decho" name="decho" type="text" aria-required="true" value="<?php if($casecardiac!=""){  echo $casecardiac->decho; } else { echo "";} ?>">
			                                                    
			                                                </div>
			                                                
			                                            </div>
			                                            <div class="col-md-12" style="padding-bottom: 9px;">
			                                                <div class="col-md-3" style="text-align: center;border: 1px solid #ccc;padding: 10px;">
			                                                    <label for="mobile" style="margin-bottom: 12px;"><u>Lipoprotein</u></label>
			                                                   	<label for="mobile" style="margin-bottom: 12px;"><u>Apolipoprotein-A1</u></label>
			                                                   	<label for="mobile" style="margin-bottom: 12px;"><u>Apolipoprotein-B</u></label>
			                                                   	<label for="mobile" style="margin-bottom: 12px;"><u>Total Cholesterol</u></label>
			                                                   	<label for="mobile" style="margin-bottom: 12px;"><u>LDL</u></label>
			                                                   	<label for="mobile" style="margin-bottom: 12px;"><u>HDL</u></label>
		                                                   		<label for="mobile" style="margin-bottom: 12px;"><u>Triglycerides</u></label>
			                                                    
			                                                </div>
			                                                <div class="col-md-3" style="text-align: center;border: 1px solid #ccc;padding: 10px;    margin: 0px 5px 0px 5px;width: 24%;height: 181px;">
			                                                    <label for="mobile" style="margin-bottom: 12px;"><u>C-Reactive Protein</u></label>  
			                                                </div>
			                                                 <div class="col-md-3" style="text-align: center;border: 1px solid #ccc;padding: 10px;    margin: 0px 5px 0px 5px;width: 22%;height: 181px;">
			                                                    <label for="mobile" style="margin-bottom: 12px;"><u>T3</u></label><br>
			                                                   	<label for="mobile" style="margin-bottom: 12px;"><u>T4</u></label><br>
			                                                   	<label for="mobile" style="margin-bottom: 12px;"><u>TSH</u></label>
			                                                    
			                                                </div>

			                                                 <div class="col-md-3" style="text-align: center;border: 1px solid #ccc;padding: 10px;    margin: 0px 5px 0px 5px; height: 181px;">
			                                                    <label for="mobile" style="margin-bottom: 12px;"><u>Vitamin B-12</u></label>
			                                                   	<label for="mobile" style="margin-bottom: 12px;"><u>25-Oh Vitamin D (Total)</u></label>  
			                                                </div>
			                                            </div>
			                                            <div class="col-md-12" style="padding-bottom: 9px;">
			                                            	<textarea class="form-control"></textarea>
			                                            </div>

                                                        <?php
                                                        if($Cardiacreport!=""){
                                                            foreach($Cardiacreport as $Cardiacreportval){
                                                                $pdate = $Cardiacreportval->dateval;
                                                                $deleted = $Cardiacreportval->deleted_at;
                                                                $tdate = date('d/m/Y');
                                                                if($pdate == $tdate && $deleted==NULL){?>
                                                                    <input class="cardiac_date1" type="hidden"  value="<?php echo $Cardiacreportval->dateval; ?>" />
                                                                    <input class="homocysteine1" type="hidden"  value="<?php echo $Cardiacreportval->homocysteine; ?>" />
                                                                    <input class="ecg1" type="hidden"  value="<?php echo $Cardiacreportval->ecg; ?>" />
                                                                    <input class="decho1" type="hidden"  value="<?php echo $Cardiacreportval->decho; ?>" />
                                                                   
                                                                
                                                            <?php   } } }  ?>

                                                            <input class="cardiac_date1" type="hidden"  />
                                                            <input class="homocysteine1" type="hidden"/>
                                                            <input class="ecg1" type="hidden"/>
                                                            <input class="decho1" type="hidden" />
			                                            
                                            </fieldset>
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savecardiac">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowcardiac">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>

                                        <!-- Serology -->
                                        <div class="tab-pane" id="16">
                                        	{!! Form::open(['action' => 'LA\MedicalcasesController@serologicalreports']) !!}
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">

                                            	<input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
												<input type="hidden" class="serological_date" name="serological_date" value="<?php echo date('d/m/Y'); ?>">
												<input type="hidden" class="serological_rand" name="serological_rand" value="<?php if($caseserological!=""){  echo $caseserological->rand_id; } else { echo rand('99',9999);} ?>">

                                                <div class="form-group col-md-12">
                                                    <label for="mobile">Serological Reports :</label><br>
                                                    <textarea class="form-control serological_report_s"   name="serological_report" rows="6" ><?php if($caseserological!=""){  echo $caseserological->serological_report; } else { echo "";} ?></textarea>
                                                    <input type="hidden" name="id" value="<?php if($caseserological!=""){  echo $caseserological->id; } else { echo "";} ?>" class="serologyid">
                                                    
                                                </div>

                                                 <?php
						                        if($Serologicalreport!=""){
						                            foreach($Serologicalreport as $Serologicalreportval){
						                                $pdate = $Serologicalreportval->dateval;
						                                $deleted = $Serologicalreportval->deleted_at;
						                                $tdate = date('d/m/Y');
						                                if($pdate == $tdate && $deleted==NULL){?>
						                                    <input class="serological_date1" type="hidden"  value="<?php echo $Serologicalreportval->dateval; ?>" />
						                                    <input class="serological_report1" type="hidden"  value="<?php echo $Serologicalreportval->semen_analysis; ?>" />
						                                   
						                                
						                            <?php   } } }  ?>

						                            <input class="serological_date1" type="hidden"  />
						                            <input class="serological_report1" type="hidden" />
                                                
                                            </fieldset>
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn saveserological">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowserological">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                             {!! Form::close() !!}
                                        </div>

                                      
                                	<!-- Semen Analysis -->
                                      <div class="tab-pane" id="17">
                                        	{!! Form::open(['action' => 'LA\MedicalcasesController@semenanalysis']) !!}
                                           
                                            <fieldset class="fieldset" style="margin-top: 10px;padding-bottom: 10px;">

                                            	<input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
												<input type="hidden" class="semen_date" name="semen_date" value="<?php echo date('d/m/Y'); ?>">
												<input type="hidden" class="semen_rand" name="semen_rand" value="<?php if($casesemen!=""){  echo $casesemen->rand_id; } else { echo rand('99',9999);} ?>">

                                                <div class="form-group col-md-12">
                                                    <label for="mobile">Semen Analysis :</label><br>
                                                    <textarea class="form-control semen_analysis_s"  name="semen_analysis" rows="6" ><?php if($casesemen!=""){  echo $casesemen->semen_analysis; } else { echo "";} ?></textarea>
                                                    <input type="hidden" name="id" value="<?php if($casesemen!=""){  echo $casesemen->id; } else { echo "";} ?>" class="semenid">
                                                </div>

                                                <?php
						                        if($Semenanalysis!=""){
						                            foreach($Semenanalysis as $Semenanalysisval){
						                                $pdate = $Semenanalysisval->dateval;
						                                $deleted = $Semenanalysisval->deleted_at;
						                                $tdate = date('d/m/Y');
						                                if($pdate == $tdate && $deleted==NULL){?>
						                                    <input class="semen_date1" type="hidden"  value="<?php echo $Semenanalysisval->dateval; ?>" />
						                                    <input class="semen_analysis1" type="hidden"  value="<?php echo $Semenanalysisval->semen_analysis; ?>" />
						                                   
						                                
						                            <?php   } } }  ?>

						                            <input class="semen_date1" type="hidden"  />
						                            <input class="semen_analysis1" type="hidden" />
                                               
                                            </fieldset>
                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn savesemen">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowsemen">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                             {!! Form::close() !!}
                                        </div>

                                        <div class="tab-pane" id="18">
                                            {!! Form::open(['action' => 'LA\MedicalcasesController@investigationdetail', 'id' => 'medicalcase-add-form5']) !!}
                                            <fieldset class="examin-fieldset">
                                              <div class="scroll-bar-wrap">
                                                  <div class="scroll-box" style="height: 160px;">
                                                <table class="table table-bordered tb-bg investigationtabledata">
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>HB</th>
                                                        <th>T3</th>
                                                        <th>T4</th>
                                                        <th>TSH</th>
                                                        <th>TotalChol</th>
                                                        <th>HdiChol</th>
                                                        <th>&nbsp;</th>
                                                    </tr>
                                                    <?php foreach($CaseInvestigation as $Investigation){ ?>
                                                        <tr id="<?php echo $Investigation->rand_id;?>">
                                                            <td><?php echo $Investigation->invest_date;?></td>
                                                            <td><?php echo $Investigation->hb;?></td>
                                                            <td><?php echo $Investigation->t3;?></td>
                                                            <td><?php echo $Investigation->t4;?></td>
                                                            <td><?php echo $Investigation->tsh;?></td>
                                                            <td><?php echo $Investigation->totalchol;?></td>
                                                            <td><?php echo $Investigation->hdichol;?></td>
                                                            <td>
                                                                <a href="javascript:void(0)" onclick="removeinvestigation(<?php echo $Investigation->rand_id;?>);">Remove</a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    
                                                    
                                                </table>
                                              </div>
                                            </div>

                                                <div class="form-group col-md-12">
                                                    <input class="regid" placeholder="RegID"  type="hidden" name="regid" value="{{ $caseData->regid }}">
                                                    <input type="hidden" class="invest_date" name="invest_date" value="<?php echo date('d/m/Y'); ?>">
                                                    <input type="hidden" class="invest_rand" name="invest_rand" value="<?php if($caseinv!=""){  echo $caseinv->rand_id; } else { echo rand('99',9999);} ?>">
                                                    
                                                </div>

                                                <div class="col-md-12"> 
                                                <fieldset class="fieldset">
                                                    <div class="form-group col-md-4" style="margin-bottom: 0px;">
                                                        <label for="mobile">HB :</label><br>
                                                        <input class="form-control hb" placeholder="HB"  name="hb" type="text" value="<?php if($caseinv!=""){  echo $caseinv->hb; } else { echo "";} ?>" aria-required="true">
                                                        <input type="hidden" name="id" value="<?php if($caseinv!=""){  echo $caseinv->id; } else { echo "";} ?>" class="invid">
                                                    </div>
                                                    <div class="form-group col-md-4" style="margin-bottom: 0px;">
                                                        <label for="mobile">T3 :</label><br>
                                                        <input class="form-control t3" placeholder="T3"  name="t3" type="text" value="<?php if($caseinv!=""){  echo $caseinv->t3; } else { echo "";} ?>" aria-required="true">
                                                    </div>
                                                    <div class="form-group col-md-4" style="margin-bottom: 0px;">
                                                        <label for="mobile">T4 :</label><br>
                                                        <input class="form-control t4" placeholder="T4"  name="t4" type="text" value="<?php if($caseinv!=""){  echo $caseinv->t4; } else { echo "";} ?>" aria-required="true">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="mobile">TSH :</label><br>
                                                        <input class="form-control tsh_i" placeholder="TSH"  name="tsh" type="text" value="<?php if($caseinv!=""){  echo $caseinv->tsh; } else { echo "";} ?>" aria-required="true">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="mobile">TotalChol :</label><br>
                                                        <input class="form-control totalchol" placeholder="TotalChol"  name="totalchol" type="text" value="<?php if($caseinv!=""){  echo $caseinv->totalchol; } else { echo "";} ?>" aria-required="true">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="mobile">HdiChol :</label><br>
                                                        <input class="form-control hdichol" placeholder="HdiChol"  name="hdichol" type="text" value="<?php if($caseinv!=""){  echo $caseinv->hdichol; } else { echo "";} ?>" aria-required="true">
                                                    </div>
                                                    
                                                </fieldset>

                                                <input class="form-control invest_date1" type="hidden" readonly />
                                                <input class="form-control hb1" type="hidden"  readonly />
                                                <input class="form-control t31" type="hidden"  readonly />
                                                <input class="form-control t41" type="hidden" readonly />
                                                <input class="form-control tsh1" type="hidden" readonly />
                                                <input class="form-control totalchol1" type="hidden" readonly />
                                                <input class="form-control hdichol1" type="hidden" readonly />

                                                <textarea class="allvalues" type="hidden" style="display: none;"></textarea>
                                                <input  name="regid1" class="regid1" type="hidden">
                                            </div>
                                                
                                            </fieldset>

                                                <?php
                                                if($CaseInvestigation!=""){
                                                    foreach($CaseInvestigation as $Investigation){
                                                        $pdate = $Investigation->invest_date;
                                                        $deleted = $Investigation->deleted_at;
                                                        $tdate = date('d/m/Y');
                                                        if($pdate == $tdate && $deleted==NULL){?>
                                                            <input class="form-control finvest_date" type="hidden" value="<?php echo $Investigation->invest_date ?>" />
                                                            <input class="form-control fhb" type="hidden"  value="<?php echo $Investigation->hb ?>" />
                                                            <input class="form-control ft3" type="hidden"  value="<?php echo $Investigation->t3 ?>" />
                                                            <input class="form-control ft4" type="hidden" value="<?php echo $Investigation->t4 ?>" />
                                                            <input class="form-control ftsh" type="hidden" value="<?php echo $Investigation->tsh ?>" />
                                                            <input class="form-control ftotalchol" type="hidden" value="<?php echo $Investigation->totalchol ?>" />
                                                            <input class="form-control fhdichol" type="hidden" value="<?php echo $Investigation->hdichol ?>" />
                                                    <?php   } } }

                                                 ?>
                                                  <input class="form-control finvest_date" type="hidden" />
                                                    <input class="form-control fhb" type="hidden"  />
                                                    <input class="form-control ft3" type="hidden"  />
                                                    <input class="form-control ft4" type="hidden" />
                                                    <input class="form-control ftsh" type="hidden"  />
                                                    <input class="form-control ftotalchol" type="hidden"/>
                                                    <input class="form-control fhdichol" type="hidden"  />

                                            <div class="col-md-12" style="text-align: center;">
                                                <button type="button" class="btn btn-success height-btn saveinvestigation">Save</button>
                                                <button type="button" class="btn btn-success height-btn savefollowinvestigation">Save Copy To Followup</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                             {!! Form::close() !!}
                                        </div>
                                    </div>
                          </div>
                </div>
            </div>
           
            
        </div>
    </div>
</div>

		</div>
	</div>
	</div>
	</div>
		

		
	</div>
	</div>
	
	
</div>
<?php
$dataPoints="";
$dataPoints1="";
$dataPoints2="";
$dataPoints3="";
if($heightdata!=""){
 foreach($heightdata as $heightdataval){

	$dataPoints[] = array(
		"y" =>$heightdataval['weight'], "label" =>$heightdataval['dob']
	);
	$dataPoints1[] = array(
		"y" =>$heightdataval['height'], "label" =>$heightdataval['dob']
	);

	
	}
} 
if($examdata!=""){
 foreach($examdata as $examdataval){

    $dataPoints2[] = array(
        "y" =>$examdataval['bp1'], "label" =>$examdataval['examination_date']
    );
    $dataPoints3[] = array(
        "y" =>$examdataval['bp2'], "label" =>$examdataval['examination_date']
    );
    }
} 
?>
@endsection
<style>
    .col-md-6{ margin-top:20px;}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
@push('scripts')
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js'></script>
<script>
$(function () {
   $('.investdate').val('<?php echo date('d/m/yy');?>');
   $('.searchcbc').trigger('click');
 $("input[name='graph']").click(function() {
        var test = $(this).val();

        $("div.desc").hide();
        $("#graph" + test).show();
    });

var chart = new CanvasJS.Chart("chartContainer", {
	
	data: [
	{
		type: "line",
		showInLegend: true,
 		name: "Height (Ft)",
		dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
	},
	{
		type: "line",
		showInLegend: true,
 		name: "Weight (Kg)",
		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
	},
	{
		type: "line",
		showInLegend: true,
 		name: "BP Low",
		dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
	},
	{
		type: "line",
		showInLegend: true,
 		name: "BP High",
		dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
	}
	]
});
chart.render();

var chart = new CanvasJS.Chart("chartContainer1", {
    
    data: [
    {
        type: "line",
        showInLegend: true,
        name: "Height (Ft)",
        dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
    }
    ]
});
chart.render();

var chart = new CanvasJS.Chart("chartContainer2", {
    
    data: [
    {
        type: "line",
        showInLegend: true,
        name: "Weight (Kg)",
        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
    }
    ]
});
chart.render();

var chart = new CanvasJS.Chart("chartContainer3", {
    
    data: [
    {
        type: "line",
        showInLegend: true,
        name: "BP Low",
        dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
    }
    ]
});
chart.render();

var chart = new CanvasJS.Chart("chartContainer4", {
    
    data: [
    {
        type: "line",
        showInLegend: true,
        name: "BP High",
        dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
    }
    ]
});
chart.render();
	

	 $(".saveheightweight").on("click", function(){
        var dateval = $('.datevalht').val();
        var height = $('.height').val();
        var weight = $('.weight').val();
      //   var blood_pressure = $('.blood_pressure').val();
     	// var blood_pressure2 = $('.blood_pressure2').val();
        var lmpdata = $('.lmpdata').val();
        alert(lmpdata);
        var regid = $('.regid').val();
        var rand1 = $('.randh').val();
        var id =  $('.heightid').val();
        var already = "";
        <?php
        if($HeightWeight!=""){
            foreach($HeightWeight as $CaseHeightWeight){ 
                $pdate = $CaseHeightWeight->dob;
                $deleted = $CaseHeightWeight->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                    //alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
        if(already ==""){
        if(dateval!=""  && height!="" && weight!=""){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/saveHeightWeights') }}",
                data: { regid:regid, dateval:dateval,height:height,weight:weight, lmpdata:lmpdata,rand:rand1}, 
                success: function( msg ) {
                    if(msg=="1"){
                    $('.heightweighttabledata tr:first').after('<tr id="'+rand1+'"><td>'+dateval+'</td><td>'+height+'</td><td>'+weight+'</td><td>'+lmpdata+'</td><td><a href="javascript:void(0)" onclick="removenewdata('+rand1+')">Remove</a></td></tr>');
                   
                    $('.hheight').val(height);$('.hweight').val(weight);$('.hlmp').val(lmp);$('.hdob').val(dob);
                    alert("Details Saved");
                  }
                 else {
                     alert("Already added.");
                 }
                }
            });
        }else {
            alert('All fields required.');
        }
    }
    else{
         $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateHeightWeights') }}",
                data: { id:id,regid:regid, dateval:dateval,height:height,weight:weight, lmpdata:lmpdata}, 
                success: function( msg ) {
                     $('.heightweighttabledata tr:nth-child(2)').empty();
                    $('.heightweighttabledata tr:first').after('<tr id="'+rand1+'"><td>'+dateval+'</td><td>'+height+'</td><td>'+weight+'</td><td>'+lmpdata+'</td><td><a href="javascript:void(0)" onclick="removenewdata('+rand1+')">Remove</a></td></tr>');
                    
                   // $('.height').val('');$('.weight').val('');$('.blood_pressure').val('');$('.blood_pressure2').val('');
                   alert("Details Update");
                }
            });
    }
    });
    $(".savefollowheightweight").on("click", function(){
        var dateval = $('.hdob').val();
        if(dateval==""){
            dateval = $('.datevalht').val();
        }
        var height = $('.hheight').val();
        if(height==""){
           height = $('.height').val(); 
        }
        var weight = $('.hweight').val();
        if(weight==""){
            weight = $('.weight').val();
        }
      //   var blood_pressure = $('.hblood_pressure').val();
      //   if(blood_pressure==""){
      //       blood_pressure = $('.blood_pressure').val();
      //   }
     	// var blood_pressure2 = $('.blood_pressure2').val();
      //   if(blood_pressure2==""){
      //       blood_pressure2 = $('.blood_pressure2').val();
      //   }
        var lmpdata = $('.hlmp').val();
        if(lmpdata==""){
            lmpdata = $('.lmp').val();
        }
        var regid = $('.regid').val();
      	var rand = '<?php echo rand('99',9999);?>';
        var nnotes = $('.notes1').val();
       
        var notes = 'Height : '+height+'\n Weight : '+weight+'\n '+nnotes+'\n';
        var notes_type = "other";
        var id = $('.nid').val();
       
        var already = "";
        <?php
        if($HeightWeight!=""){
            foreach($HeightWeight as $CaseHeightWeight){ 
                $pdate = $CaseHeightWeight->dob;
                $deleted = $CaseHeightWeight->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                    //alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
       
        if(dateval!="" && height!="" ){
        	if(already ==""){

                $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes}, 
                    success: function( msg ) {
                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/saveHeightWeights') }}",
                            data: { regid:regid, dateval:dateval,height:height,weight:weight,lmpdata:lmpdata,rand:rand}, 
                            success: function( msg ) {
                                
                            }
                        });
                      setTimeout(function(){// wait for 5 secs(2)
                       location.reload(); // then reload the page.(3)
                  }, 1000);
                }
               });

            }
            else{
                var hid =  $('.heightid').val();
                var dateval_1 = $('.datevalht').val();
                var height_1 = $('.height').val();
                var weight_1 = $('.weight').val();
                var blood_pressure_1 = $('.blood_pressure').val();
                var blood_pressure2_1 = $('.blood_pressure2').val();
                var lmpdata_1 = $('.lmpdata').val();
                $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updateHeightWeights') }}",
                    data: { id:hid,regid:regid, dateval:dateval_1,height:height_1,weight:weight_1,lmpdata:lmpdata_1}, 
                    success: function( msg ) {
                    
                    }
                });
            var notes = 'Height : '+height_1+'\n Weight : '+weight_1+'\n '+nnotes+'\n';

                $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes}, 
                    success: function( msg ) {
                      setTimeout(function(){// wait for 5 secs(2)
                       location.reload(); // then reload the page.(3)
                  }, 1000);
                }
               });
            }
            
          
        }else {
              alert('Fill all fields.');
        }
   
    });

     $(".rxdataadd").on("click", function(){
         $('.rxdataadd').prop('disabled', true);
     	var dateval = $('.datevalrx').val();
        var rxprescription = "";
        var rxdays = "";
        var rxfrequency = "";
        var rxremedy = "";
        var rxpotency = "";
        var rand = '<?php echo rand('99',9999);?>';
        var regid = $('.regid').val();
        var notes = rxprescription+'\n'+rxdays+'\n'+rxfrequency+'\n'+rxremedy+'\n'+rxpotency;
        var notes_type = "rx";
        var already = "";
        <?php
        if($CasePotency!=""){
        	foreach($CasePotency as $CasePotencyval){ 
        		$pdate = $CasePotencyval->dateval;
                $deleted = $CasePotencyval->deleted_at;
        		$tdate = date('n/d/Y');
        		if($pdate == $tdate && $deleted==NULL){?>
        			alert("Followup is already added for this date");
        			 already = "Already RX Added";
        	<?php 	} } }

         ?>
         if(already ==""){
        if(dateval!="" ){
            $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/saveremidy') }}",
            data: { regid:regid, rxprescription:rxprescription,rxdays:rxdays,rxfrequency:rxfrequency, rxremedy:rxremedy,rxpotency:rxpotency,rand:rand}, 
            success: function( msg ) {
               
                    $('.rxdatavaltable tr:first').after('<tr id="'+rand+'"><td>'+dateval+'</td><td>'+rxremedy+'</td><td>'+rxpotency+'</td><td>'+rxfrequency+'</td><td>'+rxdays+'</td><td>'+rxprescription+'</td><td><a href="javascript:void(0)" onclick="removedata('+rand+')">Remove</a></td></tr>');
                     $('.followupdataentery1').append('<label for="mobile">Follow Up on'+dateval+'</label><div class="form-group col-md-12"><textarea class="form-control">'+notes+'</textarea></div>');


                    $('.rxprescription1').val(rxprescription);$('.rxdays1').val(rxdays);$('.rxfrequency1').val(rxfrequency);$('.rxremedy1').val(rxremedy);$('.rxpotency1').val(rxpotency);
                   
                     var id = $('.nid').val();
                 	$.ajax({
		                type: "POST",
		                url: "{{ url(config('laraadmin.adminRoute') . '/savenotes') }}",
		                data: { regid:regid, notes:notes, notes_type:notes_type,rand:rand,dateval:dateval}, 
		                success: function( msg ) {
		                  
		                }
		            });
                
                    $('.rxprescription').val('');
                     setTimeout(function(){// wait for 5 secs(2)
				           location.reload(); // then reload the page.(3)
				      }, 500);
                }
            })
        }else {
            alert('Fields required.');
        }
        }
 
    });

 	$('.rxremedy').on('change', function() {
        var rmvalue = $(this).find(':selected').attr('data-remedy');
       
        var rxremedy = this.value;
        var regid = $('.regid').val();
       
        var rxpotency = $('.rxpotency1').val();
       // alert(rxpotency);
        var rxfrequency = $('.rxfrequency1').val();
        var rxdays = $('.rxdays1').val();
      
        var rxprescription = $('.rxprescription1').val();
        var dateval = $('.pdateval').val();
        var rand = $('.prand').val();
       
        if(rxdays==""){
           rxdays = $('.pprxdays1').val();
        }
       
        if(rxprescription==""){
           rxprescription = $('.pprxprescription1').val();
        }
        if(rxpotency==""){
           rxpotency = $('.pprxpotency1').val();
        }
        if(rxfrequency==""){
           rxfrequency = $('.pprxfrequency1').val();
        }
         $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/updateremidy') }}",
            data: {  regid:regid,rxremedy:rxremedy,rand:rand}, 
            success: function( msg ) {

                $('.rxdatavaltable tr:nth-child(2)').empty();
                   
                $('.rxdatavaltable tr:first').after('<tr id="'+rand+'"><td>'+dateval+'</td><td>'+rmvalue+'</td><td>'+rxpotency+'</td><td>'+rxfrequency+'</td><td>'+rxdays+'</td><td>'+rxprescription+'</td><td><a href="javascript:void(0)" onclick="removedata('+rand+')">Remove</a></td></tr>');
                
                $('.rxremedy1').val(rmvalue);
                }
            })
    });

    $('.rxpotency').on('change', function() {
        var ptvalue = $(this).find(':selected').attr('data-potency');
        var rxpotency = this.value;
        var regid = $('.regid').val();
        var rxremedy = $('.rxremedy1').val();
        var rxfrequency = $('.rxfrequency1').val();
        var rxdays = $('.rxdays1').val();
        var rxprescription = $('.rxprescription1').val();
        var dateval = $('.pdateval').val();
        var rand = $('.prand').val();

        if(rxdays==""){
           rxdays = $('.pprxdays1').val();
        }
        if(rxprescription==""){
           rxprescription = $('.pprxprescription1').val();
        }
        if(rxremedy==""){
           rxremedy = $('.pprxremedy1').val();
        }
        if(rxfrequency==""){
           rxfrequency = $('.pprxfrequency1').val();
        }
         $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/updatepotency') }}",
            data: {  regid:regid,rxpotency:rxpotency,rand:rand}, 
            success: function( msg ) {
                $('.rxdatavaltable tr:nth-child(2)').empty();
                   
                $('.rxdatavaltable tr:first').after('<tr id="'+rand+'"><td>'+dateval+'</td><td>'+rxremedy+'</td><td>'+ptvalue+'</td><td>'+rxfrequency+'</td><td>'+rxdays+'</td><td>'+rxprescription+'</td><td><a href="javascript:void(0)" onclick="removedata('+
                    rand+')">Remove</a></td></tr>');

                $('.rxpotency1').val(ptvalue);
                }
            })
    });

    $('.rxfrequency').on('change', function() {
        var fqvalue = $(this).find(':selected').attr('data-frequency');
        var rxfrequency = this.value;
        var regid = $('.regid').val();
        var rxpotency = $('.rxpotency1').val();
        var rxremedy = $('.rxremedy1').val();
        var rxdays = $('.rxdays1').val();
        var rxprescription = $('.rxprescription1').val();
        var dateval = $('.pdateval').val();
        var rand = $('.prand').val();

        if(rxdays==""){
           rxdays = $('.pprxdays1').val();
        }
        if(rxprescription==""){
           rxprescription = $('.pprxprescription1').val();
        }
        if(rxpotency==""){
           rxpotency = $('.pprxpotency1').val();
        }
        if(rxremedy==""){
           rxremedy = $('.pprxremedy1').val();
        }
        
         $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/updatefrequency') }}",
            data: {  regid:regid,rxfrequency:rxfrequency,rand:rand}, 
            success: function( msg ) {
                $('.rxdatavaltable tr:nth-child(2)').empty();
                   
                $('.rxdatavaltable tr:first').after('<tr id="'+rand+'"><td>'+dateval+'</td><td>'+rxremedy+'</td><td>'+rxpotency+'</td><td>'+fqvalue+'</td><td>'+rxdays+'</td><td>'+rxprescription+'</td><td><a href="javascript:void(0)" onclick="removedata('+rand+')">Remove</a></td></tr>');

                $('.rxfrequency1').val(fqvalue);

                }
            })
    });

    $('.rxdays').on('change', function() {
        var daysvalue = $(this).find(':selected').attr('data-days');
        var rxdays = this.value;
        
        var regid = $('.regid').val();
        var rxpotency = $('.rxpotency1').val();
        var rxremedy = $('.rxremedy1').val();
        var rxfrequency = $('.rxfrequency1').val();
        var rxprescription = $('.rxprescription1').val();
        var dateval = $('.pdateval').val();
        var rand = $('.prand').val();

        var scheme_case = $('.scheme_case').val();
        
        if(rxprescription==""){
           rxprescription = $('.pprxprescription1').val();
        }
        if(rxpotency==""){
           rxpotency = $('.pprxpotency1').val();
        }
        if(rxremedy==""){
           rxremedy = $('.pprxremedy1').val();
        }
        if(rxfrequency==""){
           rxfrequency = $('.pprxfrequency1').val();
        }
         $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/updaterxdays') }}",
            data: {  regid:regid,rxdays:rxdays,rand:rand}, 
            success: function( charge ) {
                //console.log(charge);
                //console.log("scheme_case-"+scheme_case);
                //console.log(regid+"--"+rxdays+"--"+rand);
                $('.charges').html(charge);
                var additionalCharge = parseInt($(".additional").html());
                var totalRegularCharge = parseInt(additionalCharge) + parseInt(charge);
                $(".total_chg").html(totalRegularCharge);
            
                $('.rxdatavaltable tr:nth-child(2)').empty();
                   
                $('.rxdatavaltable tr:first').after('<tr id="'+rand+'"><td>'+dateval+'</td><td>'+rxremedy+'</td><td>'+rxpotency+'</td><td>'+rxfrequency+'</td><td>'+daysvalue+'</td><td>'+rxprescription+'</td><td><a href="javascript:void(0)" onclick="removedata('+rand+')">Remove</a></td></tr>');

                $('.rxdays1').val(daysvalue);
                }
            })
    });
        $(".rxprescription").on('keyup', function() {
        var rxdays =  $('.rxdays1').val();
        var rxprescription = this.value;
        var regid = $('.regid').val();
        var rxpotency = $('.rxpotency1').val();
        var rxremedy = $('.rxremedy1').val();
        var rxfrequency = $('.rxfrequency1').val();
        if(rxdays==""){
           rxdays = $('.pprxdays1').val();
        }
        if(rxpotency==""){
           rxpotency = $('.pprxpotency1').val();
        }
        if(rxremedy==""){
           rxremedy = $('.pprxremedy1').val();
        }
        if(rxfrequency==""){
           rxfrequency = $('.pprxfrequency1').val();
        }
       
        
       var dateval = $('.pdateval').val();
        var rand = $('.prand').val();
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateprescription') }}",
                data: { regid:regid, rxprescription:rxprescription,rand:rand}, 
                success: function( msg ) {
                    $('.rxdatavaltable tr:nth-child(2)').empty();
                   
                    $('.rxdatavaltable tr:first').after('<tr id="'+rand+'"><td>'+dateval+'</td><td>'+rxremedy+'</td><td>'+rxpotency+'</td><td>'+rxfrequency+'</td><td>'+rxdays+'</td><td>'+rxprescription+'</td><td><a href="javascript:void(0)" onclick="removedata('+rand+')">Remove</a></td></tr>');
                    
                    $('.rxprescription1').val(rxprescription);

                    
                }
            });
        });
    
    $(".rxdatacopy").on("click", function(){
    
        var dateval = $('.pdateval').val();
        var rand = $('.prand').val();
        var regid = $('.regid').val();
        var rxprescription =  $('.prxprescription1').val();
        var rxdays =  $('.prxdays1').val();
        var rxfrequency =  $('.prxfrequency1').val();
        var rxremedy =  $('.prxremedy1').val();
        var rxpotency =  $('.prxpotency1').val();
       
        if(regid!=""){
            $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/copyremidy') }}",
            data: { regid:regid, rxprescription:rxprescription,rxdays:rxdays,rxfrequency:rxfrequency, rxremedy:rxremedy,rxpotency:rxpotency}, 
            success: function( msg ) {
                   $('.rxdatavaltable tr:nth-child(2)').empty();
                   
                    $('.rxdatavaltable tr:first').after('<tr id="'+rand+'"><td>'+dateval+'</td><td>'+rxremedy+'</td><td>'+rxpotency+'</td><td>'+rxfrequency+'</td><td>'+rxdays+'</td><td>'+rxprescription+'</td><td><a href="javascript:void(0)" onclick="removedata('+rand+')">Remove</a></td></tr>');
                   // $('.rxprescription').val('');
                    setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                }
            })
        }else {
            alert('Fields required.');
        }
 
    });

 	// Communication Detail

  	$(".savecommunication").on("click", function(){
        var complaint_intesity = $('.complaint_intesity').val();
        var current_date = $('.current_date').val();
        var regid = $('.regid').val();
        var rand2 = $('.rand2').val();
        var id =  $('.comid').val();
        var already = "";
        <?php
        if($CommunicationDetail!=""){
            foreach($CommunicationDetail as $CaseCommunicationDetail){
                $pdate = $CaseCommunicationDetail->currentdate;
                $deleted = $CaseCommunicationDetail->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      if(already ==""){
        if(current_date!="" && complaint_intesity!=""){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/communicationdetail') }}",
                data: { regid:regid, currentdate:current_date,complaint_intesity:complaint_intesity,rand:rand2}, 
                success: function( msg ) {
                    if(msg=="1"){
                    $('.communicationdetailtable tr:first').after('<tr id="'+rand2+'"><td>'+current_date+'</td><td>'+complaint_intesity+'</td><td><a href="javascript:void(0)" onclick="removecommunication('+rand2+')">Remove</a></td></tr>');
            
                    $('.ccomplaintintesity1').val(complaint_intesity);$('.ccurrentdate').val(current_date);

                    alert("Details Saved");
                 }
                 else {
                     alert("Already added.");
                 }

                }
            });
        }else {
            alert('All fields required.');
        }
        }
        else{
             $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatecommunicationdetail') }}",
                data: { id:id,regid:regid, complaint_intesity:complaint_intesity}, 
                success: function( msg ) {
                    $('.communicationdetailtable tr:nth-child(2)').empty();
                    $('.communicationdetailtable tr:first').after('<tr id="'+rand2+'"><td>'+current_date+'</td><td>'+complaint_intesity+'</td><td><a href="javascript:void(0)" onclick="removecommunication('+rand2+')">Remove</a></td></tr>');
            
                  $('.ccomplaintintesity1').val(complaint_intesity);$('.ccurrentdate').val(current_date);

                  alert("Details Update");

                }
            });
        }
    });
    $(".savefollowcommunication").on("click", function(){
       	var complaint_intesity = $('.ccomplaintintesity1').val();
        if(complaint_intesity==""){
            complaint_intesity = $('.complaint_intesity').val();
        }
        var current_date = $('.ccurrentdate').val();
        if(current_date==""){
            current_date = $('.current_date').val();
        }
        var dateval = $('.ccurrentdate').val();
        var regid = $('.regid').val();
        var rand2 = $('.rand2').val();
        var rand = '<?php echo rand('99',9999);?>';
        var nnotes = $('.notes1').val();
        
        var notes = 'Comments : '+complaint_intesity+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($CommunicationDetail!=""){
            foreach($CommunicationDetail as $CaseCommunicationDetail){
                $pdate = $CaseCommunicationDetail->currentdate;
                $deleted = $CaseCommunicationDetail->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(current_date!="" && complaint_intesity!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/communicationdetail') }}",
                            data: { regid:regid, currentdate:current_date,complaint_intesity:complaint_intesity,rand:rand2}, 
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                var cid =  $('.comid').val();
                var complaint_intesity_1 = $('.complaint_intesity').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatecommunicationdetail') }}",
                        data: { id:cid,regid:regid, complaint_intesity:complaint_intesity_1}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'Comments : '+complaint_intesity_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
        	 
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // save charges


    $(".savecharges").on("click", function(){
        var additional_name = $('.additional_name_m').val();
        var additional_price = $('.additional_price').val();
        var currentdate = $('.current_date').val();
        var regid = $('.regid').val();
        var randid = $('.rid').val();
        var rand_id1 = $('.rid1').val();
        var add_chg = $('.add_chg').val();
        var reg_chg = $('.reg_chg').val();
        if(randid==""){
           var rand_id = rand_id1;
        }
        else{
            var rand_id = randid;
        }
        
     
        if(additional_name!="" && additional_price!=""){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateadditional') }}",
                data: { regid:regid, additional_name:additional_name,additional_price:additional_price,rand_id:rand_id1,currentdate:currentdate}, 
                success: function( chargedata ) {
                    var chargearr = JSON.parse(chargedata);
                    console.log(chargearr);
                    var charge= chargearr['additionalCharge'];
                    var rowId = chargearr['rowId'];
                    var total_add =0;
                    var total = 0;
                    total_add = parseInt(add_chg) + parseInt(additional_price);
                    total= parseInt(total_add) + parseInt(reg_chg);
                    $(".additional").text(total_add);
                    //$(".total_chg").text(total);
                    var regularCharge = parseInt($(".charges").html());
                    var totalRegularCharge = parseInt(regularCharge) + parseInt(charge);
                    $(".additional").html(parseInt(charge));
                    $(".total_chg").html(totalRegularCharge);
                    

                    //$(".tot_chg").val(total);
                   
                   $('.chargestable tr:first').after('<tr id="'+rowId+'"><td>'+currentdate+'</td><td>'+additional_name+'</td><td>'+additional_price+'</td><td><a href="javascript:void(0)" onclick="removecharges('+rowId+')">Remove</a></td></tr>');
            
                    $('.additional_name').val('');$('.additional_price').val('');

                    // alert("Details Saved");
                    // setTimeout(function(){// wait for 5 secs(2)
                    //        location.reload(); // then reload the page.(3)
                    //   }, 1000);
                

                }
            });
        }else {
            alert('All fields required.');
        }
        
        
    });


    $(".savereceived").on("click", function(){
        var received_price = $('.received_price').val();
        var regid = $('.regid').val();
        var randid = $('.rid').val();
        var rand_id1 = $('.rid1').val();
        var tot_chg = $('.tot_chg').val();
        
        var rec_pot = $('.rec_pot').val();
       
       
        if(randid==""){
           var rand_id = rand_id1;
        }
        else{
            var rand_id = randid;
        }
     
        if(received_price!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatereceived') }}",
                data: { regid:regid, received_price:received_price,rand_id:rand_id1}, 
                success: function( msg ) {
                   $(".rece-chg").text(received_price);
                   $(".total-bal").text(parseInt($('.total_chg').html()) - parseInt(received_price));
                   alert("Payment Received");

                    // $.ajax({
                    //     type: "GET",
                    //     url: "{{ url(config('laraadmin.adminRoute') . '/getreceived') }}",
                    //     data: { rand_id:rand_id}, 
                    //     success: function( msg ) {
                    //       console.log(msg);

                    //         $(".rece-chg").text(msg);
                    //         var amt = msg;
                    //         var total = 0;
                    //         total = parseInt(tot_chg) - parseInt(amt);
                    //         $(".total-bal").text(total);
                        

                    //     }
                    // });
                

                }
            });
        }else {
            alert('All fields required.');
        }
        
        
    });

    $(".Additionalcharges").on("click", function(){
        $("#Additionalcharges").modal('show');
        var randid = $('.rid').val();
        var rand_id1 = $('.rid1').val();
        var regid = $('.regid').val();

        if(randid==""){
           var rand_id = rand_id1;
        }
        else{
            var rand_id = randid;
        }
       
            $.ajax({
                type: "GET",
                url: "{{ url(config('laraadmin.adminRoute') . '/getadditional') }}",
                data: { rand_id:rand_id,regid:regid}, 
                success: function( msg ) {
                   // alert(msg);
                    $(".additional-body").html(msg);
                }
            });
        
    });

   

    $(".infomodal").on("click", function(){
        $("#InfoModal").modal('show');
        var randid = $('.rid').val();
        var rand_id1 = $('.rid_i').val();
        var casename = $('.casename').val();
        if(randid==""){
           var rand_id = rand_id1;
        }
        else{
            var rand_id = randid;
        }
        var regidcase = $('.regidcase').val();
        
       
            $.ajax({
                type: "GET",
                url: "{{ url(config('laraadmin.adminRoute') . '/getpaymentinfo') }}",
                data: { rand_id:rand_id,casename:casename,regidcase:regidcase}, 
                success: function( msg ) {
                   // alert(msg);
                    $(".info-body").html(msg);
                }
            });
        
    });


   

    // Examination Details 

    $(".savefollowexamination").on("click", function(){
        var examination_date = $('.fexamination_date').val();
        var dateval =$('.fexamination_date').val();
        var examination_bp = $('.fexamination_bp').val();
     	var examination_bp1 = $('.fexamination_bp1').val();
        if(examination_date==""){
            examination_date = $('.examination_date').val();
        }
        if(examination_bp==""){
            examination_bp = $('.examination_bp').val();
        }
        if(examination_bp1==""){
           examination_bp1 = $('.examination_bp1').val();
        }
      
        var examination_rand = $('.examination_rand').val();

        var regid = $('.regid').val();
        var rand = '<?php echo rand('99',9999);?>';
        var nnotes = $('.notes1').val();
         var exmdata = $('.exmdata').val();
        var notes = 'BP : '+examination_bp+' - '+examination_bp1+'\n'+nnotes+'\n';
        var notes_type ="other";
        var id = $('.nid').val();
        var already = "";
        <?php
        if($CaseExamination!=""){
            foreach($CaseExamination as $CaseCaseExamination){
                $pdate = $CaseCaseExamination->examination_date;
                $deleted = $CaseCaseExamination->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(examination_bp!="" && examination_bp1!=""){
        	  if(already==""){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                data: { id:id, notes:notes},  
                success: function( msg ) {

                    $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/examinationdetail') }}",
                    data: { regid:regid, examination_date:examination_date,examination_bp:examination_bp,examination_bp1:examination_bp1, exmdata:exmdata,examination_rand:examination_rand}, 
                    success: function( msg ) {
                                         
                    }
                    });
                    setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                }
            });
         } else { 
            var eid =  $('.examid').val();
            var examination_bp_1 = $('.examination_bp').val();
            var examination_bp1_1 = $('.examination_bp1').val();
              $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateexaminationdetail') }}",
                data: { id:eid,regid:regid, examination_date:examination_date,examination_bp:examination_bp_1,examination_bp1:examination_bp1_1,exmdata:exmdata}, 

                success: function( msg ) {
                 
                   
                   
                }
            });
              var notes1 = 'BP : '+examination_bp_1+' - '+examination_bp1_1+'\n'+nnotes+'\n';
            $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes1}, 
                    success: function( msg ) {
                      setTimeout(function(){// wait for 5 secs(2)
                       location.reload(); // then reload the page.(3)
                  }, 1000);
                      
                }
               });
           } 

            
        }else {
            alert('Fill all fields');
        }
    
    });

    $(".saveexamination").on("click", function(){
        var examination_date = $('.examination_date').val();
        var examination_bp = $('.examination_bp').val();
     	var examination_bp1 = $('.examination_bp1').val();
        var exmdata = $('.exmdata').val();
        var examination_rand = $('.examination_rand').val();
        var regid = $('.regid').val();
        var examination_rand = $('.examination_rand').val();
         var id =  $('.examid').val();
         var fexamination_date = $('.fexamination_date').val();
        //var notes = $('.allvalues').val();
        var already = "";
        <?php
        if($CaseExamination!=""){
            foreach($CaseExamination as $CaseCaseExamination){
                $pdate = $CaseCaseExamination->examination_date;
                $deleted = $CaseCaseExamination->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(examination_bp!="" && examination_bp1!="" ){

        	$.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/examinationdetail') }}",
                data: { regid:regid, examination_date:examination_date,examination_bp:examination_bp,examination_bp1:examination_bp1, exmdata:exmdata,examination_rand:examination_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                    $('.examinationtabledata tr:first').after('<tr id="'+examination_rand+'"><td>'+examination_date+'</td><td>'+examination_bp+'</td><td>'+examination_bp1+'</td><td><a href="javascript:void(0)" onclick="removeexamination('+examination_rand+')">Remove</a></td></tr>');
                 
                    $('.fexamination_bp').val(examination_bp);$('.fexamination_bp1').val(examination_bp1);$('.fexamination_date').val(examination_date);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateexaminationdetail') }}",
                data: { id:id,regid:regid, examination_date:examination_date,examination_bp:examination_bp,examination_bp1:examination_bp1,exmdata:exmdata}, 
                success: function( msg ) {
                 
                    $('.examinationtabledata tr:nth-child(2)').empty();
                    $('.examinationtabledata tr:first').after('<tr id="'+examination_rand+'"><td>'+examination_date+'</td><td>'+examination_bp+'</td><td>'+examination_bp1+'</td><td><a href="javascript:void(0)" onclick="removeexamination('+examination_rand+')">Remove</a></td></tr>');
                   
                    $('.fexamination_bp').val(examination_bp);$('.fexamination_bp1').val(examination_bp1);$('.fexamination_date').val(examination_date);
                   alert("Details Update");
                   
                }
            });
    }
    });


    // Investigation Details

    $(".saveinvestigation").on("click", function(){
        var invest_date = $('.invest_date').val();
       	var hb = $('.hb').val();
     	var t3 = $('.t3').val();
        var t4 = $('.t4').val();
        var tsh = $('.tsh_i').val();
        var totalchol = $('.totalchol').val();
        var hdichol = $('.hdichol').val();
        var invest_rand = $('.invest_rand').val();
        var regid = $('.regid').val();
        var id =  $('.invid').val();
        var already = "";
        <?php
        if($CaseInvestigation!=""){
            foreach($CaseInvestigation as $Investigation){
                $pdate = $Investigation->invest_date;
                $deleted = $Investigation->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      if(already ==""){
        if(invest_rand!="" && hb!=""){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/investigationdetail') }}",
                data: { regid:regid, invest_date:invest_date,hb:hb,t3:t3, t4:t4,tsh:tsh,totalchol:totalchol,hdichol:hdichol,invest_rand:invest_rand}, 
                success: function( msg ) {
                    if(msg=="1"){
                    $('.investigationtabledata tr:first').after('<tr id="'+invest_rand+'"><td>'+invest_date+'</td><td>'+hb+'</td><td>'+t3+'</td><td>'+t4+'</td><td>'+tsh+'</td><td>'+totalchol+'</td><td>'+hdichol+'</td><td><a href="javascript:void(0)" onclick="removeinvestigation('+invest_rand+')">Remove</a></td></tr>');
                    
                    $('.fhb').val(hb);$('.ft3').val(t3);$('.ft4').val(t4);$('.ftsh').val(tsh);$('.ftotalchol').val(totalchol);$('.fhdichol').val(hdichol);$('.finvest_date').val(invest_date);
                    alert("Details Save");
                   }

                    else {
                        alert("Already added.");
                    }
                  
                }
            });
        }else {
            alert('All fields required.');
        }
        }
        else{
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateinvestigationdetail') }}",
                data: { id:id,regid:regid,hb:hb,t3:t3, t4:t4,tsh:tsh,totalchol:totalchol,hdichol:hdichol}, 
                success: function( msg ) {
                    $('.investigationtabledata tr:nth-child(2)').empty();
                    $('.investigationtabledata tr:first').after('<tr id="'+invest_rand+'"><td>'+invest_date+'</td><td>'+hb+'</td><td>'+t3+'</td><td>'+t4+'</td><td>'+tsh+'</td><td>'+totalchol+'</td><td>'+hdichol+'</td><td><a href="javascript:void(0)" onclick="removeinvestigation('+invest_rand+')">Remove</a></td></tr>');
                    
                    $('.fhb').val(hb);$('.ft3').val(t3);$('.ft4').val(t4);$('.ftsh').val(tsh);$('.ftotalchol').val(totalchol);$('.fhdichol').val(hdichol);$('.finvest_date').val(invest_date);

                  alert("Details Update");
                }
            });
        }
    });

    $(".savefollowinvestigation").on("click", function(){
        
        var invest_date = $('.finvest_date').val();
        if(invest_date==""){
            invest_date = $('.invest_date').val();
        }
        var dateval = $('.finvest_date').val();
        var hb = $('.fhb').val();
        if(hb==""){
            hb = $('.hb').val();
        }
     	var t3 = $('.ft3').val();
        if(t3==""){
            t3 = $('.t3').val();
        }
       
        var t4 = $('.ft4').val();
        if(t4==""){
            t4 = $('.t4').val();
        }
        var tsh = $('.ftsh').val();   
        if(tsh==""){
            tsh = $('.tsh_i').val(); 
        }
        var totalchol = $('.ftotalchol').val();
        if(totalchol==""){
            totalchol = $('.totalchol').val();
        }
        var hdichol = $('.fhdichol').val();
        if(hdichol==""){
            hdichol = $('.hdichol').val();
        }
        var invest_rand = $('.invest_rand').val();
        var regid = $('.regid').val();
        var rand = '<?php echo rand('99',9999);?>';
        var nnotes = $('.notes1').val();
        
        if(hb!=""){
            var notes = 'HB : '+hb+'\n'+nnotes+'\n';
        }
        else{
            var notes = nnotes+'\n';
        }
         if(t3!=""){
        var notes = 'T3 : '+t3+'\n'+notes+'\n';
        }
        else{
            var notes = notes+'\n';
        }
        if(t4!=""){
            var notes = 'T4 : '+t4+'\n'+notes+'\n';
        }
        else{
            var notes = notes+'\n';
        }
        if(tsh!=""){
            var notes = 'TSH : '+tsh+'\n'+notes+'\n';
        }
        else{
            var notes = notes+'\n';
        }
        if(totalchol!=""){
            var notes = 'TotalChol : '+totalchol+'\n'+notes+'\n';
        }
        else{
            var notes = notes+'\n';
        }
         if(hdichol!=""){
            var notes = 'HdiChol : '+hdichol+'\n'+notes+'\n';
        }
        else{
            var notes = notes+'\n';
        }
      //  var notes = 'HB : '+hb+', T3 : '+t3+', T4 : '+t4+', TSH : '+tsh+', TotalChol : '+totalchol+', HdiChol : '+hdichol+'\n'+nnotes+'\n';
        var notes_type = "other";
        var id = $('.nid').val();
        var already = "";
        <?php
        if($CaseInvestigation!=""){
            foreach($CaseInvestigation as $Investigation){
                $pdate = $Investigation->invest_date;
                $deleted = $Investigation->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(invest_date!="" && hb!=""){
                if(already ==""){

                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes}, 
                        success: function( msg ) {

                            $.ajax({
                                type: "POST",
                                url: "{{ url(config('laraadmin.adminRoute') . '/investigationdetail') }}",
                                data: { regid:regid, invest_date:invest_date,hb:hb,t3:t3, t4:t4,tsh:tsh,totalchol:totalchol,hdichol:hdichol,invest_rand:invest_rand}, 
                                success: function( msg ) {
                                  
                                }
                            });
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });

                }
                else{
                    var invid =  $('.invid').val();
                    var hb_1 = $('.hb').val();
                    var t3_1 = $('.t3').val();
                    var t4_1 = $('.t4').val();
                    var tsh_1 = $('.tsh_i').val();
                    var totalchol_1 = $('.totalchol').val();
                    var hdichol_1 = $('.hdichol').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updateinvestigationdetail') }}",
                        data: { id:invid,regid:regid,hb:hb_1,t3:t3_1, t4:t4_1,tsh:tsh_1,totalchol:totalchol_1,hdichol:hdichol_1}, 
                        success: function( msg ) {
                          
                        }
                    });
                     if(hb_1!=""){
                            var notes1 = 'HB : '+hb_1+'\n'+nnotes+'\n';
                        }
                        else{
                            var notes1 = nnotes+'\n';
                        }
                         if(t3_1!=""){
                        var notes1 = 'T3 : '+t3_1+'\n'+notes+'\n';
                        }
                        else{
                            var notes1 = notes+'\n';
                        }
                        if(t4_1!=""){
                            var notes1 = 'T4 : '+t4_1+'\n'+notes+'\n';
                        }
                        else{
                            var notes1 = notes+'\n';
                        }
                        if(tsh_1!=""){
                            var notes1 = 'TSH : '+tsh_1+'\n'+notes+'\n';
                        }
                        else{
                            var notes1 = notes+'\n';
                        }
                        if(totalchol_1!=""){
                            var notes1 = 'TotalChol : '+totalchol_1+'\n'+notes+'\n';
                        }
                        else{
                            var notes1 = notes+'\n';
                        }
                         if(hdichol_1!=""){
                            var notes1 = 'HdiChol : '+hdichol_1+'\n'+notes+'\n';
                        }
                        else{
                            var notes1 = notes+'\n';
                        }
                //var notes1 = 'HB : '+hb_1+', T3 : '+t3_1+', T4 : '+t4_1+', TSH : '+tsh_1+', TotalChol : '+totalchol_1+', HdiChol : '+hdichol_1+'\n'+nnotes+'\n';

                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
                }
        	 
           
        }else {
            alert('Fill all fields'); 
        }
    
    });

    // Homeo Detail 

    $(".savehomeo").on("click", function(){
        var diagnosis = $('.diagnosis').val();
        var complaint_intesity_h = $('.complaint_intesity_h').val();
        //var medicine_price = $('.medicine_price').val();
      
        var investigation = $('.investigation').val();
        var case_taken = $('.case_taken').val();
        var medicine_price = "0";
      //  var criteria = $('.criteria').val();
        var valmed= new Array();
       var values = new Array();
       var select = new Array();
       var time = new Array();
        $.each($("input[class='criteria']:checked"), function() {
            
          values.push($(this).val());
        }); 

         $('select[name^=medicine_for]').each(function(){
            select.push($(this).val()); 
        });  

        $('select[name^=medicine_time]').each(function(){
            time.push($(this).val()); 
        });  
        
        $('input[name^=medication_taking]').each(function(){
            valmed.push($(this).val()); 
        }); 
        var medicine_time =time;
        var medicine_for = select;
        var medication_taking = valmed;
       
        var criteria =  values;

        var regid = $('.regid').val();
      //  alert(regid);
        var homeo_date = $('.homeo_date').val();
     	var total_charges = $('.consultation_fee').val();
          var id = $('.homeoid').val();
        var already = "";
        <?php
        if($homeo!=""){
           ?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } 

         ?>
     if(already =="" ){
     
        if(diagnosis!=""){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/homeodetail') }}",
                data: { regid:regid, diagnosis:diagnosis,complaint_intesity:complaint_intesity_h,medication_taking:medication_taking, medicine_price:medicine_price,medicine_time:medicine_time,medicine_for:medicine_for,investigation:investigation,case_taken:case_taken, criteria:criteria,total_charges:total_charges}, 
                success: function( msg ) {
                   $('.homeo_date1').val(homeo_date);$('.diagnosis1').val(diagnosis);$('.complaint_intesity1').val(complaint_intesity_h);$('.medication_taking1').val(medication_taking);$('.medicine_price1').val(medicine_price);$('.medicine_time1').val(medicine_time);$('.medicine_for1').val(medicine_for);$('.investigation1').val(investigation);$('.case_taken1').val(case_taken);
                  //  $('.diagnosis').val('');$('.complaint_intesity_h').val('');$('.medication_taking').val('');$('.medicine_price').val('');$('.medicine_time').val('');$('.medicine_for').val('');$('.investigation').val('');$('.case_taken').val('');$('.criteria').val('');
                  alert("Details Save");
                }
            });
        }else {
           
            alert('All fields required.');
        }
    }
    else{
    
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatehomeodetail') }}",
                data: { id:id, regid:regid, diagnosis:diagnosis,complaint_intesity:complaint_intesity_h,medication_taking:medication_taking, medicine_price:medicine_price,medicine_time:medicine_time,medicine_for:medicine_for,investigation:investigation,case_taken:case_taken, criteria:criteria,total_charges:total_charges}, 
                success: function( msg ) {
                   $('.homeo_date1').val(homeo_date);$('.diagnosis1').val(diagnosis);$('.complaint_intesity1').val(complaint_intesity_h);$('.medication_taking1').val(medication_taking);$('.medicine_price1').val(medicine_price);$('.medicine_time1').val(medicine_time);$('.medicine_for1').val(medicine_for);$('.investigation1').val(investigation);$('.case_taken1').val(case_taken);
                  //  $('.diagnosis').val('');$('.complaint_intesity_h').val('');$('.medication_taking').val('');$('.medicine_price').val('');$('.medicine_time').val('');$('.medicine_for').val('');$('.investigation').val('');$('.case_taken').val('');$('.criteria').val('');
                  alert("Details Update");
                }
            });
    }
    });

    $(".savefollowhomeo").on("click", function(){
        var diagnosis1 = $('.diagnosis1').val();
        var complaint_intesity1 = $('.complaint_intesity1').val();
     	//var medication_taking1 = $('.medication_taking').val();
        var medicine_price1 = $('.medicine_price1').val();
        //var medicine_time1 = $('.medicine_time1').val();
       // var medicine_for1 = $('.medicine_for1').val();
        var investigation1 = $('.investigation1').val();
        var case_taken1 = $('.case_taken1').val();
        var homeo_date1 = $('.homeo_date1').val();

         var valmed= new Array();
       var values = new Array();
       var select = new Array();
       var time = new Array();
        $.each($("input[class='criteria']:checked"), function() {
            
          values.push($(this).val());
        }); 

         $('select[name^=medicine_for]').each(function(){
            select.push($(this).val()); 
        });  

        $('select[name^=medicine_time]').each(function(){
            time.push($(this).val()); 
        });  
        
        $('input[name^=medication_taking]').each(function(){
            valmed.push($(this).val()); 
        }); 
        var medicine_time1 =time;
        var medicine_for1 = select;
        var medication_taking1 = valmed;

        if(diagnosis1!=""){
            $('.followupdataentery4').append('<label for="mobile" style="padding-left: 10px;">Follow Up on'+homeo_date1+'</label><div class="form-group col-md-12" style="border: 3px solid #cac8c8;background: #fff; padding: 0;"><div class="form-group col-md-12" style="margin-bottom: 0; padding-bottom: 0;"><label for="mobile" style="margin-bottom: 0;">Diagnosis</label><p id="diagnosistest">'+diagnosis1+'</p> </div><div class="form-group col-md-12" style="margin-bottom: 0; padding-bottom: 0;"><label for="mobile" style="margin-bottom: 0;">Complaint Intensity</label><p id="domplainttest">'+complaint_intesity1+'</p></div><div class="form-group col-md-12" style="margin-bottom: 0; padding-bottom: 0;"><label for="mobile" style="margin-bottom: 0;">Medication Taking</label><p id="medicationtest">'+medication_taking1+'</p></div><div class="form-group col-md-12" style="margin-bottom: 0; padding-bottom: 0;"><label for="mobile" style="margin-bottom: 0;">Investigation</label><p id="investigationtest">'+investigation1+'</p></div></div>');


            $('.diagnosis1').val('');$('.complaint_intesity1').val('');$('.medication_taking1').val('');$('.medicine_price1').val('');$('.medicine_time1').val('');$('.medicine_for1').val('');$('.investigation1').val('');$('.case_taken1').val('');
                setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                           }, 1000); 
        }else {
            alert('All fields required.'); 
        }
    });

    $('#notes').on('keypress',function(e){
    	if(e.which == 13) {
    	var notes = $('.notes').val();
    	var regid = $('.regid').val();
    	if(notes!=""){
    		$.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/savenotes') }}",
                data: { regid:regid, notes:notes}, 
                success: function( msg ) {
                
                }
            });
    	}
    }
    });

   

    $('.rxtype').on('change', function() {
            var rxtype = this.value;

            var rand_id = $('.prand').val();

            var post_type ="";
            if(rxtype=="Courier"){
                 post_type ="Courier";
            }
            else if(rxtype=="Pickup"){
                 post_type ="Pickup";
            }
            // else{
            //       post_type ="Clinic";
            // }
             
                if(rand_id!=""){
                    $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/couriermedicine/courier') }}",
                    data: { rand_id:rand_id,post_type:post_type}, 
                    success: function( msg ) {
                       
                            //alert("Medicine added for courier");
                           
                        }
                    });
                }
                else{
                    
                    alert("No Rx Added Today");
                }
          
            

    });


    // Semen Analysis
    $(".savesemen").on("click", function(){
        var semen_date = $('.semen_date').val();
        var semen_analysis = $('.semen_analysis_s').val();
        var semen_rand = $('.semen_rand').val();
        var regid = $('.regid').val();
	    var id =  $('.semenid').val();
        var already = "";
        <?php
        if($Semenanalysis!=""){
            foreach($Semenanalysis as $Semenanalysisval){
                $pdate = $Semenanalysisval->dateval;
                $deleted = $Semenanalysisval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(semen_analysis!=""){

        	$.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/semenanalysis') }}",
                data: { regid:regid, semen_date:semen_date,semen_analysis:semen_analysis,semen_rand:semen_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.semen_analysis1').val(semen_analysis);$('.semen_date1').val(semen_date);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatesemenanalysis') }}",
                data: { id:id,regid:regid, semen_date:semen_date,semen_analysis:semen_analysis}, 
                success: function( msg ) {
                 
                   $('.semen_analysis1').val(semen_analysis);$('.semen_date1').val(semen_date);
                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowsemen").on("click", function(){
       	var semen_analysis = $('.semen_analysis1').val();
        if(semen_analysis==""){
            semen_analysis = $('.semen_analysis_s').val();
        }
        var semen_date = $('.semen_date1').val();
        if(semen_date==""){
            semen_date = $('.semen_date').val();
        }
        var regid = $('.regid').val();
        var semen_rand = $('.semen_rand').val();
        var nnotes = $('.nnotes').val();
        
        var notes = 'Semen Analysis : '+semen_analysis+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Semenanalysis!=""){
            foreach($Semenanalysis as $Semenanalysisval){
                $pdate = $Semenanalysisval->dateval;
                $deleted = $Semenanalysisval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(semen_analysis!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/semenanalysis') }}",
                            data: { regid:regid, semen_date:semen_date,semen_analysis:semen_analysis,semen_rand:semen_rand},  
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var sid =  $('.semenid').val();
                var semen_analysis_1 = $('.semen_analysis_s').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatesemenanalysis') }}",
                        data: { id:sid,regid:regid, semen_analysis:semen_analysis_1}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'Semen Analysis : '+semen_analysis_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
        	 
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

     // Serological Reports
    $(".saveserological").on("click", function(){
        var serological_date = $('.serological_date').val();
        var serological_report = $('.serological_report_s').val();
        var serological_rand = $('.serological_rand').val();
        var regid = $('.regid').val();
	    var id =  $('.serologyid').val();
        var already = "";
        <?php
        if($Serologicalreport!=""){
            foreach($Serologicalreport as $Serologicalreportval){
                $pdate = $Serologicalreportval->dateval;
                $deleted = $Serologicalreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(serological_report!=""){

        	$.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/serologicalreports') }}",
                data: { regid:regid, serological_date:serological_date,serological_report:serological_report,serological_rand:serological_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.serological_report1').val(serological_report);$('.serological_date1').val(serological_date);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateserologicalreports') }}",
                data: { id:id,regid:regid, serological_date:serological_date,serological_report:serological_report}, 
                success: function( msg ) {
                 
                   $('.serological_report1').val(serological_report);$('.serological_date1').val(serological_date);
                   alert("Details Update");
                   
                }
            });
    }
    });

     $(".savefollowserological").on("click", function(){
       	var serological_report = $('.serological_report1').val();
        if(serological_report==""){
            serological_report = $('.serological_report_s').val();
        }
        var serological_date = $('.serological_date1').val();
        if(serological_date==""){
            serological_date = $('.serological_date').val();
        }
        var regid = $('.regid').val();
        var serological_rand = $('.serological_rand').val();
        var nnotes = $('.nnotes').val();
        
        var notes = 'Serological Reports : '+serological_report+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Serologicalreport!=""){
            foreach($Serologicalreport as $Serologicalreportval){
                $pdate = $Serologicalreportval->dateval;
                $deleted = $Serologicalreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(serological_report!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/serologicalreports') }}",
                			data: { regid:regid, serological_date:serological_date,serological_report:serological_report,serological_rand:serological_rand},  
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var serid =  $('.serologyid').val();
                var serological_report_1 = $('.serological_report_s').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updateserologicalreports') }}",
                        data: { id:serid,regid:regid, serological_report:serological_report_1}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'Serological Reports : '+serological_report_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
        	 
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

   // Diabetes Report

   $(".savediabetes").on("click", function(){
        var diabetes_date = $('.diabetes_date').val();
        var diabetes_rand = $('.diabetes_rand').val();
        var blood_fasting = $('.blood_fasting').val();
        var blood_prandial = $('.blood_prandial').val();
        var blood_random = $('.blood_random').val();
        var urine_fasting = $('.urine_fasting').val();
		var urine_prandial = $('.urine_prandial').val();
        var urine_random = $('.urine_random').val();
        var glu_test = $('.glu_test').val();
        var glycosylated_hb = $('.glycosylated_hb').val();
        var regid = $('.regid').val();
	    var id =  $('.diabetes_id').val();
        var already = "";
        <?php
        if($Diabetesreport!=""){
            foreach($Diabetesreport as $Diabetesreportval){
                $pdate = $Diabetesreportval->dateval;
                $deleted = $Diabetesreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(blood_fasting!=""){

        	$.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/diabetesdetail') }}",
                data: { regid:regid, diabetes_date:diabetes_date,diabetes_rand:diabetes_rand,blood_fasting:blood_fasting,blood_prandial:blood_prandial,blood_random:blood_random,urine_fasting:urine_fasting,urine_prandial:urine_prandial,urine_random:urine_random,glu_test:glu_test,glycosylated_hb:glycosylated_hb}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.diabetes_date1').val(diabetes_date);$('.blood_fasting1').val(blood_fasting);$('.blood_prandial1').val(blood_prandial);$('.blood_random1').val(blood_random);$('.urine_fasting1').val(urine_fasting);$('.urine_prandial1').val(urine_prandial);$('.urine_random1').val(urine_random);$('.glu_test1').val(glu_test);$('.glycosylated_hb1').val(glycosylated_hb);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatediabetesdetail') }}",
                data: { id:id,regid:regid, diabetes_date:diabetes_date,blood_fasting:blood_fasting,blood_prandial:blood_prandial,blood_random:blood_random,urine_fasting:urine_fasting,urine_prandial:urine_prandial,urine_random:urine_random,glu_test:glu_test,glycosylated_hb:glycosylated_hb}, 
                success: function( msg ) {
                 
                   $('.diabetes_date1').val(diabetes_date);$('.blood_fasting1').val(blood_fasting);$('.blood_prandial1').val(blood_prandial);$('.blood_random1').val(blood_random);$('.urine_fasting1').val(urine_fasting);$('.urine_prandial1').val(urine_prandial);$('.urine_random1').val(urine_random);$('.glu_test1').val(glu_test);$('.glycosylated_hb1').val(glycosylated_hb);
                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowdiabetes").on("click", function(){
       	var diabetes_date = $('.diabetes_date1').val();
        if(diabetes_date==""){
            diabetes_date = $('.diabetes_date').val();
        }
        var blood_fasting = $('.blood_fasting1').val();
        if(blood_fasting==""){
            blood_fasting = $('.blood_fasting').val();
        }
        var blood_prandial = $('.blood_prandial1').val();
        if(blood_prandial==""){
            blood_prandial = $('.blood_prandial').val();
        }
        var blood_random = $('.blood_random1').val();
        if(blood_random==""){
            blood_random = $('.blood_random').val();
        }
        var urine_fasting = $('.urine_fasting1').val();
        if(urine_fasting==""){
            urine_fasting = $('.urine_fasting').val();
        }
        var urine_prandial = $('.urine_prandial1').val();
        if(urine_prandial==""){
            urine_prandial = $('.urine_prandial').val();
        }
        var urine_random = $('.urine_random1').val();
        if(urine_random==""){
            urine_random = $('.urine_random').val();
        }
        var glu_test = $('.glu_test1').val();
        if(glu_test==""){
            glu_test = $('.glu_test').val();
        }
        var glycosylated_hb = $('.glycosylated_hb1').val();
        if(glycosylated_hb==""){
            glycosylated_hb = $('.glycosylated_hb').val();
        }
        var regid = $('.regid').val();
        var diabetes_rand = $('.diabetes_rand').val();
        var nnotes = $('.nnotes').val();

        if(blood_fasting!=""){
            var notes_n = 'Blood Fasting : '+blood_fasting+'\n'+nnotes+'\n';
        }
        else{
            var notes_n = nnotes+'\n';
        }
        if(blood_prandial!=""){
            var notes_n = 'Blood Prandial '+blood_prandial+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(blood_random!=""){
            var notes_n = 'Blood Random '+blood_random+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(urine_fasting!=""){
            var notes_n = 'Urine Fasting :  '+urine_fasting+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(urine_prandial!=""){
            var notes_n = 'Urine Prandial :  '+urine_prandial+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
         if(urine_random!=""){
            var notes_n = ' Urine Random :  '+urine_random+'\n'+notes_n+'\n';
        }
        else{
            var notes = notes+'\n';
        }
         if(glu_test!=""){
            var notes_n = ' Glu Test :  '+glu_test+'\n'+notes_n+'\n';
        }
        
        else{
            var notes_n = notes_n+'\n';
        }
         if(glycosylated_hb!=""){
            var notes_n = 'Glycosylated HB :  '+glycosylated_hb+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        var heading = 'Diabetes Profile:\n';
        var notes = heading+' '+notes_n;
       // var notes = 'Diabetes Profile:\n Blood Examination : '+blood_fasting+' - '+blood_prandial+' - '+blood_random+'\n Urine Examination : '+urine_fasting+' - '+urine_prandial+' - '+urine_random+'\n'+glu_test+'\n'+glycosylated_hb+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Diabetesreport!=""){
            foreach($Diabetesreport as $Diabetesreportval){
                $pdate = $Diabetesreportval->dateval;
                $deleted = $Diabetesreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(blood_fasting!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/diabetesdetail') }}",
                			data: { regid:regid, diabetes_date:diabetes_date,diabetes_rand:diabetes_rand,blood_fasting:blood_fasting,blood_prandial:blood_prandial,blood_random:blood_random,urine_fasting:urine_fasting,urine_prandial:urine_prandial,urine_random:urine_random,glu_test:glu_test,glycosylated_hb:glycosylated_hb},   
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var did =  $('.diabetes_id').val();
                var blood_fasting_1 = $('.blood_fasting1').val();
                var blood_prandial_1 = $('.blood_prandial1').val();
                var blood_random_1 = $('.blood_random1').val();
                var urine_fasting_1 = $('.urine_fasting1').val();
                var urine_prandial_1 = $('.urine_prandial1').val();
                var urine_random_1 = $('.urine_random1').val();
                 var glu_test_1 = $('.glu_test1').val();
                 var glycosylated_hb_1 = $('.glycosylated_hb1').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatediabetesdetail') }}",
                        data: { id:did, regid:regid, blood_fasting:blood_fasting_1,blood_prandial:blood_prandial_1,blood_random:blood_random_1,urine_fasting:urine_fasting_1,urine_prandial:urine_prandial_1,urine_random:urine_random_1,glu_test:glu_test_1,glycosylated_hb:glycosylated_hb_1},
                        success: function( msg ) {
                           

                        }
                    });
                    var heading = 'Diabetes Profile:\n';
                     if(blood_fasting_1!=""){
                        var notes_1 = ' Blood Fasting : '+blood_fasting_1+'\n'+nnotes+'\n';
                    }
                    else{
                        var notes_1 = nnotes+'\n';
                    }
                    if(blood_prandial!=""){
                        var notes_1 = ' Blood Prandial '+blood_prandial_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(blood_random!=""){
                        var notes_1 = 'Blood Random '+blood_random_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(urine_fasting!=""){
                        var notes_1 = 'Urine Fasting :  '+urine_fasting_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(urine_prandial!=""){
                        var notes_1 = 'Urine Prandial :  '+urine_prandial_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                     if(urine_random!=""){
                        var notes_1 = ' Urine Random :  '+urine_random_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes = notes_1+'\n';
                    }
                     if(glu_test!=""){
                        var notes_1 = ' Glu Test :  '+glu_test_1+'\n'+notes_1+'\n';
                    }
                    
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                     if(glycosylated_hb!=""){
                        var notes_1 = ' Glycosylated HB :  '+glycosylated_hb_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    var notes1 = heading+' '+notes_1;
                    //var notes1 = 'Diabetes Profile:\n Blood Examination : '+blood_fasting_1+' - '+blood_prandial_1+' - '+blood_random_1+'\n Urine Examination : '+urine_fasting_1+' - '+urine_prandial_1+' - '+urine_random_1+'\n'+glu_test_1+'\n'+glycosylated_hb_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
        	 
            
        }else {
            alert('Fill all fields.'); 
        }
      
   
    });


    // Cardiac Report

    $(".savecardiac").on("click", function(){
        var cardiac_date = $('.cardiac_date').val();
        var homocysteine = $('.homocysteine').val();
        var ecg = $('.ecg').val();
        var decho_2 = $('.decho').val();
        var cardiac_rand = $('.cardiac_rand').val();
        var regid = $('.regid').val();
        var id =  $('.cardiac_id').val();
        var already = "";
        <?php
        if($Cardiacreport!=""){
            foreach($Cardiacreport as $Cardiacreportval){
                $pdate = $Cardiacreportval->dateval;
                $deleted = $Cardiacreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(homocysteine!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/cardiacreport') }}",
                data: { regid:regid, cardiac_date:cardiac_date,homocysteine:homocysteine,ecg:ecg,decho:decho_2,cardiac_rand:cardiac_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.cardiac_date1').val(cardiac_date);$('.homocysteine1').val(homocysteine);$('.ecg1').val(ecg);$('.decho1').val(decho_2);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatecardiacreport') }}",
                data: { id:id, regid:regid, cardiac_date:cardiac_date,homocysteine:homocysteine,ecg:ecg,decho:decho_2},  
                success: function( msg ) {
                 
                    $('.cardiac_date1').val(cardiac_date);$('.homocysteine1').val(homocysteine);$('.ecg1').val(ecg);$('.decho1').val(decho_2);
                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowcardiac").on("click", function(){
        var homocysteine = $('.homocysteine1').val();
        if(homocysteine==""){
            homocysteine = $('.homocysteine').val();
        }
         var ecg = $('.ecg1').val();
        if(ecg==""){
            ecg = $('.ecg').val();
        }
         var decho_2 = $('.decho1').val();
        if(decho_2==""){
            decho_2 = $('.decho').val();
        }
        var cardiac_date = $('.cardiac_date1').val();
        if(cardiac_date==""){
            cardiac_date = $('.cardiac_date').val();
        }
        var regid = $('.regid').val();
        var cardiac_rand = $('.cardiac_rand').val();
        var nnotes = $('.nnotes').val();

        var heading ="Cardiac Profile:\n";

        if(homocysteine!=""){
            var notes_n = 'Homocysteine : '+homocysteine+'\n'+nnotes+'\n';
        }
        else{
            var notes_n = nnotes+'\n';
        }
        if(ecg!=""){
            var notes_n = 'ECG : '+ecg+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(decho_2!=""){
            var notes_n = '2DECHO : '+decho_2+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        var notes = heading+' '+notes_n;
        
        //var notes = 'Cardiac Profile : Homocysteine - '+homocysteine+'\n ECG -'+ecg+'\n 2DECHO - '+decho_2+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Cardiacreport!=""){
            foreach($Cardiacreport as $Cardiacreportval){
                $pdate = $Cardiacreportval->dateval;
                $deleted = $Cardiacreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(homocysteine!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/cardiacreport') }}",
                            data: { regid:regid, cardiac_date:cardiac_date,homocysteine:homocysteine,ecg:ecg,decho:decho_2,cardiac_rand:cardiac_rand},   
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var carid =  $('.cardiac_id').val();
                var homocysteine_1 = $('.homocysteine').val();
                var ecg_1 = $('.ecg').val();
                var decho_2_1 = $('.decho').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatecardiacreport') }}",
                        data: { id:carid, regid:regid, homocysteine:homocysteine_1,ecg:ecg_1,decho:decho_2_1}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var heading ="Cardiac Profile:\n";

                        if(homocysteine_1!=""){
                            var notes_1 = 'Homocysteine : '+homocysteine_1+'\n'+nnotes+'\n';
                        }
                        else{
                            var notes_1 = nnotes+'\n';
                        }
                        if(ecg_1!=""){
                            var notes_1 = 'ECG : '+ecg_1+'\n'+notes_1+'\n';
                        }
                        else{
                            var notes_1 = notes_1+'\n';
                        }
                        if(decho_2_1!=""){
                            var notes_1 = '2DECHO : '+decho_2_1+'\n'+notes_1+'\n';
                        }
                        else{
                            var notes_1 = notes_1+'\n';
                        } 
                        var notes1 = heading+' '+notes_1;
                   // var notes1 = 'Cardiac Profile : Homocysteine - '+homocysteine_1+'\n ECG -'+ecg_1+'\n 2DECHO - '+decho_2_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });


    // Lipid Profile

    $(".savelipid").on("click", function(){
        var lipid_date = $('.lipid_date').val();
        var lipid_rand = $('.lipid_rand').val();
        var total_cholesterol = $('.total_cholesterol').val();
        var triglycerides = $('.triglycerides').val();
        var hdl_cholesterol = $('.hdl_cholesterol').val();
        var ldl_cholesterol = $('.ldl_cholesterol').val();
        var vldl = $('.vldl').val();
        var hdl_ratio = $('.hdl_ratio').val();
        var ldl_hdl = $('.ldl_hdl').val();
        var lipoprotein = $('.lipoprotein').val();
        var apolipoprotein_a = $('.apolipoprotein_a').val();
        var apolipoprotein_b = $('.apolipoprotein_b').val();
        var regid = $('.regid').val();
        var id =  $('.lipid_id').val();
        var already = "";
        <?php
        if($Lipidreport!=""){
            foreach($Lipidreport as $Lipidreportval){
                $pdate = $Lipidreportval->dateval;
                $deleted = $Lipidreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(total_cholesterol!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/lipiddetail') }}",
                data: { regid:regid, lipid_date:lipid_date,lipid_rand:lipid_rand,total_cholesterol:total_cholesterol,triglycerides:triglycerides,hdl_cholesterol:hdl_cholesterol,ldl_cholesterol:ldl_cholesterol,vldl:vldl,hdl_ratio:hdl_ratio,ldl_hdl:ldl_hdl,lipoprotein:lipoprotein,apolipoprotein_a:apolipoprotein_a,apolipoprotein_b:apolipoprotein_b}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.lipid_date1').val(lipid_date);$('.total_cholesterol1').val(total_cholesterol);$('.triglycerides1').val(triglycerides);$('.hdl_cholesterol1').val(hdl_cholesterol);$('.ldl_cholesterol1').val(ldl_cholesterol);$('.vldl1').val(vldl);$('.hdl_ratio1').val(hdl_ratio);$('.ldl_hdl1').val(ldl_hdl);$('.lipoprotein1').val(lipoprotein);$('.apolipoprotein_a1').val(apolipoprotein_a);$('.apolipoprotein_b1').val(apolipoprotein_b);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatelipiddetail') }}",
                data: { id:id , regid:regid, lipid_date:lipid_date,lipid_rand:lipid_rand,total_cholesterol:total_cholesterol,triglycerides:triglycerides,hdl_cholesterol:hdl_cholesterol,ldl_cholesterol:ldl_cholesterol,vldl:vldl,hdl_ratio:hdl_ratio,ldl_hdl:ldl_hdl,lipoprotein:lipoprotein,apolipoprotein_a:apolipoprotein_a,apolipoprotein_b:apolipoprotein_b},  
                success: function( msg ) {
                 
                   $('.lipid_date1').val(lipid_date);$('.total_cholesterol1').val(total_cholesterol);$('.triglycerides1').val(triglycerides);$('.hdl_cholesterol1').val(hdl_cholesterol);$('.ldl_cholesterol1').val(ldl_cholesterol);$('.vldl1').val(vldl);$('.hdl_ratio1').val(hdl_ratio);$('.ldl_hdl1').val(ldl_hdl);$('.lipoprotein1').val(lipoprotein);$('.apolipoprotein_a1').val(apolipoprotein_a);$('.apolipoprotein_b1').val(apolipoprotein_b);
                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowlipid").on("click", function(){
        var lipid_date = $('.lipid_date1').val();
        if(lipid_date==""){
            lipid_date = $('.lipid_date').val();
        }
        var total_cholesterol = $('.total_cholesterol1').val();
        if(total_cholesterol==""){
            total_cholesterol = $('.total_cholesterol').val();
        }
        var triglycerides = $('.triglycerides1').val();
        if(triglycerides==""){
            triglycerides = $('.triglycerides').val();
        }
        var hdl_cholesterol = $('.hdl_cholesterol1').val();
        if(hdl_cholesterol==""){
            hdl_cholesterol = $('.hdl_cholesterol').val();
        }
        var ldl_cholesterol = $('.ldl_cholesterol1').val();
        if(ldl_cholesterol==""){
            ldl_cholesterol = $('.ldl_cholesterol').val();
        }
        var vldl = $('.vldl1').val();
        if(vldl==""){
            vldl = $('.vldl').val();
        }
        var hdl_ratio = $('.hdl_ratio1').val();
        if(hdl_ratio==""){
            hdl_ratio = $('.hdl_ratio').val();
        }
        var ldl_hdl = $('.ldl_hdl1').val();
        if(ldl_hdl==""){
            ldl_hdl = $('.ldl_hdl').val();
        }
        var lipoprotein = $('.lipoprotein1').val();
        if(lipoprotein==""){
            lipoprotein = $('.lipoprotein').val();
        }
        var apolipoprotein_a = $('.apolipoprotein_a1').val();
        if(apolipoprotein_a==""){
            apolipoprotein_a = $('.apolipoprotein_a').val();
        }
        var apolipoprotein_b = $('.apolipoprotein_b1').val();
        if(apolipoprotein_b==""){
            apolipoprotein_b = $('.apolipoprotein_b').val();
        }
        var regid = $('.regid').val();
        var lipid_rand = $('.lipid_rand').val();
        var nnotes = $('.nnotes').val();

        var heading ="Lipid Profile:\n";

        if(total_cholesterol!=""){
            var notes_n = 'Total Cholesterol : '+total_cholesterol+'\n'+nnotes+'\n';
        }
        else{
            var notes_n = nnotes+'\n';
        }
        if(triglycerides!=""){
            var notes_n = 'Triglycerides : '+tiglycerides+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(hdl_cholesterol!=""){
            var notes_n = 'HDL Cholesterol : '+hdl_cholesterol+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(ldl_cholesterol!=""){
            var notes_n = 'LDL Cholesterol : '+ldl_cholesterol+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(vldl!=""){
            var notes_n = 'VLDL : '+vldl+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
         if(hdl_ratio!=""){
            var notes_n = 'Chol/HDL ratio : '+hdl_ratio+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(ldl_hdl!=""){
            var notes_n = 'LDL/HDL : '+ldl_hdl+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(lipoprotein!=""){
            var notes_n = 'Lipoprotein : '+lipoprotein+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(apolipoprotein_a!=""){
            var notes_n = 'Apolipoprotein-A : '+apolipoprotein_a+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(apolipoprotein_b!=""){
            var notes_n = 'Apolipoprotein-B : '+apolipoprotein_b+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        var notes = heading+' '+notes_n;


        
       // var notes = 'Lipid Profile:\n Total Cholesterol : '+total_cholesterol+' , Triglycerides: '+triglycerides+' , HDL Cholesterol:  '+hdl_cholesterol+' , LDL Cholesterol: '+ldl_cholesterol+' , VLDL: '+vldl+' , Chol/HDL ratio: '+hdl_ratio+' , LDL/HDL: '+ldl_hdl+' , Lipoprotein: '+lipoprotein+' , Apolipoprotein-A: '+apolipoprotein_a+' , Apolipoprotein-B: '+apolipoprotein_b+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Lipidreport!=""){
            foreach($Lipidreport as $Lipidreportval){
                $pdate = $Lipidreportval->dateval;
                $deleted = $Lipidreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(total_cholesterol!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/lipiddetail') }}",
                            data: { regid:regid, lipid_date:lipid_date,lipid_rand:lipid_rand,total_cholesterol:total_cholesterol,triglycerides:triglycerides,hdl_cholesterol:hdl_cholesterol,ldl_cholesterol:ldl_cholesterol,vldl:vldl,hdl_ratio:hdl_ratio,ldl_hdl:ldl_hdl,lipoprotein:lipoprotein,apolipoprotein_a:apolipoprotein_a,apolipoprotein_b:apolipoprotein_b},   
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                    var lid =  $('.lipid_id').val();
                    var total_cholesterol_1 = $('.total_cholesterol1').val();
                    var triglycerides_1 = $('.triglycerides1').val();
                    var hdl_cholesterol_1 = $('.hdl_cholesterol1').val();
                    var ldl_cholesterol_1 = $('.ldl_cholesterol1').val();
                    var vldl_1 = $('.vldl1').val();
                    var hdl_ratio_1 = $('.hdl_ratio1').val();
                    var ldl_hdl_1 = $('.ldl_hdl1').val();
                    var lipoprotein_1 = $('.lipoprotein1').val();
                    var apolipoprotein_a_1 = $('.apolipoprotein_a1').val();
                    var apolipoprotein_b_1 = $('.apolipoprotein_b1').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatelipiddetail') }}",
                        data: { id:lid , regid:regid, total_cholesterol:total_cholesterol_1,triglycerides:triglycerides_1,hdl_cholesterol:hdl_cholesterol_1,ldl_cholesterol:ldl_cholesterol_1,vldl:vldl_1,hdl_ratio:hdl_ratio_1,ldl_hdl:ldl_hdl_1,lipoprotein:lipoprotein_1,apolipoprotein_a:apolipoprotein_a_1,apolipoprotein_b:apolipoprotein_b_1},  
                        success: function( msg ) {
                           

                        }
                    });
                    var heading ="Lipid Profile:\n";

                    if(total_cholesterol_1!=""){
                        var notes_n = 'Total Cholesterol : '+total_cholesterol_1+'\n'+nnotes+'\n';
                    }
                    else{
                        var notes_1 = nnotes+'\n';
                    }
                    if(triglycerides_1!=""){
                        var notes_1 = 'Triglycerides : '+tiglycerides_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(hdl_cholesterol_1!=""){
                        var notes_1 = 'HDL Cholesterol : '+hdl_cholesterol_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    if(ldl_cholesterol_1!=""){
                        var notes_1 = 'LDL Cholesterol : '+ldl_cholesterol_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(vldl_1!=""){
                        var notes_1 = 'VLDL : '+vldl_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                     if(hdl_ratio_1!=""){
                        var notes_1 = 'Chol/HDL ratio : '+hdl_ratio_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    if(ldl_hdl_1!=""){
                        var notes_1 = 'LDL/HDL : '+ldl_hdl_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_n = notes_1+'\n';
                    }
                    if(lipoprotein_1!=""){
                        var notes_1 = 'Lipoprotein : '+lipoprotein_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    if(apolipoprotein_a_1!=""){
                        var notes_1 = 'Apolipoprotein-A : '+apolipoprotein_a_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(apolipoprotein_b_1!=""){
                        var notes_1 = 'Apolipoprotein-B : '+apolipoprotein_b_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_n = notes_1+'\n';
                    } 
                    var notes1 = heading+' '+notes_1;
                    //var notes1 = 'Lipid Profile:\n Total Cholesterol : '+total_cholesterol_1+' , Triglycerides: '+triglycerides_1+' , HDL Cholesterol:  '+hdl_cholesterol_1+' , LDL Cholesterol: '+ldl_cholesterol_1+' , VLDL: '+vldl_1+' , Chol/HDL ratio: '+hdl_ratio_1+' , LDL/HDL: '+ldl_hdl_1+' , Lipoprotein: '+lipoprotein_1+' , Apolipoprotein-A: '+apolipoprotein_a_1+' , Apolipoprotein-B: '+apolipoprotein_b_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // Liver Profile

    $(".saveliver").on("click", function(){
        var liver_date = $('.liver_date').val();
        var liver_rand = $('.liver_rand').val();
        var total_bil = $('.total_bil').val();
        var dir_bilirubin = $('.dir_bilirubin').val();
        var ind_bilirubin = $('.ind_bilirubin').val();
        var gamma_gt = $('.gamma_gt').val();
        var total_protein = $('.total_protein').val();
        var albumin = $('.albumin').val();
        var globulin = $('.globulin').val();
        var sgot = $('.sgot').val();
        var sgpt = $('.sgpt').val();
        var alk_phos = $('.alk_phos').val();
        var aust_antigen = $('.aust_antigen').val();
        var amylase = $('.amylase').val();
        var regid = $('.regid').val();
        var id =  $('.liver_id').val();
        var already = "";
        <?php
        if($Liverreport!=""){
            foreach($Liverreport as $Liverreportval){
                $pdate = $Liverreportval->dateval;
                $deleted = $Liverreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(total_bil!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/liverdetail') }}",
                data: { regid:regid, liver_date:liver_date,liver_rand:liver_rand,total_bil:total_bil,dir_bilirubin:dir_bilirubin,ind_bilirubin:ind_bilirubin,gamma_gt:gamma_gt,total_protein:total_protein,albumin:albumin,globulin:globulin,sgot:sgot,sgpt:sgpt,alk_phos:alk_phos,aust_antigen:aust_antigen,amylase:amylase}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.liver_date1').val(liver_date);$('.total_bil1').val(total_bil);$('.dir_bilirubin1').val(dir_bilirubin);$('.ind_bilirubin1').val(ind_bilirubin);$('.gamma_gt1').val(gamma_gt);$('.total_protein1').val(total_protein);$('.albumin1').val(albumin);$('.globulin1').val(globulin);$('.sgot1').val(sgot);$('.sgpt1').val(sgpt);$('.alk_phos1').val(alk_phos);$('.aust_antigen1').val(aust_antigen);$('.amylase1').val(amylase);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateliverdetail') }}",
                data: {id:id, regid:regid, liver_date:liver_date,liver_rand:liver_rand,total_bil:total_bil,dir_bilirubin:dir_bilirubin,ind_bilirubin:ind_bilirubin,gamma_gt:gamma_gt,total_protein:total_protein,albumin:albumin,globulin:globulin,sgot:sgot,sgpt:sgpt,alk_phos:alk_phos,aust_antigen:aust_antigen,amylase:amylase},   
                success: function( msg ) {
                 
                   $('.liver_date1').val(liver_date);$('.total_bil1').val(total_bil);$('.dir_bilirubin1').val(dir_bilirubin);$('.ind_bilirubin1').val(ind_bilirubin);$('.gamma_gt1').val(gamma_gt);$('.total_protein1').val(total_protein);$('.albumin1').val(albumin);$('.globulin1').val(globulin);$('.sgot1').val(sgot);$('.sgpt1').val(sgpt);$('.alk_phos1').val(alk_phos);$('.aust_antigen1').val(aust_antigen);$('.amylase1').val(amylase);
                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowliver").on("click", function(){
        var liver_date = $('.liver_date1').val();
        if(liver_date==""){
            liver_date = $('.liver_date').val();
        }
        var total_bil = $('.total_bil1').val();
        if(total_bil==""){
            total_bil = $('.total_bil').val();
        }
        var dir_bilirubin = $('.dir_bilirubin1').val();
        if(dir_bilirubin==""){
            dir_bilirubin = $('.dir_bilirubin').val();
        }
        var ind_bilirubin = $('.ind_bilirubin1').val();
        if(ind_bilirubin==""){
            ind_bilirubin = $('.ind_bilirubin').val();
        }
        var gamma_gt = $('.gamma_gt1').val();
        if(gamma_gt==""){
            gamma_gt = $('.gamma_gt').val();
        }
        var total_protein = $('.total_protein1').val();
        if(total_protein==""){
            total_protein = $('.total_protein').val();
        }
        var albumin = $('.albumin1').val();
        if(albumin==""){
            albumin = $('.albumin').val();
        }
        var globulin = $('.globulin1').val();
        if(globulin==""){
            globulin = $('.globulin').val();
        }
        var sgot = $('.sgot1').val();
        if(sgot==""){
            sgot = $('.sgot').val();
        }
        var sgpt = $('.sgpt1').val();
        if(sgpt==""){
            sgpt = $('.sgpt').val();
        }
        var alk_phos = $('.alk_phos1').val();
        if(alk_phos==""){
            alk_phos = $('.alk_phos').val();
        }
        var aust_antigen = $('.aust_antigen1').val();
        if(aust_antigen==""){
            aust_antigen = $('.aust_antigen').val();
        }
        var amylase = $('.amylase1').val();
        if(amylase==""){
            amylase = $('.amylase').val();
        }
        var regid = $('.regid').val();
        var liver_rand = $('.liver_rand').val();
        var nnotes = $('.nnotes').val();

        var heading ="Liver Profile:\n";

        if(total_bil!=""){
            var notes_n = 'Total Bil : '+total_bil+'\n'+nnotes+'\n';
        }
        else{
            var notes_n = nnotes+'\n';
        }
        if(dir_bilirubin!=""){
            var notes_n = 'Dir.Bilirubin : '+dir_bilirubin+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(ind_bilirubin!=""){
            var notes_n = 'Ind.Bilirubin : '+ind_bilirubin+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(gamma_gt!=""){
            var notes_n = 'Gamma G.T : '+gamma_gt+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(total_protein!=""){
            var notes_n = 'Total Proteins : '+total_protein+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(albumin!=""){
            var notes_n = 'Albumin : '+albumin+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(globulin!=""){
            var notes_n = 'Globulin : '+globulin+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(sgot!=""){
            var notes_n = 'S.G.O.T : '+sgot+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(sgpt!=""){
            var notes_n = 'S.G.P.T : '+sgpt+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(alk_phos!=""){
            var notes_n = 'Alk. Phos : '+alk_phos+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(aust_antigen!=""){
            var notes_n = 'Aust.Antigen : '+aust_antigen+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(amylase!=""){
            var notes_n = 'Amylase : '+amylase+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        
        var notes = heading+' '+notes_n;

        
        //var notes = 'Liver Profile:\n Total Bil : '+total_bil+' , Dir.Bilirubin: '+dir_bilirubin+' , Ind.Bilirubin:  '+ind_bilirubin+' , Gamma G.T: '+gamma_gt+' , Total Proteins: '+total_protein+' , Albumin: '+albumin+' , Globulin: '+globulin+' , S.G.O.T: '+sgot+' , S.G.P.T: '+sgpt+' , Alk. Phos: '+alk_phos+' , Aust.Antigen: '+aust_antigen+' , Amylase: '+amylase+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Liverreport!=""){
            foreach($Liverreport as $Liverreportval){
                $pdate = $Liverreportval->dateval;
                $deleted = $Liverreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(total_bil!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/liverdetail') }}",
                            data: { regid:regid, liver_date:liver_date,liver_rand:liver_rand,total_bil:total_bil,dir_bilirubin:dir_bilirubin,ind_bilirubin:ind_bilirubin,gamma_gt:gamma_gt,total_protein:total_protein,albumin:albumin,globulin:globulin,sgot:sgot,sgpt:sgpt,alk_phos:alk_phos,aust_antigen:aust_antigen,amylase:amylase},   
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                    var lvid =  $('.liver_id').val();
                    var total_bil_1 = $('.total_bil1').val();
                    var dir_bilirubin_1 = $('.dir_bilirubin1').val();
                    var ind_bilirubin_1 = $('.ind_bilirubin1').val();
                    var gamma_gt_1 = $('.gamma_gt1').val();
                    var total_protein_1 = $('.total_protein1').val();
                    var albumin_1 = $('.albumin1').val();
                    var globulin_1 = $('.globulin1').val();
                    var sgot_1 = $('.sgot1').val();
                    var sgpt_1 = $('.sgpt1').val();
                    var alk_phos_1 = $('.alk_phos1').val();
                    var aust_antigen_1 = $('.aust_antigen1').val();
                    var amylase_1 = $('.amylase1').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updateliverdetail') }}",
                        data: {id:lvid, regid:regid, liver_date:liver_date,liver_rand:liver_rand,total_bil:total_bil_1,dir_bilirubin:dir_bilirubin_1,ind_bilirubin:ind_bilirubin_1,gamma_gt:gamma_gt_1,total_protein:total_protein_1,albumin:albumin_1,globulin:globulin_1,sgot:sgot_1,sgpt:sgpt_1,alk_phos:alk_phos_1,aust_antigen:aust_antigen_1,amylase:amylase_1},    
                        success: function( msg ) {
                           

                        }
                    });
                    var heading ="Liver Profile:\n";

                    if(total_bil_1!=""){
                        var notes_1 = 'Total Bil : '+total_bil_1+'\n'+nnotes+'\n';
                    }
                    else{
                        var notes_1 = nnotes+'\n';
                    }
                    if(dir_bilirubin_1!=""){
                        var notes_1 = 'Dir.Bilirubin : '+dir_bilirubin_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(ind_bilirubin_1!=""){
                        var notes_1 = 'Ind.Bilirubin : '+ind_bilirubin_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_n = notes_n+'\n';
                    } 
                    if(gamma_gt_1!=""){
                        var notes_1 = 'Gamma G.T : '+gamma_gt_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(total_protein_1!=""){
                        var notes_1 = 'Total Proteins : '+total_protein_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    if(albumin_1!=""){
                        var notes_1 = 'Albumin : '+albumin_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(globulin_1!=""){
                        var notes_1 = 'Globulin : '+globulin_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    if(sgot_1!=""){
                        var notes_1 = 'S.G.O.T : '+sgot_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(sgpt_1!=""){
                        var notes_1 = 'S.G.P.T : '+sgpt_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    if(alk_phos_1!=""){
                        var notes_1 = 'Alk. Phos : '+alk_phos_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    if(aust_antigen_1!=""){
                        var notes_1 = 'Aust.Antigen : '+aust_antigen_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(amylase_1!=""){
                        var notes_1 = 'Amylase : '+amylase_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    
                    var notes1 = heading+' '+notes_1;
                    //var notes1 = 'Liver Profile:\n Total Bil : '+total_bil_1+' , Dir.Bilirubin: '+dir_bilirubin_1+' , Ind.Bilirubin:  '+ind_bilirubin_1+' , Gamma G.T: '+gamma_gt_1+' , Total Proteins: '+total_protein_1+' , Albumin: '+albumin_1+' , Globulin: '+globulin_1+' , S.G.O.T: '+sgot_1+' , S.G.P.T: '+sgpt_1+' , Alk. Phos: '+alk_phos_1+' , Aust.Antigen: '+aust_antigen_1+' , Amylase: '+amylase_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

// Specific Reports
    $(".savespecific").on("click", function(){
        var specific_date = $('.specific_date').val();
        var other_findings = $('.other_findings').val();
        var define_field1 = $('.define_field1').val();
        var define_field2 = $('.define_field2').val();
        var define_field3 = $('.define_field3').val();
        var define_field4 = $('.define_field4').val();
        var specific_rand = $('.specific_rand').val();
        var regid = $('.regid').val();
        var id =  $('.specific_id').val();
        var already = "";
        <?php
        if($Specificreport!=""){
            foreach($Specificreport as $Specificreportval){
                $pdate = $Specificreportval->dateval;
                $deleted = $Specificreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(other_findings!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/specficdetail') }}",
                data: { regid:regid, specific_date:specific_date,other_findings:other_findings,define_field1:define_field1,define_field2:define_field2,define_field3:define_field3,define_field4:define_field4,specific_rand:specific_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.specific_date1').val(specific_date);$('.other_findings1').val(other_findings);$('.define_field1_1').val(define_field1);$('.define_field2_1').val(define_field2);$('.define_field3_1').val(define_field3);$('.define_field4_1').val(define_field4);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatespecficdetail') }}",
                data: {id:id, regid:regid, specific_date:specific_date,other_findings:other_findings,define_field1:define_field1,define_field2:define_field2,define_field3:define_field3,define_field4:define_field4},  
                success: function( msg ) {
                 
                $('.specific_date1').val(specific_date);$('.other_findings1').val(other_findings);$('.define_field1_1').val(define_field1);$('.define_field2_1').val(define_field2);$('.define_field3_1').val(define_field3);$('.define_field4_1').val(define_field4);         alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowspecific").on("click", function(){
        var other_findings = $('.other_findings1').val();
        if(other_findings==""){
            other_findings = $('.other_findings_sp').val();
        }
         var define_field1 = $('.define_field1_1').val();
        if(define_field1==""){
            define_field1 = $('.define_field1').val();
        }
         var define_field2 = $('.define_field2_1').val();
        if(define_field2==""){
            define_field2 = $('.define_field2').val();
        }
        var define_field3 = $('.define_field3_1').val();
        if(define_field3==""){
            define_field3 = $('.define_field3').val();
        }
        var define_field4 = $('.define_field4_1').val();
        if(define_field4==""){
            define_field4 = $('.define_field4').val();
        }
        var specific_date = $('.specific_date1').val();
        if(specific_date==""){
            specific_date = $('.specific_date').val();
        }
        var regid = $('.regid').val();
        var specific_rand = $('.specific_rand').val();
        var nnotes = $('.nnotes').val();

        var heading ="Specific Profile:\n";

        if(other_findings!=""){
            var notes_n = 'Other Findings : '+other_findings+'\n'+nnotes+'\n';
        }
        else{
            var notes_n = nnotes+'\n';
        }
        if(define_field1!=""){
            var notes_n = 'Define Field 1 : '+define_field1+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(define_field2!=""){
            var notes_n = 'Define Field 2 : '+define_field2+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(define_field3!=""){
            var notes_n = 'Define Field 3 : '+define_field3+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(define_field4!=""){
            var notes_n = 'Define Field 4 : '+define_field4+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        var notes = heading+' '+notes_n;
        
       // var notes = 'Specific Profile : Other Findings:- '+other_findings+', Define Field 1:-'+define_field1+', Define Field 2:- '+define_field2+', Define Field 3:- '+define_field3+', Define Field 4:- '+define_field4+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Specificreport!=""){
            foreach($Specificreport as $Specificreportval){
                $pdate = $Specificreportval->dateval;
                $deleted = $Specificreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(other_findings!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/specficdetail') }}",
                            data: { regid:regid, specific_date:specific_date,other_findings:other_findings,define_field1:define_field1,define_field2:define_field2,define_field3:define_field3,define_field4:define_field4,specific_rand:specific_rand},   
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var spid =  $('.specific_id').val();
                var other_findings_1 = $('.other_findings_sp').val();
                var define_field1_1 = $('.define_field1').val();
                var define_field2_1 = $('.define_field2').val();
                var define_field3_1 = $('.define_field3').val();
                var define_field4_1 = $('.define_field4').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatespecficdetail') }}",
                        data: { id:id, regid:regid, specific_date:specific_date,other_findings:other_findings_1,define_field1:define_field1_1,define_field2:define_field2_1,define_field3:define_field3_1,define_field4:define_field4_1}, 
                        success: function( msg ) {
                           

                        }
                    });

                    var heading ="Specific Profile:\n";

                    if(other_findings_1!=""){
                        var notes_1 = 'Other Findings : '+other_findings_1+'\n'+nnotes+'\n';
                    }
                    else{
                        var notes_1 = nnotes+'\n';
                    }
                    if(define_field1_1!=""){
                        var notes_1 = 'Define Field 1 : '+define_field1_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(define_field2_1!=""){
                        var notes_1 = 'Define Field 2 : '+define_field2_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    if(define_field3_1!=""){
                        var notes_1 = 'Define Field 3 : '+define_field3_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(define_field4_1!=""){
                        var notes_1 = 'Define Field 4 : '+define_field4_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    var notes1 = heading+' '+notes_1;
                    //var notes1 = 'Specific Profile : Other Findings:- '+other_findings_1+', Define Field 1:-'+define_field1_1+', Define Field 2:- '+define_field2_1+', Define Field 3:- '+define_field3_1+', Define Field 4:- '+define_field4_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // Immunology Reports
    $(".saveimmunology").on("click", function(){
        var immunology_date = $('.immunology_date').val();
        var igg = $('.igg_imm').val();
        var ige = $('.ige').val();
        var igm = $('.igm').val();
        var iga = $('.iga').val();
        var itg = $('.itg').val();
        var immunology_rand = $('.immunology_rand').val();
        var regid = $('.regid').val();
        var id =  $('.immunology_id').val();
        var already = "";
        <?php
        if($Immunologyreport!=""){
            foreach($Immunologyreport as $Immunologyreportval){
                $pdate = $Immunologyreportval->dateval;
                $deleted = $Immunologyreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(igg!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/immunologydetail') }}",
                data: { regid:regid, immunology_date:immunology_date,igg:igg,ige:ige,igm:igm,iga:iga,itg:itg,immunology_rand:immunology_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.immunology_date1').val(immunology_date);$('.igg1').val(igg);$('.ige1').val(ige);$('.igm1').val(igm);$('.iga1').val(iga);$('.itg1').val(itg);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateimmunologydetail') }}",
                data: { id:id, regid:regid, immunology_date:immunology_date,igg:igg,ige:ige,igm:igm,iga:iga,itg:itg}, 
                success: function( msg ) {
                 
                $('.immunology_date1').val(immunology_date);$('.igg1').val(igg);$('.ige1').val(ige);$('.igm1').val(igm);$('.iga1').val(iga);$('.itg1').val(itg);   
                    alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowimmunology").on("click", function(){
        var igg = $('.igg1').val();
        if(igg==""){
            igg = $('.igg_imm').val();
        }
         var ige = $('.ige1').val();
        if(ige==""){
            ige = $('.ige').val();
        }
         var igm = $('.igm1').val();
        if(igm==""){
            igm = $('.igm').val();
        }
        var iga = $('.iga1').val();
        if(iga==""){
            iga = $('.iga').val();
        }
        var itg = $('.itg1').val();
        if(itg==""){
            itg = $('.itg').val();
        }
        var immunology_date = $('.immunology_date').val();
        var regid = $('.regid').val();
        var immunology_rand = $('.immunology_rand').val();
        var nnotes = $('.nnotes').val();

        var heading ="Immunology Profile:\n";

        if(igg!=""){
            var notes_n = 'IgG : '+igg+'\n'+nnotes+'\n';
        }
        else{
            var notes_n = nnotes+'\n';
        }
        if(ige!=""){
            var notes_n = 'IgE : '+ige+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(igm!=""){
            var notes_n = 'IgM : '+igm+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(iga!=""){
            var notes_n = 'IgA : '+iga+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(itg!=""){
            var notes_n = 'Itg : '+itg+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        var notes = heading+' '+notes_n;
        
       // var notes = 'Immunology Profile : IgG:- '+igg+', IgE:-'+ige+', IgM:- '+igm+', IgA:- '+iga+', Itg:- '+itg+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Immunologyreport!=""){
            foreach($Immunologyreport as $Immunologyreportval){
                $pdate = $Immunologyreportval->dateval;
                $deleted = $Immunologyreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(igg!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/immunologydetail') }}",
                            data: { regid:regid, immunology_date:immunology_date,igg:igg,ige:ige,igm:igm,iga:iga,itg:itg,immunology_rand:immunology_rand}, 
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                    var imid =  $('.immunology_id').val();
                    var igg_1 = $('.igg_imm').val();
                    var ige_1 = $('.ige').val();
                    var igm_1 = $('.igm').val();
                    var iga_1 = $('.iga').val();
                    var itg_1 = $('.itg').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updateimmunologydetail') }}",
                        data: { id:imid, regid:regid,igg:igg_1,ige:ige_1,igm:igm_1,iga:iga_1,itg:itg_1}, 
                        success: function( msg ) {
                           

                        }
                    });

                    var heading ="Immunology Profile:\n";

                    if(igg_1!=""){
                        var notes_1 = 'IgG : '+igg_1+'\n'+nnotes+'\n';
                    }
                    else{
                        var notes_1 = nnotes+'\n';
                    }
                    if(ige_1!=""){
                        var notes_1 = 'IgE : '+ige_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_n+'\n';
                    }
                    if(igm_1!=""){
                        var notes_1 = 'IgM : '+igm_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_n+'\n';
                    } 
                    if(iga_1!=""){
                        var notes_1 = 'IgA : '+iga_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_n+'\n';
                    }
                    if(itg_1!=""){
                        var notes_1 = 'Itg : '+itg_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_n+'\n';
                    } 
                    var notes1 = heading+' '+notes_1;
                    //var notes1 = 'Immunology Profile : IgG:- '+igg_1+', IgE:-'+ige_1+', IgM:- '+igm_1+', IgA:- '+iga_1+', Itg:- '+itg_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // USG Male
    $(".savemale").on("click", function(){
        var male_date = $('.male_date').val();
        var size = $('.size').val();
        var volume = $('.volume').val();
        var serum_psa = $('.serum_psa').val();
        var other_findings = $('.other_findings').val();
        var male_rand = $('.male_rand').val();
        var regid = $('.regid').val();
        var id =  $('.maleid').val();
        var already = "";
        <?php
        if($Malereport!=""){
            foreach($Malereport as $Malereportval){
                $pdate = $Malereportval->dateval;
                $deleted = $Malereportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(size!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/maledetail') }}",
                data: { regid:regid, male_date:male_date,size:size,volume:volume,serum_psa:serum_psa,other_findings:other_findings,male_rand:male_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.size1').val(size);$('.volume1').val(volume);$('.serum_psa1').val(serum_psa);$('.other_findings1').val(other_findings);$('.male_date1').val(male_date);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatemaledetail') }}",
                data: { id:id, regid:regid, male_date:male_date,size:size,volume:volume,serum_psa:serum_psa,other_findings:other_findings}, 
                success: function( msg ) {
                 
                   $('.size1').val(size);$('.volume1').val(volume);$('.serum_psa1').val(serum_psa);$('.other_findings1').val(other_findings);$('.male_date1').val(male_date);
                   alert("Details Update");
                   
                }
            });
    }
    });

     $(".savefollowmale").on("click", function(){
        var size = $('.size1').val();
        if(size==""){
            size = $('.size').val();
        }
        var volume = $('.volume1').val();
        if(volume==""){
            volume = $('.volume').val();
        }
        var serum_psa = $('.serum_psa1').val();
        if(serum_psa==""){
            serum_psa = $('.serum_psa').val();
        }
        var other_findings = $('.other_findings1').val();
        if(other_findings==""){
            other_findings = $('.other_findings').val();
        }
        var male_date = $('.male_date1').val();
        if(male_date==""){
            male_date = $('.male_date').val();
        }
        var regid = $('.regid').val();
        var male_rand = $('.male_rand').val();
        var nnotes = $('.nnotes').val();

        var heading ="USG Male :\n";

        if(size!=""){
            var notes_n = 'Prostate Size : '+size+'\n'+nnotes+'\n';
        }
        else{
            var notes_n = nnotes+'\n';
        }
        if(volume!=""){
            var notes_n = 'Prostate Volume : '+volume+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(serum_psa!=""){
            var notes_n = 'Serum PSA : '+serum_psa+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(other_findings!=""){
            var notes_n = 'Other Findings : '+other_findings+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        var notes = heading+' '+notes_n;
        
       // var notes = 'USG Male : Prostate Size: '+size+' , Prostate Volume:'+volume+' , Serum PSA:'+serum_psa+' , Other Findings:'+other_findings+'\n'+nnotes+'\n';

        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Malereport!=""){
            foreach($Malereport as $Malereportval){
                $pdate = $Malereportval->dateval;
                $deleted = $Malereportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(size!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/maledetail') }}",
                            data: { regid:regid, male_date:male_date,size:size,volume:volume,serum_psa:serum_psa,other_findings:other_findings,male_rand:male_rand},  
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                    var mid =  $('.maleid').val();
                    var size_1 = $('.size').val();
                    var volume_1 = $('.volume').val();
                    var serum_psa_1 = $('.serum_psa').val();
                    var other_findings_1 = $('.other_findings').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatemaledetail') }}",
                        data: { id:mid, regid:regid, male_date:male_date,size:size_1,volume:volume_1,serum_psa:serum_psa_1,other_findings:other_findings_1}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var heading ="USG Male :\n";

                        if(size_1!=""){
                            var notes_1 = 'Prostate Size : '+size_1+'\n'+nnotes+'\n';
                        }
                        else{
                            var notes_1 = nnotes+'\n';
                        }
                        if(volume_1!=""){
                            var notes_1 = 'Prostate Volume : '+volume_1+'\n'+notes_1+'\n';
                        }
                        else{
                            var notes_1 = notes_1+'\n';
                        }
                        if(serum_psa_1!=""){
                            var notes_1 = 'Serum PSA : '+serum_psa_1+'\n'+notes_1+'\n';
                        }
                        else{
                            var notes_1 = notes_1+'\n';
                        } 
                        if(other_findings_1!=""){
                            var notes_1 = 'Other Findings : '+other_findings_1+'\n'+notes_1+'\n';
                        }
                        else{
                            var notes_1 = notes_1+'\n';
                        } 
                        var notes1 = heading+' '+notes_n;
                    var notes1 = 'USG Male : Prostate Size: '+size_1+' , Prostate Volume:'+volume_1+' , Serum PSA:'+serum_psa_1+' , Other Findings:'+other_findings_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // Xray Detail
    $(".savexray").on("click", function(){
        var xray_date = $('.xray_date').val();
        var radiological_report = $('.radiological_report_s').val();
        var xray_rand = $('.xray_rand').val();
        var regid = $('.regid').val();
        var id =  $('.xrayid').val();
        var already = "";
        <?php
        if($Xrayreport!=""){
            foreach($Xrayreport as $Xrayreportval){
                $pdate = $Xrayreportval->dateval;
                $deleted = $Xrayreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(radiological_report!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/xraydetail') }}",
                data: { regid:regid, xray_date:xray_date,radiological_report:radiological_report,xray_rand:xray_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.radiological_report1').val(radiological_report);$('.xray_date1').val(xray_date);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatexraydetail') }}",
                data: { id:id,regid:regid, xray_date:xray_date,radiological_report:radiological_report}, 
                success: function( msg ) {
                 
                   $('.radiological_report1').val(radiological_report);$('.xray_date1').val(xray_date);
                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowxray").on("click", function(){
        var radiological_report = $('.radiological_report1').val();
        if(radiological_report==""){
            radiological_report = $('.radiological_report_s').val();
        }
        var xray_date = $('.xray_date1').val();
        if(xray_date==""){
            xray_date = $('.xray_date').val();
        }
        var regid = $('.regid').val();
        var xray_rand = $('.xray_rand').val();
        var nnotes = $('.nnotes').val();
        
        var notes = 'Radiological Reports : '+radiological_report+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Xrayreport!=""){
            foreach($Xrayreport as $Xrayreportval){
                $pdate = $Xrayreportval->dateval;
                $deleted = $Xrayreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(radiological_report!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/xraydetail') }}",
                            data: { regid:regid, xray_date:xray_date,radiological_report:radiological_report,xray_rand:xray_rand}, 
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var xid =  $('.xrayid').val();
                var radiological_report_1 = $('.radiological_report_s').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatexraydetail') }}",
                        data: { id:xid,regid:regid,radiological_report:radiological_report_1},
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'Radiological Reports : '+radiological_report_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // Arthritis Detail
    $(".savearthris").on("click", function(){
        var ar_date = $('.ar_date').val();
        var anti_o = $('.anti_o').val();
        var accp = $('.accp').val();
        var ra_factor = $('.ra_factor').val();
        var alkaline = $('.alkaline').val();
        var ana = $('.ana').val();
        var c_react = $('.c_react').val();
        var complement = $('.complement').val();
        var ar_rand = $('.ar_rand').val();
        var regid = $('.regid').val();
        var id =  $('.arid').val();
        var already = "";
        <?php
        if($Arithrisreport!=""){
            foreach($Arithrisreport as $Arithrisreportval){
                $pdate = $Arithrisreportval->dateval;
                $deleted = $Arithrisreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(anti_o!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/arithrisdetail') }}",
                data: { regid:regid, ar_date:ar_date,anti_o:anti_o, accp:accp, ra_factor:ra_factor, alkaline:alkaline, ana:ana, c_react:c_react, complement:complement,  ar_rand:ar_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.ar_date1').val(ar_date);$('.anti_o1').val(anti_o);$('.accp1').val(accp);$('.ra_factor1').val(ra_factor);$('.alkaline1').val(alkaline);$('.ana1').val(ana);$('.c_react1').val(c_react);$('.complement1').val(complement);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatearithrisdetail') }}",
                data: { id:id, regid:regid, ar_date:ar_date,anti_o:anti_o, accp:accp, ra_factor:ra_factor, alkaline:alkaline, ana:ana, c_react:c_react, complement:complement,  ar_rand:ar_rand}, 
                success: function( msg ) {
                 
                   $('.ar_date1').val(ar_date);$('.anti_o1').val(anti_o);$('.accp1').val(accp);$('.ra_factor1').val(ra_factor);$('.alkaline1').val(alkaline);$('.ana1').val(ana);$('.c_react1').val(c_react);$('.complement1').val(complement);
                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowarthris").on("click", function(){
        var anti_o = $('.anti_o1').val();
        if(anti_o==""){
            anti_o = $('.anti_o').val();
        }
        var accp = $('.accp1').val();
        if(accp==""){
            accp = $('.accp').val();
        }
        var ra_factor = $('.ra_factor1').val();
        if(ra_factor==""){
            ra_factor = $('.ra_factor').val();
        }
        var alkaline = $('.alkaline1').val();
        if(alkaline==""){
            alkaline = $('.alkaline').val();
        }
        var ana = $('.ana1').val();
        if(ana==""){
            ana = $('.ana').val();
        }
        var c_react = $('.c_react1').val();
        if(c_react==""){
            c_react = $('.c_react').val();
        }
        var complement = $('.complement1').val();
        if(complement==""){
            complement = $('.complement').val();
        }
        var ar_date = $('.ar_date1').val();
        if(ar_date==""){
            ar_date = $('.ar_date').val();
        }
        var regid = $('.regid').val();
        var ar_rand = $('.ar_rand').val();
        var nnotes = $('.nnotes').val();

        var heading ="Arthritis Profile:\n";

        if(anti_o!=""){
            var notes_n = 'Anti-streptolysin O : '+anti_o+'\n'+nnotes+'\n';
        }
        else{
            var notes_n = nnotes+'\n';
        }
        if(accp!=""){
            var notes_n = 'ACCP : '+accp+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(ra_factor!=""){
            var notes_n = 'RA Factor : '+ra_factor+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
         if(alkaline!=""){
            var notes_n = 'Alkaline Phosphatase : '+alkaline+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        }
        if(complement!=""){
            var notes_n = 'Complement4 (C4) : '+complement+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        if(c_react!=""){
            var notes_n = 'C-Reactive Protein : '+c_react+'\n'+notes_n+'\n';
        }
        else{
            var notes_n = notes_n+'\n';
        } 
        var notes = heading+' '+notes_n;
        
       // var notes = 'Arthritis Details : \n Anti-streptolysin O:'+anti_o+', ACCP :'+accp+', RA Factor :'+ra_factor+', Alkaline Phosphatase :'+alkaline+', Complement4 (C4) :'+complement+', ANA :'+ana+', C-Reactive Protein :'+c_react+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Arithrisreport!=""){
            foreach($Arithrisreport as $Arithrisreportval){
                $pdate = $Arithrisreportval->dateval;
                $deleted = $Arithrisreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(anti_o!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/arithrisdetail') }}",
                            data: { regid:regid, ar_date:ar_date,anti_o:anti_o, accp:accp, ra_factor:ra_factor, alkaline:alkaline, ana:ana, c_react:c_react, complement:complement,  ar_rand:ar_rand},
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var aid =  $('.arid').val();
                 var anti_o_1 = $('.anti_o').val();
                 var accp_1 = $('.accp').val();
                 var ra_factor_1 = $('.ra_factor').val();
                 var alkaline_1 = $('.alkaline').val();
                 var ana_1 = $('.ana').val();
                 var c_react_1 = $('.c_react').val();
                 var complement_1 = $('.complement').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatearithrisdetail') }}",
                        data: { id:aid, regid:regid, ar_date:ar_date,anti_o:anti_o_1, accp:accp_1, ra_factor:ra_factor_1, alkaline:alkaline_1, ana:ana_1, c_react:c_react_1, complement:complement_1,  ar_rand:ar_rand}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var heading ="Arthritis Profile:\n";

                    if(anti_o_1!=""){
                        var notes_1 = 'Anti-streptolysin O : '+anti_o_1+'\n'+nnotes+'\n';
                    }
                    else{
                        var notes_1 = nnotes+'\n';
                    }
                    if(accp_1!=""){
                        var notes_1 = 'ACCP : '+accp_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(ra_factor_1!=""){
                        var notes_1 = 'RA Factor : '+ra_factor_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                     if(alkaline_1!=""){
                        var notes_1 = 'Alkaline Phosphatase : '+alkaline_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    }
                    if(complement_1!=""){
                        var notes_1 = 'Complement4 (C4) : '+complement_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    if(c_react_1!=""){
                        var notes_1 = 'C-Reactive Protein : '+c_react_1+'\n'+notes_1+'\n';
                    }
                    else{
                        var notes_1 = notes_1+'\n';
                    } 
                    var notes1 = heading+' '+notes_1;

                    //var notes1 = 'Arthritis Details : \n Anti-streptolysin O:'+anti_o_1+', ACCP :'+accp_1+', RA Factor :'+ra_factor_1+', Alkaline Phosphatase :'+alkaline_1+', Complement4 (C4) :'+complement_1+', ANA :'+ana_1+', C-Reactive Protein :'+c_react_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // USG Female
    $(".savefemale").on("click", function(){
        var female_date = $('.female_date').val();
        var uterues_size = $('.uterues_size').val();
        var thickness = $('.thickness').val();
        var fibroids_no = $('.fibroids_no').val();
        var description = $('.description').val();
        var ovary_size_rt = $('.ovary_size_rt').val();
        var ovary_size_lt = $('.ovary_size_lt').val();
        var ovary_volume_rt = $('.ovary_volume_rt').val();
        var ovary_volume_lt = $('.ovary_volume_lt').val();
        var follicles_rt = $('.follicles_rt').val();
        var follicles_lt = $('.follicles_lt').val();
        var female_rand = $('.female_rand').val();
        var regid = $('.regid').val();
        var id =  $('.femaleid').val();
        var already = "";
        <?php
        if($Femalereport!=""){
            foreach($Femalereport as $Femalereportval){
                $pdate = $Femalereportval->dateval;
                $deleted = $Femalereportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(uterues_size!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/femaledetail') }}",
                data: { regid:regid, female_date:female_date,uterues_size:uterues_size, thickness:thickness, fibroids_no:fibroids_no, description:description, ovary_size_rt:ovary_size_rt, ovary_size_lt:ovary_size_lt, ovary_volume_rt:ovary_volume_rt,ovary_volume_lt:ovary_volume_lt,follicles_rt:follicles_rt,follicles_lt:follicles_lt, female_rand:female_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.female_date1').val(female_date);$('.uterues_size1').val(uterues_size);$('.thickness1').val(thickness);$('.fibroids_no1').val(fibroids_no);$('.description1').val(description);$('.ovary_size_rt1').val(ovary_size_rt);$('.ovary_size_lt1').val(ovary_size_lt);$('.ovary_volume_rt1').val(ovary_volume_rt);$('.ovary_volume_lt1').val(ovary_volume_lt);$('.follicles_rt1').val(follicles_rt);$('.follicles_lt1').val(follicles_lt);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatefemaledetail') }}",
                data: {id:id, regid:regid, female_date:female_date,uterues_size:uterues_size, thickness:thickness, fibroids_no:fibroids_no, description:description, ovary_size_rt:ovary_size_rt, ovary_size_lt:ovary_size_lt, ovary_volume_rt:ovary_volume_rt,ovary_volume_lt:ovary_volume_lt,follicles_rt:follicles_rt,follicles_lt:follicles_lt, female_rand:female_rand}, 
                success: function( msg ) {
                 
                   $('.female_date1').val(female_date);$('.uterues_size1').val(uterues_size);$('.thickness1').val(thickness);$('.fibroids_no1').val(fibroids_no);$('.description1').val(description);$('.ovary_size_rt1').val(ovary_size_rt);$('.ovary_size_lt1').val(ovary_size_lt);$('.ovary_volume_rt1').val(ovary_volume_rt);$('.ovary_volume_lt1').val(ovary_volume_lt);$('.follicles_rt1').val(follicles_rt);$('.follicles_lt1').val(follicles_lt);

                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowfemale").on("click", function(){
        var uterues_size = $('.uterues_size1').val();
        if(uterues_size==""){
            uterues_size = $('.uterues_size').val();
        }
        var thickness = $('.thickness1').val();
        if(thickness==""){
            thickness = $('.thickness').val();
        }
        var fibroids_no = $('.fibroids_no1').val();
        if(fibroids_no==""){
            fibroids_no = $('.fibroids_no').val();
        }
        var description = $('.description1').val();
        if(description==""){
            description = $('.description').val();
        }
        var ovary_size_rt = $('.ovary_size_rt1').val();
        if(ovary_size_rt==""){
            ovary_size_rt = $('.ovary_size_rt').val();
        }
        var ovary_size_lt = $('.ovary_size_lt1').val();
        if(ovary_size_lt==""){
            ovary_size_lt = $('.ovary_size_lt').val();
        }
        var ovary_volume_rt = $('.ovary_volume_rt1').val();
        if(ovary_volume_rt==""){
            ovary_volume_rt = $('.ovary_volume_rt').val();
        }
        var ovary_volume_lt = $('.ovary_volume_lt1').val();
        if(ovary_volume_lt==""){
            ovary_volume_lt = $('.ovary_volume_rt').val();
        }
        var follicles_rt = $('.follicles_rt1').val();
        if(follicles_rt==""){
            follicles_rt = $('.follicles_rt').val();
        }
        var follicles_lt = $('.follicles_lt1').val();
        if(follicles_lt==""){
            follicles_lt = $('.follicles_lt').val();
        }
        var female_date = $('.female_date1').val();
        if(female_date==""){
            female_date = $('.female_date').val();
        }
        var regid = $('.regid').val();
        var female_rand = $('.female_rand').val();
        var nnotes = $('.nnotes').val();
        
        var notes = 'USG Female : \n Uterues Size:'+uterues_size+', Endometrial thickness :'+thickness+', Fibroids Number :'+fibroids_no+', Description :'+description+', Ovary Size :'+ovary_size_rt+'-'+ovary_size_lt+', Ovary Volume :'+ovary_volume_rt+'-'+ovary_volume_lt+', Follicles :'+follicles_rt+'-'+follicles_lt+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Femalereport!=""){
            foreach($Femalereport as $Femalereportval){
                $pdate = $Femalereportval->dateval;
                $deleted = $Femalereportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(uterues_size!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/femaledetail') }}",
                            data: { regid:regid, female_date:female_date,uterues_size:uterues_size, thickness:thickness, fibroids_no:fibroids_no, description:description, ovary_size_rt:ovary_size_rt, ovary_size_lt:ovary_size_lt, ovary_volume_rt:ovary_volume_rt,ovary_volume_lt:ovary_volume_lt,follicles_rt:follicles_rt,follicles_lt:follicles_lt, female_rand:female_rand},
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var fid =  $('.femaleid').val();
                 var uterues_size_1 = $('.uterues_size').val();
                 var thickness_1 = $('.thickness').val();
                 var fibroids_no_1 = $('.fibroids_no').val();
                 var description_1 = $('.description').val();
                 var ovary_size_rt_1 = $('.ovary_size_rt').val();
                 var ovary_size_lt_1 = $('.ovary_size_lt').val();
                 var ovary_volume_rt_1 = $('.ovary_volume_rt').val();
                 var ovary_volume_lt_1 = $('.ovary_volume_lt').val();
                 var follicles_rt_1 = $('.follicles_rt').val();
                 var follicles_lt_1 = $('.follicles_lt').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatefemaledetail') }}",
                        data: {id:fid, regid:regid, female_date:female_date,uterues_size:uterues_size_1, thickness:thickness_1, fibroids_no:fibroids_no_1, description:description_1, ovary_size_rt:ovary_size_rt_1, ovary_size_lt:ovary_size_lt_1, ovary_volume_rt:ovary_volume_rt_1,ovary_volume_lt:ovary_volume_lt_1,follicles_rt:follicles_rt_1,follicles_lt:follicles_lt_1, female_rand:female_rand}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'USG Female : \n Uterues Size:'+uterues_size_1+', Endometrial thickness :'+thickness_1+', Fibroids Number :'+fibroids_no_1+', Description :'+description_1+', Ovary Size :'+ovary_size_rt_1+'-'+ovary_size_lt_1+', Ovary Volume :'+ovary_volume_rt_1+'-'+ovary_volume_lt_1+', Follicles :'+follicles_rt_1+'-'+follicles_lt_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // Renal Profile
    $(".saverenal").on("click", function(){
        var renal_date = $('.renal_date').val();
        var bun = $('.bun').val();
        var urea = $('.urea').val();
        var creatinine = $('.creatinine').val();
        var uric_acid = $('.uric_acid').val();
        var calcium = $('.calcium').val();
        var phosphorus = $('.phosphorus').val();
        var sodium = $('.sodium').val();
        var potassium = $('.potassium').val();
        var chloride = $('.chloride').val();
        var renal_rand = $('.renal_rand').val();
        var regid = $('.regid').val();
        var id =  $('.renalid').val();
        var already = "";
        <?php
        if($Renalreport!=""){
            foreach($Renalreport as $Renalreportval){
                $pdate = $Renalreportval->dateval;
                $deleted = $Renalreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(bun!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/renaldetail') }}",
                data: { regid:regid, renal_date:renal_date,bun:bun, urea:urea, creatinine:creatinine, uric_acid:uric_acid, calcium:calcium, phosphorus:phosphorus, sodium:sodium,potassium:potassium,chloride:chloride, renal_rand:renal_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.renal_date1').val(renal_date);$('.bun1').val(bun);$('.urea1').val(urea);$('.creatinine1').val(creatinine);$('.uric_acid1').val(uric_acid);$('.calcium1').val(calcium);$('.phosphorus1').val(phosphorus);$('.sodium1').val(sodium);$('.potassium1').val(potassium);$('.chloride1').val(chloride);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updaterenaldetail') }}",
                data: { id:id, regid:regid, renal_date:renal_date,bun:bun, urea:urea, creatinine:creatinine, uric_acid:uric_acid, calcium:calcium, phosphorus:phosphorus, sodium:sodium,potassium:potassium,chloride:chloride, renal_rand:renal_rand},  
                success: function( msg ) {
                 
                   $('.renal_date1').val(renal_date);$('.bun1').val(bun);$('.urea1').val(urea);$('.creatinine1').val(creatinine);$('.uric_acid1').val(uric_acid);$('.calcium1').val(calcium);$('.phosphorus1').val(phosphorus);$('.sodium1').val(sodium);$('.potassium1').val(potassium);$('.chloride1').val(chloride);

                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowrenal").on("click", function(){
        var bun = $('.bun1').val();
        if(bun==""){
            bun = $('.bun').val();
        }
        var urea = $('.urea1').val();
        if(urea==""){
            urea = $('.urea').val();
        }
        var creatinine = $('.creatinine1').val();
        if(creatinine==""){
            creatinine = $('.creatinine').val();
        }
        var uric_acid = $('.uric_acid1').val();
        if(uric_acid==""){
            uric_acid = $('.uric_acid').val();
        }
        var calcium = $('.calcium1').val();
        if(calcium==""){
            calcium = $('.calcium').val();
        }
        var phosphorus = $('.phosphorus1').val();
        if(phosphorus==""){
            phosphorus = $('.phosphorus').val();
        }
        var sodium = $('.sodium1').val();
        if(sodium==""){
            sodium = $('.sodium').val();
        }
        var potassium = $('.potassium1').val();
        if(potassium==""){
            potassium = $('.potassium').val();
        }
        var chloride = $('.chloride1').val();
        if(chloride==""){
            chloride = $('.chloride').val();
        }
        
        var renal_date = $('.renal_date1').val();
        if(renal_date==""){
            renal_date = $('.renal_date').val();
        }
        var regid = $('.regid').val();
        var renal_rand = $('.renal_rand').val();
        var nnotes = $('.nnotes').val();
        
        var notes = 'Renal Profile : \n B.U.N:'+bun+', Urea :'+urea+', Creatinine :'+creatinine+', Uric Acid :'+uric_acid+', Calcium :'+calcium+', Phosphorus :'+phosphorus+', Sodium :'+sodium+', Potassium :'+potassium+', Chloride :'+chloride+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Renalreport!=""){
            foreach($Renalreport as $Renalreportval){
                $pdate = $Renalreportval->dateval;
                $deleted = $Renalreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(bun!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/renaldetail') }}",
                            data: { regid:regid, renal_date:renal_date,bun:bun, urea:urea, creatinine:creatinine, uric_acid:uric_acid, calcium:calcium, phosphorus:phosphorus, sodium:sodium,potassium:potassium,chloride:chloride, renal_rand:renal_rand}, 
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var rid =  $('.renalid').val();
                 var bun_1 = $('.bun').val();
                 var urea_1 = $('.urea').val();
                 var creatinine_1 = $('.creatinine').val();
                 var uric_acid_1 = $('.uric_acid').val();
                 var calcium_1 = $('.calcium').val();
                 var phosphorus_1 = $('.phosphorus').val();
                 var sodium_1 = $('.sodium').val();
                 var potassium_1 = $('.potassium').val();
                 var chloride_1 = $('.chloride').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updaterenaldetail') }}",
                        data: { id:rid, regid:regid, renal_date:renal_date,bun:bun_1, urea:urea_1, creatinine:creatinine_1, uric_acid:uric_acid_1, calcium:calcium_1, phosphorus:phosphorus_1, sodium:sodium_1,potassium:potassium_1,chloride:chloride_1, renal_rand:renal_rand},  
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'Renal Profile : \n B.U.N:'+bun_1+', Urea :'+urea_1+', Creatinine :'+creatinine_1+', Uric Acid :'+uric_acid_1+', Calcium :'+calcium_1+', Phosphorus :'+phosphorus_1+', Sodium :'+sodium_1+', Potassium :'+potassium_1+', Chloride :'+chloride_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });


    // Stool Profile
    $(".savestool").on("click", function(){
        var stool_date = $('.stool_date').val();
        var color = $('.color').val();
        var consistency = $('.consistency').val();
        var mucus = $('.mucus').val();
        var frank_blood = $('.frank_blood').val();
        var adult_worm = $('.adult_worm').val();
        var reaction = $('.reaction').val();
        var pus_cells = $('.pus_cells').val();
        var occult_blood = $('.occult_blood').val();
        var macrophages = $('.macrophages').val();
        var rbc = $('.rbc').val();
        var ova = $('.ova').val();
        var cysts = $('.cysts').val();
        var protozoa = $('.protozoa').val();
        var yeast_cell = $('.yeast_cell').val();
        var other_findings = $('.other_findings').val();
        var stool_rand = $('.stool_rand').val();
        var regid = $('.regid').val();
        var id =  $('.stoolid').val();
        var already = "";
        <?php
        if($Stoolreport!=""){
            foreach($Stoolreport as $Stoolreportval){
                $pdate = $Stoolreportval->dateval;
                $deleted = $Stoolreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(color!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/stooldetail') }}",
                data: { regid:regid, stool_date:stool_date,color:color, consistency:consistency, mucus:mucus, frank_blood:frank_blood, adult_worm:adult_worm, reaction:reaction, pus_cells:pus_cells,occult_blood:occult_blood,macrophages:macrophages, rbc:rbc, ova:ova, cysts:cysts, protozoa:protozoa, yeast_cell:yeast_cell, other_findings:other_findings, stool_rand:stool_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.stool_date1').val(stool_date);$('.color1').val(color);$('.consistency1').val(consistency);$('.mucus1').val(mucus);$('.frank_blood1').val(frank_blood);$('.adult_worm1').val(adult_worm);$('.reaction1').val(reaction);$('.pus_cells1').val(pus_cells);$('.occult_blood1').val(occult_blood);$('.macrophages1').val(macrophages);$('.rbc1').val(rbc);$('.ova1').val(ova);$('.cysts1').val(cysts);$('.protozoa1').val(protozoa);$('.yeast_cell1').val(yeast_cell);$('.other_findings1').val(other_findings);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatestooldetail') }}",
                data: { id:id, regid:regid, stool_date:stool_date,color:color, consistency:consistency, mucus:mucus, frank_blood:frank_blood, adult_worm:adult_worm, reaction:reaction, pus_cells:pus_cells,occult_blood:occult_blood,macrophages:macrophages, rbc:rbc, ova:ova, cysts:cysts, protozoa:protozoa, yeast_cell:yeast_cell, other_findings:other_findings, stool_rand:stool_rand},
                success: function( msg ) {
                 
                   $('.stool_date1').val(stool_date);$('.color1').val(color);$('.consistency1').val(consistency);$('.mucus1').val(mucus);$('.frank_blood1').val(frank_blood);$('.adult_worm1').val(adult_worm);$('.reaction1').val(reaction);$('.pus_cells1').val(pus_cells);$('.occult_blood1').val(occult_blood);$('.macrophages1').val(macrophages);$('.rbc1').val(rbc);$('.ova1').val(ova);$('.cysts1').val(cysts);$('.protozoa1').val(protozoa);$('.yeast_cell1').val(yeast_cell);$('.other_findings1').val(other_findings);

                   alert("Details Update");
                   
                }
            });
    }
    });

$(".savefollowstool").on("click", function(){
        var color = $('.color1').val();
        if(color==""){
            color = $('.color').val();
        }
        var consistency = $('.consistency1').val();
        if(consistency==""){
            consistency = $('.consistency').val();
        }
        var mucus = $('.mucus1').val();
        if(mucus==""){
            mucus = $('.mucus').val();
        }
        var frank_blood = $('.frank_blood1').val();
        if(frank_blood==""){
            frank_blood = $('.frank_blood').val();
        }
        var adult_worm = $('.adult_worm1').val();
        if(adult_worm==""){
            adult_worm = $('.adult_worm').val();
        }
        var reaction = $('.reaction1').val();
        if(reaction==""){
            reaction = $('.reaction').val();
        }
        var pus_cells = $('.pus_cells1').val();
        if(pus_cells==""){
            pus_cells = $('.pus_cells').val();
        }
        var occult_blood = $('.occult_blood1').val();
        if(occult_blood==""){
            occult_blood = $('.occult_blood').val();
        }
        var macrophages = $('.macrophages1').val();
        if(macrophages==""){
            macrophages = $('.macrophages').val();
        }
         var rbc = $('.rbc1').val();
        if(rbc==""){
            rbc = $('.rbc').val();
        }
         var ova = $('.ova1').val();
        if(ova==""){
            ova = $('.ova').val();
        }
         var cysts = $('.cysts1').val();
        if(cysts==""){
            cysts = $('.cysts').val();
        }
         var protozoa = $('.protozoa1').val();
        if(protozoa==""){
            protozoa = $('.protozoa').val();
        }
        var yeast_cell = $('.yeast_cell1').val();
        if(yeast_cell==""){
            yeast_cell = $('.yeast_cell').val();
        }
        var other_findings = $('.other_findings1').val();
        if(other_findings==""){
            other_findings = $('.other_findings').val();
        }

        
        var stool_date = $('.stool_date1').val();
        if(stool_date==""){
            stool_date = $('.stool_date').val();
        }
        var regid = $('.regid').val();
        var stool_rand = $('.stool_rand').val();
        var nnotes = $('.nnotes').val();
        
        var notes = 'Stool Profile : \n Color:'+color+', Consistency :'+consistency+', Mucus :'+mucus+', Frank Blood :'+frank_blood+', Adult Worm :'+adult_worm+', Reaction :'+reaction+', Pus Cells :'+pus_cells+', Occult blood :'+occult_blood+', Macrophages :'+macrophages+', R.B.C :'+rbc+', Ova :'+ova+', Cysts :'+cysts+', Protozoa :'+protozoa+', Yeast Cells :'+yeast_cell+', Other Findings :'+other_findings+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Stoolreport!=""){
            foreach($Stoolreport as $Stoolreportval){
                $pdate = $Stoolreportval->dateval;
                $deleted = $Stoolreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(color!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/stooldetail') }}",
                            data: { regid:regid, stool_date:stool_date,color:color, consistency:consistency, mucus:mucus, frank_blood:frank_blood, adult_worm:adult_worm, reaction:reaction, pus_cells:pus_cells,occult_blood:occult_blood,macrophages:macrophages, rbc:rbc, ova:ova, cysts:cysts, protozoa:protozoa, yeast_cell:yeast_cell, other_findings:other_findings, stool_rand:stool_rand}, 
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var stid =  $('.stoolid').val();
                 var color_1 = $('.color').val();
                 var consistency_1 = $('.consistency').val();
                 var mucus_1 = $('.mucus').val();
                 var frank_blood_1 = $('.frank_blood').val();
                 var adult_worm_1 = $('.adult_worm').val();
                 var reaction_1 = $('.reaction').val();
                 var pus_cells_1 = $('.pus_cells').val();
                 var occult_blood_1 = $('.occult_blood').val();
                 var macrophages_1 = $('.macrophages').val();
                 var rbc_1 = $('.rbc').val();
                 var cysts_1 = $('.cysts').val();
                 var ova_1 = $('.ova').val();
                 var protozoa_1 = $('.protozoa').val();
                 var yeast_cell_1 = $('.yeast_cell').val();
                 var other_findings_1 = $('.other_findings').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatestooldetail') }}",
                        data: { id:stid, regid:regid, stool_date:stool_date,color:color_1, consistency:consistency_1, mucus:mucus_1, frank_blood:frank_blood_1, adult_worm:adult_worm_1, reaction:reaction_1, pus_cells:pus_cells_1,occult_blood:occult_blood_1,macrophages:macrophages_1, rbc:rbc_1, ova:ova_1, cysts:cysts_1, protozoa:protozoa_1, yeast_cell:yeast_cell_1, other_findings:other_findings_1, stool_rand:stool_rand}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'Stool Profile : \n Color:'+color_1+', Consistency :'+consistency_1+', Mucus :'+mucus_1+', Frank Blood :'+frank_blood_1+', Adult Worm :'+adult_worm_1+', Reaction :'+reaction_1+', Pus Cells :'+pus_cells_1+', Occult blood :'+occult_blood_1+', Macrophages :'+macrophages_1+', R.B.C :'+rbc_1+', Ova :'+ova_1+', Cysts :'+cysts_1+', Protozoa :'+protozoa_1+', Yeast Cells :'+yeast_cell_1+', Other Findings :'+other_findings_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // Endocrine Profile
    $(".saveed").on("click", function(){
        var ed_date = $('.ed_date').val();
        var t3 = $('.t_3').val();
        var t4 = $('.t_4').val();
        var tsh = $('.tsh').val();
        var ft3 = $('.ft_3').val();

        var ft4 = $('.ft_4').val();
        var anti_tpo = $('.anti_tpo').val();
        var antibody = $('.antibody').val();
        var prolactin = $('.prolactin').val();
        var fsh = $('.fsh').val();
        var lsh = $('.lsh').val();
        var progesterone_3 = $('.progesterone_3').val();
        var progesterone = $('.progesterone').val();
        var dhea = $('.dhea').val();
        var testosterone = $('.testosterone').val();
        var ama = $('.ama').val();
        var insulin = $('.insulin').val();
        var glucose = $('.glucose').val();
        var ed_rand = $('.ed_rand').val();
        var regid = $('.regid').val();
        var id =  $('.erid').val();
        var already = "";
        <?php
        if($Endocrinereport!=""){
            foreach($Endocrinereport as $Endocrinereportval){
                $pdate = $Endocrinereportval->dateval;
                $deleted = $Endocrinereportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(t3!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/endocrinedetail') }}",
                data: { regid:regid, ed_date:ed_date,t3:t3, t4:t4, tsh:tsh, ft3:ft3, ft4:ft4, anti_tpo:anti_tpo, antibody:antibody,prolactin:prolactin,fsh:fsh, lsh:lsh, progesterone_3:progesterone_3, progesterone:progesterone, dhea:dhea, testosterone:testosterone, ama:ama, insulin:insulin, glucose:glucose ,ed_rand:ed_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.ed_date1').val(ed_date);$('.t31').val(t3);$('.t41').val(t4);$('.tsh1').val(tsh);$('.ft31').val(ft3);$('.ft41').val(ft4);$('.anti_tpo1').val(anti_tpo);$('.antibody1').val(antibody);$('.prolactin1').val(prolactin);$('.fsh1').val(fsh);$('.lsh1').val(lsh);$('.progesterone_31').val(progesterone_3);$('.progesterone1').val(progesterone);$('.dhea1').val(dhea);$('.testosterone1').val(testosterone);$('.ama1').val(ama);$('.insulin1').val(insulin);$('.glucose1').val(glucose);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateendocrinedetail') }}",
                data: {id:id, regid:regid, ed_date:ed_date,t3:t3, t4:t4, tsh:tsh, ft3:ft3, ft4:ft4, anti_tpo:anti_tpo, antibody:antibody,prolactin:prolactin,fsh:fsh, lsh:lsh, progesterone_3:progesterone_3, progesterone:progesterone, dhea:dhea, testosterone:testosterone, ama:ama, insulin:insulin, glucose:glucose ,ed_rand:ed_rand}, 
                success: function( msg ) {
                 
                   $('.ed_date1').val(ed_date);$('.t31').val(t3);$('.t41').val(t4);$('.tsh1').val(tsh);$('.ft31').val(ft3);$('.ft41').val(ft4);$('.anti_tpo1').val(anti_tpo);$('.antibody1').val(antibody);$('.prolactin1').val(prolactin);$('.fsh1').val(fsh);$('.lsh1').val(lsh);$('.progesterone_31').val(progesterone_3);$('.progesterone1').val(progesterone);$('.dhea1').val(dhea);$('.testosterone1').val(testosterone);$('.ama1').val(ama);$('.insulin1').val(insulin);$('.glucose1').val(glucose);

                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowed").on("click", function(){
        var t3 = $('.t_31').val();
        if(t3==""){
            t3 = $('.t_3').val();
        }
        var t4 = $('.t_41').val();
        if(t4==""){
            t4 = $('.t_4').val();
        }
        var tsh = $('.tsh1').val();
        if(tsh==""){
            tsh = $('.tsh').val();
        }
        var ft3 = $('.ft_31').val();
        if(ft3==""){
            ft3 = $('.ft_3').val();
        }
        var ft4 = $('.ft_41').val();
        if(ft4==""){
            ft4 = $('.ft_4').val();
        }
        var anti_tpo = $('.anti_tpo1').val();
        if(anti_tpo==""){
            anti_tpo = $('.anti_tpo').val();
        }
        var antibody = $('.antibody1').val();
        if(antibody==""){
            antibody = $('.antibody').val();
        }
        var prolactin = $('.prolactin1').val();
        if(prolactin==""){
            prolactin = $('.prolactin').val();
        }
        var fsh = $('.fsh1').val();
        if(fsh==""){
            fsh = $('.fsh').val();
        }
         var lsh = $('.lsh1').val();
        if(lsh==""){
            lsh = $('.lsh').val();
        }
         var progesterone_3 = $('.progesterone_31').val();
        if(progesterone_3==""){
            progesterone_3 = $('.progesterone_3').val();
        }
         var progesterone = $('.progesterone1').val();
        if(progesterone==""){
            progesterone = $('.progesterone').val();
        }
         var dhea = $('.dhea1').val();
        if(dhea==""){
            dhea = $('.dhea').val();
        }
        var testosterone = $('.testosterone1').val();
        if(testosterone==""){
            testosterone = $('.testosterone').val();
        }
        var ama = $('.ama1').val();
        if(ama==""){
            ama = $('.ama').val();
        }
        var insulin = $('.insulin1').val();
        if(insulin==""){
            insulin = $('.insulin').val();
        }
        var glucose = $('.glucose1').val();
        if(glucose==""){
            glucose = $('.glucose').val();
        }
        
        var ed_date = $('.ed_date').val();
        if(ed_date==""){
            ed_date = $('.ed_date').val();
        }
        var regid = $('.regid').val();
        var ed_rand = $('.ed_rand').val();
        var nnotes = $('.nnotes').val();
        
        var notes = 'Endocrine Profile : \n T3:'+t3+', T4 :'+t4+', TSH :'+tsh+', FT3 :'+ft3+', FT4 :'+ft4+', Anti TPO :'+anti_tpo+', Antibody Thyroglobulin :'+antibody+', Prolactin :'+prolactin+', FSH Day3 :'+fsh+', LSH Surge :'+lsh+', Progesterone Day3 :'+progesterone_3+', Progesterone :'+progesterone+', DHEA-S :'+dhea+', Testosterone :'+testosterone+', AMA :'+ama+', Fasting Insulin :'+insulin+', Fasting Glucose :'+glucose+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Endocrinereport!=""){
            foreach($Endocrinereport as $Endocrinereportval){
                $pdate = $Endocrinereportval->dateval;
                $deleted = $Endocrinereportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(t3!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/endocrinedetail') }}",
                            data: { regid:regid, ed_date:ed_date,t3:t3, t4:t4, tsh:tsh, ft3:ft3, ft4:ft4, anti_tpo:anti_tpo, antibody:antibody,prolactin:prolactin,fsh:fsh, lsh:lsh, progesterone_3:progesterone_3, progesterone:progesterone, dhea:dhea, testosterone:testosterone, ama:ama, insulin:insulin, glucose:glucose ,ed_rand:ed_rand}, 
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var eid =  $('.erid').val();
                 var t3_1 = $('.t_3').val();
                 var t4_1 = $('.t_4').val();
                 var tsh_1 = $('.tsh').val();
                 var ft3_1 = $('.ft_3').val();
                 var ft4_1 = $('.ft_4').val();
                 var anti_tpo_1 = $('.anti_tpo').val();
                 var antibody_1 = $('.antibody').val();
                 var prolactin_1 = $('.prolactin').val();
                 var fsh_1 = $('.fsh').val();
                 var lsh_1 = $('.lsh').val();
                 var progesterone_3_1 = $('.progesterone_3').val();
                 var progesterone_1 = $('.progesterone').val();
                 var dhea_1 = $('.dhea').val();
                 var testosterone_1 = $('.testosterone').val();
                 var ama_1 = $('.ama').val();
                 var insulin_1 = $('.insulin').val();
                 var glucose_1 = $('.glucose').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updateendocrinedetail') }}",
                        data: {id:id, regid:regid, ed_date:ed_date,t3:t3_1, t4:t4_1, tsh:tsh_1, ft3:ft3_1, ft4:ft4_1, anti_tpo:anti_tpo_1, antibody:antibody_1,prolactin:prolactin_1,fsh:fsh_1, lsh:lsh_1, progesterone_3:progesterone_3_1, progesterone:progesterone_1, dhea:dhea_1, testosterone:testosterone_1, ama:ama_1, insulin:insulin_1, glucose:glucose_1 ,ed_rand:ed_rand}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'Endocrine Profile : \n T3:'+t3_1+', T4 :'+t4_1+', TSH :'+tsh_1+', FT3 :'+ft3_1+', FT4 :'+ft4_1+', Anti TPO :'+anti_tpo_1+', Antibody Thyroglobulin :'+antibody_1+', Prolactin :'+prolactin_1+', FSH Day3 :'+fsh_1+', LSH Surge :'+lsh_1+', Progesterone Day3 :'+progesterone_3_1+', Progesterone :'+progesterone_1+', DHEA-S :'+dhea_1+', Testosterone :'+testosterone_1+', AMA :'+ama_1+', Fasting Insulin :'+insulin_1+', Fasting Glucose :'+glucose_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // Uriine Profile
    $(".saveurine").on("click", function(){
        var urine_date = $('.urine_date').val();
        var color = $('.color_u').val();
        var quantity = $('.quantity').val();
        var appearance = $('.appearance').val();
        var gra = $('.gra').val();
        var protein = $('.protein').val();
        var reaction = $('.reaction').val();
        var pus_cells = $('.pus_cells').val();
        var occult_blood = $('.occult_blood').val();
        var sugar = $('.sugar').val();
        var rbc = $('.rbc').val();
        var acetone = $('.acetone').val();
        var pigments = $('.pigments').val();
        var urobilinogen = $('.urobilinogen').val();
        var epith_cells = $('.epith_cells').val();
        var other_findings = $('.other_findings').val();
        var urine_rand = $('.urine_rand').val();
        var regid = $('.regid').val();
        var id =  $('.urineid').val();
        var already = "";
        <?php
        if($Urinereport!=""){
            foreach($Urinereport as $Urinereportval){
                $pdate = $Urinereportval->dateval;
                $deleted = $Urinereportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(quantity!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/urinedetail') }}",
                data: { regid:regid, urine_date:urine_date,color:color, quantity:quantity, appearance:appearance, gra:gra, protein:protein, reaction:reaction, pus_cells:pus_cells,occult_blood:occult_blood,sugar:sugar, rbc:rbc, acetone:acetone, pigments:pigments, urobilinogen:urobilinogen, epith_cells:epith_cells, other_findings:other_findings, urine_rand:urine_rand}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.urine_date1').val(urine_date);$('.color1').val(color);$('.quantity1').val(quantity);$('.appearance1').val(appearance);$('.gra1').val(gra);$('.protein1').val(protein);$('.reaction1').val(reaction);$('.pus_cells1').val(pus_cells);$('.occult_blood1').val(occult_blood);$('.sugar1').val(sugar);$('.rbc1').val(rbc);$('.acetone1').val(acetone);$('.pigments1').val(pigments);$('.urobilinogen1').val(urobilinogen);$('.epith_cells1').val(epith_cells);$('.other_findings1').val(other_findings);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updateurinedetail') }}",
                data: {id:id, regid:regid, urine_date:urine_date,color:color, quantity:quantity, appearance:appearance, gra:gra, protein:protein, reaction:reaction, pus_cells:pus_cells,occult_blood:occult_blood,sugar:sugar, rbc:rbc, acetone:acetone, pigments:pigments, urobilinogen:urobilinogen, epith_cells:epith_cells, other_findings:other_findings, urine_rand:urine_rand},
                success: function( msg ) {
                 
                   $('.urine_date1').val(urine_date);$('.color1').val(color);$('.quantity1').val(quantity);$('.appearance1').val(appearance);$('.gra1').val(gra);$('.protein1').val(protein);$('.reaction1').val(reaction);$('.pus_cells1').val(pus_cells);$('.occult_blood1').val(occult_blood);$('.sugar1').val(sugar);$('.rbc1').val(rbc);$('.acetone1').val(acetone);$('.pigments1').val(pigments);$('.urobilinogen1').val(urobilinogen);$('.epith_cells1').val(epith_cells);$('.other_findings1').val(other_findings);

                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowurine").on("click", function(){
        var color = $('.color1').val();
        if(color==""){
            color = $('.color_u').val();
        }
        var quantity = $('.quantity1').val();
        if(quantity==""){
            quantity = $('.quantity').val();
        }
        var appearance = $('.appearance1').val();
        if(appearance==""){
            appearance = $('.appearance').val();
        }
        var gra = $('.gra1').val();
        if(gra==""){
            gra = $('.gra').val();
        }
        var protein = $('.protein1').val();
        if(protein==""){
            protein = $('.protein').val();
        }
        var reaction = $('.reaction1').val();
        if(reaction==""){
            reaction = $('.reaction').val();
        }
        var pus_cells = $('.pus_cells1').val();
        if(pus_cells==""){
            pus_cells = $('.pus_cells').val();
        }
        var occult_blood = $('.occult_blood1').val();
        if(occult_blood==""){
            occult_blood = $('.occult_blood').val();
        }
        var sugar = $('.sugar1').val();
        if(sugar==""){
            sugar = $('.sugar').val();
        }
         var rbc = $('.rbc1').val();
        if(rbc==""){
            rbc = $('.rbc').val();
        }
         var acetone = $('.acetone1').val();
        if(acetone==""){
            acetone = $('.acetone').val();
        }
         var pigments = $('.pigments1').val();
        if(pigments==""){
            pigments = $('.pigments').val();
        }
         var urobilinogen = $('.urobilinogen1').val();
        if(urobilinogen==""){
            urobilinogen = $('.urobilinogen').val();
        }
        var epith_cells = $('.epith_cells1').val();
        if(epith_cells==""){
            epith_cells = $('.epith_cells').val();
        }
        var other_findings = $('.other_findings1').val();
        if(other_findings==""){
            other_findings = $('.other_findings').val();
        }

        
        var urine_date = $('.urine_date1').val();
        if(urine_date==""){
            urine_date = $('.urine_date').val();
        }
        var regid = $('.regid').val();
        var urine_rand = $('.urine_rand').val();
        var nnotes = $('.nnotes').val();
        
        var notes = 'Urine Profile : \n Color:'+color+', Quantity :'+quantity+', Appearance :'+appearance+', Specific GRA: :'+gra+', Protein :'+protein+', Reaction :'+reaction+', Pus Cells :'+pus_cells+', Occult blood :'+occult_blood+', Sugar :'+sugar+', R.B.C :'+rbc+', Acetone Bodies: :'+acetone+', Bile Pigments :'+pigments+', Urobilinogen :'+urobilinogen+', Epith Cells :'+epith_cells+', Other Findings :'+other_findings+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Urinereport!=""){
            foreach($Urinereport as $Urinereportval){
                $pdate = $Urinereportval->dateval;
                $deleted = $Urinereportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(color!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/urinedetail') }}",
                            data: { regid:regid, urine_date:urine_date,color:color, quantity:quantity, appearance:appearance, gra:gra, protein:protein, reaction:reaction, pus_cells:pus_cells,occult_blood:occult_blood,sugar:sugar, rbc:rbc, acetone:acetone, pigments:pigments, urobilinogen:urobilinogen, epith_cells:epith_cells, other_findings:other_findings, urine_rand:urine_rand}, 
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var uid =  $('.urineid').val();
                 var color_1 = $('.color_u').val();
                 var quantity_1 = $('.quantity').val();
                 var appearance_1 = $('.appearance').val();
                 var gra_1 = $('.gra').val();
                 var protein_1 = $('.protein').val();
                 var reaction_1 = $('.reaction').val();
                 var pus_cells_1 = $('.pus_cells').val();
                 var occult_blood_1 = $('.occult_blood').val();
                 var sugar_1 = $('.sugar').val();
                 var rbc_1 = $('.rbc').val();
                 var acetone_1 = $('.acetone').val();
                 var pigments_1 = $('.pigments').val();
                 var urobilinogen_1 = $('.urobilinogen').val();
                 var epith_cells_1 = $('.epith_cells').val();
                 var other_findings_1 = $('.other_findings').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updateurinedetail') }}",
                        data: {id:uid, regid:regid, urine_date:urine_date,color:color_1, quantity:quantity_1, appearance:appearance_1, gra:gra_1, protein:protein_1, reaction:reaction_1, pus_cells:pus_cells_1,occult_blood:occult_blood_1,sugar:sugar_1, rbc:rbc_1, acetone:acetone_1, pigments:pigments_1, urobilinogen:urobilinogen_1, epith_cells:epith_cells_1, other_findings:other_findings, urine_rand:urine_rand}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'Urine Profile : \n Color:'+color_1+', Quantity :'+quantity_1+', Appearance :'+appearance_1+', Specific GRA: :'+gra_1+', Protein :'+protein_1+', Reaction :'+reaction_1+', Pus Cells :'+pus_cells_1+', Occult blood :'+occult_blood_1+', Sugar :'+sugar_1+', R.B.C :'+rbc_1+', Acetone Bodies: :'+acetone_1+', Bile Pigments :'+pigments_1+', Urobilinogen :'+urobilinogen_1+', Epith Cells :'+epith_cells_1+', Other Findings :'+other_findings_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });

    // CBC Profile
    $(".savecbc").on("click", function(){
        var cbc_date = $('.cbc_date').val();
        var hb = $('.hb_1').val();
        var rbc = $('.rbc_c').val();
        var wbc = $('.wbc').val();
        var vitamind = $('.vitamind').val();
        var vitaminb = $('.vitaminb').val();
        var platelets = $('.platelets').val();
        var lymphocytes = $('.lymphocytes').val();
        var eosinophils = $('.eosinophils').val();
        var monocytes = $('.monocytes').val();
        var basophils = $('.basophils').val();
        var band_cells = $('.band_cells').val();
        var abnor_rbc = $('.abnor_rbc').val();
        var abnor_wbc = $('.abnor_wbc').val();
        var parasites = $('.parasites').val();
        var esr = $('.esr').val();
        var neutrophils = $('.neutrophils').val();
        var cbc_rand = $('.cbc_rand').val();
        var regid = $('.regid').val();
        var id =  $('.cbcid').val();
        var already = "";
        <?php
        if($Cbcreport!=""){
            foreach($Cbcreport as $Cbcreportval){
                $pdate = $Cbcreportval->dateval;
                $deleted = $Cbcreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
          if(already =="" ){
         if(hb!=""){

            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/cbcdetail') }}",
                data: { regid:regid, cbc_date:cbc_date,hb:hb, wbc:wbc, vitamind:vitamind, vitaminb:vitaminb, platelets:platelets, lymphocytes:lymphocytes, eosinophils:eosinophils,monocytes:monocytes,basophils:basophils, rbc:rbc, band_cells:band_cells, abnor_rbc:abnor_rbc, abnor_wbc:abnor_wbc, parasites:parasites, esr:esr, cbc_rand:cbc_rand, neutrophils:neutrophils}, 
                success: function( msg ) {
                   
                    if(msg=="1"){
                 
                    $('.cbc_date1').val(cbc_date);$('.hb1').val(hb);$('.wbc1').val(wbc);$('.vitamind1').val(vitamind);$('.vitaminb1').val(vitaminb);$('.platelets1').val(platelets);$('.lymphocytes1').val(lymphocytes);$('.eosinophils1').val(eosinophils);$('.monocytes1').val(monocytes);$('.basophils1').val(basophils);$('.rbc1').val(rbc);$('.band_cells1').val(band_cells);$('.abnor_rbc1').val(abnor_rbc);$('.abnor_wbc1').val(abnor_wbc);$('.parasites1').val(parasites);$('.esr1').val(esr);$('.neutrophils1').val(neutrophils);
                    alert("Details Save");
                    }

                    else {
                        alert("Already added.");
                    }
                                 
                }
            });
           
        }else {
            alert('All fields required.'); 
        }
    }
    else{
        $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/updatecbcdetail') }}",
                data: { id:id, regid:regid, cbc_date:cbc_date,hb:hb, wbc:wbc, vitamind:vitamind, vitaminb:vitaminb, platelets:platelets, lymphocytes:lymphocytes, eosinophils:eosinophils,monocytes:monocytes,basophils:basophils, rbc:rbc, band_cells:band_cells, abnor_rbc:abnor_rbc, abnor_wbc:abnor_wbc, parasites:parasites, esr:esr, cbc_rand:cbc_rand, neutrophils:neutrophils}, 
                success: function( msg ) {
                 
                   $('.cbc_date1').val(cbc_date);$('.hb1').val(hb);$('.wbc1').val(wbc);$('.vitamind1').val(vitamind);$('.vitaminb1').val(vitaminb);$('.platelets1').val(platelets);$('.lymphocytes1').val(lymphocytes);$('.eosinophils1').val(eosinophils);$('.monocytes1').val(monocytes);$('.basophils1').val(basophils);$('.rbc1').val(rbc);$('.band_cells1').val(band_cells);$('.abnor_rbc1').val(abnor_rbc);$('.abnor_wbc1').val(abnor_wbc);$('.parasites1').val(parasites);$('.esr1').val(esr);$('.neutrophils1').val(neutrophils);

                   alert("Details Update");
                   
                }
            });
    }
    });

    $(".savefollowcbc").on("click", function(){
        var hb = $('.hb_1').val();
        if(hb==""){
            hb = $('.hb').val();
        }
        var wbc = $('.wbc1').val();
        if(wbc==""){
            wbc = $('.wbc').val();
        }
        var vitamind = $('.vitamind1').val();
        if(vitamind==""){
            vitamind = $('.vitamind').val();
        }
        var vitaminb = $('.vitaminb1').val();
        if(vitaminb==""){
            vitaminb = $('.vitaminb').val();
        }
        var platelets = $('.platelets1').val();
        if(platelets==""){
            platelets = $('.platelets').val();
        }
        var lymphocytes = $('.lymphocytes1').val();
        if(lymphocytes==""){
            lymphocytes = $('.lymphocytes').val();
        }
        var eosinophils = $('.eosinophils1').val();
        if(eosinophils==""){
            eosinophils = $('.eosinophils').val();
        }
        var monocytes = $('.monocytes1').val();
        if(monocytes==""){
            monocytes = $('.monocytes').val();
        }
        var basophils = $('.basophils1').val();
        if(basophils==""){
            basophils = $('.basophils').val();
        }
         var rbc = $('.rbc1').val();
        if(rbc==""){
            rbc = $('.rbc_c').val();
        }
         var band_cells = $('.band_cells1').val();
        if(band_cells==""){
            band_cells = $('.band_cells').val();
        }
         var abnor_rbc = $('.abnor_rbc1').val();
        if(abnor_rbc==""){
            abnor_rbc = $('.abnor_rbc').val();
        }
         var abnor_wbc = $('.abnor_wbc1').val();
        if(abnor_wbc==""){
            abnor_wbc = $('.abnor_wbc').val();
        }
        var parasites = $('.parasites1').val();
        if(parasites==""){
            parasites = $('.parasites').val();
        }
        var esr = $('.esr1').val();
        if(esr==""){
            esr = $('.esr').val();
        }
        var neutrophils = $('.neutrophils1').val();
        if(neutrophils==""){
            neutrophils = $('.neutrophils').val();
        }
        var cbc_date = $('.cbc_date1').val();
        if(cbc_date==""){
            cbc_date = $('.cbc_date').val();
        }
        var regid = $('.regid').val();
        var cbc_rand = $('.cbc_rand').val();
        var nnotes = $('.nnotes').val();
        
        var notes = 'CBC Profile : \n HB:'+hb+', RBC :'+rbc+', WBC :'+wbc+', Platelets: :'+platelets+', Vitamin B-12  :'+vitaminb+', 25-Oh Vitamin D(Total) :'+vitamind+', Neutrophils :'+neutrophils+', Lymphocytes :'+lymphocytes+', Eosinophils :'+eosinophils+', Monocytes :'+monocytes+', Basophils :'+basophils+', Band Cells :'+band_cells+', Abnor R.B.C :'+abnor_rbc+', Parasites :'+parasites+', Abnor W.B.C :'+abnor_wbc+', ESR:'+esr+'\n'+nnotes+'\n';
        var notes_type = "other";

        var id = $('.nid').val();
        var already = "";
        <?php
        if($Cbcreport!=""){
            foreach($Cbcreport as $Cbcreportval){
                $pdate = $Cbcreportval->dateval;
                $deleted = $Cbcreportval->deleted_at;
                $tdate = date('d/m/Y');
                if($pdate == $tdate && $deleted==NULL){?>
                  //  alert("Followup is already added for this date");
                     already = "Already  Added";
            <?php   } } }

         ?>
      
        if(hb!=""){
             if(already==""){

                     $.ajax({
                    type: "POST",
                    url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                    data: { id:id, notes:notes},  
                    success: function( msg ) {

                        $.ajax({
                            type: "POST",
                            url: "{{ url(config('laraadmin.adminRoute') . '/cbcdetail') }}",
                            data: { regid:regid, cbc_date:cbc_date,hb:hb, wbc:wbc, vitamind:vitamind, vitaminb:vitaminb, platelets:platelets, lymphocytes:lymphocytes, eosinophils:eosinophils,monocytes:monocytes,basophils:basophils, rbc:rbc, band_cells:band_cells, abnor_rbc:abnor_rbc, abnor_wbc:abnor_wbc, parasites:parasites, esr:esr, cbc_rand:cbc_rand, neutrophils:neutrophils},  
                            success: function( msg ) {
                               

                            }
                        });
                        setTimeout(function(){// wait for 5 secs(2)
                               location.reload(); // then reload the page.(3)
                          }, 1000);
                    }
                });
             }
             else{
                 var cbid =  $('.cbcid').val();
                 var hb_1 = $('.hb_1').val();
                 var wbc_1 = $('.wbc').val();
                 var platelets_1 = $('.platelets').val();
                 var vitaminb_1 = $('.vitaminb').val();
                 var vitamind_1 = $('.vitamind').val();
                 var lymphocytes_1 = $('.lymphocytes').val();
                 var eosinophils_1 = $('.eosinophils').val();
                 var neutrophils_1 = $('.neutrophils').val();
                 var monocytes_1 = $('.monocytes').val();
                 var basophils_1 = $('.basophils').val();
                 var rbc_1_1 = $('.rbc_c').val();
                 var band_cells_1 = $('.band_cells').val();
                 var abnor_rbc_1 = $('.abnor_rbc').val();
                 var abnor_wbc_1 = $('.abnor_wbc').val();
                 var parasites_1 = $('.parasites').val();
                 var esr_1 = $('.esr').val();
                    $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatecbcdetail') }}",
                        data: { id:cbid, regid:regid, cbc_date:cbc_date,hb:hb_1, wbc:wbc_1, vitamind:vitamind_1, vitaminb:vitaminb_1, platelets:platelets_1, lymphocytes:lymphocytes_1, eosinophils:eosinophils_1,monocytes:monocytes_1,basophils:basophils_1, rbc:rbc_1_1, band_cells:band_cells_1, abnor_rbc:abnor_rbc_1, abnor_wbc:abnor_wbc_1, parasites:parasites_1, esr:esr_1, cbc_rand:cbc_rand, neutrophils:neutrophils}, 
                        success: function( msg ) {
                           

                        }
                    });
                    var notes1 = 'CBC Profile : \n HB:'+hb_1+', RBC :'+rbc_1_1+', WBC :'+wbc_1+', Platelets: :'+platelets_1+', Vitamin B-12  :'+vitaminb_1+', 25-Oh Vitamin D(Total) :'+vitamind_1+', Neutrophils :'+neutrophils_1+', Lymphocytes :'+lymphocytes_1+', Eosinophils :'+eosinophils_1+', Monocytes :'+monocytes_1+', Basophils :'+basophils_1+', Band Cells :'+band_cells_1+', Abnor R.B.C :'+abnor_rbc_1+', Parasites :'+parasites_1+', Abnor W.B.C :'+abnor_wbc_1+', ESR:'+esr_1+'\n'+nnotes+'\n';
                     $.ajax({
                        type: "POST",
                        url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
                        data: { id:id, notes:notes1}, 
                        success: function( msg ) {
                          setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
                    }
                   });
             }
             
            
        }else {
            alert('Fill all fields.'); 
        }
   
    });


});

/*function closemodal(){
        var getfirst = <?php echo Session::forget('rid'); ?>
        
        $('#Additionalcharges').modal('hide');
        setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
          }, 1000);
}*/

function removedata(id){
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/deletedata') }}",
        data: { rand:id}, 
        success: function( msg ) {
        	//console.log(msg);
            $('#'+id).remove();
            $('#'+id).remove();
            setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000);
        }
    });
}

function removecharges(id){
   
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/deletecharges') }}",
        data: { id:id}, 
        success: function( charge ) {
            
            $('#'+id).remove();
            var regularCharge = parseInt($(".charges").html());
            var totalRegularCharge = parseInt(regularCharge) + parseInt(charge);
            $(".additional").html(parseInt(charge));
            $(".total_chg").html(totalRegularCharge);
            
            // setTimeout(function(){// wait for 5 secs(2)
            //                location.reload(); // then reload the page.(3)
            //           }, 1000);
        }
    });
}
function removenewdata(id){
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/deleteheightdata') }}",
        data: { rand:id}, 
        success: function( msg ) {
            $('#'+id).remove();

        }
    });
}

function removecommunication(id){
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/deletecommunication') }}",
        data: { rand:id}, 
        success: function( msg ) {
            $('#'+id).remove();
        }
    });
}

function removeexamination(id){
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/deleteexamination') }}",
        data: { rand:id}, 
        success: function( msg ) {
            $('#'+id).remove();
        }
    });
}

function removeinvestigation(id){
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/deleteinvestigation') }}",
        data: { rand:id}, 
        success: function( msg ) {
            $('#'+id).remove();
        }
    });
}

function removeimage(id){
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/deletepicture') }}",
        data: { rand:id}, 
        success: function( msg ) {
            $('#'+id).remove();

        }
    });
}

 $('#country_name_0').on('keyup',function(){ 
    var query = $(this).val();
    
    if(query != '') {
     var _token = $('input[name="_token"]').val();
     $.ajax({
      url: "{{ url(config('laraadmin.adminRoute') . '/fetch') }}",
      method:"POST",
      data:{query:query, _token:_token},
      success:function(data){
       
       $('#countryList_0').fadeIn();  
       $('#countryList_0').html(data);
       $('#countryList_0').find('.dropdown-menu').attr('id','dropdown-menu_0');
       
       $('#dropdown-menu_0' +'> li').on('click', function(){  
            $('#country_name_0').val($(this).text());
            
            $('#countryList_0').fadeOut();  
        
            var query = $(this).text();
            
            if(query != '')
            {
        
             var _token = $('input[name="_token"]').val();
             $.ajax({
              url: "{{ url(config('laraadmin.adminRoute') . '/fetchmedicine') }}",
              method:"POST",
              data:{query:query, _token:_token},
              success:function(data){
                console.log('medicine desis data:', data);
                $('#medicine_for_0').html(data);
                // $('#medicine_for_0').next('.select2-container--default').remove();
                // $('#medicine_for_0').select2();
              }
             });
            }
        });
      }
     });
    }
  });

$('.extra-fields-customer').click(function() {
  var i = $('.customer_records_cloned').length+1;

  var html = $('.customer_records').clone(true);
  html.removeClass('customer_records').addClass('customer_records_cloned');
  html.addClass('single remove');
  html.find('.extra-fields-customer').remove(); 
  html.append('<a href="#" class="btn btn-danger remove-field btn-remove-customer" style="margin-left: 4px;width: 68px;margin-bottom: 10px;padding: 5px 1px 4px 1px;">Remove</a>');
  //html.find('.customer_records_cloned .single').attr("class", "remove");
  
  html.find('.country_name').unbind('keyup');
  html.find('.country_name').val(null);
  html.find('.medicine_for').val(null);
  html.find('.country_name').attr('id','country_name_'+i);
  html.find('.countryList').attr('id','countryList_'+i);
  html.find('.medicine_for').attr('id','medicine_for_'+i);
  //html.find('.select2-container--default').remove();
  html.find('.medicine_time').attr('id','medicine_time'+i);
  
  
//   $('.customer_records_dynamic .customer_records_cloned').addClass('single remove');
//   $('.single .extra-fields-customer').remove();
//   $('.single').append('<a href="#" class="btn btn-danger remove-field btn-remove-customer" style="margin-left: 4px;width: 68px;margin-bottom: 10px;padding: 5px 1px 4px 1px;">Remove</a>');
//   $('.customer_records_dynamic > .single').attr("class", "remove");


  html.appendTo('.customer_records_dynamic');
  
  $('#country_name_'+i).on('keyup',function(){ 
    var query = $(this).val();
    
    if(query != '') {
     var _token = $('input[name="_token"]').val();
     $.ajax({
      url: "{{ url(config('laraadmin.adminRoute') . '/fetch') }}",
      method:"POST",
      data:{query:query, _token:_token},
      success:function(data){
       
       $('#countryList_'+i).fadeIn();  
       $('#countryList_'+i).html(data);
       $('#countryList_'+i).find('.dropdown-menu').attr('id','dropdown-menu_'+i);
       
       $('#dropdown-menu_'+i +'> li').on('click', function(){  
            $('#country_name_'+i).val($(this).text());
            
            $('#countryList_'+i).fadeOut();  
        
            var query = $(this).text();
            
            if(query != '')
            {
        
             var _token = $('input[name="_token"]').val();
             $.ajax({
              url: "{{ url(config('laraadmin.adminRoute') . '/fetchmedicine') }}",
              method:"POST",
              data:{query:query, _token:_token},
              success:function(data){
               console.log('medicine desis data:', data);
                $('#medicine_for_'+i).html(data);
                //$('#medicine_for_'+i).select2();
                $('#medicine_time_'+i).html(data);
                //$('#medicine_time_'+i).select2();
              }
             });
            }
        });
      }
     });
    }
  });
  
  


  $('.customer_records_dynamic input').each(function() {
    var count = 0;
    var fieldname = $(this).attr("name");
    $(this).attr('name', fieldname + count);
    count++;
  });

});

$(document).on('click', '.remove-field', function(e) {
  $(this).parent('.remove').remove();
  e.preventDefault();
});

// $('.country_name').on('keyup',function(){ 
//     var query = $(this).val();
//     if(query != '')
//     {
//      var _token = $('input[name="_token"]').val();
//      $.ajax({
//       url: "{{ url(config('laraadmin.adminRoute') . '/fetch') }}",
//       method:"POST",
//       data:{query:query, _token:_token},
//       success:function(data){
//       $('.countryList').fadeIn();  
//       $('.countryList').html(data);
//       }
//      });
//     }
// });

// $(document).on('click', 'li', function(){  

//     $('.country_name').val($(this).text());

// //alert($(this).parent().parent().find(".country_name").val($(this).text()));
    
//     $('.countryList').fadeOut();  

//     var query = $(this).text();
    
//     if(query != '')
//     {

//      var _token = $('input[name="_token"]').val();
//      $.ajax({
//       url: "{{ url(config('laraadmin.adminRoute') . '/fetchmedicine') }}",
//       method:"POST",
//       data:{query:query, _token:_token},
//       success:function(data){
//       $('.medicine_for').html(data);
//       }
//      });
//     }
// });

function medicine_price_get(obj){
    var price = $(obj).children('option:selected').attr('data-price');
     $(obj).parent().parent().find(".medicine_price").val(price);
}

function getRelatedDiseases(obj){
    console.log(obj);
    //var medicineId = $(obj).children('option:selected').val();
}

function focusfunction(id){
     $("."+id).focus();
     var rand_id = id;

      
      $(".rxdatavaltable tr").click(function() {
        var selected = $(this).hasClass("highlight");
        //alert(selected);
        $(".rxdatavaltable tr").removeClass("highlight");
        if(!selected ||selected){
                $(this).addClass("highlight");
        }
    });

     //$(".rxdatavaltable tr[id="+id+"]").css("background-color", "grey");
        var rid = id;
        $('.rid').val(id);

       $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/getpotency') }}",
        data: { rand_id:rand_id}, 
        success: function( msg ) {
          // console.log(msg);
           var scheme_case = $(".scheme_case").val();
           if(scheme_case==""){

            var splitted = msg.split("/");
            var total =0;
            var balance = 0;
            var t1 = 0;
            var t2 = 0;
            var t3 = 0;
            t1=splitted[0];
            t2 = splitted[1];
            t3 = splitted[2];
            total = parseInt(t1) + parseInt(t2);

            balance = parseInt(total) - parseInt(t3);
           
            $(".charges").text(splitted[0]);
            $(".additional").text(splitted[1]);
            $(".rece-chg").text(splitted[2]);
            $(".total_chg").text(total);
            $(".total-bal").text(balance);
           }

        }
    });
}

function updatenotes(obj){
    var notes = $(obj).parent().find('.notes1').val();

        var id = $(obj).parent().find('.notesid').val();
        $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/updatenotes') }}",
            data: { id:id, notes:notes}, 
            success: function( msg ) {
                
        }
       });
}

function getmodalview(){
    $.ajax({
        type: "POST",
        url: "{{ url(config('laraadmin.adminRoute') . '/modalview') }}",
        data: { }, 
        success: function( msg ) {
        
        }
    });
}

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
  if(target=="#1a"){

    var dateval=$('.datesearch').val();
    $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchcb') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#cbcdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#hb").val(splitted[0]);
            $("#rbc").val(splitted[1]);
            $("#wbc").val(splitted[2]);
            $("#platelets").val(splitted[3]);
            $("#vitaminb").val(splitted[4]);
            $("#vitamind").val(splitted[5]);
            $("#neutrophils").val(splitted[6]);
            $("#lymphocytes").val(splitted[7]);
            $("#eosinophils").val(splitted[8]);
            $("#monocytes").val(splitted[9]);
            $("#basophils").val(splitted[10]);
            $("#band_cells").val(splitted[11]);
            $("#abnor_rbc").val(splitted[12]);
            $("#abnor_wbc").val(splitted[13]);
            $("#parasites").val(splitted[14]);
            $("#esr").val(splitted[15]);
            $(".savedate1").val(dateval);

        }
        else{

            $("#hb").val('');
            $("#rbc").val('');
            $("#wbc").val('');
            $("#platelets").val('');
            $("#vitaminb").val('');
            $("#vitamind").val('');
            $("#neutrophils").val('');
            $("#lymphocytes").val('');
            $("#eosinophils").val('');
            $("#monocytes").val('');
            $("#basophils").val('');
            $("#band_cells").val('');
            $("#abnor_rbc").val('');
            $("#abnor_wbc").val('');
            $("#parasites").val('');
            $("#esr").val('');
            //alert("No data found for this date");

        }

    }

    });

  }
  if(target=="#2a"){

    var dateval = $('.savedate1').val();    
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchurine') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#urinediv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#quantity").val(splitted[0]);
            $("#color").val(splitted[1]);
            $("#appearance").val(splitted[2]);
            $("#reaction").val(splitted[3]);
            $("#gra").val(splitted[4]);
            $("#protein").val(splitted[5]);
            $("#sugar").val(splitted[6]);
            $("#acetone").val(splitted[7]);
            $("#pigments").val(splitted[8]);
            $("#occult_blood").val(splitted[9]);
            $("#urobilinogen").val(splitted[10]);
            $("#epith_cells").val(splitted[11]);
            $("#pus_cells").val(splitted[12]);
            $("#rbc_urine").val(splitted[13]);
            $("#other_findings").val(splitted[14]);
        }
        else{
            //$('#urinediv').show();
            $("#quantity").val('');
            $("#color").val('');
            $("#appearance").val('');
            $("#reaction").val('');
            $("#gra").val('');
            $("#protein").val('');
            $("#sugar").val('');
            $("#acetone").val('');
            $("#pigments").val('');
            $("#occult_blood").val('');
            $("#urobilinogen").val('');
            $("#epith_cells").val('');
            $("#pus_cells").val('');
            $("#rbc_urine").val('');
            $("#other_findings").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#3a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchstool') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#stooldiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#color_stool").val(splitted[0]);
            $("#consistency").val(splitted[1]);
            $("#mucus").val(splitted[2]);
            $("#frank_blood").val(splitted[3]);
            $("#adult_worm").val(splitted[4]);
            $("#reaction_stool").val(splitted[5]);
            $("#pus_cells_stool").val(splitted[6]);
            $("#occult_blood_stool").val(splitted[7]);
            $("#macrophages").val(splitted[8]);
            $("#rbc_stool").val(splitted[9]);
            $("#ova").val(splitted[10]);
            $("#cysts").val(splitted[11]);
            $("#protozoa").val(splitted[12]);
            $("#yeast_cell").val(splitted[13]);
            $("#other_findings_stool").val(splitted[14]);
        }
        else{
            $("#color_stool").val('');
            $("#consistency").val('');
            $("#mucus").val('');
            $("#frank_blood").val('');
            $("#adult_worm").val('');
            $("#reaction_stool").val('');
            $("#pus_cells_stool").val('');
            $("#occult_blood_stool").val('');
            $("#macrophages").val('');
            $("#rbc_stool").val('');
            $("#ova").val('');
            $("#cysts").val('');
            $("#protozoa").val('');
            $("#yeast_cell").val('');
            $("#other_findings_stool").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#4a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searcharithris') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#arithrisdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#anti_o").val(splitted[0]);
            $("#accp").val(splitted[1]);
            $("#ra_factor").val(splitted[2]);
            $("#alkaline").val(splitted[3]);
            $("#ana").val(splitted[4]);
            $("#c_react").val(splitted[5]);
            $("#complement").val(splitted[6]);
        }
        else{
           
            $("#anti_o").val('');
            $("#accp").val('');
            $("#ra_factor").val('');
            $("#alkaline").val('');
            $("#ana").val('');
            $("#c_react").val('');
            $("#complement").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#5a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchendocrine') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
         $('#endocrinediv').show();
        if(data!=""){
            var splitted = data.split("/");
           
            $("#t3_end").val(splitted[0]);
            $("#t4_end").val(splitted[1]);
            $("#tsh_end").val(splitted[2]);
            $("#ft3").val(splitted[3]);
            $("#ft4").val(splitted[4]);
            $("#anti_tpo").val(splitted[5]);
            $("#antibody").val(splitted[6]);
            $("#prolactin").val(splitted[7]);
            $("#fsh").val(splitted[8]);
            $("#lsh").val(splitted[9]);
            $("#progesterone_3").val(splitted[10]);
            $("#progesterone").val(splitted[11]);
            $("#dhea").val(splitted[12]);
            $("#testosterone").val(splitted[13]);
            $("#ama").val(splitted[14]);
            $("#insulin").val(splitted[15]);
            $("#glucose").val(splitted[16]);
        }
        else{

            $("#t3_end").val('');
            $("#t4_end").val('');
            $("#tsh_end").val('');
            $("#ft3").val('');
            $("#ft4").val('');
            $("#anti_tpo").val('');
            $("#antibody").val('');
            $("#prolactin").val('');
            $("#fsh").val('');
            $("#lsh").val('');
            $("#progesterone_3").val('');
            $("#progesterone").val('');
            $("#dhea").val('');
            $("#testosterone").val('');
            $("#ama").val('');
            $("#insulin").val('');
            $("#glucose").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#6a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchrenal') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#renaldiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#bun").val(splitted[0]);
            $("#urea").val(splitted[1]);
            $("#creatinine").val(splitted[2]);
            $("#uric_acid").val(splitted[3]);
            $("#calcium").val(splitted[4]);
            $("#phosphorus").val(splitted[5]);
            $("#sodium").val(splitted[6]);
            $("#potassium").val(splitted[7]);
            $("#chloride").val(splitted[8]);
        }
        else{

            $("#bun").val('');
            $("#urea").val('');
            $("#creatinine").val('');
            $("#uric_acid").val('');
            $("#calcium").val('');
            $("#phosphorus").val('');
            $("#sodium").val('');
            $("#potassium").val('');
            $("#chloride").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#7a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchxray') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#xraydiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#radiological_report").val(splitted[0]);
        }
        else{
            $("#radiological_report").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#8a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchusgfemale') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#usgfemalediv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#uterues_size").val(splitted[0]);
            $("#thickness").val(splitted[1]);
            $("#fibroids_no").val(splitted[2]);
            $("#description").val(splitted[3]);
            $("#ovary_size_rt").val(splitted[4]);
            $("#ovary_size_lt").val(splitted[5]);
            $("#ovary_volume_rt").val(splitted[6]);
            $("#ovary_volume_lt").val(splitted[7]);
            $("#follicles_lt").val(splitted[8]);
            $("#follicles_rt").val(splitted[9]);
        }
        else{
             $("#uterues_size").val('');
            $("#thickness").val('');
            $("#fibroids_no").val('');
            $("#description").val('');
            $("#ovary_size_rt").val('');
            $("#ovary_size_lt").val('');
            $("#ovary_volume_rt").val('');
            $("#ovary_volume_lt").val('');
            $("#follicles_lt").val('');
            $("#follicles_rt").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#9a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchusgmale') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#usgmalediv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#size").val(splitted[0]);
            $("#volume").val(splitted[1]);
            $("#serum_psa").val(splitted[2]);
            $("#other_findings_male").val(splitted[3]);
        }
        else{
            $("#size").val('');
            $("#volume").val('');
            $("#serum_psa").val('');
            $("#other_findings_male").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

   if(target=="#10a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchimmunlogy') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
       $('#immunlogydiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#igg").val(splitted[0]);
            $("#ige").val(splitted[1]);
            $("#igm").val(splitted[2]);
            $("#iga").val(splitted[3]);
            $("#itg").val(splitted[4]);
        }
        else{
            $("#igg").val('');
            $("#ige").val('');
            $("#igm").val('');
            $("#iga").val('');
            $("#itg").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

   if(target=="#11a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchspecific') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#specificdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#other_findings_specific").val(splitted[0]);
            $("#define_field1").val(splitted[1]);
            $("#define_field2").val(splitted[2]);
            $("#define_field3").val(splitted[3]);
            $("#define_field4").val(splitted[4]);
        }
        else{
            $("#other_findings_specific").val('');
            $("#define_field1").val('');
            $("#define_field2").val('');
            $("#define_field3").val('');
            $("#define_field4").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

   if(target=="#12a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchliver') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#liverdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#total_bil").val(splitted[0]);
            $("#dir_bilirubin").val(splitted[1]);
            $("#ind_bilirubin").val(splitted[2]);
            $("#gamma_gt").val(splitted[3]);
            $("#total_protein").val(splitted[4]);
            $("#albumin").val(splitted[5]);
            $("#globulin").val(splitted[6]);
            $("#sgot").val(splitted[7]);
            $("#sgpt").val(splitted[8]);
            $("#alk_phos").val(splitted[9]);
            $("#aust_antigen").val(splitted[10]);
            $("#amylase").val(splitted[11]);
        }
        else{
           
            $("#total_bil").val('');
            $("#dir_bilirubin").val('');
            $("#ind_bilirubin").val('');
            $("#gamma_gt").val('');
            $("#total_protein").val('');
            $("#albumin").val('');
            $("#globulin").val('');
            $("#sgot").val('');
            $("#sgpt").val('');
            $("#alk_phos").val('');
            $("#aust_antigen").val('');
            $("#amylase").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

    if(target=="#13a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchlipid') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#lipiddiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#total_cholesterol").val(splitted[0]);
            $("#triglycerides_lipid").val(splitted[1]);
            $("#hdl_cholesterol").val(splitted[2]);
            $("#ldl_cholesterol").val(splitted[3]);
            $("#vldl").val(splitted[4]);
            $("#hdl_ratio").val(splitted[5]);
            $("#ldl_hdl").val(splitted[6]);
            $("#lipoprotein").val(splitted[7]);
            $("#apolipoprotein_a").val(splitted[8]);
            $("#apolipoprotein_b").val(splitted[9]);
        }
        else{
            $("#total_cholesterol").val('');
            $("#triglycerides_lipid").val('');
            $("#hdl_cholesterol").val('');
            $("#ldl_cholesterol").val('');
            $("#vldl").val('');
            $("#hdl_ratio").val('');
            $("#ldl_hdl").val('');
            $("#lipoprotein").val('');
            $("#apolipoprotein_a").val('');
            $("#apolipoprotein_b").val('');
           // alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#14a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchdiabetes') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#diabetesdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#blood_fasting").val(splitted[0]);
            $("#blood_prandial").val(splitted[1]);
            $("#blood_random").val(splitted[2]);
            $("#urine_fasting").val(splitted[3]);
            $("#urine_prandial").val(splitted[4]);
            $("#urine_random").val(splitted[5]);
            $("#glu_test").val(splitted[6]);
            $("#glycosylated_hb").val(splitted[7]);
        }
        else{
            $("#blood_fasting").val('');
            $("#blood_prandial").val('');
            $("#blood_random").val('');
            $("#urine_fasting").val('');
            $("#urine_prandial").val('');
            $("#urine_random").val('');
            $("#glu_test").val('');
            $("#glycosylated_hb").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#15a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchcardiac') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
       $('#cardiacdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            $("#homocysteine").val(splitted[0]);
            $("#ecg").val(splitted[1]);
            $("#decho").val(splitted[2]);
        }
        else{
            $("#homocysteine").val('');
            $("#ecg").val('');
            $("#decho").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#16a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchserology') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
         $('#serologydiv').show();
        if(data!=""){
            var splitted = data.split("/");
           
            $("#serological_report").val(splitted[0]);
        }
        else{
             $("#serological_report").val('');
            //alert("No data found for this date");

        }

    }

    });

  }

  if(target=="#17a"){

    var dateval=$('.savedate1').val();
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchsemen') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#semendiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#semen_analysis").val(splitted[0]);
        }
        else{
            $("#semen_analysis").val('');
           // alert("No data found for this date");

        }

    }

    });

  }

});

// Search CBC by date
 $(".searchcbc").on("click", function(){
    var dateval=$('.datesearch').val();
    $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchcb') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#cbcdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#hb").val(splitted[0]);
            $("#rbc").val(splitted[1]);
            $("#wbc").val(splitted[2]);
            $("#platelets").val(splitted[3]);
            $("#vitaminb").val(splitted[4]);
            $("#vitamind").val(splitted[5]);
            $("#neutrophils").val(splitted[6]);
            $("#lymphocytes").val(splitted[7]);
            $("#eosinophils").val(splitted[8]);
            $("#monocytes").val(splitted[9]);
            $("#basophils").val(splitted[10]);
            $("#band_cells").val(splitted[11]);
            $("#abnor_rbc").val(splitted[12]);
            $("#abnor_wbc").val(splitted[13]);
            $("#parasites").val(splitted[14]);
            $("#esr").val(splitted[15]);
            $(".savedate1").val(dateval);

        }
        else{

            $("#hb").val('');
            $("#rbc").val('');
            $("#wbc").val('');
            $("#platelets").val('');
            $("#vitaminb").val('');
            $("#vitamind").val('');
            $("#neutrophils").val('');
            $("#lymphocytes").val('');
            $("#eosinophils").val('');
            $("#monocytes").val('');
            $("#basophils").val('');
            $("#band_cells").val('');
            $("#abnor_rbc").val('');
            $("#abnor_wbc").val('');
            $("#parasites").val('');
            $("#esr").val('');
            //alert("No data found for this date");

        }

    }

    });
});

 // Search Urine by date
 $(".searchurine1").on("click", function(){
   
    var dateval=$('.urinedate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchurine') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#urinediv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#quantity").val(splitted[0]);
            $("#color").val(splitted[1]);
            $("#appearance").val(splitted[2]);
            $("#reaction").val(splitted[3]);
            $("#gra").val(splitted[4]);
            $("#protein").val(splitted[5]);
            $("#sugar").val(splitted[6]);
            $("#acetone").val(splitted[7]);
            $("#pigments").val(splitted[8]);
            $("#occult_blood").val(splitted[9]);
            $("#urobilinogen").val(splitted[10]);
            $("#epith_cells").val(splitted[11]);
            $("#pus_cells").val(splitted[12]);
            $("#rbc_urine").val(splitted[13]);
            $("#other_findings").val(splitted[14]);
        }
        else{
            //$('#urinediv').show();
            $("#quantity").val('');
            $("#color").val('');
            $("#appearance").val('');
            $("#reaction").val('');
            $("#gra").val('');
            $("#protein").val('');
            $("#sugar").val('');
            $("#acetone").val('');
            $("#pigments").val('');
            $("#occult_blood").val('');
            $("#urobilinogen").val('');
            $("#epith_cells").val('');
            $("#pus_cells").val('');
            $("#rbc_urine").val('');
            $("#other_findings").val('');
            //alert("No data found for this date");

        }

    }

    });
});

 // Search Stool Profile by date
$(".searchstool").on("click", function(){
    var dateval=$('.stooldate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchstool') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#stooldiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#color_stool").val(splitted[0]);
            $("#consistency").val(splitted[1]);
            $("#mucus").val(splitted[2]);
            $("#frank_blood").val(splitted[3]);
            $("#adult_worm").val(splitted[4]);
            $("#reaction_stool").val(splitted[5]);
            $("#pus_cells_stool").val(splitted[6]);
            $("#occult_blood_stool").val(splitted[7]);
            $("#macrophages").val(splitted[8]);
            $("#rbc_stool").val(splitted[9]);
            $("#ova").val(splitted[10]);
            $("#cysts").val(splitted[11]);
            $("#protozoa").val(splitted[12]);
            $("#yeast_cell").val(splitted[13]);
            $("#other_findings_stool").val(splitted[14]);
        }
        else{
            $("#color_stool").val('');
            $("#consistency").val('');
            $("#mucus").val('');
            $("#frank_blood").val('');
            $("#adult_worm").val('');
            $("#reaction_stool").val('');
            $("#pus_cells_stool").val('');
            $("#occult_blood_stool").val('');
            $("#macrophages").val('');
            $("#rbc_stool").val('');
            $("#ova").val('');
            $("#cysts").val('');
            $("#protozoa").val('');
            $("#yeast_cell").val('');
            $("#other_findings_stool").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Arithrits Profile by date
$(".searcharithris").on("click", function(){
    var dateval=$('.arithrisdate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searcharithris') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#arithrisdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#anti_o").val(splitted[0]);
            $("#accp").val(splitted[1]);
            $("#ra_factor").val(splitted[2]);
            $("#alkaline").val(splitted[3]);
            $("#ana").val(splitted[4]);
            $("#c_react").val(splitted[5]);
            $("#complement").val(splitted[6]);
        }
        else{
           
            $("#anti_o").val('');
            $("#accp").val('');
            $("#ra_factor").val('');
            $("#alkaline").val('');
            $("#ana").val('');
            $("#c_react").val('');
            $("#complement").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Endocrine Profile by date
$(".searchendocrine").on("click", function(){
    var dateval=$('.endocrinedate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchendocrine') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#endocrinediv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#t3_end").val(splitted[0]);
            $("#t4_end").val(splitted[1]);
            $("#tsh_end").val(splitted[2]);
            $("#ft3").val(splitted[3]);
            $("#ft4").val(splitted[4]);
            $("#anti_tpo").val(splitted[5]);
            $("#antibody").val(splitted[6]);
            $("#prolactin").val(splitted[7]);
            $("#fsh").val(splitted[8]);
            $("#lsh").val(splitted[9]);
            $("#progesterone_3").val(splitted[10]);
            $("#progesterone").val(splitted[11]);
            $("#dhea").val(splitted[12]);
            $("#testosterone").val(splitted[13]);
            $("#ama").val(splitted[14]);
            $("#insulin").val(splitted[15]);
            $("#glucose").val(splitted[16]);
        }
        else{

            $("#t3_end").val('');
            $("#t4_end").val('');
            $("#tsh_end").val('');
            $("#ft3").val('');
            $("#ft4").val('');
            $("#anti_tpo").val('');
            $("#antibody").val('');
            $("#prolactin").val('');
            $("#fsh").val('');
            $("#lsh").val('');
            $("#progesterone_3").val('');
            $("#progesterone").val('');
            $("#dhea").val('');
            $("#testosterone").val('');
            $("#ama").val('');
            $("#insulin").val('');
            $("#glucose").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Endocrine Profile by date
$(".searchrenal").on("click", function(){
    var dateval=$('.renaldate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchrenal') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#renaldiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#bun").val(splitted[0]);
            $("#urea").val(splitted[1]);
            $("#creatinine").val(splitted[2]);
            $("#uric_acid").val(splitted[3]);
            $("#calcium").val(splitted[4]);
            $("#phosphorus").val(splitted[5]);
            $("#sodium").val(splitted[6]);
            $("#potassium").val(splitted[7]);
            $("#chloride").val(splitted[8]);
        }
        else{

            $("#bun").val('');
            $("#urea").val('');
            $("#creatinine").val('');
            $("#uric_acid").val('');
            $("#calcium").val('');
            $("#phosphorus").val('');
            $("#sodium").val('');
            $("#potassium").val('');
            $("#chloride").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Xray Profile by date
$(".searchxray").on("click", function(){
    var dateval=$('.xraydate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchxray') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#xraydiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#radiological_report").val(splitted[0]);
        }
        else{
            $("#radiological_report").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search USG Female by date
$(".searchusgfemale").on("click", function(){
    var dateval=$('.usgfemaledate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchusgfemale') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#usgfemalediv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#uterues_size").val(splitted[0]);
            $("#thickness").val(splitted[1]);
            $("#fibroids_no").val(splitted[2]);
            $("#description").val(splitted[3]);
            $("#ovary_size_rt").val(splitted[4]);
            $("#ovary_size_lt").val(splitted[5]);
            $("#ovary_volume_rt").val(splitted[6]);
            $("#ovary_volume_lt").val(splitted[7]);
            $("#follicles_lt").val(splitted[8]);
            $("#follicles_rt").val(splitted[9]);
        }
        else{
             $("#uterues_size").val('');
            $("#thickness").val('');
            $("#fibroids_no").val('');
            $("#description").val('');
            $("#ovary_size_rt").val('');
            $("#ovary_size_lt").val('');
            $("#ovary_volume_rt").val('');
            $("#ovary_volume_lt").val('');
            $("#follicles_lt").val('');
            $("#follicles_rt").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search USG Male by date
$(".searchusgmale").on("click", function(){
    var dateval=$('.usgmaledate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchusgmale') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#usgmalediv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#size").val(splitted[0]);
            $("#volume").val(splitted[1]);
            $("#serum_psa").val(splitted[2]);
            $("#other_findings_male").val(splitted[3]);
        }
        else{
            $("#size").val('');
            $("#volume").val('');
            $("#serum_psa").val('');
            $("#other_findings_male").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Immunology by date
$(".searchimmunlogy").on("click", function(){
    var dateval=$('.immunlogydate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchimmunlogy') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#immunlogydiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#igg").val(splitted[0]);
            $("#ige").val(splitted[1]);
            $("#igm").val(splitted[2]);
            $("#iga").val(splitted[3]);
            $("#itg").val(splitted[4]);
        }
        else{
            $("#igg").val('');
            $("#ige").val('');
            $("#igm").val('');
            $("#iga").val('');
            $("#itg").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Specific by date
$(".searchspecific").on("click", function(){
    var dateval=$('.specificdate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchspecific') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#specificdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#other_findings_specific").val(splitted[0]);
            $("#define_field1").val(splitted[1]);
            $("#define_field2").val(splitted[2]);
            $("#define_field3").val(splitted[3]);
            $("#define_field4").val(splitted[4]);
        }
        else{
            $("#other_findings_specific").val('');
            $("#define_field1").val('');
            $("#define_field2").val('');
            $("#define_field3").val('');
            $("#define_field4").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Cardiac by date
$(".searchcardiac").on("click", function(){
    var dateval=$('.cardiacdate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchcardiac') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#cardiacdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#homocysteine").val(splitted[0]);
            $("#ecg").val(splitted[1]);
            $("#decho").val(splitted[2]);
        }
        else{
            $("#homocysteine").val('');
            $("#ecg").val('');
            $("#decho").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Serology by date
$(".searchserology").on("click", function(){
    var dateval=$('.serologydate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchserology') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
         $('#serologydiv').show();
        if(data!=""){
            var splitted = data.split("/");
           
            $("#serological_report").val(splitted[0]);
        }
        else{
             $("#serological_report").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Semen by date
$(".searchsemen").on("click", function(){
    var dateval=$('.semendate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchsemen') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#semendiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#semen_analysis").val(splitted[0]);
        }
        else{
            $("#semen_analysis").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Diabetes by date
$(".searchdiabetes").on("click", function(){
    var dateval=$('.diabetesdate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchdiabetes') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#diabetesdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#blood_fasting").val(splitted[0]);
            $("#blood_prandial").val(splitted[1]);
            $("#blood_random").val(splitted[2]);
            $("#urine_fasting").val(splitted[3]);
            $("#urine_prandial").val(splitted[4]);
            $("#urine_random").val(splitted[5]);
            $("#glu_test").val(splitted[6]);
            $("#glycosylated_hb").val(splitted[7]);
        }
        else{
            $("#blood_fasting").val('');
            $("#blood_prandial").val('');
            $("#blood_random").val('');
            $("#urine_fasting").val('');
            $("#urine_prandial").val('');
            $("#urine_random").val('');
            $("#glu_test").val('');
            $("#glycosylated_hb").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Lipid profile by date
$(".searchlipid").on("click", function(){
    var dateval=$('.lipiddate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchlipid') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#lipiddiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#total_cholesterol").val(splitted[0]);
            $("#triglycerides_lipid").val(splitted[1]);
            $("#hdl_cholesterol").val(splitted[2]);
            $("#ldl_cholesterol").val(splitted[3]);
            $("#vldl").val(splitted[4]);
            $("#hdl_ratio").val(splitted[5]);
            $("#ldl_hdl").val(splitted[6]);
            $("#lipoprotein").val(splitted[7]);
            $("#apolipoprotein_a").val(splitted[8]);
            $("#apolipoprotein_b").val(splitted[9]);
        }
        else{
            $("#total_cholesterol").val('');
            $("#triglycerides_lipid").val('');
            $("#hdl_cholesterol").val('');
            $("#ldl_cholesterol").val('');
            $("#vldl").val('');
            $("#hdl_ratio").val('');
            $("#ldl_hdl").val('');
            $("#lipoprotein").val('');
            $("#apolipoprotein_a").val('');
            $("#apolipoprotein_b").val('');
            //alert("No data found for this date");

        }

    }

    });
});

// Search Liver profile by date
$(".searchliver").on("click", function(){
    var dateval=$('.liverdate').val();
     $('.investdate').val(dateval);
    var regid = $('.regid').val();
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchliver') }}",
    data:{'dateval':dateval, 'regid':regid},
    success:function(data){
        $('#liverdiv').show();
        if(data!=""){
            var splitted = data.split("/");
            
            $("#total_bil").val(splitted[0]);
            $("#dir_bilirubin").val(splitted[1]);
            $("#ind_bilirubin").val(splitted[2]);
            $("#gamma_gt").val(splitted[3]);
            $("#total_protein").val(splitted[4]);
            $("#albumin").val(splitted[5]);
            $("#globulin").val(splitted[6]);
            $("#sgot").val(splitted[7]);
            $("#sgpt").val(splitted[8]);
            $("#alk_phos").val(splitted[9]);
            $("#aust_antigen").val(splitted[10]);
            $("#amylase").val(splitted[11]);
        }
        else{
           
            $("#total_bil").val('');
            $("#dir_bilirubin").val('');
            $("#ind_bilirubin").val('');
            $("#gamma_gt").val('');
            $("#total_protein").val('');
            $("#albumin").val('');
            $("#globulin").val('');
            $("#sgot").val('');
            $("#sgpt").val('');
            $("#alk_phos").val('');
            $("#aust_antigen").val('');
            $("#amylase").val('');
            //alert("No data found for this date");

        }

    }

    });
});

</script>
<script type="text/javascript">
    $(function () {
        $(".datepicker").datepicker({
          format:'dd/mm/yyyy', 
            autoclose: true, 
            todayHighlight: true,
        });
    });

     $(function () {
        $(".datepickerheight").datepicker({
          format:'dd/mm/yyyy', 
            autoclose: true, 
            todayHighlight: true,
            endDate: '+0d',
        });
    });
    $("#timepicker").datetimepicker({
        format: "LT",
        icons: {
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down"
        }
  });

function getadditional(obj){
    var getprice = $(obj).children('option:selected').attr('data-price');
     $(obj).parent().parent().find(".additional_price").val(getprice);
}

function updatecharges(id){
  var regid = id;
            $.ajax({
                type: "GET",
                url: "{{ url(config('laraadmin.adminRoute') . '/getregcharges') }}",
                data: { regid:regid}, 
                success: function( msg ) {
                   var splitted = msg.split("/");
            
                  $(".regcharges").val(splitted[0]);
                  $(".chgid").val(splitted[2]);
                  $("#Updatecharges").modal('show');
                    
                  //  $(".additional-body").html(msg);
                }
            });
}
</script>
@endpush

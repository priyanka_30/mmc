@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/medicalcases') }}">Medicalcase</a> :
@endsection
@section("contentheader_description", $potencydetail->$view_col)
@section("section", "Medicalcase")
@section("section_url", url(config('laraadmin.adminRoute') . '/medicalcases'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Medicalcase Edit : ".$potencydetail->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="post" action="{{ url(config('laraadmin.adminRoute') . '/medicalcases/potencyupdate/') }}" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate">
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <input type="hidden" name="id" value="{{ $potencydetail->id }}">
													
							<div class="form-group col-md-6">
							<label for="Emailid">Remedy :</label>
							<input class="form-control"  name="rxremedy" type="text" value="{{ $potencydetail->rxremedy }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="phone">Potency :</label>
							<input class="form-control"  name="rxpotency" type="text" value="{{ $potencydetail->rxpotency }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="address">Frequency :</label>
							<input class="form-control" name="rxfrequency" type="text" value="{{ $potencydetail->rxfrequency }}" aria-required="true"></div>
							<div class="form-group col-md-6">
							<label for="city">Days* :</label>
							<input class="form-control"required="1" name="rxdays" type="text" value="{{ $potencydetail->rxdays }}" aria-required="true"></div>
							<div class="form-group col-md-12">
							<label for="pin">Prescription:</label>
							<input class="form-control"required="1" name="rxprescription" type="text" value="{{ $potencydetail->rxprescription }}" aria-required="true"></div>
																	
					                    <br>
					<div class="form-group col-md-12">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
@endpush

@extends('la.layouts.app')

@section('htmlheader_title')
	Doctor View
@endsection


@section('main-content')

<div id="page-content" class="profile2">
	<div class="bg-success clearfix">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-3">
					<img class="profile-image" src="{{ asset('la-assets/img/'.$doctor->profilepic)}}"style="height: 100px; width: 100px;">
				</div>
				<div class="col-md-9">
					<h4 class="name">{{ $doctor->$view_col }}</h4>
					<div class="row stats">
						<div class="col-md-6 stat"><div class="label2" data-toggle="tooltip" data-placement="top" title="Designation">{{ $doctor->designation }}</div></div>
						
					</div>
					<p class="desc">{{ substr($doctor->about, 0, 33) }}@if(strlen($doctor->about) > 33)...@endif</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="dats1"><i class="fa fa-envelope-o"></i> {{ $doctor->email }}</div>
			<div class="dats1"><i class="fa fa-phone"></i> {{ $doctor->mobile }}</div>
		</div>
		
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		@if($doctor->id != Auth::user()->context_id)
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/doctors') }}" data-toggle="tooltip" data-placement="right" title="Back to Doctors"><i class="fa fa-chevron-left"></i></a></li>
		@endif
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		
		@if($doctor->id == Auth::user()->context_id || Entrust::hasRole("SUPER_ADMIN"))
			<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settings" data-target="#tab-account-settings"><i class="fa fa-key"></i> Account settings</a></li>
		@endif
		<li class=""><a role="tab" data-toggle="tab" href="#tab-view-appointments" data-target="#tab-view-appointments"><i class="fa fa-key"></i> View Appointments</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'title')
						@la_display($module, 'name')
						@la_display($module, 'middlename')
						@la_display($module, 'surname')
						@la_display($module, 'date_birth')
						@la_display($module, 'gender')
						@la_display($module, 'mobile')		
						@la_display($module, 'email')
						@la_display($module, 'city')
						@la_display($module, 'address')
						@la_display($module, 'permanentaddress')
						@la_display($module, 'qualification')
						@la_display($module, 'joiningdate')
						@la_display($module, 'registrationId')
						@la_display($module, 'aadharnumber')
						@la_display($module, 'pannumber')
						@la_display($module, 'profilepic')
						
						
					</div>
				</div>
			</div>
		</div>
		
		
		@if($doctor->id == Auth::user()->context_id || Entrust::hasRole("SUPER_ADMIN"))
		<div role="tabpanel" class="tab-pane fade" id="tab-account-settings">
			<div class="tab-content">
				<form action="{{ url(config('laraadmin.adminRoute') . '/doctors/change_password/'.$doctor->id) }}" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8">
					{{ csrf_field() }}
					<div class="panel">
						<div class="panel-default panel-heading">
							<h4>Account settings</h4>
						</div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							@if(Session::has('success_message'))
								<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_message') }}</p>
							@endif
							<div class="form-group">
								<label for="password" class=" col-md-2">Password</label>
								<div class=" col-md-10">
									<input type="password" name="password" value="" id="password" class="form-control" placeholder="Password" autocomplete="off" required="required" data-rule-minlength="6" data-msg-minlength="Please enter at least 6 characters.">
								</div>
							</div>
							<div class="form-group">
								<label for="password_confirmation" class=" col-md-2">Retype password</label>
								<div class=" col-md-10">
									<input type="password" name="password_confirmation" value="" id="password_confirmation" class="form-control" placeholder="Retype password" autocomplete="off" required="required" data-rule-equalto="#password" data-msg-equalto="Please enter the same value again.">
								</div>
							</div>
						</div>
						<div class="panel-footer">
							<button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Change Password</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@endif
		<div role="tabpanel" class="tab-pane active fade in" id="tab-view-appointments">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>View Appointments</h4>
					</div>
					<div class="panel-body">
						<table id="example1" class="table table-bordered">
						<thead>
						<tr class="success">
							<th>Patient Name</th>
							<th>Booking Date</th>
							<th>Booking Time</th>
							<th>Phone</th>
							<th>Action</th>
						</tr>
						
						@foreach ($appointments as $appoint)
						<tr>
							<td>{{ $appoint->patient_name }}</td>
							<td>{{ $appoint->booking_date }}</td>
							<td>{{ $appoint->booking_time }}</td>
							<td>{{ $appoint->phone }}</td>
							<td>
								@if($appoint->status == "Accepted")
								Accepted
								
								@if($appoint->patient_id )
								<form action="{{ url(config('laraadmin.adminRoute') . '/doctors/newcase/') }}" id="password-reset-form" class="general-form dashed-row white" method="post" style="display: inline-block; ">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="appointment_id" value="{{ $appoint->id }}">
									<input type="hidden" name="patient_id" value="{{ $appoint->patient_id }}">
									<input type="hidden" name="type" value="followup">
									<button type="submit" class="btn btn-success" style="margin-left: 19px;">Followup</button>
								</form>
								
								@else
								<form action="{{ url(config('laraadmin.adminRoute') . '/doctors/newcase/') }}" id="password-reset-form" class="general-form dashed-row white" method="post" style="display: inline-block; ">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="appointment_id" value="{{ $appoint->id }}">
									<input type="hidden" name="patient_id" value="{{ $appoint->patient_id }}">
									<input type="hidden" name="type" value="newcase">
									<button type="submit" class="btn btn-success" style="margin-left: 19px;">New Case</button>
								</form>
								@endif
								
								@else
								<form action="{{ url(config('laraadmin.adminRoute') . '/doctors/update_status/'.$doctor->id) }}" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8" style="display: inline-block; ">
									<input type="hidden" name="patient_id" value="{{ $appoint->id }}">
									<input type="hidden" name="status" value="Accepted">
									<button type="submit" class="btn btn-info">Accept</button>
								</form>	
								<form action="{{ url(config('laraadmin.adminRoute') . '/doctors/delete_status/'.$doctor->id) }}" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8" style="display: inline-block; ">
									<input type="hidden" name="patient_id" value="{{ $appoint->id }}">
									<button type="submit" class="btn btn-danger">Decline</button>
								</form>
							@endif
							
							</td>
						</tr>
						@endforeach
						</thead>
						<tbody>
							
						</tbody>
						</table>
						
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
</div>
@endsection

@push('scripts')
<script>
$(function () {
	@if($doctor->id == Auth::user()->id || Entrust::hasRole("SUPER_ADMIN"))
	$('#password-reset-form').validate({
		
	});
	@endif
});
</script>
@endpush

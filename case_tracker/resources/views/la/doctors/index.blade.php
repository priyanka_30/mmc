@extends("la.layouts.app")

@section("contentheader_title", "Doctor")
@section("contentheader_description", "Doctor listing")
@section("section", "Doctor")
@section("sub_section", "Listing")
@section("htmlheader_title", "Doctor Listing")

@section("headerElems")
@la_access("Doctors", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Doctor</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Doctors", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Doctor</h4>
			</div>
			{!! Form::open(['action' => 'LA\DoctorsController@store', 'id' => 'doctor-add-form' , 'onsubmit' => 'return checkForm(this)' ,'files'=>true]) !!}
			<div class="modal-body">
				<div class="box-body">
				    <?php //echo "<pre>"; print_r($module); die();?>
				    @if($errors->any())
			        <h4 style="color: #000;background: #d4edda;border-color :#d4edda; text-align: center; font-size: 16px;padding: 7px;">{{$errors->first()}}</h4>
			        @endif
                    @la_form($module)
				
					{{--
					
					@la_input($module, 'designation')
					@la_input($module, 'gender')
					@la_input($module, 'mobile')
					@la_input($module, 'mobile2')
					@la_input($module, 'email')
					@la_input($module, 'consultation_fee')
					@la_input($module, 'dept')
					@la_input($module, 'city')
					@la_input($module, 'address')
					@la_input($module, 'about')
					@la_input($module, 'date_birth')
					@la_input($module, 'date_hire')
					@la_input($module, 'date_left')
					@la_input($module, 'salary_cur')
					--}}
					
					<div class="form-group" style="display:none;">
						<label for="role">Role* :</label>
						<select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="role">
							<option value="3">Doctor</option>
						</select>
					</div>
					<input type="file" name="profilepic">
				</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access


@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	//$("#NewModal").modal("show");
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/doctor_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#doctor-add-form").validate({
		
	});
});

function checkForm(form)
  {
  
    if(form.password.value != "" ) {
      if(form.password.value.length < 6) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least six characters!";
       
        form.password.focus();
        return false;
      }
           re = /[0-9]/;
      if(!re.test(form.password.value)) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least one number (0-9)!";
        form.password.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(form.password.value)) {
      	document.getElementById("password_strength").innerHTML = "Password must contain at least one lowercase letter (a-z)!";
         
        form.password.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(form.password.value)) {
      	document.getElementById("password_strength").innerHTML = "Password must contain at least one uppercase letter (A-Z)!";
                form.password.focus();
        return false;
      }
    } else {
      form.password.focus();
      return false;
    }

  }
</script>
@endpush

<style>
    .form-group{ float:left; width:45%;margin-left:15px; margin-right:15px;}
    .modal.in .modal-dialog{ width:800px;}
</style>
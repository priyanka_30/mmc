@extends("la.layouts.app")

@section("contentheader_title", "Remedy")
@section("contentheader_description", "Remedy listing")
@section("section", "Remedy")
@section("sub_section", "Listing")
@section("htmlheader_title", "Remedy Listing")

@section("headerElems")
@la_access("Smstemplate", "create")
	<!-- <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Smstemplate</button> -->
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			<th>RegID</th>
			<th>Name</th>
			<th>Remedy</th>
			<th>Potency</th>
			<th>Frequency</th>
			<th>Days</th>
			<th>Prescription</th>
			<th>Post Type</th>
			<th>Print Stickers</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach($CasePotency as $val){ ?>
			<tr>
				<td><?php echo  $val->cregid; ?></td>
				<td><?php echo  $val->first_name. " ".$val->surname; ?></td>
				<td><?php echo  $val->name; ?></td>
				<td><?php echo  $val->pname; ?></td>
				<td><?php echo  $val->frequency; ?></td>
				<td><?php echo  $val->days; ?></td>
				<td><?php echo  $val->rxprescription; ?></td>
				<td><?php  if($val->post_type==""){ echo "N/A";} else { echo  $val->post_type;} ; ?></td>
				<td>
					<a onClick="openSticker(<?php echo $val->cregid; ?>,'<?php echo $val->first_name; ?>')" class="btn btn-primary btn-xs" style="display:inline;padding: 2px 7px 2px 8px; margin-left:10px; ">Print Stickers</a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		</table>
	</div>
</div>

@la_access("Remedy", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Smstemplate</h4>
			</div>
			{!! Form::open(['action' => 'LA\SmstemplateController@store', 'id' => 'daycharges-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    <div class="box-body">
                    <div class="form-group">
                    	<label for="templatename">Template Name :</label>
                    	<input class="form-control" placeholder="Enter Template Name" name="templatename" type="text" value="">
                    </div>
                    <div class="form-group">
                    		<label for="message">Message :</label>
                    		<textarea class="form-control" placeholder="Enter Message" name="message" type="text" value="" rows="5"></textarea>
                		</div>
                    <div class="form-group"><label for="smstype">SMS Type :</label>
                    <!-- 	<input class="form-control" placeholder="Enter SMS Type" name="smstype" type="text" value=""> -->
                    <label class="container-radio">Normal
                              <input type="radio" name="smstype" class="send_sms" value="Normal">
                              <span class="checkmark-radio"></span>
                            </label>
                            <label class="container-radio">Birthday
                              <input type="radio" name="smstype" class="send_sms" value="Birthday">
                              <span class="checkmark-radio"></span>
                            </label>
                             <label class="container-radio">Courier
                              <input type="radio" name="smstype" class="send_sms" value="Courier">
                              <span class="checkmark-radio"></span>
                            </label>
                             </div>					
									</div>
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'tags')
					@la_input($module, 'color')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="openSticker" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Print Stickers</h4>
            </div>
            {!! Form::open(['action' => 'LA\DailycollectionController@printstickers', 'id' => 'daycharges-add-form', 'target'=>'_blank']) !!}
            <div class="modal-body">
                <div class="box-body">
                  
                 <div class="form-group col-md-12 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control sticker_regid" placeholder="RegID" readonly  name="sticker_regid" type="text" value="" aria-required="true"required>
                    </div>

                    <div class="form-group col-md-12 " class="patient-info-td">
                        <label for="mobile" >Name</label>
                        <input class="form-control sticker_name" placeholder="" readonly  name="sticker_name" type="text" value="" aria-required="true"required>
                    </div>

                    <div class="form-group col-md-12 " class="patient-info-td">
                        <label for="mobile" >No. of Stickers Print ?</label>
                        <input class="form-control" placeholder=""   name="nostickers" type="text" value="" aria-required="true"required>
                    </div>

                    
                  
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success height-btn">Print</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
	 function openSticker(id,name){
    $('.sticker_regid').val(id);
    $('.sticker_name').val(name);
    $('#openSticker').modal('show');
}
$(function () {
	$("#example1").DataTable({
		processing: true,
     //   serverSide: true,
      //  ajax: "{{ url(config('laraadmin.adminRoute') . '/smstemplate_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
@endpush
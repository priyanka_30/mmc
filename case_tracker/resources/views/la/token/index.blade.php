@extends("la.layouts.app")

@section("contentheader_title", "Add Token")
@section("contentheader_description", "Token listing")
@section("section", "Add Token")
@section("sub_section", "Listing")
@section("htmlheader_title", "Token Listing")

@section("headerElems")
@la_access("Token", "create")
	<!-- <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Token</button> -->
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<style>
	.highlight { background-color: grey; }
</style>


@la_access("Token", "create")
{!! Form::open(['action' => 'LA\DaychargesController@store', 'id' => 'daycharges-add-form']) !!}
<div class="box-body">
                   <?php if((Auth::user()->type != "Doctor") && (Auth::user()->type != "Admin")) { ?>
                   <fieldset class="fieldset" >
                    <div class="col-md-12" style="padding: 0px;">
                         <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px; "> Name </label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 13%; padding-left: 0px;">
                              <input  type="text" class="search_name" name="search_name" class="form-control" autocomplete="off">
                            </div>
                          </div>
                        </div>

                         <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px; "> Surname </label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 13%; padding-left: 0px;">
                              <input  type="text" class="surname" name="surname" class="form-control" autocomplete="off">
                            </div>
                          </div>
                        </div>

                        <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px;"> Mobile</label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 13%;">
                              <input  type="text" class="search_surname" name="search_surname" class="form-control" autocomplete="off">
                            </div>
                          </div>
                        </div>

                        <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px;"> RegId</label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 26%;">
                              <input  type="text" class="search_regid"  name="search_regid" class="form-control" autocomplete="off">
                              <button type="button" class="btn btn-success searchbtn" style="float: right;margin-top: -4px;" >Search</button>
                            </div>
                          </div>
                        </div>

                         <input type="hidden" name="regid" class="regid">
                                <input type="hidden" name="name" class="fname">
                                <input type="hidden" name="dname" class="dname">
                                <input type="hidden" name="lastid" class="lastid">
                                <input type="hidden" name="pname" class="pname">
                                <input type="hidden" name="packexp" class="packexp">
                        
                    </div>
                </fieldset>
            <?php } ?>

                        <div class="col-md-12"  style="padding-left:0;padding-right:0">
                    <?php if((Auth::user()->type != "Doctor") && (Auth::user()->type != "Admin")) { ?>

                     <div class="col-md-4" style="padding: 0px; margin-top: 3rem;">
                      

                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <table class="table table-bordered tb-bg rxdatavaltable">
                                        <tr>
                                            <th>RegID</th>
                                            <th>Name</th>
                                        </tr>
                                        <tbody class="hide-body">
                                            <tr>
                                                <td style="padding: 14px;"></td>
                                                <td style="padding: 14px;"></td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 14px;"></td>
                                                <td style="padding: 14px;"></td>
                                            </tr>
                                        </tbody>
                                        <tbody class="search-body">
                                            
                                        </tbody>
                                    </table>
                        </div>
                        
                        
                        
                        
                     </div>

                     <div class="col-md-3" style="margin-top: 5rem;">
                         <div class="form-group col-md-12" style="padding-left:0; padding-right:0;text-align: center;">
                            <button type="button" class="btn btn-success height-btn addbtn" ><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                        </div>
                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0;text-align: center;">

                            <button type="button" class="btn btn-success height-btn removebtn" ><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                            <input type="hidden" name="tokenid" class="tokenid">
                        </div>
                     </div>
                     <?php } ?>
                     <?php if((Auth::user()->type == "Doctor") || (Auth::user()->type == "Admin")) { ?>
                     <div class="col-md-12" style="padding-left:0; padding-right:0">
                     <?php } else { ?>
                        <div class="col-md-5" style="padding-left:0; padding-right:0">
                        <?php } ?>
                     
                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0;margin-top: 3rem;">
                            <table class="table table-bordered tb-bg addtokentable">
                                        <tr>
                                            <th>Sno</th>
                                            <th>RegID</th>
                                            <th>Name</th>
                                            <th>Doctor</th>
                                            <?php if((Auth::user()->type == "Doctor") && (Auth::user()->type == "Admin") ) { ?>
                                                <th>Mobile</th>
                                                <th>Mobile1</th>
                                                <th>Address</th>
                                                <th>Balance</th>
                                               
                                              <?php }   ?>

                                               <th>Package</th>
                                                <th>Package Expiry</th>
                                               

                                        </tr>
                                        <tbody>
                                            <?php 
                                            $sno = 1;
                                            foreach($tokendata as $data){
                                                if($data->rowcolor){ 
                                                    $class = "background-color: grey; color:#fff;"; 
                                                 }
                                                 else{
                                                    $class = "";
                                                 }
                                            ?>
                                            <tr id="<?php echo $data->id ?>" class="<?php echo $data->id ?>"  style= "<?php echo $class; ?>" >
                                                <td onclick="removefunction(<?php echo $data->id ?>);"><?php echo  $sno; ?></td>
                                                <td onclick="removefunction(<?php echo $data->id ?>);">
                                                    <?php if((Auth::user()->type == "Doctor") || (Auth::user()->type == "Admin") ){ ?> 
                                                        <a href="http://smartops.co.in/managemyclinic/admin/medicalcases/<?php echo $data->tregid; ?>" style="color: #48B0F7;"><?php echo $data->tregid; ?></a>
                                                    <?php } else { 
                                                    echo $data->tregid;  } ?></td>
                                                <td onclick="removefunction(<?php echo $data->id ?>);"><?php echo $data->first_name; ?></td>
                                                <td onclick="removefunction(<?php echo $data->id ?>);"><?php echo $data->dname; ?></td>

                                                <?php if((Auth::user()->type == "Doctor") && (Auth::user()->type == "Admin")) { ?>
                                                    <td onclick="removefunction(<?php echo $data->id ?>);"><?php echo $data->mobile1; ?></td>
                                                    <td onclick="removefunction(<?php echo $data->id ?>);"><?php echo $data->phone; ?></td>
                                                    <td onclick="removefunction(<?php echo $data->id ?>);"><?php echo $data->address." " .$data->area." , ".$data->city. " ,".$data->state; ?></td>
                                                     <td onclick="removefunction(<?php echo $data->id ?>);"><?php echo $data->sum; ?></td>
                                                   

                                                <?php }   ?>
                                                 <td onclick="removefunction(<?php echo $data->id ?>);"><?php echo $data->packname; ?></td>
                                                    <td onclick="removefunction(<?php echo $data->id ?>);"><?php echo date('d M Y',strtotime($data->date_left)); ?></td>
                                                    
                                            </tr>
                                        <?php $sno++; } ?>
                                           
                                        </tbody>
                                    </table>
                        </div>
                        
                        
                        
                        
                        
                     </div>
                </div>
                </div>
                {!! Form::close() !!}


<div class="modal fade" id="AddHeightWeight" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:800px">
        <div class="modal-content medicalcase_model">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Height Weight Entry</h4>
            </div>
            {!! Form::open(['action' => 'LA\MedicalcasesController@saveCaseHeightWeights', 'id' => 'medicalcase-add-form5']) !!}
               
                   {{--  @la_form($module) --}}
            <div class="modal-body">
                <div class="box-body">
                  {{--  @la_form($module) --}}

                  <div class="form-group col-md-4 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control reg_id" placeholder="RegID" readonly  name="regid" type="text" value="" aria-required="true"required>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="form-group col-md-8" style="padding-left:0">
                            <table>
                                <tr>
                                    <td>
                                        <table class="table table-bordered tb-bg heightweighttabledata_ad">
                                            <tr>
                                                <th>Date</th>
                                                <th>Height</th>
                                                <th>Weight</th>
                                                <!-- <th>BP</th>
                                                <th>BP</th> -->
                                                <th>LMP</th>
                                             
                                            </tr>
                                            <tbody class="height-body">
                                                
                                            </tbody>
                                            
                                        </table>
                                    </td>
                                    
                                </tr>
                                
                            </table>
                        </div>
                        <div class="form-group col-md-4" style="padding-left:0;padding-right:0">
                                  <?php 
                                  $ht = "1";
                                  $wt = "50"; ?>
                                <table>
                                   
                                    <tr>
                                        <td>
                                                    <input class="form-control datevalht" type="hidden" name="dateval" readonly value=""/>
                                                    <input class="form-control datevalht1" type="hidden" value=""/>
                                                    <input type="hidden" class="randht" name="rand" value="">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="form-group col-md-12">
                                                <div class="form-group col-md-7" style="padding-left:0;padding-right:0">
                                                    <label for="mobile">Height:</label>
                                                    <input class="form-control height_ad" placeholder="Height"  name="height" type="text" value="" aria-required="true">
                                                    
                                                    
                                                </div>
                                                <div class="form-group col-md-5" style="padding-right: 0;padding-left: 3px;margin-top: 32px;color: red;">
                                                    <span class="ht"></span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group col-md-12">
                                                <div class="form-group col-md-7" style="padding-left:0;padding-right:0">
                                                    <label for="mobile">Weight:</label>
                                                    <input class="form-control weight_ad" placeholder="Weight"  name="weight" type="text" value="" aria-required="true">
                                                </div>
                                                <div class="form-group col-md-5" style="padding-right: 0;padding-left: 3px;margin-top: 32px;color: red;">
                                                    <span class="wt"></span>
                                                </div>
                                            </div>
                                          
                                             
                                        </td>
                                    </tr>
                                </table>
                            
                        </div>
                    </div>
                    <div class="col-md-12"> 
                    
                            <fieldset class="fieldset lmpdata" style="padding-top: 6px;">
                                <div class="form-group col-md-6" style="margin-top: 0px;margin-bottom: 10px;">
                                    <label for="mobile">LMP :</label>
                                    <div id="datepicker" class="input-group date datepickerheight" data-date-format="dd-mm-yyyy">
                                        <input class="form-control lmp" type="text" name="lmpdata" readonly value=""/>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </fieldset>
               
                    
                </div>
              
            </div>
            </div>
            <div class="modal-footer height-entry-footer">
                <button type="button" class="btn btn-success height-btn saveweightheight">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="packagehistory" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Package History</h4>
            </div>
            {!! Form::open(['action' => 'LA\DaychargesController@store', 'id' => 'daycharges-add-form']) !!}
            <div class="modal-body">
                <div class="box-body">
                  
                 <div class="form-group col-md-4 " class="patient-info-td">
                        <label for="mobile" >Reg ID</label>
                        <input class="form-control pregid" placeholder="RegID" readonly  name="regid" type="text" value="" aria-required="true"required>
                    </div>
                    <div class="col-md-12"  style="padding-left:0;padding-right:0; margin-top: 30px;">
                   

                    
                        

                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <table class="table table-bordered tb-bg familytable">
                                        <tr>
                                            <th>Package Date</th>
                                            <th>Package</th>
                                            <th>Period</th>
                                            <th>From Date</th>
                                            <th>To Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        <tbody class="package-body">
                                            
                                        </tbody>
                                    </table>
                        </div>
                      

                    
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="modal fade" id="AddPackage" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content medicalcase_model">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Packages</h4>
                </div>
                {!! Form::open(['action' => 'LA\MedicalcasesController@savepackages', 'class' => 'packageform']) !!}
                <div class="modal-body">
                    <div class="box-body">
                      {{--  @la_form($module) --}}
                      
                        <fieldset class="examin-fieldset">
                            <div class="form-group col-md-12 " style="margin-top: 14px;">
                                <label for="mobile" >Reg ID</label>
                                 <input class="form-control" id="case_id" placeholder=""  name="case_id"  value="" aria-required="true" readonly style="width: 22%;"> 
                                 <input type="hidden" class="regcasename">
                                 <input type="hidden" class="regmobile">
                            </div>
                            <div class="form-group col-md-6 " style="margin-top: 14px;">
                                <span class="regname" style="font-weight: bold;"></span> 
                            </div>
                            <div class="form-group col-md-6 " style="margin-top: 14px;">
                                <span class="packagename" style="font-weight: bold;"></span> 
                            </div>
                                 <div class="form-group col-md-6" style="margin-top: 14px;">
                                    
                                    <label class="container-radio">From Today
                                      <input type="radio" name="package_start" class="package_start" value="<?php echo date("Y-m-d"); ?>">
                                      <span class="checkmark-radio"></span>
                                    </label>
                                    <label class="container-radio" id="expirypack" style="display: none;">From Expiry
                                      <input type="radio" name="package_start" class="package_start" >
                                      <span class="checkmark-radio"></span>
                                    </label>
                                   
                                </div>
                               
                                <div class="form-horizontal ">
                                  <div class="control-group form-inline">
                                    <label for="name" class="col-md-2 login-label" style="margin-top: 14px;padding-right: 0px;" > From</label>
                                    <div class="controls col-md-4" style="padding-left: 0px;margin-top: 14px;">
                                     <div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control package_start_date" type="text" name="package_start_date" readonly value=""/>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group col-md-12 " >
                                <label for="name">Select Package:</label>
                                <select class="form-control select2-hidden-accessible scheme" required="1" data-placeholder="Select Scheme" rel="select2" name="packages" id="packages" tabindex="-1" aria-hidden="true" aria-required="true"required onchange="getprice(this);">
                                        <option value="Other">Other</option>
                                    <?php foreach($packages as $packagelist){?>
                                        <option value="<?php echo $packagelist->id;?>" data-price="<?php echo $packagelist->color;?>" data-months="<?php echo $packagelist->tags;?>"><?php echo $packagelist->name;?> <?php echo " - ". $packagelist->tags;?></option>
                                    <?php } ?>
                                        
                                </select>   
                              <input type="hidden" class="packagemonths">
                            </div>
                            <div class="form-group col-md-6 " >
                                <span class="packcharges"></span>
                            </div>

                            
                        <!--     <div class="col-md-12 religion-box">
                                 <label>Pacakge Details</label> <br>
                                <span class="packagename"></span>
                            </div>
                            <div class="col-md-12 religion-box">
                                <span class="pacakgestart"></span>
                            </div>
                            <div class="col-md-12 religion-box">
                                <span class="pacakge_expiry"></span>
                            </div>
                            <div class="col-md-12 religion-box">
                                <span class="expmsg" style="color: red;"></span>
                            </div> -->
                                            
                            
                        </fieldset>
                    </div>
                </div>
                <div class="modal-footer height-entry-footer">
                    <button type="button" class="btn btn-success height-btn savepicture">Save</button>
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddFamily" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Family</h4>
            </div>
            {!! Form::open(['action' => 'LA\DaychargesController@store', 'id' => 'daycharges-add-form']) !!}
            <div class="modal-body">
                <div class="box-body">
                  
                 <fieldset class="fieldset" >
                    <div class="col-md-12" style="padding: 0px;">
                         <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px; "> Name </label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 20%; padding-left: 0px;">
                              <input  type="text" class="search_name1" name="search_name" class="form-control" autocomplete="off">
                            </div>
                          </div>
                        </div>

                        <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px;"> Surname</label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 20%;">
                              <input  type="text" class="search_surname" name="search_surname" class="form-control" autocomplete="off">
                            </div>
                          </div>
                        </div>

                        <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-1 login-label" style="margin-top: 3px;"> RegId</label>
                            <div class="controls col-md-3" style="margin-bottom: 7px;width: 32%;">
                              <input  type="text" class="search_regid1"  name="search_regid" class="form-control" autocomplete="off">
                              <button type="button" class="btn btn-success searchbtn1" style="float: right;margin-top: -4px;" >Search</button>
                            </div>
                          </div>
                        </div>
                        
                    </div>
                </fieldset>
                        <div class="col-md-12"  style="padding-left:0;padding-right:0; margin-top: 30px;">
                   

                     <div class="col-md-4" style="padding: 0px;">
                        

                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <table class="table table-bordered tb-bg familytable">
                                        <tr>
                                            <th>RegID</th>
                                            <th>Name</th>
                                        </tr>
                                        <tbody class="search-body">
                                            
                                        </tbody>
                                    </table>
                        </div>
                        
                        <div class="col-md-12">&nbsp;</div>
                        
                        
                     </div>

                     <div class="col-md-3" style="margin-top: 2rem;">
                         <div class="form-group col-md-12" style="padding-left:0; padding-right:0;text-align: center;">
                            <button type="button" class="btn btn-success height-btn addbtnfamily" ><i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
                            <input type="hidden" name="f_regid" class="f_regid">
                            <input type="hidden" name="f_name" class="f_name">
                            <input type="hidden" name="f_surname" class="f_surname">
                            <input type="hidden" name="f_reg_id" class="f_reg_id">
                        </div>
                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0;text-align: center;">

                            <button type="button" class="btn btn-success height-btn removebtnfamily" ><i class="fa fa-angle-double-left" aria-hidden="true"></i></button>
                            <input type="hidden" name="familyid" class="familyid">
                        </div>
                     </div>
                  
                     <div class="col-md-5" style="padding-left:0; padding-right:0">
                      
                        <!--  <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-3 login-label" style="margin-top: 5px;"> Name </label>
                            <div class="controls col-md-9" style="margin-bottom: 7px;">
                              <input  type="text" class="search_fname" name="search_fname" class="form-control" >
                            </div>
                          </div>
                        </div>

                         <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-3 login-label" style="margin-top: 5px;"> Surname</label>
                            <div class="controls col-md-9" style="margin-bottom: 7px;">
                              <input  type="text" class="search_fsurname" name="search_fsurname" class="form-control" >
                            </div>
                          </div>
                        </div>

                        <div class="form-horizontal ">
                          <div class="control-group form-inline">
                            <label for="name" class="col-md-3 login-label" style="margin-top: 5px;"> RegId</label>
                            <div class="controls col-md-9" style="margin-bottom: 7px;">
                              <input  type="text" class="search_fregid"  name="search_fregid" class="form-control" >
                            </div>
                          </div>
                        </div>

                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <button type="button" class="btn btn-success searchfamilybtn" style="margin-left: 85px;" >Search</button>
                        </div> -->
                   
                        <div class="form-group col-md-12" style="padding-left:0; padding-right:0">
                            <table class="table table-bordered tb-bg addtokentablefamily">
                                        <tr>
                                           
                                            <th>RegID</th>
                                            <th>Name</th>
                                        </tr>
                                        <tbody class="searchfamily">

                                        </tbody>
                                    </table>
                        </div>
                        
                        
                        
                        
                        
                     </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
	$(document).ready(function(){
    $("#AddModal").modal('show');
});
$(function () {
	$("#example1").DataTable({
		processing: true,
        // serverSide: true,
        // ajax: "{{ url(config('laraadmin.adminRoute') . '/token_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#daycharges-add-form").validate({
		
	});


    $(".searchbtn").on("click", function(){
        var search_name = $('.search_name').val();
        var search_surname = $('.search_surname').val();
        var surname = $('.surname').val();
        var search_regid = $('.search_regid').val();
            $.ajax({
                type: "GET",
                url: "{{ url(config('laraadmin.adminRoute') . '/searchtoken') }}",
                data: { search_name:search_name, search_surname:search_surname,search_regid:search_regid,surname:surname}, 
                success: function( data ) {
                     
                    $(".search-body").html(data);
                    $(".hide-body").css("display","none");

                }
            });    
        
    });


  	$(".addbtn").on("click", function(){
        var regid = $('.regid').val();
        var fname = $('.fname').val();
        var dname = $('.dname').val();
        var lastid = $('.lastid').val();
        var pname = $('.pname').val();
        var packexp = $('.packexp').val();
       	var sno = parseInt(lastid) + 1;
      
        if(regid!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/savetoken') }}",
                data: { regid:regid}, 
                success: function( msg ) {
                    if(msg=="1"){
                        $('.addtokentable tr:first').after('<tr><td>'+sno+'</td><td>'+regid+'</td><td>'+fname+'</td><td>'+dname+'</td><td>'+pname+'</td><td>'+packexp+'</td></tr>');
                    }

                }
            });
        }else {
            alert('Select Id.');
        }
       
    });

    $(".removebtn").on("click", function(){
        var id = $('.tokenid').val();
      
        if(id!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/deletetoken') }}",
                data: { id:id}, 
                success: function( msg ) {
                    
                    $('#'+id).remove();
                }
            });
        }else {
            alert('Select Id.');
        }
       
    });


});

function focusfunction(id,name,doctor,pname,packexp,lastid){
     $("."+id).focus();
     var regid = id;
      $(".rxdatavaltable tr").click(function() {
        var selected = $(this).hasClass("highlight");
        //alert(selected);
        $(".rxdatavaltable tr").removeClass("highlight");
        if(!selected ||selected){
                $(this).addClass("highlight");
        }
    });

     //$(".rxdatavaltable tr[id="+id+"]").css("background-color", "grey");
        var rid = id;
        $('.regid').val(id);
        $('.fname').val(name);
        $('.dname').val(doctor);
        $('.pname').val(pname);
        $('.packexp').val(packexp);
        $('.lastid').val(lastid);

    
}

function removefunction(id){
    
     $("."+id).focus();
     var tokenid = id;
      $(".addtokentable tr").click(function() {
        var selected = $(this).hasClass("highlight");
        //alert(selected);
        $(".addtokentable tr").removeClass("highlight");
        if(!selected ||selected){
                $(this).addClass("highlight");
        }
    });

      $('.tokenid').val(id);

    
}

function focusfunctionfamily(id,name,surname){
     $("."+id).focus();
     var regid = id;
      $(".familytable tr").click(function() {
        var selected = $(this).hasClass("highlight");
        //alert(selected);
        $(".familytable tr").removeClass("highlight");
        if(!selected ||selected){
                $(this).addClass("highlight");
        }
    });

     //$(".rxdatavaltable tr[id="+id+"]").css("background-color", "grey");
        var rid = id;
        $('.f_regid').val(id);
        $('.f_name').val(name);
        $('.f_surname').val(surname);

    
}

function removefunctionfamily(id){
    
     $("."+id).focus();
     var familyid = id;
      $(".addtokentablefamily tr").click(function() {
        var selected = $(this).hasClass("highlight");
        //alert(selected);
        $(".addtokentablefamily tr").removeClass("highlight");
        if(!selected ||selected){
                $(this).addClass("highlight");
        }
    });

      $('.familyid').val(id);

    
}

$('#search_id').on('keyup',function(){
    var value=$(this).val();
    
    $.ajax({
    type : 'get',
    url : "{{ url(config('laraadmin.adminRoute') . '/searchtokenid') }}",
    data:{'search':value},
    success:function(data){
        
        var splitted = data.split("/");
        $('.regid').val(splitted[0]);
        $('.fname').val(splitted[1]);
        $('.dname').val(splitted[2]);
        $('.lastid').val(splitted[3]);
    }

    });
});

function openHeightWeight(id){
   
    $('.reg_id').val(id);

    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/getheightweight') }}",
            data: { id:id }, 
            success: function( msg ) {
           
                var heightarr = JSON.parse(msg);
                var height= heightarr['height'];
                var weight= heightarr['weight'];
                var lmp= heightarr['lmp'];
                var gender= heightarr['gender'];
                var dateval= heightarr['dateval'];
                var rand_id= heightarr['rand_id'];
                var ht= heightarr['ht'];
                var wt= heightarr['wt'];
                var tddata= heightarr['tddata'];
                var datevalht= heightarr['datevalht'];

                $('.height_ad').val(height);
                $('.weight_ad').val(weight);
                $('.lmp').val(lmp);
                $('.gender').val(gender);
                $('.datevalht').val(dateval);
                $('.randht').val(rand_id);
                $('.ht').text(ht+" CM");
                $('.wt').text(wt+" KG");

                $('.datevalht1').val(datevalht);

                 $(".height-body").html(tddata);
                if(gender=="Male"){
                    $('.lmpdata').css("display","none");
                 
                }
                $('#AddHeightWeight').modal('show');
            }
        });
}

$(".saveweightheight").on("click", function(){
        var dateval = $('.datevalht').val();
        var height = $('.height_ad').val();
        var weight = $('.weight_ad').val();
        var lmp = $('.lmp').val();
        var rand1 = $('.randht').val();
        var regid = $('.reg_id').val();
        var datevalht = $('.datevalht1').val();
      //  if(datevalht!="")
        if(dateval!="" || height!="" || weight!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/saveCaseHeightWeights') }}",
                data: { regid:regid, dateval:dateval,height:height,weight:weight,lmp:lmp,rand:rand1}, 
                success: function( msg ) {
                    if(msg=="1"){
                    $('.heightweighttabledata_ad tr:first').after('<tr id="'+rand1+'"><td>'+dateval+'</td><td>'+height+'</td><td>'+weight+'</td><td>'+lmp+'</td></tr>');
                }
                else {
                     alert("Already added.");
                 }
                  
                }
            });
        }else {
            alert('All fields required.');
        }
    });

function openHistory(id){
   
   
    
    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/packagehistory') }}",
            data: { id:id }, 
            success: function( msg ) {
                console.log(msg);
                $(".pregid").val(id);
                $(".package-body").html(msg);
                $('#packagehistory').modal('show');
            }
        });
   
}

function deletepackage(id){ 
    var id = id;
    
    $.ajax({
            type: "POST",
            url: "{{ url(config('laraadmin.adminRoute') . '/deletepackage') }}",
            data: { id:id }, 
            success: function( msg ) {
                console.log(msg);
                 alert("Package Remove Successfully");
                   setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1000); 
               
            }
        });
   
}
function openPackage(id){
    $('#case_id').val(id);
    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/getpacakge') }}",
            data: { id:id }, 
            success: function( msg ) {
                 var packagearr = JSON.parse(msg);
                 var pacakge_expiry= packagearr['packageexpiry'];
                var today = packagearr['today'];
                var packid = packagearr['packid'];
                
                var pacakgestart = packagearr['pacakgestart'];
                var packagename = packagearr['packagename'];
                var regname = packagearr['regname'];
                var regmobile = packagearr['regmobile'];
                if(today > pacakge_expiry){
                    var expmsg = "Your Pacakge has been Expired";
                }
                $('.pacakge_expiry').text('Package End Date : '+ pacakge_expiry);
                
                if(pacakge_expiry!="N/A"){
                    $('.package_start').val(pacakge_expiry);
                    $('#expirypack').css("display","inline-block");

                }
                $('.packagename').text(packagename);
                $('.pacakgestart').text('Package Start Date : '+ pacakgestart);
                $('.regname').text(regname);
                $('.regcasename').val(regname);
                $('.regmobile').val(regmobile);
                $('.expmsg').text(expmsg);
                if(packid!="N/A"){
                    $(".savepicture").html("Update Package");
                }
                else{
                    $(".savepicture").html("Save");
                }
               $('#AddPackage').modal('show');
            }
        });

}

 

 function openFamily(id){
  
    $('.f_reg_id').val(id);
    
    var id = id;
    $.ajax({
            type: "GET",
            url: "{{ url(config('laraadmin.adminRoute') . '/familydata') }}",
            data: { id:id }, 
            success: function( msg ) {
              
                $(".searchfamily").html(msg);
                if(msg==""){
                    var data = "<tr><td style='padding: 14px;'></td><td style='padding: 14px;'></td></tr><tr><td style='padding: 14px;'></td><td style='padding: 14px;'></td></tr>";
                    $(".searchfamily").html(data);
                }
                var tddata = "<tr><td style='padding: 14px;'></td><td style='padding: 14px;'></td></tr><tr><td style='padding: 14px;'></td><td style='padding: 14px;'></td></tr>";
                $(".search-body").html(tddata);
                $('#AddFamily').modal('show');
            }
        });
   
}

$(function () {

    $(".searchbtn1").on("click", function(){
        var search_name = $('.search_name1').val();
        var search_surname = $('.search_surname').val();
        var search_regid = $('.search_regid1').val();
        var f_reg_id = $('.f_reg_id').val();
        if(search_name!="" || search_surname!="" || search_regid!=""){
            $.ajax({
                type: "GET",
                url: "{{ url(config('laraadmin.adminRoute') . '/searchcases') }}",
                data: { search_name:search_name, search_surname:search_surname, search_regid:search_regid,f_reg_id:f_reg_id}, 
                success: function( data ) {
                     if(data!=""){
                        $(".search-body").html(data);
                     }
                     else{
                        var tddata = "<tr><td colspan='2' style='text-align:center;'>No Record Found</td></tr>";
                        $(".search-body").html(tddata);
                     }

                }
            });  
            }
            else{
                alert("Fields required");
            }  
        
    });
});

$(".addbtnfamily").on("click", function(){
        var f_regid = $('.f_regid').val();
        var f_name = $('.f_name').val();
        var f_reg_id = $('.f_reg_id').val();
        var f_surname = $('.f_surname').val();
        if(f_reg_id!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/savefamily') }}",
                data: { f_regid:f_regid ,f_reg_id:f_reg_id,f_surname:f_surname,f_name:f_name }, 
                success: function( msg ) {
                    if(msg=="1"){
                        $('.addtokentablefamily tr:first').after('<tr id="'+f_regid+'"><td onclick="removefunction('+f_regid+')">'+f_regid+'</td><td onclick="removefunction('+f_regid+')">'+f_name+'</td></tr>');
                    }

                }
            });
        }else {
            alert('Select Id.');
        }
       
});

$(".removebtnfamily").on("click", function(){
        var id = $('.familyid').val();
      
        if(id!="" ){
            $.ajax({
                type: "POST",
                url: "{{ url(config('laraadmin.adminRoute') . '/deletefamily') }}",
                data: { id:id}, 
                success: function( msg ) {
                    
                    $('#'+id).remove();
                }
            });
        }else {
            alert('Select Id.');
        }
       
    });

$('.savepicture').on('click', function() {
        var regcasename = $('.regcasename').val();
        var packagemonths = $('.packagemonths').val();
        var regmobile = $('.regmobile').val();

        if (confirm("Dear "+ regcasename + " Your package has been renewed till "+ packagemonths + " Thanks for Renewal. Regards Homecare" + "\n Above SMS will be scheduled to be sent after 1 hour. Are you Sure you want schedule this SMS?" )) {

                $.ajax({
                    type: "GET",
                    url: "{{ url(config('laraadmin.adminRoute') . '/packagemessage') }}",
                    data: { regmobile:regmobile, regcasename:regcasename}, 
                    success: function( data ) {

                    }
                });  
             
               $('.packageform').submit();
            }
            else{
                $('.packageform').submit();
            }
    });
</script>
@endpush
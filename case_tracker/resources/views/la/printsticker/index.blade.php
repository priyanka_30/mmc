@extends("la.layouts.app")

@section("contentheader_title", "Print Stickers")




@section("main-content")

<style type="text/css" media="print">
  @page { size: landscape; }

  

</style>

<div class="box">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<div class="row">
			<?php

				for($i=1;$i<=$no; $i++){
			 ?>
			<div class="col-xs-2" style="text-align: center;border: 1px solid #ccc; border-radius: 10px;margin: 10px;">
				<p style="padding-top: 11px;">Homeo Clinic 1 </p>
				<p><?php echo $name;?></p>
				<p style="padding-bottom: 19px;"><span style="float:left;"><?php echo date('d M Y') ?></span> 
					<span style="float: right;">ID:<?php echo $regid; ?></span></p>

			</div>
		<?php } ?>
		<div class="col-xs-12">
			<button class="btn btn-success btnprint" onclick="printbtn();" >Print</button>
		</div>
		</div>
	</div>
</div>



@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
	function printbtn(){
		$('.btnprint').css('display','none');
		$('.main-footer').css('display','none');
		
		window.print();


	}
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/smstemplate_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
@endpush
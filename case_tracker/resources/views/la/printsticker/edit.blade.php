@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/daycharges') }}">SMS Templates</a> :
@endsection
@section("contentheader_description", $daycharge->$view_col)
@section("section", "SMS Template")
@section("section_url", url(config('laraadmin.adminRoute') . '/smstemplate'))
@section("sub_section", "Edit")

@section("htmlheader_title", "SMS Template Edit : ".$daycharge->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($daycharge, ['route' => [config('laraadmin.adminRoute') . '.smstemplate.update', $daycharge->id ], 'method'=>'PUT', 'id' => 'daycharge-edit-form']) !!}
					<!-- @la_form($module) -->

					<div class="box-body">
                    <div class="form-group">
                    	<label for="templatename">Template Name :</label>
                    	<input class="form-control" placeholder="Enter Template Name" name="templatename" type="text" value="<?php echo $daycharge->templatename; ?>">
                    </div>
                    <div class="form-group">
                    	<label for="message">Message :</label>
                    	<textarea class="form-control" placeholder="Enter Message" name="message" type="text" rows="5"><?php echo $daycharge->message; ?></textarea>
                    </div>
                    <div class="form-group"><label for="smstype">SMS Type :</label>
                    <!-- 	<input class="form-control" placeholder="Enter SMS Type" name="smstype" type="text" value=""> -->
                    <label class="container-radio">Normal
                              <input type="radio" name="smstype" class="send_sms" value="Normal" <?php if($daycharge->smstype == "Normal"){ echo "checked"; } ?> >
                              <span class="checkmark-radio"></span>
                            </label>
                            <label class="container-radio">Birthday
                              <input type="radio" name="smstype" class="send_sms" value="Birthday" <?php if($daycharge->smstype == "Birthday"){ echo "checked"; } ?>>
                              <span class="checkmark-radio"></span>
                            </label>
                             <label class="container-radio">Courier
                              <input type="radio" name="smstype" class="send_sms" value="Courier" <?php if($daycharge->smstype == "Courier"){ echo "checked"; } ?>>
                              <span class="checkmark-radio"></span>
                            </label>
                             </div>					
									</div>
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'tags')
					@la_input($module, 'color')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/daycharges') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#daycharge-edit-form").validate({
		
	});
});
</script>
@endpush

@extends("la.layouts.app")

@section("contentheader_title", "Bankdeposit")
@section("contentheader_description", "Bankdeposit listing")
@section("section", "Bankdeposit")
@section("sub_section", "Listing")
@section("htmlheader_title", "Bankdeposit Listing")

@section("headerElems")
@la_access("Bankdeposit", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Bankdeposit</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
					<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
           
        </tbody>
		</table>
	</div>
</div>

@la_access("Bankdeposit", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Bankdeposit</h4>
			</div>
			{!! Form::open(['action' => 'LA\BankdepositController@savedeposit', 'id' => 'daycharges-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" name="">
                    <div class="form-group col-md-12">
						<label for="mobile" >Date</label>
						<div id="datepicker" class="input-group date datepicker" data-date-format="dd-mm-yyyy">
						    <input class="form-control" type="text" name="deposit_date" readonly />
						    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
						</div>
					</div>
					
					
					<div class="form-group col-md-12">
						<label for="mobile">Amount :</label>
						<input class="form-control" placeholder="Amount"  name="amount" type="text" value="" aria-required="true"required>
					</div>
					
				
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
      //  serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/bankdeposit_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#daycharges-add-form").validate({
		
	});
});
</script>
<script type="text/javascript">
	$(function () {
		$(".datepicker").datepicker({
		  format:'dd/mm/yyyy', 
		    autoclose: true, 
		    todayHighlight: true,
		}).l('update', new Date());
	});
</script>
@endpush
@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/medicalcases') }}">Medicalcase</a> :
@endsection
@section("contentheader_description", $medicalcase->$view_col)
@section("section", "Medicalcase")
@section("section_url", url(config('laraadmin.adminRoute') . '/medicalcases'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Medicalcase Edit : ".$medicalcase->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="POST" action="http://smartops.co.in/managemyclinic/admin/medicalcases/1" accept-charset="UTF-8" id="medicalcase-edit-form" novalidate="novalidate"><input name="_method" type="hidden" value="PUT">
					<input name="_token" type="hidden" value="t7zf9XsgafWGkIcUKczXQBWtg88Eb15RrewvRMiR">
													<div class="form-group col-md-6">
														<label for="mobile">Mobile* :</label>
														<input class="form-control"  name="mobile1" type="text" value="{{ $medicalcase->mobile1 }}" aria-required="true"></div>
														<div class="form-group col-md-6">
														<label for="mobile">Mobile2* :</label>
														<input class="form-control" name="mobile2" type="text" value="{{ $medicalcase->mobile2 }}" aria-required="true"></div>
														<div class="form-group col-md-6">
														<label for="mobile">Pin* :</label>
														<input class="form-control" name="pin" type="text" value="{{ $medicalcase->pin }}" aria-required="true"></div>
														<div class="form-group col-md-6">
														<label for="mobile">State* :</label>
														<input class="form-control"required="1" name="" type="text" value="{{ $medicalcase->state }}" aria-required="true"></div>
														<div class="form-group col-md-6">
														<label for="mobile">Gender* :</label>
														<input class="form-control"required="1" name="" type="text" value="{{ $medicalcase->gender }}" aria-required="true"></div>
														<div class="form-group col-md-6"><label for="city">City :</label>
														<input class="form-control" name="city" type="text" value="{{ $medicalcase->city }}"></div>
														<div class="form-group col-md-6">
														<label for="mobile">Phone* :</label>
														<input class="form-control" name="phone" type="text" value="{{ $medicalcase->phone }}" aria-required="true"></div>
														<div class="form-group col-md-6">
														<label for="mobile">Religion* :</label>
														<input class="form-control" name="religion" type="text" value="{{ $medicalcase->religion }}" aria-required="true"></div>
														<div class="form-group col-md-6">
														<label for="occupation">Occupation* :</label>
														<input class="form-control" name="occupation" type="text" value="{{ $medicalcase->occupation }}" aria-required="true"></div>
														<div class="form-group col-md-6">
														<label for="status">Status* :</label>
														<input class="form-control" name="status" type="text" value="{{ $medicalcase->status }}" aria-required="true"></div>
														<div class="form-group col-md-6">
														<label for="dob">DOB* :</label>
														<input class="form-control" name="dob" type="text" value="{{ $medicalcase->dob }}" aria-required="true"></div>
														<div class="form-group col-md-6">
														<label for="area">Area/Sector* :</label>
														<input class="form-control" name="mobile2" type="text" value="{{ $medicalcase->area }}" aria-required="true"></div>	
														<div class="form-group">
															<label for="address">Address :</label><input class="form-control" name="address" type="text" value="{{ $medicalcase->address }}" aria-required="true"></div>				
					                    <br>
					<div class="form-group">
						<input class="btn btn-success" type="submit" value="Update"> <button class="btn btn-default pull-right"><a href="http://smartops.co.in/managemyclinic/admin/medicalcases">Cancel</a></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#medicalcase-edit-form").validate({
		
	});
});
</script>
@endpush

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ LAConfigs::getByKey('site_description') }}">
    <meta name="author" content="Dwij IT Solutions">

    <meta property="og:title" content="{{ LAConfigs::getByKey('sitename') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="{{ LAConfigs::getByKey('site_description') }}" />
    
    <meta property="og:url" content="http://laraadmin.com/" />
    <meta property="og:sitename" content="laraAdmin" />
    <meta property="og:image" content="http://demo.adminlte.acacha.org/img/LaraAdmin-600x600.jpg" />
    
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@laraadmin" />
    <meta name="twitter:creator" content="@laraadmin" />
    
    <title>{{ LAConfigs::getByKey('sitename') }}</title>
    
    <!-- Bootstrap core CSS -->
</head>
<style>
.header1{
width:100%;
float:left;
background-color:#0d679f;
position:fixed;
padding-right:10px;
display: block;
  }
  .header1 img{
   padding-top:24px;
   float:left;
   padding-left: 130px;
  }
.header{
height:300px;
width:1120px;
float:left;
background-color:#0d679f;
  }
ul{
    list-style-type:none;
    float:right;
    }
    li{
    display:inline-block;
    }
    ul li a{
    color:black;
    margin:0 50px;
    text-decoration:none;
    font-size:22px;
    }
    .button{
    font-family: "Roboto", Sans-serif;
    font-size: 18px;
   color:#ffffff;
   border-style: solid;
   float:right;
   padding:10px;
    border-width: 2px 2px 2px 2px;
   border-radius:25px 25px 25px 25px;
   top:20px;
   margin:9px auto
   }
    .header-bottom {
    width: 100%;
    height:330px;
    float: left;
}
.header-bottom1 img{
    height:100%;
    width:100%;
}
.write{
    float:left;
    position:absolute;
    padding: 55px 0px 0px 0px;
}
.write p{
    float:left;
    font-size:48px;
    margin-left: 200px;
    color:#ffffff;
}
.write span {
    width: 100%;
    float:left;
    font-size:25px;
    margin-left: 157px;
    color:#ffffff;
}
.main1{
    height: auto !important;
    width:50%;
    float:left;
    background-color:#ffffff;
    position:relative;
    margin-left:10%;
    margin-right:10%;
}
.main1 img{
    width:500px;
    margin-left:4%;

}
</style>
<body data-spy="scroll" data-offset="0" data-target="#navigation">
<!-- Fixed navbar -->
<div id="navigation" class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-collapse collapse">
            <div class="header1">
        <img src="http://case.metaschool8.com/caselogo.jpeg" height="10px">
    <div class="header">
<div class="button">Login</div>
<ul>  
        <li><a href="#">Our Solutions</a></li>
        <li><a href="#">Resources</a></li>
        <li><a href="#">About Us</a></li>
        
</ul>
</div>
            
        </div>

        <div class="header-bottom">
    <div class="write">
        <p>About Us.</p></br><span>
            Together, making healthcare better as the most</br>
            trusted surgical process innovation company.</span>
    </div>
</div>

<div class="main1">
    <img src="image/main.jpg"></div><!--/.nav-collapse -->
    </div>
</div>

<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/la-assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
</body>
</html>

@extends('la.layouts.auth')

@section('htmlheader_title')
    Register
@endsection

@section('content')

    <body class="hold-transition register-page">
    <div class="register-box">
        <div class="register-logo">
            <a href="{{ url('/home') }}"><b>{{ LAConfigs::getByKey('sitename_part1') }} </b></a>
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="register-box-body" style="margin-top: 2%;">
            <p class="login-box-msg">Register Patient</p>
            <form action="{{ url('/patientregister') }}" method="post" onsubmit="return checkForm(this)">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <!-- <div class="box-body"> -->
                   
                    <div class="form-group">
                        <label for="name">Name* :</label>
                        <input class="form-control" placeholder="Enter Name" data-rule-minlength="5" data-rule-maxlength="250" required="1" name="name" type="text" value="" aria-required="true">
                    </div>
                    <div class="form-group">
                        <label for="gender">Gender* : </label><br>
                        <div class="radio">
                            <label><input checked="checked" name="gender" type="radio" value="Male"> Male </label>
                            <label><input name="gender" type="radio" value="Female"> Female </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mobile">Mobile* :</label>
                        <input class="form-control" placeholder="Enter Mobile" min="0"  required="1" name="mobile" type="number" value="" aria-required="true">
                    </div>
                    <div class="form-group">
                        <label for="email">Email* :</label>
                        <input class="form-control" placeholder="Enter Email" data-rule-minlength="5" data-rule-maxlength="250" required="1" data-rule-email="true" name="email" type="email" value="" aria-required="true">
                    </div>
                     <div class="form-group">
                        <label for="address">Password :</label>
                        <input class="form-control" placeholder="Enter Password" name="password" type="password" value=""  required>
                          <span id="password_strength" style="color: red;"></span>
                    </div>
                     <div class="form-group">
                        <label for="address">Address :</label>
                        <textarea class="form-control" placeholder="Enter Address" data-rule-maxlength="1000" cols="30" rows="3" name="address"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="city">City :</label>
                        <input class="form-control" placeholder="Enter City" data-rule-maxlength="50" name="city" type="text" value="">
                    </div>
                   
                    <div class="form-group">
                        <label for="role">Role* :</label>
                        <select class="form-control" required="1" data-placeholder="Select Role" rel="select2" name="role">
                            <option value="5">Patient</option>
                        </select>
                    </div>
                 
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                    </div><!-- /.col -->
               
            </form>

            @include('auth.partials.social_login')
            <hr>
            <center><a href="{{ url('/login') }}" class="text-center">Log In</a></center>
        </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    @include('la.layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });

        function checkForm(form)
  {
  
    if(form.password.value != "" ) {
      if(form.password.value.length < 6) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least six characters!";
       
        form.password.focus();
        return false;
      }
           re = /[0-9]/;
      if(!re.test(form.password.value)) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least one number (0-9)!";
        form.password.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(form.password.value)) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least one lowercase letter (a-z)!";
         
        form.password.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(form.password.value)) {
        document.getElementById("password_strength").innerHTML = "Password must contain at least one uppercase letter (A-Z)!";
                form.password.focus();
        return false;
      }
    } else {
      form.password.focus();
      return false;
    }

  }
    </script>
</body>

@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ LAConfigs::getByKey('site_description') }}">
    <meta name="author" content="Dwij IT Solutions">

    <meta property="og:title" content="{{ LAConfigs::getByKey('sitename') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="{{ LAConfigs::getByKey('site_description') }}" />
    
    <meta property="og:url" content="http://laraadmin.com/" />
    <meta property="og:sitename" content="laraAdmin" />
    <meta property="og:image" content="http://demo.adminlte.acacha.org/img/LaraAdmin-600x600.jpg" />
    
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@laraadmin" />
    <meta name="twitter:creator" content="@laraadmin" />
    
    <title>{{ LAConfigs::getByKey('sitename') }}</title>
    
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/la-assets/css/bootstrap.css') }}" rel="stylesheet">

    <link href="{{ asset('la-assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- Custom styles for this template -->
    <link href="{{ asset('/la-assets/css/main.css') }}" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

    <script src="{{ asset('/la-assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('/la-assets/js/smoothscroll.js') }}"></script>
    <style type="text/css">
        .register:hover{
            .btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
        }

       .carousel-caption {
  position: absolute;
  left: 0%;
  right: 15%;
  bottom: 20px;
  z-index: 10;
  padding-top: 0px;
  padding-bottom: 3rem;
  text-align: left;
 
}
@media only screen and (max-width: 600px) {
.carousel-caption {
    left: 10%;
    right: 20%;
    padding-bottom: 0px;
  }

   .carousel-caption {
  position: absolute;
  left: 10%;
  right: 15%;
  bottom: 20px;
  top: 24px;
  z-index: 10;
  padding-top: 0px;
  padding-bottom: 0px;
  text-align: left;
 
}
.caption-h{
  	font-size: 14px; 
  	color: #128212;
  }

  .caption-p {
  	color: #333;
  	font-size: 14px;
  	margin-left: 6px;
  }
}
@media screen and (min-width: 768px){
 .carousel-caption {
    left: 10%;
    right: 20%;
    padding-bottom: 24rem;
  }

  .caption-h{
  	font-size: 34px; 
  	color: #128212;
  }

  .caption-p {
  	color: #333;
  	font-size: 22px;
  	margin-left: 6px;
  }
}

.navbar-default {
    background-color: #5cb85c;
    border-color: #5cb85c;
}
.navbar-default .navbar-nav > li > a {
    color: #fff;
}

.navbar-default .navbar-brand {
    color: #fff;
}

    </style>

</head>

<body data-spy="scroll" data-offset="0" data-target="#navigation">

<!-- Fixed navbar -->
<div id="navigation" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><b>{{ LAConfigs::getByKey('sitename') }}</b></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{ url('/') }}" class="smoothScroll">Home</a></li>
                <li><a href="{{ url('/#') }}" class="smoothScroll">About</a></li>
                <li><a href="{{ url('/contactus') }}" class="smoothScroll">Contact</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li><a href="{{ url('/patientsignup') }}">Register</a></li>
                    <li><a href="{{ url('/login') }}">Log In</a></li>
                    
                @else
                    <li><a href="{{ url(config('laraadmin.adminRoute')) }}">{{ Auth::user()->name }}</a></li>
                @endif
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>


<section id="home" name="home"></section>
<div  style="background-color: #ffffff;">
	<div>
  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="{{ asset('la-assets/img/slider1.jpg') }}" alt="Los Angeles" style="width:100%;">
        <div class="carousel-caption">
          <h3 class="caption-h" >Welcome to our Medical Care Center </h3>
          <p class="caption-p">We take care our patients health</p>
        </div>
      </div>

      <div class="item">
        <img src="{{ asset('la-assets/img/slider2.jpg') }}" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <h3 class="caption-h">Welcome to our Medical Care Center </h3>
          <p class="caption-p">We take care our patients health</p>
        </div>
      </div>
    
      <div class="item">
        <img src="{{ asset('la-assets/img/slider3.jpg') }}" alt="New York" style="width:100%;">
        <div class="carousel-caption">
         <h3 class="caption-h">Welcome to our Medical Care Center </h3>
          <p class="caption-p">We take care our patients health</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

   <!--  <div class="container">
        <div class="row centered">
            <div class="col-lg-12">
                <img src="https://cdn2.iconfinder.com/data/icons/color-svg-vector-icons-part-2/512/add_plus_new_user_green_page-512.png" height="200">
                @if (Auth::guest())
                <h3><a href="{{ url('/login') }}" class="btn btn-lg btn-success">Get Started!</a></h3><br>
                 @else
                 <h3><a href="{{ url(config('laraadmin.adminRoute')) }}" class="btn btn-lg btn-success">Get Started!</a></h3><br>
                 @endif
            </div>
            
        </div>
    </div> --> <!--/ .container -->
</div><!--/ #headerwrap -->


<section id="about" name="about"></section>
<!-- INTRO WRAP -->

<div style="padding-top: 40px;">
	<div class="container">
		<div class="col-md-4">
			<p><iframe  src="https://www.youtube.com/embed/ScMzIvxBSi4"></iframe></p>
		</div>

		<div class="col-md-4">
			<p><iframe  src="https://www.youtube.com/embed/ScMzIvxBSi4"></iframe></p>
		</div>

		<div class="col-md-4">
			<p><iframe  src="https://www.youtube.com/embed/ScMzIvxBSi4"></iframe></p>
		</div>
	</div>

</div>

<section id="contact" name="contact"></section>
<div id="footerwrap">
    <div class="container">
        <div class="col-lg-5">
            <h3>Contact Us</h3><br>
            <p>
                Manage My Clinic,<br/>
                clinic management company,<br/>
            </p>
            <div class="contact-link"><i class="fa fa-envelope-o"></i> <a href="mailto:hello@managemyclinic.com">hello@managemyclinic.com</a></div>
        </div>

        <div class="col-lg-7">
            <h3>Drop Us A Line</h3>
            <br>
            <form role="form" action="#" method="post" enctype="plain">
                <div class="form-group">
                    <label for="name1">Your Name</label>
                    <input type="name" name="Name" class="form-control" id="name1" placeholder="Your Name">
                </div>
                <div class="form-group">
                    <label for="email1">Email address</label>
                    <input type="email" name="Mail" class="form-control" id="email1" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label>Your Text</label>
                    <textarea class="form-control" name="Message" rows="3"></textarea>
                </div>
                <br>
                <button type="submit" class="btn btn-large btn-success">SUBMIT</button>
            </form>
        </div>
    </div>
</div>
<div id="c">
    <div class="container">
        <p>
            <strong>Copyright &copy; 2020. Powered by <a href="#"><b>Manage My Clinic</b></a>
        </p>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/la-assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
</body>
</html>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<style type="text/css">
	/* FONTS */
    @media screen {
		@font-face {
		  font-family: 'Lato';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
		}
		
		@font-face {
		  font-family: 'Lato';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
		}
		
		@font-face {
		  font-family: 'Lato';
		  font-style: italic;
		  font-weight: 400;
		  src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
		}
		
		@font-face {
		  font-family: 'Lato';
		  font-style: italic;
		  font-weight: 700;
		  src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
		}
    }
    
    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
    table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    img { -ms-interpolation-mode: bicubic; }

    /* RESET STYLES */
    img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
    table { border-collapse: collapse !important; }
    body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    /* ANDROID CENTER FIX */
    div[style*="margin: 16px 0;"] { margin: 0 !important; }
</style>
</head>
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Lato', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Looks like you tried signing in a few too many times. Let's see if we can get you back into your account.
</div>

<p>&nbsp;</p>
<!-- HIDDEN PREHEADER TEXT -->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td align="center" bgcolor="#f4f4f4">
<table border="0" width="480" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0; color: #000001; font-size: 22px;" align="left" valign="top" bgcolor="#f4f4f4">&nbsp;</td>
</tr>
<tr>
<td style="padding: 40px 10px 40px 30px; color: #000001; font-size: 22px;" align="left" valign="top" bgcolor="#ffffff">Reset Password</td>
</tr>
</tbody>
</table>
</td>
</tr>
<!-- LOGO --> <!-- HERO --> <!-- COPY BLOCK -->
<tr>
<td style="padding: 0px 10px 0px 10px;" align="center" bgcolor="#f4f4f4">
 
<table border="0" width="480" cellspacing="0" cellpadding="0">  
    <tbody>
      <tr>
        <td bgcolor="#ffffff">
<table border="0" width="460" cellspacing="0" cellpadding="0" style="margin-left:10px;border:1px solid #828282;  border-radius:10px;">
<tbody>
<tr style="height: 56px;">
<td style="padding: 30px 10px; height: 56px;" align="center" valign="top" bgcolor="#ffffff"><a href="#" target="_blank" rel="noopener"> <img style="display: block; font-family: 'Lato', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;" src="http://smartops.co.in/managemyclinic/key.png" alt="Logo" border="0" /> </a></td>
</tr>
<tr>
<td style="padding: 10px 30px 10px 30px; color: #2a3439; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 27px; font-weight: 700; line-height: 25px;" align="center" bgcolor="#ffffff">
<p style="margin: 0;">HoldThatChair</p>
</td>
</tr>
<!-- COPY -->
<tr style="height: 25px;">
<td style="padding: 10px 30px 20px; color: #2a3439; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 25px; height: 25px;" align="center" bgcolor="#ffffff">
<p style="margin: 0 70px;">Hello click the button below to reset your password</p>
</td>
</tr>
<tr>
<td align="center" bgcolor="#ffffff">
<div style="width: 80%;"><a style="font-size: 20px; background-color: #392f2a; font-family: Helvetica, Arial, sans-serif; margin: 0 30px; color: #ffffff; text-decoration: none; padding: 15px 55px; border-radius: 3px; border: 1px solid #392F2A; display: inline-block;" href="#" target="_blank" rel="noopener">RESET PASSWORD</a></div>
</td>
</tr>
  <tr>
<td style="padding: 0; color: #000001; font-size: 22px;" align="left" valign="top" bgcolor="#ffffff">&nbsp;</td>
</tr>
  
<!-- BULLETPROOF BUTTON -->
 </tbody>
</table>
    </td>
   </tr>
   </tbody> 
 </table>
</td>
</tr>
<!-- FOOTER -->
<tr>
<td style="padding: 0px 10px 0px 10px;" align="center" bgcolor="#f4f4f4">
<table border="0" width="480" cellspacing="0" cellpadding="0"><!-- PERMISSION REMINDER -->
<tbody>
<tr>
<td style="padding: 20px 30px 30px 30px; color: #747576; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 11px; font-weight: 400; line-height: 18px;" align="center" bgcolor="#ffffff">
<p style="margin: 0;">Your received this email to let you know about important changes to<br /> your HoldYourChair Account and Services.<br /> &copy;2020 HoldThatChair, Chicago, USA</p>
</td>
</tr>
  <tr>
<td style="padding: 0; color: #000001; font-size: 22px;" align="left" valign="top" bgcolor="#f4f4f4">&nbsp;</td>
</tr>
<!-- ADDRESS --></tbody>
</table>
</td>
</tr>
</tbody>
</table>


</body>
</html>
<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasePotencynew extends Model
{
    use SoftDeletes;
	
	protected $table = 'case_potencies1';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	//protected $dates = ['deleted_at'];
}

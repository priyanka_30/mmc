<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaseData extends Model
{
    use SoftDeletes;
	
	protected $table = 'case_datas';
	
	protected $hidden = [
        
    ];

	
	//protected $dates = ['deleted_at'];
	protected $guarded = [];
}

<?php

/* ================== Homepage ================== */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/contactus', 'HomeController@contactus');
Route::get('/aboutus', 'HomeController@aboutus');
Route::get('/patientsignup', 'HomeController@patientsignup');
Route::post('/patientregister', 'HomeController@patientregister');
Route::post('/createdetails','HomeController@createdetails');
Route::post('/searchdetails','HomeController@searchdetails');
Route::post('/fetchdetails','HomeController@fetchdetails');
Route::post('/updateprofile','HomeController@updateprofile');

Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	Route::post(config('laraadmin.adminRoute'). '/loadmoredata', 'LA\DashboardController@loadmoredata');
    Route::post(config('laraadmin.adminRoute') . '/sendbirthdy','LA\DashboardController@sendbirthdy');
    Route::post(config('laraadmin.adminRoute') . '/sendchats','LA\DashboardController@sendchats');
    Route::post(config('laraadmin.adminRoute') . '/sendmail','LA\DashboardController@sendmail');
    Route::get(config('laraadmin.adminRoute') . '/fetch','LA\DashboardController@fetch');

    /* ================== Migration ================== */
	
	//Route::get(config('laraadmin.adminRoute'), 'LA\MigrationController@index');
	Route::get(config('laraadmin.adminRoute'). '/migration', 'LA\MigrationController@index');
    Route::post(config('laraadmin.adminRoute') . '/savepersonaldata','LA\MigrationController@savepersonaldata');
    Route::post(config('laraadmin.adminRoute') . '/savecommunicationdetails','LA\MigrationController@savecommunicationdetails');
    Route::post(config('laraadmin.adminRoute') . '/savedoctorsdetail','LA\MigrationController@savedoctorsdetail');
    Route::post(config('laraadmin.adminRoute') . '/saveheightweightdetails','LA\MigrationController@saveheightweightdetails');
    Route::post(config('laraadmin.adminRoute') . '/savereminderdetails','LA\MigrationController@savereminderdetails');
    Route::post(config('laraadmin.adminRoute') . '/savehomeodetails','LA\MigrationController@savehomeodetails');
    Route::post(config('laraadmin.adminRoute') . '/savefollowupdetails','LA\MigrationController@savefollowupdetails');
    Route::post(config('laraadmin.adminRoute') . '/saveadditionalcharges','LA\MigrationController@saveadditionalcharges');
    Route::post(config('laraadmin.adminRoute') . '/saveinvestigation','LA\MigrationController@saveinvestigation');
    Route::post(config('laraadmin.adminRoute') . '/savefollowupnotes','LA\MigrationController@savefollowupnotes');
    Route::post(config('laraadmin.adminRoute') . '/savebill','LA\MigrationController@savebill');

     /* ================== Group SMS ================== */
	
	//Route::get(config('laraadmin.adminRoute'), 'LA\MigrationController@index');
	Route::get(config('laraadmin.adminRoute'). '/groupsms', 'LA\GroupsmsController@index');
	Route::post(config('laraadmin.adminRoute') . '/sendgroupsms','LA\GroupsmsController@sendgroupsms');

    //Route::get(config('laraadmin.adminRoute'), 'LA\MigrationController@index');
	Route::get(config('laraadmin.adminRoute'). '/apitest', 'LA\ApitestController@index');
	Route::post(config('laraadmin.adminRoute') . '/createdetails','LA\ApitestController@createdetails');
   
    

	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/employees/change_password/{id}', 'LA\EmployeesController@change_password');
	Route::get(config('laraadmin.adminRoute') . '/employees/assign/{id}', 'LA\EmployeesController@assign');
	Route::post(config('laraadmin.adminRoute') . '/employees/savepackages/', 'LA\EmployeesController@savepackages');

	/* ================== Doctor ================== */
	Route::resource(config('laraadmin.adminRoute') . '/doctors', 'LA\DoctorsController');
	Route::get(config('laraadmin.adminRoute') . '/doctor_dt_ajax', 'LA\DoctorsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/doctors/change_password/{id}', 'LA\DoctorsController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/doctors/update_status/{id}', 'LA\DoctorsController@update_status');
	Route::post(config('laraadmin.adminRoute') . '/doctors/delete_status/{id}', 'LA\DoctorsController@delete_status');
	Route::post(config('laraadmin.adminRoute') . '/doctors/newcase/', 'LA\DoctorsController@newcase');
   	Route::post(config('laraadmin.adminRoute') . '/doctors/save/', 'LA\DoctorsController@save');
   	Route::post(config('laraadmin.adminRoute') . '/doctors/sendsms/', 'LA\DoctorsController@sendsms');

	/* ================== Clinicadmin ================== */
	Route::resource(config('laraadmin.adminRoute') . '/clinicadmins', 'LA\ClinicadminsController');
	Route::get(config('laraadmin.adminRoute') . '/clinicadmin_dt_ajax', 'LA\ClinicadminsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/clinicadmins/change_password/{id}', 'LA\ClinicadminsController@change_password');


	/* ================== Receptionist ================== */
	Route::resource(config('laraadmin.adminRoute') . '/receptionists', 'LA\ReceptionistsController');
	Route::get(config('laraadmin.adminRoute') . '/receptionist_dt_ajax', 'LA\ReceptionistsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/receptionists/change_password/{id}', 'LA\ReceptionistsController@change_password');

	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');
	
	/* ================== Accounts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/accounts', 'LA\AccountsController');
	Route::get(config('laraadmin.adminRoute') . '/account_dt_ajax', 'LA\AccountsController@dtajax');
    Route::post(config('laraadmin.adminRoute') . '/accounts/change_password/{id}', 'LA\AccountsController@change_password');

	/* ================== Package ================== */
	Route::resource(config('laraadmin.adminRoute') . '/packages', 'LA\PackagesController');
	Route::get(config('laraadmin.adminRoute') . '/package_dt_ajax', 'LA\PackagesController@dtajax');
	
	/* ================== Daycharges ================== */
	Route::resource(config('laraadmin.adminRoute') . '/daycharges', 'LA\DaychargesController');
	Route::get(config('laraadmin.adminRoute') . '/daycharge_dt_ajax', 'LA\DaychargesController@dtajax');


	/* ================== Dispensary ================== */
	Route::resource(config('laraadmin.adminRoute') . '/dispensaries', 'LA\DispensariesController');
	Route::get(config('laraadmin.adminRoute') . '/dispensary_dt_ajax', 'LA\DispensariesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/dispensaries/change_password/{id}', 'LA\DispensariesController@change_password');

	
	/* ================== Payment ================== */
	Route::resource(config('laraadmin.adminRoute') . '/payments', 'LA\PaymentsController');
	Route::get(config('laraadmin.adminRoute') . '/payment_dt_ajax', 'LA\PaymentsController@dtajax');
	
	/* ================== Stocks ================== */
	Route::resource(config('laraadmin.adminRoute') . '/stocks', 'LA\StocksController');
	Route::get(config('laraadmin.adminRoute') . '/stock_dt_ajax', 'LA\StocksController@dtajax');

	/* ================== Medicalcase ================== */
	Route::resource(config('laraadmin.adminRoute') . '/medicalcases', 'LA\MedicalcasesController');
	Route::get(config('laraadmin.adminRoute') . '/medicalcase_dt_ajax', 'LA\MedicalcasesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/newcaseadd', 'LA\MedicalcasesController@newcaseadd');

	Route::post(config('laraadmin.adminRoute') . '/packagesave','LA\MedicalcasesController@packagesave');
	Route::post(config('laraadmin.adminRoute') . '/usersendsms','LA\MedicalcasesController@usersendsms');
	Route::post(config('laraadmin.adminRoute') . '/deletefamily','LA\MedicalcasesController@deletefamily');
	Route::post(config('laraadmin.adminRoute') . '/savefamily','LA\MedicalcasesController@savefamily');
	Route::get(config('laraadmin.adminRoute') . '/searchfamily', 'LA\MedicalcasesController@searchfamily');
	Route::get(config('laraadmin.adminRoute') . '/familydata', 'LA\MedicalcasesController@familydata');
	Route::get(config('laraadmin.adminRoute') . '/packagehistory', 'LA\MedicalcasesController@packagehistory');
	Route::get(config('laraadmin.adminRoute') . '/searchcases','LA\MedicalcasesController@searchcases');
	Route::get(config('laraadmin.adminRoute') . '/getheightweight', 'LA\MedicalcasesController@getheightweight');
	Route::get(config('laraadmin.adminRoute') . '/getpacakge', 'LA\MedicalcasesController@getpacakge');
	Route::post(config('laraadmin.adminRoute') . '/fetch', 'LA\MedicalcasesController@fetch');
	Route::post(config('laraadmin.adminRoute') . '/generatePDF','LA\MedicalcasesController@generatePDF');
	Route::post(config('laraadmin.adminRoute') . '/fetchmedicine', 'LA\MedicalcasesController@fetchmedicine');
    Route::post(config('laraadmin.adminRoute') . '/create', 'LA\MedicalcasesController@create');
    Route::post(config('laraadmin.adminRoute') . '/referedsms', 'LA\MedicalcasesController@referedsms');
    Route::post(config('laraadmin.adminRoute') . '/basicdetail','LA\MedicalcasesController@basicdetail');
    Route::post(config('laraadmin.adminRoute') . '/saveremidy','LA\MedicalcasesController@saveremidy');
    Route::post(config('laraadmin.adminRoute') . '/updateremidy','LA\MedicalcasesController@updateremidy');
    Route::post(config('laraadmin.adminRoute') . '/updatepotency','LA\MedicalcasesController@updatepotency');
    Route::post(config('laraadmin.adminRoute') . '/updatefrequency','LA\MedicalcasesController@updatefrequency');
    Route::post(config('laraadmin.adminRoute') . '/updaterxdays','LA\MedicalcasesController@updaterxdays');
 	Route::post(config('laraadmin.adminRoute') . '/updateprescription','LA\MedicalcasesController@updateprescription');
 	Route::post(config('laraadmin.adminRoute') . '/copyremidy','LA\MedicalcasesController@copyremidy');
    
    Route::post(config('laraadmin.adminRoute') . '/saveCaseHeightWeights','LA\MedicalcasesController@saveCaseHeightWeights');
    Route::post(config('laraadmin.adminRoute') . '/saveHeightWeights','LA\MedicalcasesController@saveHeightWeights');
    Route::post(config('laraadmin.adminRoute') . '/updateHeightWeights','LA\MedicalcasesController@updateHeightWeights');
    Route::post(config('laraadmin.adminRoute') . '/examinationdetail','LA\MedicalcasesController@examinationdetail');
    Route::post(config('laraadmin.adminRoute') . '/updateexaminationdetail','LA\MedicalcasesController@updateexaminationdetail');
    Route::post(config('laraadmin.adminRoute') . '/updatecommunicationdetail','LA\MedicalcasesController@updatecommunicationdetail');
 	Route::post(config('laraadmin.adminRoute') . '/updateinvestigationdetail','LA\MedicalcasesController@updateinvestigationdetail');
    Route::post(config('laraadmin.adminRoute') . '/updatehomeodetail','LA\MedicalcasesController@updatehomeodetail');
    Route::post(config('laraadmin.adminRoute') . '/picturedetail','LA\MedicalcasesController@picturedetail');
 	Route::post(config('laraadmin.adminRoute') . '/savenotes','LA\MedicalcasesController@savenotes');
 	Route::post(config('laraadmin.adminRoute') . '/updatenotes','LA\MedicalcasesController@updatenotes');
    
    Route::post(config('laraadmin.adminRoute') . '/deletepackage','LA\MedicalcasesController@deletepackage');
    Route::post(config('laraadmin.adminRoute') . '/deletedata','LA\MedicalcasesController@deletedata');
    Route::post(config('laraadmin.adminRoute') . '/deletecharges','LA\MedicalcasesController@deletecharges');
    Route::post(config('laraadmin.adminRoute') . '/deleteheightdata','LA\MedicalcasesController@deleteheightdata');
    Route::post(config('laraadmin.adminRoute') . '/deletecommunication','LA\MedicalcasesController@deletecommunication');
    Route::post(config('laraadmin.adminRoute') . '/deleteexamination','LA\MedicalcasesController@deleteexamination');
 	Route::post(config('laraadmin.adminRoute') . '/deleteinvestigation','LA\MedicalcasesController@deleteinvestigation');
 	Route::post(config('laraadmin.adminRoute') . '/deletepicture','LA\MedicalcasesController@deletepicture');
 	Route::get(config('laraadmin.adminRoute') . '/getbilldetails','LA\MedicalcasesController@getbilldetails');
 	Route::get(config('laraadmin.adminRoute') . '/search','LA\MedicalcasesController@search');
 	Route::get(config('laraadmin.adminRoute') . '/searchcb','LA\MedicalcasesController@searchcb');
 	Route::get(config('laraadmin.adminRoute') . '/searchurine','LA\MedicalcasesController@searchurine');
 	Route::get(config('laraadmin.adminRoute') . '/searchstool','LA\MedicalcasesController@searchstool');
 	Route::get(config('laraadmin.adminRoute') . '/searcharithris','LA\MedicalcasesController@searcharithris');
 	Route::get(config('laraadmin.adminRoute') . '/searchendocrine','LA\MedicalcasesController@searchendocrine');
 	Route::get(config('laraadmin.adminRoute') . '/searchrenal','LA\MedicalcasesController@searchrenal');
 	Route::get(config('laraadmin.adminRoute') . '/searchxray','LA\MedicalcasesController@searchxray');
 	Route::get(config('laraadmin.adminRoute') . '/searchusgfemale','LA\MedicalcasesController@searchusgfemale');
 	Route::get(config('laraadmin.adminRoute') . '/searchusgmale','LA\MedicalcasesController@searchusgmale');
 	Route::get(config('laraadmin.adminRoute') . '/searchimmunlogy','LA\MedicalcasesController@searchimmunlogy');
 	Route::get(config('laraadmin.adminRoute') . '/searchspecific','LA\MedicalcasesController@searchspecific');
 	Route::get(config('laraadmin.adminRoute') . '/searchcardiac','LA\MedicalcasesController@searchcardiac');
 	Route::get(config('laraadmin.adminRoute') . '/searchserology','LA\MedicalcasesController@searchserology');
 	Route::get(config('laraadmin.adminRoute') . '/searchsemen','LA\MedicalcasesController@searchsemen');
 	Route::get(config('laraadmin.adminRoute') . '/searchdiabetes','LA\MedicalcasesController@searchdiabetes');
 	Route::get(config('laraadmin.adminRoute') . '/searchlipid','LA\MedicalcasesController@searchlipid');
 	Route::get(config('laraadmin.adminRoute') . '/searchliver','LA\MedicalcasesController@searchliver');
 	Route::get(config('laraadmin.adminRoute') . '/getregcharges','LA\MedicalcasesController@getregcharges');
 	Route::get(config('laraadmin.adminRoute') . '/calculateheight','LA\MedicalcasesController@calculateheight');

    Route::post(config('laraadmin.adminRoute') . '/homeodetail','LA\MedicalcasesController@homeodetail');
    Route::post(config('laraadmin.adminRoute') . '/medicalcases/addreminder','LA\MedicalcasesController@addreminder');
    
    Route::post(config('laraadmin.adminRoute') . '/updatecharges','LA\MedicalcasesController@updatecharges');
    Route::post(config('laraadmin.adminRoute') . '/investigationdetail','LA\MedicalcasesController@investigationdetail');
    Route::post(config('laraadmin.adminRoute') . '/communicationdetail','LA\MedicalcasesController@communicationdetail');
    Route::post(config('laraadmin.adminRoute') . '/updateadditional','LA\MedicalcasesController@updateadditional');
    Route::get(config('laraadmin.adminRoute') . '/getadditional','LA\MedicalcasesController@getadditional');
    Route::get(config('laraadmin.adminRoute') . '/getpaymentinfo','LA\MedicalcasesController@getpaymentinfo');
    Route::get(config('laraadmin.adminRoute') . '/getreceived','LA\MedicalcasesController@getreceived');
    Route::post(config('laraadmin.adminRoute') . '/updatereceived','LA\MedicalcasesController@updatereceived');
    Route::post(config('laraadmin.adminRoute') . '/getpotency','LA\MedicalcasesController@getpotency');
    Route::post(config('laraadmin.adminRoute') . '/semenanalysis','LA\MedicalcasesController@semenanalysis');
    Route::post(config('laraadmin.adminRoute') . '/updatesemenanalysis','LA\MedicalcasesController@updatesemenanalysis');
 	Route::post(config('laraadmin.adminRoute') . '/serologicalreports','LA\MedicalcasesController@serologicalreports');
    Route::post(config('laraadmin.adminRoute') . '/updateserologicalreports','LA\MedicalcasesController@updateserologicalreports');
    Route::post(config('laraadmin.adminRoute') . '/diabetesdetail','LA\MedicalcasesController@diabetesdetail');
    Route::post(config('laraadmin.adminRoute') . '/updatediabetesdetail','LA\MedicalcasesController@updatediabetesdetail');
    Route::post(config('laraadmin.adminRoute') . '/cardiacreport','LA\MedicalcasesController@cardiacreport');
    Route::post(config('laraadmin.adminRoute') . '/updatecardiacreport','LA\MedicalcasesController@updatecardiacreport');
    Route::post(config('laraadmin.adminRoute') . '/lipiddetail','LA\MedicalcasesController@lipiddetail');
    Route::post(config('laraadmin.adminRoute') . '/updatelipiddetail','LA\MedicalcasesController@updatelipiddetail');
 	Route::post(config('laraadmin.adminRoute') . '/liverdetail','LA\MedicalcasesController@liverdetail');
    Route::post(config('laraadmin.adminRoute') . '/updateliverdetail','LA\MedicalcasesController@updateliverdetail');
    Route::post(config('laraadmin.adminRoute') . '/specficdetail','LA\MedicalcasesController@specficdetail');
    Route::post(config('laraadmin.adminRoute') . '/updatespecficdetail','LA\MedicalcasesController@updatespecficdetail');
    Route::post(config('laraadmin.adminRoute') . '/immunologydetail','LA\MedicalcasesController@immunologydetail');
    Route::post(config('laraadmin.adminRoute') . '/updateimmunologydetail','LA\MedicalcasesController@updateimmunologydetail');
    Route::post(config('laraadmin.adminRoute') . '/maledetail','LA\MedicalcasesController@maledetail');
    Route::post(config('laraadmin.adminRoute') . '/updatemaledetail','LA\MedicalcasesController@updatemaledetail');
    Route::post(config('laraadmin.adminRoute') . '/savepackages','LA\MedicalcasesController@savepackages');
    
 	Route::post(config('laraadmin.adminRoute') . '/xraydetail','LA\MedicalcasesController@xraydetail');
    Route::post(config('laraadmin.adminRoute') . '/updatexraydetail','LA\MedicalcasesController@updatexraydetail');
    Route::post(config('laraadmin.adminRoute') . '/arithrisdetail','LA\MedicalcasesController@arithrisdetail');
    Route::post(config('laraadmin.adminRoute') . '/updatearithrisdetail','LA\MedicalcasesController@updatearithrisdetail');
 	Route::post(config('laraadmin.adminRoute') . '/femaledetail','LA\MedicalcasesController@femaledetail');
    Route::post(config('laraadmin.adminRoute') . '/updatefemaledetail','LA\MedicalcasesController@updatefemaledetail');
	Route::post(config('laraadmin.adminRoute') . '/renaldetail','LA\MedicalcasesController@renaldetail');
    Route::post(config('laraadmin.adminRoute') . '/updaterenaldetail','LA\MedicalcasesController@updaterenaldetail');
    Route::post(config('laraadmin.adminRoute') . '/stooldetail','LA\MedicalcasesController@stooldetail');
    Route::post(config('laraadmin.adminRoute') . '/updatestooldetail','LA\MedicalcasesController@updatestooldetail');
    Route::post(config('laraadmin.adminRoute') . '/endocrinedetail','LA\MedicalcasesController@endocrinedetail');
    Route::post(config('laraadmin.adminRoute') . '/updateendocrinedetail','LA\MedicalcasesController@updateendocrinedetail');
 	Route::post(config('laraadmin.adminRoute') . '/urinedetail','LA\MedicalcasesController@urinedetail');
    Route::post(config('laraadmin.adminRoute') . '/updateurinedetail','LA\MedicalcasesController@updateurinedetail');
    Route::post(config('laraadmin.adminRoute') . '/cbcdetail','LA\MedicalcasesController@cbcdetail');
    Route::post(config('laraadmin.adminRoute') . '/updatecbcdetail','LA\MedicalcasesController@updatecbcdetail');
    Route::get(config('laraadmin.adminRoute') . '/medicalcases/{id}/homeoedit/', 'LA\MedicalcasesController@homeoedit');
	Route::post(config('laraadmin.adminRoute') . '/medicalcases/homeoupdate/', 'LA\MedicalcasesController@homeoupdate');
	Route::post(config('laraadmin.adminRoute') . '/medicalcases/caseupdate/', 'LA\MedicalcasesController@caseupdate');
    Route::get(config('laraadmin.adminRoute') . '/medicalcases/{id}/potencyedit/', 'LA\MedicalcasesController@potencyedit');
    Route::post(config('laraadmin.adminRoute') . '/medicalcases/potencyupdate/','LA\MedicalcasesController@potencyupdate');
    Route::get(config('laraadmin.adminRoute') . '/medicalcases/{id}/heightweightedit/', 'LA\MedicalcasesController@heightweightedit');
    Route::post(config('laraadmin.adminRoute') . '/medicalcases/heightweightupdate/','LA\MedicalcasesController@heightweightupdate');
    Route::post(config('laraadmin.adminRoute') . '/medicalcases/delete_appt/{id}', 'LA\MedicalcasesController@delete_appt');

    /* ================== Static Pages ================== */
	Route::resource(config('laraadmin.adminRoute') . '/staticpages', 'LA\StaticpagesController');
	Route::get(config('laraadmin.adminRoute') . '/staticpage_dt_ajax', 'LA\StaticpagesController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');

	/* ================== Book Appointment ================== */
	Route::resource(config('laraadmin.adminRoute') . '/appointments', 'LA\AppointmentsController');
	Route::get(config('laraadmin.adminRoute') . '/appointment_dt_ajax', 'LA\AppointmentsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/appointments/change_password/{id}', 'LA\AppointmentsController@change_password');
	Route::get(config('laraadmin.adminRoute') . '/getdoctordetail', 'LA\AppointmentsController@getdoctordetail');
	Route::get(config('laraadmin.adminRoute') . '/getbooktime', 'LA\AppointmentsController@getbooktime');
	Route::post(config('laraadmin.adminRoute') . '/appointments/updatestatus','LA\AppointmentsController@updatestatus');

	/* ================== Case Reminder ================== */
	Route::resource(config('laraadmin.adminRoute') . '/casereminder', 'LA\CasereminderController');
	Route::get(config('laraadmin.adminRoute') . '/casereminder_dt_ajax', 'LA\CasereminderController@dtajax');
	Route::resource(config('laraadmin.adminRoute') . '/dailyreminder', 'LA\CasereminderController@dailyreminder');

	/* ================== Couriers ================== */
	Route::resource(config('laraadmin.adminRoute') . '/couriers', 'LA\CouriersController');
	Route::get(config('laraadmin.adminRoute') . '/couriers_dt_ajax', 'LA\CouriersController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/couriers/create', 'LA\CouriersController@create');
	Route::post(config('laraadmin.adminRoute') . '/couriers/couriersupdate', 'LA\CouriersController@couriersupdate');

	/* ================== Couriers Medicine ================== */
	Route::resource(config('laraadmin.adminRoute') . '/couriermedicine', 'LA\CouriermedicineController');
	Route::get(config('laraadmin.adminRoute') . '/couriermedicine_dt_ajax', 'LA\CouriermedicineController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/couriermedicine/courier/', 'LA\CouriermedicineController@courier');
	Route::get(config('laraadmin.adminRoute') . '/couriermedicine/assign/{id}', 'LA\CouriermedicineController@assign');
	Route::post(config('laraadmin.adminRoute') . '/couriermedicine/savepackages/', 'LA\CouriermedicineController@savepackages');
	Route::post(config('laraadmin.adminRoute') . '/couriermedicine/medicineupdate/', 'LA\CouriermedicineController@medicineupdate');
	Route::post(config('laraadmin.adminRoute') . '/couriermedicine/updatenotify/', 'LA\CouriermedicineController@updatenotify');
	Route::get(config('laraadmin.adminRoute') . '/getmedicinedetail', 'LA\CouriermedicineController@getmedicinedetail');
	Route::post(config('laraadmin.adminRoute') . '/couriermedicine/sendsms/', 'LA\CouriermedicineController@sendsms');
	Route::get(config('laraadmin.adminRoute') . '/getcourierdetails', 'LA\CouriermedicineController@getcourierdetails');

	/* ================== Upcoming Appointments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/pendingappointments', 'LA\PendingappointmentsController');
	Route::get(config('laraadmin.adminRoute') . '/pendingappointments_dt_ajax', 'LA\PendingappointmentsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/pendingappointments/sendpendingsms/', 'LA\ExpensesController@sendpendingsms');
	Route::post(config('laraadmin.adminRoute') . '/pendingappointments/updatestatus','LA\PendingappointmentsController@updatestatus');
	Route::post(config('laraadmin.adminRoute') . '/pendingappointments/search','LA\PendingappointmentsController@search');
	Route::get(config('laraadmin.adminRoute') . '/getcasedetail', 'LA\PendingappointmentsController@getcasedetail');
	Route::post(config('laraadmin.adminRoute') . '/pendingappointments/sendsms/', 'LA\PendingappointmentsController@sendsms');


	/* ================== Token ================== */
	Route::resource(config('laraadmin.adminRoute') . '/token', 'LA\TokenController');
	Route::get(config('laraadmin.adminRoute') . '/token_dt_ajax', 'LA\TokenController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/searchtoken','LA\TokenController@searchtoken');
	Route::post(config('laraadmin.adminRoute') . '/savetoken','LA\TokenController@savetoken');
	Route::get(config('laraadmin.adminRoute') . '/searchtokenid','LA\TokenController@searchtokenid');
	Route::post(config('laraadmin.adminRoute') . '/deletetoken','LA\TokenController@deletetoken');
	Route::get(config('laraadmin.adminRoute') . '/getheightweight', 'LA\TokenController@getheightweight');
	Route::post(config('laraadmin.adminRoute') . '/saveCaseHeightWeights','LA\TokenController@saveCaseHeightWeights');
	Route::get(config('laraadmin.adminRoute') . '/packagehistory', 'LA\TokenController@packagehistory');
	Route::post(config('laraadmin.adminRoute') . '/deletepackage','LA\TokenController@deletepackage');
	Route::post(config('laraadmin.adminRoute') . '/savepackages','LA\TokenController@savepackages');
	Route::post(config('laraadmin.adminRoute') . '/deletefamily','LA\TokenController@deletefamily');
	Route::post(config('laraadmin.adminRoute') . '/savefamily','LA\TokenController@savefamily');
	Route::get(config('laraadmin.adminRoute') . '/searchfamily', 'LA\TokenController@searchfamily');
	Route::get(config('laraadmin.adminRoute') . '/familydata', 'LA\TokenController@familydata');
	Route::get(config('laraadmin.adminRoute') . '/searchcases','LA\TokenController@searchcases');




	/* ================== Daily Collection ================== */
	Route::resource(config('laraadmin.adminRoute') . '/dailycollection', 'LA\DailycollectionController');
	Route::get(config('laraadmin.adminRoute') . '/dailycollection_dt_ajax', 'LA\DailycollectionController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/dailycollection/search','LA\DailycollectionController@search');
	Route::post(config('laraadmin.adminRoute') . '/dailycollection/updatereceived','LA\DailycollectionController@updatereceived');
	Route::get(config('laraadmin.adminRoute') . '/getbilldetails','LA\DailycollectionController@getbilldetails');
	Route::post(config('laraadmin.adminRoute') . '/generatePDF','LA\DailycollectionController@generatePDF');
	Route::post(config('laraadmin.adminRoute') . '/savepackages','LA\DailycollectionController@savepackages');
	Route::get(config('laraadmin.adminRoute') . '/getamountdetail', 'LA\DailycollectionController@getamountdetail');
	Route::post(config('laraadmin.adminRoute') . '/packagesave','LA\DailycollectionController@packagesave');
	Route::get(config('laraadmin.adminRoute') . '/getsmsdetail', 'LA\DailycollectionController@getsmsdetail');
	Route::post(config('laraadmin.adminRoute') . '/printstickers','LA\DailycollectionController@printstickers');

	/* ================== Expenses ================== */
	Route::resource(config('laraadmin.adminRoute') . '/expenses', 'LA\ExpensesController');
	Route::get(config('laraadmin.adminRoute') . '/expenses_dt_ajax', 'LA\ExpensesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/saveexpenses','LA\ExpensesController@saveexpenses');
	Route::post(config('laraadmin.adminRoute') . '/expenses/expensesupdate/', 'LA\ExpensesController@expensesupdate');

	/* ================== Expenses Head ================== */
	Route::resource(config('laraadmin.adminRoute') . '/expenseshead', 'LA\ExpensesheadController');
	Route::get(config('laraadmin.adminRoute') . '/expenseshead_dt_ajax', 'LA\ExpensesheadController@dtajax');

	/* ================== Billing ================== */
	Route::resource(config('laraadmin.adminRoute') . '/billing', 'LA\BillingController');
	Route::get(config('laraadmin.adminRoute') . '/billing_dt_ajax', 'LA\BillingController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/addbilling','LA\BillingController@addbilling');

	/* ================== Billing ================== */
	Route::resource(config('laraadmin.adminRoute') . '/refrencedetails', 'LA\RefrencedetailsController');
	Route::get(config('laraadmin.adminRoute') . '/refrencedetails_dt_ajax', 'LA\RefrencedetailsController@dtajax');

	/* ================== Package Expiry ================== */
	Route::resource(config('laraadmin.adminRoute') . '/packageexpiry', 'LA\PackageexpiryController');
	Route::get(config('laraadmin.adminRoute') . '/packageexpiry_dt_ajax', 'LA\PackageexpiryController@dtajax');

	/* ================== Monthwise followps ================== */
	Route::resource(config('laraadmin.adminRoute') . '/casemonthwise', 'LA\CasemonthwiseController');
	Route::get(config('laraadmin.adminRoute') . '/casemonthwise_dt_ajax', 'LA\CasemonthwiseController@dtajax');

	/* ================== Bank Deposit ================== */
	Route::resource(config('laraadmin.adminRoute') . '/bankdeposit', 'LA\BankdepositController');
	Route::get(config('laraadmin.adminRoute') . '/bankdeposit_dt_ajax', 'LA\BankdepositController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/savedeposit','LA\BankdepositController@savedeposit');

	/* ================== Cash Deposit ================== */
	Route::resource(config('laraadmin.adminRoute') . '/cashdeposit', 'LA\CashdepositController');
	Route::get(config('laraadmin.adminRoute') . '/cashdeposit_dt_ajax', 'LA\CashdepositController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/savedeposit','LA\CashdepositController@savedeposit');

	/* ================== View Collection ================== */
	Route::resource(config('laraadmin.adminRoute') . '/viewcollection', 'LA\ViewcollectionController');
	Route::get(config('laraadmin.adminRoute') . '/viewcollection_dt_ajax', 'LA\ViewcollectionController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/viewcollection/search','LA\ViewcollectionController@search');

	/* ================== Target Details ================== */
	Route::resource(config('laraadmin.adminRoute') . '/targetdetails', 'LA\TargetdetailsController');
	Route::get(config('laraadmin.adminRoute') . '/targetdetails_dt_ajax', 'LA\TargetdetailsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/savetarget','LA\TargetdetailsController@savetarget');
	Route::post(config('laraadmin.adminRoute') . '/search','LA\TargetdetailsController@search');

	/* ================== Record ================== */
	Route::resource(config('laraadmin.adminRoute') . '/record', 'LA\RecordController');
	Route::get(config('laraadmin.adminRoute') . '/record_dt_ajax', 'LA\RecordController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/saverecord','LA\RecordController@saverecord');
	Route::get(config('laraadmin.adminRoute') . '/searchrecord','LA\RecordController@searchrecord');
	Route::post(config('laraadmin.adminRoute') . '/newrecord','LA\RecordController@newrecord');
	


	/* ================== AllRecord ================== */
	Route::resource(config('laraadmin.adminRoute') . '/allrecord', 'LA\AllrecordController');
	Route::get(config('laraadmin.adminRoute') . '/allrecord_dt_ajax', 'LA\AllrecordController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/saverecord','LA\AllrecordController@saverecord');
	Route::get(config('laraadmin.adminRoute') . '/searchrecord','LA\AllrecordController@searchrecord');

	/* ================== SMStemplate ================== */
	Route::resource(config('laraadmin.adminRoute') . '/smstemplate', 'LA\SmstemplateController');
	Route::get(config('laraadmin.adminRoute') . '/smstemplate_dt_ajax', 'LA\SmstemplateController@dtajax');

	/* ================== Remedy ================== */
	Route::resource(config('laraadmin.adminRoute') . '/remedy', 'LA\RemedyController');
	Route::get(config('laraadmin.adminRoute') . '/remedy_dt_ajax', 'LA\RemedyController@dtajax');

	/* ================== Printsticker ================== */
	Route::resource(config('laraadmin.adminRoute') . '/printsticker/{id}/{name}/{no}', 'LA\PrintstickerController');
	Route::get(config('laraadmin.adminRoute') . '/remedy_dt_ajax', 'LA\PrintstickerController@dtajax');


	Route::get('/view-clear', function() {
     $exitCode = Artisan::call('view:clear');
     return 'View cache cleared';
 });

});

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function CaseCreateSms($phone_number,$message){
        
        
        $postData = array(
        	'authkey' => 'MWJlYjg4ZTFmZDF',
        	'mobiles' => $phone_number,
          	'message' => $message,
          	'sender' => "RNDSMS",
          	'type' => '1',
			'route' => '2'
         );

        $url =  "http://roundsms.com/api/sendhttp.php?"; 

        $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

		//return $phone_number;
        
    }	

     public function CaseReferSms($phone_number,$message){
        
        
        $postData = array(
        	'authkey' => 'MWJlYjg4ZTFmZDF',
        	'mobiles' => $phone_number,
          	'message' => $message,
          	'sender' => "RNDSMS",
          	'type' => '1',
			'route' => '2'
         );

        $url =  "http://roundsms.com/api/sendhttp.php?"; 

        $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

		//return $phone_number;
        
    }

    public function CaseRegister($phone_number,$var1,$var2,$var3){
        
        
        $postData = array(
        	'From' => "SMARTO",
          	'To' => $phone_number,
          	'TemplateName' => "Userdetail",
          	'VAR1' => $var1,
          	'VAR2' => $var2,
          	'VAR3' => $var3,
        );

        $url = "http://2factor.in/API/V1/fd726117-8949-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS";

        $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

		//return $phone_number;
        
    }

     public function birthdaysms($phone_number,$var1){
        
        
        $postData = array(
        	'From' => "SMARTO",
          	'To' => $phone_number,
          	'TemplateName' => "birthday",
          	'VAR1' => $var1,
        );

        $url = "http://2factor.in/API/V1/fd726117-8949-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS";

        $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

		//return $phone_number;
        
    }

 	public function AppointmentReminder($phone_number,$var1){
        
        
        $postData = array(
        	'From' => "SMARTO",
          	'To' => $phone_number,
          	'TemplateName' => "reminder",
          	'VAR1' => $var1,
        );

        $url = "http://2factor.in/API/V1/fd726117-8949-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS";

        $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

		//return $phone_number;
        
    }

    public function CourierMedicine($phone_number,$message){
        
        
        $postData = array(
        	'authkey' => 'MWJlYjg4ZTFmZDF',
        	'mobiles' => $phone_number,
          	'message' => $message,
          	'sender' => "RNDSMS",
          	'type' => '1',
			'route' => '2'
         );

        	$url =  "http://roundsms.com/api/sendhttp.php?";  

        $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

		//return $phone_number;
        
    }

     public function CaseUserSms($phone_number,$message){
        
        
        $postData = array(
        	'authkey' => 'MWJlYjg4ZTFmZDF',
        	'mobiles' => $phone_number,
          	'message' => $message,
          	'sender' => "RNDSMS",
          	'type' => '1',
			'route' => '2'
         );

        	$url =  "http://roundsms.com/api/sendhttp.php?";  

        $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		 CURLOPT_POSTFIELDS => $postData,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}


		//return $phone_number;
        
    }	


}

<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use Dwij\Laraadmin\Helpers\LAHelper;

use App\User;
use App\Models\Account;
use App\Models\Employee;
use App\Models\Apitest;
use App\Models\Users;
use App\Models\Usersprofile;
use App\Role;
use Mail;
use Log;


/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $request->session()->put('showalert', 0);
        $roleCount = \App\Role::count();
		if($roleCount != 0) {
			if($roleCount != 0) {
				return view('home');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }
    
    
    
    
    public function contactus()
    {
        $roleCount = \App\Role::count();
		if($roleCount != 0) {
			if($roleCount != 0) {
				return view('contactus');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }
    public function aboutus()
    {
        $roleCount = \App\Role::count();
		if($roleCount != 0) {
			if($roleCount != 0) {
				return view('aboutus');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }
    
    public function patientsignup()
    {
        $roleCount = \App\Role::count();
        if($roleCount != 0) {
            if($roleCount != 0) {
                return view('register');
            }
        } else {
            return view('errors.error', [
                'title' => 'Migration not completed',
                'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
            ]);
        }
    }

    public function patientregister(Request $request)
    {
        
            //echo "<pre>"; print_r($request->all()); die();
            // generate password
            $password = $request->password;//LAHelper::gen_password();
            $email = $request->email;
            $mobile= $request->mobile;
            $name= $request->name;
            $usersget = User::where('email','=', $request->email)->first();
            if(count($usersget)){
                return redirect()->back()->withErrors([ 'Email-ID already exists' ]);
            }
            else{
            // Create Employee
            $employee_id = Module::insert("Employees", $request);
            $send= $this->CaseRegister($mobile,$name,$email,$password);
            // Create User
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($password),
                'context_id' => $employee_id,
                'type' => "Employee",
            ]);
    
            // update user role
            $user->detachRoles();
            $role = Role::find($request->role);
            $user->attachRole($role);
            
            }
            if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
                // Send mail to User his Password
                Mail::send('emails.send_login_cred', ['user' => $user, 'password' => $password], function ($m) use ($user) {
                    $m->from('hello@laraadmin.com', 'LaraAdmin');
                    $m->to($user->email, $user->name)->subject('LaraAdmin - Your Login Credentials');
                });
            } else {
                Log::info("User created: username: ".$user->email." Password: ".$password);
            }
           // die();
            return redirect('http://smartops.co.in/managemyclinic/login');
            
        
    }

   

    public function createdetails(Request $request)
    {
        $data = $request->input();
        if(isset($data['jobtitle']) && $data['jobtitle']!="" && isset($data['email']) && $data['email']!="" && isset($data['mobile']) && $data['mobile']!=""){
                
                // Create Account
                $existuser = Apitest::where('email', '=', $data['email'])->first();
                if(empty($existuser)) {
                   
                    
                    // Create User
                    $user = Apitest::create([
                        //'name' => $data['name'],
                        'email' => $data['email'],
                        'jobtitle' => $data['jobtitle'],
                        'mobile' => $data['mobile'],
                        'created_at' => date('Y-m-d H:i:s'),
                    ]);

                    $finalarr = array();
                    $finalarr['email'] = $data['email'];
                    $finalarr['jobtitle'] = $data['jobtitle'];
                    $finalarr['mobile'] = $data['mobile'];
                    
                    $dataresult['userDetail'] = $finalarr;
                    $dataresult['message'] = 'New user created';
                    $dataresult['status'] = 200;
                    
                }else {
                    $dataresult['userDetail'] =  array();
                    $dataresult['message'] = 'Email already exist';
                    $dataresult['status'] = 400;
                }
                
        }else {
            $dataresult['userDetail'] =  array();
            $dataresult['message'] = 'All Fields required.';
            $dataresult['status'] = 400;
        }
        echo json_encode($dataresult); die;
    }


    public function searchdetails(Request $request)
    {
       
        $data = $request->input();
        $searchinput = $data['searchinput'];
        if(isset($data['searchinput']) && $data['searchinput']!="" ){
                
                
                $existuser = Apitest::select("*")
                                ->where('jobtitle', 'LIKE', "%{$data['searchinput']}%")
                                ->orWhere('mobile', 'LIKE', "%{$data['searchinput']}%")
                                ->get();

                             
                if(!empty($existuser)) {

                    $searchlist = array();
                       $i=0;
                       foreach($existuser as $existuserlist){
                           
                               $searchlist[$i]['jobtitle'] =$existuserlist['jobtitle'];
                               $searchlist[$i]['mobile'] = $existuserlist['mobile'];
                               $searchlist[$i]['email'] = $existuserlist['email'];
                               $i++;
                        }
                   
                    $dataresult['searchlist'] = $searchlist;
                    $dataresult['message'] = 'Search List';
                    $dataresult['status'] = 200;
                    
                }else {
                   
                    $dataresult['message'] = 'No result found';
                    $dataresult['status'] = 400;
                }
                
        }else {
            $dataresult['message'] = 'All Fields required.';
            $dataresult['status'] = 400;
        }
        echo json_encode($dataresult); die;
    }

    public function fetchdetails(Request $request)
    {
       
        $data = $request->input();
      
        $getEmaillist = explode(',',$data['emailaddresses']['email']);
        $getMobilelist = explode(',',$data['phonenumbers']['number']);
         // print_r($getEmaillist);

        if(isset($data['jobtitle']) && $data['jobtitle']!="" ){
                
                // Create Account
           
                foreach($getEmaillist as $key=>$value){
                    foreach($getMobilelist as $keys=>$values){
                    
                    $existuser = Apitest::where('email', '=', $value)->first();
                    
                    
                    if(empty($existuser)) {
                        
                        $user = Apitest::create([
                        //'name' => $data['name'],
                        'email' => $value,
                        'jobtitle' => $data['jobtitle'],
                        'mobile' => $values,
                        'created_at' => date('Y-m-d H:i:s'),
                        ]);

                        $finalarr = array();
                        $finalarr['email'] = $value;
                        $finalarr['jobtitle'] = $data['jobtitle'];
                        $finalarr['mobile'] = $values;
                        
                        $dataresult['userDetail'] = $finalarr;
                        $dataresult['message'] = 'New user created';
                        $dataresult['status'] = 200;
                    }
                    else {
                        $dataresult['userDetail'] =  array();
                        $dataresult['message'] = 'Email already exist';
                        $dataresult['status'] = 400;
                    }
                }
            }
                
                
        }else {
            $dataresult['userDetail'] =  array();
            $dataresult['message'] = 'All Fields required.';
            $dataresult['status'] = 400;
        }
        echo json_encode($dataresult); die;
    }

    public function updateprofile(Request $request)
    {
        $data = $request->input();
       
        if(isset($data['user_id']) && $data['user_id']!=""){
            $existemailuser = Users::where('id', '=', $data['user_id'])->first();
            $ifEmailExist = Users::where('email', '=', $data['email'])->where('id', '!=', $data['user_id'])->first();
            

            if($ifEmailExist){
                $dataresult['message'] = 'Email Address is already exist!';
                $dataresult['status'] = 400;
            } else {
                if(!empty($existemailuser)) {
                    if(isset($data['email']) && $data['email']!=""){
                        $updateuser = Users::where('id', $data['user_id'])->update(['email' => $data['email']]);
                    }
                     if(isset($data['password']) && $data['password']!=""){
                        $password = Hash::make($data['password']);
                        $updateuser = Users::where('id', $data['user_id'])->update(['password' => $password]);
                    }
                    
                    $fname = $data['fname'];
                    $lname = $data['lname'];

                    if(isset($data['fname']) && $data['fname']!=""){
                           $updateuser1 = Usersprofile::where('user_id', $data['user_id'])->update(['firstname' => $fname,'lastname' => $lname]);
                    }
                    if(isset($data['gender']) && $data['gender']!=""){
                            $updateuser1 = Usersprofile::where('user_id', $data['user_id'])->update(['gender' => $data['gender']]);
                    }
                    if(isset($data['birthdate']) && $data['birthdate']!=""){
                            $updateuser1 = Usersprofile::where('user_id', $data['user_id'])->update(['birthdate' => $data['birthdate']]);
                    }
                    if(isset($data['profile_url']) && $data['profile_url']!=""){
                            $updateuser2 = Usersprofile::where('user_id', $data['user_id'])->update(['image' => $data['profile_url']]);
                    }
                      
                   
                    $finalarr = array();
                    $finalarr['email'] = $existemailuser['email_address'];
                    $finalarr['_id'] = $existemailuser['id'];
                        $custdata = Usersprofile::where('user_id', '=', $existemailuser['id'])->first();
                        $finalarr['fname'] = $custdata['firstname'];
                        $finalarr['lname'] = $custdata['lastname'];
                        $finalarr['gender'] = $custdata['gender'];
                        $finalarr['birthdate'] = $custdata['birthdate'];
                        $finalarr['profileUrl'] = $custdata['image'];
                   
                    $dataresult['userDetail'] = $finalarr;
                    // sending email
                    $dataresult['status'] = 200;
                    $dataresult['message'] = 'Profile updated successfully';
                } else {
                     $dataresult['message'] = 'User Id is incorrect!';
                     $dataresult['status'] = 400;
                }
            }

              
         }else {
            $dataresult['message'] = 'User Id is required!';
            $dataresult['status'] = 400;
        }
        echo json_encode($dataresult); die;
    }
}
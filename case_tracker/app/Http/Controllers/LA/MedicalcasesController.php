<?php

/**

 * Controller genrated using LaraAdmin

 * Help: http://laraadmin.com

 */



namespace App\Http\Controllers\LA;



use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use DB;

// use PDF;

use Validator;

use Datatables; 

use Collective\Html\FormFacade as Form;

use Dwij\Laraadmin\Models\Module;

use Dwij\Laraadmin\Models\ModuleFields;



use App\Models\Medicalcase;

use App\Models\CaseData;

use App\Models\BasicDetail;

use App\Models\HomeoDetail;

use App\Models\AddReminder;

use App\Models\CaseHeight;

use App\Models\CasePotency;

use App\Models\CommunicationDetail;

use App\Models\Doctor;

use App\Models\Package;

use App\Models\Employee;

use App\Models\Payment;

use App\Models\CaseExamination;

use App\Models\CaseInvestigation;

use App\Models\CaseImage;

use App\Models\CaseHeightWeight;

use App\Models\CaseNotes;

use App\Models\Couriermedicine;

use App\Models\CaseSemenanalysis;

use App\Models\CaseSerological;

use App\Models\CaseDiabetes;

use App\Models\CaseCardiac;

use App\Models\CaseLipid;

use App\Models\CaseLiver;

use App\Models\CaseSpecific;

use App\Models\CaseImmunology;

use App\Models\CaseUsgmale;

use App\Models\CaseXray;

use App\Models\CaseArthritis;

use App\Models\CaseUsgfemale;

use App\Models\CaseRenal;

use App\Models\CaseStool;

use App\Models\CaseEndocrine;

use App\Models\CaseMedicines;

use App\Models\CaseFrequency;

use App\Models\CaseUrine;

use App\Models\CaseCbc;

use App\Models\CaseDaycharges;

use App\Models\CaseAdditionalcharges;

use App\Models\Potency;

use App\Models\Pendingappointments;

use App\Models\Familygroup;

use App\Models\Packagehistory;

use App\Models\Billing;

use App\Models\Smsreport;



class MedicalcasesController extends Controller

{

	public $show_action = true;

	public $view_col = 'name';

	

	public $listing_cols = ['regid', 'name', 'doctorname','dob'];

	

	public function __construct() {

		// Field Access of Listing Columns

		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {

			$this->middleware(function ($request, $next) {

				$this->listing_cols = ModuleFields::listingColumnAccessScan('Medicalcases', $this->listing_cols);

				return $next($request);

			});

		} else {

			$this->listing_cols = ModuleFields::listingColumnAccessScan('Medicalcases', $this->listing_cols);

		}

	}

	

	/**

	 * Display a listing of the Organizations.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function index(Request $request)

	{

	   

		$module = Module::get('Medicalcases');

        $doctors = DB::table('doctors')->get();

        $packages = DB::table('packages')->where('deleted_at','=',NULL)->orderBy('id','ASC')->get();

        $patients = DB::table('employees')->get();

        $stocks = DB::table('stocks')->get();

     	$payment = DB::table('payments')->get();

     	$religion = DB::table('religion')->get();

     	$occupation = DB::table('occupation')->get();

     	$refrence = DB::table('refrencetype')->get();

     	$title = DB::table('title')->get();

     	$status = DB::table('status')->get();

     	$sms = DB::table('smstemplate')->get();

     	$appointment = DB::table('appointments')->where([ ['assistant_doctor','=',Auth::user()->context_id], ['id','=',session()->get('id')]])->first();

 	 	$myArray = json_decode(json_encode($appointment), true);

 	 	

 	 	$casedata = DB::table('case_datas')->where('id','=',$myArray['patient_id'])->first();

 	 	$mydata = json_decode(json_encode($casedata), true);



 	 	$case_data = DB::table('case_datas')->orderBy('id','DESC')->first();

 	 	$iddata = json_decode(json_encode($case_data), true);

 	 



	 	$heightweight = DB::table('heightweight')->get();



 	 	if($iddata['regid']!=""){

 	 		$rgid = $iddata['regid'] + 1;

 	 	}

 	 	else{

 	 		$rgid = "1000";

 	 	}

 	 	



 	 	

	 	$medicalcases = DB::table('medicalcases')->where('regid','=','1029')->first();

		if(Module::hasAccess($module->id)) {

			return View('la.medicalcases.index', [

				'show_actions' => $this->show_action,

				'listing_cols' => $this->listing_cols,

				'module' => $module,

				'doctors' => $doctors,

				'packages' => $packages,

				'patients'=>$patients,

				'stocks'=>$stocks,

				'payment'=>$payment,

				'appointment'=>$myArray,

				'casedata'=>$mydata,

				'rgid'=>$rgid,

				'heightweight'=>$heightweight,

				'medicalcases'=>$medicalcases,

				'religion' => $religion,

				'occupation' => $occupation,

				'status' => $status,

				'title' => $title,

				'refrence' => $refrence,

				'sms' => $sms,

			]);

		} else {

            return redirect(config('laraadmin.adminRoute')."/");

        }

        

        

	}

	

	public function savepackages(Request $request)

	{

		// echo "<pre>";print_r($request->all());die();

		// echo $request->package_start_date; die();

		$case = DB::table('case_datas')->where('regid', $request->case_id)->first();

		$caseid = $case->id;

		$employees = Medicalcase::where('regid', $request->case_id)->first();

		$employees->package = $request->packages;

		

		if($request->package_start_date!=""){

			$package_start = $this->convertdate($request->package_start_date);

		}

		else{

			$package_start = $request->package_start;

		}



		$package = DB::table('packages')->where('id','=',$request->packages)->first();

		//echo "<pre>"; print_r($package);

		$packagemonth = $package->expiry_date;

		//echo $packagemonth;

		$created_at = date('d/m/Y');

		$expirydate= date('n/d/Y',strtotime('+'.$packagemonth.' days',strtotime(str_replace('/', '-', $created_at))));

		//$expirydate = date('m/d/Y',strtotime($expiry));



		$employees->package_start = $package_start;

		$employees->date_left = $expirydate;

		$employees->save();



		$bill = Billing::latest()->first();

		$billno = $bill->BillNo + 1;

		$packagesave = Packagehistory::create(['regid' => $caseid, 'packageid'=>$request->packages, 'todate'=>$expirydate,'fromdate'=>date('m/d/Y'), 

				'packagedate'=>date('m/d/Y')]);

		$billing = Billing::create(['regid' => $caseid, 'charges'=>$package->color,'Balance'=>$package->color, 'todate'=>$expirydate,'fromdate'=>date('n/j/Y'),'BillDate'=>date('m/d/Y'), 'to_date'=>date('Y-m-d',strtotime($expirydate)), 'BillNo' => $billno]);

		return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function packagemessage(Request $request)

	{

		

		$send= $this->AppointmentReminder($request->regmobile,$request->regcasename);

		echo "success"; die();

	}

	/**

	 * Show the form for creating a new organization.

	 *

	 * @return \Illuminate\Http\Response

	 */



	public function convertdate($date){

	      //return $date;

	      if($date!=""){

			$date1 = explode('/', $date);

			$newdate = $date1[2].'-'.$date1[1].'-'.$date1[0];

			return $newdate;

	      }else {

	          return '';

	      }

	}



	public function nconvertdate($date){

	      //return $date;

	      if($date!=""){

			$date1 = explode('/', $date);

			$newdate = $date1[1].'/'.$date1[0].'/'.$date1[2];

			return $newdate;

	      }else {

	          return '';

	      }

	}



	public function packagesave(Request $request)

	{

		$case = DB::table('case_datas')->where('regid', $request->case_id)->first();

		$caseid = $case->id;

		$employees = Medicalcase::where('regid', $request->case_id)->first();

		$employees->package = $request->packages;

		

		if($request->package_start_date!=""){

			$package_start = $this->convertdate($request->package_start_date);

		}

		else{

			$package_start = $request->package_start;

		}



		$package = DB::table('packages')->where('id','=',$request->packages)->first();

		//echo "<pre>"; print_r($package);

		$packagemonth = $package->expiry_date;

		//echo $packagemonth;

		$created_at = date('d/m/Y');

		$expirydate= date('n/d/Y',strtotime('+'.$packagemonth.' days',strtotime(str_replace('/', '-', $created_at))));

		//$expirydate = date('m/d/Y',strtotime($expiry));

		$employees->package_start = $package_start;

		$employees->date_left = $expirydate;

		$employees->save();



		$bill = Billing::latest()->first();

		$billno = $bill->BillNo + 1;

		$packagesave = Packagehistory::create(['regid' => $caseid, 'packageid'=>$request->packages, 'todate'=>$expirydate,'fromdate'=>date('m/d/Y'), 

				'packagedate'=>date('m/d/Y')]);

		$billing = Billing::create(['regid' => $caseid, 'charges'=>$package->color,'Balance'=>$package->color, 'todate'=>$expirydate,'fromdate'=>date('n/j/Y'),'BillDate'=>date('m/d/Y'), 'to_date'=>date('Y-m-d',strtotime($expirydate)), 'BillNo' => $billno]);

		return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

	}

	public function create(Request $request)

	{

		

		$this->layout = null;

		$data = $request->all();

		//echo "<pre>"; print_r($data);die();

		if(isset($data['mobile2']) && $data['mobile2']!=""){ $mobile2 =  $data['mobile2']; } else { $mobile2 = '';}

		if(isset($data['first_name']) && $data['first_name']!=""){ $first_name =  $data['first_name']; } else { $first_name ='';}

		if(isset($data['gender']) && $data['gender']!=""){ $gender =  $data['gender']; } else { $gender ='';}

		if(isset($data['mobile1']) && $data['mobile1']!=""){ $mobile1 =  $data['mobile1']; } else { $mobile1 = '';}

		if(isset($data['city']) && $data['city']!=""){ $city =  $data['city']; } else { $city = '';}

		if(isset($data['address']) && $data['address']!=""){ $address =  $data['address']; } else { $address ='';}

		if(isset($data['assitant_doctor']) && $data['assitant_doctor']!=""){ $assitant_doctor =  $data['assitant_doctor']; } else { $assitant_doctor = '';}

		if(isset($data['consultation_fee']) && $data['consultation_fee']!=""){ $consultation_fee =  $data['consultation_fee']; } else { $consultation_fee = '';}

		if(isset($data['title']) && $data['title']!=""){ $title =  $data['title']; } else { $title = '';}

		if(isset($data['middle_name']) && $data['middle_name']!=""){ $middle_name =  $data['middle_name']; } else { $middle_name = '';}

		if(isset($data['surname']) && $data['surname']!=""){ $surname =  $data['surname']; } else { $surname = '';}

		if(isset($data['status']) && $data['status']!=""){ $status =  $data['status']; } else { $status = '';}

		if(isset($data['road']) && $data['road']!=""){ $road =  $data['road']; } else { $road = '';}

		if(isset($data['religion']) && $data['religion']!=""){ $religion =  $data['religion']; } else { $religion = '';}

		if(isset($data['occupation']) && $data['occupation']!=""){ $occupation =  $data['occupation']; } else { $occupation = '';}

		if(isset($data['phone']) && $data['phone']!=""){ $phone =  $data['phone']; } else { $phone = '';}

		if(isset($data['area']) && $data['area']!=""){ $area =  $data['area']; } else { $area = '';}

		if(isset($data['state']) && $data['state']!=""){ $state =  $data['state']; } else { $state = '';}

		if(isset($data['pin']) && $data['pin']!=""){ $pin =  $data['pin']; } else { $pin = '';}

		$courier_outstation = '';

		if(isset($data['send_sms']) && $data['send_sms']!=""){ $send_sms =  $data['send_sms']; } else { $send_sms = '';}

		if(isset($data['email']) && $data['email']!=""){ $email =  $data['email']; } else { $email = '';}

		if(isset($data['reference']) && $data['reference']!=""){ $reference =  $data['reference']; } else { $reference = '';}

		if(isset($data['refered_by']) && $data['refered_by']!=""){ $refered_by =  $data['refered_by']; } else { $refered_by = '';}

		if(isset($data['refered_search']) && $data['refered_search']!=""){ $refered_search =  $data['refered_search']; } else { $refered_search = '';}

		if(isset($data['scheme']) && $data['scheme']!=""){ $scheme =  $data['scheme']; } else { $scheme = '';}



		//if(isset($data['patientid']) && $data['patientid']!=""){ $patientid =  $data['patientid']; } else { $patientid = '';}

 //$date = [ 'name' => $first_name,'regid' => $data['regid'],'designation'=>'','gender'=>$gender, 'mobile'=>$mobile1,  'mobile2'=>$mobile2,  'doctorname'=>$assitant_doctor,'dept'=>'', 'city'=>$city, 'address'=>$address, 'about'=>'','date_birth'=>$this->convertdate($data['date_of_birth']), 'date_left'=>'','salary_cur'=>'' ];

	//echo "<pre>"; print_r($date); die();

		if($scheme=="0"){

			$expirydate = "12/31/9999";

		}

		else{

		$package = DB::table('packages')->where('id','=',$scheme)->first();

		//echo "<pre>"; print_r($package);

		$packagemonth = $package->expiry_date;

		//echo $packagemonth;

		$created_at = date('d/m/Y');

		$expiry= date('d/m/Y',strtotime('+'.$packagemonth.' days',strtotime(str_replace('/', '-', $created_at))));

		$expirydate = date('m/d/Y',strtotime($expiry));

		//echo $expirydate;

		}

		

		$cdate = $this->nconvertdate($data['dob']);

		$user=MedicalCase::create([ 'name' => $first_name,'regid' => $data['regid'],'designation'=>'','gender'=>$gender, 'mobile'=>$mobile1,  'mobile2'=>$mobile2,  'doctorname'=>$assitant_doctor,'dept'=>'', 'city'=>$city, 'address'=>$address, 'about'=>'','date_birth'=>$this->convertdate($data['date_of_birth']), 'consultation_fee' => $consultation_fee, 'package'=>$scheme, 'date_left'=>$expirydate,'salary_cur'=>'' , 'dob'=>date('n/d/Y',strtotime($cdate))]);

		if($scheme!="0"){

			$packagesave = Packagehistory::create(['regid' => $data['regid'], 'packageid'=>$scheme, 'todate'=>$expirydate,'fromdate'=>date('m/d/Y'), 

				'packagedate'=>date('m/d/Y')]);

		}

          

	    	$user1 = CaseData::create([

			            'regid' => $data['regid'],

                        'assitant_doctor'=>$assitant_doctor,

                        'consultation_fee'=>$consultation_fee,

                        'dob'=> date('n/d/Y',strtotime($cdate)),

                        'title' => $title,

                        'first_name'=>$first_name,

                        'middle_name'=>$middle_name,

                        'surname'=>$surname,

                        'gender'=>$gender,

                        'status'=>$status,

                        'date_of_birth'=>$this->convertdate($data['date_of_birth']),

                        'address'=>$address,

                        'road'=>$road,

                        'religion'=>$religion,

                        'occupation'=>$occupation,

                        'phone'=>$phone,

                        'area'=>$area,

                        'city'=>$city,

                        'mobile1'=>$mobile1,

                        'state'=>$state,

                        'pin'=>$pin,

                        'mobile2'=>$mobile2,

                        'courier_outstation'=>$courier_outstation,

                        'send_sms'=>$send_sms,

                        'email'=>$email,

                        'reference' => $reference,

                        'refered_name' => $refered_search,

                        // 'patientid' => $patientid,

                        'refered_by' => $refered_by,

                    ]);



	    	$payment = Payment::create([

	    		'case_id' => $data['regid'],

	    		'patient_name'=>$first_name." ".$surname,

	    		'doctor_id'=>$assitant_doctor,

	    		'consultation_fee'=>'0',





	    	]);

	    	

 	 		if($send_sms == "1"){

 	 			$smsreport = Smsreport::create([

					 'regid' => $data['regid'],

					 'send_date' => date('Y-m-d'),

					 'sms_type' => 'Case Create',

				]);



				$val = "Dear ".$first_name." ".$surname.", Thank you for visiting HomoeoCARE. Please note your Registration Id ".$data['regid'].". Please use this for all future communications. The clinic nos are 0172-4659000/01.";

	    		$msg = $this->CaseCreateSms($phone,$val); 



    		}

            echo "success"; die();

		 //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function referedsms(Request $request)

	{

		

		$this->layout = null;

		$data = $request->all();

		//echo "<pre>"; print_r($data);die();

		if(isset($data['first_name']) && $data['first_name']!=""){ $first_name =  $data['first_name']; } else { $first_name ='';}

		

		if(isset($data['refered_by']) && $data['refered_by']!=""){ $refered_by =  $data['refered_by']; } else { $refered_by = '';}

		if(isset($data['refered_search']) && $data['refered_search']!=""){ $refered_search =  $data['refered_search']; } else { $refered_search = '';}

		if(isset($data['refered_sms']) && $data['refered_sms']!=""){ $refered_sms =  $data['refered_sms']; } else { $refered_sms = '';}

 

	    	if($refered_by !="" && $refered_sms == "1"){

		    	$casedata = DB::table('case_datas')->where('regid','=',$refered_by)->first();

	 	 		$mydata = json_decode(json_encode($casedata), true);

	 	 		$refer_phone = $mydata['phone'];

	 	 		$refer_name = $mydata['first_name']." ".$mydata['surname'];

	 	 		$u_name = $first_name;



	 	 		$smsreport = Smsreport::create([

					 'regid' => $refered_by,

					 'send_date' => date('Y-m-d'),

					 'sms_type' => 'Refrence',

				]);

	 	 		$val = "Dear ".$refer_name.", thanks for referring ".$u_name." to Clinic. We appreciate and acknowledge your support and will do our best to help him.";



				$send= $this->CaseReferSms($refer_phone,$val);

 	 		}

	    	

            echo "success"; die();

		 //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function basicdetail(Request $request){

	

		$data = $request->all();

		if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

		if(isset($data['title']) && $data['title']!=""){ $title =  $data['title']; } else { $title = '';}

		if(isset($data['first_name']) && $data['first_name']!=""){ $first_name =  $data['first_name']; } else { $first_name = '';}

		if(isset($data['middle_name']) && $data['middle_name']!=""){ $middle_name =  $data['middle_name']; } else { $middle_name = '';}

		if(isset($data['surname']) && $data['surname']!=""){ $surname =  $data['surname']; } else { $surname = '';}

		if(isset($data['gender']) && $data['gender']!=""){ $gender =  $data['gender']; } else { $gender = '';}

		if(isset($data['religion']) && $data['religion']!=""){ $religion =  $data['religion']; } else { $religion = '';}

		if(isset($data['status']) && $data['status']!=""){ $status =  $data['status']; } else { $status = '';}

		if(isset($data['occupation']) && $data['occupation']!=""){ $occupation =  $data['occupation']; } else { $occupation = '';}

		if(isset($data['address']) && $data['address']!=""){ $address =  $data['address']; } else { $address = '';}

		if(isset($data['road']) && $data['road']!=""){ $road =  $data['road']; } else { $road = '';}

		if(isset($data['courieroutstation']) && $data['courieroutstation']!=""){ $courieroutstation =  $data['courieroutstation']; } else { $courieroutstation = '';}

		if(isset($data['area']) && $data['area']!=""){ $area =  $data['area']; } else { $area = '';}

		if(isset($data['city']) && $data['city']!=""){ $city =  $data['city']; } else { $city = '';}

		if(isset($data['state']) && $data['state']!=""){ $state =  $data['state']; } else { $state = '';}

		if(isset($data['pin']) && $data['pin']!=""){ $pin =  $data['pin']; } else { $pin = '';}

		if(isset($data['phone_no']) && $data['phone_no']!=""){ $phone_no =  $data['phone_no']; } else { $phone_no = '';}

		if(isset($data['email_id']) && $data['email_id']!=""){ $email_id =  $data['email_id']; } else { $email_id = '';}

		if(isset($data['mobile1']) && $data['mobile1']!=""){ $mobile1 =  $data['mobile1']; } else { $mobile1 = '';}

		if(isset($data['mobile2']) && $data['mobile2']!=""){ $mobile2 =  $data['mobile2']; } else { $mobile2 = '';}

		if(isset($data['reference']) && $data['reference']!=""){ $reference =  $data['reference']; } else { $reference = '';}

		if(isset($data['refered_by']) && $data['refered_by']!=""){ $refered_by =  $data['refered_by']; } else { $refered_by = '';}

		if(isset($data['refered_search']) && $data['refered_search']!=""){ $refered_search =  $data['refered_search']; } else { $refered_search = '';}

		if(isset($data['send_sms']) && $data['send_sms']!=""){ $send_sms =  $data['send_sms']; } else { $send_sms = '';}

		if(isset($data['scheme']) && $data['scheme']!=""){ $scheme =  $data['scheme']; } else { $scheme = '';}

       

		$user = BasicDetail::create([

			           'regid' => $regid,

			           'current_date' =>$this->convertdate( $data['current_date']),

                        'title' => $title,

                        'first_name' => $first_name,

                        'middle_name' => $middle_name,

                        'surname' => $surname,

                        'gender' => $gender,

                        'religion' => $religion,

                        'status' => $status,

                        'date_of_birth' => $this->convertdate($data['date_of_birth']),

                        'occupation' => $occupation,

                        'address' => $address,

                        'road' => $road,

                        'courieroutstation' => $courieroutstation,

                        'area' => $area,

                        'city' => $city,

                        'state' => $state,

                        'pin' => $pin,

                        'phone_no' => $phone_no,

                        'email_id' => $email_id,

                        'mobile1' => $mobile1,

                        'mobile2' => $mobile2,

                        'reference' => $reference,

                        'refered_name' => $refered_search,

                        'send_sms' => $send_sms,

                        'scheme' => $scheme,

                        'end_date' => $this->convertdate($data['end_date']),



                    ]);

				//echo "<pre>"; print_r($request->all()); //die();



		 return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}

	public function homeodetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	//	echo "<pre>"; print_r($data); die();

	    if(isset($data['disease']) && $data['disease']!=""){ $disease =  $data['disease']; } else { $disease = '';}

	    if(isset($data['diagnosis']) && $data['diagnosis']!=""){ $diagnosis =  $data['diagnosis']; } else { $diagnosis = '';}

	    if(isset($data['constitutional']) && $data['constitutional']!=""){ $constitutional =  $data['constitutional']; } else { $constitutional = '';}

	    if(isset($data['complaint_intesity']) && $data['complaint_intesity']!=""){ $complaint_intesity =  $data['complaint_intesity']; } else { $complaint_intesity = '';}

	    if(isset($data['acute']) && $data['acute']!=""){ $acute =  $data['acute']; } else { $acute = '';}

	    if(isset($data['intercurrent']) && $data['intercurrent']!=""){ $intercurrent =  $data['intercurrent']; } else { $intercurrent = '';}

	    if(isset($data['thermal']) && $data['thermal']!=""){ $thermal =  $data['thermal']; } else { $thermal = '';}

	    //if(isset($data['medication_taking']) && $data['medication_taking']!=""){ $medication_taking =  $data['medication_taking']; } else { $medication_taking = '';}

     	if(isset($data['medicine_price']) && $data['medicine_price']!=""){ $medicine_price =  $data['medicine_price']; } else { $medicine_price = '';}

     	//if(isset($data['medicine_time']) && $data['medicine_time']!=""){ $medicine_time =  $data['medicine_time']; } else { $medicine_time = '';}

     	//if(isset($data['medicine_for']) && $data['medicine_for']!=""){ $medicine_for =  $data['medicine_for']; } else { $medicine_for = '';}

	    if(isset($data['prognosis']) && $data['prognosis']!=""){ $prognosis =  $data['prognosis']; } else { $prognosis = '';}

	    if(isset($data['define_criteria1']) && $data['define_criteria1']!=""){ $define_criteria1 =  $data['define_criteria1']; } else { $define_criteria1 = '';}

	    if(isset($data['investigation']) && $data['investigation']!=""){ $investigation =  $data['investigation']; } else { $investigation = '';}

	    if(isset($data['define_criteria2']) && $data['define_criteria2']!=""){ $define_criteria2 =  $data['define_criteria2']; } else { $define_criteria2 = '';}

	    if(isset($data['case_taken']) && $data['case_taken']!=""){ $case_taken =  $data['case_taken']; } else { $case_taken = '';}

	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

	    if(isset($data['total_charges']) && $data['total_charges']!=""){ $total_charges =  $data['total_charges']; } else { $total_charges = '';}

	    $str = implode(", ", $data['criteria']);



	    $medication_taking = implode(", ", $data['medication_taking']);

	    // echo $medication_given;

	    $medicine_for = implode(", ", $data['medicine_for']);

	    $medicine_time = implode(", ", $data['medicine_time']);

	    

	   // $total = $total_charges + $medicine_price;

		$user = HomeoDetail::create([

			 'disease' => $disease,

			 'regid' => $data['regid'],

			 'diagnosis' => $diagnosis,

			 'constitutional' => $constitutional,

			 'complaint_intesity' => $complaint_intesity,

			 'acute' => $acute,

			 'intercurrent' => $intercurrent,

			 'thermal' => $thermal,

			 'medication_taking' => $medication_taking,

		  	 'medicine_time' => $medicine_time,

			 'medicine_for' => $medicine_for,

			 'medicine_price'=>"0",

			 'prognosis' => $prognosis,

			 'define_criteria1' => $define_criteria1,

			 'investigation' => $investigation,

			 'define_criteria2' => $define_criteria2,

			 'case_taken' => $case_taken,

			 'total_charges'=>"0",

			 'homeo_date'=>date('d/m/Y'),

			 'criteria' => $str,



  		]);

  		

  		//$update = DB::table('payments') ->where('case_id', $regid)->update( [ 'medicine_price' => $medicine_price, 'total_price'=>$total ]);

		echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}

	

	public function saveremidy(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['rxdays']) && $data['rxdays']!=""){ $rxdays =  $data['rxdays']; } else { $rxdays = '';}

	    if(isset($data['rxfrequency']) && $data['rxfrequency']!=""){ $rxfrequency =  $data['rxfrequency']; } else { $rxfrequency = '';}

	    if(isset($data['rxremedy']) && $data['rxremedy']!=""){ $rxremedy =  $data['rxremedy']; } else { $rxremedy = '';}

	    if(isset($data['rxpotency']) && $data['rxpotency']!=""){ $rxpotency =  $data['rxpotency']; } else { $rxpotency = '';}

	    if(isset($data['rxprescription']) && $data['rxprescription']!=""){ $rxprescription =  $data['rxprescription']; } else { $rxprescription = '';}

	    if(isset($data['rand']) && $data['rand']!=""){ $rand =  $data['rand']; } else { $rand = '';}

	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

	   // echo  "a".$regid;

	    $caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();



		$caseID = $caseData->id;

		$randno= date('Ymd')."".$caseID;

		$user = CasePotency::create([

			 'rand_id' => $randno,

			 'regid' => $caseID,

			 'rxdays' => $rxdays,

			 'rxfrequency' => $rxfrequency,

			 'rxremedy' => $rxremedy,

			 'rxpotency' => $rxpotency,

			 'rxprescription' => $rxprescription,

			 'dateval' => date('n/d/Y'),

	    ]);



	    $user1 = Couriermedicine::create([

			 'rand_id' => $randno,

			 'case_id' => $regid,

			  'regid' => $caseID,

			 'days' => $rxdays,

			 'frequency' => $rxfrequency,

			 'remedy' => $rxremedy,

			 'potency' => $rxpotency,

			 // 'prescription' => $rxprescription,

			 'currentdate'=>date('Y-m-d'),

	    ]);

	echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function updateremidy(Request $request){



	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['rxremedy']) && $data['rxremedy']!=""){ $rxremedy =  $data['rxremedy']; } else { $rxremedy = '';}

	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

	    if(isset($data['rand']) && $data['rand']!=""){ $rand =  $data['rand']; } else { $rand = '';}



	    $caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

	   	$casep = CasePotency::where('regid','=',$caseID)->latest()->first();

	   	$pid = $casep->id;



	  	$affected = DB::table('case_potencies')

              ->where('id','=', $pid)

              ->update(['rxremedy' => $request->rxremedy]);

             

		



		$courier = DB::table('courier_medicine')

              ->where('rand_id','=', $rand)

              ->update(['remedy' => $request->rxremedy]);

	 echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function updatepotency(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['rxpotency']) && $data['rxpotency']!=""){ $rxpotency =  $data['rxpotency']; } else { $rxpotency = '';}

	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

	    if(isset($data['rand']) && $data['rand']!=""){ $rand =  $data['rand']; } else { $rand = '';}



	    $caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

	   	$casep = CasePotency::where('regid','=',$caseID)->latest()->first();

	   	$pid = $casep->id;



	  	$affected = DB::table('case_potencies')

              ->where('id','=', $pid)

              ->update(['rxpotency' => $request->rxpotency]);

             

		



		$courier = DB::table('courier_medicine')

              ->where('rand_id','=', $rand)

              ->update(['potency' => $request->potency]);

	   

	echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function updatefrequency(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

	    if(isset($data['rand']) && $data['rand']!=""){ $rand =  $data['rand']; } else { $rand = '';}



	    $caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

	   	$casep = CasePotency::where('regid','=',$caseID)->latest()->first();

	   	$pid = $casep->id;



	  	$affected = DB::table('case_potencies')

              ->where('id','=', $pid)

              ->update(['rxfrequency' => $request->rxfrequency]);

             

		



		$courier = DB::table('courier_medicine')

              ->where('rand_id','=', $rand)

              ->update(['frequency' => $request->rxfrequency]);



	 

	echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function updaterxdays(Request $request){

	    $this->layout = null;

		$data = $request->all();

		

	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

	    if(isset($data['rand']) && $data['rand']!=""){ $rand =  $data['rand']; } else { $rand = '';}



	    $cdays = $request->rxdays;

	    $caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

	    $medicalData = DB::table('medicalcases')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;



	   	$casep = CasePotency::where('regid','=',$caseID)->latest()->first();

	   	$charges = CaseDaycharges::where('id','=',$cdays)->latest()->first();

	   	//echo "<pre>"; print_r($charges); exit;

	   

	   	

	   	$charges_days = $charges->regular_charges;

		$update = DB::table('payments') ->where('case_id', $regid)->update( [ 'consultation_fee' => $charges_days, 'payment_date'=>date('Y-m-d') ]);



		$todaydt = date('n/d/Y');

		$todate= date('n/d/Y', strtotime($todaydt. ' +'. $charges->days));

		$bill = Billing::latest()->first();

		$billno = $bill->BillNo + 1;

		$bill = Billing::where('fromdate','=',date('n/j/Y'))->where('regid','=',$caseID)->first();

	  	$date_now = date("Y-m-d");

	  	$dateexp = date('Y-m-d',strtotime($medicalData->date_left));

		if($medicalData->package== 0 || $date_now > $dateexp ){



		

		

		if(!empty($bill)){

			$create = DB::table('bill') ->where('regid', $caseID)->update( [ 'fromdate' => date('n/j/Y'),'todate' => $todate, 

				'BillDate' => date('n/d/Y'),'charges' => $charges_days,'Balance' =>$charges_days,'to_date' => date('Y-m-d',strtotime($todate)) ]);



		}

		else{

		$create = Billing::create([

			'regid' => $caseID,

			'fromdate' => date('n/j/Y'),

			'todate' => $todate,

			'BillDate' => date('n/d/Y'),

			'charges' => $charges_days,

			'Balance' =>$charges_days,

			'BillNo' => $billno,

			'to_date' => date('Y-m-d',strtotime($todate)),



		]);

	}

}

		$pid = $casep->id;

		$affected = DB::table('case_potencies')

              ->where('id','=', $pid)

              ->update(['rxdays' => $request->rxdays , 'charges' => $charges_days, 'todate' => date('Y-m-d',strtotime($todate))]);



	   $token = DB::table('token')

              ->where('regid','=', $regid)

              ->update(['rowcolor' => 1]);

	   	echo $charges_days;

	   	$medicalcase = Medicalcase::where('regid', '=', $data['regid'])->first();

		if($medicalcase) {

		    if(isset($medicalcase->package)) {

	   		    $packages = DB::table('packages')->where('id','=',$medicalcase->package)->first();

	   		    $expiryPackageDate = $medicalcase->date_left;

    		    if(strtotime($expiryPackageDate) > strtotime(date('m/d/Y'))){

    		        //die('fdsd');

    		        $charges_days = 0;

    		    }

	   		}

		}

		

	   

	   	$potency_update = CasePotency::where('id', $pid)->first();

	   	$appdate = date('Y-m-d', strtotime($potency_update->created_at));



		// $potency_update->rxdays = $request->rxdays;

		// $potency_update->charges = $charges_days;

		// $potency_update->save();



		



		$courier = DB::table('courier_medicine')

              ->where('rand_id','=', $rand)

              ->update(['days' => $request->rxdays]);

		

		if($request->rxdays=="1 month"){

     			$days= "30 days";

     		}

     		elseif ($request->rxdays=="2 month") {

     			$days= "60 days";

     		}

     		elseif ($request->rxdays=="3 month") {

     			$days= "90 days";

     		}

     		elseif ($request->rxdays=="4 month") {

     			$days= "120 days";

     		}

     		elseif ($request->rxdays=="5 month") {

     			$days= "150 days";

     		}

     		elseif ($request->rxdays=="6 month") {

     			$days= "180 days";

     		}

     		elseif ($request->rxdays=="7 month") {

     			$days= "210 days";

     		}

     		else{

     			$days = $request->rxdays;

     		}



     		

        $NewDate= date('Y-m-d', strtotime($appdate. ' +'. $days));

     	$pending = DB::table('pending_appointments')->where('regid','=',$regid)->latest()->first();

     	

     	if($pending) {

     	    $pending_id = $pending->id;

         	$pending_update = Pendingappointments::where('id',$pending_id)->first();

            

    		if($pending_update){

    			$pending_update->days = $days;

    			$pending_update->next_date = $NewDate;

    			$pending_update->save();

    		}    

     	} else{

			$user1 = Pendingappointments::create([

			 'rand_id' => $rand,

			 'regid' => $regid,

			 'days' => $days,

			 'last_date' => $appdate,

			 'next_date' => $NewDate,

	        ]);

		}



		

		

		  die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function updateprescription(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

	    if(isset($data['rand']) && $data['rand']!=""){ $rand =  $data['rand']; } else { $rand = '';}



	    $caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;



	   	$casep = CasePotency::where('regid','=',$caseID)->latest()->first();

	   	$pid = $casep->id;



	   	$affected = DB::table('case_potencies')

              ->where('id','=', $pid)

              ->update(['rxprescription' => $request->rxprescription]);

             

		



		

	   	

	echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function copyremidy(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

	    $medicalData = DB::table('medicalcases')->where('regid', '=', $regid)->first();

	    $caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;



	   	$casep = CasePotency::where('regid','=',$caseID)->latest()->first();

	   	$pid = $casep->id;

	   	$potency_update = CasePotency::where('id', $pid)->first();





   	 	$cdays = $request->rxdays;

		$charges = CaseDaycharges::where('id','=',$cdays)->latest()->first();

		$charges_days = $charges->regular_charges;

		$todaydt = date('n/d/Y');

		$todate= date('n/d/Y', strtotime($todaydt. ' +'. $charges->days));



		$potency_update->rxprescription = $request->rxprescription;

		$potency_update->rxdays = $request->rxdays;

		$potency_update->rxfrequency = $request->rxfrequency;

		$potency_update->rxremedy = $request->rxremedy;

		$potency_update->rxpotency = $request->rxpotency;

		$potency_update->charges = $charges_days;

		$potency_update->todate = date('Y-m-d',strtotime($todate));

		



		$potency_update->save();



		$token = DB::table('token')

              ->where('regid','=', $regid)

              ->update(['rowcolor' => 1]);

              

		$update = DB::table('payments') ->where('case_id', $regid)->update( [ 'consultation_fee' => $charges_days, 'payment_date'=>date('Y-m-d') ]);



		

		$bill = Billing::latest()->first();

		$billno = $bill->BillNo + 1;

		$bill = Billing::where('fromdate','=',date('n/j/Y'))->where('regid','=',$caseID)->first();

	  	$date_now = date("Y-m-d");

	  	$dateexp = date('Y-m-d',strtotime($medicalData->date_left));

		if($medicalData->package== 0 || $date_now > $dateexp ){



		

		

		if(!empty($bill)){

			$create = DB::table('bill') ->where('regid', $caseID)->update( [ 'fromdate' => date('n/j/Y'),'todate' => $todate, 

				'BillDate' => date('n/d/Y'),'charges' => $charges_days,'Balance' =>$charges_days,'to_date' => date('Y-m-d',strtotime($todate)) ]);



		}

		else{

		$create = Billing::create([

			'regid' => $caseID,

			'fromdate' => date('n/j/Y'),

			'todate' => $todate,

			'BillDate' => date('n/j/Y'),

			'charges' => $charges_days,

			'Balance' =>$charges_days,

			'BillNo' => $billno,

			'to_date' => date('Y-m-d',strtotime($todate)),



		]);

	}

}

	echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}

	

	public function saveHeightWeights(Request $request){

	    $this->layout = null;

		$data = $request->all();

		//echo "<pre> test"; print_r($data); die();

	    if(isset($data['height']) && $data['height']!=""){ $height =  $data['height']; } else { $height = '';}

    	if(isset($data['weight']) && $data['weight']!=""){ $weight =  $data['weight']; } else { $weight = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['blood_pressure']) && $data['blood_pressure']!=""){ $blood_pressure =  $data['blood_pressure']; } else { $blood_pressure = '';}

    	if(isset($data['lmpdata']) && $data['lmpdata']!=""){ $lmp =  $data['lmpdata']; } else { $lmp = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['rand']) && $data['rand']!=""){ $rand=  $data['rand']; } else { $rand = '';}

    	if(isset($data['blood_pressure2']) && $data['blood_pressure2']!=""){ $blood_pressure2 =  $data['blood_pressure2']; } else { $blood_pressure2 = '';}

    	$check = DB::table('case_heights')->where([ ['dob', '=', $data['dateval']], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();



    	if($check==""){

			$user = CaseHeight::create([

	             'regid' => $regid,

				 'dob' => $data['dateval'],

				 'height' => $height,

				 'weight' => $weight,

				 'blood_pressure' => $blood_pressure,

				 'blood_pressure2' => $blood_pressure2,

				 'rand_id' =>$rand,

				 'lmp' => $lmp,

			]);

		

		

			echo "1"; die();

		

		}

		else{

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function addreminder(Request $request){

		

		$data = $request->all();

    	if(isset($data['remind_on']) && $data['remind_on']!=""){ $remind_on =  $data['remind_on']; } else { $remind_on = '';}

    	if(isset($data['timepick']) && $data['timepick']!=""){ $timepick =  $data['timepick']; } else { $timepick = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['heading']) && $data['heading']!=""){ $heading =  $data['heading']; } else { $heading = '';}

    	if(isset($data['comments']) && $data['comments']!=""){ $comments =  $data['comments']; } else { $comments = '';}

		$user = AddReminder::create([

             'remind_on' => $remind_on,

			 'timepick' => $timepick,

			 'regid' => $regid,

			 'heading' => $heading,

			 'comments' => $comments,





		]);

		 return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}

	



	public function communicationdetail(Request $request){

		$this->layout = null;

		$data = $request->all();

		if(isset($data['complaint_intesity']) && $data['complaint_intesity']!=""){ $complaint_intesity =  $data['complaint_intesity']; } else { $complaint_intesity = '';}

		

		if(isset($data['rand']) && $data['rand']!=""){ $rand=  $data['rand']; } else { $rand = '';}

		if(isset($data['currentdate']) && $data['currentdate']!=""){ $currentdate=  $data['currentdate']; } else { $currentdate = '';}

		if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

   		$check = CommunicationDetail::where([ ['currentdate', '=', $currentdate], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

   		if($check==""){

			$user = CommunicationDetail::create([

					'regid' => $regid,

					'rand_id' =>$rand,

					'currentdate' => $currentdate,

	                'complaint_intesity' => $complaint_intesity,

	        ]);



        	echo "1"; die();

		}

		else{

			echo "0"; die();

		}



       

	   // return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function examinationdetail(Request $request){



	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['examination_date']) && $data['examination_date']!=""){ $examination_date =  $data['examination_date']; } else { $examination_date = '';}

    	if(isset($data['examination_bp']) && $data['examination_bp']!=""){ $examination_bp =  $data['examination_bp']; } else { $examination_bp = '';}

    	if(isset($data['examination_bp1']) && $data['examination_bp1']!=""){ $examination_bp1 =  $data['examination_bp1']; } else { $examination_bp1 = '';}

    	if(isset($data['exmdata']) && $data['exmdata']!=""){ $exmdata =  $data['exmdata']; } else { $exmdata = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['examination_rand']) && $data['examination_rand']!=""){ $examination_rand=  $data['examination_rand']; } else { $examination_rand = '';}

    	$check = CaseExamination::where([ ['examination_date', '=', $examination_date], ['deleted_at','=',NULL], ['regid','=',$regid] ])->latest()->first();

    	//echo "<pre>test"; print_r(count($height));

    	if($check==""){

		$user = CaseExamination::create([

             'regid' => $regid,

             'rand_id' =>$examination_rand,

			 'examination_date' => $examination_date,

			 'bp1' => $examination_bp,

			 'bp2' => $examination_bp1,

			 'examination' => $exmdata,

		]);

	

			

			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

	

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function investigationdetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['invest_date']) && $data['invest_date']!=""){ $invest_date =  $data['invest_date']; } else { $invest_date = '';}

    	if(isset($data['hb']) && $data['hb']!=""){ $hb =  $data['hb']; } else { $hb = '';}

    	if(isset($data['t3']) && $data['t3']!=""){ $t3 =  $data['t3']; } else { $t3 = '';}

    	if(isset($data['t4']) && $data['t4']!=""){ $t4 =  $data['t4']; } else { $t4 = '';}

    	if(isset($data['tsh']) && $data['tsh']!=""){ $tsh =  $data['tsh']; } else { $tsh = '';}

    	if(isset($data['totalchol']) && $data['totalchol']!=""){ $totalchol =  $data['totalchol']; } else { $totalchol = '';}

    	if(isset($data['hdichol']) && $data['hdichol']!=""){ $hdichol =  $data['hdichol']; } else { $hdichol = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['invest_rand']) && $data['invest_rand']!=""){ $invest_rand=  $data['invest_rand']; } else { $invest_rand = '';}

    	$check = CaseInvestigation::where([ ['invest_date', '=', $invest_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseInvestigation::create([

             'regid' => $regid,

             'rand_id' =>$invest_rand,

             'invest_date' =>$invest_date,

			 'hb' => $hb,

			 't3' => $t3,

			 't4' => $t4,

			 'tsh' => $tsh,

			 'totalchol' => $totalchol,

			 'hdichol' => $hdichol,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}





	public function picturedetail(Request $request){

	    $this->layout = null;

		$data = $request->all();



	    if(isset($data['pic_date']) && $data['pic_date']!=""){ $pic_date =  $data['pic_date']; } else { $pic_date = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['rand_id']) && $data['rand_id']!=""){ $rand_id=  $data['rand_id']; } else { $rand_id = '';}



	  	if ($files = $request->file('picture')) {

           $destinationPath = 'la-assets/img/'; // upload path

           $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();

           $files->move($destinationPath, $profileImage);

           $insert['picture'] = "$profileImage";

           $insert['rand_id'] =  $rand_id;

           $insert['regid'] = $regid;

        }

		$user = CaseImage::create($insert);

	//echo "success"; die();

        return redirect(config('laraadmin.adminRoute')."/medicalcases/".$regid);

	}



	public function savenotes(Request $request){

	    $this->layout = null;

		$data = $request->all();

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['notes']) && $data['notes']!=""){ $notes=  $data['notes']; } else { $notes = '';}

    	if(isset($data['notes_type']) && $data['notes_type']!=""){ $notes_type=  $data['notes_type']; } else { $notes_type = '';}

    	if(isset($data['rand']) && $data['rand']!=""){ $rand =  $data['rand']; } else { $rand = '';}



    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();



		$caseID = $caseData->id;



    	$CasePotency =  DB::table('case_potencies')->where('regid','=',$caseID)->orderBy('id','DESC')->first();



    	$potencyid = $CasePotency->id; 

    	

		$user = CaseNotes::create([

             'regid' => $potencyid,

             'notes' =>$notes,

             'notes_type' => $notes_type,

             'rand_id' => $rand,

             'dateval' => date('m/d/Y'),

		]);

	echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function updatenotes(Request $request)

	{	

		$notes = CaseNotes::where('id', $request->id)->first();

		

		$notes->notes = $request->notes;

		

		$notes->save();

		echo "success"; die();

	}



	public function semenanalysis(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['semen_date']) && $data['semen_date']!=""){ $semen_date =  $data['semen_date']; } else { $semen_date = '';}

    	if(isset($data['semen_analysis']) && $data['semen_analysis']!=""){ $semen_analysis =  $data['semen_analysis']; } else { $semen_analysis = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['semen_rand']) && $data['semen_rand']!=""){ $semen_rand=  $data['semen_rand']; } else { $semen_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseSemenanalysis::where([ ['dateval', '=', $semen_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseSemenanalysis::create([

             'regid' => $caseID,

             'rand_id' =>$semen_rand,

             'dateval' =>$semen_date,

			 'semen_analysis' => $semen_analysis,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function serologicalreports(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['serological_date']) && $data['serological_date']!=""){ $serological_date =  $data['serological_date']; } else { $serological_date = '';}

    	if(isset($data['serological_report']) && $data['serological_report']!=""){ $serological_report =  $data['serological_report']; } else { $serological_report = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['serological_rand']) && $data['serological_rand']!=""){ $serological_rand=  $data['serological_rand']; } else { $serological_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseSerological::where([ ['dateval', '=', $serological_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseSerological::create([

             'regid' => $caseID,

             'rand_id' =>$serological_rand,

             'dateval' =>$serological_date,

			 'serological_report' => $serological_report,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function diabetesdetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['diabetes_date']) && $data['diabetes_date']!=""){ $diabetes_date =  $data['diabetes_date']; } else { $diabetes_date = '';}

    	if(isset($data['blood_fasting']) && $data['blood_fasting']!=""){ $blood_fasting =  $data['blood_fasting']; } else { $blood_fasting = '';}

    	if(isset($data['blood_prandial']) && $data['blood_prandial']!=""){ $blood_prandial =  $data['blood_prandial']; } else { $blood_prandial = '';}



    	if(isset($data['blood_random']) && $data['blood_random']!=""){ $blood_random =  $data['blood_random']; } else { $blood_random = '';}



    	if(isset($data['urine_fasting']) && $data['urine_fasting']!=""){ $urine_fasting =  $data['urine_fasting']; } else { $urine_fasting = '';}



    	if(isset($data['urine_prandial']) && $data['urine_prandial']!=""){ $urine_prandial =  $data['urine_prandial']; } else { $urine_prandial = '';}



    	if(isset($data['urine_random']) && $data['urine_random']!=""){ $urine_random =  $data['urine_random']; } else { $urine_random = '';}



    	if(isset($data['glu_test']) && $data['glu_test']!=""){ $glu_test =  $data['glu_test']; } else { $glu_test = '';}



    	if(isset($data['glycosylated_hb']) && $data['glycosylated_hb']!=""){ $glycosylated_hb =  $data['glycosylated_hb']; } else { $glycosylated_hb = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['diabetes_rand']) && $data['diabetes_rand']!=""){ $diabetes_rand=  $data['diabetes_rand']; } else { $diabetes_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseDiabetes::where([ ['dateval', '=', $diabetes_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseDiabetes::create([

             'regid' => $caseID,

             'rand_id' =>$diabetes_rand,

             'dateval' =>$diabetes_date,

			 'blood_fasting' => $blood_fasting,

			 'blood_prandial' => $blood_prandial,

			 'blood_random' => $blood_random,

			 'urine_fasting' => $urine_fasting,

			 'urine_prandial' => $urine_prandial,

			 'urine_random' => $urine_random,

			 'glu_test' => $glu_test,

			 'glycosylated_hb' => $glycosylated_hb,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function cardiacreport(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['cardiac_date']) && $data['cardiac_date']!=""){ $cardiac_date =  $data['cardiac_date']; } else { $cardiac_date = '';}

    	if(isset($data['homocysteine']) && $data['homocysteine']!=""){ $homocysteine =  $data['homocysteine']; } else { $homocysteine = '';}

    	if(isset($data['ecg']) && $data['ecg']!=""){ $ecg =  $data['ecg']; } else { $ecg = '';}

    	if(isset($data['decho']) && $data['decho']!=""){ $decho =  $data['decho']; } else { $decho = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['cardiac_rand']) && $data['cardiac_rand']!=""){ $cardiac_rand=  $data['cardiac_rand']; } else { $cardiac_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseCardiac::where([ ['dateval', '=', $cardiac_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseCardiac::create([

             'regid' => $caseID,

             'rand_id' =>$cardiac_rand,

             'dateval' =>$cardiac_date,

			 'homocysteine' => $homocysteine,

			 'ecg' => $ecg,

			 'decho' => $decho,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function lipiddetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['lipid_date']) && $data['lipid_date']!=""){ $lipid_date =  $data['lipid_date']; } else { $lipid_date = '';}

    	if(isset($data['total_cholesterol']) && $data['total_cholesterol']!=""){ $total_cholesterol =  $data['total_cholesterol']; } else { $total_cholesterol = '';}

    	if(isset($data['triglycerides']) && $data['triglycerides']!=""){ $triglycerides =  $data['triglycerides']; } else { $triglycerides = '';}

    	if(isset($data['hdl_cholesterol']) && $data['hdl_cholesterol']!=""){ $hdl_cholesterol =  $data['hdl_cholesterol']; } else { $hdl_cholesterol = '';}

    	if(isset($data['ldl_cholesterol']) && $data['ldl_cholesterol']!=""){ $ldl_cholesterol =  $data['ldl_cholesterol']; } else { $ldl_cholesterol = '';}

    	if(isset($data['vldl']) && $data['vldl']!=""){ $vldl =  $data['vldl']; } else { $vldl = '';}

    	if(isset($data['hdl_ratio']) && $data['hdl_ratio']!=""){ $hdl_ratio =  $data['hdl_ratio']; } else { $hdl_ratio = '';}

    	if(isset($data['ldl_hdl']) && $data['ldl_hdl']!=""){ $ldl_hdl =  $data['ldl_hdl']; } else { $ldl_hdl = '';}

    	if(isset($data['lipoprotein']) && $data['lipoprotein']!=""){ $lipoprotein =  $data['lipoprotein']; } else { $lipoprotein = '';}

    	if(isset($data['apolipoprotein_a']) && $data['apolipoprotein_a']!=""){ $apolipoprotein_a =  $data['apolipoprotein_a']; } else { $apolipoprotein_a = '';}

    	if(isset($data['apolipoprotein_b']) && $data['apolipoprotein_b']!=""){ $apolipoprotein_b =  $data['apolipoprotein_b']; } else { $apolipoprotein_b = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['lipid_rand']) && $data['lipid_rand']!=""){ $lipid_rand=  $data['lipid_rand']; } else { $lipid_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseLipid::where([ ['dateval', '=', $lipid_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseLipid::create([

             'regid' => $caseID,

             'rand_id' =>$lipid_rand,

             'dateval' =>$lipid_date,

			 'total_cholesterol' => $total_cholesterol,

			 'triglycerides' => $triglycerides,

			 'hdl_cholesterol' => $hdl_cholesterol,

			 'ldl_cholesterol' => $ldl_cholesterol,

			 'vldl' => $vldl,

			 'hdl_ratio' => $hdl_ratio,

			 'ldl_hdl' => $ldl_hdl,

			 'lipoprotein' => $lipoprotein,

			 'apolipoprotein_a' => $apolipoprotein_a,

			 'apolipoprotein_b' => $apolipoprotein_b,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function liverdetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['liver_date']) && $data['liver_date']!=""){ $liver_date =  $data['liver_date']; } else { $liver_date = '';}

    	if(isset($data['total_bil']) && $data['total_bil']!=""){ $total_bil =  $data['total_bil']; } else { $total_bil = '';}

    	if(isset($data['dir_bilirubin']) && $data['dir_bilirubin']!=""){ $dir_bilirubin =  $data['dir_bilirubin']; } else { $dir_bilirubin = '';}

    	if(isset($data['ind_bilirubin']) && $data['ind_bilirubin']!=""){ $ind_bilirubin =  $data['ind_bilirubin']; } else { $ind_bilirubin = '';}

    	if(isset($data['gamma_gt']) && $data['gamma_gt']!=""){ $gamma_gt =  $data['gamma_gt']; } else { $gamma_gt = '';}

    	if(isset($data['total_protein']) && $data['total_protein']!=""){ $total_protein =  $data['total_protein']; } else { $total_protein = '';}

    	if(isset($data['albumin']) && $data['albumin']!=""){ $albumin =  $data['albumin']; } else { $albumin = '';}

    	if(isset($data['globulin']) && $data['globulin']!=""){ $globulin =  $data['globulin']; } else { $globulin = '';}

    	if(isset($data['sgot']) && $data['sgot']!=""){ $sgot =  $data['sgot']; } else { $sgot = '';}

    	if(isset($data['sgpt']) && $data['sgpt']!=""){ $sgpt =  $data['sgpt']; } else { $sgpt = '';}

    	if(isset($data['alk_phos']) && $data['alk_phos']!=""){ $alk_phos =  $data['alk_phos']; } else { $alk_phos = '';}

    	if(isset($data['aust_antigen']) && $data['aust_antigen']!=""){ $aust_antigen =  $data['aust_antigen']; } else { $aust_antigen = '';}

    	if(isset($data['amylase']) && $data['amylase']!=""){ $amylase =  $data['amylase']; } else { $amylase = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['liver_rand']) && $data['liver_rand']!=""){ $liver_rand=  $data['liver_rand']; } else { $liver_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseLiver::where([ ['dateval', '=', $liver_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseLiver::create([

             'regid' => $caseID,

             'rand_id' =>$liver_rand,

             'dateval' =>$liver_date,

			 'total_bil' => $total_bil,

			 'dir_bilirubin' => $dir_bilirubin,

			 'ind_bilirubin' => $ind_bilirubin,

			 'gamma_gt' => $gamma_gt,

			 'total_protein' => $total_protein,

			 'albumin' => $albumin,

			 'globulin' => $globulin,

			 'sgot' => $sgot,

			 'sgpt' => $sgpt,

			 'alk_phos' => $alk_phos,

			 'aust_antigen' => $aust_antigen,

  			 'amylase' => $amylase,



		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function specficdetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['specific_date']) && $data['specific_date']!=""){ $specific_date =  $data['specific_date']; } else { $specific_date = '';}

    	if(isset($data['other_findings']) && $data['other_findings']!=""){ $other_findings =  $data['other_findings']; } else { $other_findings = '';}

    	if(isset($data['define_field1']) && $data['define_field1']!=""){ $define_field1 =  $data['define_field1']; } else { $define_field1 = '';}

    	if(isset($data['define_field2']) && $data['define_field2']!=""){ $define_field2 =  $data['define_field2']; } else { $define_field2 = '';}

    	if(isset($data['define_field3']) && $data['define_field3']!=""){ $define_field3 =  $data['define_field3']; } else { $define_field3 = '';}

    	if(isset($data['define_field4']) && $data['define_field4']!=""){ $define_field4 =  $data['define_field4']; } else { $define_field4 = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['specific_rand']) && $data['specific_rand']!=""){ $specific_rand=  $data['specific_rand']; } else { $specific_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseSpecific::where([ ['dateval', '=', $specific_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseSpecific::create([

             'regid' => $caseID,

             'rand_id' =>$specific_rand,

             'dateval' =>$specific_date,

			 'other_findings' => $other_findings,

			 'define_field1' => $define_field1,

			 'define_field2' => $define_field2,

			 'define_field3' => $define_field3,

			 'define_field4' => $define_field4,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function immunologydetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['immunology_date']) && $data['immunology_date']!=""){ $immunology_date =  $data['immunology_date']; } else { $immunology_date = '';}

    	if(isset($data['igg']) && $data['igg']!=""){ $igg =  $data['igg']; } else { $igg = '';}

    	if(isset($data['ige']) && $data['ige']!=""){ $ige =  $data['ige']; } else { $ige = '';}

    	if(isset($data['igm']) && $data['igm']!=""){ $igm =  $data['igm']; } else { $igm = '';}

    	if(isset($data['iga']) && $data['iga']!=""){ $iga =  $data['iga']; } else { $iga = '';}

    	if(isset($data['itg']) && $data['itg']!=""){ $itg =  $data['itg']; } else { $itg = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['immunology_rand']) && $data['immunology_rand']!=""){ $immunology_rand=  $data['immunology_rand']; } else { $immunology_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseImmunology::where([ ['dateval', '=', $immunology_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseImmunology::create([

             'regid' => $caseID,

             'rand_id' =>$immunology_rand,

             'dateval' =>$immunology_date,

			 'igg' => $igg,

			 'ige' => $ige,

			 'igm' => $igm,

			 'iga' => $iga,

			 'itg' => $itg,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function maledetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['male_date']) && $data['male_date']!=""){ $male_date =  $data['male_date']; } else { $male_date = '';}

    	if(isset($data['size']) && $data['size']!=""){ $size =  $data['size']; } else { $size = '';}

    	if(isset($data['volume']) && $data['volume']!=""){ $volume =  $data['volume']; } else { $volume = '';}

    	if(isset($data['serum_psa']) && $data['serum_psa']!=""){ $serum_psa =  $data['serum_psa']; } else { $serum_psa = '';}

    	if(isset($data['other_findings']) && $data['other_findings']!=""){ $other_findings =  $data['other_findings']; } else { $other_findings = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['male_rand']) && $data['male_rand']!=""){ $male_rand=  $data['male_rand']; } else { $male_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseUsgmale::where([ ['dateval', '=', $male_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseUsgmale::create([

             'regid' => $caseID,

             'rand_id' =>$male_rand,

             'dateval' =>$male_date,

			 'size' => $size,

			 'volume' => $volume,

			 'serum_psa' => $serum_psa,

			 'other_findings' => $other_findings,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function xraydetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['xray_date']) && $data['xray_date']!=""){ $xray_date =  $data['xray_date']; } else { $xray_date = '';}

    	if(isset($data['radiological_report']) && $data['radiological_report']!=""){ $radiological_report =  $data['radiological_report']; } else { $radiological_report = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['xray_rand']) && $data['xray_rand']!=""){ $xray_rand=  $data['xray_rand']; } else { $xray_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseXray::where([ ['dateval', '=', $xray_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseXray::create([

             'regid' => $caseID,

             'rand_id' =>$xray_rand,

             'dateval' =>$xray_date,

			 'radiological_report' => $radiological_report,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function arithrisdetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['ar_date']) && $data['ar_date']!=""){ $ar_date =  $data['ar_date']; } else { $ar_date = '';}

    	if(isset($data['anti_o']) && $data['anti_o']!=""){ $anti_o =  $data['anti_o']; } else { $anti_o = '';}

    	if(isset($data['accp']) && $data['accp']!=""){ $accp =  $data['accp']; } else { $accp = '';}

    	if(isset($data['ra_factor']) && $data['ra_factor']!=""){ $ra_factor =  $data['ra_factor']; } else { $ra_factor = '';}

    	if(isset($data['alkaline']) && $data['alkaline']!=""){ $alkaline =  $data['alkaline']; } else { $alkaline = '';}

    	if(isset($data['complement']) && $data['complement']!=""){ $complement =  $data['complement']; } else { $complement = '';}

    	if(isset($data['ana']) && $data['ana']!=""){ $ana =  $data['ana']; } else { $ana = '';}

    	if(isset($data['c_react']) && $data['c_react']!=""){ $c_react =  $data['c_react']; } else { $c_react = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['ar_rand']) && $data['ar_rand']!=""){ $ar_rand=  $data['ar_rand']; } else { $ar_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseArthritis::where([ ['dateval', '=', $ar_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseArthritis::create([

             'regid' => $caseID,

             'rand_id' =>$ar_rand,

             'dateval' =>$ar_date,

             'accp' => $accp,

             'anti_o' => $anti_o,

             'c4' => $complement,

			 'ra_factor' => $ra_factor,

			 'alkaline' => $alkaline,

			 'ana' => $ana,

			 'c_react' => $c_react,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function femaledetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['female_date']) && $data['female_date']!=""){ $female_date =  $data['female_date']; } else { $female_date = '';}

    	if(isset($data['uterues_size']) && $data['uterues_size']!=""){ $uterues_size =  $data['uterues_size']; } else { $uterues_size = '';}

    	if(isset($data['thickness']) && $data['thickness']!=""){ $thickness =  $data['thickness']; } else { $thickness = '';}

    	if(isset($data['fibroids_no']) && $data['fibroids_no']!=""){ $fibroids_no =  $data['fibroids_no']; } else { $fibroids_no = '';}

    	if(isset($data['description']) && $data['description']!=""){ $description =  $data['description']; } else { $description = '';}

    	if(isset($data['ovary_size_rt']) && $data['ovary_size_rt']!=""){ $ovary_size_rt =  $data['ovary_size_rt']; } else { $ovary_size_rt = '';}

    	if(isset($data['ovary_size_lt']) && $data['ovary_size_lt']!=""){ $ovary_size_lt =  $data['ovary_size_lt']; } else { $ovary_size_lt = '';}

    	if(isset($data['ovary_volume_rt']) && $data['ovary_volume_rt']!=""){ $ovary_volume_rt =  $data['ovary_volume_rt']; } else { $ovary_volume_rt = '';}

    	if(isset($data['ovary_volume_lt']) && $data['ovary_volume_lt']!=""){ $ovary_volume_lt =  $data['ovary_volume_lt']; } else { $ovary_volume_lt = '';}

    	if(isset($data['follicles_rt']) && $data['follicles_rt']!=""){ $follicles_rt =  $data['follicles_rt']; } else { $follicles_rt = '';}

    	if(isset($data['follicles_lt']) && $data['follicles_lt']!=""){ $follicles_lt =  $data['follicles_lt']; } else { $follicles_lt = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['female_rand']) && $data['female_rand']!=""){ $female_rand=  $data['female_rand']; } else { $female_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseUsgfemale::where([ ['dateval', '=', $female_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseUsgfemale::create([

             'regid' => $caseID,

             'rand_id' =>$female_rand,

             'dateval' =>$female_date,

             'uterues_size' => $uterues_size,

             'thickness' => $thickness,

             'fibroids_no' => $fibroids_no,

			 'description' => $description,

			 'ovary_size_rt' => $ovary_size_rt,

			 'ovary_size_lt' => $ovary_size_lt,

			 'ovary_volume_rt' => $ovary_volume_rt,

			 'ovary_volume_lt' => $ovary_volume_lt,

			 'follicles_rt' => $follicles_rt,

			 'follicles_lt' => $follicles_lt,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function renaldetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['renal_date']) && $data['renal_date']!=""){ $renal_date =  $data['renal_date']; } else { $renal_date = '';}

    	if(isset($data['bun']) && $data['bun']!=""){ $bun =  $data['bun']; } else { $bun = '';}

    	if(isset($data['urea']) && $data['urea']!=""){ $urea =  $data['urea']; } else { $urea = '';}

    	if(isset($data['creatinine']) && $data['creatinine']!=""){ $creatinine =  $data['creatinine']; } else { $creatinine = '';}

    	if(isset($data['uric_acid']) && $data['uric_acid']!=""){ $uric_acid =  $data['uric_acid']; } else { $uric_acid = '';}

    	if(isset($data['calcium']) && $data['calcium']!=""){ $calcium =  $data['calcium']; } else { $calcium = '';}

    	if(isset($data['phosphorus']) && $data['phosphorus']!=""){ $phosphorus =  $data['phosphorus']; } else { $phosphorus = '';}

    	if(isset($data['sodium']) && $data['sodium']!=""){ $sodium =  $data['sodium']; } else { $sodium = '';}

    	if(isset($data['potassium']) && $data['potassium']!=""){ $potassium =  $data['potassium']; } else { $potassium = '';}

    	if(isset($data['chloride']) && $data['chloride']!=""){ $chloride =  $data['chloride']; } else { $chloride = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['renal_rand']) && $data['renal_rand']!=""){ $renal_rand=  $data['renal_rand']; } else { $renal_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseRenal::where([ ['dateval', '=', $renal_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseRenal::create([

             'regid' => $caseID,

             'rand_id' =>$renal_rand,

             'dateval' =>$renal_date,

             'bun' => $bun,

             'urea' => $urea,

             'creatinine' => $creatinine,

			 'uric_acid' => $uric_acid,

			 'calcium' => $calcium,

			 'sodium' => $sodium,

			 'phosphorus' => $phosphorus,

			 'potassium' => $potassium,

			 'chloride' => $chloride,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function stooldetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['stool_date']) && $data['stool_date']!=""){ $stool_date =  $data['stool_date']; } else { $stool_date = '';}

    	if(isset($data['color']) && $data['color']!=""){ $color =  $data['color']; } else { $color = '';}

    	if(isset($data['consistency']) && $data['consistency']!=""){ $consistency =  $data['consistency']; } else { $consistency = '';}

    	if(isset($data['mucus']) && $data['mucus']!=""){ $mucus =  $data['mucus']; } else { $mucus = '';}

    	if(isset($data['frank_blood']) && $data['frank_blood']!=""){ $frank_blood =  $data['frank_blood']; } else { $frank_blood = '';}

    	if(isset($data['adult_worm']) && $data['adult_worm']!=""){ $adult_worm =  $data['adult_worm']; } else { $adult_worm = '';}

    	if(isset($data['reaction']) && $data['reaction']!=""){ $reaction =  $data['reaction']; } else { $reaction = '';}

    	if(isset($data['pus_cells']) && $data['pus_cells']!=""){ $pus_cells =  $data['pus_cells']; } else { $pus_cells = '';}

    	if(isset($data['occult_blood']) && $data['occult_blood']!=""){ $occult_blood =  $data['occult_blood']; } else { $occult_blood = '';}

    	if(isset($data['macrophages']) && $data['macrophages']!=""){ $macrophages =  $data['macrophages']; } else { $macrophages = '';}

    	if(isset($data['rbc']) && $data['rbc']!=""){ $rbc =  $data['rbc']; } else { $rbc = '';}

    	if(isset($data['ova']) && $data['ova']!=""){ $ova =  $data['ova']; } else { $ova = '';}

    	if(isset($data['protozoa']) && $data['protozoa']!=""){ $protozoa =  $data['protozoa']; } else { $protozoa = '';}

    	if(isset($data['yeast_cell']) && $data['yeast_cell']!=""){ $yeast_cell =  $data['yeast_cell']; } else { $yeast_cell = '';}

    	if(isset($data['other_findings']) && $data['other_findings']!=""){ $other_findings =  $data['other_findings']; } else { $other_findings = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['stool_rand']) && $data['stool_rand']!=""){ $stool_rand=  $data['stool_rand']; } else { $stool_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseStool::where([ ['dateval', '=', $stool_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseStool::create([

             'regid' => $caseID,

             'rand_id' =>$stool_rand,

             'dateval' =>$stool_date,

             'color' => $color,

             'consistency' => $consistency,

             'mucus' => $mucus,

			 'frank_blood' => $frank_blood,

			 'adult_worm' => $adult_worm,

			 'reaction' => $reaction,

			 'pus_cells' => $pus_cells,

			 'occult_blood' => $occult_blood,

			 'macrophages' => $macrophages,

			 'rbc' => $rbc,

			 'ova' => $ova,

			 'protozoa' => $protozoa,

			 'yeast_cell' => $yeast_cell,

			 'other_findings' => $other_findings,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function endocrinedetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['ed_date']) && $data['ed_date']!=""){ $ed_date =  $data['ed_date']; } else { $ed_date = '';}

    	if(isset($data['t3']) && $data['t3']!=""){ $t3 =  $data['t3']; } else { $t3 = '';}

    	if(isset($data['t4']) && $data['t4']!=""){ $t4 =  $data['t4']; } else { $t4 = '';}

    	if(isset($data['tsh']) && $data['tsh']!=""){ $tsh =  $data['tsh']; } else { $tsh = '';}

    	if(isset($data['ft3']) && $data['ft3']!=""){ $ft3 =  $data['ft3']; } else { $ft3 = '';}

    	if(isset($data['ft4']) && $data['ft4']!=""){ $ft4 =  $data['ft4']; } else { $ft4 = '';}

    	if(isset($data['anti_tpo']) && $data['anti_tpo']!=""){ $anti_tpo =  $data['anti_tpo']; } else { $anti_tpo = '';}

    	if(isset($data['antibody']) && $data['antibody']!=""){ $antibody =  $data['antibody']; } else { $antibody = '';}

    	if(isset($data['prolactin']) && $data['prolactin']!=""){ $prolactin =  $data['prolactin']; } else { $prolactin = '';}

    	if(isset($data['fsh']) && $data['fsh']!=""){ $fsh =  $data['fsh']; } else { $fsh = '';}

    	if(isset($data['lsh']) && $data['lsh']!=""){ $lsh =  $data['lsh']; } else { $lsh = '';}

    	if(isset($data['progesterone_3']) && $data['progesterone_3']!=""){ $progesterone_3 =  $data['progesterone_3']; } else { $progesterone_3 = '';}

    	if(isset($data['progesterone']) && $data['progesterone']!=""){ $progesterone =  $data['progesterone']; } else { $progesterone = '';}

    	if(isset($data['dhea']) && $data['dhea']!=""){ $dhea =  $data['dhea']; } else { $dhea = '';}

    	if(isset($data['testosterone']) && $data['testosterone']!=""){ $testosterone =  $data['testosterone']; } else { $testosterone = '';}

    	if(isset($data['ama']) && $data['ama']!=""){ $ama =  $data['ama']; } else { $ama = '';}

    	if(isset($data['insulin']) && $data['insulin']!=""){ $insulin =  $data['insulin']; } else { $insulin = '';}

    	if(isset($data['glucose']) && $data['glucose']!=""){ $glucose =  $data['glucose']; } else { $glucose = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['ed_rand']) && $data['ed_rand']!=""){ $ed_rand=  $data['ed_rand']; } else { $ed_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseEndocrine::where([ ['dateval', '=', $ed_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseEndocrine::create([

             'regid' => $caseID,

             'rand_id' =>$ed_rand,

             'dateval' =>$ed_date,

             't3' => $t3,

             't4' => $t4,

             'tsh' => $tsh,

			 'ft3' => $ft3,

			 'ft4' => $ft4,

			 'anti_tpo' => $anti_tpo,

			 'antibody' => $antibody,

			 'prolactin' => $prolactin,

			 'fsh' => $fsh,

			 'lsh' => $lsh,

			 'progesterone_3' => $progesterone_3,

			 'progesterone' => $progesterone,

			 'dhea' => $dhea,

			 'testosterone' => $testosterone,

			 'ama' => $ama,

			 'insulin' => $insulin,

			 'glucose' => $glucose,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function urinedetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['urine_date']) && $data['urine_date']!=""){ $urine_date =  $data['urine_date']; } else { $urine_date = '';}

    	if(isset($data['color']) && $data['color']!=""){ $color =  $data['color']; } else { $color = '';}

    	if(isset($data['quantity']) && $data['quantity']!=""){ $quantity =  $data['quantity']; } else { $quantity = '';}

    	if(isset($data['appearance']) && $data['appearance']!=""){ $appearance =  $data['appearance']; } else { $appearance = '';}

    	if(isset($data['gra']) && $data['gra']!=""){ $gra =  $data['gra']; } else { $gra = '';}

    	if(isset($data['protein']) && $data['protein']!=""){ $protein =  $data['protein']; } else { $protein = '';}

    	if(isset($data['reaction']) && $data['reaction']!=""){ $reaction =  $data['reaction']; } else { $reaction = '';}

    	if(isset($data['pus_cells']) && $data['pus_cells']!=""){ $pus_cells =  $data['pus_cells']; } else { $pus_cells = '';}

    	if(isset($data['occult_blood']) && $data['occult_blood']!=""){ $occult_blood =  $data['occult_blood']; } else { $occult_blood = '';}

    	if(isset($data['sugar']) && $data['sugar']!=""){ $sugar =  $data['sugar']; } else { $sugar = '';}

    	if(isset($data['rbc']) && $data['rbc']!=""){ $rbc =  $data['rbc']; } else { $rbc = '';}

    	if(isset($data['acetone']) && $data['acetone']!=""){ $acetone =  $data['acetone']; } else { $acetone = '';}

    	if(isset($data['pigments']) && $data['pigments']!=""){ $pigments =  $data['pigments']; } else { $pigments = '';}

    	if(isset($data['urobilinogen']) && $data['urobilinogen']!=""){ $urobilinogen =  $data['urobilinogen']; } else { $urobilinogen = '';}

    	if(isset($data['epith_cells']) && $data['epith_cells']!=""){ $epith_cells =  $data['epith_cells']; } else { $epith_cells = '';}



    	if(isset($data['other_findings']) && $data['other_findings']!=""){ $other_findings =  $data['other_findings']; } else { $other_findings = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['urine_rand']) && $data['urine_rand']!=""){ $urine_rand=  $data['urine_rand']; } else { $urine_rand = '';}

    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseUrine::where([ ['dateval', '=', $urine_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseUrine::create([

             'regid' => $caseID,

             'rand_id' =>$urine_rand,

             'dateval' =>$urine_date,

             'color' => $color,

             'quantity' => $quantity,

             'appearance' => $appearance,

			 'gra' => $gra,

			 'protein' => $protein,

			 'reaction' => $reaction,

			 'pus_cells' => $pus_cells,

			 'occult_blood' => $occult_blood,

			 'sugar' => $sugar,

			 'rbc' => $rbc,

			 'acetone' => $acetone,

			 'pigments' => $pigments,

			 'urobilinogen' => $urobilinogen,

			 'epith_cells' => $epith_cells,

			 'other_findings' => $other_findings,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

       

    }



    public function cbcdetail(Request $request){

	    $this->layout = null;

		$data = $request->all();

	    if(isset($data['cbc_date']) && $data['cbc_date']!=""){ $cbc_date =  $data['cbc_date']; } else { $cbc_date = '';}

    	if(isset($data['hb']) && $data['hb']!=""){ $hb =  $data['hb']; } else { $hb = '';}

    	if(isset($data['wbc']) && $data['wbc']!=""){ $wbc =  $data['wbc']; } else { $wbc = '';}

    	if(isset($data['platelets']) && $data['platelets']!=""){ $platelets =  $data['platelets']; } else { $platelets = '';}

    	if(isset($data['vitaminb']) && $data['vitaminb']!=""){ $vitaminb =  $data['vitaminb']; } else { $vitaminb = '';}

    	if(isset($data['vitamind']) && $data['vitamind']!=""){ $vitamind =  $data['vitamind']; } else { $vitamind = '';}

    	if(isset($data['neutrophils']) && $data['neutrophils']!=""){ $neutrophils =  $data['neutrophils']; } else { $neutrophils = '';}

    	if(isset($data['lymphocytes']) && $data['lymphocytes']!=""){ $lymphocytes =  $data['lymphocytes']; } else { $lymphocytes = '';}

    	if(isset($data['eosinophils']) && $data['eosinophils']!=""){ $eosinophils =  $data['eosinophils']; } else { $eosinophils = '';}

    	if(isset($data['monocytes']) && $data['monocytes']!=""){ $monocytes =  $data['monocytes']; } else { $monocytes = '';}

    	if(isset($data['rbc']) && $data['rbc']!=""){ $rbc =  $data['rbc']; } else { $rbc = '';}

    	if(isset($data['basophils']) && $data['basophils']!=""){ $basophils =  $data['basophils']; } else { $basophils = '';}

    	if(isset($data['band_cells']) && $data['band_cells']!=""){ $band_cells =  $data['band_cells']; } else { $band_cells = '';}

    	if(isset($data['abnor_rbc']) && $data['abnor_rbc']!=""){ $abnor_rbc =  $data['abnor_rbc']; } else { $abnor_rbc = '';}

    	if(isset($data['abnor_wbc']) && $data['abnor_wbc']!=""){ $abnor_wbc =  $data['abnor_wbc']; } else { $abnor_wbc = '';}



    	if(isset($data['parasites']) && $data['parasites']!=""){ $parasites =  $data['parasites']; } else { $parasites = '';}

        if(isset($data['esr']) && $data['esr']!=""){ $esr =  $data['esr']; } else { $esr = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['cbc_rand']) && $data['cbc_rand']!=""){ $cbc_rand=  $data['cbc_rand']; } else { $cbc_rand = '';}



    	$caseData = DB::table('case_datas')->where('regid', '=', $regid)->first();

		$caseID = $caseData->id;

    	$check = CaseCbc::where([ ['dateval', '=', $cbc_date], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){

		$user = CaseCbc::create([

             'regid' => $caseID,

             'rand_id' =>$cbc_rand,

             'dateval' =>$cbc_date,

             'hb' => $hb,

             'wbc' => $wbc,

             'platelets' => $platelets,

			 'vitaminb' => $vitaminb,

			 'vitamind' => $vitamind,

			 'neutrophils' => $neutrophils,

			 'lymphocytes' => $lymphocytes,

			 'eosinophils' => $eosinophils,

			 'monocytes' => $monocytes,

			 'rbc' => $rbc,

			 'basophils' => $basophils,

			 'band_cells' => $band_cells,

			 'abnor_rbc' => $abnor_rbc,

			 'abnor_wbc' => $abnor_wbc,

			 'parasites' => $parasites,

             'esr' => $esr,

		]);



			echo "1"; die();

		}

		else{

			

			echo "0"; die();

		}

       

    }



    public function updateadditional(Request $request)

	{	

		$this->layout = null;

		$data = $request->all();

		$total = 0;

		//echo "<pre>"; print_r($request->all());

		if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	if(isset($data['rand_id']) && $data['rand_id']!=""){ $rand_id=  $data['rand_id']; } else { $rand_id = '';}

    	if(isset($data['additional_name']) && $data['additional_name']!=""){ $additional_name =  $data['additional_name']; } else { $additional_name = '';}

    	if(isset($data['additional_price']) && $data['additional_price']!=""){ $additional_price=  $data['additional_price']; } else { $additional_price = '';}

    	if(isset($data['currentdate']) && $data['currentdate']!=""){ $currentdate=  $data['currentdate']; } else { $currentdate = '';}

    	//echo $data['rid'];

    	$case = DB::table('case_datas') ->where('regid','=',$regid)->first();

    	$caseid = $case->id;

		$check = CasePotency::where([ ['regid','=',$caseid], ['deleted_at','=',NULL] ])->latest()->first();

		// echo "<pre>"; print_r($check); 	die();

			$charges = $check->additional_price;

		

			$total = $charges + $additional_price;



			$update = DB::table('case_potencies') ->where('id', '=',$check->id)->update( [ 'additional_price' => $total ]);



    	$chargeId = CaseAdditionalcharges::create([

             'regid' => $caseid,

             'rand_id' =>$rand_id,

             'dateval' =>date('n/d/Y'),

             'additional_price' => $additional_price,

             'additional_name' => $additional_name,

		]);

        

        $additionalCharge = CaseAdditionalcharges::where('regid', $regid)->where('rand_id', '=', $rand_id)->sum('additional_price');

        if(empty($additionalCharge)){ $additionalCharge =0;}

		$update = DB::table('payments') ->where('case_id', $regid)->update( [ 'medicine_price' => $total, 'updated_at' => date('Y-m-d h:i:s') ]);

		$arr = array('rowId'=>$chargeId['id'],'additionalCharge'=>$additionalCharge);

		echo json_encode($arr); die();

	}



	public function usersendsms(Request $request)

	{	

		$this->layout = null;

		$data = $request->all();

		

	//echo "<pre>"; print_r($request->all());

		if(isset($data['smsid']) && $data['smsid']!=""){ $smsid =  $data['smsid']; } else { $smsid = '';}

    	if(isset($data['message']) && $data['message']!=""){ $message=  $data['message']; } else { $message = '';}

    	$casedata = DB::table('case_datas')->where('regid','=',$data['smsid'])->first();

    	//echo "<pre>"; print_r($casedata); die();



    	$smsreport = Smsreport::create([

			 'regid' => $casedata->regid,

			 'send_date' => date('Y-m-d'),

			 'sms_type' => 'Normal',

		]);

    	$phone = $casedata->phone;

    	$name = $casedata->first_name." ".$casedata->surname;

    	if($phone==""){

    		$phone = $casedata->mobile1;

    	}

    	$value = str_replace('<<Name>>', $name, $message); 

    	//echo $value; die();

		$msg = $this->CaseUserSms($phone,$value); 

		return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');



	}





	/**

	 * Store a newly created organization in database.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		if(Module::hasAccess("Medicalcases", "create")) {

		

			$rules = Module::validateRules("Medicalcases", $request);

			

			$validator = Validator::make($request->all(), $rules);

			

			if ($validator->fails()) {

				return redirect()->back()->withErrors($validator)->withInput();

			}

			

			$insert_id = Module::insert("Medicalcases", $request);

			

			return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

			

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}

    

    function fetch(Request $request)

    {

     if($request->get('query'))

     {

      $query = $request->get('query');

      $data = DB::table('mmc_medicines')

        ->where('medicine_name', 'LIKE', "%{$query}%")

        ->limit(10)

        ->get();

      $output = '<ul class="dropdown-menu" style="display:block; position:relative">';

      foreach($data as $row)

      {

       $output .= '

       <li><a href="#">'.$row->medicine_name.'</a></li>

       ';

      }

      $output .= '</ul>';

      echo $output;

     }

    }



    function fetchmedicine(Request $request)

    {

     if($request->get('query'))

     {

      $query = $request->get('query');

      $data = DB::table('mmc_medicines')

        ->where('medicine_name', '=', $query)

        ->limit(10)

        ->get();

      $output = '';

      foreach($data as $row)

      {

       $output .= '

       <option>'.$row->related_diseases.'</option>

       ';

      }

     // $output .= '</ul>';

      echo $output;

     }

    }

    

	/**

	 * Display the specified organization.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		//echo "<pre>"; print_r($request->all());

		if(Module::hasAccess("Medicalcases", "view")) {

			

			//$medicalcase = Medicalcase::find($id);

			$potencyid="";

			$plast ="";

			$regId = $id;

			$medicalcase = Medicalcase::where('regid', '=', $regId)->first();

			$caseData = CaseData::where('regid', '=', $regId)->first();

			$caseID = $caseData->id;

			$case_docter = DB::table('doctors')

				->select('doctors.*','case_datas.regid','case_datas.assitant_doctor')

				->join('case_datas','case_datas.assitant_doctor','=','doctors.id')

				->where('case_datas.regid','=',$regId)

				->first();

			//echo "<pre>"; print_r($case_docter); die();

			$Homedetail = HomeoDetail::where('regid', '=', $regId)->orderBy('id','DESC')->first();

			$CasePotency =  DB::table('case_potencies')

				->select('case_potencies.*','stocks.name','stocks.id as sid','case_frequency.frequency','case_frequency.id as fid', 'potencies.id as pid' ,'potencies.name as pname', 'daycharges.id as dayid', 'daycharges.days')

				->join('stocks','stocks.id','=','case_potencies.rxremedy')

				->join('case_frequency','case_frequency.id','=','case_potencies.rxfrequency')

				->join('potencies','potencies.id','=','case_potencies.rxpotency')

				->join('daycharges','daycharges.id','=','case_potencies.rxdays')

				->where('case_potencies.regid','=',$caseID)

				->where('case_potencies.deleted_at','=',NULL)

				->orderBy('id','DESC')

				->get();



				$CasePotencyid =  DB::table('case_potencies')->where('case_potencies.regid','=',$caseID)->first();



				if($CasePotencyid){

					$notesid = $CasePotencyid->id;

				}

				else{

					$notesid = '0';

				}

				

				$pacakeid = DB::table('medicalcases')

				->select('medicalcases.*','packages.id as pid','packages.name as packname')

				->join('packages','packages.id','=','medicalcases.package')

				->where('medicalcases.regid','=',$regId)

				->first();

		//	echo "<pre>"; print_r($pacakeid); die();

			// die();

			$daysMedicine = DB::table('daycharges')->get();

			$Potency = DB::table('potencies1')->orderBy('id','ASC')->get();

			//echo "<pre>"; print_r($daysMedicine); die();

			$countpotency = count($CasePotency); 

			// $HeightWeight = CaseHeight::where('regid', '=', $regId)->orderBy('dob','DESC')->get();

			$CommunicationDetail = CommunicationDetail::where('regid', '=', $regId)->orderBy('currentdate','DESC')->get();

			$CaseExamination = CaseExamination::where('regid', '=', $regId)->orderBy('examination_date','DESC')->get();

			$CaseInvestigation = CaseInvestigation::where('regid', '=', $caseID)->orderBy('invest_date','DESC')->get();

			$stocks = DB::table('stocks')->get();

			$doctors = DB::table('doctors')->get();

			$payments = DB::table('payments')->where('case_id', '=', $regId)->orderBy('id','DESC')->first();

			$CaseImage = DB::table('upload_picture')->where([ ['regid', '=', $regId], ['deleted_at','=',NULL]])->orderBy('id','DESC')->get();

			$HeightWeight = DB::table('case_heights')->where([ ['regid', '=', $regId], ['deleted_at','=',NULL] ])->orderBy('dob','DESC')->get();

			$heightdata = json_decode(json_encode($HeightWeight), true);

			$exambp = DB::table('case_examination')->where([ ['regid', '=', $regId], ['deleted_at','=',NULL] ])->orderBy('examination_date','DESC')->get();

			$examdata = json_decode(json_encode($exambp), true);

			$heightweight = DB::table('heightweight')->get();

			

			$notes = DB::table('notes')->where([ ['regid', '=', $regId],['notes_type','=','other'] ])->orderBy('id','DESC')->get();

			$notes1 = DB::table('notes')->where([ ['regid', '=', $regId],['deleted_at','=',NULL] ])->orderBy('id','DESC')->get();

			$homeo = HomeoDetail::where('regid', '=', $regId)->first();

		//	echo "<pre>"; print_r($homeo); die();

			//echo print_r(count($CasePotency)); 

			if(count($CasePotency)!=0){



			$casep = CasePotency::where('regid','=',$caseID)->latest()->first();

			if($casep!=""){

	   		$pid = $casep->id;

	   		$potencyid = DB::table('case_potencies')

				->select('case_potencies.*','stocks.name','stocks.id as sid','case_frequency.frequency','case_frequency.id as fid', 'potencies.id as pid' ,'potencies.name as pname')

				->join('stocks','stocks.id','=','case_potencies.rxremedy')

				->join('case_frequency','case_frequency.id','=','case_potencies.rxfrequency')

				->join('potencies','potencies.id','=','case_potencies.rxpotency')

				->where('case_potencies.id','=',$pid)

				->first();

	   		}

	   		else{

	   			$potencyid = DB::table('case_potencies')

				->select('case_potencies.*','stocks.name','stocks.id as sid','case_frequency.frequency','case_frequency.id as fid', 'potencies.id as pid' ,'potencies.name as pname')

				->join('stocks','stocks.id','=','case_potencies.rxremedy')

				->join('case_frequency','case_frequency.id','=','case_potencies.rxfrequency')

				->join('potencies','potencies.id','=','case_potencies.rxpotency')

				->where('case_potencies.regid','=',$caseID)

				->first();

	   		}



	   		if($CasePotency!=""){

	   		

	   		$plast = DB::table('case_potencies')

				->select('case_potencies.*','stocks.name','stocks.id as sid','case_frequency.frequency','case_frequency.id as fid', 'potencies.id as pid' ,'potencies.name as pname')

				->join('stocks','stocks.id','=','case_potencies.rxremedy')

				->join('case_frequency','case_frequency.id','=','case_potencies.rxfrequency')

				->join('potencies','potencies.id','=','case_potencies.rxpotency')

				->where('case_potencies.regid','=',$caseID)

				->orderBy('id', 'desc')

				->skip(1)

				->take(1)

				->first();

	   		}

	   		else{

	   			$plast = DB::table('case_potencies')

				->select('case_potencies.*','stocks.name','stocks.id as sid','case_frequency.frequency','case_frequency.id as fid', 'potencies.id as pid' ,'potencies.name as pname')

				->join('stocks','stocks.id','=','case_potencies.rxremedy')

				->join('case_frequency','case_frequency.id','=','case_potencies.rxfrequency')

				->join('potencies','potencies.id','=','case_potencies.rxpotency')

				->where('case_potencies.regid','=',$caseID)

				->first();

	   		}



	   		}



	   		$caseh = DB::table('case_heights')->where([ ['dob', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		//echo "<pre>"; print_r($caseh); die();

	   		$caseex = CaseExamination::where([ ['examination_date', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$casecom = CommunicationDetail::where([ ['currentdate', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$caseinv = CaseInvestigation::where([ ['invest_date', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();



	   		$casent = DB::table('notes')->where([ ['dateval','=',date('m/d/Y')], ['deleted_at','=',NULL],['regid','=',$notesid] ])->first();



	   		$casept = CasePotency::where([ ['dateval', '=', date('n/d/Y')], ['deleted_at','=',NULL],['regid','=',$caseID] ])->first();

	   		//echo "<pre>"; print_r($casept); die();

	   		$casehomeo = HomeoDetail::where([  ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Homedetailss = HomeoDetail::where('regid', '=', $regId)->orderBy('id','DESC')->get();

	   		$casesemen = CaseSemenanalysis::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Semenanalysis = CaseSemenanalysis::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$caseserological = CaseSerological::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Serologicalreport = CaseSerological::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$casediabetes = CaseDiabetes::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Diabetesreport = CaseDiabetes::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$casecardiac = CaseCardiac::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Cardiacreport = CaseCardiac::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$caselipid= CaseLipid::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Lipidreport = CaseLipid::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$caseliver= CaseLiver::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Liverreport = CaseLiver::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$casespecific= CaseSpecific::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Specificreport = CaseSpecific::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$caseimmunology= CaseImmunology::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Immunologyreport = CaseImmunology::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$casemale= CaseUsgmale::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Malereport = CaseUsgmale::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$casexray= CaseXray::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Xrayreport = CaseXray::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$casearithris= CaseArthritis::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Arithrisreport = CaseArthritis::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$casefemale= CaseUsgfemale::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Femalereport = CaseUsgfemale::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$caserenal= CaseRenal::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Renalreport = CaseRenal::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$casestool= CaseStool::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Stoolreport = CaseStool::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$caseendocrine= CaseEndocrine::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Endocrinereport = CaseEndocrine::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$caseurine= CaseUrine::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Urinereport = CaseUrine::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$casecbc= CaseCbc::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->first();

	   		$Cbcreport = CaseCbc::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();

	   		$medicines = DB::table('stocks')->get();

	   		$cfrequency = DB::table('case_frequency')->get();

	   		$diseases = DB::table('diseases')->get();

	   		$daycharges = DB::table('daycharges')->get();

	   		$medicinetaking = DB::table('mmc_medicines')->limit(10)->get();

            

            

	   		$paydays = CasePotency::where('regid','=',$caseID)->latest()->first();

	   		$casecharges = CasePotency::where([ ['dateval', '=', date('d/m/Y')], ['deleted_at','=',NULL],['regid','=',$regId] ])->get();

	   		$addchg = CaseAdditionalcharges::where('regid', '=', $regId)->orderBy('dateval','DESC')->get();



	   		$couriermed = Couriermedicine::where([ ['currentdate', '=', date('Y-m-d')], ['deleted_at','=',NULL],['case_id','=',$regId] ])->first();

	   		

	   		$packages = array();

	   		if(isset($medicalcase->package)) {

	   		    $packages = DB::table('packages')->where('id','=',$medicalcase->package)->first();

	   		}

  

	   		//$family = DB::table('familygroup')->where('regid','=',$regId)->get();



	   		$family=  DB::table('familygroup')

				->select('familygroup.regid','familygroup.family_regid','case_datas.regid','case_datas.first_name')

				->join('case_datas','case_datas.regid','=','familygroup.family_regid')

				//->join('case_potencies','case_potencies.regid','=','payments.case_id')

				->where('familygroup.regid','=',$regId)

				->where('familygroup.deleted_at','=',NULL)

				// ->orWhere('familygroup.family_regid','=',$regId)

				->get();

				

	   		$chargesadd = DB::table('charges')->get();

	   		

	   		$billing = Billing::where('regid','=',$caseID)->latest()->first();

	   		$casecbc_show= CaseCbc::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$caseurine_show = CaseUrine::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$casestool_show = CaseStool::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$casearithris_show = CaseArthritis::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$caseendocrine_show = CaseEndocrine::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$caserenal_show = CaseRenal::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$casexray_show = CaseXray::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$casefemale_show = CaseUsgfemale::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$casemale_show = CaseUsgmale::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$caseimmunology_show = CaseImmunology::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$casespecific_show = CaseSpecific::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$caselipid_show = CaseLipid::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$caseliver_show = CaseLiver::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$casediabetes_show = CaseDiabetes::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$caseserological_show = CaseSerological::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$casesemen_show = CaseSemenanalysis::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();

	   		$casecardiac_show = CaseCardiac::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->get();



	   		$countcbc = CaseCbc::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countsero = CaseSerological::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countcardiac = CaseCardiac::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countsemen = CaseSemenanalysis::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countdiabetes = CaseDiabetes::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countliver = CaseLiver::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countlipid = CaseLipid::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countspecific = CaseSpecific::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countimmunology = CaseImmunology::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countmale = CaseUsgmale::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countfemale = CaseUsgfemale::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countxray = CaseXray::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countrenal = CaseRenal::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countendocrine = CaseEndocrine::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countarithris = CaseArthritis::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$countstool = CaseStool::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();

	   		$counturine = CaseUrine::where([ ['deleted_at','=',NULL],['regid','=',$caseID] ])->count();



	   		$additional_price = CaseAdditionalcharges::where([ ['dateval', '=', date('n/j/Y')],['regid','=',$caseID] ])->latest()->first();



			if(isset($medicalcase->id)) {

				$module = Module::get('Medicalcases');

				$module->row = $medicalcase;

				

				return view('la.medicalcases.show', [

					'module' => $module,

					'view_col' => $this->view_col,

					'no_header' => true,

					'caseData'=>$caseData,

					'Homedetail'=>$Homedetail,

					'CasePotency' =>$CasePotency,

					'HeightWeight' =>$HeightWeight,

					'CommunicationDetail' =>$CommunicationDetail,

					'CaseExamination' =>$CaseExamination,

					'CaseInvestigation' =>$CaseInvestigation,

					'stocks'=>$stocks,

					'daysMedicine'=>$daysMedicine,

					'doctors'=>$doctors,

					'payments'=>$payments,

					'CaseImage'=>$CaseImage,

					'heightdata' => $heightdata,

					'heightweight'=>$heightweight,

					'notes'=>$notes,

					'notes1'=>$notes1,

					'potencyid' => $potencyid,

					'plast' => $plast,

					'caseh' => $caseh,

					'caseex' => $caseex,

					'casecom' => $casecom,

					'caseinv' => $caseinv,

					'casent' =>$casent,

					'homeo' => $homeo,

					'countpotency' => $countpotency,

					'examdata' => $examdata,

					'casept' => $casept,

					'casehomeo' => $casehomeo,

					'Homedetailss' => $Homedetailss,

					'casesemen' => $casesemen,

					'Semenanalysis' => $Semenanalysis,

					'caseserological' => $caseserological,

					'Serologicalreport' => $Serologicalreport,

					'casediabetes' => $casediabetes,

					'Diabetesreport' => $Diabetesreport,

					'casecardiac' => $casecardiac,

					'Cardiacreport' => $Cardiacreport,

					'caselipid' => $caselipid,

					'Lipidreport' => $Lipidreport,

					'caseliver' => $caseliver,

					'Liverreport' => $Liverreport,

					'casespecific' => $casespecific,

					'Specificreport' => $Specificreport,

					'caseimmunology' => $caseimmunology,

					'Immunologyreport' => $Immunologyreport,

					'casemale' => $casemale,

					'Malereport' =>$Malereport,

					'casexray' => $casexray,

					'Xrayreport' => $Xrayreport,

					'casearithris' => $casearithris,

					'Arithrisreport' => $Arithrisreport,

					'casefemale' => $casefemale,

					'Femalereport' => $Femalereport,

					'caserenal' => $caserenal,

					'Renalreport' => $Renalreport,

					'casestool' => $casestool,

					'Stoolreport' => $Stoolreport,

					'caseendocrine' => $caseendocrine,

					'Endocrinereport' => $Endocrinereport,

					'medicines' => $medicines,

					'cfrequency' => $cfrequency,

					'caseurine' => $caseurine,

					'Urinereport' => $Urinereport,

					'casecbc' => $casecbc,

					'Cbcreport' => $Cbcreport,

					'diseases' => $diseases,

					'daycharges' => $daycharges,

					'paydays' => $paydays,

					'casecharges' => $casecharges,

					'addchg' => $addchg,

					'couriermed' => $couriermed,

					'Potency'=>$Potency,

					'no_padding' => "no-padding",

					'medicinetaking' => $medicinetaking,

					'packages' => $packages,

					'family' => $family,

					'chargesadd' => $chargesadd,

					'pacakeid' => $pacakeid,

					'medicalcase' => $medicalcase,

					'billing' => $billing,

					'casecbc_show' => $casecbc_show,

					'caseurine_show' => $caseurine_show,

					'casestool_show' => $casestool_show,

					'casearithris_show' => $casearithris_show,

					'caseendocrine_show' => $caseendocrine_show,

					'caserenal_show' => $caserenal_show,

					'casexray_show' => $casexray_show,

					'casefemale_show' => $casefemale_show,

					'casemale_show' => $casemale_show,

					'caseimmunology_show' => $caseimmunology_show,

					'casespecific_show' => $casespecific_show,

					'caselipid_show' => $caselipid_show,

					'caseliver_show' => $caseliver_show,

					'casediabetes_show' => $casediabetes_show,

					'caseserological_show' => $caseserological_show,

					'casesemen_show' => $casesemen_show,

					'casecardiac_show' => $casecardiac_show,

					'countcbc' => $countcbc,

					'countsero' => $countsero,

					'countcardiac' => $countcardiac,

					'countsemen' => $countsemen,

					'countdiabetes' => $countdiabetes,

					'countliver' => $countliver,

					'countlipid' => $countlipid,

					'countspecific' => $countspecific,

					'countimmunology' => $countimmunology,

					'countmale' => $countmale,

					'countfemale' => $countfemale,

					'countxray' => $countxray,

					'countrenal' => $countrenal,

					'countendocrine' => $countendocrine,

					'countarithris' => $countarithris,

					'countstool' => $countstool,

					'counturine' => $counturine,

					'additional_price' => $additional_price,

					'case_docter' => $case_docter,



				])->with('medicalcase', $medicalcase);

			} else {

				return view('errors.404', [

					'record_id' => $id,

					'record_name' => ucfirst("medicalcase"),

				]);



			}

		} else {

			return redirect(config('laraadmin.adminRoute')."/");



		}

	}





	public function modalview(){

		$CaseExamination = CaseExamination::get();

		$view = view('la.medicalcases.index');

		echo $view->render(); // Hello, World!

		return response()->json(array('success' => true, 'html'=>$view));

	}



	/**

	 * Show the form for editing the specified organization.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit($id)

	{	

		$data = CaseData::where('regid','=', $id)->first();

		$doctors = DB::table('doctors')->get();

		$packages = DB::table('packages')->get();

		$module = Module::get('Medicalcases');

		return View('la.medicalcases.edit', [

				'medicalcase' => $data,	

				'doctors' => $doctors,	

				'packages' => $packages,

				'view_col' => $this->view_col,	

				'module' => $module,	

			]);

						

	}



	public function homeoedit($id)

	{

		 

		if(Module::hasAccess("Medicalcases", "edit")) {

			$homeodetail = HomeoDetail::find($id);

			if(isset($homeodetail->id)) {

				

				$module = Module::get('Medicalcases');

				

				$module->row = $homeodetail;

				return view('la.medicalcases.homeoedit', [

					'module' => $module,

					'view_col' => $this->view_col,

				])->with('homeodetail', $homeodetail);

			} else {

				return view('errors.404', [

					'record_id' => $id,

					'record_name' => ucfirst("homeodetail"),

				]);

			}

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}



	public function potencyedit($id)

	{

		 

		if(Module::hasAccess("Medicalcases", "edit")) {

			$potencydetail = CasePotency::find($id);

			if(isset($potencydetail->id)) {

				

				$module = Module::get('Medicalcases');

				

				$module->row = $potencydetail;

				return view('la.medicalcases.potencyedit', [

					'module' => $module,

					'view_col' => $this->view_col,

				])->with('potencydetail', $potencydetail);

			} else {

				return view('errors.404', [

					'record_id' => $id,

					'record_name' => ucfirst("potencydetail"),

				]);

			}

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}



	public function heightweightedit($id)

	{

		 

		if(Module::hasAccess("Medicalcases", "edit")) {

			$heightweight = CaseHeight::find($id);

			if(isset($heightweight->id)) {

				

				$module = Module::get('Medicalcases');

				

				$module->row = $heightweight;

				return view('la.medicalcases.heightedit', [

					'module' => $module,

					'view_col' => $this->view_col,

				])->with('heightweight', $heightweight);

			} else {

				return view('errors.404', [

					'record_id' => $id,

					'record_name' => ucfirst("heightweight"),

				]);

			}

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}



	/**

	 * Update the specified organization in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request, $id)

	{

		if(Module::hasAccess("Medicalcases", "edit")) {

			$data =$request->all();

			//echo $id;

		/*echo "<pre>"; print_r($data); echo "</pre>"; die();*/



			$rules = Module::validateRules("Medicalcases", $request, true);

			

			$validator = Validator::make($request->all(), $rules);

			

			if ($validator->fails()) {

				return redirect()->back()->withErrors($validator)->withInput();;

			}



			$insert_id = Module::updateRow("Medicalcases", $request, $id);

			

			return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

			

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}



	public function updatereceived(Request $request)

	{	

		//echo $request->id.",".$request->examination_date.$request->examination_bp.",".$request->examination_bp1; die();

		$chg = CasePotency::where('rand_id', $request->rand_id)->first();

		$regid = $chg->regid;

		$rprice = $request->received_price;

	//	echo "<pre>"; print_r($chg->regid);die();

		$chg->received_price = $rprice;

		$chg->received_date = date('Y-m-d');

		$chg->save();



		$update = DB::table('payments') ->where('case_id', $regid)->update( [ 'received_price' => $rprice, 'updated_at' => date('Y-m-d h:i:s'), 'received_date'=>date('Y-m-d')  ]);

		

		$chg1 = CaseAdditionalcharges::where([ ['rand_id', $request->rand_id], ['additional_name','=','Cash'] ])->first();

		if($chg1){

			$recid =$chg1->id;

			

			$totalprice = $request->received_price;

		    $update = DB::table('additional_charges') ->where('rand_id', $request->rand_id) -> where('additional_name','=','Cash')->delete();

            $payment_rec = CaseAdditionalcharges::create([

    			 'regid' => $regid,

	             'rand_id' =>$request->rand_id,

	             'dateval' =>date('d/m/Y'),

	             'received_price' => $request->received_price,

	             'additional_name' => "Cash",





	    	]);

	    	

		}

		else{

		$payment_rec = CaseAdditionalcharges::create([

    			'regid' => $regid,

	             'rand_id' =>$request->rand_id,

	             'dateval' =>date('d/m/Y'),

	             'received_price' => $request->received_price,

	             'additional_name' => "Cash",





	    	]);

		}

	}



	public function getadditional(Request $request)

	{	

	

		 $rand_id = $request->rand_id;

		 $output="";

		

		 $caseData = CaseData::where('regid', '=', $request->regid)->first();

			$caseID = $caseData->id;

		//$products= CaseAdditionalcharges::where('regid',$caseID)->get();

		$products=	DB::table('additional_charges')

					->select('additional_charges.*','charges.id as chgid', 'charges.charges')

					->join('charges', 'charges.id','=', 'additional_charges.additional_name')

					->where('additional_charges.regid', $caseID)->orderBy('id','desc')->get();



		// echo "<pre>"; print_r($products); die();

		

		foreach ($products as $key => $product) {

			if($product->received_price==""){

			$output = "<tr id='$product->id'>";

			$output .= "<td>".$product->dateval."</td>";

			$output .= "<td>".$product->charges."</td>";

			$output .= "<td>".$product->additional_price."</td>";

			$output .= "<td><a href='javascript:void(0)' onclick='removecharges($product->id);'>Remove</a></td>";

			$output .= "</tr>";

		

			echo $output;

		}

		}

		

	}


	public function getregcharges(Request $request)
	{
		$output="";

		$caseid=DB::table('case_datas')->where('regid','=',$request->regid)->first();
		$products=DB::table('bill')->where('regid','=',$caseid->id)->orderBy("id", "desc")->first();
		
		
		//foreach ($products as $key => $product) {
		$output= $products->charges."/";
		$output.= $request->regid."/";
		$output .= $products->id;
		echo $output;
		//}
	// 	return Response($output);
	}


	public function updatecharges(Request $request)
	{
		//echo "<pre>"; print_r($request->all());
		 $caseid = DB::table('case_datas')->where('regid', $request->regid)->first();
		// echo "<pre>"; print_r($caseid); die();
		$case_pot=DB::table('case_potencies')->where('regid','=',$caseid->id)->orderBy("id", "desc")->first();
	//	die();
		$update = DB::table('bill')->where('id','=',$request->chgid)->update(['charges' => $request->updatechg, 'Balance' => $request->updatechg, ]);

		$update_pot = DB::table('case_potencies')->where('id','=',$case_pot->id)->update(['charges' => $request->updatechg]);

		//echo "success";
		
		return redirect("http://smartops.co.in/managemyclinic/admin/medicalcases/".$request->regid);
	}


	public function getpaymentinfo(Request $request)

	{	

	

		 $rand_id = $request->rand_id;

		 $casename = $request->casename;

		 $output="";

		



		/*$products= CaseAdditionalcharges::where('rand_id',$rand_id)->get();

		

		

		foreach ($products as $key => $product) {

			if($product->additional_price==0){

				$price = "";

			}

			else{

				$price = $product->additional_price;

			}

			$output = "<tr>";

			$output .= "<td>".$product->regid."</td>";

			$output .= "<td>".$product->dateval."</td>";

			$output .= "<td>".$casename."</td>";

			$output .= "<td>".$product->additional_name."</td>";

			$output .= "<td>".$price."</td>";

			$output .= "<td>".$product->received_price."</td>";

			$output .= "</tr>"; */

			

					//echo "<pre>"; print_r($dataa); die();

			$case = DB::table('case_datas') ->where('id', $request->regidcase)->first();

			//$products= Billing::where('regid',$request->regidcase)->get();

			$products=	DB::table('additional_charges')

					->select('additional_charges.*','charges.id as chgid', 'charges.charges')

					->join('charges', 'charges.id','=', 'additional_charges.additional_name')

					->where('additional_charges.regid', $request->regidcase)->orderBy('id','desc')->get();

			foreach ($products as $key => $product) {

			

			$output = "<tr>";

			$output .= "<td>".$case->regid."</td>";

			$output .= "<td>".$product->dateval."</td>";

			$output .= "<td>".$casename."</td>";

			$output .= "<td>".$product->charges."</td>";

			$output .= "<td>".$product->additional_price."</td>";

			$output .= "<td>".$product->additional_price."</td>";

			$output .= "</tr>";

		

			echo $output;

		}

		

	}



	public function getreceived(Request $request)

	{	

	

		 $rand_id = $request->rand_id;

		 

		 $output="";

		



		$products= CaseAdditionalcharges::where('rand_id',$rand_id)->get();

		// echo "<pre>"; print_r($products); die();

		

		foreach ($products as $key => $product) {

			

			$output = $product->received_price;

		

			echo $output;

		}

		

	}



	public function saveCaseHeightWeights(Request $request){

	  	$this->layout = null;

		$data = $request->all();

	

	    if(isset($data['height']) && $data['height']!=""){ $height =  $data['height']; } else { $height = '';}

    	if(isset($data['weight']) && $data['weight']!=""){ $weight =  $data['weight']; } else { $weight = '';}

    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}

    	

    	if(isset($data['lmpdata']) && $data['lmpdata']!=""){ $lmp =  $data['lmpdata']; } else { $lmp = '';}

    	if(isset($data['rand']) && $data['rand']!=""){ $rand=  $data['rand']; } else { $rand = '';}

    	

    	$check = DB::table('case_heights')->where([ ['dob', '=', $data['dateval']], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();



    	if($check==""){

			$user = CaseHeight::create([

	             'regid' => $regid,

				 'dob' => $data['dateval'],

				 'height' => $height,

				 'weight' => $weight,

				 'rand_id' =>$rand,

				 'lmp' => $lmp,

			]);

			echo "1"; die();

		}

		else{

			$heightid = $check->id;

			$heightdata = CaseHeight::where('id', $heightid)->first();



			$heightdata->height = $height;

			$heightdata->weight = $weight;

			$heightdata->lmp = $lmp;

			$heightdata->save();

		}

		echo "success"; die();

       //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function updateHeightWeights(Request $request)

	{	

	

		$height = CaseHeight::where('id', $request->id)->first();

		

		

		$height->height = $request->height;

		$height->weight = $request->weight;

		$height->lmp = $request->lmpdata;

		$height->blood_pressure = $request->blood_pressure;

		$height->blood_pressure2 = $request->blood_pressure2;

		$height->save();



		

		//return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

	}



	public function updateexaminationdetail(Request $request)

	{	

		//echo $request->id.",".$request->examination_date.$request->examination_bp.",".$request->examination_bp1; die();

		$exam1 = CaseExamination::where('id', $request->id)->first();

		

		//echo "<pre>"; print_r($height);

		

		$exam1->examination_date = $request->examination_date;

		$exam1->bp1 = $request->examination_bp;

		$exam1->bp2 = $request->examination_bp1;

		$exam1->examination = $request->exmdata;

		$exam1->save();



		

		// /echo "success" ; die();

		//return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

	}



	public function updatecommunicationdetail(Request $request)

	{	

		//echo "<pre>"; print_r($request->all()); 

		$com = CommunicationDetail::where('id', $request->id)->first();

		

		$com->complaint_intesity = $request->complaint_intesity;

		$com->save();

		//echo "success" ; die();

		//return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

	}



	public function updateinvestigationdetail(Request $request)

	{	

		//echo "<pre>"; print_r($request->all()); 

		$inv = CaseInvestigation::where('id', $request->id)->first();

		

		$inv->hb = $request->hb;

		$inv->t3 = $request->t3;

		$inv->t4 = $request->t4;

		$inv->tsh = $request->tsh;

		$inv->totalchol = $request->totalchol;

		$inv->hdichol = $request->hdichol;

		$inv->save();

		//echo "success" ; die();

		//return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

	}



	public function updatehomeodetail(Request $request)

	{

		

		$homeo = HomeoDetail::where('id', $request->id)->first();

		

		$homeo->diagnosis = $request->diagnosis;

		$homeo->complaint_intesity = $request->complaint_intesity;

		$medtk = implode(", ",$request->medication_taking);

		$homeo->medication_taking = $medtk;

		$homeo->investigation = $request->investigation;

		$homeo->case_taken = $request->case_taken;

		$str = implode(", ", $request->criteria);

		$homeo->criteria = $str;

		$homeo->save();

		//return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');





	}



	public function homeoupdate(Request $request)

	{

		

		$homeo = HomeoDetail::where('id', $request->id)->first();

		

		$homeo->diagnosis = $request->diagnosis;

		$homeo->complaint_intesity = $request->complaint_intesity;

		$homeo->medication_taking = $request->medication_taking;

		$homeo->investigation = $request->investigation;

		$homeo->case_taken = $request->case_taken;

		$homeo->save();

		return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');





	}



	public function caseupdate(Request $request)

	{



		

		$casedata = CaseData::where('id', $request->caseid)->first();



		$casedata->assitant_doctor = $request->assitant_doctor;

		$casedata->title = $request->title;

		$casedata->first_name = $request->first_name;

		$casedata->middle_name = $request->middle_name;

		$casedata->surname = $request->surname;

		$casedata->date_of_birth = $request->date_of_birth;

		$casedata->gender = $request->gender;

		$casedata->status = $request->status;

		$casedata->address = $request->address;

		$casedata->road = $request->road;

		$casedata->area = $request->area;

		$casedata->city = $request->city;

		$casedata->state = $request->state;

		$casedata->pin = $request->pin;

		$casedata->email = $request->email;

		$casedata->scheme = $request->scheme;

		$casedata->religion = $request->religion;

		$casedata->phone = $request->phone;

		$casedata->occupation = $request->occupation;

		$casedata->mobile1 = $request->mobile1;

		$casedata->mobile2 = $request->mobile2;

		

		$casedata->save();



		$medical = Medicalcase::where('regid', $request->regid)->first();



		$medical->name = $request->first_name;

		$medical->mobile = $request->phone;

		$medical->doctorname = $request->assitant_doctor;



		$medical->save();



		return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');





	}



	public function potencyupdate(Request $request)

	{	



		$potency = CasePotency::where('id', $request->id)->first();

		

		$potency->rxremedy = $request->rxremedy;

		$potency->rxpotency = $request->rxpotency;

		$potency->rxfrequency = $request->rxfrequency;

		$potency->rxdays = $request->rxdays;

		$potency->rxprescription = $request->rxprescription;

		$potency->save();

		return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

	}



	



	public function heightweightupdate(Request $request)

	{	

		//echo "<pre>"; print_r($request->all()); die();

		$height = CaseHeight::where('id', $request->id)->first();

		

		$height->dob = $request->dob;

		$height->height = $request->height;

		$height->weight = $request->weight;

		$height->lmp = $request->lmp;

		$height->blood_pressure = $request->blood_pressure;

		$height->save();

		return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

	}



	public function updatesemenanalysis(Request $request)

	{	

		

		$semen = CaseSemenanalysis::where('id', $request->id)->first();

		

		$semen->dateval = $request->semen_date;

		$semen->semen_analysis = $request->semen_analysis;

		$semen->save();



	}



	public function updateserologicalreports(Request $request)

	{	

		

		$serology = CaseSerological::where('id', $request->id)->first();

		

		$serology->dateval = $request->serological_date;

		$serology->serological_report = $request->serological_report;

		$serology->save();



	}



	public function updatediabetesdetail(Request $request)

	{	

		

		$diabetes = CaseDiabetes::where('id', $request->id)->first();

		

		$diabetes->blood_fasting = $request->blood_fasting;

		$diabetes->blood_prandial = $request->blood_prandial;

		$diabetes->blood_random = $request->blood_random;

		$diabetes->urine_fasting = $request->urine_fasting;

		$diabetes->urine_prandial = $request->urine_prandial;

		$diabetes->urine_random = $request->urine_random;

		$diabetes->glu_test = $request->glu_test;

		$diabetes->glycosylated_hb = $request->glycosylated_hb;

		$diabetes->save();



	}



	public function updatecardiacreport(Request $request)

	{	

		

		$cardiac = CaseCardiac::where('id', $request->id)->first();

		

		$cardiac->homocysteine = $request->homocysteine;

		$cardiac->ecg = $request->ecg;

		$cardiac->decho = $request->decho;

		$cardiac->save();



	}



	public function updatelipiddetail(Request $request)

	{	

		

		$lipid = CaseLipid::where('id', $request->id)->first();

		

		$lipid->total_cholesterol = $request->total_cholesterol;

		$lipid->triglycerides = $request->triglycerides;

		$lipid->hdl_cholesterol = $request->hdl_cholesterol;

		$lipid->ldl_cholesterol = $request->ldl_cholesterol;

		$lipid->vldl = $request->vldl;

		$lipid->hdl_ratio = $request->hdl_ratio;

		$lipid->ldl_hdl = $request->ldl_hdl;

		$lipid->lipoprotein = $request->lipoprotein;

		$lipid->apolipoprotein_a = $request->apolipoprotein_a;

		$lipid->apolipoprotein_b = $request->apolipoprotein_b;

		$lipid->save();



	}



	public function updateliverdetail(Request $request)

	{	

		

		$liver = CaseLiver::where('id', $request->id)->first();

		

		$liver->total_bil = $request->total_bil;

		$liver->dir_bilirubin = $request->dir_bilirubin;

		$liver->ind_bilirubin = $request->ind_bilirubin;

		$liver->gamma_gt = $request->gamma_gt;

		$liver->total_protein = $request->total_protein;

		$liver->albumin = $request->albumin;

		$liver->globulin = $request->globulin;

		$liver->sgot = $request->sgot;

		$liver->sgpt = $request->sgpt;

		$liver->alk_phos = $request->alk_phos;

		$liver->aust_antigen = $request->aust_antigen;

		$liver->amylase = $request->amylase;

		$liver->save();



	}



	public function updatespecficdetail(Request $request)

	{	

		$specific = CaseSpecific::where('id', $request->id)->first();

		

		$specific->other_findings = $request->other_findings;

		$specific->define_field1 = $request->define_field1;

		$specific->define_field2 = $request->define_field2;

		$specific->define_field3 = $request->define_field3;

		$specific->define_field4 = $request->define_field4;

		$specific->save();



	}



	public function updateimmunologydetail(Request $request)

	{	

		$immunology = CaseImmunology::where('id', $request->id)->first();

		

		$immunology->igg = $request->igg;

		$immunology->ige = $request->ige;

		$immunology->igm = $request->igm;

		$immunology->iga = $request->iga;

		$immunology->itg = $request->itg;

		$immunology->save();



	}



	public function updatemaledetail(Request $request)

	{	

		$male = CaseUsgmale::where('id', $request->id)->first();

		

		$male->size = $request->size;

		$male->volume = $request->volume;

		$male->serum_psa = $request->serum_psa;

		$male->other_findings = $request->other_findings;

		$male->save();



	}



	public function updatexraydetail(Request $request)

	{	

		

		$xray = CaseXray::where('id', $request->id)->first();

		

		$xray->radiological_report = $request->radiological_report;

		$xray->save();



	}



	public function updatearithrisdetail(Request $request)

	{	

		$arth = CaseArthritis::where('id', $request->id)->first();

		

		$arth->anti_o = $request->anti_o;

		$arth->accp = $request->accp;

		$arth->ra_factor = $request->ra_factor;

		$arth->alkaline = $request->alkaline;

		$arth->c4 = $request->complement;

		$arth->ana = $request->ana;

		$arth->c_react = $request->c_react;

		$arth->save();



	}



	public function updatefemaledetail(Request $request)

	{	

		$female = CaseUsgfemale::where('id', $request->id)->first();

		

		$female->uterues_size = $request->uterues_size;

		$female->thickness = $request->thickness;

		$female->fibroids_no = $request->fibroids_no;

		$female->description = $request->description;

		$female->ovary_size_rt = $request->ovary_size_rt;

		$female->ovary_size_lt = $request->ovary_size_lt;

		$female->ovary_volume_rt = $request->ovary_volume_rt;

		$female->ovary_volume_lt = $request->ovary_volume_lt;

		$female->follicles_rt = $request->follicles_rt;

		$female->follicles_lt = $request->follicles_lt;

		$female->save();



	}



	public function updaterenaldetail(Request $request)

	{	

		$renal = CaseRenal::where('id', $request->id)->first();

		

		$renal->bun = $request->bun;

		$renal->urea = $request->urea;

		$renal->uric_acid = $request->uric_acid;

		$renal->creatinine = $request->creatinine;

		$renal->calcium = $request->calcium;

		$renal->phosphorus = $request->phosphorus;

		$renal->sodium = $request->sodium;

		$renal->potassium = $request->potassium;

		$renal->chloride = $request->chloride;

		$renal->save();



	}



	public function updatestooldetail(Request $request)

	{	

		$stool = CaseStool::where('id', $request->id)->first();

		

		$stool->color = $request->color;

		$stool->consistency = $request->consistency;

		$stool->mucus = $request->mucus;

		$stool->frank_blood = $request->frank_blood;

		$stool->adult_worm = $request->adult_worm;

		$stool->reaction = $request->reaction;

		$stool->pus_cells = $request->pus_cells;

		$stool->occult_blood = $request->occult_blood;

		$stool->macrophages = $request->macrophages;

		$stool->rbc = $request->rbc;

		$stool->ova = $request->ova;

		$stool->protozoa = $request->protozoa;

		$stool->yeast_cell = $request->yeast_cell;

		$stool->other_findings = $request->other_findings;

		$stool->save();



	}



	public function updateendocrinedetail(Request $request)

	{	

		$ed = CaseEndocrine::where('id', $request->id)->first();

		

		$ed->t3 = $request->t3;

		$ed->t4 = $request->t4;

		$ed->tsh = $request->tsh;

		$ed->ft3 = $request->ft3;

		$ed->ft4 = $request->ft4;

		$ed->anti_tpo = $request->anti_tpo;

		$ed->antibody = $request->antibody;

		$ed->prolactin = $request->prolactin;

		$ed->fsh = $request->fsh;

		$ed->fsh = $request->fsh;

		$ed->progesterone_3 = $request->progesterone_3;

		$ed->progesterone = $request->progesterone;

		$ed->dhea = $request->dhea;

		$ed->testosterone = $request->testosterone;

		$ed->ama = $request->ama;

		$ed->insulin = $request->insulin;

		$ed->glucose = $request->glucose;



		$ed->save();



	}



	public function updateurinedetail(Request $request)

	{	

		$Urine = CaseUrine::where('id', $request->id)->first();

		

		$Urine->color = $request->color;

		$Urine->quantity = $request->quantity;

		$Urine->appearance = $request->appearance;

		$Urine->gra = $request->gra;

		$Urine->protein = $request->protein;

		$Urine->reaction = $request->reaction;

		$Urine->pus_cells = $request->pus_cells;

		$Urine->occult_blood = $request->occult_blood;

		$Urine->sugar = $request->sugar;

		$Urine->rbc = $request->rbc;

		$Urine->acetone = $request->acetone;

		$Urine->pigments = $request->pigments;

		$Urine->urobilinogen = $request->urobilinogen;

		$Urine->epith_cells = $request->epith_cells;

		$Urine->other_findings = $request->other_findings;

		$Urine->save();



	}



	public function updatecbcdetail(Request $request)

	{	

		$cbc = CaseCbc::where('id', $request->id)->first();

		

		$cbc->hb = $request->hb;

		$cbc->wbc = $request->wbc;

		$cbc->platelets = $request->platelets;

		$cbc->vitaminb = $request->vitaminb;

		$cbc->vitamind = $request->vitamind;

		$cbc->neutrophils = $request->neutrophils;

		$cbc->lymphocytes = $request->lymphocytes;

		$cbc->eosinophils = $request->eosinophils;

		$cbc->monocytes = $request->monocytes;

		$cbc->rbc = $request->rbc;

		$cbc->basophils = $request->basophils;

		$cbc->band_cells = $request->band_cells;

		$cbc->abnor_rbc = $request->abnor_rbc;

		$cbc->abnor_wbc = $request->abnor_wbc;

		$cbc->parasites = $request->parasites;

		$cbc->esr = $request->esr;

		$cbc->save();





	}



	public function getpacakge(Request $request){

		$caseid = $request->id;



		$medicalcase=DB::table('medicalcases')->where('regid','=',$caseid)->first();

		$packid = $medicalcase->package;

		if($packid!=""){

		$package=DB::table('packages')->where('id','=',$packid)->first();



		$packageexpiry = date('d/m/Y', strtotime($medicalcase->date_left));

	//	$currdate = date('d/m/Y');

		$packagestart = $medicalcase->package_start;



		$arr = array('packageexpiry'=>$packageexpiry,'packid'=>$packid,'pacakgestart'=>$medicalcase->package_start,'packagename'=>$package->name,'today'=>date('Y-m-d'),'regname'=>$medicalcase->name,'regmobile'=>$medicalcase->mobile,'currdate'=>date('Y-m-d'),'packagestart'=>$packagestart);

		}

		else{

			$arr = array("packageexpiry" => "N/A",'pacakgestart'=>"N/A",'packagename'=>"N/A" ,'packid'=>"N/A",'regname'=>$medicalcase->name,'regmobile'=>$medicalcase->mobile,'currdate'=>date('Y-m-d'),'packagestart'=>"");

		}



		echo json_encode($arr); die();

	}

	public function getheightweight(Request $request){

		$caseid = $request->id;



		$casedata=DB::table('case_datas')->where('regid','=',$caseid)->first();

		$HeightWeight = DB::table('case_heights')->where([ ['regid', '=', $caseid], ['deleted_at','=',NULL], ['dob','=',date('d/m/Y')] ])->first();

		$heightweight = DB::table('heightweight')->get();



		$ht= "";

		$wt= "";

		$date1 = $casedata->date_of_birth;

		$date2 = date("Y-m-d");



		$ts1 = strtotime($date1);

		$ts2 = strtotime($date2);



		$year1 = date('Y', $ts1);

		$year2 = date('Y', $ts2);



		$month1 = date('m', $ts1);

		$month2 = date('m', $ts2);



		$diff1 = (($year2 - $year1) * 12) + ($month2 - $month1);

		//echo $diff1;

        if($casedata->gender!=""){

		$string = $casedata->gender;



		//Get the first character.

		$firstCharacter = $string[0];

        }else{

            $firstCharacter = '';

        }

        foreach($heightweight as $heightweightval) {

        	if($heightweightval->months == $diff1 && $heightweightval->gender == $firstCharacter)

			{

				$ht= $heightweightval->height;

				$wt= $heightweightval->weight;

			}

            elseif($heightweightval->gender == $firstCharacter && $diff1 > 240){

                $ht= $heightweightval->height;

                $wt= $heightweightval->weight;



            }

        }

		$gender = $casedata->gender;

		$htdata= DB::table('case_heights')->where([ ['regid', '=', $caseid], ['deleted_at','=',NULL] ])->get();

		// echo "<pre>"; print_r($products); die();

		$tddata[] = "";

		foreach ($htdata as $key => $product) {

			

			$output = "<tr>";

			$output .= "<td>".$product->dob."</td>";

			$output .= "<td>".$product->height."</td>";

			$output .= "<td>".$product->weight."</td>";

			$output .= "<td>".$product->lmp."</td>";

			$output .= "</tr>";

		

			$tddata[] = $output;

	

		}

		if ($HeightWeight!=""){

			$height = $HeightWeight->height;

			$weight = $HeightWeight->weight;

			$lmp = $HeightWeight->lmp;

			$regid = $casedata->regid;

			$dateval = $HeightWeight->dob;

			$rand_id = $HeightWeight->rand_id;

		

			$arr = array("height" => $height,'weight'=>$weight,'lmp'=>$lmp ,'gender'=>$gender,'dateval'=>$dateval,'rand_id'=>$rand_id,'ht' =>$ht,'wt' =>$wt,'tddata' =>$tddata,'datevalht'=>$dateval);

		}

		else{

			$arr = array("height" => "",'weight'=>"",'lmp'=>"" ,'gender'=>$gender,'dateval'=>date('d/m/Y'),'rand_id'=>rand('99',9999),'ht' =>$ht,'wt' =>$wt,'tddata' =>$tddata,'datevalht'=>"");

		}



		



		echo json_encode($arr); die();

	}

	

	public function billconvertdate($date){

	      //return $date;

	      if($date!=""){

			$date1 = explode('/', $date);

			$newdate = $date1[1].'/'.$date1[0].'/'.$date1[2];

			return $newdate;

	      }else {

	          return '';

	      }

	}



	public function getbilldetails(Request $request)

	{

		$output="";

		$billdate = $this->billconvertdate($request->billdate);

		$dtbill = date('n/d/Y',strtotime($billdate));

		$bill_case_id = $request->bill_case_id;



		$caseid=DB::table('case_datas')->where('regid','=',$request->bill_case_id)->first();

		$products=DB::table('bill')->where([ ['regid','=',$caseid->id], ['BillDate','=', $dtbill] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $caseid->first_name." ". $caseid->surname ."/";

		$output.= $product->charges."/";

		$output.= $product->regid."/";

		$output.= $product->BillNo;

		}

		return Response($output);

	}



	public function search(Request $request)

	{

		

	

	$output="";

	$products=DB::table('case_datas')->where('regid','=',$request->search)->get();

	

	foreach ($products as $key => $product) {

	$output=$product->first_name.' '.$product->middle_name.' '.$product->surname;

	}

	// echo "<pre>"; print_r($output); die();

	return Response($output);

	   

	   

	}



	public function searchcb(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_cbc')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->hb."/";

		$output.= $product->rbc."/";

		$output.= $product->wbc."/";

		$output.= $product->platelets."/";

		$output.= $product->vitaminb."/";

		$output.= $product->vitamind."/";

		$output.= $product->neutrophils."/";

		$output.= $product->lymphocytes."/";

		$output.= $product->eosinophils."/";

		$output.= $product->monocytes."/";

		$output.= $product->basophils."/";

		$output.= $product->band_cells."/";

		$output.= $product->abnor_rbc."/";

		$output.= $product->abnor_wbc."/";

		$output.= $product->parasites."/";

		$output.= $product->esr;

		}

		return Response($output);

	}



	public function searchurine(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_urine')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->quantity."/";

		$output.= $product->color."/";

		$output.= $product->appearance."/";

		$output.= $product->reaction."/";

		$output.= $product->gra."/";

		$output.= $product->protein."/";

		$output.= $product->sugar."/";

		$output.= $product->acetone."/";

		$output.= $product->pigments."/";

		$output.= $product->occult_blood."/";

		$output.= $product->urobilinogen."/";

		$output.= $product->epith_cells."/";

		$output.= $product->pus_cells."/";

		$output.= $product->rbc."/";

		$output.= $product->other_findings;

		}

		return Response($output);

	}



	public function searchstool(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_stool')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->color."/";

		$output.= $product->consistency."/";

		$output.= $product->mucus."/";

		$output.= $product->frank_blood."/";

		$output.= $product->adult_worm."/";

		$output.= $product->reaction."/";

		$output.= $product->pus_cells."/";

		$output.= $product->occult_blood."/";

		$output.= $product->macrophages."/";

		$output.= $product->rbc."/";

		$output.= $product->ova."/";

		$output.= $product->cysts."/";

		$output.= $product->protozoa."/";

		$output.= $product->yeast_cell."/";

		$output.= $product->other_findings;

		}

		return Response($output);

	}



	public function searcharithris(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_arthritis')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->anti_o."/";

		$output.= $product->accp."/";

		$output.= $product->ra_factor."/";

		$output.= $product->alkaline."/";

		$output.= $product->ana."/";

		$output.= $product->c_react."/";

		$output.= $product->c4;

		}

		return Response($output);

	}



	public function searchendocrine(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_endocrine')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->t3."/";

		$output.= $product->t4."/";

		$output.= $product->tsh."/";

		$output.= $product->ft3."/";

		$output.= $product->ft4."/";

		$output.= $product->anti_tpo."/";

		$output.= $product->antibody."/";

		$output.= $product->prolactin."/";

		$output.= $product->fsh."/";

		$output.= $product->lsh."/";

		$output.= $product->progesterone_3."/";

		$output.= $product->progesterone."/";

		$output.= $product->dhea."/";

		$output.= $product->testosterone."/";

		$output.= $product->ama."/";

		$output.= $product->insulin."/";

		$output.= $product->glucose;

		}

		return Response($output);

	}



	public function searchrenal(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_renal_profile')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->bun."/";

		$output.= $product->urea."/";

		$output.= $product->creatinine."/";

		$output.= $product->uric_acid."/";

		$output.= $product->calcium."/";

		$output.= $product->phosphorus."/";

		$output.= $product->sodium."/";

		$output.= $product->potassium."/";

		$output.= $product->chloride;

		}

		return Response($output);

	}



	public function searchxray(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_xray')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->radiological_report."/";

		}

		return Response($output);

	}



	public function searchusgfemale(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_usgfemale')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->uterues_size."/";

		$output.= $product->thickness."/";

		$output.= $product->fibroids_no."/";

		$output.= $product->description."/";

		$output.= $product->ovary_size_rt."/";

		$output.= $product->ovary_size_lt."/";

		$output.= $product->ovary_volume_rt."/";

		$output.= $product->ovary_volume_lt."/";

		$output.= $product->follicles_rt."/";

		$output.= $product->follicles_lt;

		}

		return Response($output);

	}



	public function searchusgmale(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_usgmale')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->size."/";

		$output.= $product->volume."/";

		$output.= $product->serum_psa."/";

		$output.= $product->other_findings;

		}

		return Response($output);

	}



	public function searchimmunlogy(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_immunology')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->igg."/";

		$output.= $product->ige."/";

		$output.= $product->igm."/";

		$output.= $product->iga."/";

		$output.= $product->itg;

		}

		return Response($output);

	}



	public function searchspecific(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_specific')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->other_findings."/";

		$output.= $product->define_field1."/";

		$output.= $product->define_field2."/";

		$output.= $product->define_field3."/";

		$output.= $product->define_field4;

		}

		return Response($output);

	}



	public function searchcardiac(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_cardiac')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->homocysteine."/";

		$output.= $product->ecg."/";

		$output.= $product->decho;

		}

		return Response($output);

	}



	public function searchserology(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_serological_reports')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->serological_report."/";

		}

		return Response($output);

	}



	public function searchsemen(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_semenanalysis')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->semen_analysis."/";

		}

		return Response($output);

	}



	public function searchdiabetes(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_diabetes')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->blood_fasting."/";

		$output.= $product->blood_prandial."/";

		$output.= $product->blood_random."/";

		$output.= $product->urine_fasting."/";

		$output.= $product->urine_prandial."/";

		$output.= $product->urine_random."/";

		$output.= $product->glu_test."/";

		$output.= $product->glycosylated_hb;

		}

		return Response($output);

	}



	public function searchlipid(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_lipid_profile')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->total_cholesterol."/";

		$output.= $product->triglycerides."/";

		$output.= $product->hdl_cholesterol."/";

		$output.= $product->ldl_cholesterol."/";

		$output.= $product->vldl."/";

		$output.= $product->hdl_ratio."/";

		$output.= $product->ldl_hdl."/";

		$output.= $product->lipoprotein."/";

		$output.= $product->apolipoprotein_a."/";

		$output.= $product->apolipoprotein_b;

		}

		return Response($output);

	}



	public function searchliver(Request $request)

	{

		$output="";

		$regid = $request->regid;

		$dateval = $request->dateval;



		$products=DB::table('case_liver_profile')->where([ ['regid','=',$regid], ['dateval','=', $dateval] ])->get();

		

		foreach ($products as $key => $product) {

		$output= $product->total_bil."/";

		$output.= $product->dir_bilirubin."/";

		$output.= $product->ind_bilirubin."/";

		$output.= $product->gamma_gt."/";

		$output.= $product->total_protein."/";

		$output.= $product->albumin."/";

		$output.= $product->globulin."/";

		$output.= $product->sgot."/";

		$output.= $product->sgpt."/";

		$output.= $product->alk_phos."/";

		$output.= $product->aust_antigen."/";

		$output.= $product->amylase;

		}

		return Response($output);

	}



	public function searchcases(Request $request)

	{		

		$search_name = $request->get('search_name');

		$search_surname = $request->get('search_surname');

		$search_regid = $request->get('search_regid');

		$f_reg_id = $request->get('f_reg_id');

		$output="";



		if($search_name!=""){

			$data = DB::table('case_datas')

			->where('first_name', 'LIKE', "%{$search_name}%")

			->where('regid', '!=', $f_reg_id)

			->where('deleted_at', '=', NULL)

			->get();

		}



		if($search_surname!=""){

			$data = DB::table('case_datas')

			->where('surname', 'LIKE', "%{$search_surname}%")

			->where('regid', '!=', $f_reg_id)

			->where('deleted_at', '=', NULL)

			->get();

		}

		if($search_regid!=""){

			$data = DB::table('case_datas')

			->where('regid', '=', $search_regid)

			->where('regid', '!=', $f_reg_id)

			->where('deleted_at', '=', NULL)

			->get();

		}

		

		

	      foreach($data as $row)

	      {

	       	$output = "<tr  class='".$row->regid."'>";

			$output .= "<td onclick=focusfunction(".$row->regid.",'$row->first_name','$row->surname');>".$row->regid."</td>";

			$output .= "<td onclick=focusfunction(".$row->regid.",'$row->first_name','$row->surname');>".$row->first_name." ".$row->surname."</td>";

			$output .= "</tr>";

		

			echo $output;

	      }

	     

	    

	}



	public function searchfamily(Request $request)

	{		

		$search_fname = $request->get('search_fname');

		$search_fsurname = $request->get('search_fsurname');

		$search_fregid = $request->get('search_fregid');

		$output="";



		if($search_fname!=""){

			$data = DB::table('familygroup')

			->where('name', 'LIKE', "%{$search_fname}%")

			->get();

		}



		if($search_fsurname!=""){

			$data = DB::table('familygroup')

			->where('surname', 'LIKE', "%{$search_fsurname}%")

			->get();

		}

		if($search_fregid!=""){

			$data = DB::table('familygroup')

			->where('family_regid', '=', $search_regid)

			->get();

		}

		

		

	      foreach($data as $product)

	      {

	       	$output = "<tr id='".$product->family_regid."' class='".$product->family_regid."'>";

			$output .= "<td onclick='removefunction(".$product->family_regid.");'>".$product->family_regid."</td>";

			$output .= "<td onclick='removefunction(".$product->family_regid.");'>".$product->name." ".$product->surname."</td>";

			$output .= "</tr>";

		

			echo $output;

	      }

	     

	    

	}



	public function savefamily(Request $request){

		$this->layout = null;

		$data = $request->all();

		if(isset($data['f_regid']) && $data['f_regid']!=""){ $f_regid =  $data['f_regid']; } else { $f_regid = '';}

		if(isset($data['f_reg_id']) && $data['f_reg_id']!=""){ $f_reg_id =  $data['f_reg_id']; } else { $f_reg_id = '';}

		if(isset($data['f_name']) && $data['f_name']!=""){ $f_name =  $data['f_name']; } else { $f_name = '';}

		if(isset($data['f_surname']) && $data['f_surname']!=""){ $f_surname =  $data['f_surname']; } else { $f_surname = '';}

   		

    	$check = DB::table('familygroup')->where([ ['regid','=',$f_reg_id], ['family_regid','=',$f_regid], ['deleted_at','=',NULL]])->first();

    	if($check==""){

			$user = Familygroup::create([

					'regid' => $f_reg_id,

					'family_regid' =>$f_regid,

					'name' =>$f_name,

					'surname' =>$f_surname,

	        ]);



	        $user1 = Familygroup::create([

					'regid' => $f_regid,

					'family_regid' =>$f_reg_id,

					'name' =>$f_name,

					'surname' =>$f_surname,

	        ]);



	        echo "1"; die();

	    }

	    else{

	    	echo "0"; die();

	    }





       // echo "success"; die();



	}



	public function familydata(Request $request){

		$caseid = $request->id;



		$casedata=DB::table('familygroup')->where([ ['regid','=',$caseid], ['deleted_at','=',NULL]])->get();

		

		

		foreach ($casedata as $key => $product) {

			$casename=DB::table('case_datas')->where('regid','=',$product->family_regid)->first();



			$output = "<tr id='".$product->family_regid."' class='".$product->family_regid."'>";

			$output .= "<td onclick='removefunction(".$product->family_regid.");'>".$product->family_regid."</td>";

			$output .= "<td onclick='removefunction(".$product->family_regid.");'>".$casename->first_name." ".$casename->surname."</td>";

			$output .= "</tr>";

		

			echo $output;

	

		}

		

	}



	public function packagehistory(Request $request){

		$caseid = $request->id;

		$dataid=DB::table('case_datas')->where([ ['regid','=',$caseid], ['deleted_at','=',NULL]])->first();

		$case_id = $dataid->id;

		$casedata=DB::table('packagehistory')

					->select('packagehistory.*','packages.id as pid','packages.name','packages.tags')

					->join('packages','packages.id','=','packagehistory.packageid')

					->where([ ['packagehistory.regid','=',$case_id], ['packagehistory.deleted_at','=',NULL]])->orderBy('id','DESC')->get();

		

		

			

		foreach ($casedata as $key => $product) {

			

			

			$output = "<tr>";

			$output .= "<td>".date('d M Y',strtotime($product->packagedate))."</td>";

			$output .= "<td>".$product->name."</td>";

			$output .= "<td>".$product->tags."</td>";

			$output .= "<td>".date('d M Y',strtotime($product->fromdate))."</td>";

			$output .= "<td>".date('d M Y',strtotime($product->todate))."</td>";

			$output .= "<td><a href='javascript:void(0)' onclick='deletepackage($product->id);'>Remove</a></td>";

			$output .= "</tr>";

			echo $output;

		

			

	

		}

		

	}







	public function deletefamily(Request $request){

	     $this->layout = null;

		 $data = $request->all();

		 //echo $data['rand'];

	     Familygroup::where('family_regid', '=',$data['id'])->delete();

	    

	     echo "success"; die();

	}



	public function calculateheight(Request $request)

	{



		$dob = $request->db;

		$gender = $request->gender;

		

		

		$date1 = $dob;

		$date2 = date("d/m/Y");



		$ts1 = strtotime($date1);

		$ts2 = strtotime($date2);



		$year1 = date('Y', $ts1);

		$year2 = date('Y', $ts2);



		$month1 = date('m', $ts1);

		$month2 = date('m', $ts2);



		$diff1 = (($year2 - $year1) * 12) + ($month2 - $month1);

		



		$string = $gender;



		//Get the first character.

		$firstCharacter = $string[1];

		

		$output="";

		$height=DB::table('heightweight')->where([['months','=',$diff1],['gender','=',$firstCharacter]])->get();

		print_r($height);

		foreach ($height as $key => $product) {

			$output=$product->height."/";

			$output.=$product->weight;



		}

		//print_r($output);

		

		return Response($output);

	   

	   

	}



	/**

	 * Remove the specified organization from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function destroy($id)

	{



		if(Module::hasAccess("Medicalcases", "delete")) {

			//CaseData::find($id)->delete();

			CaseData::where('regid', '=',$id)->delete();

			Medicalcase::where('regid', '=',$id)->delete();

			// CasePotency::where('regid', '=',$id)->delete();

			// CaseHeight::where('regid', '=',$id)->delete();

			// CommunicationDetail::where('regid', '=',$id)->delete();

			// CaseExamination::where('regid', '=',$id)->delete();

			// CaseInvestigation::where('regid', '=',$id)->delete();

			// CaseImage::where('regid', '=',$id)->delete();

			// HomeoDetail::where('regid', '=',$id)->delete();

			// Redirecting to index() method

			return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}

	public function getpotency(Request $request){

     	$output="";

		$rand_id = $request->rand_id;

	



	  //   CasePotency::where('rand_id', '=',$data['rand_id'])->first();



     	$products=DB::table('case_potencies')->where('rand_id','=',$rand_id)->get();

		

		foreach ($products as $key => $product) {

		$output= $product->charges."/";

		$output.= $product->additional_price."/";

		$output.= $product->received_price;

		

		}

		return Response($output);

	     

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function deletedata(Request $request){

	     $this->layout = null;

		 $data = $request->all();

		 //echo $data['rand'];

	     CasePotency::where('id', '=',$data['rand'])->delete();

	    // CaseNotes::where('rand_id', '=',$data['rand'])->delete();

	    

	     echo $data['rand']; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function deletecharges(Request $request){

	     $this->layout = null;

		 $data = $request->all();

		 //echo $data['rand'];

        

	     $remove = CaseAdditionalcharges::where('id', '=',$data['id'])->first();

	     $randid = $remove->rand_id;

	     $charges = $remove->additional_price;



	     $updatechg = CasePotency::where('rand_id', '=',$randid)->first();



	     $additional = $updatechg->additional_price;

	     $total = $additional - $charges;

	    // $updatechg->additional_name = "";

	     $updatechg->additional_price = $total; 

	     $updatechg->save();



	     CaseAdditionalcharges::where('id', '=',$data['id'])->delete();

	     $additionalCharge = CaseAdditionalcharges::where('rand_id', '=', $randid)->sum('additional_price');

	     if(empty($additionalCharge)){ $additionalCharge =0;}

	     echo $additionalCharge; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}

	public function deleteheightdata(Request $request){

	     $this->layout = null;

		 $data = $request->all();

	     

	     CaseHeight::where('rand_id', '=',$data['rand'])->delete();

	     //HeightWeight::find($existuser->id)->delete();   

	     

		

	    echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function deletecommunication(Request $request){

	     $this->layout = null;

		 $data = $request->all();

		// echo $data['rand']; die();

	     CommunicationDetail::where('rand_id', '=',$data['rand'])->delete();

	    

	     echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function deleteexamination(Request $request){

	     $this->layout = null;

		 $data = $request->all();

		// echo $data['rand']; die();

	     CaseExamination::where('rand_id', '=',$data['rand'])->delete();

	    

	     echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function deleteinvestigation(Request $request){

	     $this->layout = null;

		 $data = $request->all();

		// echo $data['rand']; die();

	     CaseInvestigation::where('rand_id', '=',$data['rand'])->delete();

	    

	     echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}

	public function deletepicture(Request $request){

	    // $this->layout = null;

		 $data = $request->all();

		//echo $data['rand']; die();

	     CaseImage::where('rand_id', '=',$data['rand'])->delete();

	    

	     echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}



	public function deletepackage(Request $request){

	     $this->layout = null;

		 $data = $request->all();

		// echo $data; die();

		 $packhis = DB::table('packagehistory') ->where('id', $data['id'])->first();

 	 	$packid = DB::table('packagehistory') ->where('regid', $packhis->regid)->orderBy('id', 'desc')->skip(1)->take(1)->first();

 	 	$caseid = DB::table('case_datas') ->where('id', $packhis->regid)->first();



 	 	$updatemed = DB::table('medicalcases') ->where('regid', $caseid->regid)->update( [ 'package' => $packid->packageid , 'date_left' => $packid->todate ]);



	     $pack = DB::table('packagehistory') ->where('id', $data['id'])->update( [ 'deleted_at' => date('Y-m-d h:i:s') ]);

	    

	    // echo "success"; die();

        //return redirect(config('laraadmin.adminRoute')."/medicalcases");

	}





	 public function generatePDF(Request $request)

    {

    	$data = $request->all();

    	$my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('user_template.docx'));

    	$casesid=DB::table('case_datas')->where('id','=',$request->case_u_id)->first();

		$cases=DB::table('medicalcases')->where('regid','=',$casesid->regid)->first();

		$package = $cases->package;

		if($package!="0") {

   		   $package_date = $cases->date_left;

   		}

   		else{

   			$package_date = "N/A";

   		}

   		

		// $medget=  DB::table('payments')

		// ->select('payments.case_id','payments.received_date','case_potencies.regid','case_potencies.received_date','case_potencies.rxdays','case_potencies.dateval')

		// ->join('case_potencies','case_potencies.received_date','=','payments.received_date')

		// //->join('case_potencies','case_potencies.regid','=','payments.case_id')

		// ->where('payments.case_id','=',$request->case_u_id)

		// ->first();

		

		// $dateval= $this->convertdate($medget->dateval);



		// $days= $medget->rxdays;

		

		// $medicine_to= date('Y-m-d', strtotime($dateval. ' +'. $days));

		// $total_days = 'from '.$dateval.' to '.$medicine_to;



		$dayget = DB::table('bill')->where('BillNo','=',$request->billno)->first();

    	$my_template->setValue('patient_name',$request->case_name);

		$my_template->setValue('received',$request->rece_amount);

		$my_template->setValue('billno', $request->billno);

		$my_template->setValue('date',$request->bill_date); 

		$my_template->setValue('package',$package_date); 

		$my_template->setValue('fromdate',$dayget->fromdate); 

		$my_template->setValue('todate',$dayget->todate); 

		       



		    try{

		        $my_template->saveAs(storage_path('bill.docx'));

		    }catch (Exception $e){

		        //handle exception

		    }



		    return response()->download(storage_path('bill.docx'));

    }



	



	/**

	 * Datatable Ajax fetch

	 *

	 * @return

	 */

	public function dtajax()

	{

		//echo "<pre>test"; print_r($request->all());

	    $newlist = ['regid', 'name', 'doctorname','dob'];

		$values = DB::table('medicalcases')->select($newlist)->whereNull('deleted_at')->orderBy('regid','DESC');

		$out = Datatables::of($values)->make();

		$data = $out->getData();



		$fields_popup = ModuleFields::getModuleFields('Medicalcases');

	//	echo "<pre>"; print_r($data);die();

		for($i=0; $i < count($data->data); $i++) {

			for ($j=0; $j < count($this->listing_cols); $j++) {

				$col = $this->listing_cols[$j];

				if($fields_popup[$col] != null && $fields_popup[$col]->field_type_str == "Image") {

					if($data->data[$i][$j] != 0) {

						$img = \App\Models\Upload::find($data->data[$i][$j]);

						if(isset($img->name)) {

							$data->data[$i][$j] = '<img src="'.$img->path().'?s=50">';

						} else {

							$data->data[$i][$j] = "";

						}

					} else {

						$data->data[$i][$j] = "";

					}

				}

				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {

					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);

				}

			     $medicalcase = Medicalcase::where('id', '=', $data->data[$i][0])->first();

				if($col == $this->view_col) {

					if(Auth::user()->type != "Receptionist" ) {

						$data->data[$i][$j] = '<a class="namec" href="'.url(config('laraadmin.adminRoute') . '/medicalcases/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';

					}

				}

				if($col == 'id') {

					$data->data[$i][0] = $medicalcase['regid'];

				}

				// else if($col == "author") {

				//    $data->data[$i][$j];

				// }

			}

			

			if($this->show_action) {

				$output = '';

				if(Module::hasAccess("Medicalcases", "edit")) {

					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/medicalcases/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:0px 3px 0px 3px;"><i class="fa fa-edit"></i></a>';

				}

				

				if(Module::hasAccess("Medicalcases", "delete")) {

					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.medicalcases.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);

					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';

					$output .= Form::close();

				}

				

				$output .= '<a onClick="openPackage('.$data->data[$i][0].')" class="btn btn-success btn-xs" style="display:inline;padding:2px; margin-left:10px;">Package</a>';

				$output .= '<a onClick="openBill('.$data->data[$i][0].')" class="btn btn-primary btn-xs" style="display:inline;padding: 2px 7px 2px 8px; margin-left:10px;">Bill</a>';

				$output .= '<a onClick="openHeightWeight('.$data->data[$i][0].')" class="btn btn-primary btn-xs" style="display:inline;padding: 2px 7px 2px 8px; margin-left:10px; background-color:#343a40; border-color: #343a40;">Add Height</a>';

				$output .= '<a onClick="openFamily('.$data->data[$i][0].')" class="btn btn-primary btn-xs" style="display:inline;padding: 2px 7px 2px 8px; margin-left:10px; ">Add Family</a>';

				$output .= '<a onClick="openHistory('.$data->data[$i][0].')" class="btn btn-success btn-xs" style="display:inline;padding: 2px 7px 2px 8px; margin-left:10px; ">Package History</a>';

				$output .= '<a onClick="opensms('.$data->data[$i][0].')" class="btn btn-success btn-xs" style="display:inline;padding: 2px 7px 2px 8px; margin-left:10px; background-color:#343a40; border-color: #343a40;">Send SMS</a>';

				$output .= '<a onClick="openSmsreport('.$data->data[$i][0].')" class="btn btn-primary btn-xs" style="display:inline;padding: 2px 7px 2px 8px; margin-left:10px; ">SMS Report</a>';

				 

				

				$data->data[$i][] = (string)$output;

			}

		}

		$out->setData($data);

		return $out;

	}

	function newcaseadd(){

	    $this->layout = null;

	    $getcaseId = rand(100000,999999);

	    echo $getcaseId; die();

	}

}


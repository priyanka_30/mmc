<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Medicalcase;
use App\Models\CaseData;
use App\Models\BasicDetail;
use App\Models\HomeoDetail;
use App\Models\AddReminder;
use App\Models\CaseHeight;
use App\Models\CasePotency;
use App\Models\CommunicationDetail;
use App\Models\Doctor;
use App\Models\Package;
use App\Models\Employee;
use App\Models\Appointment;
use App\Models\Couriermedicine;
use App\Models\Pendingappointments;
use App\Models\Smsreport;
class PendingappointmentsController extends Controller
{
	public $show_action = true;
	public $view_col = 'regid';
	
	public $listing_cols = ['regid', 'last_date' , 'next_date', 'days'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Pendingappointments', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Pendingappointments', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Organizations.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    
	  $month = date('n');
	  $year = date('Y');
	  $month = date('d');
      $module = Module::get('Pendingappointments');
    //   $c_data = DB::table('case_datas')->get();
    //   foreach ($c_data as $data) {
    //   		$did[] = $data->id;
      		

    //   }
    //   $regid = implode(',', $did);
    //    // echo "<pre>"; print_r(explode(',', $regid))."<br>";
    //   $p_id = explode(',', $regid);
    //   foreach ($p_id as $pid) {
    //   	 $case = DB::table('case_potencies')
    //   	 ->select('case_potencies.*','case_datas.id as cid','case_datas.regid','case_datas.first_name','case_datas.surname','case_datas.phone','case_datas.mobile1')
    //   	 ->join('case_datas','case_datas.id','=','case_potencies.regid')
    //   	 ->where('case_potencies.regid','=',$pid)
    //   	 ->where('case_potencies.todate','>',date('Y-m-d'))
    //   	 ->latest()->first();
    //   	  echo "<pre>"; print_r($case);
    //   }
    
     	 
			$cdate = date('Y-m-d');
		  // die();	
       $case = DB::select("SELECT case_potencies.* ,case_datas.id as cid,case_datas.regid,case_datas.first_name,case_datas.surname,case_datas.phone,case_datas.mobile1 FROM case_potencies LEFT JOIN case_datas ON case_datas.id=case_potencies.regid where case_potencies.todate > '".$cdate."' order by case_potencies.id DESC");
// echo print_r($case); 
    //  $case = DB::select("SELECT bill.* ,case_datas.id as cid,case_datas.regid,case_datas.first_name,case_datas.surname,case_datas.phone,case_datas.mobile1 FROM bill LEFT JOIN case_datas ON case_datas.id=bill.regid WHERE month(str_to_date(todate,'%m/%d/%Y')) = month(curdate()) and year(str_to_date(todate,'%m/%d/%Y')) = year(curdate())");
			
		if(Module::hasAccess($module->id)) {
			return View('la.pendingappointments.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'case' =>  $case,
				'fromdate' => "",
				'todate' => "",
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
        
        
	}

	/**
	 * Show the form for creating a new organization.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function convertdate($date){
	      //return $date;
	      if($date!=""){
			$date1 = explode('/', $date);
			$newdate = $date1[2].'-'.$date1[1].'-'.$date1[0];
			return $newdate;
	      }else {
	          return '';
	      }
	}

	public function sendpendingsms(Request $request)
	{
		$casedata = DB::table('case_datas')->where('regid','=',$request->regid)->first();
		$phone = $casedata->phone;
    	if($phone==""){
    		$phone = $casedata->mobile1;
    	}

		$msg = $this->CaseCreateSms($phone); 
		echo "success"; die();
	}

	public function updatestatus(Request $request)
	{
		$data = $request->all();
	    if(isset($data['billid']) && $data['billid']!=""){ $billid =  $data['billid']; } else { $billid = '';}
	    if(isset($data['call_status']) && $data['call_status']!=""){ $call_status =  $data['call_status']; } else { $call_status = '';}
	    if(isset($data['call_date']) && $data['call_date']!=""){ $call_date =  $data['call_date']; } else { $call_date = '';}
	    
	   
		$update = DB::table('case_potencies')->where('id', $billid)->update([ 'call_status' => $call_status, 'call_date' => $call_date ]);

	
		
		return redirect()->route(config('laraadmin.adminRoute') . '.pendingappointments.index');
	}

	public function search(Request $request)
	{
		
		$module = Module::get('Pendingappointments');
		$from_date = $this->convertdate($request->get('from_date'));
		$to_date = $this->convertdate($request->get('to_date'));
		//echo $from_date; echo $to_date;
		if($from_date !=""){
			
			//$payments = DB::table('bill')->where('fromdate', '=',date('n/d/Y',strtotime($query)))->get();
			$case =  DB::table('case_potencies')
                ->select('case_potencies.*','case_datas.id as cid','case_datas.regid','case_datas.first_name','case_datas.surname','case_datas.phone','case_datas.mobile1')
                ->leftJoin('case_datas','case_datas.id','=','case_potencies.regid')
         	 	->whereBetween('case_potencies.todate', [$from_date, $to_date])
                ->get();
			//echo "<pre>"; print_r($case); die();
				
		}
		else{

			$case = DB::select("SELECT case_potencies.* ,case_datas.id as cid,case_datas.regid,case_datas.first_name,case_datas.surname,case_datas.phone,case_datas.mobile1 FROM case_potencies LEFT JOIN case_datas ON case_datas.id=case_potencies.regid where case_potencies.todate > '".$cdate."' order by case_potencies.id DESC");
		// $payments = DB::table('bill')->where('fromdate', '=',date('n/d/Y'))->get();
		}
		//echo "<pre>"; print_r($payments); die();

		
			return View('la.pendingappointments.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'case' =>  $case,
				'fromdate' => $request->get('from_date'),
				'todate' => $request->get('to_date'),
			]);
		
	}


	public function getcasedetail(Request $request){
		$medid = $request->id;
	//	echo "<pre>"; print_r($request->all());
		$medicalcase=DB::table('case_datas')->where('regid','=',$medid)->first();
		
		
	//	echo "<pre>"; print_r($casedata);die();
		$message = "Greetings ".$medicalcase->first_name." ".$medicalcase->surname." , Records show your medicines are due.Our New Timings are Mon-Sat 10:00 to 7:00 Regards Dr Nanda's Homeoclinic 0172-4659000";
		$arr = array('regid'=>$medicalcase->regid,'message'=>$message,'phone'=>$medicalcase->mobile1,);
		echo json_encode($arr); die();
	}
	
	public function sendsms(Request $request)
	{
		
		$phone = $request->phone;
		$regid = $request->smsid;
		$message = $request->message;
		$smsreport = Smsreport::create([
			 'regid' => $regid,
			 'send_date' => date('Y-m-d'),
			 'sms_type' => 'Follow Up Due',
		]);
		

		$msg = $this->CourierMedicine($phone,$message); 
		return redirect()->route(config('laraadmin.adminRoute') . '.pendingappointments.index');

	}
	
	
	/**
	 * Store a newly created organization in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Appointments", "create")) {
		
			$rules = Module::validateRules("Appointments", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Appointments", $request);
			//echo "<pre>"; print_r($insert_id); echo "</pre>"; 
			Appointment::where('id', $insert_id)->update(['patient_id' => Auth::user()->context_id]);
			return redirect()->route(config('laraadmin.adminRoute') . '.appointments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Couriermedicine", "view")) {
			
			$couriermedicine = Couriermedicine::find($id);
			
			
            //$HeightWeighty = HeightWeight::find(1);
            //echo "<pre>"; print_r($medicalcase); die();
			if(isset($couriermedicine->id)) {
				$module = Module::get('Couriermedicine');
				$module->row = $couriermedicine;
				
				return view('la.couriermedicine.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('couriermedicine', $couriermedicine);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("couriermedicine"),
				]);

			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");

		}
	}

	/**
	 * Show the form for editing the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Couriermedicine", "edit")) {
			$couriermedicine = Couriermedicine::find($id);
			if(isset($couriermedicine->id)) {
				$couriermedicine = Couriermedicine::find($id);
				
				$module = Module::get('Couriermedicine');
				
				$module->row = $couriermedicine;
				$stocks = DB::table('stocks')->get();
				$couriers = Couriermedicine::where('id','=',$id)->first();
		     	$user = CaseData::where('regid','=',$couriers->case_id)->first();
				return view('la.couriermedicine.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
					'stocks' => $stocks,
					'user' => $user,
				])->with('couriermedicine', $couriermedicine);
			} 
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified organization in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Appointments", "edit")) {
			
			$rules = Module::validateRules("Appointments", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Appointments", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.appointments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}



	/**
	 * Remove the specified organization from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Couriermedicine", "delete")) {
			Couriermedicine::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.couriermedicine.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	
	
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
	    $newlist = [ 'regid', 'fromdate' , 'todate'];
		$values = DB::table('bill')->select($newlist)->where('fromdate','=',date('n/m/d'))->whereNull('deleted_at');

		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Pendingappointments');
	
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) {
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && $fields_popup[$col]->field_type_str == "Image") {
					if($data->data[$i][$j] != 0) {
						$img = \App\Models\Upload::find($data->data[$i][$j]);
						if(isset($img->name)) {
							$data->data[$i][$j] = '<img src="'.$img->path().'?s=50">';
						} else {
							$data->data[$i][$j] = "";
						}
					} else {
						$data->data[$i][$j] = "";
					}
				}
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
			     $medicalcase = Medicalcase::where('id', '=', $data->data[$i][0])->first();
				// if($col == $this->view_col) {
				// 	$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/couriermedicine/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				// }
				
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Couriermedicine", "edit")) {
					//$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/couriermedicine/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Pendingappointments", "delete")) {
					//$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.couriermedicine.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
				 $output .= ' <button class="btn btn-successs btn-xs" type="button">Send SMS</button>';
					// $output .= Form::close();
				}
				
				
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
	function newcaseadd(){
	    $this->layout = null;
	    $getcaseId = rand(100000,999999);
	    echo $getcaseId; die();
	}
}

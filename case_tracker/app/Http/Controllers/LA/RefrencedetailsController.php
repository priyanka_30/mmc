<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Billing;
use App\Models\CaseData;

class RefrencedetailsController extends Controller
{
	public $show_action = true;
	public $view_col = 'refered_by';
	public $listing_cols = ['refered_by'];
	
	public function __construct() {
		// Field Access of Listing Columns
		// if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
		// 	$this->middleware(function ($request, $next) {
		// 		$this->listing_cols = ModuleFields::listingColumnAccessScan('CaseData', $this->listing_cols);
		// 		return $next($request);
		// 	});
		// } else {
		// 	$this->listing_cols = ModuleFields::listingColumnAccessScan('CaseData', $this->listing_cols);
		// }
	}
	
	/**
	 * Display a listing of the Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('CaseData');
		$ref = DB::table('refrencetype')->orderBy('id', 'DESC')->get();
		$count1 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '1')
			->count();

		$count2 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '2')
			->count();

		$count3 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '3')
			->count();

		$count4 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '4')
			->count();

		$count5 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '5')
			->count();
		$count6 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '6')
			->count();
		$count7 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '7')
			->count();
		$count8 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '8')
			->count();
		$count9 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '9')
			->count();
		$count10 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '10')
			->count();
		$count11 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '11')
			->count();	
		$count12 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '12')
			->count();
		$count13 = DB::table('refrencetype')
			->select('refrencetype.*','case_datas.refered_by')
			->join('case_datas', 'case_datas.refered_by', '=' ,'refrencetype.id')
			->where( 'refrencetype.id', '=' , '13')
			->count();
			
			//echo "<pre>"; print_r($ref); die();
		//if(Module::hasAccess($module->id)) {
			return View('la.refrencedetails.index', [
				
				'module' => $module,
				
				'ref' => $ref,
				'count1' => $count1,
				'count2' => $count2,
				'count3' => $count3,
				'count4' => $count4,
				'count5' => $count5,
				'count6' => $count6,
				'count7' => $count7,
				'count8' => $count8,
				'count9' => $count9,
				'count10' => $count10,
				'count11' => $count11,
				'count12' => $count12,
				'count13' => $count13,
			]);
		
	}

	/**
	 * Show the form for creating a new Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	public function addbilling(Request $request){
		
		$data = $request->all();
		if(isset($data['case_id']) && $data['case_id']!=""){ $case_id =  $data['case_id']; } else { $case_id = '';}
		if(isset($data['amount']) && $data['amount']!=""){ $amount =  $data['amount']; } else { $amount = '';}
		if(isset($data['mode']) && $data['mode']!=""){ $mode =  $data['mode']; } else { $mode = '';}
		$case = DB::table('case_datas')->where('regid','=',$case_id)->first();
		$first_name = $case->first_name;
		$surname = $case->surname;
   		
			$user = Billing::create([
					'case_id' => $case_id,
					'patient_name' => $first_name.' '.$surname,
					'received_date' =>date('Y-m-d'),
					'payment_date' =>date('Y-m-d'),
					'received_price' =>$amount,
					'payment_mode' =>$mode,
	        ]);

        return redirect()->route(config('laraadmin.adminRoute') . '.billing.index');

	}

	/**
	 * Store a newly created Daycharges in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Daycharges", "create")) {
		
			$rules = Module::validateRules("Daycharges", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Daycharges", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Daycharges", "view")) {
			
			$daycharge = Daycharge::find($id);
			if(isset($daycharge->id)) {
				$module = Module::get('Daycharges');
				$module->row = $daycharge;
				
				return view('la.daycharges.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('daycharge', $daycharge);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("daycharge"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Daycharges", "edit")) {
			
			$daycharge = Daycharge::find($id);
			if(isset($daycharge->id)) {
				
				$module = Module::get('Daycharges');
				
				$module->row = $daycharge;
				
				return view('la.daycharges.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('daycharge', $daycharge);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("daycharge"),
				]);
			}			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified Daycharges in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Daycharges", "edit")) {
			
			$rules = Module::validateRules("Daycharges", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Daycharges", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified Daycharges from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Daycharges", "delete")) {
			Daycharge::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		
	}
}

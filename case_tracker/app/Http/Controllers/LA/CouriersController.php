<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Medicalcase;
use App\Models\CaseData;
use App\Models\BasicDetail;
use App\Models\HomeoDetail;
use App\Models\AddReminder;
use App\Models\CaseHeight;
use App\Models\CasePotency;
use App\Models\CommunicationDetail;
use App\Models\Doctor;
use App\Models\Package;
use App\Models\Employee;
use App\Models\Casereminder;
use App\Models\Couriers;
use App\Models\CouriersPackage;

class CouriersController extends Controller
{
	public $show_action = true;
	public $view_col = 'package_id';
	
	public $listing_cols = ['id', 'package_id', 'total_no_package'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Couriers', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Couriers', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Organizations.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    $varId = Auth::user()->context_id;
	    $vartype = Auth::user()->type;
	    // if($vartype=='Doctor'|| $vartype=='Clinic admin'){
	    //  	return redirect(config('laraadmin.adminRoute') . '/doctors/'.$varId.'#tab-view-appointments');
	    //  }
		$module = Module::get('Couriers');
       	$stocks = DB::table('stocks')->get();
       	$medicines = DB::table('medicines')->get();
        
		if(Module::hasAccess($module->id)) {
			return View('la.couriers.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'stocks' => $stocks,
				'medicines' => $medicines,

			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
        
        
	}

	/**
	 * Show the form for creating a new organization.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function convertdate($date){
	      //return $date;
	      if($date!=""){
			$date1 = explode('/', $date);
			$newdate = $date1[2].'-'.$date1[1].'-'.$date1[0];
			return $newdate;
	      }else {
	          return '';
	      }
	}
	public function create(Request $request)
	{
		$name =  $request->name;
	    $quantity =  $request->quantity;
	    $price =  $request->price;
	    $package_id =  $request->package_id;

	    if(count($name) > count($quantity))
	        $count = count($quantity);
	    else $count = count($name);
	    
	    for($i = 0; $i < $count; $i++){
	        $data = array(
	            'name' => $name[$i],
	            'quantity' => $quantity[$i],
	            'price' => $price[$i],
	            'package_id'=> $package_id,
	        );

	        $insertData[] = $data;
	    }
	    $data=array('package_id'=>$package_id,"total_no_package"=>$count);
		Couriers::insert($data);

	    CouriersPackage::insert($insertData);
		return redirect()->route(config('laraadmin.adminRoute') . '.couriers.index');
	}
	
	

    
	/**
	 * Store a newly created organization in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Couriers", "create")) {
		
			$rules = Module::validateRules("Couriers", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Couriers", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.couriers.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Couriers", "view")) {
			
			$casereminder = Couriers::find($id);
			
			
            //$HeightWeighty = HeightWeight::find(1);
            //echo "<pre>"; print_r($medicalcase); die();
			if(isset($casereminder->id)) {
				$module = Module::get('Couriers');
				$module->row = $casereminder;
				
				return view('la.couriers.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('casereminder', $casereminder);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("couriers"),
				]);

			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");

		}
	}

	/**
	 * Show the form for editing the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Couriers", "edit")) {
			$casereminder = Couriers::find($id);
			if(isset($casereminder->id)) {
				$casereminder = Couriers::find($id);
				$couriers = CouriersPackage::where('package_id','=',$casereminder->package_id)->get();
				$stocks = DB::table('stocks')->get();
				$module = Module::get('Couriers');
				
				$module->row = $casereminder;
				
				return view('la.couriers.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
					'couriers' => $couriers,
					'stocks' => $stocks,
				])->with('casereminder', $casereminder);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("couriers"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified organization in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Couriers", "edit")) {
			
			$rules = Module::validateRules("Couriers", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Couriers", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.couriers.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	public function couriersupdate(Request $request)
	{	
		
		DB::table('courier_package_list')->where('package_id', $request->package_id)->delete();
		$name =  $request->name;
	    $quantity =  $request->quantity;
	    $price =  $request->price;
	    $package_id =  $request->package_id;

	    

	    if(count($name) > count($quantity))
	        $count = count($quantity);
	    else $count = count($name);
	    
	    for($i = 0; $i < $count; $i++){
	        $data = array(
	            'name' => $name[$i],
	            'quantity' => $quantity[$i],
	            'price' => $price[$i],
	            'package_id'=> $package_id,
	        );

	        $insertData[] = $data;
	    }
	 
	    CouriersPackage::insert($insertData);
		return redirect()->route(config('laraadmin.adminRoute') . '.couriers.index');
	}

	/**
	 * Remove the specified organization from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Couriers", "delete")) {
			Couriers::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.couriers.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
	    $newlist = ['id', 'package_id', 'total_no_package'];
		$values = DB::table('couriers')->select($newlist)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Couriers');
	//	echo "<pre>"; print_r($data);die();
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) {
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && $fields_popup[$col]->field_type_str == "Image") {
					if($data->data[$i][$j] != 0) {
						$img = \App\Models\Upload::find($data->data[$i][$j]);
						if(isset($img->name)) {
							$data->data[$i][$j] = '<img src="'.$img->path().'?s=50">';
						} else {
							$data->data[$i][$j] = "";
						}
					} else {
						$data->data[$i][$j] = "";
					}
				}
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
			     $medicalcase = Medicalcase::where('id', '=', $data->data[$i][0])->first();
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/couriers/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Couriers", "edit")) {
					//$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/casereminder/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Couriers", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.couriers.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
	function newcaseadd(){
	    $this->layout = null;
	    $getcaseId = rand(100000,999999);
	    echo $getcaseId; die();
	}
}

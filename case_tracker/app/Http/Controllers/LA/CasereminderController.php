<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Medicalcase;
use App\Models\CaseData;
use App\Models\BasicDetail;
use App\Models\HomeoDetail;
use App\Models\AddReminder;
use App\Models\CaseHeight;
use App\Models\CasePotency;
use App\Models\CommunicationDetail;
use App\Models\Doctor;
use App\Models\Package;
use App\Models\Employee;
use App\Models\Appointment;
use App\Models\Casereminder;
class CasereminderController extends Controller
{
	public $show_action = true;
	public $view_col = 'patient_name';
	
	public $listing_cols = ['id', 'patient_name', 'start_date', 'heading', 'comments'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Casereminder', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Casereminder', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Organizations.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    $varId = Auth::user()->context_id;
	    $vartype = Auth::user()->type;
	    // if($vartype=='Doctor'|| $vartype=='Clinic admin'){
	    //  	return redirect(config('laraadmin.adminRoute') . '/doctors/'.$varId.'#tab-view-appointments');
	    //  }
		$module = Module::get('Casereminder');
       // $casedata = DB::table('case_datas')->where('assitant_doctor','=', Auth::user()->name)->get();

        /*$casedata = DB::table('case_datas')
		       ->join('appointments', 'appointments.patient_id', '=', 'case_datas.patientid')
        	   ->where('appointments.status','=', 'Accepted')
		       ->get();*/
        $casedata = DB::table('case_datas')->get();
		if(Module::hasAccess($module->id)) {
			return View('la.casereminder.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'casedata'=>$casedata,

			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
        
        
	}

	/**
	 * Show the form for creating a new organization.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function convertdate($date){
	      //return $date;
	      if($date!=""){
			$date1 = explode('/', $date);
			$newdate = $date1[2].'-'.$date1[1].'-'.$date1[0];
			return $newdate;
	      }else {
	          return '';
	      }
	}

	
	

    
	/**
	 * Store a newly created organization in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Casereminder", "create")) {
		
			$rules = Module::validateRules("Casereminder", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Casereminder", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.casereminder.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Casereminder", "view")) {
			
			$casereminder = Casereminder::find($id);
			
			
            //$HeightWeighty = HeightWeight::find(1);
            //echo "<pre>"; print_r($medicalcase); die();
			if(isset($casereminder->id)) {
				$module = Module::get('Casereminder');
				$module->row = $casereminder;
				
				return view('la.casereminder.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('casereminder', $casereminder);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("casereminder"),
				]);

			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");

		}
	}

	/**
	 * Show the form for editing the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Casereminder", "edit")) {
			$casereminder = Casereminder::find($id);
			if(isset($casereminder->id)) {
				$casereminder = Casereminder::find($id);
				
				$module = Module::get('Casereminder');
				
				$module->row = $casereminder;
				
				return view('la.casereminder.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('casereminder', $casereminder);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("casereminder"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified organization in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		echo "<pre>"; print_r($request->all());
		if(Module::hasAccess("Casereminder", "edit")) {
			
			$rules = Module::validateRules("Casereminder", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Casereminder", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.casereminder.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified organization from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Casereminder", "delete")) {
			Casereminder::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.casereminder.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	public function dailyreminder(Request $request)
	{
		$data = DB::table('case_reminder')->where('start_date','=',date('Y-m-d'))->get();
		$iddata = json_decode(json_encode($data), true);
		
		foreach ($iddata as $key=> $values) {
			$caseid[] = $values['patient_name'];	
			
		}
		$cid= implode(",", $caseid);
     	$ccid = explode(",", $cid);

     	foreach($ccid as $ids){
     		
            $caseData = DB::table('case_datas')->where('regid','=',$ids)->first();
            $mydata = json_decode(json_encode($caseData), true);
          
           	$send= $this->AppointmentReminder($mydata['phone'],$mydata['first_name']);
           	echo "success<br>"; 
         }
         

	}


	
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
	    $newlist = ['id', 'patient_name', 'start_date', 'heading', 'comments'];
		$values = DB::table('case_reminder')->select($newlist)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Casereminder');
	//	echo "<pre>"; print_r($data);die();
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) {
				$col = $this->listing_cols[$j];
				if($col == "start_date") {
				   $data->data[$i][$j] = date('d M Y',strtotime($data->data[$i][$j]));
				}
				elseif($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
			     $medicalcase = Medicalcase::where('id', '=', $data->data[$i][0])->first();
				
				
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Casereminder", "edit")) {
					//$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/casereminder/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Casereminder", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.casereminder.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
	function newcaseadd(){
	    $this->layout = null;
	    $getcaseId = rand(100000,999999);
	    echo $getcaseId; die();
	}
}

<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\User;
use App\Models\Employee;
use App\Models\Doctor;
use App\Models\Account;
use App\Models\Clinicadmin;
use App\Models\Dispensary;
use App\Models\Receptionist;

class UsersController extends Controller
{
	public $show_action = false;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name', 'email', 'type'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Users', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Users', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Users.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Users');
		
		if(Module::hasAccess($module->id)) {
			return View('la.users.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Display the specified user.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//if(Module::hasAccess("Users", "view")) {
		$user = User::findOrFail($id);
		//echo "<pre>"; print_r($user); die();
		if(isset($user->id) && $user->id == Auth::user()->id) { //echo $user['type']; die();
			if($user['type'] == "Employee") {
				$Employee = Employee::findOrFail($user->context_id);
				if(isset($Employee->id)){
					return redirect(config('laraadmin.adminRoute') . '/employees/'.$Employee->id);
				}else{
					return view('errors.404', [
						'record_id' => $id,
						'record_name' => ucfirst("user"),
					]);
				}
				
			} else if($user['type'] == "Doctor") {
				$Doctor = Doctor::findOrFail($user->context_id);
				if(isset($Doctor->id)){
					return redirect(config('laraadmin.adminRoute') . '/doctors/'.$Doctor->id);
				}else{
					return view('errors.404', [
						'record_id' => $id,
						'record_name' => ucfirst("user"),
					]);
				}
			}else if($user['type'] == "Clinicadmin") {
				$Clinicadmin = Clinicadmin::findOrFail($user->context_id);
				if(isset($Clinicadmin->id)){
					return redirect(config('laraadmin.adminRoute') . '/clinicadmins/'.$Clinicadmin->id);
				}else{
					return view('errors.404', [
						'record_id' => $id,
						'record_name' => ucfirst("user"),
					]);
				}
			} else if($user['type'] == "Receptionist") {
				$Receptionist = Receptionist::findOrFail($user->context_id);
				if(isset($Receptionist->id)){
					return redirect(config('laraadmin.adminRoute') . '/receptionists/'.$Receptionist->id);
				}else{
					return view('errors.404', [
						'record_id' => $id,
						'record_name' => ucfirst("user"),
					]);
				}
			}else if($user['type'] == "Account") {
				$Account = Account::findOrFail($user->context_id);
				if(isset($Account->id)){
					return redirect(config('laraadmin.adminRoute') . '/accounts/'.$Account->id);
				}else{
					return view('errors.404', [
						'record_id' => $id,
						'record_name' => ucfirst("user"),
					]);
				}
			}else if($user['type'] == "Dispensary") {
				$Dispensary = Dispensary::findOrFail($user->context_id);
				if(isset($Dispensary->id)){
					return redirect(config('laraadmin.adminRoute') . '/dispensaries/'.$Dispensary->id);
				}else{
					return view('errors.404', [
						'record_id' => $id,
						'record_name' => ucfirst("user"),
					]);
				}
			}else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("user"),
				]);
			}
		} else {
			return view('errors.404', [
				'record_id' => $id,
				'record_name' => ucfirst("user"),
			]);
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('users')->select($this->listing_cols)->where('type','=','Doctor')->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Users');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/users/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Users", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/users/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Users", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.users.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}

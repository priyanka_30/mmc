<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Dailycollection;
use App\Models\CaseHeight;
use App\Models\Packagehistory;
use App\Models\Medicalcase;
use App\Models\Familygroup;
use App\Models\Billing;
use App\Models\Receipt;

class DailycollectionController extends Controller
{
	public $show_action = true;
	public $view_col = 'id';
	public $listing_cols = ['id'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Dailycollection', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Dailycollection', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Dailycollection');
		$sms = DB::table('smstemplate')->get();
		 $tokendata=  DB::table('token')
			->select('token.regid','token.id','token.dateval','token.created_at','case_datas.id as caseid','case_datas.regid as rid','case_datas.first_name','case_datas.assitant_doctor','doctors.id as did', 'doctors.name as dname' ,'bill.regid', DB::raw('sum(bill.balance) as sum' ))
			->leftJoin('case_datas','case_datas.regid','=','token.regid')
			->leftJoin('doctors','doctors.id','=','case_datas.assitant_doctor')
			->leftJoin('bill','bill.regid','=','case_datas.id')
		//	->join('additional_charges','additional_charges.regid','=','case_datas.id')
			->where('token.deleted_at','=',NULL)
			->where('token.dateval','=',date('d/m/Y'))
		//	->orWhere('additional_charges.dateval','=',date('n/d/Y'))
			->groupBy('bill.regid')
			->orderBy('token.id','desc')
			
			
		//	->orderBy('bill.id','desc')
			->get();
			//echo "<pre>"; print_r($tokendata); die();
		$packages = DB::table('packages')->where('deleted_at','=',NULL)->orderBy('id','ASC')->get();
	
		if(Module::hasAccess($module->id)) {
			return View('la.dailycollection.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'tokendata' => $tokendata,
				'packages' => $packages,
				'sms' => $sms,
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}



	public function searchtoken(Request $request)
	{		
		$search_name = $request->get('search_name');
		$search_surname = $request->get('search_surname');
		$search_regid = $request->get('search_regid');
		$output="";

		if($search_name!=""){
			$data = DB::table('case_datas')
			->where('first_name', 'LIKE', "%{$search_name}%")
			->where('deleted_at', '=', NULL)
			->get();
		}

		if($search_surname!=""){
			$data = DB::table('case_datas')
			->where('phone', '=', $search_surname)
			->orWhere('mobile1',  '=', $search_surname)
			->where('deleted_at', '=', NULL)
			->get();
		}
		if($search_regid!=""){
			$data = DB::table('case_datas')
			->where('regid', '=', $search_regid)
			->where('deleted_at', '=', NULL)
			->get();
		}
		$check = Token::latest()->first();
		if($check!=""){
			$lastid = $check->id;
		}
		else{
			$lastid = "0";
		}
		
	      foreach($data as $row)
	      {
	       	$output = "<tr id='".$row->regid."' class='".$row->regid."'>";
			$output .= "<td onclick=focusfunction(".$row->regid.",'$row->first_name','$row->assitant_doctor',".$lastid.");>".$row->regid."</td>";
			$output .= "<td onclick=focusfunction(".$row->regid.",'$row->first_name','$row->assitant_doctor',".$lastid.");>".$row->first_name."</td>";
			$output .= "</tr>";
		
			echo $output;
	      }
	     
	    
	}

	public function searchtokenid(Request $request)
	{
		$output="";
		

		$products=DB::table('case_datas')->where('regid','=',$request->search)->get();
		$check = Token::latest()->first();
		$lastid = $check->id;
		
		foreach ($products as $product) {
		$output= $product->regid."/";
		$output.= $product->first_name."/";
		$output.= $product->assitant_doctor."/";
		$output.= $lastid;
		}
		return Response($output);
	}

	public function savetoken(Request $request){
		$this->layout = null;
		$data = $request->all();
		if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}
   		$check = DB::table('token')->where([ ['regid','=',$regid], ['dateval','=',date('d/m/Y')]])->first();
   		if($check==""){
			$user = Token::create([
					'regid' => $regid,
					'dateval' =>date('d/m/Y'),
	        ]);

        	echo "1"; die();
    	}
    	else{
    		echo "0"; die();
    	}

	}

	public function deletetoken(Request $request){
	     $this->layout = null;
		 $data = $request->all();
		 //echo $data['rand'];
	     Token::where('id', '=',$data['id'])->delete();
	    
	     echo "success"; die();
	}

	public function getheightweight(Request $request){
		$caseid = $request->id;

		$casedata=DB::table('case_datas')->where('regid','=',$caseid)->first();
		$HeightWeight = DB::table('case_heights')->where([ ['regid', '=', $caseid], ['deleted_at','=',NULL], ['dob','=',date('d/m/Y')] ])->orderBy('dob', 'DESC')->first();
		$heightweight = DB::table('heightweight')->get();

		$ht= "";
		$wt= "";
		$date1 = $casedata->date_of_birth;
		$date2 = date("Y-m-d");

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);

		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);

		$diff1 = (($year2 - $year1) * 12) + ($month2 - $month1);
		//echo $diff1;
        if($casedata->gender!=""){
		$string = $casedata->gender;

		//Get the first character.
		$firstCharacter = $string[0];
        }else{
            $firstCharacter = '';
        }
        foreach($heightweight as $heightweightval) {
        	if($heightweightval->months == $diff1 && $heightweightval->gender == $firstCharacter)
			{
				$ht= $heightweightval->height;
				$wt= $heightweightval->weight;
			}
            elseif($heightweightval->gender == $firstCharacter && $diff1 > 240){
                $ht= $heightweightval->height;
                $wt= $heightweightval->weight;

            }
        }
		$gender = $casedata->gender;
		$htdata= DB::table('case_heights')->where([ ['regid', '=', $caseid], ['deleted_at','=',NULL] ])->get();
		// echo "<pre>"; print_r($products); die();
		$tddata[] = "";
		foreach ($htdata as $key => $product) {
			
			$output = "<tr>";
			$output .= "<td>".$product->dob."</td>";
			$output .= "<td>".$product->height."</td>";
			$output .= "<td>".$product->weight."</td>";
			$output .= "<td>".$product->lmp."</td>";
			$output .= "</tr>";
		
			$tddata[] = $output;
	
		}
		if ($HeightWeight!=""){
			$height = $HeightWeight->height;
			$weight = $HeightWeight->weight;
			$lmp = $HeightWeight->lmp;
			$regid = $casedata->regid;
			$dateval = $HeightWeight->dob;
			$rand_id = $HeightWeight->rand_id;
		
			$arr = array("height" => $height,'weight'=>$weight,'lmp'=>$lmp ,'gender'=>$gender,'dateval'=>$dateval,'rand_id'=>$rand_id,'ht' =>$ht,'wt' =>$wt,'tddata' =>$tddata,'datevalht'=>$dateval);
		}
		else{
			$arr = array("height" => "",'weight'=>"",'lmp'=>"" ,'gender'=>$gender,'dateval'=>date('d/m/Y'),'rand_id'=>rand('99',9999),'ht' =>$ht,'wt' =>$wt,'tddata' =>$tddata,'datevalht'=>"");
		}

		

		echo json_encode($arr); die();
	}

	public function saveCaseHeightWeights(Request $request){
	  	$this->layout = null;
		$data = $request->all();
	
	    if(isset($data['height']) && $data['height']!=""){ $height =  $data['height']; } else { $height = '';}
    	if(isset($data['weight']) && $data['weight']!=""){ $weight =  $data['weight']; } else { $weight = '';}
    	if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}
    	
    	if(isset($data['lmpdata']) && $data['lmpdata']!=""){ $lmp =  $data['lmpdata']; } else { $lmp = '';}
    	if(isset($data['rand']) && $data['rand']!=""){ $rand=  $data['rand']; } else { $rand = '';}
    	
    	$check = DB::table('case_heights')->where([ ['dob', '=', $data['dateval']], ['deleted_at','=',NULL],['regid','=',$regid] ])->latest()->first();

    	if($check==""){
			$user = CaseHeight::create([
	             'regid' => $regid,
				 'dob' => $data['dateval'],
				 'height' => $height,
				 'weight' => $weight,
				 'rand_id' =>$rand,
				 'lmp' => $lmp,
			]);
			echo "1"; die();
		}
		else{
			$heightid = $check->id;
			$heightdata = CaseHeight::where('id', $heightid)->first();

			$heightdata->height = $height;
			$heightdata->weight = $weight;
			$heightdata->lmp = $lmp;
			$heightdata->save();
		}
		echo "success"; die();
       //return redirect(config('laraadmin.adminRoute')."/medicalcases");
	}

	public function packagehistory(Request $request){
		$caseid = $request->id;
		$dataid=DB::table('case_datas')->where([ ['regid','=',$caseid], ['deleted_at','=',NULL]])->first();
		$case_id = $dataid->id;
		$casedata=DB::table('packagehistory')
					->select('packagehistory.*','packages.id as pid','packages.name','packages.tags')
					->join('packages','packages.id','=','packagehistory.packageid')
					->where([ ['packagehistory.regid','=',$case_id], ['packagehistory.deleted_at','=',NULL]])->orderBy('id','DESC')->get();
		
		
			
		foreach ($casedata as $key => $product) {
			
			$output = "<tr>";
			$output .= "<td>".date('d M Y',strtotime($product->packagedate))."</td>";
			$output .= "<td>".$product->name."</td>";
			$output .= "<td>".$product->tags."</td>";
			$output .= "<td>".date('d M Y',strtotime($product->fromdate))."</td>";
			$output .= "<td>".date('d M Y',strtotime($product->todate))."</td>";
			$output .= "<td><a href='javascript:void(0)' onclick='deletepackage($product->id);'>Remove</a></td>";
			$output .= "</tr>";
			echo $output;
		
			
	
		}
		
	}

	public function deletepackage(Request $request){
	     $this->layout = null;
		 $data = $request->all();
		// echo $data; die();
		 $packhis = DB::table('packagehistory') ->where('id', $data['id'])->first();
 	 	$packid = DB::table('packagehistory') ->where('regid', $packhis->regid)->orderBy('id', 'desc')->skip(1)->take(1)->first();
 	 	$caseid = DB::table('case_datas') ->where('id', $packhis->regid)->first();

 	 	$updatemed = DB::table('medicalcases') ->where('regid', $caseid->regid)->update( [ 'package' => $packid->packageid , 'date_left' => $packid->todate ]);

	     $pack = DB::table('packagehistory') ->where('id', $data['id'])->update( [ 'deleted_at' => date('Y-m-d h:i:s') ]);
	    
	    // echo "success"; die();
        //return redirect(config('laraadmin.adminRoute')."/medicalcases");
	}

	public function savepackages(Request $request)
	{
		// echo "<pre>";print_r($request->all());die();
		// echo $request->package_start_date; die();
		$case = DB::table('case_datas')->where('regid', $request->case_id)->first();
		$caseid = $case->id;
		$employees = Medicalcase::where('regid', $request->case_id)->first();
		$employees->package = $request->packages;
		
		if($request->package_start_date!=""){
			$package_start = $this->convertdate($request->package_start_date);
		}
		else{
			$package_start = $request->package_start;
		}

		$package = DB::table('packages')->where('id','=',$request->packages)->first();
		//echo "<pre>"; print_r($package);
		$packagemonth = $package->expiry_date;
		//echo $packagemonth;
		$created_at = date('d/m/Y');
		$expiry= date('d/m/Y',strtotime('+'.$packagemonth.' days',strtotime(str_replace('/', '-', $created_at))));
		$expirydate = date('m/d/Y',strtotime($expiry));

		$employees->package_start = $package_start;
		$employees->date_left = $expirydate;
		$employees->save();
		$bill = Billing::latest()->first();
		$billno = $bill->BillNo + 1;
		$packagesave = Packagehistory::create(['regid' => $caseid, 'packageid'=>$request->packages, 'todate'=>$expirydate,'fromdate'=>date('m/d/Y'), 
				'packagedate'=>date('m/d/Y')]);
		$billing = Billing::create(['regid' => $caseid, 'charges'=>$package->color,'Balance'=>$package->color, 'todate'=>$expirydate,'fromdate'=>date('n/d/Y'),'BillDate'=>date('m/d/Y'), 'to_date'=>date('Y-m-d',strtotime($expirydate)), 'BillNo' => $billno]);
		return redirect()->route(config('laraadmin.adminRoute') . '.dailycollection.index');
	}

	public function packagemessage(Request $request)
	{
	
		$send= $this->AppointmentReminder($request->regmobile,$request->regcasename);
		echo "success"; die();
	}

	public function familydata(Request $request){
		$caseid = $request->id;

		$casedata=DB::table('familygroup')->where([ ['regid','=',$caseid], ['deleted_at','=',NULL]])->get();
		
		if(!empty($casedata)){
		foreach ($casedata as $key => $product) {
			if(!empty($product->family_regid)){
			$casename=DB::table('case_datas')->where('regid','=',$product->family_regid)->first();

			$output = "<tr id='".$product->family_regid."' class='".$product->family_regid."'>";
			$output .= "<td onclick='removefunctionfamily(".$product->family_regid.");'>".$product->family_regid."</td>";
			$output .= "<td onclick='removefunctionfamily(".$product->family_regid.");'>".$casename->first_name." ".$casename->surname."</td>";
			$output .= "</tr>";
		
			echo $output;
		}
	
		}
	}
		
	}

	public function deletefamily(Request $request){
	     $this->layout = null;
		 $data = $request->all();
		 //echo $data['rand'];
	     Familygroup::where('family_regid', '=',$data['id'])->delete();
	    
	     echo "success"; die();
	}

	public function searchfamily(Request $request)
	{		
		$search_fname = $request->get('search_fname');
		$search_fsurname = $request->get('search_fsurname');
		$search_fregid = $request->get('search_fregid');
		$output="";

		if($search_fname!=""){
			$data = DB::table('familygroup')
			->where('name', 'LIKE', "%{$search_fname}%")
			->get();
		}

		if($search_fsurname!=""){
			$data = DB::table('familygroup')
			->where('surname', 'LIKE', "%{$search_fsurname}%")
			->get();
		}
		if($search_fregid!=""){
			$data = DB::table('familygroup')
			->where('family_regid', '=', $search_regid)
			->get();
		}
		
		
	      foreach($data as $product)
	      {
	       	$output = "<tr id='".$product->family_regid."' class='".$product->family_regid."'>";
			$output .= "<td onclick='removefunction(".$product->family_regid.");'>".$product->family_regid."</td>";
			$output .= "<td onclick='removefunction(".$product->family_regid.");'>".$product->name." ".$product->surname."</td>";
			$output .= "</tr>";
		
			echo $output;
	      }
	     
	    
	}

	public function savefamily(Request $request){
		$this->layout = null;
		$data = $request->all();
		if(isset($data['f_regid']) && $data['f_regid']!=""){ $f_regid =  $data['f_regid']; } else { $f_regid = '';}
		if(isset($data['f_reg_id']) && $data['f_reg_id']!=""){ $f_reg_id =  $data['f_reg_id']; } else { $f_reg_id = '';}
		if(isset($data['f_name']) && $data['f_name']!=""){ $f_name =  $data['f_name']; } else { $f_name = '';}
		if(isset($data['f_surname']) && $data['f_surname']!=""){ $f_surname =  $data['f_surname']; } else { $f_surname = '';}
   		
    	$check = DB::table('familygroup')->where([ ['regid','=',$f_reg_id], ['family_regid','=',$f_regid], ['deleted_at','=',NULL]])->first();
    	if($check==""){
			$user = Familygroup::create([
					'regid' => $f_reg_id,
					'family_regid' =>$f_regid,
					'name' =>$f_name,
					'surname' =>$f_surname,
	        ]);

	        $user1 = Familygroup::create([
					'regid' => $f_regid,
					'family_regid' =>$f_reg_id,
					'name' =>$f_name,
					'surname' =>$f_surname,
	        ]);

	        echo "1"; die();
	    }
	    else{
	    	echo "0"; die();
	    }


       // echo "success"; die();

	}

	public function searchcases(Request $request)
	{		
		$search_name = $request->get('search_name');
		$search_surname = $request->get('search_surname');
		$search_regid = $request->get('search_regid');
		$f_reg_id = $request->get('f_reg_id');
		$output="";

		if($search_name!=""){
			$data = DB::table('case_datas')
			->where('first_name', 'LIKE', "%{$search_name}%")
			->where('regid', '!=', $f_reg_id)
			->where('deleted_at', '=', NULL)
			->get();
		}

		if($search_surname!=""){
			$data = DB::table('case_datas')
			->where('surname', 'LIKE', "%{$search_surname}%")
			->where('regid', '!=', $f_reg_id)
			->where('deleted_at', '=', NULL)
			->get();
		}
		if($search_regid!=""){
			$data = DB::table('case_datas')
			->where('regid', '=', $search_regid)
			->where('regid', '!=', $f_reg_id)
			->where('deleted_at', '=', NULL)
			->get();
		}
		
		
	      foreach($data as $row)
	      {
	       	$output = "<tr  class='".$row->regid."'>";
			$output .= "<td onclick=focusfunctionfamily(".$row->regid.",'$row->first_name','$row->surname');>".$row->regid."</td>";
			$output .= "<td onclick=focusfunctionfamily(".$row->regid.",'$row->first_name','$row->surname');>".$row->first_name." ".$row->surname."</td>";
			$output .= "</tr>";
		
			echo $output;
	      }
	     
	    
	}

	public function getamountdetail(Request $request){
		$caseid = $request->id;

		$casedata1=DB::table('bill')->where([ ['regid','=',$caseid], ['fromdate','=',date('n/d/Y')] ])->get();
		$additional=DB::table('additional_charges')->where([ ['regid','=',$caseid], ['dateval','=',date('n/d/Y')] ])->get();
		if(empty($casedata1)){
			$casedata=DB::table('bill')->where([ ['regid','=',$caseid]])->orderBy('id','desc')->latest()->get();
		}
		else{
			$casedata=DB::table('bill')->where([ ['regid','=',$caseid], ['fromdate','=',date('n/d/Y')] ])->get();
		}
		
		$additionalchg=DB::table('additional_charges')->where([ ['regid','=',$caseid], ['dateval','=',date('n/d/Y')] ])->orderBy('id','desc')->latest()->first();
		$chg = 0;
		if(!empty($additionalchg)){
			
			 $chg = $additionalchg->additional_price;
		}
		$total = 0;
		
	//	echo "<pre>";print_r($casedata); die();
		foreach ($casedata as $key => $product) {
			//$bal = $product->Balance;
		$total = $chg + $product->Balance;
			$output = "<tr>";
			$output .= "<td style='text-align:center;'>".date('d M Y',strtotime($product->fromdate))."</td>";
			$output .= "<td style='text-align:center;'>".$product->charges."</td>";
			$output .= "<td style='text-align:center;'>".$product->received."</td>";
			$output .= "<td style='text-align:center;'>".$total."</td>";
			$output .= "</tr>";
			$output .= "<input type='hidden' name='billid' class='billid' value='".$product->id."'>";
		
			echo $output;
		
	
		}
	
		
	}

	public function updatereceived(Request $request)
	{

		$caseid = DB::table('case_datas')->where('regid', $request->regid)->first();
		//echo $caseid->id;
		$bill = DB::table('bill')->where([ ['regid','=',$caseid->id], ['fromdate','=',date('n/j/Y')] ])->first();
	//	echo "<pre>"; print_r($bill); die();
		$charges = $bill->charges;
		$totalchg = $charges - $request->received_price;
		$update = DB::table('bill')->where([ ['regid','=',$caseid->id], ['fromdate','=',date('n/j/Y')] ])->update([ 'received' => $request->received_price, 'balance' => $totalchg, 'payment_mode' => $request->mode ]);

		$create = Receipt::create([
					'regid' => $caseid->id,
					'amount' =>$request->received_price,
					'mode' =>$request->mode,
					'receiptdate' =>date('n/j/Y'),
	        ]);
		
		return redirect()->route(config('laraadmin.adminRoute') . '.dailycollection.index');
	}

	public function billconvertdate($date){
	      //return $date;
	      if($date!=""){
			$date1 = explode('/', $date);
			$newdate = $date1[1].'/'.$date1[0].'/'.$date1[2];
			return $newdate;
	      }else {
	          return '';
	      }
	}

	public function getbilldetails(Request $request)
	{
		$output="";
		$billdate = $this->billconvertdate($request->billdate);
		$dtbill = date('n/d/Y',strtotime($billdate));
		$bill_case_id = $request->bill_case_id;

		$caseid=DB::table('case_datas')->where('regid','=',$request->bill_case_id)->first();
		$products=DB::table('bill')->where([ ['regid','=',$caseid->id], ['BillDate','=', $dtbill] ])->get();
		
		foreach ($products as $key => $product) {
		$output= $caseid->first_name." ". $caseid->surname ."/";
		$output.= $product->charges."/";
		$output.= $product->regid."/";
		$output.= $product->BillNo;
		}
		return Response($output);
	}

	public function getsmsdetail(Request $request){
		$caseid = $request->id;

		$casedata=DB::table('smsreport')->where([ ['regid','=',$caseid] ])->get();
		
		
	//	echo "<pre>";print_r($additionalchg->additional_price); die();
		foreach ($casedata as $key => $product) {
			//$bal = $product->Balance;
		
			$output = "<tr>";
			$output .= "<td style='text-align:center;'>".date('d M Y',strtotime($product->send_date))."</td>";
			$output .= "<td style='text-align:center;'>".$product->sms_type."</td>";
		
			echo $output;
	}
}

	public function generatePDF(Request $request)
    {
    	$data = $request->all();
    	if($request->billtemplate == "simplebill"){
    		$my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('bill.docx'));
    	}
    	else if($request->billtemplate == "packagebill"){
    		$my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('user_template.docx'));
    	}
    	else if($request->billtemplate == "registrationbill"){
    		$my_template = new \PhpOffice\PhpWord\TemplateProcessor(public_path('registration.docx'));
    	}
    	
    	$casesid=DB::table('case_datas')->where('id','=',$request->case_u_id)->first();
		$cases=DB::table('medicalcases')->where('regid','=',$casesid->regid)->first();
		$package = $cases->package;
		if($package!="0") {
   		   $package_date = $cases->date_left;
   		}
   		else{
   			$package_date = "N/A";
   		}
   		
		// $medget=  DB::table('payments')
		// ->select('payments.case_id','payments.received_date','case_potencies.regid','case_potencies.received_date','case_potencies.rxdays','case_potencies.dateval')
		// ->join('case_potencies','case_potencies.received_date','=','payments.received_date')
		// //->join('case_potencies','case_potencies.regid','=','payments.case_id')
		// ->where('payments.case_id','=',$request->case_u_id)
		// ->first();
		
		// $dateval= $this->convertdate($medget->dateval);

		// $days= $medget->rxdays;
		
		// $medicine_to= date('Y-m-d', strtotime($dateval. ' +'. $days));
		// $total_days = 'from '.$dateval.' to '.$medicine_to;
		$dayget = DB::table('bill')->where('BillNo','=',$request->billno)->first();
   		$now = strtotime($dayget->todate); // or your date as well
		$your_date = strtotime($dayget->fromdate);
		$datediff = $now - $your_date;
		$days =  round($datediff / (60 * 60 * 24));

		
    	$my_template->setValue('patient_name',$request->case_name);
		$my_template->setValue('received',$request->rece_amount);
		$my_template->setValue('billno', $request->billno);
		$my_template->setValue('date',$request->bill_date); 
		$my_template->setValue('package',$package_date); 
		$my_template->setValue('fromdate',$dayget->fromdate); 
		$my_template->setValue('todate',$dayget->todate); 
		$my_template->setValue('days',$days);        

		    try{
		        $my_template->saveAs(storage_path('bill.docx'));
		    }catch (Exception $e){
		        //handle exception
		    }

		    return response()->download(storage_path('bill.docx'));
    }

    public function convertdate($date){

	      //return $date;

	      if($date!=""){

			$date1 = explode('/', $date);

			$newdate = $date1[2].'-'.$date1[1].'-'.$date1[0];

			return $newdate;

	      }else {

	          return '';

	      }

	}

    public function packagesave(Request $request)
	{
		//echo "<pre>"; print_r($request->all()); die();
		$case = DB::table('case_datas')->where('regid', $request->case_id)->first();
		$caseid = $case->id;
		$employees = Medicalcase::where('regid', $request->case_id)->first();
		$employees->package = $request->packages;
		
		if($request->package_start_date!=""){
			$package_start = $this->billconvertdate($request->package_start_date);
		}
		else{
			$package_start = $this->billconvertdate($request->package_start);
		}
		//echo $package_start;die();

		$package = DB::table('packages')->where('id','=',$request->packages)->first();
		//echo "<pre>"; print_r($package);
		$packagemonth = $package->expiry_date;
		//echo $packagemonth;
		$created_at = date('d/m/Y',strtotime($package_start));
	//	echo $created_at; die();
		$expirydate= date('n/d/Y',strtotime('+'.$packagemonth.' days',strtotime(str_replace('/', '-', $created_at))));
		//$expirydate = date('m/d/Y',strtotime($expiry));
		$employees->package_start = $package_start;
		$employees->date_left = $expirydate;
		$employees->save();

// echo $expirydate."<br>";
// echo $package_start; die();
		$bill = Billing::latest()->first();
		$billno = $bill->BillNo + 1;
		$packagesave = Packagehistory::create(['regid' => $caseid, 'packageid'=>$request->packages, 'todate'=>$expirydate,'fromdate'=>$package_start, 
				'packagedate'=>date('m/d/Y')]);
		$billing = Billing::create(['regid' => $caseid, 'charges'=>$package->color,'Balance'=>$package->color, 'todate'=>$expirydate,'fromdate'=>date('n/d/Y'),'BillDate'=>date('m/d/Y'), 'to_date'=>date('Y-m-d',strtotime($expirydate)), 'BillNo' => $billno]);
		return redirect()->route(config('laraadmin.adminRoute') . '.dailycollection.index');
	}


	public function printstickers(Request $request)
	{
		
		$regid = $request->sticker_regid;
		$name = $request->sticker_name;
		$nostickers = $request->nostickers;

		return redirect("http://smartops.co.in/managemyclinic/admin/printsticker/".$regid.'/'.$name.'/'.$nostickers);
			
		 
	}

	/**
	 * Store a newly created Daycharges in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Daycharges", "create")) {
		
			$rules = Module::validateRules("Daycharges", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Daycharges", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}



	/**
	 * Display the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Daycharges", "view")) {
			
			$daycharge = Daycharge::find($id);
			if(isset($daycharge->id)) {
				$module = Module::get('Daycharges');
				$module->row = $daycharge;
				
				return view('la.daycharges.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('daycharge', $daycharge);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("daycharge"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Daycharges", "edit")) {
			
			$daycharge = Daycharge::find($id);
			if(isset($daycharge->id)) {
				
				$module = Module::get('Daycharges');
				
				$module->row = $daycharge;
				
				return view('la.daycharges.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('daycharge', $daycharge);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("daycharge"),
				]);
			}			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified Daycharges in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Daycharges", "edit")) {
			
			$rules = Module::validateRules("Daycharges", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Daycharges", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified Daycharges from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Daycharges", "delete")) {
			Daycharge::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('token')->select($this->listing_cols)->where('dateval','=', date('d/m/Y'))->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Token');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				// if($col == $this->view_col) {
				// 	$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/daycharges/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				// }
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				// if(Module::hasAccess("Token", "edit")) {
				// 	$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/daycharges/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				// }
				
				// if(Module::hasAccess("Token", "delete")) {
				// 	$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.daycharges.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
				// 	$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
				// 	$output .= Form::close();
				// }
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}

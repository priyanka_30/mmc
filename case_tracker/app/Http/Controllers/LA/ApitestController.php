<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use DB;
use Validator;
use Datatables;
use Mail;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;


use App\Models\Apitest;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class ApitestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(Request $request)
    {
       
       
        return view('la.apitest',[
           
        ]);
    }

    public function createdetails(Request $request)
    {
        
        $data = $request->input();
        if(isset($data['jobtitle']) && $data['jobtitle']!="" && isset($data['email']) && $data['email']!="" && isset($data['mobile']) && $data['mobile']!=""){
                
                // Create Account
                $existuser = Tblmuser::where('email_address', '=', $data['email'])->first();
                if(empty($existuser)) {
                   
                    
                    // Create User
                    $user = Apitest::create([
                        //'name' => $data['name'],
                        'email' => $data['email'],
                        'jobtitle' => $data['jobtitle'],
                        'mobile' => $data['mobile'],
                        'date_created' => date('Y-m-d H:i:s'),
                    ]);
                    
                    
                }else {
                    $dataresult['userDetail'] =  array();
                    $dataresult['message'] = 'Email already exist';
                    $dataresult['status'] = 400;
                }
                
        }else {
            $dataresult['userDetail'] =  array();
            $dataresult['message'] = 'All Fields required.';
            $dataresult['status'] = 400;
        }
        echo json_encode($dataresult); die;
    }
    

   
    


}
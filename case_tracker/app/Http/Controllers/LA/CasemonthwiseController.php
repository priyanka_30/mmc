<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Dailycollection;
use App\Models\Medicalcase;
use App\Models\CaseData;

class CasemonthwiseController extends Controller
{
	public $show_action = true;
	public $view_col = 'id';
	public $listing_cols = ['id'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Medicalcase', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Medicalcase', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Casemonthwise');
		

		
        $case = DB::select('select count(id) as id, year(STR_TO_DATE(dob, "%m/%d/%Y")) as year, month(STR_TO_DATE(dob, "%m/%d/%Y")) as month from medicalcases group by year(STR_TO_DATE(dob, "%m/%d/%Y")), month(STR_TO_DATE(dob, "%m/%d/%Y"))');

        $pot = DB::select('select count(id) as pid, year(STR_TO_DATE(dateval, "%m/%d/%Y")) as year, month(STR_TO_DATE(dateval, "%m/%d/%Y")) as month from case_potencies group by year(STR_TO_DATE(dateval, "%m/%d/%Y")), month(STR_TO_DATE(dateval, "%m/%d/%Y"))');
       

		if(Module::hasAccess($module->id)) {
			return View('la.casemonthwise.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'case' => $case,
				'pot' => $pot,
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created Daycharges in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Daycharges", "create")) {
		
			$rules = Module::validateRules("Daycharges", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Daycharges", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Daycharges", "view")) {
			
			$daycharge = Daycharge::find($id);
			if(isset($daycharge->id)) {
				$module = Module::get('Daycharges');
				$module->row = $daycharge;
				
				return view('la.daycharges.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('daycharge', $daycharge);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("daycharge"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Daycharges", "edit")) {
			
			$daycharge = Daycharge::find($id);
			if(isset($daycharge->id)) {
				
				$module = Module::get('Daycharges');
				
				$module->row = $daycharge;
				
				return view('la.daycharges.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('daycharge', $daycharge);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("daycharge"),
				]);
			}			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified Daycharges in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Daycharges", "edit")) {
			
			$rules = Module::validateRules("Daycharges", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Daycharges", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified Daycharges from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Daycharges", "delete")) {
			Daycharge::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('payments')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Dailycollection');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/daycharges/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Daycharges", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/daycharges/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Daycharges", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.daycharges.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}

<?php

/**

 * Controller genrated using LaraAdmin

 * Help: http://laraadmin.com

 */



namespace App\Http\Controllers\LA;



use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use DB;

use Validator;

use Datatables;

use Collective\Html\FormFacade as Form;

use Dwij\Laraadmin\Models\Module;

use Dwij\Laraadmin\Models\ModuleFields;



use App\Models\CasePotency;



class RemedyController extends Controller

{

	public $show_action = true;

	public $view_col = 'id';

	public $listing_cols = ['id'];

	

	public function __construct() {

		// Field Access of Listing Columns

		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {

			$this->middleware(function ($request, $next) {

				$this->listing_cols = ModuleFields::listingColumnAccessScan('Remedy', $this->listing_cols);

				return $next($request);

			});

		} else {

			$this->listing_cols = ModuleFields::listingColumnAccessScan('Remedy', $this->listing_cols);

		}

	}

	

	/**

	 * Display a listing of the Daycharges.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function index()

	{

		$module = Module::get('Remedy');

		$date = date('n/d/Y');

//echo $date;

		$CasePotency =  DB::table('case_potencies')

				->select('case_potencies.*','stocks.name','stocks.id as sid','case_frequency.frequency','case_frequency.id as fid', 'potencies.id as pid' ,'potencies.name as pname', 'daycharges.id as dayid', 'daycharges.days','case_datas.id as cid','case_datas.regid as cregid','case_datas.first_name','case_datas.surname','courier_medicine.regid','courier_medicine.post_type')

				->join('stocks','stocks.id','=','case_potencies.rxremedy')

				->join('case_frequency','case_frequency.id','=','case_potencies.rxfrequency')

				->join('potencies','potencies.id','=','case_potencies.rxpotency')

				->join('daycharges','daycharges.id','=','case_potencies.rxdays')

				->join('case_datas','case_datas.id','=','case_potencies.regid')

				->join('courier_medicine','courier_medicine.regid','=','case_potencies.regid')

				->where('case_potencies.dateval','=',$date)

				->where('case_potencies.deleted_at','=',NULL)

				->orderBy('id','DESC')

				->get();

//echo "<pre>"; print_r($CasePotency); die();			

		if(Module::hasAccess($module->id)) {

			return View('la.remedy.index', [

				'show_actions' => $this->show_action,

				'listing_cols' => $this->listing_cols,

				'module' => $module,

				'CasePotency' =>$CasePotency,

			]);

		} else {

            return redirect(config('laraadmin.adminRoute')."/");

        }

	}



	/**

	 * Show the form for creating a new Daycharges.

	 *

	 * @return \Illuminate\Http\Response

	 */

	public function create()

	{

		//

	}



	/**

	 * Store a newly created Daycharges in database.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @return \Illuminate\Http\Response

	 */

	public function store(Request $request)

	{

		if(Module::hasAccess("Smstemplate", "create")) {

		

			$rules = Module::validateRules("Smstemplate", $request);

			

			$validator = Validator::make($request->all(), $rules);

			

			if ($validator->fails()) {

				return redirect()->back()->withErrors($validator)->withInput();

			}

			

			$insert_id = Module::insert("Smstemplate", $request);

			

			return redirect()->route(config('laraadmin.adminRoute') . '.smstemplate.index');

			

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}



	/**

	 * Display the specified Daycharges.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function show($id)

	{

		if(Module::hasAccess("Daycharges", "view")) {

			

			$daycharge = Daycharge::find($id);

			if(isset($daycharge->id)) {

				$module = Module::get('Daycharges');

				$module->row = $daycharge;

				

				return view('la.daycharges.show', [

					'module' => $module,

					'view_col' => $this->view_col,

					'no_header' => true,

					'no_padding' => "no-padding"

				])->with('daycharge', $daycharge);

			} else {

				return view('errors.404', [

					'record_id' => $id,

					'record_name' => ucfirst("daycharge"),

				]);

			}

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}



	/**

	 * Show the form for editing the specified Daycharges.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function edit($id)

	{

		if(Module::hasAccess("Smstemplate", "edit")) {

			

			$daycharge = Smstemplate::find($id);



			if(isset($daycharge->id)) {

				

				$module = Module::get('Smstemplate');

				

				$module->row = $daycharge;

				

				return view('la.smstemplate.edit', [

					'module' => $module,

					'view_col' => $this->view_col,

				])->with('daycharge', $daycharge);

			} else {

				return view('errors.404', [

					'record_id' => $id,

					'record_name' => ucfirst("smstemplate"),

				]);

			}			

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}



	/**

	 * Update the specified Daycharges in storage.

	 *

	 * @param  \Illuminate\Http\Request  $request

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function update(Request $request, $id)

	{

		if(Module::hasAccess("Smstemplate", "edit")) {

			

			$rules = Module::validateRules("Smstemplate", $request, true);

			

			$validator = Validator::make($request->all(), $rules);

			

			if ($validator->fails()) {

				return redirect()->back()->withErrors($validator)->withInput();;

			}

			

			$insert_id = Module::updateRow("Smstemplate", $request, $id);

			

			return redirect()->route(config('laraadmin.adminRoute') . '.smstemplate.index');

			

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}



	/**

	 * Remove the specified Daycharges from storage.

	 *

	 * @param  int  $id

	 * @return \Illuminate\Http\Response

	 */

	public function destroy($id)

	{

		if(Module::hasAccess("Daycharges", "delete")) {

			Daycharge::find($id)->delete();

			

			// Redirecting to index() method

			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');

		} else {

			return redirect(config('laraadmin.adminRoute')."/");

		}

	}

	

	/**

	 * Datatable Ajax fetch

	 *

	 * @return

	 */

	public function dtajax()

	{

		$values = DB::table('case_potencies')->select($this->listing_cols)->whereNull('deleted_at');

		$out = Datatables::of($values)->make();

		$data = $out->getData();



		$fields_popup = ModuleFields::getModuleFields('Remedy');

		

		for($i=0; $i < count($data->data); $i++) {

			for ($j=0; $j < count($this->listing_cols); $j++) { 

				$col = $this->listing_cols[$j];

				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {

					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);

				}

				if($col == $this->view_col) {

					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/daycharges/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';

				}

				// else if($col == "author") {

				//    $data->data[$i][$j];

				// }

			}

			

			if($this->show_action) {

				$output = '';

				if(Module::hasAccess("Smstemplate", "edit")) {

					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/smstemplate/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';

				}

				

				if(Module::hasAccess("Smstemplate", "delete")) {

					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.smstemplate.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);

					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';

					$output .= Form::close();

				}

				$data->data[$i][] = (string)$output;

			}

		}

		$out->setData($data);

		return $out;

	}

}


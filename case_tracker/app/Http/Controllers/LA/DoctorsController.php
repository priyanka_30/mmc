<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use Dwij\Laraadmin\Helpers\LAHelper;

use App\User;
use App\Models\Doctor;
use App\Models\Appointment;
use App\Models\CaseData;
use App\Role;
use Mail;
use Log;
use App\Models\CaseImage;

class DoctorsController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name', 'mobile', 'email'];
	
	public function __construct() {
		
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Doctors', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Doctors', $this->listing_cols);
		}
		

	}
	
	/**
	 * Display a listing of the Doctors.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Doctors');
	    //echo "<pre>"; print_r($module); die();
		if(Module::hasAccess($module->id)) {
			return View('la.doctors.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new doctor.
	 *
	 * @return \Illuminate\Http\Response
	 */


	/**
	 * Store a newly created doctor in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */

	public function store(Request $request)
	{

		if(Module::hasAccess("Doctors", "create")) {

		    $insert = array();
			$rules = Module::validateRules("Doctors", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$usersget = User::where('email','=', $request->email)->first();
			if(count($usersget)){
				return redirect()->back()->withErrors([ 'Email-ID already exists' ]);
			}
			else{
			// generate password
			//$password = $request->email;//LAHelper::gen_password();
			$password = $request->password;//LAHelper::gen_password();
			$email = $request->email;
            $mobile= $request->mobile;
            $name= $request->name;
			if ($files = $request->file('profilepic')) {
               $destinationPath = 'la-assets/img/'; // upload path
               $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();

               $files->move($destinationPath, $profileImage);
                $insert['profilepic'] = "$profileImage";
    		}
            $insert['name'] = $request->name;
			$insert['title'] = $request->title;
			$insert['firstname'] = $request->firstname;
			$insert['middlename'] = $request->middlename;
			$insert['surname'] = $request->surname;
			$insert['qualification'] = $request->qualification;
			$insert['instutitue'] = $request->instutitue;
			$insert['mobile'] = $request->mobile;
			$insert['email'] = $request->email;
			$doctor = Doctor::create($insert);  	
			$send= $this->CaseRegister($mobile,$name,$email,$password);
	
			// Create Doctor
			//$doctor_id = Module::insert("Doctors", $request);

			// Create User
			$user = User::create([
				'name' => $request->name,
				'email' => $request->email,
				'password' => bcrypt($password),
				'context_id' => $doctor,
				'type' => "Doctor",
			]);
	
			// update user role
			$user->detachRoles();
			$role = Role::find($request->role);
			$user->attachRole($role);
		}
			
			if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
				// Send mail to User his Password
				Mail::send('emails.send_login_cred', ['user' => $user, 'password' => $password], function ($m) use ($user) {
					$m->from('hello@laraadmin.com', 'LaraAdmin');
					$m->to($user->email, $user->name)->subject('LaraAdmin - Your Login Credentials');
				});
			} else {
				Log::info("User created: username: ".$user->email." Password: ".$password);
			}
			
			return redirect()->route(config('laraadmin.adminRoute') . '.doctors.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified doctor.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Doctors", "view")) {
			
			$doctor = Doctor::find($id);
			//echo "<pre>";print_r($doctor);die();
			if(isset($doctor->id)) {
				$module = Module::get('Doctors');
				$module->row = $doctor;
				
				// Get User Table Information
				$user = User::where('context_id', '=', $id)->where('type', 'Doctor')->firstOrFail();
				$appointments = Appointment::where('assistant_doctor', '=', $id)->get();
				
				$casedata = DB::table('case_datas')->first();
				
				return view('la.doctors.show', [

					'user' => $user,
					'casedata' =>$casedata,
					'appointments' => $appointments,
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('doctor', $doctor);
			} 
			else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("doctor"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified doctor.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		
		if(Module::hasAccess("Doctors", "edit")) {
			
			$doctor = Doctor::find($id);
			if(isset($doctor->id)) {
				$module = Module::get('Doctors');
				unset($module->fields['password']);
				$module->row = $doctor;
				
				// Get User Table Information
				//$user = User::where('context_id', '=', $id)->where('type', 'Doctor')->firstOrFail();
				
				return view('la.doctors.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
					//'user' => $user,
				])->with('doctor', $doctor);
			} 
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified doctor in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Doctors", "edit")) {
			
			$rules = Module::validateRules("Doctors", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$doctor_id = Module::updateRow("Doctors", $request, $id);
        	
			// Update User
			// $user = User::where('context_id', $doctor_id)->where('type', 'Doctor')->first();
			// $user->name = $request->name;
			// $user->save();
			
			
			return redirect()->route(config('laraadmin.adminRoute') . '.doctors.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified doctor from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Doctors", "delete")) {
			Doctor::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.doctors.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('doctors')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Doctors');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 

				$col = $this->listing_cols[$j];
				
				if($col=='email' && $data->data[$i][$j] ==""){
					$data->data[$i][$j] = 'N/A';
				}
				if($col=='mobile' && $data->data[$i][$j] ==""){
					$data->data[$i][$j] = 'N/A';
				}
				elseif($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/doctors/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Doctors", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/doctors/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Doctors", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.doctors.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
	
	/**
     * Change Doctor Password
     *
     * @return
     */
	public function change_password($id, Request $request) {
		
		$validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
			'password_confirmation' => 'required|min:6|same:password'
        ]);
		
		if ($validator->fails()) {
			return \Redirect::to(config('laraadmin.adminRoute') . '/doctors/'.$id)->withErrors($validator);
		}
		
		$doctor = Doctor::find($id);
		$user = User::where("context_id", $doctor->id)->where('type', 'Doctor')->first();
		$user->password = bcrypt($request->password);
		$user->save();
		
		\Session::flash('success_message', 'Password is successfully changed');
		
		// Send mail to User his new Password
		if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
			// Send mail to User his new Password
			Mail::send('emails.send_login_cred_change', ['user' => $user, 'password' => $request->password], function ($m) use ($user) {
				$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
				$m->to($user->email, $user->name)->subject('LaraAdmin - Login Credentials chnaged');
			});
		} else {
			Log::info("User change_password: username: ".$user->email." Password: ".$request->password);
		}
		
		return redirect(config('laraadmin.adminRoute') . '/doctors/'.$id.'#tab-account-settings');
	}

	public function update_status( $id,Request $request) {

		$doctor = Doctor::find($id);
		$appointments = Appointment::where('id', $request->patient_id)->first();
		
		$appointments->status = $request->status;
		$appointments->save();
		
		
		return redirect(config('laraadmin.adminRoute') . '/doctors/'.$id.'#tab-view-appointments');
	}

	public function delete_status( $id,Request $request) {

		$doctor = Doctor::find($id);
		$appointments = Appointment::where('id', $request->patient_id)->first();
		Appointment::find($request->patient_id)->delete();
		
		
		return redirect(config('laraadmin.adminRoute') . '/doctors/'.$id.'#tab-view-appointments');
	}

	public function newcase(Request $request)
	{
		
		//echo "<pre>"; print_r($request->patient_id); 
		$casedata = DB::table('case_datas')->where('regid','=',$request->patient_id)->first();
 	 	$mydata = json_decode(json_encode($casedata), true);
 	 	//echo "<pre>"; print_r($casedata); die();
 	 	$regid = $mydata['regid']; 

 	 	if($request->type=="newcase"){
 	 		$id = session()->put('id', $request->appointment_id);
 	 		//return redirect("http://smartops.co.in/managemyclinic/admin/medicalcases/");
 	 		return redirect()->route(config('laraadmin.adminRoute') . '.medicalcases.index');
 	 	}
 	 	else{
 	 		
		return redirect("http://smartops.co.in/managemyclinic/admin/medicalcases/".$request->patient_id);
		}
	}

	public function sendsms(Request $request)
	{
		//echo "<pre>"; print_r($request->all()); die();	
		return redirect()->route(config('laraadmin.adminRoute') . '.doctors.index');
	}
	

}



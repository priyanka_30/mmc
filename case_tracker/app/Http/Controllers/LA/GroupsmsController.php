<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use DB;
use Validator;
use Datatables;
use Mail;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\TempCase;
use App\Models\Medicalcase;
use App\Models\CaseData;
use App\Models\CommunicationDetail;
use App\Models\Doctor;
use App\Models\CaseHeight;
use App\Models\Casereminder;
use App\Models\HomeoDetail;
use App\Models\CasePotency;
use App\Models\CaseAdditionalcharges;
use App\Models\CaseInvestigation;
use App\Models\CaseCbc;
use App\Models\CaseUrine;
use App\Models\CaseStool;
use App\Models\CaseArthritis;
use App\Models\CaseEndocrine;
use App\Models\CaseRenal;
use App\Models\CaseXray;
use App\Models\CaseUsgfemale;
use App\Models\CaseUsgmale;
use App\Models\CaseImmunology;
use App\Models\CaseSpecific;
use App\Models\CaseLiver;
use App\Models\CaseLipid;
use App\Models\CaseDiabetes;
use App\Models\CaseCardiac;
use App\Models\CaseSerological;
use App\Models\CaseSemenanalysis;
use App\Models\CaseNotes;


/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class GroupsmsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(Request $request)
    {
       
       
        return view('la.groupsms',[
           
        ]);
    }
    

    public  function sendgroupsms(Request $request)
    {
        $data = $request->all();
        $message = $data['message'];
        $from_id = $data['from_id'];
        $to_id = $data['to_id'];

        for($i=$from_id; $i<=$to_id; $i++){

            $arr[] = $i;
        }
        $val= implode(",", $arr);
        $myarray = explode(",", $val);
        $getMobilelist = CaseData::whereIn('regid', $myarray)->get();
        foreach($getMobilelist as $list){
            $phone_no = $list->phone;
            $send= $this->CaseCreateSms($phone_no);  
        }
        return redirect(config('laraadmin.adminRoute')."/groupsms");

    } 


}
<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Medicalcase;
use App\Models\CaseData;
use App\Models\BasicDetail;
use App\Models\HomeoDetail;
use App\Models\AddReminder;
use App\Models\CaseHeight;
use App\Models\CasePotency;
use App\Models\CommunicationDetail;
use App\Models\Doctor;
use App\Models\Package;
use App\Models\Employee;
use App\Models\Appointment;
class AppointmentsController extends Controller
{
	public $show_action = true;
	public $view_col = 'patient_name';
	
	public $listing_cols = ['id', 'patient_name', 'booking_date', 'booking_time',  'status'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Appointments', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Appointments', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Organizations.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    $varId = Auth::user()->context_id;
	    $vartype = Auth::user()->type;
	    if($vartype=='Doctor'|| $vartype=='Clinic admin'){
	     	return redirect(config('laraadmin.adminRoute') . '/doctors/'.$varId.'#tab-view-appointments');
	     }
	    
		$module = Module::get('Appointments');
        $doctors = DB::table('doctors')->get();
        $packages = DB::table('packages')->get();
        $patients = DB::table('employees')->get();
        $casename = DB::table('case_datas')->get();
       // echo "<pre>"; print_r($module); die();
		if(Module::hasAccess($module->id)) {
			return View('la.appointments.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'doctors' => $doctors,
				'packages' => $packages,
				'patients'=>$patients,
				'casename' => $casename,

			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
        
        
	}

	/**
	 * Show the form for creating a new organization.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function convertdate($date){
	      //return $date;
	      if($date!=""){
			$date1 = explode('/', $date);
			$newdate = $date1[2].'-'.$date1[1].'-'.$date1[0];
			return $newdate;
	      }else {
	          return '';
	      }
	}

	
	public function getdoctordetail(Request $request)
	{
		//echo $request->value;
		$case = DB::table('case_datas')
			->select('case_datas.*','doctors.id as did', 'doctors.name')
			->join('doctors','doctors.id','=','case_datas.assitant_doctor')
			->where('case_datas.regid','=', $request->value)
			->first();
		//echo "<pre>"; print_r($case);
		$doctorname = $case->name;
		$doctorid = $case->did;
		if(!empty($case)){
		$arr = array('doctorname'=>$doctorname, 'doctorid'=>$doctorid);
		}
		else{
			$arr = array("doctorname" => "",'doctorid'=>"");
		}

		echo json_encode($arr); die();
	}

	public function getbooktime(Request $request)
	{
		$appdata = DB::table('appointments')->where('booking_date', '=',date('d/m/Y'))->where('booking_time', '=',$request->booktime)->first();
		
		if(!empty($appdata)){
		 	echo "1";
		}
		else{
			echo "0";
		}
		die();
	}

	public function updatestatus(Request $request)
	{
		$data = $request->all();
	    if(isset($data['appid']) && $data['appid']!=""){ $appid =  $data['appid']; } else { $appid = '';}
	    if(isset($data['status']) && $data['status']!=""){ $status =  $data['status']; } else { $status = '';}
	    
	   
		$update = DB::table('appointments')->where('id', $appid)->update([ 'status' => $status ]);

	
		
		return redirect()->route(config('laraadmin.adminRoute') . '.appointments.index');
	}

    
	/**
	 * Store a newly created organization in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Appointments", "create")) {
		
			$rules = Module::validateRules("Appointments", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			$data = $request->all();
			$patientname = DB::table('case_datas')->where('regid', '=',$data['patient_name'])->first();
			if(!empty($patientname)){
				$fname = $patientname->first_name." ".$patientname->surname;
			}
			else{
				$fname = $data['patient_name'];
			}
			 $appdata = DB::table('appointments')->where('booking_date', '=',$data['booking_date'])->where('booking_time', '=',$data['booking_time'])->first();
			 if(empty($appdata)){
		    $user1 = Appointment::create([
			            'booking_date' => $data['booking_date'],
                        'booking_time'=>$data['booking_time'],
                        'assistant_doctor'=>$data['assistant_doctor'],
                        'patient_name'=>$fname,
                        'patient_id'=>$data['patient_name'],
                        'address'=>$data['address'],
                        'state'=>$data['state'],
                        'city'=>$data['city'],
                        'phone'=>$data['phone'],
                        'status'=>$data['status']
                    ]);
			}
			 else{
			 	return redirect()->back()->withErrors([ 'Appointment Time not available' ]);
			 }
                    
			
			//echo "<pre>"; print_r($insert_id); echo "</pre>"; 
			Appointment::where('id', $user1)->update(['patient_id' => Auth::user()->context_id]);
			return redirect()->route(config('laraadmin.adminRoute') . '.appointments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Appointments", "view")) {
			
			$medicalcase = Appointment::find($id);
			
			
            //$HeightWeighty = HeightWeight::find(1);
            //echo "<pre>"; print_r($medicalcase); die();
			if(isset($medicalcase->id)) {
				$module = Module::get('Appointments');
				$module->row = $medicalcase;
				
				return view('la.appointments.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('medicalcase', $medicalcase);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("appointments"),
				]);

			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");

		}
	}

	/**
	 * Show the form for editing the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Appointments", "edit")) {
			$medicalcase = Medicalcase::find($id);
			if(isset($medicalcase->id)) {
				$medicalcase = Medicalcase::find($id);
				
				$module = Module::get('Appointments');
				
				$module->row = $medicalcase;
				
				return view('la.appointments.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('medicalcase', $medicalcase);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("appointments"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified organization in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Appointments", "edit")) {
			
			$rules = Module::validateRules("Appointments", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Appointments", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.appointments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified organization from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Appointments", "delete")) {
			Appointment::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.appointments.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
	    $newlist = ['id', 'patient_name', 'booking_date', 'booking_time', 'status'];
		$values = DB::table('appointments')->select($newlist)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Appointments');
	//	echo "<pre>"; print_r($data);die();
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) {
				$col = $this->listing_cols[$j];
				
			
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
			     $medicalcase = Medicalcase::where('id', '=', $data->data[$i][0])->first();
				
				
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				$output .= '<a onclick="openstatus('.$data->data[$i][0].')" class="btn btn-primary btn-xs" style="display:inline;padding:2px 5px 3px 5px;">Status</a>';
				if(Module::hasAccess("Appointments", "edit")) {
				//	$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/medicalcases/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Appointments", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.appointments.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
	function newcaseadd(){
	    $this->layout = null;
	    $getcaseId = rand(100000,999999);
	    echo $getcaseId; die();
	}
}

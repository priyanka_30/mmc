<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use DB;
use Validator;
use Datatables;
use Mail;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\CaseData;
use App\Models\Doctor;
use App\Models\CaseChats;
/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $today = date('d');
        $caseData = CaseData::whereDay('date_of_birth','=',date('d'))->get();
        $doctor = Doctor::where('id' ,'=', Auth::user()->context_id)->first();
        $casechat = CaseChats::orderBy('id', 'DESC')->limit(10)->get()->reverse();
        
        if(Auth::user()->context_id!=$casechat[0]['userid']){
            $unreadcount =  0;
            $affected = DB::table('users')->update(array('unread' => $unreadcount));
        }
        
        $value = $request->session()->get('showalert');
        $setvalue = $value;
        if($value!=1){
            $request->session()->put('showalert', '1');
        }
        
        return view('la.dashboard',[
            "casedata" => $caseData,
            "doctor" => $doctor,
            "casechat" => $casechat,
            "showpop" =>$setvalue,
        ]);
    }
    
    public function loadmoredata(Request $request){
            $output = '';
            $id = $request->id;
            $chat = CaseChats::where('id','<',$id)->orderBy('id', 'DESC')->limit(10)->get()->reverse();
           
            if(!$chat->isEmpty()){
                $getchatid = '';
                foreach($chat as $chatdatas) {
                    if($getchatid==""){ $getchatid = $chatdatas->id;}
                }
                 $output = '<div id="remove-row" class="text-center">
                    <button id="btn-more" data-id="'.$getchatid.'" class="loadmore-btn text-center" onclick="loadmoredata(this)">Load More</button>
                    </div>';
                foreach($chat as $chats) {
                    $output .= '<div class="item">
                    <img src="https://www.gravatar.com/avatar/64e1b8d34f425d19e1ee2ea7236d3028.jpg?s=80&d=mm&r=g" alt="user image" class="online">
                        <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> '.date('d/m/yy h:i A',strtotime($chats->created_at)).'</small>'.$chats->username.
                        '</a>'.$chats->message.'</p>
                        </div>';
                }
                echo $output;
            }
    }

    public function sendbirthdy(Request $request){
       
        $sendbirthdy = $request->sendbirthdy;
         foreach($sendbirthdy as $value){
           $id[] = $value;    
         }

         $cid= implode(",", $id);
         $ccid = explode(",", $cid);
         foreach($ccid as $ids){
            $caseData = DB::table('case_datas')->where('id','=',$ids)->first();
            $mydata = json_decode(json_encode($caseData), true);

            $phone_no = $mydata['phone'];
            $name = $mydata['first_name'];

            $send= $this->birthdaysms($phone_no,$name);

            return redirect(config('laraadmin.adminRoute')."/");
         }
        
     
    }

    public function sendchats(Request $request){
        $this->layout = null;
        $data = $request->all();
        
        if(isset($data['message']) && $data['message']!=""){ $message =  $data['message']; } else { $message = '';}
        if(isset($data['username']) && $data['username']!=""){ $username =  $data['username']; } else { $username = '';}
        if(isset($data['userid']) && $data['userid']!=""){ $userid =  $data['userid']; } else { $userid = '';}
        $time = date("h:i"); 
        $user = CaseChats::create([
             'userid' => $userid,
             'username' =>$username,
             'message' =>$message,
             'chat_time'=>$time,
             'chat_date' => date('d/m/Y'),
        ]);
        
       // $affected = DB::table('users')->where('unread', '=', 0)->update(array('unread' => 1));
        $getunread = DB::table('users')->where('id','=',1)->first();
        $getunreadval = json_decode(json_encode($getunread), true);
        $unreadcount =  $getunreadval['unread'] + 1;
        $affected = DB::table('users')->update(array('unread' => $unreadcount));

        echo die();
    }

    public function sendmail(Request $request){
   
        $to_email = $request->emailto;
        $mailmessage = $request->mailmessage;
        $subject = $request->subject;
        
        $data = array("body" => $mailmessage);
          
        Mail::send("la.email.dashboard", $data, function($message) use ($to_email,$subject) {
        $message->to($to_email)
        ->subject($subject);
        $message->from("lavleshmishra6@gmail.com","Managemyclinic");
        });
   
        return 'Email sent Successfully';

    }

    public  function fetch(Request $request)
    {
         if($request->get('query'))
         {
          $query = $request->get('query');
          $data = DB::table('users')
            ->where('email', 'LIKE', "%{$query}%")
            ->get();
          $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
          foreach($data as $row)
          {
           $output .= '
           <li class="emaildata"><a href="#">'.$row->email.'</a></li>
           ';
          }
          $output .= '</ul>';
          echo $output;
         }
    }
}
<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Expenses;
use App\Models\Record;

class RecordController extends Controller
{
	public $show_action = true;
	public $view_col = 'record';
	public $listing_cols = ['regid','name','mobile', 'recordtype','comment','created_at'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Record', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Record', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Record');
		$expenseshead = DB::table('expenseshead')->get();
		

    	$data=  DB::table('records')->orderBy('id', 'DESC')->get();

    	
    	//echo "<pre>"; print_r($expensedata); die(); 
		if(Module::hasAccess($module->id)) {
			return View('la.record.index', [
				'expenseshead' => $expenseshead,
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'data' => $data,
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	public function searchrecord(Request $request)
	{
		$product = DB::table('case_datas')->where('regid','=', $request->regid)->first();
		//echo "<pre>"; print_r($product->first_name); die();
		//$output="";

		$output = $product->first_name." ". $product->surname."/";
		$output .= $product->mobile1."/";
		$output .= $product->phone;
		return Response($output);
	}

	public function newrecord(Request $request)
	{
		$data = $request->all();
		//echo "succes"; die();
	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}
	    if(isset($data['recordtype']) && $data['recordtype']!=""){ $recordtype =  $data['recordtype']; } else { $recordtype = '';}
	    if(isset($data['comment']) && $data['comment']!=""){ $comment =  $data['comment']; } else { $comment = '';}
	    if(isset($data['name']) && $data['name']!=""){ $name =  $data['name']; } else { $name = '';}
	    if(isset($data['mobile']) && $data['mobile']!=""){ $mobile =  $data['mobile']; } else { $mobile = '';}
	    if(isset($data['recorddate']) && $data['recorddate']!=""){ $recorddate =  $data['recorddate']; } else { $recorddate = '';}

	    $currentdate =  date('Y-m-d');
	  //  echo $currentdate;
	   $check = Record::where([ ['recorddate', '=', $currentdate],['regid','=',$regid] ])->first();
	   // $check = Record::where([ ['regid','=',$regid] ])->first();
	 //   echo  "<pre>"; print_r($data); die();
	   	if($check==""){
		$user = Record::create([
			 'regid' => $regid,
			 'recordtype' => $recordtype,
			 'comment' => $comment,
			 'name' => $name,
			 'mobile' => $mobile,
			 'recorddate' => $recorddate,
	    ]);
	 }
	 else{
	 	return redirect()->back()->withErrors([ 'Record Already Added for Today' ]);
	 }

	
		return redirect()->route(config('laraadmin.adminRoute') . '.record.index');
	}

	public function saverecord(Request $request)
	{
		$data = $request->all();
		echo "succes"; die();
	    if(isset($data['regid']) && $data['regid']!=""){ $regid =  $data['regid']; } else { $regid = '';}
	    if(isset($data['recordtype']) && $data['recordtype']!=""){ $recordtype =  $data['recordtype']; } else { $recordtype = '';}
	    if(isset($data['comment']) && $data['comment']!=""){ $comment =  $data['comment']; } else { $comment = '';}
	    if(isset($data['name']) && $data['name']!=""){ $name =  $data['name']; } else { $name = '';}
	    if(isset($data['mobile']) && $data['mobile']!=""){ $mobile =  $data['mobile']; } else { $mobile = '';}
	    if(isset($data['recorddate']) && $data['recorddate']!=""){ $recorddate =  $data['recorddate']; } else { $recorddate = '';}

	    $currentdate =  date('Y-m-d');
	  //  echo $currentdate;
	   $check = Record::where([ ['recorddate', '=', $currentdate],['regid','=',$regid] ])->first();
	   // $check = Record::where([ ['regid','=',$regid] ])->first();
	    echo  "<pre>"; print_r($data); die();
	   	if($check==""){
		$user = Record::create([
			 'regid' => $regid,
			 'recordtype' => $recordtype,
			 'comment' => $comment,
			 'name' => $name,
			 'mobile' => $mobile,
			 'recorddate' => $recorddate,
	    ]);
	 }
	 else{
	 	return redirect()->back()->withErrors([ 'Record Already Added for Today' ]);
	 }

	
		return redirect()->route(config('laraadmin.adminRoute') . '.record.index');
	}

	/**
	 * Store a newly created Daycharges in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Daycharges", "create")) {
		
			$rules = Module::validateRules("Daycharges", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Daycharges", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Daycharges", "view")) {
			
			$daycharge = Daycharge::find($id);
			if(isset($daycharge->id)) {
				$module = Module::get('Daycharges');
				$module->row = $daycharge;
				
				return view('la.daycharges.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('daycharge', $daycharge);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("daycharge"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		
		$data = Expenses::where('id','=', $id)->first();
	
		$expenseshead = DB::table('expenseshead')->get();
		
		$module = Module::get('Expenses');
		return View('la.expenses.edit', [
				'expenses' => $data,	
				'expenseshead' => $expenseshead,	
				'view_col' => $this->view_col,	
				'module' => $module,	
			]);
	}

	

	/**
	 * Update the specified Daycharges in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Daycharges", "edit")) {
			
			$rules = Module::validateRules("Daycharges", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Daycharges", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified Daycharges from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Expenses", "delete")) {
			Expenses::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.expenses.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('records')->select($this->listing_cols)->whereMonth('recorddate','=',date('m'))->whereYear('recorddate','=',date('Y'))->whereNull('deleted_at')->limit(1000);
		
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Record');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($col == "created_at") {
				   $data->data[$i][$j] = date('d M Y',strtotime($data->data[$i][$j]));
				}

				if($col=='regid' && $data->data[$i][$j] ==""){
					$data->data[$i][$j] = 'N/A';
				}
				elseif($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				// if($col == $this->view_col) {
				// 	$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/daycharges/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				// }

				 
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Expenses", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/expenses/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Expenses", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.expenses.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}

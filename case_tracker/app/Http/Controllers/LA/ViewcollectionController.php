<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Dailycollection;
use App\Models\Bankdeposit;

class ViewcollectionController extends Controller
{
	public $show_action = true;
	public $view_col = 'id';
	public $listing_cols = ['id'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Dailycollection', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Dailycollection', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//echo "<pre>"; print_r($request->all()); die();
		$module = Module::get('Viewcollection');
		$totalexp = 0;
		// $payments = DB::table('bill')
		// 		->select('bill.*','case_datas.id as cid','case_datas.regid', 'case_datas.first_name','case_datas.surname')
		// 		->join('case_datas','case_datas.id','=','bill.regid')
		// 		->where('fromdate', '=',date('n/j/Y'))
		// 		->where('bill.deleted_at', '=',NULL)
		// 		->get();
		$payments = DB::table('receipt')->where('receiptdate', '=',date('n/j/Y'))->get();
		$totalbank = 0;
		$totalcash = 0;
		$amount = 0;
		$targetamount = 0;
		$total = 0;
		$total_additional = 0;
		$totalbankmonth = 0;
		$bank_sum = 0;
		$cash_sum = 0;
		

		if(Module::hasAccess($module->id)) {
			return View('la.viewcollection.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'payments' => $payments,
				'query' => '',
				'totalbank' =>$totalbank,
				'totalcash' => $totalcash,
				'amount' => $amount,
				'targetamount' => $targetamount,
				'total' => $total,
				'total_additional' => $total_additional,
				'totalbankmonth' => $totalbankmonth,
				'bank_sum' => $bank_sum,
				'cash_sum' => $cash_sum,

				
			]);
		} else {
			
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	public function convertdate($date){
	      //return $date;
	      if($date!=""){
			$date1 = explode('/', $date);
			$newdate = $date1[2].'-'.$date1[1].'-'.$date1[0];
			return $newdate;
	      }else {
	          return '';
	      }
	}
	public function billconvertdate($date){
	      //return $date;
	      if($date!=""){
			$date1 = explode('/', $date);
			$newdate = $date1[1].'/'.$date1[0].'/'.$date1[2];
			return $newdate;
	      }else {
	          return '';
	      }
	}

	public function newconvertdate($date){
	      //return $date;
	      if($date!=""){
			$date1 = explode('/', $date);
			$newdate = $date1[1].'/'.$date1[0].'/'.$date1[2];
			return $newdate;
	      }else {
	          return '';
	      }
	}

	public function search(Request $request)
	{
		
		$module = Module::get('Viewcollection');
		$query = $this->convertdate($request->get('date'));
		//echo $this->newconvertdate($request->get('date')); die();
		$dateval = $this->newconvertdate($request->get('date'));
		$billdate = $this->billconvertdate($request->get('date'));
	//echo date('d',strtotime($request->get('date')));
		
		$totalbank = 0;
		$totalcash = 0;
		$amount = 0;
		$targetamount = 0;
		$total = 0;
		$total_additional = 0;
		$totalbankmonth = 0;

		

		
		
		//$previous_date =  date('Y-m-d',strtotime('-45 days',strtotime($query)));
		$previous_date = "2020-09-01";
		//echo $previous_date."<br>";
		//echo $query."<br>";
		if(!empty($query)){
			//$payments = DB::table('payments')->where('received_date', '=',$query)->get();
			$coll = DB::table('bill')->where('fromdate', '=',date('n/j/Y',strtotime($billdate)))->get();
			$payments = DB::table('receipt')->where('receiptdate', '=',date('n/j/Y',strtotime($billdate)))->get();
			$additional = DB::table('additional_charges')->where('dateval', '=',date('n/j/Y',strtotime($billdate)))->get();

			foreach($coll as $collect){
				$total += $collect->received;
			}

			foreach($additional as $addcharges){
				$total_additional += $addcharges->additional_price;
			}
			
			$bank_deposit = DB::table('bank_deposit')->where('deposit_date', '=',$request->get('date'))->get();
			foreach($bank_deposit as $bank){
				$totalbank += $bank->amount;
			}

			$bank_deposit_month = Bankdeposit::whereMonth('dateval', '=',date('d',strtotime($request->get('date'))))->whereYear('dateval', '=',date('Y',strtotime($request->get('date'))))->get();
			foreach($bank_deposit_month as $bankmonth){
				$totalbankmonth += $bankmonth->amount;
			}

			$cash_deposit = DB::table('cash_deposit')->where('deposit_date', '=',$request->get('date'))->get();
			foreach($cash_deposit as $cash){
				$totalcash += $cash->amount;
			}

			$expenses = DB::table('expenses')->where('dateval', '=', date('n/j/Y',strtotime($dateval)))->get();
			foreach($expenses as $exp){
				$amount += $exp->amount;
			}

			$target = DB::table('daily_target')->where('dateval', '=', date('n/d/Y',strtotime($dateval)))->get();
			foreach($target as $daily){
				$targetamount += $daily->collection;
			}

		}
		else{
		$payments = DB::table('payments')->where('received_date', '=',date('Y-m-d'))->get();
		}

		$cash_sum = DB::table("cash_deposit")->whereBetween('dateval', [$previous_date, $query])->sum('amount');
		$bank_sum = DB::table("bank_deposit")->whereBetween('dateval', [$previous_date, $query])->sum('amount');
		//echo $cash_sum. "<br>";
		// echo $bank_sum."<br>"; 
	//	echo $cash_sum - $bank_sum; die();

		
			return View('la.viewcollection.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,
				'payments' => $payments,
				'query' => $request->get('date'),
				'totalbank' =>$totalbank,
				'totalcash' => $totalcash,
				'amount' => $amount,
				'targetamount' => $targetamount,
				'total' => $total,
				'total_additional' => $total_additional,
				'totalbankmonth' => $totalbankmonth,
				'cash_sum' => $cash_sum,
				'bank_sum' => $bank_sum,
			]);
		
	}

	/**
	 * Show the form for creating a new Daycharges.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created Daycharges in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Daycharges", "create")) {
		
			$rules = Module::validateRules("Daycharges", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Daycharges", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Daycharges", "view")) {
			
			$daycharge = Daycharge::find($id);
			if(isset($daycharge->id)) {
				$module = Module::get('Daycharges');
				$module->row = $daycharge;
				
				return view('la.daycharges.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('daycharge', $daycharge);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("daycharge"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified Daycharges.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Daycharges", "edit")) {
			
			$daycharge = Daycharge::find($id);
			if(isset($daycharge->id)) {
				
				$module = Module::get('Daycharges');
				
				$module->row = $daycharge;
				
				return view('la.daycharges.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('daycharge', $daycharge);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("daycharge"),
				]);
			}			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified Daycharges in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Daycharges", "edit")) {
			
			$rules = Module::validateRules("Daycharges", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Daycharges", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified Daycharges from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Daycharges", "delete")) {
			Daycharge::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.daycharges.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('payments')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Dailycollection');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/daycharges/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Daycharges", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/daycharges/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Daycharges", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.daycharges.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}

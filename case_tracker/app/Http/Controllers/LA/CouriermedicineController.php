<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Medicalcase;
use App\Models\CaseData;
use App\Models\BasicDetail;
use App\Models\HomeoDetail;
use App\Models\AddReminder;
use App\Models\CaseHeight;
use App\Models\CasePotency;
use App\Models\CommunicationDetail;
use App\Models\Doctor;
use App\Models\Package;
use App\Models\Employee;
use App\Models\Appointment;
use App\Models\Couriermedicine;
use App\Models\Smsreport;
class CouriermedicineController extends Controller
{
	public $show_action = true;
	public $view_col = 'case_id';
	
	public $listing_cols = ['id', 'case_id', 'days','is_assign', 'post_type'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Couriermedicine', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Couriermedicine', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Organizations.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    
	   // echo "tesr"; die();
		$module = Module::get('Couriermedicine');
		$couriers = DB::table('courier_medicine')->where('id','=',session()->get('pid'))->first();
     	$myArray = json_decode(json_encode($couriers), true);
     	$regid = $myArray['case_id'];
     	$user = CaseData::where('regid','=',$regid)->first();
        $session = session()->get('pid');
      	
      	$data = DB::table('courier_medicine')
      			->select('courier_medicine.*', 'case_datas.regid','case_datas.first_name','case_datas.surname')
      			->join('case_datas','case_datas.regid','=','courier_medicine.case_id')
      			->where('currentdate','=',date('Y-m-d'))
      			->where('post_type','=','Courier')
      			->get();

		if(Module::hasAccess($module->id)) {
			return View('la.couriermedicine.index', [
				'courier' => $myArray,
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'session' => $session,
				'user' => $user,
				'module' => $module,
				'data' => $data,
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
        
        
	}

	/**
	 * Show the form for creating a new organization.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function convertdate($date){
	      //return $date;
	      if($date!=""){
			$date1 = explode('/', $date);
			$newdate = $date1[2].'-'.$date1[1].'-'.$date1[0];
			return $newdate;
	      }else {
	          return '';
	      }
	}

	
	
	public function courier(Request $request){
	   // $this->layout = null;
		$data = $request->all();
	   
	   	$casep = Couriermedicine::where('rand_id','=',$request->rand_id)->first();
	   
	   	$casep->post_type = $request->post_type;
	   	$casep->read_type = "unread";
	   	$casep->save();
	   
	   
		echo "success"; die();
        //return redirect(config('laraadmin.adminRoute')."/medicalcases");
	}

	public function updatenotify(Request $request){
	   // $this->layout = null;
		$data = $request->all();
	   
	   	$affected = DB::table('courier_medicine')->where('read_type', '=', 'unread')->update(array('read_type' => 'read'));
	   	if($affected){
		echo "1"; die();
		}
		else{
			echo "0"; die();
		}
        //return redirect(config('laraadmin.adminRoute')."/medicalcases");
	}

	public function getcourierdetails(Request $request){
		$caseid = $request->id;

		$casedata=DB::table('courier_medicine')->where([ ['case_id','=',$caseid],['post_type','=',"Courier"] ])->get();
		
		
	//	echo "<pre>";print_r($additionalchg->additional_price); die();
		foreach ($casedata as $key => $product) {
			//$bal = $product->Balance;
			
			$output = "<tr>";
			$output .= "<td style='text-align:center;'>".date('d M Y',strtotime($product->created_at))."</td>";
			$output .= "<td style='text-align:center;'>".$product->pcd."</td>";
			$output .= "<td style='text-align:center;'>".$product->courier."</td>";
			
			echo $output;
		
	
		}
	
		
	}
    
	/**
	 * Store a newly created organization in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Appointments", "create")) {
		
			$rules = Module::validateRules("Appointments", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Appointments", $request);
			//echo "<pre>"; print_r($insert_id); echo "</pre>"; 
			Appointment::where('id', $insert_id)->update(['patient_id' => Auth::user()->context_id]);
			return redirect()->route(config('laraadmin.adminRoute') . '.appointments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Couriermedicine", "view")) {
			
			$couriermedicine = Couriermedicine::find($id);
			
			
            //$HeightWeighty = HeightWeight::find(1);
            //echo "<pre>"; print_r($medicalcase); die();
			if(isset($couriermedicine->id)) {
				$module = Module::get('Couriermedicine');
				$module->row = $couriermedicine;
				
				return view('la.couriermedicine.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('couriermedicine', $couriermedicine);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("couriermedicine"),
				]);

			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");

		}
	}

	/**
	 * Show the form for editing the specified organization.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Couriermedicine", "edit")) {
			$couriermedicine = Couriermedicine::find($id);
			if(isset($couriermedicine->id)) {
				$couriermedicine = Couriermedicine::find($id);
				
				$module = Module::get('Couriermedicine');
				
				$module->row = $couriermedicine;
				$stocks = DB::table('stocks')->get();
				$couriers = Couriermedicine::where('id','=',$id)->first();
		     	$user = CaseData::where('regid','=',$couriers->case_id)->first();
				return view('la.couriermedicine.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
					'stocks' => $stocks,
					'user' => $user,
				])->with('couriermedicine', $couriermedicine);
			} 
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified organization in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Appointments", "edit")) {
			
			$rules = Module::validateRules("Appointments", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Appointments", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.appointments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	public function getmedicinedetail(Request $request){
		$medid = $request->id;
	//	echo "<pre>"; print_r($request->all());
		$medicalcase=DB::table('courier_medicine')->where('id','=',$medid)->first();
		
		$case_id= $medicalcase->case_id	;
		
		$casedata=DB::table('case_datas')->where('regid','=',$case_id)->first();
	//	echo "<pre>"; print_r($casedata);die();
		$message = "Dear ".$casedata->first_name." ".$casedata->surname."  Your medicines has been despatched via ".$medicalcase->courier." and the POD number is ".$medicalcase->pcd.".For tracking log on www.dtdc.com  Regards Dr Nanda's Homeoclinic";
		$arr = array('phone'=>$casedata->mobile1,'first_name'=>$casedata->first_name,'surname'=>$casedata->surname,'pcd'=>$medicalcase->pcd,'courier'=>$medicalcase->courier,'regid'=>$casedata->regid,'message'=>$message);
		echo json_encode($arr); die();
	}


	public function medicineupdate(Request $request)
	{
		$cour = Couriermedicine::where('id', $request->id)->first();
		
		$cour->pcd = $request->pcd;
		$cour->courier = $request->courier;
		$cour->pickup = $request->pickup;
		$cour->save();

		return redirect()->route(config('laraadmin.adminRoute') . '.couriermedicine.index');
	}

	/**
	 * Remove the specified organization from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Couriermedicine", "delete")) {
			Couriermedicine::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.couriermedicine.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	public function assign($id)
	{
		// echo $id; die();
		session()->put('pid', $id);
		return redirect()->route(config('laraadmin.adminRoute') . '.couriermedicine.index');

	}

	public function savepackages(Request $request)
	{
		//echo "<pre>";print_r($request->all());die();

		$cour = Couriermedicine::where('id', $request->id)->first();
		
		$cour->pcd = $request->pcd;
		$cour->courier = $request->courier;
		$cour->pickup = $request->pickup;
	 	$cour->is_assign = $request->is_assign;
		$cour->save();
		return redirect()->route(config('laraadmin.adminRoute') . '.couriermedicine.index');
		
	}

	public function sendsms(Request $request)
	{
		$phone = $request->phone;
		$message = $request->message;
		$first_name = $request->first_name;
		$surname = $request->surname;
		$pcd = $request->pcd;
		$courier = $request->courier;
		$regid = $request->regid;

		$smsreport = Smsreport::create([
			 'regid' => $regid,
			 'send_date' => date('Y-m-d'),
			 'sms_type' => 'Courier',
		]);
		

		$msg = $this->CourierMedicine($phone,$message); 
		return redirect()->route(config('laraadmin.adminRoute') . '.couriermedicine.index');

	}
	
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
	    $newlist = ['id','case_id', 'days','is_assign','post_type'];
		$values = DB::table('courier_medicine')->select($newlist)->where('currentdate','=',date('Y-m-d'))->where('post_type','=','Courier')->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Couriermedicine');
	
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) {
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && $fields_popup[$col]->field_type_str == "Image") {
					if($data->data[$i][$j] != 0) {
						$img = \App\Models\Upload::find($data->data[$i][$j]);
						if(isset($img->name)) {
							$data->data[$i][$j] = '<img src="'.$img->path().'?s=50">';
						} else {
							$data->data[$i][$j] = "";
						}
					} else {
						$data->data[$i][$j] = "";
					}
				}
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
			     $medicalcase = Medicalcase::where('id', '=', $data->data[$i][0])->first();
				// if($col == $this->view_col) {
				// 	$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/couriermedicine/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				// }
				
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Couriermedicine", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/couriermedicine/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Couriermedicine", "delete")) {
					// $output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.couriermedicine.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					// $output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					// $output .= Form::close();
				}
				//if($data->data[$i][2]=="0"){
				$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/couriermedicine/assign/'.$data->data[$i][0]).'" class="btn btn-primary btn-xs" style="display:inline;padding:2px 5px 3px 5px; margin-left:8px">Assign</a>';

				$output .= '<a onClick="openSendSms('.$data->data[$i][0].')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px; margin-left:8px">Send SMS</a>';
				//}
				//else{
					$output .= ' <button class="btn btn-success btn-xs" type="button"><i class="fa fa-check" style="margin-right: 4px;"></i>Assigned</button>';
				//}
				
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
	function newcaseadd(){
	    $this->layout = null;
	    $getcaseId = rand(100000,999999);
	    echo $getcaseId; die();
	}
}

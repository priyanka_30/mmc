<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use DB;
use Validator;
use Datatables;
use Mail;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\TempCase;
use App\Models\Medicalcase;
use App\Models\CaseData;
use App\Models\CommunicationDetail;
use App\Models\Doctor;
use App\Models\CaseHeight;
use App\Models\Casereminder;
use App\Models\HomeoDetail;
use App\Models\CasePotency;
use App\Models\CaseAdditionalcharges;
use App\Models\CaseInvestigation;
use App\Models\CaseCbc;
use App\Models\CaseUrine;
use App\Models\CaseStool;
use App\Models\CaseArthritis;
use App\Models\CaseEndocrine;
use App\Models\CaseRenal;
use App\Models\CaseXray;
use App\Models\CaseUsgfemale;
use App\Models\CaseUsgmale;
use App\Models\CaseImmunology;
use App\Models\CaseSpecific;
use App\Models\CaseLiver;
use App\Models\CaseLipid;
use App\Models\CaseDiabetes;
use App\Models\CaseCardiac;
use App\Models\CaseSerological;
use App\Models\CaseSemenanalysis;
use App\Models\CaseNotes;
use App\Models\Bill;
use App\Models\CasePotencynew;
use App\Models\Dailytarget;
use App\Models\Bankdeposit;
use App\Models\Cashdeposit;
use App\Models\Record;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class MigrationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(Request $request)
    {
       
       
        return view('la.migration',[
           
        ]);
    }
    

   
    /* Import Case Datas */
    public  function savepersonaldata(Request $request)
    {
         $datas = DB::table('temp_personal')->limit(10000)->get();

         foreach($datas as $data){
            $check = DB::table('case_datas')->where('regid','=',$data->RegID)->first();
            $check1 = DB::table('medicalcases')->where('regid','=',$data->RegID)->first();
             if(!$check){
               $query= CaseData::updateOrCreate([
                    'id' => $data->ID,
                    'regid' => $data->RegID,
                    'assitant_doctor'=>$data->AssistantDoctorID, 
                    'title'=>$data->TitleID, 
                    'first_name' => $data->Name,
                    'middle_name'=>$data->Middlename, 
                    'surname'=>$data->Surname, 
                    'date_of_birth' => $data->DateOfBirth,
                    'gender'=>$data->Gender, 
                    'status'=>$data->StatusID, 
                    'address' => $data->Address,
                    'religion'=>$data->ReligionID, 
                    'occupation'=>$data->OccupationID, 
                    'phone' => $data->Phone,
                    'mobile1'=>$data->Mobile1, 
                    'mobile2'=>$data->Mobile2, 
                    'email'=>$data->Email, 
                    'send_sms'=>$data->SMSAlert, 
                    'area' => $data->Area,
                    'city'=>$data->City, 
                    'state'=>$data->State, 
                    'pin' => $data->PIN,
                    'road'=>$data->Road, 
                    'courier_outstation'=>$data->Outstation, 
                    'refered_by' => $data->RefBy, 
                    'city'=>$data->City, 
                    'state'=>$data->State, 
                    'pin' => $data->PIN,
                    'road'=>$data->Road, 
                    'courier_outstation'=>$data->Outstation, 
                    'refered_by' => $data->RefBy, 
                    'scheme' => $data->SchemeId, 
                ]);

            }

            if(!$check1){
            $query= Medicalcase::updateOrCreate([
                'regid' => $data->RegID,
                'doctorname'=>$data->AssistantDoctorID, 
                'name' => $data->Name,
                'date_birth' => $data->DateOfBirth,
                'mobile' => $data->Phone,
                'mobile2'=>$data->Mobile1, 
                'address' => $data->Address,
                'city'=>$data->City, 
                'package' => $data->PackageID,
                'dob' => $data->DateOfEntry,
                'date_left' => $data->EndDate,

            ]);

        }
       
        }
       // echo "success"; die();

        return redirect(config('laraadmin.adminRoute')."/migration");
    }


    /* Import Communication Table */
    public  function savecommunicationdetails(Request $request)
    {
         $datas = DB::table('temp_communication')->get();

         foreach($datas as $data){
            $check = DB::table('communication_details')->where('regid','=',$data->RegID)->first();
             if(!$check){
               $query= CommunicationDetail::updateOrCreate([
                    'regid' => $data->RegID,
                    'rand_id'=>$data->RegID, 
                    'currentdate'=>$data->CommunicationDate, 
                    'complaint_intesity' => $data->Comments,
                ]);

            }

       
        }

        return redirect(config('laraadmin.adminRoute')."/migration");
    }

    /* Import Doctors Table */
    public  function savedoctorsdetail(Request $request)
    {
         $datas = DB::table('temp_doctors')->get();

         foreach($datas as $data){
            $check = DB::table('doctors')->where('id','=',$data->ID)->first();
             if(!$check){
               $query= Doctor::updateOrCreate([
                    'name'=>$data->AssistantDoctor, 
                ]);

            }

       
        }

        return redirect(config('laraadmin.adminRoute')."/migration");
    }

    /* Import HeightWeight Table */
    public  function saveheightweightdetails(Request $request)
    {
         $datas = DB::table('temp_heightweight')->get();

         foreach($datas as $data){
            $check = DB::table('case_heights')->where('id','=',$data->ID)->first();
             if(!$check){
               $query= CaseHeight::updateOrCreate([
                    'regid'=>$data->RegID, 
                    'rand_id'=>$data->RegID, 
                    'dob'=>$data->RecordDate, 
                    'height'=>$data->Height, 
                    'weight'=>$data->Weight, 
                ]);

            }

       
        }

        return redirect(config('laraadmin.adminRoute')."/migration");
    }


    /* Import Reminders Table */
    public  function savereminderdetails(Request $request)
    {
         $datas = DB::table('temp_reminder')->get();

         foreach($datas as $data){
            $check = DB::table('case_reminder')->where('id','=',$data->id)->first();
             if(!$check){
               $query= Casereminder::updateOrCreate([
                    'patient_name'=>$data->patient_name, 
                    'start_date'=>$data->start_date, 
                    'heading'=>$data->heading, 
                    'comments'=>$data->comments,
                ]);

            }

       
        }

        return redirect(config('laraadmin.adminRoute')."/migration");
    }

    /* Import Reminders Table */
    public  function savehomeodetails(Request $request)
    {
         $datas = DB::table('temp_personal')->get();

         foreach($datas as $data){
            $check = DB::table('homeo_details')->where('regid','=',$data->RegID)->first();
             if(!$check){
             	if($data->Skin=="FALSE"){
             		$skin = "N/A";
             	}
             	else{
             		$skin = "Skin";
             	}
             	
             	if($data->DisabilityDisorder=="FALSE"){
             		$disability = "N/A";
             	}
             	else{
             		$disability = "Disability Disorder";
             	}

             	if($data->PCOD=="FALSE"){
             		$pcod = "N/A";
             	}
             	else{
             		$pcod = "PCOD";
             	}

             	if($data->Tyroid=="FALSE"){
             		$thyroid = "N/A";
             	}
             	else{
             		$thyroid = "Thyroid";
             	}

             	if($data->LifestyleDisorder=="FALSE"){
             		$lifestyle = "N/A";
             	}
             	else{
             		$lifestyle = "Lifestyle Disorder";
             	}
               $query= HomeoDetail::updateOrCreate([
                    'regid'=>$data->RegID, 
                    'homeo_date'=>$data->DateOfLastUpdate, 
                    'diagnosis'=>$data->Diagnosis, 
                    'complaint_intesity'=>$data->ComplaintIntensity,
                    'medication_taking'=>$data->MedicationTaking, 
                    'investigation'=>$data->Investigation, 
                    'criteria'=>$skin.",".$disability.",".$pcod.",".$thyroid.",".$lifestyle, 
                ]);

            }

       
        }

        return redirect(config('laraadmin.adminRoute')."/migration");
    }

    /* Import Followp Data Table */
    public  function savefollowupdetails(Request $request)
    {
         $datas = DB::table('temp_followup')->get();

         foreach($datas as $data){
            $check = DB::table('case_potencies')->where('id','=',$data->ID)->first();
            $check1 = DB::table('additional_charges')->where('id','=',$data->ID)->first();
             if(!$check){
               $query= CasePotency::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>$data->FollowupDate, 
                    'rxremedy'=>$data->RemedyID, 
                    'rxpotency'=>$data->PotencyID,
                    'rxfrequency'=>$data->FrequencyID, 
                    'rxdays'=>$data->DaysID,
                    'rxprescription'=>$data->Additional,
                ]);


            }
       
        }

        return redirect(config('laraadmin.adminRoute')."/migration");
    }

     /* Import Additional charges Table */
    public  function saveadditionalcharges(Request $request)
    {
         $datas = DB::table('temp_appiledcharges')->offset(40000)->limit(5000)->get();
         $id = 40001;
         foreach($datas as $data){
            $check = DB::table('additional_charges')->where('id','=',$data->ID)->first();
            $randid = date('Ymd',strtotime($data->ChargesDate)).''.$data->PersonalID;
             if(!$check){
               $query= CaseAdditionalcharges::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=> $randid, 
                    'dateval'=>$data->ChargesDate, 
                    'additional_price'=>$data->Amount, 
                    'additional_name'=>$data->ChargesID,
                ]);


            }
       
        }

        return redirect(config('laraadmin.adminRoute')."/migration");
    }

    /* Import Investigation Table */
    public  function saveinvestigation(Request $request)
    {
         $datas = DB::table('temp_investigation')->get();

         foreach($datas as $data){
            $check = DB::table('case_investigation')->where('id','=',$data->ID)->first();
            $checkcbc = DB::table('case_cbc')->where('id','=',$data->ID)->first();
            $checkurine = DB::table('case_urine')->where('id','=',$data->ID)->first();
            $checkstool = DB::table('case_stool')->where('id','=',$data->ID)->first();
            $checkarthris = DB::table('case_arthritis')->where('id','=',$data->ID)->first();
            $checkendorcrine = DB::table('case_endocrine')->where('id','=',$data->ID)->first();
            $checkrenal = DB::table('case_renal_profile')->where('id','=',$data->ID)->first();
            $checkxray = DB::table('case_xray')->where('id','=',$data->ID)->first();
            $checkfemale = DB::table('case_usgfemale')->where('id','=',$data->ID)->first();
            $checkmale = DB::table('case_usgmale')->where('id','=',$data->ID)->first();
            $checkimmunology = DB::table('case_immunology')->where('id','=',$data->ID)->first();
            $checkspecific = DB::table('case_specific')->where('id','=',$data->ID)->first();
            $checkliver = DB::table('case_liver_profile')->where('id','=',$data->ID)->first();
            $checklipid = DB::table('case_lipid_profile')->where('id','=',$data->ID)->first();
            $checkdiabetes = DB::table('case_diabetes')->where('id','=',$data->ID)->first();
            $checkcardiac = DB::table('case_cardiac')->where('id','=',$data->ID)->first();
            $checkserology = DB::table('case_serological_reports')->where('id','=',$data->ID)->first();
            $checksemen = DB::table('case_semenanalysis')->where('id','=',$data->ID)->first();

             /*if(!$check){
               $query= CaseInvestigation::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'invest_date'=>date('d/m/Y', strtotime($data->InvestigationDate)), 
                    'hb'=>$data->HB, 
                    't3'=>$data->T3,
                    't4'=>$data->T4, 
                    'tsh'=>$data->TSH, 
                    'totalchol'=>$data->TotalChol,
                    'hdichol'=>$data->HdiChol,
                ]);


            }

            if(!$checkcbc){
               $query= CaseCbc::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)), 
                    'hb'=>$data->HB, 
                    'rbc'=>$data->InvestigationCBCRBC,
                    'wbc'=>$data->WBC, 
                    'platelets'=>$data->Platelets, 
                    'vitaminb'=>$data->VitaminB12,
                    'vitamind'=>$data->OhVitaminD,
                    'neutrophils'=>$data->Neutrophilis, 
                    'lymphocytes'=>$data->Lymphocyte,
                    'eosinophils'=>$data->Eosinophilis, 
                    'monocytes'=>$data->Monocytes, 
                    'basophils'=>$data->Basophilis,
                    'band_cells'=>$data->Bandcells,
                    'abnor_rbc'=>$data->ARBC,
                    'abnor_wbc'=>$data->AWBC, 
                    'parasites'=>$data->Parasites, 
                    'esr'=>$data->ESR,
                ]);


            }


            if(!$checkurine){
               $query= CaseUrine::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)), 
                    'quantity'=>$data->Quantity, 
                    'color'=>$data->InvestigationUrineColor,
                    'appearance'=>$data->Appearance, 
                    'reaction'=>$data->InvestigationUrineReaction, 
                    'gra'=>$data->GRA,
                    'protein'=>$data->Protien,
                    'sugar'=>$data->Sugar, 
                    'acetone'=>$data->Acetone,
                    'pigments'=>$data->BilePigments, 
                    'occult_blood'=>$data->InvestigationUrineOccultBlood, 
                    'urobilinogen'=>$data->Urobili,
                    'epith_cells'=>$data->EpithCells,
                    'pus_cells'=>$data->InvestigationUrinePusCells,
                    'rbc'=>$data->InvestigationUrineRBC, 
                    'other_findings'=>$data->InvestigationUrineOtherFindings, 
                ]);


            }
            

            if(!$checkstool){
               $query= CaseStool::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'color'=>$data->InvestigationStoolColor,
                    'consistency'=>$data->Consistancy, 
                    'mucus'=>$data->Mucus, 
                    'frank_blood'=>$data->FrankBlood,
                    'adult_worm'=>$data->AdultWorm,
                    'reaction'=>$data->InvestigationStoolReaction, 
                    'pus_cells'=>$data->InvestigationStoolPusCells,
                    'occult_blood'=>$data->InvestigationStoolOccultBlood, 
                    'macrophages'=>$data->Macrophages, 
                    'rbc'=>$data->InvestigationStoolRBC,
                    'ova'=>$data->OVA,
                    'cysts'=>$data->Cysts,
                    'protozoa'=>$data->Protozoa,
                    'yeast_cell'=>$data->YeastCells, 
                    'other_findings'=>$data->InvestigationStoolOtherFindings, 
                ]);


            } 

            if(!$checkarthris){
               $query= CaseArthritis::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'anti_o'=>$data->AntiStreptolysin,
                    'accp'=>$data->ACCP, 
                    'ra_factor'=>$data->RA, 
                    'alkaline'=>$data->AlkalinePhosphatase,
                    'ana'=>$data->ANA,
                    'c_react'=>$data->ReactiveProtein, 
                     'c4'=>$data->Complement4,
                ]);


            } 

            if(!$checkendorcrine){
               $query= CaseEndocrine::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    't3'=>$data->T3,
                    't4'=>$data->T4, 
                    'tsh'=>$data->TSH, 
                    'ft3'=>$data->FT3,
                    'ft4'=>$data->FT4,
                    'anti_tpo'=>$data->AntiTPO, 
                    'antibody'=>$data->AntibodyThyroglobulin,
                    'prolactin'=>$data->Prolactin,
                    'fsh'=>$data->FSHDay3, 
                    'lsh'=>$data->LHSurge, 
                    'progesterone_3'=>$data->ProgesteroneDay3,
                    'progesterone'=>$data->Progesterone,
                    'dhea'=>$data->DHEA, 
                    'testosterone'=>$data->Testosterone,
                    'ama'=>$data->AMA, 
                    'insulin'=>$data->FastingInsulin,
                ]);


            } 

            if(!$checkrenal){
               $query= CaseRenal::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'bun'=>$data->BUN,
                    'urea'=>$data->Urea, 
                    'creatinine'=>$data->Creatinine, 
                    'uric_acid'=>$data->UricAcid,
                    'calcium'=>$data->Calcium,
                    'phosphorus'=>$data->Phosphorus, 
                    'sodium'=>$data->Sodium,
                    'potassium'=>$data->Potassium,
                    'chloride'=>$data->Chloride, 
                ]);
            }   

            if(!$checkxray){
               $query= CaseXray::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'radiological_report'=>$data->Radiology,
                ]);
            } 

            if(!$checkfemale){
               $query= CaseUsgfemale::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'uterues_size'=>$data->UterusSize,
                    'thickness'=>$data->EndometrialThickness, 
                    'fibroids_no'=>$data->FibroidsNumber, 
                    'description'=>$data->Description,
                    'ovary_size_rt'=>$data->SizeRt,
                    'ovary_size_lt'=>$data->SizeLt, 
                    'ovary_volume_rt'=>$data->VolumeRt,
                    'ovary_volume_lt'=>$data->VolumeLt,
                    'follicles_rt'=>$data->FolliclesRt, 
                    'follicles_lt'=>$data->FolliclesLt, 
                ]);
            }

            if(!$checkmale){
               $query= CaseUsgmale::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'size'=>$data->ProstateSize,
                    'volume'=>$data->ProstateVolume, 
                    'serum_psa'=>$data->Serum, 
                    'other_findings'=>$data->InvestigationMaleUSGOtherfindings, 
                ]);
            }

            if(!$checkimmunology){
               $query= CaseImmunology::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'igg'=>$data->IgG,
                    'ige'=>$data->IgE, 
                    'igm'=>$data->IgM, 
                    'iga'=>$data->IgA, 
                    'itg'=>$data->Ttg, 
                ]);
            }

            if(!$checkspecific){
               $query= CaseSpecific::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'define_field1'=>$data->Field1,
                    'define_field2'=>$data->Field2, 
                    'define_field3'=>$data->Field3, 
                    'define_field4'=>$data->Field4, 
                ]);
            } 


            if(!$checkliver){
               $query= CaseLiver::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'total_bil'=>$data->TotalBil,
                    'dir_bilirubin'=>$data->DirBil, 
                    'ind_bilirubin'=>$data->IndBil, 
                    'gamma_gt'=>$data->GammaGT, 
                    'total_protein'=>$data->TotalProtiens,
                    'albumin'=>$data->Albumin, 
                    'globulin'=>$data->Globilin, 
                    'sgot'=>$data->SGOT, 
                    'sgpt'=>$data->SGPT, 
                    'alk_phos'=>$data->AlkPhos,
                    'aust_antigen'=>$data->Aust, 
                    'amylase'=>$data->Amylase, 
                ]);
            }  

            if(!$checklipid){
               $query= CaseLipid::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'total_cholesterol'=>$data->TotalChol,
                    'triglycerides'=>$data->Trigly, 
                    'hdl_cholesterol'=>$data->HdiChol, 
                    'ldl_cholesterol'=>$data->LdlChol, 
                    'vldl'=>$data->VLDL,
                    'hdl_ratio'=>$data->Hdlc, 
                    'ldl_hdl'=>$data->LDL_HDL, 
                    'lipoprotein'=>$data->Lipoprotein, 
                    'apolipoprotein_a'=>$data->ApolipoproteinA1, 
                    'apolipoprotein_b'=>$data->ApolipoproteinB,
                ]);
            } 

            if(!$checkdiabetes){
               $query= CaseDiabetes::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'blood_fasting'=>$data->BFasting,
                    'blood_prandial'=>$data->BPost, 
                    'blood_random'=>$data->BRandom, 
                    'urine_fasting'=>$data->UFasting, 
                    'urine_prandial'=>$data->UPost,
                    'urine_random'=>$data->URandom, 
                    'glu_test'=>$data->GluTest, 
                    'glycosylated_hb'=>$data->GlyHB, 
                ]);
            } */ 

            if(!$checkcardiac){
               $query= CaseCardiac::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'homocysteine'=>$data->Homocysteine,
                    'ecg'=>$data->ECG, 
                    'decho'=>$data->DECHO, 
                ]);
            }

            if(!$checkserology){
               $query= CaseSerological::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'serological_report'=>$data->SerologyReport,
                ]);
            } 

            if(!$checksemen){
               $query= CaseSemenanalysis::updateOrCreate([
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>rand('99',9999), 
                    'dateval'=>date('d/m/Y', strtotime($data->InvestigationDate)),
                    'semen_analysis'=>$data->SemenAnalysis,
                ]);
            }  
        }

        return redirect(config('laraadmin.adminRoute')."/migration");
    }

    /* Import Followp Data Table */
    public  function savefollowupnotes(Request $request)
    {
         $datas = DB::table('temp_followup1')->offset(10000)->limit(5000)->get();
         $id = 129527;
         foreach($datas as $data){
            $tempid = $data->ID;
           $comment = DB::table('temp_followup2')->where('FollowupID','=',$tempid)->get();
         // echo "<pre>"; print_r($comment); 
            if(!empty($comment)){
            $randid = date('Ymd',strtotime($data->FollowupDate)).''.$data->PersonalID;
            $charges = DB::table('daycharges')->where('id','=',$data->DaysID)->first();
            $charges_days = $charges->regular_charges;
            $todaydt = $data->FollowupDate;
            $todate= date('n/d/Y', strtotime($todaydt. ' +'. $charges->days));
               $query= CasePotency::updateOrCreate([
                    'id' => $id,
                    'regid'=>$data->PersonalID, 
                    'rand_id'=>$randid, 
                    'dateval'=>$data->FollowupDate, 
                    'rxremedy'=>$data->RemedyID, 
                    'rxpotency'=>$data->PotencyID,
                    'rxfrequency'=>$data->FrequencyID, 
                    'rxdays'=>$data->DaysID,
                    'rxprescription'=>$data->Additional,
                    'todate' => date('Y-m-d',strtotime($todate)),
                ]);

               $query1= CaseNotes::updateOrCreate([
                    'regid'=>$id, 
                   'rand_id'=>$randid, 
                  'dateval'=>$data->FollowupDate, 
                    'notes'=>$comment[0]->Followup, 
                ]);

               $id++;
           }
       
        }
      // echo "<pre>"; print_r($datas); die();
        return redirect(config('laraadmin.adminRoute')."/migration");
    }

     /* Import Followp Data Table */
    public  function savebill(Request $request)
    {
         $datas = DB::table('temp_record')->offset(0)->limit(10000)->get();
       //  $id = 1;
         foreach($datas as $data){
          //  $randid = date('Ymd',strtotime($data->ChargesDate)).''.$data->PersonalID;
           
        //   echo "<pre>"; print_r($comment); 
           // if(!$comment){
               // $query= Bill::updateOrCreate([
               //      'id' => $id,
               //      'regid'=>$data->PersonalID, 
               //      'Treatment'=>$data->Treatment, 
               //      'Disease'=>$data->Disease, 
               //      'fromdate'=>$data->FromDate, 
               //      'todate'=>$data->ToDate,
               //      'to_date'=>date('Y-m-d',strtotime($data->ToDate)), 
               //      'Rupess'=>$data->Rupess,
               //      'BillNo'=>$data->BillNo,
               //      'DoctorID'=>$data->DoctorID, 
               //      'RupeesInAmount'=>$data->RupeesInAmount,
               //      'Days'=>$data->Days, 
               //      'BillDate'=>$data->BillDate,
               //      'payment_mode'=>'',
               //      'charges'=>$data->Charges,
               //      'received'=>$data->Received,
               //      'Balance'=>$data->Balance,
               //      'BalanceInAmount'=>$data->BalanceInAmount,
               //      'ReceivedInAmount'=>$data->ReceivedInAmount,
               //      'call_status'=>'',
               //      'call_date'=>'',
               //  ]);


                // $query= CaseAdditionalcharges::updateOrCreate([
                //     'id' => $id,
                //     'regid'=>$data->PersonalID, 
                //     'rand_id'=>$randid, 
                //     'dateval'=>$data->ChargesDate, 
                //     'additional_name'=>$data->ChargesID, 
                //     'additional_price'=>$data->Amount,
                // ]);


                //     $query= Bankdeposit::updateOrCreate([
                //    // 'id' => $id,
                //     'deposit_date'=>date('d/m/Y',strtotime($data->RecordDate)), 
                //     'dateval'=>date('Y-m-d',strtotime($data->RecordDate)), 
                //     'amount'=>$data->BankDeposit,
                //     'created_at' =>date('Y-m-d',strtotime($data->RecordDate))
                // ]);


                    $query= Record::updateOrCreate([
                    // 'id' => $id,
                    'regid'=>$data->RegID, 
                    'comment'=>$data->InsTransfeructions,
                    'name'=>$data->CallerName, 
                    'mobile'=>$data->Mobile1,
                    'recordtype'=>$data->CallType, 
                    'recorddate'=>date('Y-m-d',strtotime($data->CallDate)),
                    'created_at' =>date('Y-m-d',strtotime($data->CallDate))
                ]);

               

              // $id++;
           // }
       
        }
      // echo "<pre>"; print_r($datas); die();
        return redirect(config('laraadmin.adminRoute')."/migration");
    }

}